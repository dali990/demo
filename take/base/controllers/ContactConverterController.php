<?php

class ContactConverterController extends SIMAController 
{
    public function actionIndex() 
    {
        $contacts_types_list = CHtml::listData(ContactType::model()->findAll(), 'id', 'name');
        $phone_number_types = CHtml::listData(PhoneNumberType::model()->findAll(), 'id', 'name');
        
        $uniq = SIMAHtml::uniqid();
        
        $html_partial = $this->renderPartial('list_contacts', ['uniq' => $uniq], true, false);
        
        Yii::app()->clientScript->registerScript(
            "contacts_$uniq",
            "
                $('#contact-gui-table$uniq').simaGuiTable('hideColumns', ['phone_number', 'email']).simaGuiTable('populate');
                $('#phone_number_types_$uniq').hide();
            ",
            CClientScript::POS_READY
        );
        
        $html = Yii::app()->controller->renderContentLayout(
            [
                'name' => Yii::t('BaseModule.ContactConverter', 'ContactConverter'),
                'options'=> [
                    [
                        'html'=> SIMAHtml::dropDownList(null, null, $contacts_types_list, 
                            [
                                'empty' => Yii::t('BaseModule.ContactConverter', 'ContactType'), 
                                'id'    => "contact-converter-type$uniq",
                                'onchange' => "sima.contactConverter.contactTypeChange('$uniq')"
                            ]
                        )
                    ],
                    [
                        'html'=> SIMAHtml::dropDownList(null, null, 
                            [
                                'phone_number' => Yii::t('BaseModule.PhoneNumber', 'PhoneNumber'),
                                'email' => Yii::t('Contact','Email')
                            ], 
                            [
                                'empty' => Yii::t('BaseModule.ContactConverter', 'ConvertType'), 
                                'id'    => "contact-converter-typeto$uniq",
                                'onchange' => "sima.contactConverter.convertTypeChange('$uniq')"
                            ]
                        )
                    ],
                    [
                        'html'=> SIMAHtml::dropDownList(null, null, $phone_number_types, 
                            [
                                'empty' => Yii::t('BaseModule.PhoneNumber', 'PhoneNumberTypes'), 
                                'id'    => "phone_number_types_$uniq",
                                'onchange' => "sima.contactConverter.phoneNumberTypeChange('$uniq')"
                            ]
                        )
                    ],
                ]
            ],
            [
                [
                    'html'=> $html_partial
                ],
            ],
            ['id'=>'contact-converter-'.$uniq,' class'=>'sima-layout-contact-converter']
        );
                
        $this->respondOK([
            'html' => $html,
            'title' => Yii::t('BaseModule.ContactConverter', 'ContactConverter')
        ]);               
    }
    
    public function actionSaveContacts() 
    {
        $error_msgs = null;
        $contacts   = $this->filter_post_input('contacts', false);

        if (!empty($contacts))
        {
            foreach ($contacts as $contact_data) 
            {
                $contact = Contact::model()->findByPk($contact_data['contact_id']);
                if (empty($contact))
                {
                    continue;
                }

                //Cuvamo kontakt tipa telefon/mob
                if (isset($contact_data['phone_params']))
                {
                    $phone_params = $contact_data['phone_params'];
                    $phoneNumber = PhoneNumber::model()->findByAttributes([
                        'country_id' => $phone_params['country_id'],
                        'provider_number' => $phone_params['provider_number'],
                        'phone_number' => $phone_params['phone_number'],
                    ]);
                    if (empty($phoneNumber))
                    {
                        $phoneNumber = new PhoneNumber();
                        $phoneNumber->country_id      = $phone_params['country_id'];
                        $phoneNumber->provider_number = $phone_params['provider_number'];
                        $phoneNumber->phone_number    = $phone_params['phone_number'];
                    }
                    $phoneNumber->type_id = $contact_data['phone_type'];
                    $phoneNumber->partner_id = $contact->partner->id;
                    $phoneNumber->comment = $contact->comment;
                    
                    if ($phoneNumber->validate())
                    {
                        $phoneNumber->save();
                        if(!empty($contact->partner->main_contact_id) && $contact->partner->main_contact_id === $contact->id)
                        {
                            $contact->partner->main_phone_number_id = $phoneNumber->id;
                            $contact->partner->main_contact_id = null;
                            $contact->partner->save();
                        }
                        $contact->delete();
                    }
                    else 
                    {
                        $error_msgs .= "<b>".$contact->value."</b><br> ";
                        $error_msgs .= SIMAHtml::showErrorsInHTML($phoneNumber)."<br>";
                    }
                }
                //Cuvamo kontakt tipa email
                else if (isset($contact_data['email'])) 
                {
                    $emailAddress = EmailAddress::model()->find([
                        'model_filter' => [
                            'email' => $contact_data['email']
                        ]
                    ]);
                    if (empty($emailAddress))
                    {
                        $emailAddress = new EmailAddress();
                        $emailAddress->email = $contact_data['email'];
                    }
                    $emailAddress->owner_id = $contact->partner_id;
                    $emailAddress->comment = $contact->comment;
                    
                    if ($emailAddress->validate())
                    {
                        $emailAddress->save();
                        if(!empty($contact->partner->main_contact_id) && $contact->partner->main_contact_id === $contact->id)
                        {
                            $contact->partner->main_email_address_id = $emailAddress->id;
                            $contact->partner->main_contact_id = null;
                            $contact->partner->save();
                        }
                        $contact->delete();
                    }
                    else 
                    {
                        $error_msgs .= "<b>".$contact->value."</b><br> ";
                        $error_msgs .= SIMAHtml::showErrorsInHTML($emailAddress)."<br>";
                    }                    
                }

            }            
        }

        if (!empty($error_msgs)) 
        {   
           $this->raiseNote($error_msgs);
        }
        
        $this->respondOK();
    }    
}