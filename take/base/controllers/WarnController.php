<?php

class WarnController extends SIMAController
{
    public function actionRenderWarnsForModel($model_name, $model_id)
    {
        $model = $model_name::model()->findByPk($model_id);
        $html = '';
        if (!is_null($model))
        {
            $html = Yii::app()->warnManager->renderWarnsForModel($model);
        }

        $this->respondOK([
            'html' => $html
        ]);
    }

    public function actionCheckResolveWarnForModel($model_name, $model_id, $warn_code)
    {
        $model = $model_name::model()->findByPk($model_id);
        if (is_null($model))
        {
            throw new SIMAWarnExceptionModelNotFound($model_name);
        }

        $warn_definition = Yii::app()->warnManager->getWarnForModelDefinition($warn_code, $model_name);
        if (!isset($warn_definition['checkResolveFunc']))
        {
            throw new SIMAException(Yii::t('BaseModule.Warns', 'checkResolveFuncNotFound', [
                '{warn_code}' => $warn_code,
                '{model_name}' => $model_name
            ]));
        }
        
        $response = call_user_func($warn_definition['checkResolveFunc'], Yii::app()->user->model, $model);

        if (!isset($response['status']))
        {
            throw new SIMAException(Yii::t('BaseModule.Warns', 'checkResolveFuncIncorrectResponse'));
        }

        $this->respondOK($response);
    }
    
    public function actionResolveWarnForModel($model_name, $model_id, $warn_code)
    {
        $model = $model_name::model()->findByPk($model_id);
        if (is_null($model))
        {
            throw new SIMAWarnExceptionModelNotFound($model_name);
        }

        $warn_definition = Yii::app()->warnManager->getWarnForModelDefinition($warn_code, $model_name);
        if (!isset($warn_definition['resolveFunc']))
        {
            throw new SIMAException(Yii::t('BaseModule.Warns', 'resolveFuncNotFound', [
                '{warn_code}' => $warn_code,
                '{model_name}' => $model_name
            ]));
        }
        
        call_user_func($warn_definition['resolveFunc'], Yii::app()->user->model, $model);
        
        $this->respondOK([], $model);
    }
    
    public function actionRenderWarnsForUser($user_id)
    {
        $user = User::model()->findByPkWithCheck($user_id);
        
        $user_warns = Yii::app()->warnManager->getWarns($user);
        
        $html = '';
        foreach ($user_warns as $user_warn_key => $user_warn_value)
        {
            if ($user_warn_value > 0)
            {
                $user_warn_value = intval($user_warn_value);
                $user_warn_title = Yii::app()->warnManager->getWarnDisplayName($user_warn_key);
                $html .= 
                    "<div style='padding: 10px;'>
                        <span>$user_warn_title</span>: 
                        <span>$user_warn_value</span>
                    </div>";
            }
        }
        
        $this->respondOK([
            'html' => $html
        ]);
    }
}

