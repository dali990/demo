<?php

/**
  How to use:

  $this->widget('base.extensions.SIMAMultiSearchField.SIMAMultiSearchField', array(
       'id' => 'neki_uniq', //ako se ne zada onda se generise automatski uniq za komponentu
       'model' => 'ime_nekog_modela ili model',
       'relation' => 'neka_relacija',
       'name' => 'neko_ime', //obicno se ne zadaje kada se zadaje model. Sluzi samo ako nije zadat model i onda se ne pretrazuju vec unose novi pojmovi
       //Inicijalne vrednosti. Mora biti niz nizova, gde svaki podniz ima id i display_name
       'value' => [
            [
                'id'=>1,
                'display_name'=>'test1'
            ],
            [
                'id'=>2,
                'display_name'=>'test2'
            ]
       ],       
       'action' => 'naziv_akcije', //akcija koja ce se pozvati da pretrazi model ili njegovu relaciju. Default je definisana u parametru searchFieldAction
       'filter' => ['username'=>'aaaa'], //dodatni filter koji sluzi da se suzi pretraga modela i njegove relacije
       'htmlOptions' => [] //niz dodatnih html opcija
   ));
 */
class SIMAMultiSearchField extends CInputWidget 
{
    public $id = null;
    public $model = null;
    public $relation = null; //relacija u kojoj se pretrazuje
    public $name = '';
    //value mora biti niz nizova, gde svaki podniz ima id i display_name
    public $value = [];
    public $action = null;
    public $filter = [];
    public $htmlOptions = [];    
    
    public function run() 
    {
        if (is_null($this->model) && $this->name === '')
        {
            throw new Exception('Mora biti zadat ili model ili name.');
        }
        $id = (!is_null($this->id))?$this->id:SIMAHtml::uniqid();
        $model = $this->model;
        if (is_string($model))
        {
            $model = $model::model();
        }
        $relation = $this->relation;
        $value = $this->value;
        if (!is_array($value))
        {
            throw new Exception('Vrednost(value) mora biti zadata kao niz.');
        }
        $action = (!is_null($this->action))?$this->action:Yii::app()->params['searchFieldAction'];
        $filter = $this->filter;
        $htmlOptions = $this->htmlOptions;
        if (!isset($htmlOptions['placeholder']))
        {
            $htmlOptions['placeholder'] = 'Pretraži';
        }
        
        if (!is_null($model) && !is_null($relation))
        {
            $name = get_class($model)."[".str_replace('.', '][', $relation)."][ids]";
        }
        else if (!is_null($model))
        {            
            $name = get_class($model);
        }
        else
        {
            $name = $this->name;
        }        
        
        echo $this->render('index',[
            'id'=>$id,
            'model'=>$model,
            'name'=>$name,
            'value'=>$value,            
            'htmlOptions'=>$htmlOptions
        ]);
        
        $json = CJSON::encode([
            'action'=>$action,
            'model'=>(!is_null($model))?get_class($model):null,
            'relation'=>$relation,
            'value'=>$value,
            'filter'=>$filter            
        ]);
        
        $register_script = "$('#".$id."').simaMultiSearchField('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }

    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.simaMultiSearchField.js', CClientScript::POS_HEAD);
        $css_file = $baseUrl . '/css/simaMultiSearchField.css';
        Yii::app()->clientScript->registerCssFile($css_file);
    }

}
