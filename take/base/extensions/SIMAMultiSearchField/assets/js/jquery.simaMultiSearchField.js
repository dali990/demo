
(function($){
    
    var methods = {
        
        init : function(params) {
            
            var _this = this;            
            params = params || {};
            
            if (typeof params.action !=='undefined' && params.action !== null)
                _this.data('action', params.action);
            if (typeof params.model !=='undefined' && params.model !== null)
                _this.data('model', params.model);
            if (typeof params.relation !=='undefined' && params.relation !== null)
                _this.data('relation', params.relation);
            if (typeof params.value !=='undefined' && params.value !== null)
                _this.data('value', params.value);
            if (typeof params.filter !=='undefined' && params.filter !== null)   
                _this.data('filter', params.filter);
            
            var sf_value_input = _this.find('.sima-multi-sf-value-input');
            var sf_display_wrapper = _this.find('.sima-multi-sf-display-wrapper');
            var sf_display_list = _this.find('.sima-multi-sf-display-list');
            var sf_display_input = _this.find('.sima-multi-sf-display-input');
            var sf_result = $("<div class='sima-multi-sf-result' tabindex='-1'></div>");
            
            $("body").append(sf_result);
            _this.bind('destroyed', function() {                
                sf_result.remove();                
            });            
            
            _this.data('sf_value_input', sf_value_input);
            _this.data('sf_display_wrapper', sf_display_wrapper);
            _this.data('sf_display_list', sf_display_list);
            _this.data('sf_display_input', sf_display_input);
            _this.data('sf_result', sf_result);            
            
            _this.data('globalTimeout', null);
            _this.data('refresh_timeout', null);
            _this.data('group', sima.uniqid()); //id grupe za koju se zovu ajax pozivi
            
            sf_result.bind("mousedownoutside", function(event){                
                hideResult(_this);
            });
                     
           sf_display_input.on('keydown', {_this:_this}, onDisplayInputKeydown);
           sf_display_list.on('click', '.list-item .remove', {_this:_this}, onRemoveListItem);
           sf_result.on('click', '.sima-ui-sf-choice', {_this:_this}, onResultChoice);
           sf_result.on('keydown', {_this:_this}, onResultKeydown);
           
           //inicijalno setovanje list item-a
           if (typeof _this.data('value') !== 'undefined')
           {
               $.each(_this.data('value'), function(index, value) {
                    appendListItem(_this, value.id, value.display_name);
               });
           }
           
           setDisplayInputWidth(_this);
        },
        refresh : function()
        {
            var _this = this;
            if (typeof _this.data('sf_result') !== 'undefined')
            {                
                if(_this.data('refresh_timeout') !== null)
                {
                    clearTimeout(_this.data('refresh_timeout'));
                }  
                _this.data('refresh_timeout', setTimeout(function(){
                    if (typeof _this.data('sf_result') !== 'undefined')
                    {
                        var left = _this.data('sf_display_wrapper').offset().left;
                        var top = _this.data('sf_display_wrapper').offset().top + _this.data('sf_display_wrapper').outerHeight()+2;
                        _this.data('sf_result').css({left: left + 'px', top: top + 'px'});
                    }
                },5));
            }
        },
    };
    
    function onDisplayInputKeydown(event)
    {
        var _this = event.data._this;        
        if (event.which === 8 && _this.data('sf_display_input').caret() === 0 &&
                isTextSelected(_this.data('sf_display_input')[0]) === false) //backspace
        {            
            var last_item = _this.data('sf_display_list').find('.list-item').last();            
            if (typeof last_item !== 'undefined' && last_item.length > 0)
            {                
                removeListItem(_this, last_item);
            }
            event.stopPropagation();
            event.preventDefault();
        }
        else if (event.which === 27) //esc
        {            
            hideResult(_this);
            _this.data('sf_display_input').focus();
            event.stopPropagation();
            event.preventDefault();
        }
        else
        {
            if (typeof _this.data('model') !== 'undefined')
            {            
                if (event.which === 40) //down
                {            
                    if (_this.data('sf_result').children().first().length > 0)
                    {         
                        _this.data('sf_result').focus();
                        _this.data('sf_result').children('.selected').each(function(){
                            $(this).removeClass('selected');
                        });
                        _this.data('sf_result').children().first().addClass('selected');                
                    }
                    event.stopPropagation();
                    event.preventDefault();
                }
                else if (event.which !== 37 && event.which !== 38 && event.which !== 39 && event.which !== 27)
                {            
                    if(_this.data('globalTimeout') !== null) 
                    {
                        clearTimeout(_this.data('globalTimeout'));                    
                    } 
                    _this.data('globalTimeout', setTimeout(function(){
                        searchFunc(_this);
                    },500));
                }
            }
            else
            {
                if (event.which === 13) //enter
                {            
                    appendListItem(_this, null, _this.data('sf_display_input').val());
                    event.stopPropagation();
                    event.preventDefault();
                }
            }
        }
    }
    
    function onResultKeydown(event)
    {
        var _this = event.data._this;
        if (event.which === 27) //esc
        {
            hideResult(_this);
            _this.data('sf_display_input').focus();            
            event.stopPropagation();
            event.preventDefault();
        }
        else if (event.which === 38) //up
        {
            var selected = _this.data('sf_result').children('.selected');                    
            if (selected.length > 0)
            {
                if (selected.prev().length > 0)
                {
                    selected.removeClass('selected');
                    selected.prev().addClass('selected');                    
                }
                else
                {
                    selected.removeClass('selected');
                    _this.data('sf_display_input').focus();                    
                }
            }
            event.stopPropagation();
            event.preventDefault();
        }
        else if (event.which === 40) //down
        {
            var selected = _this.data('sf_result').children('.selected');
            if (selected.length > 0)
            {
                if (selected.next().is(':visible') === true && selected.next().length > 0)
                {
                    selected.removeClass('selected');
                    selected.next().addClass('selected');                    
                }
            }
            else
            {
                if (_this.data('sf_result').children().first().length > 0)
                {
                    _this.data('sf_result').children().first().addClass('selected');
                }
            }
            event.stopPropagation();
            event.preventDefault();
        }
        else if (event.which === 37 || event.which === 39) //left, right
        {
            event.stopPropagation();
            event.preventDefault();
        }
        else if (event.which === 13) //enter
        {
            var selected = _this.data('sf_result').children('.selected');
            if (selected.length > 0)
            {
                appendListItem(_this, selected.data('value'), selected.data('display'));                
            }
            event.stopPropagation();
            event.preventDefault();
        }
    }
    
    function onResultChoice(event)
    {
        var _this = event.data._this;
        var choice = $(this);
        appendListItem(_this, choice.data('value'), choice.data('display'));
    }        
    
    function onRemoveListItem(event)
    {
        var _this = event.data._this;
        var list_item = $(this).parents('.list-item:first');
        removeListItem(_this, list_item);
    }
    
    function searchFunc(_this)
    {
        var filter = _this.data('filter');
        if (jQuery.isEmptyObject(filter)) 
            filter={};
        filter.text = _this.data('sf_display_input').val();
        
        var get_params = {};
        get_params.Model = _this.data('model');
        if (typeof _this.data('relation') !== 'undefined')
        {
            get_params.relation = _this.data('relation');
        }
        showResult(_this);
        sima.ajax.get(_this.data('action'),{
            get_params:get_params,
            data:{
                filters:filter
            },
            group: _this.data('group'),
            async: true,
            loadingCircle: (_this.data('sf_result')),
            success_function:function(response){
                _this.data('sf_result').html(response.result);
                _this.data('sf_result').scrollTop(0);
                if (_this.data('sf_result').find('p').length === 0)
                {
                    hideResult(_this);
                }
            }
        });
    }
    
    function appendListItem(_this, id, value)
    {        
        if (id === null)
        {
            id = 1;
            var last_item = _this.data('sf_display_list').find('.list-item').last();
            if (typeof last_item !== 'undefined' && last_item.length > 0)
            {
                id = parseInt(last_item.data('id')) + 1;
            }
        }
        var list_item = _this.data('sf_display_list').find('.list-item[data-id="'+id+'"]');
        if (typeof list_item !== 'undefined' && list_item.length > 0)
        {            
            sima.dialog.openWarn('Već postoji!');
        }
        else if (value === '')
        {
            sima.dialog.openWarn('Vrednost ne može biti prazna!');
        }
        else
        {           
            list_item = $("<span class='list-item' data-id='"+id+"' data-display_name='"+value+"'><span class='name'>"+value+"</span><span class='remove'>x</span></span>");
            _this.data('sf_display_input').before(list_item);
            setValueInput(_this);
            hideResult(_this);
            _this.data('sf_display_input').val('');
            _this.data('sf_display_input').focus();
            setDisplayInputWidth(_this);
        }        
    }
    
    function removeListItem(_this, list_item)
    {
        list_item.remove();
        setValueInput(_this);
        setDisplayInputWidth(_this);
    }
    
    function setValueInput(_this)
    {
        var list = _this.data('sf_display_list');
        var value_input = _this.data('sf_value_input');
        var value = [];
        list.find('.list-item').each(function(){
            var list_item = $(this);
            var curr_value = {
                id:list_item.data('id'),
                display_name:list_item.data('display_name')
            };
            value.push(curr_value);
        });        
        value_input.val(JSON.stringify(value));
    }
    
    function setDisplayInputWidth(_this)
    {
        var items_width = 0;
        _this.data('sf_display_list').find('.list-item').each(function(){
            if ((items_width + $(this).outerWidth(true)) > (_this.data('sf_display_list').width() - 100))
            {                
                if ((items_width + $(this).outerWidth(true)) > _this.data('sf_display_list').width())
                {
                    items_width = $(this).outerWidth(true);
                }
                else
                {
                    items_width = 0;
                }
            }
            else
            {                
                items_width += $(this).outerWidth(true);
            }
        });        
        var width = _this.data('sf_display_list').width() - items_width - 4;
        _this.data('sf_display_input').css({width:width});
    }
    
    function showResult(_this)
    {
         _this.data('sf_result').show();
         _this.simaMultiSearchField('refresh');
    }
    
    function hideResult(_this)
    {
        _this.data('sf_result').hide();
    }    
    
    function isTextSelected(input)
    {
        var startPos = input.selectionStart;
        var endPos = input.selectionEnd;
        var doc = document.selection;

        if(doc && doc.createRange().text.length !== 0)
        {
            return true;
        }
        else if (!doc && input.value.substring(startPos,endPos).length !== 0)
        {
            return true;
        }
        return false;
    }
    
    $.fn.simaMultiSearchField = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.simaMultiSearchField');
            }
        });
        return ret;
    };  
})(jQuery);