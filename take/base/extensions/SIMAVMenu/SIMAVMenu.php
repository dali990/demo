<?php

class SIMAVMenu extends CWidget 
{
    public $id = null;
    public $menu_array = null;
    
    public function run() 
    {
        if(!isset($this->menu_array) || !is_array($this->menu_array))
        {
            throw new Exception(Yii::t('BaseModule.VMenu', 'InvalidMenuArray'));
        }
        
        if(empty($this->id))
        {
            $this->id = SIMAHtml::uniqid().'_v_menu';
        }
        
        echo $this->render('index', array(
            'id' => $this->id,
            'menu_array' => $this->menu_array
        ));
                        
        $params = array(
        );
        $json = CJSON::encode($params);
        $register_script = "$('#" . $this->id . "').vmenu('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
    
    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.vmenu.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/vmenu.css');
    }
}