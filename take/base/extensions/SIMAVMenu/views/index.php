<div id="<?php echo $id; ?>" class="sima-ui-vertical-menu scroll-container">
    <div class="sima-ui-vertical-menu-wrapper scroll-container">
        <?php
        echo $this->render('menuItem', array(
            'menu_array' => $menu_array,
            'level' => 1,
        ));
        ?>
    </div>
</div>

