<ul class="sima-ui-vertical-menu-wrapper-lev<?php echo $level; ?>">
    <?php
    foreach($menu_array as $menu_item)
    {
    ?>
        <li class="sima-ui-vmenu-item <?php if($level !== 1){ echo "_hidden"; } ?>" 
            <?php 
            if(isset($menu_item['datas']) && count($menu_item['datas']) > 0)
            {
                foreach($menu_item['datas'] as $key => $value)
                {
                    if(is_array($value))
                    {
                        $value = json_encode($value);
                    }
                ?>
                    data-<?php echo $key; ?>='<?php echo $value; ?>'
                <?php
                }
            }
            ?>
            >
            <a href="#">
                <?php 
                if(!empty($menu_item['submenu'])) 
                {
                ?>
                    <span class="sima-ui-vmenu-item-arrow sima-icon _arrow-right _24"></span>
                <?php
                }
                ?>
                <span class="sima-ui-vmenu-item-name" title="<?php echo $menu_item['display_name']; ?>">
                    <?php 
                        echo $menu_item['display_name'];
                    ?>
                </span>
                <?php
                    if(isset($menu_item['supscript']) )
                    {
                    ?>
                        <sup><?php echo $menu_item['supscript']; ?></sup>
                    <?php
                    }
                ?>
                
                <span class="sima-ui-vmenu-item-select sima-icon _selected_white _24"></span>
            </a>
            
            <?php
            if(isset($menu_item['submenu']) && count($menu_item['submenu']) > 0) 
            {
                echo $this->render('menuItem', array(
                    'menu_array' => $menu_item['submenu'],
                    'level' => $level+1,
                ));
            } 
            ?>
        </li>
    <?php
    }
    ?>
</ul>