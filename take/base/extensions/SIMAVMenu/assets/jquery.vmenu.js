/* global sima */

(function($){
    
    var methods = {
        init: function(params){
            var localThis = this;
            
            localThis.on('click', 'li', {localThis:localThis}, onMenuItemClick);
            localThis.on('click', '.sima-ui-vmenu-item-arrow', {localThis:localThis}, onArrowClick);   
        },
        getSelectedItem: function(itemLi) {
            var localThis = $(this);
            var selectedItem = localThis.find('.sima-ui-vmenu-item ').children('a.active');
            return selectedItem.parent();
        },
        selectItem: function(itemLi) {
            var localThis = $(this);
            selectItemLi(localThis,itemLi);
        }
    };
    
    function onMenuItemClick(event)
    {        
        event.stopPropagation();
                
        var localThis = event.data.localThis;
        
        var childA = $(this).children('a');
        
        if(childA.children('span').hasClass('_arrow-right'))
        {
            openItemLi(localThis, $(this));
        }
        
        if(!childA.hasClass('active'))
        {
            deselectAllLis(localThis);
            selectItemLi(localThis, $(this));
        }
    }
    
    function onArrowClick(event)
    {
        event.stopPropagation();
        
        var localThis = event.data.localThis;
        
        var parentLi = $(this).parent('a').parent('li');
        
        if($(this).hasClass('_arrow-right'))
        {
            openItemLi(localThis, parentLi);
        }
        else if($(this).hasClass('_arrow-bottom'))
        {
            closeItemLi(localThis, parentLi);
        }
    }
    
    function openItemLi(localThis, itemLi)
    {
        itemLi.children('ul').children('li').removeClass('_hidden');
        
        var childA = itemLi.children('a');
        var arrow_span = childA.children('span.sima-ui-vmenu-item-arrow');
        arrow_span.removeClass('_arrow-right');
        arrow_span.addClass('_arrow-bottom');
                
        localThis.trigger('openItem', [itemLi]);
    }
    
    function closeItemLi(localThis, itemLi)
    {
        itemLi.children('ul').children('li').addClass('_hidden');
        
        var arrow_span = itemLi.children('a').children('span.sima-ui-vmenu-item-arrow');
        arrow_span.removeClass('_arrow-bottom');
        arrow_span.addClass('_arrow-right');
                
        localThis.trigger('closeItem', [itemLi]);
    }
    
    function selectItemLi(localThis, itemLi)
    {
        var childA = itemLi.children('a');
        childA.addClass('active');
                
        localThis.trigger('selectItem', [itemLi]);
    }
    
    function deselectAllLis(localThis)
    {        
        var activeAs = localThis.find('a.active');
        
        for(var i=0; i<activeAs.length; i++)
        {
            $(activeAs[i]).removeClass('active');
        }
    }
    
    $.fn.vmenu = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.vmenu');
            }
        });
        return ret;
    };
    
})( jQuery );