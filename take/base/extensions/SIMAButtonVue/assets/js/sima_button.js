/* global Vue, sima */

Vue.component('sima-yii-button', {
    props: {
        button_id: {
            type: String,
            default: ''
        },
        title: {
            type: String,
            default: ''
        },
        onclick: {
            type: [Object, Array],
            default: function(){
                return {};
            }
        },
        nohoverborder: {
            type: Boolean,
            default: false
        },
        half: {
            type: Boolean,
            default: false
        },
        icon: {
            type: String,
            default: ''
        },
        iconsize: {
            type: Number,
            default: 0
        },
        noborder: {
            type: Boolean,
            default: false
        },
        transparentbackground: {
            type: Boolean,
            default: false
        },
        hoverdisplaysubactions: {
            type: Boolean,
            default: false
        },
        hoverbackgroundcolor: {
            type: String,
            default: ''
        },
        tooltip: {
            type: String,
            default: ''
        },
        'security_question': {
            type: String,
            default: ''
        },
        issubaction: { /// nije za stvarno postavljanje spolja, automatski se postavlja pri generisanju podakcija
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        },
        'onclick_stoppropagation': {
            type: Boolean,
            default: true
        },
        subactiondata: {
            type: [Object, Array],
            default: function(){
                return {};
            }
        },
//        subbuttonsdata: {
//            type: Object,
//            default: function(){
//                return {};
//            }
//        },
        'subactions_position': {
            type: String,
            default: 'bottom'
        },
        additional_classes: {
            type: [Object, Array],
            default: function(){
                return {};
            }
        },
        icon_additional_classes: {
            type: Object,
            default: function(){
                return {};
            }
        },
        html: {
            type: String,
            default: ''
        },
        html_options: {
            type: Object,
            default: function(){
                return {};
            }
        },
        on_scroll_action: {
            type: String,
            default: 'HIDE'
        }
    },
    data: function () {
        return {
            local_disabled: this.disabled,
            local_timed_disabled: false,
            local_additional_classes: [],
            showSubactions: false,
            mounted: false,
            button_codes_disabled: [],
            on_scroll_interval: null
        };
    },
    computed: {
        subactionshorizontal: function(){
            var result = null;
            if(this.subactions_position === 'top' || this.subactions_position === 'bottom')
            {
                result = false;
            }
            else if(this.subactions_position === 'left' || this.subactions_position === 'right')
            {
                result = true;
            }
            else
            {
                var message = "subactions_position mora biti 'top' ili 'bottom' ili 'left' ili 'right'";
                console.error(message);
            }
            return result;
        },
        buttonClasses: function(){
            var result = {
                'sima-old-btn': true,
                _vue: true,
                _autodisabled: this.local_disabled || this.local_timed_disabled,
                _half: this.half,
                _no_border: this.noborder,
                _transparent_background: this.transparentbackground,
                _hover_background_e0e0e0: this.hoverbackgroundcolor === 'e0e0e0',
                _subaction: this.issubaction
            };
            
            for (var i in this.additional_classes) {
                result[this.additional_classes[i]] = true;
            }
            for (var i in this.local_additional_classes) {
                result[this.local_additional_classes[i]] = true;
            }
            
            return result;
        },
        iconClassParsed: function(){
            var result = this.icon;
            
//            var valid_icons = [
//                '_open-in-desktop-app', '_file-locked', '_file-unlocked',
//                '_edit', '_add', '_order-scan-add', '_order-scan-accepted', '_dialog',
//                '_refresh', '_recycle', '_delete', '_cancel', '_delete-forever',
//                '_restore-from-trash', '_qrcode', '_reset_default', '_not_ok',
//                '_copy', '_confirm-da-file-modifications', '_revert-da-file-modifications',
//                '_hamburger', '_zip', '_review', '_arrow-2-bottom', '_open-dd-manager',
//                '_main-settings', '_employees', 
//                '_end-arrow', '_right-arrow', '_left-arrow', '_start-arrow', '_eye',
//                '_close', '_dots', '_save', '_open-modified-file-in-da'
//            ];
            
//            if(valid_icons.includes(this.icon))
//            {
//            }
//            else 
            if(this.icon === 'edit' || this.icon === 'add' || this.icon === 'dots')
            {
                result = '_' + this.icon;
            }
//            else
//            {
//                var message = 'nepoznata ikonica: "'+this.icon+'"';
////                sima.addError('sima-button - iconClassParsed', message);
//                console.log(message);
//            }

            if (this.icon.indexOf(' ') > -1)
            {
                console.log('"'+this.icon+'" sadrzi space');
            }

            return result;
        },
        iconClasses: {
            cache: false,
            get: function(){
                var result = {
                    'sima-old-btn-icon': true,
                    'sima-icon': true,
                    '_16': this.iconsize === 16 || this.icon.includes('_16'),
                    '_18': this.iconsize === 18 || this.icon.includes('_18'),
                    '_24': this.iconsize === 24 || this.icon.includes('_24')
                };
                result[this.iconClassParsed] = true;
            
                for (var i in this.icon_additional_classes) {
                    result[this.icon_additional_classes[i]] = true;
                }
                    
                return result;
            }
        },
        haveAction: function(){
            return Object.keys(this.onclick).length > 0;
        },
        haveSubactions: function(){
            return Object.keys(this.subactiondata).length > 0;
//            return Object.keys(this.subbuttonsdata).length > 0;
        },
        showDropDownArrow: function(){
            return !this.hoverdisplaysubactions && this.haveSubactions && this.haveAction;
        }
    },
    methods: {
        onCloseSubactions: function(){
            this.toggleSubactionsDisplay(false);
            this.$emit('close-subactions');
        },
        disableButton: function(){
            this.local_disabled = true;
        },
        enableButton: function(){
            this.local_disabled = false;
        },
        disableSubActionsByCode: function(code){
            var _this = this;
            Object.keys(this.subactiondata).forEach(function(key,index) {
//            Object.keys(this.subbuttonsdata).forEach(function(key,index) {
                var subaction = _this.subactiondata[key];
//                var subaction = _this.subbuttonsdata[key];
                if(typeof subaction.htmlOptions !== 'undefined' && 'data-code' in subaction.htmlOptions && subaction.htmlOptions['data-code'] === code)
                {
                    subaction.disabled = true;
                }
            });
        },
        enableSubActionsByCode: function(code){
            var _this = this;
            Object.keys(this.subactiondata).forEach(function(key,index) {
//            Object.keys(this.subbuttonsdata).forEach(function(key,index) {
                var subaction = _this.subactiondata[key];
//                var subaction = _this.subbuttonsdata[key];
                if(typeof subaction.htmlOptions !== 'undefined' && 'data-code' in subaction.htmlOptions && subaction.htmlOptions['data-code'] === code)
                {
                    subaction.disabled = false;
                }
            });
        },
        onClick: function(event){
            if(this.onclick_stoppropagation === true)
            {
                event.stopPropagation();
            }
            if(this.haveAction)
            {
                this.local_timed_disabled = true;
                var _this = this;
                setTimeout(function() {
                    _this.local_timed_disabled = false;
                }, 1000);
                this.$emit('close-subactions');
                
                if(this.security_question.length > 0)
                {
                    var _this = this;
                    sima.dialog.openYesNo(this.security_question + '?', 
                        function(){
                            sima.dialog.close();
//                            sima.executeFunction(_this.action);
                            sima.executeFunction(_this.onclick);
                        }
                    );
                }
                else
                {
//                    sima.executeFunction(this.action);
                    sima.executeFunction(this.onclick);
                }
            }
            else if(this.haveSubactions)
            {
                this.toggleSubactionsDisplay();
            }
        },
        onDropDownArrowClick: function(){
            this.toggleSubactionsDisplay();
        },
        onMouseEnter: function(){
            if(this.hoverdisplaysubactions === true)
            {
                this.toggleSubactionsDisplay(true);
            }
        },
        onMouseLeave: function(){
            if(this.hoverdisplaysubactions === true)
            {
                this.toggleSubactionsDisplay(false);
            }
        },
        toggleSubactionsDisplay: function(forcing_display){
            if(forcing_display === true || (typeof forcing_display === 'undefined' && !this.showSubactions))
            {
                this.showSubactions = true;
                
                this.bindOnScroll();
            }
            else
            {
                this.showSubactions = false;
                
                this.unbindOnScroll();
            }
        },
        hasClass: function(class_name) {
            var has_class = false;
            
            $.each(this.buttonClasses, function(index, value) {
                if (index === class_name && value === true)
                {
                    has_class = true;
                    return false;
                }
            });
            
            return has_class;
        },
        addClass: function(class_name) {
            if (this.local_additional_classes.indexOf(class_name) === -1)
            {
                this.local_additional_classes.push(class_name);
            }
        },
        removeClass: function(class_name) {
            var index = this.local_additional_classes.indexOf(class_name);
            if (index !== -1)
            {
                this.local_additional_classes.splice(index, 1);
            }
        },
        bindOnScroll: function() /// ukradeno iz simapopup
        {
            var local_this = this;
            var this_el = $(this.$el);
            var on_scroll_interval =  this_el.onPositionChanged(function(){
                if(local_this.on_scroll_action === 'HIDE')
                {
                    local_this.toggleSubactionsDisplay(false);
                }
                else /// TO IMPLEMENT: MOVE
                {
                    
                }
            }, 10);
            local_this.on_scroll_interval = on_scroll_interval;
        },
        unbindOnScroll: function() /// ukradeno iz simapopup
        {
            clearInterval(this.on_scroll_interval);
        },
        onClickOutside: function(){
            this.onCloseSubactions();
        }
    },
    watch: {
        disabled: function(new_disabled, old_disabled){
            if(new_disabled === old_disabled)
            {
                return;
            }
            this.local_disabled = new_disabled;
        }
    },
    directives: {
        'click-outside': {
            bind: function(el, binding, vNode) {
                // Provided expression must evaluate to a function.
                if (typeof binding.value !== 'function') {
                    const compName = vNode.context.name;
                    let warn = `[Vue-click-outside:] provided expression '${binding.expression}' is not a function, but has to be`;
                    if (compName) { warn += `Found in component '${compName}'`; }

                    console.warn(warn);
                }
                // Define Handler and cache it on the element
                const bubble = binding.modifiers.bubble;
                const handler = (e) => {
                    if (bubble || (!el.contains(e.target) && el !== e.target)) {
                        binding.value(e);
                    }
                };
                el.__vueClickOutside__ = handler;

                // add Event Listeners
                document.addEventListener('click', handler);
            },
            unbind: function(el, binding) {
              // Remove Event Listeners
              document.removeEventListener('click', el.__vueClickOutside__);
              el.__vueClickOutside__ = null;

            }
          }
    },
    mounted: function(){
        var _this = this;
        
        this.$nextTick(function () {
            // Code that will run only after the
            // entire view has been rendered
            this.mounted = true;
            
            /// validacija
            if(
                    (typeof this.title === 'undefined' || this.title === '') 
                    && 
                    (typeof this.icon === 'undefined' || this.icon === '')
                    && 
                    (typeof this.html === 'undefined' || this.html === '')
            )
            {
                var message = "title ili icon ili html mora biti postavljen\n\
onclick: "+JSON.stringify(this.onclick)+"\n\
icon: "+JSON.stringify(this.icon)+"\n\
subactiondata: "+JSON.stringify(this.subactiondata)+"\n";
                console.error(message);
            }
            
            if(this.button_id !== '')
            {
                var button_jquery_obj = $('#'+this.button_id);
                if(typeof sima.layout !== 'undefined')
                {
                    sima.layout.allignFirstLayoutParent(button_jquery_obj,'TRIGGER_sima_yii_button_js');
                }
            }
        });
        
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    template: '#sima_yii_button_template'
});

