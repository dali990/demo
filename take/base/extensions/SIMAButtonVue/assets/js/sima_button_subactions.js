/* global Vue */

Vue.component('sima-yii-button-subactions', {
    props: {
        subactiondata: {
            type: [Object, Array],
            default: function(){
                return {};
            }
        },
        subactionshorizontal: {
            type: Boolean,
            default: false
        },
        show: {
            type: Boolean,
            default: false
        },
        'subactions_position': {
            type: String,
            default: 'bottom'
        }
    },
    data: function(){
        return {
            element_width: 0,
            element_height: 0,
            subactionsStyleLeft: 0,
            subactionsStyleTop: 0,
            subactionsStyleWidth: 'auto',
            subactionsStyleHeight: 'auto',
            parent_bounding_rect: null
        };
    },
    computed: {
        subactionsWrapperClasses: function(){
            return {
                'btn-dropdown-box': true,
                _vue: true,
                'sima-popup': true,
                '_main': true,
                '_horizontal': this.subactionshorizontal,
                '_vertical': !this.subactionshorizontal
            };
        },
        subactionsDisplayStyle: function(){
            var result = 'none';
            if(this.show === true)
            {
                result = 'block';
            }
            return result;
        },
        subactionsStyle: function(){
            return {
                display: this.subactionsDisplayStyle,
                top: this.subactionsStyleTop + 'px',
                left: this.subactionsStyleLeft + 'px',
                width: this.subactionsStyleWidth,
                height: this.subactionsStyleWidth
            };
        },
        relativeToElLeft: function(){
            return this.parent_bounding_rect.left;
        },
        relativeToElTop: function(){
            return this.parent_bounding_rect.top;
        },
        relativeToElWidth: function(){
            return this.parent_bounding_rect.width;
        },
        relativeToElHeight: function(){
            return this.parent_bounding_rect.height;
        },
        popupWidth: function(){
            return this.$el.offsetWidth;
        },
        popupHeight: function(){
            return this.$el.offsetHeight;
        },
        padding: function(){
            return this.element_width;
        }
    },
    methods: {
        onCloseSubactions: function(){
            this.$emit('close-subactions');
        },
        setLeftRightAlign: function(){
            var distance_from_left = this.relativeToElLeft + this.relativeToElWidth;
            var distance_from_right = $(window).width() - this.relativeToElLeft;
            
            //ako ima mesta popup ide desno
            if (distance_from_right >= this.popupWidth)
            {
                this.subactionsStyleLeft = this.relativeToElLeft;
            }
            //ako ima mesta popup ide levo
            else if (distance_from_left >= this.popupWidth)
            {
                this.subactionsStyleLeft = this.relativeToElLeft - this.popupWidth + this.relativeToElWidth;
            }
            //ako nema mesta ni desno ni levo ide gde ima vise mesta
            else if (distance_from_right >= distance_from_left)
            {
                this.subactionsStyleWidth = (distance_from_right - this.padding) +'px';
                this.subactionsStyleLeft = this.relativeToElLeft;
            }        
            else
            {
                this.subactionsStyleWidth = (distance_from_left - this.padding) +'px';    
                
                var new_left = this.relativeToElLeft - this.popupWidth + this.relativeToElWidth;
                
                if(new_left < 0)
                {
                    new_left = 0;
                }
                
                this.subactionsStyleLeft = new_left;       
            }
        },
        setTopBottomAlign: function(){
            var distance_from_top = this.relativeToElTop;
            var distance_from_bottom = $(window).height() - this.relativeToElTop - this.relativeToElHeight;

            //ako ima mesta popup ide dole
            if (distance_from_bottom >= this.popupHeight)
            {
                this.subactionsStyleTop = this.relativeToElTop;
            } 
            //ako ima mesta popup ide gore
            else if (distance_from_top >= this.popupHeight)
            {
                this.subactionsStyleTop = this.relativeToElTop + this.relativeToElHeight - this.popupHeight;
            }               
            //ako nema mesta ni dole ni gore ide gde ima vise mesta
            else if (distance_from_bottom >= distance_from_top)
            {
                this.subactionsStyleTop = this.relativeToElTop;
            }        
            else
            {
                this.subactionsStyleTop = this.relativeToElTop + this.relativeToElHeight - this.popupHeight;
            }
        },
        setBottomPosition: function(){
            var distance_from_top = this.relativeToElTop;
            var distance_from_bottom = $(window).height() - this.relativeToElTop - this.relativeToElHeight;

            //ako ima mesta popup ide dole
            if (distance_from_bottom >= this.popupHeight)
            {
                this.subactionsStyleTop = this.relativeToElTop + this.relativeToElHeight;
            }
            //ako ima mesta popup ide gore
            else if (distance_from_top >= this.popupHeight)
            {   
                this.subactionsStyleTop = this.relativeToElTop - this.popupHeight;
            }        
            //ako nema mesta ni dole ni gore ide gde ima vise mesta
            else if (distance_from_bottom >= distance_from_top)
            {
                this.subactionsStyleHeight = distance_from_bottom - this.padding;
                this.subactionsStyleTop = this.relativeToElTop + this.relativeToElHeight;
//                refreshSimaPopupScrollContainer(popup);
            }        
            else
            {
                this.subactionsStyleHeight = distance_from_top - this.padding;
                this.subactionsStyleTop = this.relativeToElTop - this.popupHeight;
//                refreshSimaPopupScrollContainer(popup);
            }
        },
        setTopPosition: function(){
            var distance_from_top = this.relativeToElTop;
            var distance_from_bottom = $(window).height() - this.relativeToElTop - this.relativeToElHeight;

            //ako ima mesta popup ide gore
            if (distance_from_top >= this.popupHeight)
            {
                this.subactionsStyleTop = this.relativeToElTop - this.popupHeight;
            }
            //ako ima mesta popup ide dole
            else if (distance_from_bottom >= this.popupHeight)
            {
                this.subactionsStyleTop = this.relativeToElTop + this.relativeToElHeight;
            }                
            //ako nema mesta ni dole ni gore ide gde ima vise mesta
            else if (distance_from_bottom >= distance_from_top)
            {
                this.subactionsStyleHeight = distance_from_bottom - this.padding;
                this.subactionsStyleTop = this.relativeToElTop + this.relativeToElHeight;
//                refreshSimaPopupScrollContainer(popup);
            }        
            else
            {
                this.subactionsStyleHeight = distance_from_top - this.padding;
                this.subactionsStyleTop = this.relativeToElTop - this.popupHeight;
//                refreshSimaPopupScrollContainer(popup);
            }
        },
        setRightPosition: function(){
            var distance_from_left = this.relativeToElLeft;
            var distance_from_right = $(window).width() - this.relativeToElLeft - this.relativeToElWidth;

            //ako ima mesta popup ide desno
            if (distance_from_right >= this.popupWidth)
            {
                this.subactionsStyleLeft = this.relativeToElLeft + this.relativeToElWidth;
            }
            //ako ima mesta popup ide levo
            else if (distance_from_left >= this.popupWidth)
            {
                this.subactionsStyleLeft = this.relativeToElLeft - this.popupWidth;
            }
            //ako nema mesta ni desno ni levo ide gde ima vise mesta
            else if (distance_from_right >= distance_from_left)
            {
                this.subactionsStyleWidth = (distance_from_right - this.padding) +'px';
                this.subactionsStyleLeft = this.relativeToElLeft + this.relativeToElWidth;           
            }        
            else
            {
                this.subactionsStyleWidth = (distance_from_left - this.padding) +'px';
                this.subactionsStyleLeft = this.relativeToElLeft + this.popupWidth;            
            }
        },
        setLeftPosition: function(){
            var distance_from_left = this.relativeToElLeft;
            var distance_from_right = $(window).width() - this.relativeToElLeft - this.relativeToElWidth;

            //ako ima mesta popup ide levo
            if (distance_from_left >= this.popupWidth)
            {            
                this.subactionsStyleLeft = this.relativeToElLeft - this.popupWidth;
            }
            //ako ima mesta popup ide desno
            else if (distance_from_right >= this.popupWidth)
            {
                this.subactionsStyleLeft = this.relativeToElLeft + this.relativeToElWidth;
            }        
            //ako nema mesta ni desno ni levo ide gde ima vise mesta
            else if (distance_from_right >= distance_from_left)
            {                
                var _padding = this.padding;
                this.subactionsStyleWidth = distance_from_right - _padding;
                this.subactionsStyleLeft = this.relativeToElLeft + this.relativeToElWidth;
            }        
            else
            {                
                var _padding = this.padding;
                this.subactionsStyleWidth = distance_from_left - _padding;
                this.subactionsStyleLeft = this.relativeToElLeft + this.popupWidth;
            }
        },
        recalculateSubactionsStyleLeftTop: function(){
            if(this.$parent.mounted === false)
            {
                return;
            }
            
            if (this.subactions_position === 'bottom')
            {
                this.setLeftRightAlign();
                this.setBottomPosition();
            }
            else if (this.subactions_position === 'top')
            {
                this.setLeftRightAlign();
                this.setTopPosition();
            }
            else if (this.subactions_position === 'right')
            {
                this.subactionsStyleTop = this.relativeToElTop;
                this.setRightPosition();
            }
            else if (this.subactions_position === 'left')
            {
                this.setTopBottomAlign();
                this.setLeftPosition();
            }
        }
    },
    watch: {
        show: function(new_show, old_show){
            if(new_show === old_show)
            {
                return;
            }
            if(new_show === true)
            {
                this.$nextTick(function () {
                    // Code that will run only after the
                    // entire view has been re-rendered
                    if(this.element_width !== this.$el.clientWidth)
                    {
                        this.element_width = this.$el.clientWidth;
                    }
                    if(this.element_height !== this.$el.clientHeight)
                    {
                        this.element_height = this.$el.clientHeight;
                    }
                    
                    var new_parent_bounding_rect = this.$parent.$el.getBoundingClientRect();
                    if(this.parent_bounding_rect === null || (
                            this.parent_bounding_rect.x !== new_parent_bounding_rect.x
                            || this.parent_bounding_rect.y !== new_parent_bounding_rect.y
                            || this.parent_bounding_rect.width !== new_parent_bounding_rect.width
                            || this.parent_bounding_rect.height !== new_parent_bounding_rect.height
                            || this.parent_bounding_rect.top !== new_parent_bounding_rect.top
                            || this.parent_bounding_rect.right !== new_parent_bounding_rect.right
                            || this.parent_bounding_rect.bottom !== new_parent_bounding_rect.bottom
                            || this.parent_bounding_rect.left !== new_parent_bounding_rect.left
                        ))
                    {
                        this.parent_bounding_rect = new_parent_bounding_rect;
                        this.recalculateSubactionsStyleLeftTop();
                    }
                });
            }
            else
            {
                this.$children.forEach(function(child)
                {
                    child.toggleSubactionsDisplay(false);
                });
            }
        }
    },
    template: '#sima_button_subactions_template'
});

