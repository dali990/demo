<script type="text/x-template" id="sima_button_subactions_template">
    <div v-bind:class="subactionsWrapperClasses" v-bind:style="subactionsStyle">
        <sima-yii-button 
            v-for="subaction in subactiondata"
            v-bind:key="JSON.stringify(subaction.title) + JSON.stringify(subaction.tooltip) + JSON.stringify(subaction.icon) + JSON.stringify(subaction.html)"
            v-bind:title="subaction.title"
            v-bind:icon="subaction.icon"
            v-bind:iconsize="subaction.iconsize"
            v-bind:half="subaction.half"
            v-bind:issubaction=true
            v-bind:onclick="subaction.onclick"
            v-bind:subactiondata="subaction.subactions_data"
            v-bind:tooltip="subaction.tooltip"
            v-bind:subactionshorizontal="false"
            v-bind:subactions_position="subaction.subactions_position"
            v-bind:html_options="subaction.htmlOptions"
            v-bind:disabled="subaction.disabled"
            v-bind:html="subaction.html"
            @close-subactions="onCloseSubactions"
        ></sima-yii-button>
    </div>
</script>