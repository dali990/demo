<script type="text/x-template" id="sima_yii_button_template">
    <div v-if="html.length==0" 
            v-bind:class='buttonClasses' 
            v-on:click="onClick" v-on:mouseenter="onMouseEnter" 
            v-on:mouseleave.stop.prevent="onMouseLeave" 
            v-click-outside="onClickOutside"
            v-bind:title="tooltip" 
            v-bind="html_options" 
            ref="simabutton">
        <span class='sima-old-btn-title' v-if="title.length > 0" v-bind:title="title" v-html="title"></span>
        <div v-bind:class="iconClasses" v-if="icon.length > 0"></div>
        <div class="sima-old-btn _half _dropdown _black _18" v-if="showDropDownArrow" v-on:click.stop="onDropDownArrowClick"></div>
        <sima-yii-button-subactions
            v-if="haveSubactions"
            v-bind:show="showSubactions"
            v-bind:subactiondata="subactiondata"
            v-bind:subactionshorizontal="subactionshorizontal"
            v-bind:subactions_position="subactions_position"
            @close-subactions="onCloseSubactions"
        >
        </sima-yii-button-subactions>
    </div>
    <div v-else v-html="html" v-bind:class='buttonClasses' v-on:click.stop="onClick" v-on:mouseenter="onMouseEnter" v-on:mouseleave.stop.prevent="onMouseLeave" v-bind:title="tooltip" v-bind="html_options" ref="simabutton">
    </div>
</script>
