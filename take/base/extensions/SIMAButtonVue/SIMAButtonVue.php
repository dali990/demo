<?php

class SIMAButtonVue extends CWidget 
{
    public $id = null;
    public $icon = '';    
    public $iconsize = 16;    
    public $title = '';
    public $onclick = [];
    public $subactions_data = [];
    public $subactions_position = 'bottom';
    public $half = false;
    public $hover_display_subactions = false;
    public $transparent_background = false;
    public $no_hover_border = false;
    public $no_border = false;
    public $onclick_stoppropagation = true;
    public $hover_background_color = null;
    public $security_question = '';
    public $tooltip = '';
    public $additional_classes = [];
    public $icon_additional_classes = [];
    public $disabled = false;
    public $html_options = [];
    
    public function run()
    {
        if(empty($this->id))
        {
            $this->id = SIMAHtml::uniqid();
        }
        
        if(!in_array($this->subactions_position, ['bottom', 'top', 'left', 'right']))
        {
            throw new Exception('subactions_position invalid');
        }
                
        echo $this->render('index', [
            'half' => $this->half
        ]);
        $js_data = [
            'id' => $this->id,
            'title' => $this->title,
            'onclick' => $this->onclick,
            'icon' => $this->icon,
            'icon_size' => $this->iconsize,
            'half' => $this->half,
            'no_border' => $this->no_border,
            'transparent_background' => $this->transparent_background,
            'no_hover_border' => $this->no_hover_border,
            'hover_background_color' => $this->hover_background_color,
            'hover_display_subactions' => $this->hover_display_subactions,
            'subactions_position' => $this->subactions_position,
            'subaction_data' => $this->subactions_data,
            'security_question' => $this->security_question,
            'tooltip' => $this->tooltip,
            'additional_classes' => $this->additional_classes,
            'icon_additional_classes' => $this->icon_additional_classes,
            'onclick_stoppropagation' => $this->onclick_stoppropagation,
            'disabled' => $this->disabled,
            'html_options' => $this->html_options,
        ];
        $js_data_encoded = SIMAMisc::multiDimensionalImplode(",", $js_data, '{', '}', true, '"', ['"', "\n", "\r"]);
        $js_script = "if(!sima.isEmpty($('#{$this->id}')))
        {
            new Vue({
                el:'#{$this->id}',
                data: $js_data_encoded
            });
        }";
        
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $js_script, CClientScript::POS_END);
    }
    
    public static function registerManual() {
        $component_dir = dirname(__FILE__);
        $assets = $component_dir.'/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/sima_button.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/sima_button_subactions.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), include($component_dir.'/templates/sima_button.php'), CClientScript::POS_END);
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), include($component_dir.'/templates/sima_button_subactions.php'), CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/sima_button.css');
    }
}
