<?php

class SIMAUpload extends CWidget 
{
    public static $STATUS_SUCCESS = 'Success';
    public static $STATUS_PROGRESS = 'Progress';
    public static $STATUS_STOPPED = 'Stopped';
    
    public $id = null;
    public $repmanager_upload = true;
    public $initial_file_display = null;
    public $form_id = null;
    public $form_input_data_id = 'File_file_version_id';
    public $allowed_extansions=null;
    public $additional_data=null;
    public $additional_options = [];
    
    public function run() 
    {
        if ($this->id == '')
        {
            $this->id = SIMAHtml::uniqid();
        }
        
        $view_params = [
            'id' => $this->id
        ];
        
        if(empty($this->form_id))
        {
            throw new Exception('form_id empty');
        }
        
        if(!empty($this->initial_file_display))
        {
            $view_params['initial_file_display'] = $this->initial_file_display;
        }
        
        if(Yii::app()->repManager->use_new_repmanager !== true)
        {
            $this->repmanager_upload = false;
        }
        
        echo $this->render('index', $view_params);
                      
        $params = [
            'form_id' => $this->form_id,
            'form_input_data_id' => $this->form_input_data_id,
            'repmanager_upload' => $this->repmanager_upload,
            'allowed_extansions'=>$this->allowed_extansions,
            'additional_data' => $this->additional_data,
            'additional_options' => $this->additional_options
        ];
        $json = CJSON::encode($params);
        $register_script = "$('#" . $this->id . "').simaupload('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
    
    public function registerManual() 
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/cropper.css');
        Yii::app()->clientScript->registerScriptFile($baseUrl .'/js/jquery.simaupload.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($baseUrl .'/js/simaUpload.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($baseUrl .'/js/cropper.js' , CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaupload.css');
        
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), "sima.upload = new SIMAUpload();", CClientScript::POS_READY);
    }
    
    /**
     * funkcija parsira uploadkey dobijen nakon save-a forme sa simaupload widget-om
     * ukoliko nije adekvatnog oblika (nije to taj key) vraca false
     * inace vraca tempfilename
     * 
     * @param type $upload_key
     * @param type $only_statuses
     * @param type $user_readable - da li da greska bude za korisnika (warn) ili sistemska (exception)
     * @return type
     *      - bool false u slucaju da key nije simaupload
     *      - string temp_file_name
     * @throws SIMAExceptionSIMAUploadNotRequestedStatus
     */
    public static function ParseUploadKey($upload_key, $only_statuses=[], $user_readable=false)
    {
        $result = false;
        
        if (strpos($upload_key, 'SIMAUpload')!==FALSE)
        {            
            $decoded_key = CJSON::decode($upload_key);
            $status = $decoded_key['status'];
            
            if(!is_array($only_statuses))
            {
                $only_statuses = [$only_statuses];
            }
            
            if(empty($only_statuses))
            {
                $only_statuses = [SIMAUpload::$STATUS_SUCCESS];
            }
            
            if(!in_array($status, $only_statuses))
            {
                if($user_readable === true)
                {
                    throw new SIMAWarnException(Yii::t('FilesModule.File', 'CanNotSaveFileUploadInProgress'));
                }
                else
                {
                    throw new SIMAExceptionSIMAUploadNotRequestedStatus($status, $only_statuses);
                }
            }
            
            if($status === SIMAUpload::$STATUS_SUCCESS)
            {
                $temp_file_name = $decoded_key['temp_file_name'];
                $file_display_name = $decoded_key['file_display_name'];
                
                $additional_data = [];
                if(isset($decoded_key['additional_data']))
                {
                    $additional_data = $decoded_key['additional_data'];
                }

                $result = [
                    'temp_file_name' => $temp_file_name,
                    'file_display_name' => $file_display_name,
                    'additional_data' => $additional_data
                ];
                
//                if(Yii::app()->useNewRepmanager())
                if(Yii::app()->repManager->use_new_repmanager === true && isset($decoded_key['rep_manager_id']))
                {
                    $result['rep_manager_id'] = $decoded_key['rep_manager_id'];
                }
            }
        }
        
        return $result;
    }
}