<?php

return [
    'ReceivedStatusNotEqualRequestedStatus' => 'Status of uploaded file not like requested:'
        .'</br>{receivedStatus} <> {requestedStatus}',
    'drag' => 'Drag here',
    'here' => ' ',
    'FileDontHaveExtension' => 'File upload can not be finished, file does not have right extension {file_name}',
    'InvalidFileExtension' => 'File upload can not be done, extension isnt right ! "{file_name}" ',
];
