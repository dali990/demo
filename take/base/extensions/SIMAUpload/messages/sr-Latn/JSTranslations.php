<?php

return [
    'CannotUploadDirOrEmptyFile' => 'Ne moze se izvrsiti upload jer je izabran direktorijum ili prazan fajl',
    'FileDontHaveExtension' => 'Ne moze se izvrsiti upload, fajl nema extenziju! "{file_name}"',
    'InvalidFileExtension' => 'Ne moze se izvrsiti upload, izabrani fajl "{file_name}" nema odgovarajucu extenziju!'
                                . '</br>Dozvoljene ekstenzije: {allowed_extendions}',
    'InvalidImageDimensionsNotEqual' => 'Ne moze se izvrsiti upload, izabranom fajlu "{file_name}" visina i sirina moraju biti jednaki!'
                                . '</br>{width}px =/= {height}px',
    'SIMAUploadPerformingInitialRequest' => 'U toku je provera slobodnog prostora'
];