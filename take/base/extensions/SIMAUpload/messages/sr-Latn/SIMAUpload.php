<?php

return [
    'ReceivedStatusNotEqualRequestedStatus' => 'Status upload-ovanog fajla nije kao zahtevani:'
        .'</br>{receivedStatus} <> {requestedStatus}',
    'Success' => 'Uspesno',
    'Progress' => 'U toku',
    'drag' => 'Ovde',
    'here' => 'prevucite',
];
