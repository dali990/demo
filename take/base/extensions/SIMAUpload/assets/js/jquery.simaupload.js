
/* global SparkMD5, sima */

(function($){
    var methods = {
        init: function(params){
            var localThis = $(this);

            localThis.data('form_id', params.form_id);
            localThis.data('form_input_data_id', params.form_input_data_id);
            localThis.data('repmanager_upload', params.repmanager_upload);
            localThis.data('additional_data', params.additional_data);
            localThis.data('additional_options', params.additional_options);
            localThis.data('transfer_id', null);
            localThis.data('stopped', false);
            
            var allowed_extensions=sima.clone(params.allowed_extansions);
            if (allowed_extensions !== null) {
                for (var i=allowed_extensions.length; i--;) {
                    allowed_extensions[i]="."+allowed_extensions[i];
                }
                var ext = allowed_extensions.toString();
                $('.sima-upload-input').attr('accept', ext);
            }
            localThis.data('extensions',allowed_extensions);
            var allowed_extensions_html = '<ul>';
            $.each(allowed_extensions, function(index, value){
                allowed_extensions_html += '<li>'+value+'</li>';
            });
            allowed_extensions_html += '</ul>';
            localThis.data('allowed_extensions_html', allowed_extensions_html);
              
            localThis.on('change', '.sima-upload-input', {localThis: localThis}, onFileUploadChange);
            localThis.on('drop', {localThis: localThis}, onFileDrop);
            localThis.on('click', '.sima-upload-stop', function () {
                $(this).hide();
                localThis.data('stopped', true);
                var transfer_id = localThis.data('transfer_id');
                if(transfer_id !== null)
                {
                    sima.transfers.stopUpload(transfer_id);
                }
                var progress_span = localThis.find('.sima-upload-progress');
                progress_span.text("Stopirano");
//                $(localThis).trigger("UPLOAD_STOPPED", [{transfer_id:transfer_id}]);
                setInputValue(localThis, {
                    status: "Stopped",
                    transfer_id: transfer_id
                });
            });
            localThis.on('remove', function(){
                var transfer_id = localThis.data('transfer_id');
                if(transfer_id !== null)
                {
                    sima.transfers.stopUpload(transfer_id);
                }
            });
            localThis.find('.sima-upload-stop').hide();

            $(sima.transfers).on('UPLOAD_STARTED', {localThis:localThis}, onUploadStarted);
            $(sima.transfers).on('UPLOAD_SUCCESS', {localThis:localThis}, onUploadSuccess);
            $(sima.transfers).on('UPLOAD_PROGRESS', {localThis:localThis}, onUploadProgress);
            $(sima.transfers).on('UPLOAD_ERROR', {localThis:localThis}, onUploadError);
            $(sima.transfers).on('PERFORMING_INITIAL_REQUEST', {localThis:localThis}, onPerformingInitialRequest);
        },
        delete: function(drag_str, here_str)
        {
            var localThis = $(this);
            
            var form_input_data_id = localThis.data('form_input_data_id');
            var form_input_data = $('#'+form_input_data_id);
            form_input_data.val('delete');
            
            localThis.find('.sima-upload-file-info').text(drag_str);
            localThis.find('.sima-upload-progress').text(here_str);
        }
    };

    function onUploadStarted(event, event_message)
    {
        var localThis = event.data.localThis;
        var transfer_id = localThis.data('transfer_id');

        if(transfer_id === event_message.transfer_id)
        {
            var progress_span = localThis.find('.sima-upload-progress');
            progress_span.text("Zapoceto");

            var upload_stop = localThis.find('.sima-upload-stop');
            upload_stop.show();

//            $(localThis).trigger("UPLOAD_STARTED", [{transfer_id: transfer_id}]);
            setInputValue(localThis, {
                status: "Started",
                transfer_id: transfer_id
            });
        }
    }

    function onUploadSuccess(event, event_message)
    {
        var localThis = event.data.localThis;
        var transfer_id = localThis.data('transfer_id');

        if(transfer_id === event_message.transfer_id)
        {
            var progress_span = localThis.find('.sima-upload-progress');
            progress_span.text("Uspesno zavrseno");

            var upload_stop = localThis.find('.sima-upload-stop');
            upload_stop.hide();

//            $(localThis).trigger("UPLOAD_SUCCESS", [event_message]);
            var params = {
                status: "Success",
                temp_file_name: event_message.temp_file_name,
                file_display_name: event_message.file_display_name,
                transfer_id: event_message.transfer_id
            };
            if(sima.getUseNewRepmanager())
            {
                params.rep_manager_id = event_message.rep_manager_id;
            }

            setInputValue(localThis, params);
        }
    }

    function onUploadError(event, event_message)
    {
        var localThis = event.data.localThis;
        var transfer_id = localThis.data('transfer_id');

        if(transfer_id === event_message.transfer_id)
        {
            var progress_span = localThis.find('.sima-upload-progress');
            progress_span.text("Doslo je do greske");

            var upload_stop = localThis.find('.sima-upload-stop');
            upload_stop.hide();

//            $(localThis).trigger("UPLOAD_ERROR", [{transfer_id: transfer_id}]);
            setInputValue(localThis, {
                status: "Error",
                transfer_id: transfer_id
            });
        }
    }

    function onUploadProgress(event, event_message)
    {
        var localThis = event.data.localThis;
        var transfer_id = localThis.data('transfer_id');

        if(transfer_id === event_message.transfer_id)
        {
            if(localThis.data('stopped') === false)
            {
                var progress_span = localThis.find('.sima-upload-progress');

                var percent_progress = event_message.percent_progress;
                var transfer_speed = event_message.transfer_speed;
                var time_left = event_message.time_left;

                transfer_speed = sima.misc.formatSize(transfer_speed)+"/s";
                time_left = sima.misc.formatTime(time_left);

                progress_span.text(percent_progress+"% - "+transfer_speed+" - "+time_left);

                setInputValue(localThis, {
                    status: "Progress",
                    transfer_id: event_message.transfer_id
                });
            }
        }
    }
    
    function onPerformingInitialRequest(event, event_message)
    {
        var localThis = event.data.localThis;
        var transfer_id = localThis.data('transfer_id');
        if(transfer_id === event_message.transfer_id)
        {
            var progress_span = localThis.find('.sima-upload-progress');
            progress_span.text(sima.translate('SIMAUploadPerformingInitialRequest'));
        }
    }

    function onFileDrop(event)
    {
        event.stopPropagation();
        event.preventDefault();

        var localThis = event.data.localThis;

        var file = event.originalEvent.dataTransfer.files[0];
        /// is file
        var is_file = true;
        if (event.originalEvent.dataTransfer.files.length === 0) { /// ff
            is_file = false;
        }
        else if (event.originalEvent.dataTransfer.items) { /// if chrome
            if (typeof (event.originalEvent.dataTransfer.items[0].webkitGetAsEntry) === "function") {
                is_file = event.originalEvent.dataTransfer.items[0].webkitGetAsEntry().isFile;
            } else if (typeof (event.originalEvent.dataTransfer.items[0].getAsEntry) === "function") {
                is_file = event.originalEvent.dataTransfer.items[0].getAsEntry().isFile;
            }
        }

        if(is_file === true)
        {
            cropOrParse(localThis, file);
        }
        else
        {
            alert(sima.translate('CannotUploadDirOrEmptyFile'));
        }
    }

    function cropOrParse(localThis, file)
    {
        var is_crop = false;
        if(localThis.data('additional_options')['use_crop'] !== 'undefine' && localThis.data('additional_options')['use_crop'] === true)
        {
            is_crop = true;
            cropPopUp(localThis, file);
        }
        if(!is_crop)
        {
            ParseFile(localThis, file);
        }
    }
    
    //dodato za Cropovanje fotke
    function cropPopUp(localThis, file)
    {
        validate_uplad_file(localThis, file, function(localThis, file, condition_exeptions_message)
        {
            if(condition_exeptions_message!==true){
                sima.dialog.openWarn(condition_exeptions_message);
            } else {
                var reader = new FileReader();
              // inject an image with the src url

                reader.onload = function(event) {
                    var the_url = event.target.result;
                    var yes_func_params = {};
                    yes_func_params.title = sima.translate('Save');
                    var uniq = sima.uniqid();
                    yes_func_params.func = function(obj){
                        var cropDiv = document.getElementById("crop-" + uniq);
                        var button = cropDiv.getElementsByTagName('button')[0];
                        button.click();
                        var ImageURL = $('#cropped-image-'+uniq).attr('src');
                        $('#cropped-image-'+uniq).removeAttr('src');
                        var block = ImageURL.split(';');
                        var contentType = block[0].split(":")[1];
                        var realData = block[1].split(",")[1];
                        var blob = b64toBlob(realData, contentType);
                        //u MS Edge browseru ne moze da se instancira novi fajl(new File), 
                        //pa je instanciran Blob kome se dodaje atribute name
                        try{
                            var f = new File([blob], file.name);
                        } 
                        catch(e)
                        {
                            var f = new Blob([blob], { type: contentType });
                            f.name = file.name;
                        }
                        sima.dialog.closeYesNo(); 

                        /// nakon crop-a slika mora biti cetvrtasta
                        /// ako nije vec tako postavljeno
                        var current_data = localThis.data('additional_options');
                        var image_dimensions_equal_was_true = true;
                        if(current_data['image_dimensions_equal'] !== true)
                        {
                            image_dimensions_equal_was_true = false;
                            current_data['image_dimensions_equal'] = true;
                            localThis.data('additional_options', current_data);
                        }
                        
                        ParseFile(localThis, f);
                        
                        /// nakon parsiranja crop-a
                        /// ako smo mi eksplicitno postavili dinemsions equal
                        /// vracamo na false
                        if(image_dimensions_equal_was_true === false)
                        {
                            current_data['image_dimensions_equal'] = false;
                            localThis.data('additional_options', current_data);
                        }

                };
                var no_func_params =  {};
                no_func_params.hidden =  true;
                var html = '<div class="img-crop-custom sima-layout-panel" id="crop-' + uniq + '"> <img src="' + the_url + '" alt="User Image"><button type="button" >Crop</button><div></div></div><script> simaCropperCropImage('+ uniq +');</script>';
                sima.dialog.openYesNo(html, yes_func_params, no_func_params, null, true, true);
              };
              reader.readAsDataURL(file);
                
            }
            
        });
    }

    function onFileUploadChange(event)
    { 
        var localThis = event.data.localThis;
        var input = localThis.find('.sima-upload-input');
        var file = input.prop('files')[0]; 
        cropOrParse(localThis, file);
    }
    
    //dodato za croppovanje fotki
    function b64toBlob(b64Data, contentType, sliceSize) 
    {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    }

    function ParseFile(localThis, file)
    {
        validate_uplad_file(localThis,file, function(localThis,file, condition_exeptions_message)
        {
            if(condition_exeptions_message!==true){
                    sima.dialog.openWarn(condition_exeptions_message);
            }
            else
            {
                try
                {
                    start_upload(localThis,file);
                }
                catch(NS_ERROR_FILE_ACCESS_DENIED)
                {
                    alert('Ne moze se izvrsiti upload jer je izabran direktorijum\n"'+file.name+'"');
                }
            }
        });
    }
    
    function validate_uplad_file(localThis,file, on_success_function)
    {
        var to_run_on_succes_at_end = true;
        var condition = true;
        
        if (typeof file === 'undefined' )
        {
            condition = sima.translate('CannotUploadDirOrEmptyFile');
        }
        else if(file.size <= 1)
        {
            condition = sima.translate('CannotUploadDirOrEmptyFile')+'\n"'+file.name+'"';
        }
        else if(!validate_extensions(localThis,file))
        {   
            if( ((file.name).split(".")).length === 1)
            {
                condition= sima.translate('FileDontHaveExtension',{
                    '{file_name}':file.name
                });
            }
            else
            {
                condition = sima.translate('InvalidFileExtension',{
                    '{file_name}':file.name,
                    '{allowed_extendions}': localThis.data('allowed_extensions_html')
                });
            } 
        }
        else
        {
            $.each(localThis.data('additional_options'), function(key, value){
                if(key === 'image_dimensions_equal' && value === true)
                {
                    to_run_on_succes_at_end = false;
                    var img = new Image();
                    img.onload = function(){
                        if(img.width !== img.height)
                        {
                            condition = sima.translate('InvalidImageDimensionsNotEqual',{
                                '{file_name}':file.name,
                                '{width}': img.width,
                                '{height}': img.height
                            });
                        }
                        on_success_function(localThis, file, condition);
                    };
                    img.src = window.URL.createObjectURL(file);
                }
            });
        }
                
        if(to_run_on_succes_at_end === true)
        {
            on_success_function(localThis, file, condition);
        }
    }
    
    function validate_extensions(localThis,file){
        
        if(!sima.isEmpty(localThis.data('extensions')))
        {       
            var file_name=file.name;
            var is_extensions_true=false;
            var array_extensions=localThis.data('extensions');
            var length_of_extension;
            var length_of_file_name=file_name.length;
            var extensions_from_input;
           
            for(var i=0; i<array_extensions.length; i++)
            {
                const array_extension = array_extensions[i];
                length_of_extension=array_extensions[i].length;
                extensions_from_input=file_name.substring(length_of_file_name-length_of_extension,length_of_file_name);
                if( extensions_from_input.toLowerCase() === array_extension.toLowerCase() ){
                    is_extensions_true=true;
                    break;
                }
                  
            }  
             return is_extensions_true;
        }
        else{
            return true;
        }
    }

    function start_upload(localThis,file){
        setInputValue(localThis, {
            status: "Initializing"
        });
        
        var r = new FileReader();
        var blob = file.slice(0, 1);
        r.readAsBinaryString(blob);

        var file_info = localThis.find('.sima-upload-file-info');

        var old_transfer_id = localThis.data('transfer_id');
        if(old_transfer_id !== null)
        {
            sima.transfers.stopUpload(old_transfer_id);
        }

        localThis.data('stopped', false);

        var transfer_id = sima.transfers.initUpload(file, localThis.data('repmanager_upload'));
        localThis.data('transfer_id', transfer_id);
        sima.transfers.startUpload(transfer_id);

        var file_name = file.name;
        var file_size = file.size;

        if(file_name.length > 20)
        {
            file_name = file_name.substring(0, 20)+"...";
        }
        file_size = sima.misc.formatSize(file_size);

        file_info.text(file_name + " " + file_size);
    }
    function setInputValue(localThis, value)
    {
        value.key = "SIMAUpload";
        value.additional_data = localThis.data('additional_data');
        
        var form_id = localThis.data('form_id');
        var form_input_data_id = localThis.data('form_input_data_id');
        var form = $('#'+form_id);
        var form_input_data = form.find('#'+form_input_data_id);
        
        form_input_data.val(JSON.stringify(value));
    }
    
    $.fn.simaupload = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.vmenu');
            }
        });
        return ret;
    };
})( jQuery );

