/* global sima */

function SIMAUpload()
{
    this.form = function(success_function)
    {
        var params = {
            status: 'validate',
            onSave: function(response){
                sima.callFunction(success_function, response.form_data.File.attributes.file_version_id);
            },
            formName: 'justupload'
        };
        sima.model.form('File', '', params);
    };
}

