<?php
class SIMAExceptionSIMAUploadNotRequestedStatus extends SIMAException
{
    public function __construct($receivedStatus, $requestedStatuses)
    {
        $requestedStatusesParsed = '';
        foreach($requestedStatuses as $requestedStatus)
        {
            if(!empty($requestedStatusesParsed))
            {
                $requestedStatusesParsed .= ', ';
            }
            
            $requestedStatusesParsed .= Yii::t('SIMAUpload.SIMAUpload', $requestedStatus);
        }
        
        $err_msg = Yii::t('SIMAUpload.SIMAUpload', 'ReceivedStatusNotEqualRequestedStatus', [
            '{receivedStatus}' => Yii::t('SIMAUpload.SIMAUpload', $receivedStatus),
            '{requestedStatus}' =>$requestedStatusesParsed,
        ]);
        parent::__construct($err_msg);
    }
}

