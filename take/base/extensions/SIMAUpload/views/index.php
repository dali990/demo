<?php
$drag = Yii::t('SIMAUpload.SIMAUpload','drag');
$here = Yii::t('SIMAUpload.SIMAUpload','here');
?>
<div id='<?php echo $id; ?>' class='sima-upload-wrapper'>
    <div class="sima-upload-input-container sima-icon _upload _24">
        <input class='sima-upload-input' type="file" name="myFiles">
    </div>
    <span class='sima-upload-file-info'>
        <?php 
        if(isset($initial_file_display))
        {
            echo '<div class="sima-upload-file-display">'.$initial_file_display.'</div>';
            
            if(SIMAMisc::isVueComponentEnabled())
            {
                echo $this->widget(SIMAButtonVue::class, [
                    'icon' => '_recycle',
                    'iconsize' => 16,
                    'onclick'=>['sima.callPluginMethod', '#'.$id, 'simaupload', 'delete', "$drag", "$here"]
                ], true);
            }
            else
            {
                echo $this->widget('SIMAButton', [
                    'action' => [
                        'icon'=>'sima-icon _recycle _16',
                        'onclick'=>['sima.callPluginMethod', '#'.$id, 'simaupload', 'delete', "$drag", "$here"]
                    ]
                ], true);
            }
        }
        else
        {
            echo $drag;
        }
        ?>
    </span>
    <span class='sima-upload-progress'>
        <?php 
        if(!isset($initial_file_display))
        {
            echo $here;
        }
        ?>
    </span>
    <span class='sima-ui-button sima-upload-stop'><?php echo Yii::t('BaseModule.Common', 'Stop'); ?></span>
</div>

