
<div id="<?php echo $this->id; ?>" class="sima-diff-viewer <?php echo $this->class; ?>">
    <div class="sima-diff-viewer-toggle-groups">
        <?php
        $toggle_button_id = SIMAHtml::uniqid().'_tb';
        if(SIMAMisc::isVueComponentEnabled())
        {
            echo Yii::app()->controller->widget(SIMAButtonVue::class,[
                'id' => $toggle_button_id,
                'title' => Yii::t('SIMADiffViewer.Translation', 'ToggleGroupsWithOnlySameItems'),
                'tooltip' => Yii::t('SIMADiffViewer.Translation', 'ToggleGroupsWithOnlySameItems'),
                'onclick' => ['sima.diffViewer.toogleGroupsWithOnlySameItems', "$toggle_button_id"]
            ], true);
        }
        else
        {
            echo Yii::app()->controller->widget('SIMAButton',[
                'id' => $toggle_button_id,
                'action'=>[
                    'title'=>Yii::t('SIMADiffViewer.Translation', 'ToggleGroupsWithOnlySameItems'),
                    'tooltip'=>Yii::t('SIMADiffViewer.Translation', 'ToggleGroupsWithOnlySameItems'),
                    'onclick'=>['sima.diffViewer.toogleGroupsWithOnlySameItems', "$toggle_button_id"]
                ]
            ], true);
        }
        ?>
    </div>
    <?php
        foreach ($this->diffs as $diff)
        {
            if (!empty($diff['type']) && $diff['type'] === 'group')
            {
                $columns_cnt = count($diff['columns']);
                $fixed_width = 105 + (($columns_cnt + 2) * (4 + 6)); //105 je zbir prve dve kolone, i dodamo za svaku kolonu 4px za padding levo i desno(po 2) i jos dodatnih 6px
                $column_width = "calc((100% - {$fixed_width}px) / $columns_cnt)";
                $is_group_diff_confirmed = isset($diff['diff_confirmed']) && (SIMAMisc::filter_bool_var($diff['diff_confirmed']) === true);
                $group_diff_confirmed_class = ($is_group_diff_confirmed) ? '_diff_confirmed' : '';
                ?>
                    <div class="sima-diff-viewer-group <?=$group_diff_confirmed_class?>" data-group_id="<?=$diff['group_id']?>">
                        <div class="sima-diff-viewer-group-title">
                            <?php
                                echo $diff['group_name'];
                            ?>
                        </div>
                        <div class="sima-diff-viewer-group-toggle-same-items">
                            <?php
                            $same_items_in_group_toggle_button_id = SIMAHtml::uniqid().'_siigtb';
                            if(SIMAMisc::isVueComponentEnabled())
                            {
                                echo Yii::app()->controller->widget(SIMAButtonVue::class,[
                                    'id' => $same_items_in_group_toggle_button_id,
                                    'title' => Yii::t('SIMADiffViewer.Translation', 'ToggleSameItemsInGroup'),
                                    'tooltip' => Yii::t('SIMADiffViewer.Translation', 'ToggleSameItemsInGroup'),
                                    'onclick' => ['sima.diffViewer.toogleSameItemsInGroup', "$same_items_in_group_toggle_button_id"]
                                ], true);
                            }
                            else
                            {
                             echo Yii::app()->controller->widget('SIMAButton',[
                                    'id' => $same_items_in_group_toggle_button_id,
                                    'action'=>[
                                        'title'=>Yii::t('SIMADiffViewer.Translation', 'ToggleSameItemsInGroup'),
                                        'tooltip'=>Yii::t('SIMADiffViewer.Translation', 'ToggleSameItemsInGroup'),
                                        'onclick'=>['sima.diffViewer.toogleSameItemsInGroup', "$same_items_in_group_toggle_button_id"]
                                    ]
                                ], true);
                            }
                            ?>
                        </div>
                        <div class="sima-diff-viewer-group-toggle-cache">
                            <?php
                                if ($this->can_confirm_diffs === true)
                                {
                                   ?>
                                       <input type="checkbox" class="sima-diff-viewer-group-toggle-cache-chBox" 
                                           onclick="sima.diffViewer.toogleGroupSaveDiff($(this))" <?=$is_group_diff_confirmed ? 'checked' : ''?>>
                                   <?php
                                   echo Yii::t('SIMADiffViewer.Translation', 'ConfirmDiffs');
                                }
                            ?>
                        </div>
                        <?php
                            $column_aligns = [];
                            if (isset($diff['columns']))
                            {
                                ?>
                                    <div class="sima-diff-viewer-header">
                                        <div class="sima-diff-viewer-row">
                                            <div class="sima-diff-viewer-row-cell" style="width: 25px;">
                                                <input type="checkbox" class="sima-diff-viewer-chBox <?=$is_group_diff_confirmed ? '_disabled' : ''?>" checked="checked">
                                            </div>
                                            <div class="sima-diff-viewer-row-cell" style="width: 80px;">
                                                <?php echo Yii::t('BaseModule.Common', 'Status'); ?>
                                            </div>
                                            <?php
                                                foreach ($diff['columns'] as $column)
                                                {
                                                    $column_name = $column;
                                                    $column_align = '';
                                                    if (is_array($column))
                                                    {
                                                        if (isset($column[0]))
                                                        {
                                                            $column_name = $column[0];
                                                        }
                                                        if (isset($column['align']))
                                                        {
                                                            $column_align = "_{$column['align']}-align";
                                                        }
                                                    }
                                                    $column_aligns[$column_name] = $column_align;
                                                    ?>
                                                    <div class="sima-diff-viewer-row-cell <?php echo $column_align; ?>" style="width: <?php echo $column_width; ?>">
                                                        <?php echo $column_name; ?>
                                                    </div>    
                                                    <?php
                                                }
                                            ?>
                                        </div>
                                    </div>
                                <?php
                            }
                            if (!empty($diff['items']))
                            {
                                ?>
                                    <div class="sima-diff-viewer-body">
                                        <?php
                                            foreach ($diff['items'] as $item)
                                            {
                                                $item_status = strtolower($item['status']);
                                                $item_save_data = !empty($item['data']) ? $item['data'] : [];
                                                ?>
                                                    <div class="sima-diff-viewer-row _<?php echo $item_status; echo ($item['status']==='SAME') ? ' _hidden' : ''; ?>" 
                                                         data-save-data="<?php echo htmlspecialchars(CJSON::encode($item_save_data)); ?>">
                                                        <div class="sima-diff-viewer-row-cell _checkBox" style="width: 25px;">
                                                            <?php
                                                                if ($item_status !== 'same')
                                                                {
                                                                    $item_checked = $is_group_diff_confirmed ? '' : 'checked';
                                                                    $item_disabled = $is_group_diff_confirmed ? '_disabled' : '';
                                                                    ?>
                                                                        <input type="checkbox" class="sima-diff-viewer-chBox <?=$item_disabled?>" <?=$item_checked?>>
                                                                    <?php
                                                                }
                                                            ?>
                                                        </div>
                                                        <div class="sima-diff-viewer-row-cell _<?php echo $item_status; ?> _status" style="width: 80px;">
                                                            <?php echo Yii::t('SIMADiffViewer.Translation', 'Diff'.$item['status']); ?>
                                                        </div>
                                                        <?php
                                                            foreach ($item['columns'] as $column_key=>$column_value)
                                                            {
                                                                ?>
                                                                    <div class="sima-diff-viewer-row-cell <?php echo isset($column_aligns[$column_key]) ? $column_aligns[$column_key] : ''; ?>" 
                                                                         style="width: <?php echo $column_width; ?>">
                                                                        <?php
                                                                            if (
                                                                                    array_key_exists('old_value',$column_value) && array_key_exists('new_value',$column_value) && 
                                                                                    $column_value['old_value'] != $column_value['new_value']
                                                                               )
                                                                            {
                                                                                ?>
                                                                                    <div class="sima-diff-viewer-row-cell-old-value" 
                                                                                        title="<?php echo Yii::t('SIMADiffViewer.Translation', 'OldValueTitle'); ?>">
                                                                                        <span class="sima-diff-viewer-row-cell-old-value-title">
                                                                                            <?php echo Yii::t('SIMADiffViewer.Translation', 'OldValue').':'; ?>
                                                                                        </span>
                                                                                        <span class="sima-diff-viewer-row-cell-old-value-value">
                                                                                            &nbsp;<?php echo $column_value['old_value']; ?>
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="sima-diff-viewer-row-cell-new-value" 
                                                                                        title="<?php echo Yii::t('SIMADiffViewer.Translation', 'NewValueTitle'); ?>">
                                                                                        <span class="sima-diff-viewer-row-cell-new-value-title">
                                                                                            <?php echo Yii::t('SIMADiffViewer.Translation', 'NewValue').':'; ?>
                                                                                        </span>
                                                                                        <span class="sima-diff-viewer-row-cell-new-value-value">
                                                                                            &nbsp;<?php echo $column_value['new_value'];  ?>
                                                                                        </span>
                                                                                    </div>
                                                                                <?php
                                                                            }
                                                                            else if (array_key_exists('old_value',$column_value))
                                                                            {
                                                                                echo $column_value['old_value']; 
                                                                            }
                                                                            else if (array_key_exists('new_value',$column_value))
                                                                            {
                                                                                echo $column_value['new_value'];
                                                                            }
                                                                        ?>
                                                                    </div>
                                                                <?php
                                                            }
                                                        ?>
                                                    </div>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                <?php
                            }
                        ?>
                    </div>
                <?php
            }
        }
    ?>
</div>

