<?php

class SIMADiffViewerController extends SIMAController
{
    public function actionRenderDiffViewer()
    {
        $data = $this->setEventHeader();
        
        $diffs = $this->filter_input($data, 'diffs', false);
        $can_confirm_diffs = $this->filter_input($data, 'can_confirm_diffs', false);

        //Sasa A. - salje se cisto neki procenat, jer ne moze unutar ekstenzije posto se razlike obradjuju unutar view, a tamo ne moze da se salje procenat,
        //jer se radi echo prilikom slanja procenta i onda se ne posalje ceo html koji se izrenderuje unutar view-a. Prebacivanje obrade diff-a van view nije 
        //jednostavano jer imamo foreach unutar foreach-a, a procenti bi trebalo da se salju unutar drugog foreach-a
        $this->sendUpdateEvent(50);
        
        $html = $this->widget('SIMADiffViewer', [
            'diffs' => $diffs,
            'can_confirm_diffs' => $can_confirm_diffs
        ], true);
        
        $this->sendUpdateEvent(100);

        $this->sendEndEvent([
            'html' => $html
        ]);
    }
    
    public function actionSaveDiffViewer()
    {
        $data = $this->setEventHeader();
        $diffs = $data['client_data'];
        
        if(empty($diffs))
        {
            throw new SIMAWarnException(Yii::t('SIMADiffViewer.Translation', 'PleaseSelectSynchronizationItems'));
        }

        if (!empty($data['groups_cache']))
        {
            $groups_cache = $data['groups_cache'];
            foreach ($groups_cache as $group_cache)
            {
                if (SIMAMisc::filter_bool_var($group_cache['cache']) === true && intval($group_cache['number_of_diffs_checked']) > 0)
                {
                    throw new SIMAWarnException(Yii::t('SIMADiffViewer.Translation', 'CacheUsedWithCheckedDiffs'));
                }
            }
        }

        if(isset($data['validate_after_save']) && SIMAMisc::filter_bool_var($data['validate_after_save']) === true)
        {
            $transaction = Yii::app()->db->beginTransaction();
            try
            {
                $this->handleDiffs($diffs, false);
                $this->validateModels($diffs);
            }
            catch(Exception $e)
            {
                $transaction->rollback();
                throw $e;
            }
            $transaction->commit();
        }
        else
        {
            $this->handleDiffs($diffs, true);          
        }              
        
        $this->sendEndEvent();
    }
    
    private function handleDiffs(&$diffs, $validate) 
    {
        $i = 0;
        $diffs_cnt = count($diffs);
        foreach ($diffs as &$diff) 
        {
            $percent = round(($i/$diffs_cnt)*70, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
            if (!empty($diff['status']))
            {
                $status = strtolower($diff['status']);
                if ($status === 'remove')
                {
                    if (!empty($diff['model']) && !empty($diff['model_id']))
                    {
                        $model = $diff['model']::model()->findByPk($diff['model_id']);
                        if (!is_null($model))
                        {
                            $model->delete($validate);
                        }
                    }
                }
                else if ($status === 'add')
                {
                    if (!empty($diff['model']) && !empty($diff['attributes']))
                    {
                        $model = new $diff['model'];
                        $model->attributes = $diff['attributes'];
                        $model->save($validate);
                        $diff['model_id'] = $model->getPrimaryKey();
                    }
                }
                else if ($status === 'modify')
                {
                    if (!empty($diff['model']) && !empty($diff['model_id']) && !empty($diff['attributes']))
                    {
                        $model = $diff['model']::model()->findByPk($diff['model_id']);
                        if (!is_null($model))
                        {
                            $model->attributes = $diff['attributes'];
                            $model->save($validate);
                        }
                    }
                }
            }
            $i++;
        }
    }
    
    private function validateModels($diffs) 
    {
        foreach ($diffs as $diff) 
        {
            if (strtolower($diff['status']) !== 'remove')
            {
                $model = $diff['model']::model()->findByPkWithCheck($diff['model_id']);
                if (!$model->validate()) 
                {
                    $error_msg = SIMAHtml::showErrorsInHTML($model);                              
                    throw new SIMAWarnException($error_msg);
                }
            }
        }
    }
}

