<?php

return [
    'OldValue' => 'Old',
    'NewValue' => 'New',
    'OldValueTitle' => 'Old value',
    'NewValueTitle' => 'New value',
    'DiffADD' => 'Add',
    'DiffREMOVE' => 'Remove',
    'DiffMODIFY' => 'Modify',
    'DiffSAME' => 'Same',
    'ToggleGroupsWithOnlySameItems' => 'Show/Hide groups without distinctions and cached',
    'ToggleSameItemsInGroup' => 'Show/Hide without distinctions',
    'ConfirmDiffs' => 'Remember differences',
    'CacheUsedWithCheckedDiffs' => 'Save failed! Can not mark the distinctions for save, if you checked "Remember deviations"',
    'PleaseSelectSynchronizationItems' => 'Please select synchronizat ionItems'
];

