<?php

return [
    'OldValue' => 'Stara',
    'NewValue' => 'Nova',
    'OldValueTitle' => 'Stara vrednost',
    'NewValueTitle' => 'Nova vrednost',
    'DiffADD' => 'Dodaje se',
    'DiffREMOVE' => 'Briše se',
    'DiffMODIFY' => 'Menja se',
    'DiffSAME' => 'Bez razlike',
    'ToggleGroupsWithOnlySameItems' => 'Prikaži/Sakrij grupe bez razlike i keširane',
    'ToggleSameItemsInGroup' => 'Prikaži/Sakrij bez razlike',
    'ConfirmDiffs' => 'Zapamti odstupanja',
    'CacheUsedWithCheckedDiffs' => 'Čuvanje nije uspelo! Ne možete da označite razlike za primenu ako ste označili "zapamti odstupanja"',
    'PleaseSelectSynchronizationItems' => 'Molimo vas izaberite stavke za sinhronizaciju'
];

