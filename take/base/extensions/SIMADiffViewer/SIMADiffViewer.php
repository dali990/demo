<?php

/*
     * Primer zadavanja:
     * 
        $this->widget('SIMADiffViewer', [
            'id' => SIMAHtml::uniqid(),
            'diffs' => [
                [
                    'type' => 'group',
                    'group_id' => 'id_grupe',
                    'group_name' => 'Grupa 1',
                    'columns' => ['name', 'debit', 'credit'], //kolone koje se ovde zadaju moraju i ispod da se definisu njihove vrednosti
                    'items' => [
                        [
                            'status' => 'MODIFY', //status jos moze biti ADD, REMOVE, SAME
                            'columns' => [
                                'name' => [
                                    'old_value' => 'asdas',
                                    'new_value' => 'asdas'
                                ],
                                'debit' => [
                                    'old_value' => 23,
                                    'new_value' => 32
                                ],
                                'credit' => [
                                    'old_value' => 23,
                                    'new_value' => 32
                                ]
                            ],
                            'data' => [ //data se automatski salje kada se klikne na save. Ako nije zadata funkcija za cuvanje onda se vrsi automatsko cuvanje, naravno ako su zadate 
                                    //neophodne vrednosti u data. U slucaju da je zadata save funkcija, onda se njoj prosledjuju razlike za cuvanje
                                'status' => 'MODIFY',
                                'model' => 'AccountTransaction',
                                'model_id' => 5,
                                'attributes' => [
                                    'debit' => 32,
                                    'credit' => 32,
                                    ...
                                ]
                            ]
                        ],
                        ...
                    ]
                ],
                ...
            ],
     *      //ima podrsku da se zapamte razlike i da se postave kao dobre
            'can_confirm_diffs'=>false
        ]);
     */

class SIMADiffViewer extends CWidget 
{
    public $id = null;
    public $diffs = [];
    //ima podrsku da se zapamte razlike i da se postave kao dobre
    public $can_confirm_diffs = false;
    public $class = '';
    
    public function run() 
    {
        if(empty($this->id))
        {
            $this->id = SIMAHtml::uniqid();
        }
        if (empty($this->diffs))
        {
            $this->diffs = [];
        }

        echo $this->render('index', []);
        
        $register_script = "sima.diffViewer.init('$this->id');";
        Yii::app()->clientScript->registerScript($this->id, $register_script, CClientScript::POS_READY);
    }
    
    public static function registerManual() 
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/simaDiffViewer.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaDiffViewer.css');
        
        $sima_diff_viewer_script = "sima.diffViewer = new SIMADiffViewer();";
        Yii::app()->clientScript->registerScript('sima_diff_viewer'.SIMAHtml::uniqid(), $sima_diff_viewer_script, CClientScript::POS_READY);
    }
}