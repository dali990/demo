
/* global sima */

function SIMADiffViewer()
{
    this.max_items_per_save = 50;
    
    this.init = function(wrap_id)
    {
        var wrap = $('#'+wrap_id);
        wrap.on('click', '.sima-diff-viewer-header .sima-diff-viewer-chBox', function(){
            var global_chbox = $(this);
            $(this).parents('.sima-diff-viewer-group:first').find('.sima-diff-viewer-body .sima-diff-viewer-chBox').each(function(){
                $(this).prop('checked',global_chbox.is(':checked'));
            });
        });
        wrap.find('.sima-diff-viewer-group').each(function(){
            var same_items_length = $(this).find('.sima-diff-viewer-body .sima-diff-viewer-row._same').length;
            //ako nema bez razlika sakrivamo dugme za toogle bez razlika
            if (same_items_length === 0)
            {
                $(this).find('.sima-diff-viewer-group-toggle-same-items').hide();
            }
            //ako su svi redovi bez razlika
            if ($(this).find('.sima-diff-viewer-body .sima-diff-viewer-row').length === same_items_length)
            {
                $(this).addClass('_no-changes').hide();
                $(this).find('.sima-diff-viewer-header .sima-diff-viewer-chBox').hide();
                $(this).find('.sima-diff-viewer-group-toggle-cache-chBox').prop('checked', false);
                $(this).find('.sima-diff-viewer-group-toggle-cache').hide();
            }
            //ako je grupa kesirana onda je sakrivamo
            if ($(this).hasClass('_diff_confirmed'))
            {
                $(this).hide();
            }
            //ako nema nijedan cekiran checkbox, onda necemo cekirati ni grupni
            if ($(this).find('.sima-diff-viewer-body .sima-diff-viewer-chBox:checked').length === 0)
            {
                $(this).find('.sima-diff-viewer-header .sima-diff-viewer-chBox').prop('checked', false);
            }
        });
        //ako nema nijedna grupa samo "bez izmena" i kesirana onda ne prikazujemo dugme za prikazivanje/skrivanje grupa, odnosno da nema nijedna koja je skrivena u startu
        if (wrap.find('.sima-diff-viewer-group._no-changes').length === 0 && 
                wrap.find('.sima-diff-viewer-group._diff_confirmed').length === 0)
        {
            wrap.find('.sima-diff-viewer-toggle-groups').hide();
        }
    };
    
    this.renderFromArray = function(diffs_array, params)
    {
        if (sima.isEmpty(params))
        {
            params = {};
        }
        
        var progress_bar_obj = sima.getLastActiveFunctionalElement();
        if (sima.isEmpty(progress_bar_obj))
        {
            progress_bar_obj = $('body');
        }
        
        sima.ajaxLong.start('diffViewer/renderDiffViewer', {
            showProgressBar: progress_bar_obj,
            data:{
                diffs:diffs_array,
                can_confirm_diffs:!sima.isEmpty(params.confirm_diff_func)
            },
            onEnd: function(response) {                                        
                render(response.html, params);
            }
        });
    };
    
    this.renderFromHtml = function(diffs_html, params)
    {
        render(diffs_html, params);
    };
    
    this.toogleSameItemsInGroup = function(button_id)
    {
        var _button = $('#'+button_id);
        _button.toggleClass('_same-items-visible');
        _button.parents('.sima-diff-viewer-group:first').find('.sima-diff-viewer-body .sima-diff-viewer-row._same').slideToggle();
    };
    
    this.toogleGroupsWithOnlySameItems = function(button_id)
    {
        var _button = $('#'+button_id);
        _button.toggleClass('_same-items-visible');
        _button.parents('.sima-diff-viewer:first').find('.sima-diff-viewer-group').each(function(){
            if ($(this).hasClass('_no-changes') || $(this).hasClass('_diff_confirmed'))
            {
                if (_button.hasClass('_same-items-visible'))
                {
                    $(this).find('.sima-diff-viewer-body .sima-diff-viewer-row._same').show();
                    $(this).find('.sima-diff-viewer-group-toggle-same-items').addClass('_same-items-visible');
                }
                else
                {
                    $(this).find('.sima-diff-viewer-body .sima-diff-viewer-row._same').hide();
                    $(this).find('.sima-diff-viewer-group-toggle-same-items').removeClass('_same-items-visible');
                }
                $(this).slideToggle();
            }
        });
    };
    
    this.toogleGroupSaveDiff = function(_checkBox)
    {
        var group = _checkBox.parents('.sima-diff-viewer-group:first');
        _checkBox.toggleClass('_input_changed'); //ima samo dva stanja, tako da moze toggle, da ako se zada pa skine, da se ne radi bzvz ponovo
        if (_checkBox.is(':checked'))
        {
            group.find('.sima-diff-viewer-chBox').each(function(){
                $(this).prop('checked', false).addClass('_disabled');
            });
        }
        else
        {
            group.find('.sima-diff-viewer-chBox').each(function(){
                $(this).removeClass('_disabled');
            });
        }
    };
    
    function render(diffs_html, params)
    {
        if (sima.isEmpty(params))
        {
            params = {};
        }
        
        if (sima.isEmpty(params.save_action))
        {
            params.save_action = 'diffViewer/saveDiffViewer';
        }
        
        var uniq_id = 'sima_diff_viewer_' + sima.uniqid();
        var html = {
            title: sima.translate('Differences'),
            html: '<div id="'+uniq_id+'">' + diffs_html + '</div>'
        };
        
        if(params.save_func_hidden === true)
        {
            sima.dialog.openInFullScreen(html.html, html.title);
        }
        else
        {
            var yes_func_params = {
                func: function() {
                    var wrap = $('#'+uniq_id);
                    var diffs = [];
                    var groups_cache = [];
                    wrap.find('.sima-diff-viewer-body .sima-diff-viewer-chBox:checked').each(function(){
                        diffs.push($(this).parents('.sima-diff-viewer-row:first').data('save-data'));
                    });
                    wrap.find('.sima-diff-viewer-group-toggle-cache-chBox._input_changed').each(function(){
                        var group = $(this).parents('.sima-diff-viewer-group:first');
                        groups_cache.push({
                            group_id:group.data('group_id'),
                            cache:$(this).is(':checked'),
                            number_of_diffs_checked:group.find('.sima-diff-viewer-body .sima-diff-viewer-chBox:checked').length
                        });
                    });

                    sima.dialog.close();

                    if (!sima.isEmpty(params.save_func))
                    {
                        params.save_func(diffs, groups_cache);
                    }
                    else
                    {
                        saveDiffs(diffs, groups_cache, params);
                    }
                },
                title: sima.translate('Save')
            };

            var no_func_params = {
                hidden: true
            };

            sima.dialog.openYesNo(html, yes_func_params, no_func_params, null, null, true);
        }
    }
    
    function saveDiffs(diffs, groups_cache, params)
    {
        if (!sima.isEmpty(diffs))
        {
            var loading_obj = sima.getLastActiveFunctionalElement();
            if (sima.isEmpty(loading_obj))
            {
                loading_obj = $('body');
            }

            sima.ajaxLong.start(params.save_action, {
                data: {
                    client_data: diffs,
                    validate_after_save: params.validate_after_save
                },
                split_data: sima.diffViewer.max_items_per_save,
                showProgressBar: loading_obj,
                onEnd: function(response) {
                    afterSaveDiffs(params, groups_cache);
                }
            });
        }
        else
        {
            afterSaveDiffs(params, groups_cache);
        }
    }
    
    function afterSaveDiffs(params, groups_cache)
    {
        if (!sima.isEmpty(params.after_save_func))
        {
            params.after_save_func();
        }
        if (!sima.isEmpty(params.confirm_diff_func))
        {
            params.confirm_diff_func(groups_cache);
        }
    }
}


