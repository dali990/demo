
/* global sima */

(function($){
    
    var methods = {
        init: function(params) {
            
            var _this = $(this);  
            var keyup_search = null;
            _this.data('input_search', _this.find('.sima-ui-filter-search-filed'));
            _this.data('search_function', params.search_function);
            
            _this.data('input_search').keyup(function(e){
                if(e.keyCode === 13)//enter
                {
                    executeTextSearch(_this);
                }
            });
            
            if(params.searchOnKeyup === true)
            {
                _this.data('input_search').on('input', function(){
                    if(keyup_search !== null)
                    {
                        clearTimeout(keyup_search);
                    }
                    keyup_search = setTimeout(function(){
                        executeTextSearch(_this);
                    }, 500);
                });
            }
        },
        toggleOptions: function() {            
            var _this = $(this);
            sima.notImplemented();
        },
        resetFilters: function() {
            var _this = $(this);
            _this.data('input_search').val('');
        },
        getValue: function() {
            var _this = $(this);
            return _this.data('input_search').val();
        }
    };
    
    function executeTextSearch(_this)
    {
        var input_text = _this.data('input_search').val();
        _this.trigger('text_search', [{text:input_text}]);
        
        if(!sima.isEmpty(_this.data('search_function')))
        {
            sima.executeFunction(_this.data('search_function'), []);
        }
    }
    
    $.fn.simaUIFilter = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jquery.simaUIFilter');
            }
        });
        return ret;
    };
})(jQuery);