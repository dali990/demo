<?php

class SIMAUIFilter extends CWidget 
{
    public $id = null;
    public $class = '';    
    public $htmlOptions = [];
    public $searchOnKeyup = false;
    public $search_function = null;

    public function run() 
    {
        if(empty($this->id))
        {
            $this->id = SIMAHtml::uniqid();
        }
        
        $html_options = '';
        foreach ($this->htmlOptions as $option_key=>$option_value)
        {
            $html_options .= " $option_key='$option_value'";
        }
        
        echo $this->render('index', [
            'html_options'=>$html_options            
        ]);

        $params = [
            'searchOnKeyup' => $this->searchOnKeyup,
            'search_function' => $this->search_function
        ];
        $json = CJSON::encode($params);
        $register_script = "$('#" . $this->id . "').simaUIFilter('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }    
    
    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.simaUIFilter.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaUIFilter.css');
    }
}