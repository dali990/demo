<?php

class SIMAIcons extends CWidget 
{
    public function registerManual() 
    {
        $assets = dirname(__FILE__) . '/assets';

        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/style.css');
    }
}