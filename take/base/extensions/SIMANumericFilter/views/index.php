<div id="<?php echo $this->id; ?>" class="sima-ui-numeric-filter">
    
    <?php
        $default_sign_class = 'arrow-equal-left';
        if($this->default_sign === '=')
        {
            $default_sign_class = 'arrow-equal';
        }
    ?>
    <div class="sima-ui-numeric-filter-btn-dropdown sima-ui-numeric-filter-icon <?=$default_sign_class?> _black _20"
         data-class_name="<?=$default_sign_class?> -equal-left" data-value="<?=$this->default_sign?>"></div>
    <input class="sima-ui-numeric-filter-input main_value" name="<?php echo $this->name; ?>" type="text" value="<?php echo $this->value; ?>" 
           data-min_value="" data-max_value="" data-min_class_name="" data-max_class_name=""/>    
    <input class="sima-ui-numeric-filter-input main_display" type="text" value="<?php echo $this->value; ?>"    
        <?php
            foreach ($htmlOptions as $key => $value)
            {
                if ($value != false)
                    echo $key . '="' . $value . '" ';
            }
        ?>
    />    
    <span class="sima-ui-numeric-filter-clear sima-icon _remove _16 <?php echo isset($htmlOptions['disabled'])?'_disabled':''; ?>"></span>
        
    <div class="sima-ui-numeric-filter-btn-dropdown-container" style="display: none;">
        <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-equal _black _20" data-class_name="arrow-equal" data-value="="></div>
        <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-left _black _20" data-class_name="arrow-left" data-value="<"></div>
        <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-equal-left _black _20" data-class_name="arrow-equal-left" data-value="<="></div>
        <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-right _black _20" data-class_name="arrow-right" data-value=">"></div>
        <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-equal-right _black _20" data-class_name="arrow-equal-right" data-value=">="></div>
        <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-range _black _20" data-class_name="arrow-range"></div>
    </div>
    
    <div class="sima-ui-numeric-filter-range-wrapper">        
        <div class="sima-ui-numeric-filter-range-min-wrapper">            
            <div class="sima-ui-numeric-filter-btn-dropdown-range sima-ui-numeric-filter-icon arrow-equal-left _black _20" data-class_name="arrow-equal-left" data-value="<="></div>
            <div class="sima-ui-numeric-filter-btn-dropdown-container-range" style="display: none;">
                <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-left _black _20" data-class_name="arrow-left" data-value="<"></div>
                <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-equal-left _black _20" data-class_name="arrow-equal-left" data-value="<="></div>
                <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-right _black _20" data-class_name="arrow-right" data-value=">"></div>
                <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-equal-right _black _20" data-class_name="arrow-equal-right" data-value=">="></div>
            </div>           
            <input class="sima-ui-numeric-filter-input range" type="text" value="<?php echo $this->value; ?>"/>
        </div>
        
        <div class="sima-ui-numeric-filter-range-max-wrapper">            
            <div class="sima-ui-numeric-filter-btn-dropdown-range sima-ui-numeric-filter-icon arrow-equal-left _black _20" data-class_name="arrow-equal-left" data-value="<="></div>
            <div class="sima-ui-numeric-filter-btn-dropdown-container-range" style="display: none;">
                <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-left _black _20" data-class_name="arrow-left" data-value="<"></div>
                <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-equal-left _black _20" data-class_name="arrow-equal-left" data-value="<="></div>
                <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-right _black _20" data-class_name="arrow-right" data-value=">"></div>
                <div class="sima-ui-numeric-filter-btn-icon sima-ui-numeric-filter-icon arrow-equal-right _black _20" data-class_name="arrow-equal-right" data-value=">="></div>
            </div>           
            <input class="sima-ui-numeric-filter-input range" type="text" value="<?php echo $this->value; ?>"/>
        </div>
        <div class="sima-ui-numeric-filter-btn-apply-icon">Potvrdi</div>
    </div>
</div>

