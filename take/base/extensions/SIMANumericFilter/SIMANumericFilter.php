<?php

class SIMANumericFilter extends CWidget 
{
    public $id = null;
    public $name = '';
    public $value = '';
    public $htmlOptions = [];
    public $onchange = '';
    public $numericInputParams = [];
    public $default_sign = '<=';
    
    public function run() 
    {   
        if(empty($this->id))
        {
            $this->id = SIMAHtml::uniqid().'_numeric_filter';
        }                                
        
        echo $this->render('index', [
            'htmlOptions' => $this->htmlOptions,            
        ]);
                
        $params = [            
            'onchange'=>$this->onchange,
            'numericInputParams'=>$this->numericInputParams
        ];
        $json = CJSON::encode($params);
        $register_script = "$('#" . $this->id . "').simaNumericFilter('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
    
    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.simaNumericFilter.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaNumericFilter.css');
    }
}