<?php

require_once(dirname(__FILE__) . '/tcpdf/tcpdf.php');

class SIMATCPdf extends TCPDF
{
    public $htmlHeader = '';
    public $htmlFooter = '';
    public $include_page_numbering = true;
    public $only_show_header_on_first_page = false;
    
    public function SetFont($family, $style='', $size=null, $fontfile='', $subset='default', $out=true)
    {        
        $this->setHeaderFont([$family, $style, $size]);
        $this->setFooterFont([$family, $style, $size]);
        
        parent::SetFont($family, $style, $size, $fontfile, $subset, $out);
    }
    
    public function setHtmlHeader($htmlHeader, $only_show_header_on_first_page=false) 
    {        
        $this->htmlHeader = empty($htmlHeader)?'':$htmlHeader;
        $this->only_show_header_on_first_page = $only_show_header_on_first_page;
    }
    
    public function Header() 
    {
        $only_show_header_on_first_page = filter_var($this->only_show_header_on_first_page, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if ($only_show_header_on_first_page===false || ($only_show_header_on_first_page===true && intval($this->page) === 1)) 
        {
            $this->SetFont('freesans', '', 12);
            $this->writeHTML($this->htmlHeader, true, false, true, false, 'C');
        }
    }
    
    public function setHtmlFooter($htmlFooter, $include_page_numbering=true) 
    {        
        $this->include_page_numbering = $include_page_numbering;
        $this->htmlFooter = (empty($htmlFooter)?'':$htmlFooter);
    }

    public function Footer() 
    {        
        $this->SetFont('freesans', '', 12);
        $include_page_numbering = filter_var($this->include_page_numbering, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if ($include_page_numbering === true)
        {
            $this->printPageNumber();
        }
        else
        {
            $page_numbering = $this->getPageNumbering();
            $html_footer = $this->htmlFooter;
            $new_html_footer = str_replace('**PN**', $page_numbering, $html_footer);            
            $this->writeHTML($new_html_footer, true, false, true, false, 'C');
        }
    }

    private function printPageNumber()
    {
        $pagenumtxt = $this->getPageNumbering();
        $this->SetY($this->y);
        //Print page number
        if ($this->getRTL()) 
        {            
            $this->SetX($this->original_rMargin);
            $this->Cell(0, 0, $pagenumtxt, 0, 0, 'L');
        } 
        else 
        {            
            $this->SetX($this->original_lMargin);
            $this->Cell(0, 0, $this->getAliasRightShift().$pagenumtxt, 0, 0, 'R');
        }
    }
    
    private function getPageNumbering()
    {
        $w_page = isset($this->l['w_page']) ? $this->l['w_page'].' ' : '';
        if (empty($this->pagegroups)) 
        {
            $pagenumtxt = $w_page.$this->getAliasNumPage().' / '.$this->getAliasNbPages();
        } 
        else 
        {
            $pagenumtxt = $w_page.$this->getPageNumGroupAlias().' / '.$this->getPageGroupAlias();
        }
        
        return $pagenumtxt;
    }
    
    // Colored table
    public function ColoredTable($header,$data, $cell_w, $cell_h, $cell_padding, $add_page = true) 
    {
        // add a page
        if ($add_page)
        {
            $this->AddPage();
        }
        
        // Colors, line width and bold font
//        $this->SetFillColor(255, 0, 0);
//        $this->SetTextColor(255);
//        $this->SetDrawColor(128, 0, 0);
//        $this->SetLineWidth(0.3);
//        $this->SetFont('', 'B');
//        
        $this->SetCellPadding($cell_padding);
        
        // Header
        $num_headers = 0;
        if(is_array($header))
        {
            $num_headers = count($header);
            for($i = 0; $i < $num_headers; ++$i) {
                $this->Cell($cell_w, 7, $header[$i], 1, 0, 'C', 1);
            }
            $this->Ln();
        }
        else if(is_int($header))
        {
            $num_headers = $header;
        }
        else
        {
            throw new Exception('Invalid header');
        }
        // Color and font restoration
//        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Data
        $fill = 0;
        $column_counter = 0;
        foreach($data as $d) {
            $this->writeHTMLCell($cell_w, $cell_h, '', '', $d, 'LRTB');
            $column_counter++;
            
            if($column_counter === $num_headers)
            {
                $column_counter = 0;
                $this->Ln();
            }
            
            $this->checkPageBreak($cell_h,'',true);
        }
    }
    
    /**
     * redefinisana f-ja
     * @param type $msg
     * @throws Exception
     */
    public function Error($msg) {
        // unset all class variables
        $this->_destroy(true);
        throw new Exception('TCPDF ERROR: '.$msg);
    }
}
