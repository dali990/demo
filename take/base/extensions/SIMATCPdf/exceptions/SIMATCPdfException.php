<?php

class SIMATCPdfException extends SIMAException
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
