<?php

//ucitavamo eksteniziju wkhtmltopdf
require_once dirname(__FILE__) . '/vendor/autoload.php';

use mikehaertl\wkhtmlto\Pdf;

/**
 * Primer kreiranja:
 * $pdf = new SIMAHtmlToPdf($options, $additional_params);
 * $options je niz opcija koji je definisan na stranici http://wkhtmltopdf.org/usage/wkhtmltopdf.txt
 * Bitna stavka je da ako se zadaje margin-top i postoji header, onda mora u margin-top da se uracuna visina headera + zeljeni razmak.
 * Isto vazi i za margin-bottom i footer. To je zbog toga sto se margine odnose na content.
 * $additional_params - onlyShowHeaderOnFirstPage(samo prilazuje header na prvoj strani)
 */

class SIMAHtmlToPdf extends Pdf
{
    public function __construct(&$options=[], $additional_params=[])
    {
        if (isset($options['header-html']))
        {
            $options['header-html'] = $this->wrapHtmlInDocument($options['header-html'], 'HEADER', $additional_params);
        }
        if (isset($options['footer-html']))
        {
            $options['footer-html'] = $this->wrapHtmlInDocument($options['footer-html'], 'FOOTER');
        }
        
        parent::__construct($options);
    }
    
    public function addPage($input, $options=array(), $type=null)
    {
        return parent::addPage($this->wrapHtmlInDocument($input), $options, $type);
    }
    
    private function wrapHtmlInDocument($html='', $htmlType='BODY', $params=[])
    {
        //Sasa A. - za sada se ne koristi, jer nisam uspeo da napravim da se header javlja samo na prvoj strani. Tako da ce se u tom slucaju header ubacivati u body
//        $onlyShowHeaderOnFirstPage = SIMAMisc::filter_bool_var(isset($params['onlyShowHeaderOnFirstPage']) ? $params['onlyShowHeaderOnFirstPage'] : false);
        return "
                <!DOCTYPE html>
                <html>
                    <head>
                        <meta charset='UTF-8' />
                        
                        <style>
                        </style>
                        
                        <script>
                            function subst() {
                                var vars={};
                                var x=document.location.search.substring(1).split('&');
                                for(var i in x) 
                                {
                                    var z=x[i].split('=',2);
                                    vars[z[0]] = unescape(z[1]);
                                }
                                var x=['frompage','topage','page','webpage','section','subsection','subsubsection'];
                                
                                for(var i in x)
                                {
                                    var y = document.getElementsByClassName(x[i]);
                                    for(var j=0; j<y.length; ++j)
                                    {
                                        y[j].textContent = vars[x[i]];
                                    }
                                }
                                
                                var y = document.getElementsByClassName('sima-iframe');
                                for(var j = 0; j < y.length; j++)
                                {
                                    y[j].height = y[j].contentWindow.document.body.scrollHeight + 'px';
                                }

                                //if ('$htmlType' == 'HEADER')
                                //{
                                    //var y = document.getElementsByClassName('$htmlType');
                                    //if (vars[x[2]] != 1)
                                    //{
                                        //for(var j = 0; j < y.length; j++)
                                        //{
                                            //y[j].innerHTML = '';
                                        //}
                                    //}
                                //}
                            }
                        </script>
                    </head>
                    <body style='border:0; margin: 0;' onload='subst()'>
                        <div class='$htmlType'>
                            $html
                        </div>
                    </body>
                </html>";
    }
}