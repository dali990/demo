
/* global Vue, sima */

Vue.component('sima-tabs', {
    template: '#sima-tabs-template',
    props: {
        tabs: {
            type: Array,
            default: function() {
                return [];
            }
        },
        tree_options: {
            type: Object,
            default: function() {
                return {};
            }
        },
        tabs_panel_layout_options: {
            type: Object,
            default: function() {
                return {
                    proportion: 0.1,
                    min_width: 200
                };
            }
        },
        content_panel_layout_options: {
            type: Object,
            default: function() {
                return {
                    proportion: 0.9
                };
            }
        },
        has_tab_refresh: {
            type: Boolean,
            default: true
        },
        default_selected: {
            type: [String, Object, Array],
            default: ''
        }
    },
    data: function () {
        return {
            loaded_tabs_content: []
        };
    },
    watch: {
        tabs: function() {
            this.setNewTreeData(this.tabs);
        }
    },
    computed: {
        tab_refresh_icon_class: function() {
            return 'sima-icon _refresh _white _16';
        }
    },
    created: function() {
        if (!sima.isEmpty(this.default_selected))
        {
            var _default_selected = this.default_selected;
            if (typeof _default_selected === 'object' && typeof _default_selected[0] !== 'undefined')
            {
                _default_selected = _default_selected[0];
            }

            this.tree_options.default_selected = _default_selected;
        }

        if (!sima.isEmpty(this.tabs))
        {
            this.tree_options.tree_data = this.tabs;
        }
    },
    mounted: function() {
        var _this = this;

        this.initTabEvents();

        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        initTabEvents: function() {
            var _this = this;

            this.$refs.tree.$on('afterTreeItemSelect', function(tree_item) {
                if (!_this.isTabLoaded(tree_item.item_data.code))
                {
                    _this.loadTab(tree_item);
                }
                else
                {
                    _this.selectTab(tree_item);
                }
            });
            
            this.$refs.tree.$on('afterTreeItemUnselect', function(tree_item) {
                _this.unselectTab(tree_item);
            });
            
            this.$refs.tree.$on('afterTreeItemSelectionChanged', function(tree_item, selection_type) {
                _this.$emit('afterTreeItemSelectionChanged', tree_item, selection_type);
            });
        },
        loadTab: function(tree_item) {
            var _this = this;
            var tree_item_data = tree_item.item_data;

            if (!sima.isEmpty(tree_item_data.content_html))
            {
                this.addTabToLoadedTabs(tree_item);
                setTimeout(function() {
                    _this.afterTabLoaded(tree_item, tree_item_data.content_html);
                }, 0);
            }
            else if (!sima.isEmpty(tree_item_data.content_action))
            {
                var content_action = tree_item_data.content_action;
                var get_params = !sima.isEmpty(content_action.get_params) ? content_action.get_params : {};
                var post_params = !sima.isEmpty(content_action.post_params) ? content_action.post_params : {};
                
                if (typeof _this.default_selected.selector === 'object')
                {
                    post_params.selector = _this.default_selected.selector;
                }
                
                this.addTabToLoadedTabs(tree_item);
                
                sima.ajax.get(content_action.action,{
                    get_params: get_params,
                    data: post_params,
                    async: true,
                    loadingCircle: $(_this.$el).find('.sima-tabs-vue-content:first'),
                    success_function: function(response)
                    {
                        _this.afterTabLoaded(tree_item, response.html);
                    }
                });
            }
        },
        //ako tab nije ucitan, dodajemo ga na spisak ucitanih i dodajemo ikonicu za refresh taba
        addTabToLoadedTabs: function(tree_item) {
            var tree_item_data = tree_item.item_data;
            var tree_item_code = tree_item_data.code;

            if (!this.isTabLoaded(tree_item_code))
            {
                this.loaded_tabs_content.push({
                    code: tree_item_code,
                    tree_item: tree_item,
                    visible: false
                });
            }
        },
        //dodaje html taba u DOM i selektuje ga
        afterTabLoaded: function(tree_item, tab_html) {
            var tree_item_data = tree_item.item_data;
            var tree_item_code = tree_item_data.code;

            //dodajemo sadrzaj taba u DOM
            $(this.$el).find('.sima-tab-vue-content[data-code="'+tree_item_code+'"]').html(tab_html);
            
            this.selectTab(tree_item);
        },
        selectTab: function(tree_item) {
            var _this = this;
            var tree_item_data = tree_item.item_data;
            var tree_item_code = tree_item_data.code;
            
            //dodajemo refresh ikonicu ako ne postoji
            if (this.has_tab_refresh === true)
            {
                this.addRefreshIconToTab(tree_item);
            }
            
            $.each(this.loaded_tabs_content, function(index, value) {
                if (value.code === tree_item_code)
                {
                    value.visible = true;
                    _this.hideShowRefreshIconInTab(value.tree_item, true);
                }
                else
                {
                    value.visible = false;
                    _this.hideShowRefreshIconInTab(value.tree_item, false);
                }
            });
            
            this.$nextTick(function() {
               sima.layout.allignObject($(_this.$el));
            });
        },
        unselectTab: function(tree_item) {
            var _this = this;
            var tree_item_data = tree_item.item_data;
            var tree_item_code = tree_item_data.code;

            $.each(this.loaded_tabs_content, function(index, value) {
                if (value.code === tree_item_code)
                {
                    value.visible = false;
                    _this.hideShowRefreshIconInTab(value.tree_item, false);
                }
            });
        },
        isTabLoaded: function(tab_code) {
            var founded = false;
            $.each(this.loaded_tabs_content, function(index, value) {
                if (founded === false && value.code === tab_code)
                {
                    founded = true;
                }
            });
            
            return founded;
        },
        hideShowRefreshIconInTab: function(tree_item, show) {
            if (this.has_tab_refresh === true && typeof tree_item.item_data.title.parts !== 'undefined')
            {
                var _this = this;
                $.each(tree_item.item_data.title.parts, function(index, value) {
                    if (value.class === _this.tab_refresh_icon_class)
                    {
                        value.visible = show;
                    }
                });
            }
        },
        addRefreshIconToTab: function(tree_item) {
            var _this = this;
            var tree_item_data = tree_item.item_data;

            if (typeof tree_item_data.title === 'string')
            {
                tree_item_data.title = {
                    text: tree_item_data.title,
                    parts: []
                };
            }
            else if (typeof tree_item_data.title.parts === 'undefined')
            {
                tree_item_data.title.parts = [];
            }

            var has_tab_refresh = false;
            $.each(tree_item_data.title.parts, function(index, value) {
                if (value.class === _this.tab_refresh_icon_class)
                {
                    has_tab_refresh = true;
                    return false;
                }
            });

            if (has_tab_refresh === false)
            {
                tree_item_data.title.parts.unshift({
                    class: _this.tab_refresh_icon_class,
                    position: 'after',
                    visible: false,
                    onclick: function() {
                        _this.loadTab(tree_item);
                    }
                });
            }
        },
        setNewTreeData: function(tabs) {
            var _this = this;

            this.$refs.tree.setNewTreeData(tabs);
            
            //moramo opet trigerovati afterTreeItemSelect za selektovani item, kako bi se dodala refresh ikonica, jer ce new_tabs da pregazi
            this.$nextTick(function() {
                if (typeof _this.$refs.tree !== 'undefined' && _this.$refs.tree.main_vue_obj.selected_item !== null)
                {
                    _this.$refs.tree.$emit('afterTreeItemSelect', _this.$refs.tree.main_vue_obj.selected_item);
                }
            });
        },
        selectTabByCode: function(code) {
            this.$refs.tree.selectItemByCode(code);
        },
        reloadTab: function(tab_code) {
            for(var i = 0; i < this.loaded_tabs_content.length; i++)
            {
                if (this.loaded_tabs_content[i].code === tab_code)
                {
                    this.loaded_tabs_content.splice(i, 1);
                    this.selectTabByCode(tab_code);
                    break;
                }
            }
        }
    }
});