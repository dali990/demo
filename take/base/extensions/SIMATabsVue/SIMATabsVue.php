<?php

class SIMATabsVue extends CWidget 
{
    public static function registerManual() 
    {
        $dirname = dirname(__FILE__);
        $assets = $dirname . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        $uniq = SIMAHtml::uniqid();
        
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/simaTabsVue.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/simaTabsVueLayoutEventHandler.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaTabsVue.css');
        Yii::app()->clientScript->registerScript("sima_tabs_$uniq", include($dirname.'/templates/sima_tabs.php'), CClientScript::POS_END);
    }
}