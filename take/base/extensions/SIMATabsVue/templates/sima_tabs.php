<script type="text/x-template" id="sima-tabs-template">
    <div class="sima-tabs-vue sima-layout-panel _splitter _vertical">
        <sima-tree 
            ref="tree"
            v-bind:data-sima-layout-init='JSON.stringify(tabs_panel_layout_options)'
            v-bind="tree_options"
        >
        </sima-tree>
        <div 
            class="sima-tabs-vue-content sima-layout-panel"
            v-bind:data-sima-layout-init='JSON.stringify(content_panel_layout_options)'
        >
            <div 
                ref="content"
                class="sima-tab-vue-content sima-layout-panel" 
                :key="loaded_tab_content.code"
                v-bind:data-code="loaded_tab_content.code"
                v-for="loaded_tab_content in loaded_tabs_content"
                v-show="loaded_tab_content.visible" 
            >    
            </div>
        </div>
    </div>
</script>