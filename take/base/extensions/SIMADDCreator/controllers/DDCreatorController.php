<?php

class DDCreatorController extends SIMAController
{
    private function localRenderPartial($view, $data=NULL, $return=false, $processOutput=false)
    {
        $view = 'base.extensions.SIMADDCreator.views.'.$view;

        if($return)
        {
            return parent::renderPartial($view, $data, $return, $processOutput);
        }
        else
        {
//            echo parent::renderPartial($view, $data, $return, $processOutput);
            $errmsg = __METHOD__.' - nije trebalo doci dovde';
            error_log($errmsg);
            Yii::app()->errorReport->createAutoClientBafRequest($errmsg);
        }
    }
    
    /**
     * akcija koja refresuje listu licnih tagova u fajl menadzeru
     */
    public function actionRefreshPersonalFilters()
    {
        $user_id = $this->filter_post_input('user_id');
        $user = User::model()->findByPk($user_id);
        
        $personal_tags = $user->limittag_personalTags;
        
        $html = $this->localRenderPartial('personalTags', array(
            'personal_tags'=>$personal_tags,
        ), true, true);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    /**
     * akcija koja refresuje listu security tagova u fajl menadzeru
     */
    public function actionRefreshSecurityFilters()
    {
        $user_id = $this->filter_post_input('user_id');
        $user = User::model()->findByPk($user_id);
        
        $security_tags = $user->tql_security_tags;
        
        $html = $this->localRenderPartial('securityTags', array(
            'security_tags'=>$security_tags,
        ), true, true);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
}