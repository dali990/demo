<?php

class SIMADDCreator extends CWidget 
{
    public $id = null;
    public $name = "";
    public $tree_json = null;
    public $security_tags = array();
    public $personal_tags = array();
    
    public function run() 
    {
//        if(empty($this->dd_id))
//        {
//            throw new Exception('Definition id must not be null');
//        }
        
        $directory_definition_filters = Yii::app()->params['directoryDefinitionFilters'];
        $directory_definition_types = Yii::app()->params['directoryDefinitionTypes'];
        
        if(empty($this->id))
        {
            $this->id = SIMAHtml::uniqid().'_dd_creator';
        }
        
        echo $this->render('index', array(
            'id' => $this->id,
            'security_tags'=> $this->security_tags,
            'personal_tags'=> $this->personal_tags,
            'filters'=>$directory_definition_filters,
            'types'=>$directory_definition_types,
        ));
                        
        $params = array(
            'name' => $this->name,
            'tree_json' => $this->tree_json
        );
        $json = CJSON::encode($params);
        $register_script = "$('#" . $this->id . "').ddCreator('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);      
    }
    
    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.ddCreator.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/ddCreator.css');
    }
}