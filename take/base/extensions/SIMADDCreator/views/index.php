<?php
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
    
    $uniq = SIMAHtml::uniqid();
?>

<?php $this->beginWidget('SIMAResizablePanel',
        array(
            'id'=>$id,
            'type'=>'vertical',
            'proportions'=>array(0.4, 0.6),
            'class'=>'sima-ui-dd-creator'
        )); ?>

    <div class="sima-ui-dd-creator-left-side sima-layout-panel _horizontal">
        <?php
            echo $this->render('tree', array());
        ?>
    </div>
    <div class="sima-layout-panel">
        <?php
            echo $this->render('filters', array(
                'id' => $id,
                'security_tags'=> $security_tags,
                'personal_tags'=> $personal_tags,
                'filters'=>$filters,
                'types'=>$types
            ));
        ?>
    </div>

<?php $this->endWidget(); ?>
