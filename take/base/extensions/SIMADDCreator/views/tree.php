<div class="sima-ui-dd-creator-tree-options sima-layout-fixed-panel">
    <input class='directory_definition_name' disabled placeholder="Naziv"></input>
    <button class='button disabled'><?php echo Yii::t('SIMADDCreator.Translation', 'SaveDefinition'); ?></button>
</div>
<div class="sima-layout-fixed-panel">
    <div class="sima-ui-dd-creator-tree-attributes">
        <span class="attribute_item">
            <?php echo Yii::t('SIMADDCreator.Translation', 'ShowUnlisted'); ?><input type="checkbox" id="show_unlisted" name="show_unlisted">
        </span>
        <span class="attribute_item">
            Ne prikazuj prazne direktorijume<input type="checkbox" id="hideempty" name="hideempty">
        </span>
        <span class="attribute_item">
            <?php echo Yii::t('SIMADDCreator.Translation', 'Recursive'); ?><input type="checkbox" id="tag_group_tree_view" name="tag_group_tree_view">
        </span>
        <span class="attribute_item">
            Minimalni broj fajlova<input style="width:30px; height: 12px;" type="input" id="min_file_listed" name="min_file_listed">
        </span>
        <span class="attribute_item">
            <?php echo Yii::t('SIMADDCreator.Translation', 'NullName'); ?><input type="input" style="width:100px; height: 12px;" id="null_name" name="null_name"> 
        </span>
        <span class="attribute_item">
             Skip<input type="checkbox" id="skip" name="skip">
        </span>
    </div>
</div>
<div class="sima-ui-dd-creator-tree sima-layout-panel">
    <span class="sima-ui-dd-creator-tree-startup_message">Prvo selektujete neku definiciju sa leve strane ili dodajte novu a zatim ovde prevlacite tagove sa desne strane</span>
    <ul class="sima-ui-dd-creator-tree-list"></ul>
</div>