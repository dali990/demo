<div class="sima-ui-dd-creator-filters security">
    <div class="sima-ui-dd-creator-filters-head"><?php echo Yii::t('SIMADDCreator.Translation', 'SecurityFilters'); ?> 
        <?php if (Yii::app()->user->checkAccess('modelTQLSecurityTagAdd')) { ?>
        <span class='add_security_filter'><?php 
            echo SIMAHtml::modelFormOpen(TQLSecurityTag::model(), array(
                'init_data'=>array(
                    'TQLSecurityTag'=>array(
                        'owner_id' => Yii::app()->user->id,
                    )
                 ),
                'onSave'=>'function(response){$("#'.$id.'").ddCreator("refreshSecurityFilters", "'.Yii::app()->user->id.'")}'
            )); 
        ?></span>
        <?php }?>
    </div>
    <div class="sima-ui-dd-creator-filters-body">
        <?php
            foreach ($security_tags as $tag)
            {
                ?>
                <div class="sima-ui-dd-creator-type-body-item filter security"
                     data-filter="<?php echo isset($tag->name)?$tag->name:''; ?>"
                     data-id="<?php echo isset($tag->id)?$tag->id:''; ?>"
                     data-alias="<?php echo isset($tag->name)?$tag->name:''; ?>"
                     data-filter_type="<?php echo 'tql_security_filter'; ?>" 
                     data-attributes="show_unlisted">
                    <span class="sima-ui-dd-creator-type-body-item-text filter security">
                        <?php
                            echo isset($tag->name)?$tag->name:'';
                        ?>
                    </span>
                </div>
                <?php
            }
        ?>
        <div class='clearfix'></div>
    </div>
</div>
<div class="sima-ui-dd-creator-filters personal">
    <div class="sima-ui-dd-creator-filters-head"><?php echo Yii::t('SIMADDCreator.Translation', 'PersonalFilters'); ?> <span class='add_personal_filter'><?php 
        echo SIMAHtml::modelFormOpen(TQLPersonalTag::model(), array(
                'init_data'=>array(
                    'TQLPersonalTag'=>array(
                        'owner_id' => Yii::app()->user->id,
                    )
                 ),
                'onSave'=>'function(response){$("#'.$id.'").ddCreator("refreshPersonalFilters", "'.Yii::app()->user->id.'")}'
            )); 
    ?></span></div>
    <div class="sima-ui-dd-creator-filters-body">
        <?php
            foreach ($personal_tags as $tag)
            {
                ?>
                <div class="sima-ui-dd-creator-type-body-item filter personal"
                     data-filter="<?php echo isset($tag->name)?$tag->name:''; ?>"
                     data-id="<?php echo isset($tag->id)?$tag->id:''; ?>"
                     data-alias="<?php echo isset($tag->name)?$tag->name:''; ?>"
                     data-filter_type="<?php echo 'tql_personal_filter'; ?>" 
                     data-attributes="show_unlisted">
                    <span class="sima-ui-dd-creator-type-body-item-text filter personal">
                        <?php
                            echo isset($tag->name)?$tag->name:'';
                        ?>
                    </span>
                </div>
                <?php
            }
        ?>
        <div class='clearfix'></div>
    </div>
</div>
<div class="sima-ui-dd-creator-filters">
    <div class="sima-ui-dd-creator-filters-head"><?php echo Yii::t('SIMADDCreator.Translation', 'Filters'); ?></div>
    <div class="sima-ui-dd-creator-filters-body">
        <?php
            foreach ($filters as $filter_key => $filter_value)
            {
                if (isset($filter_value['personality']) && $filter_value['personality'] == true)
                {
                    $limit_tag = TQLLimitTag::model()->findByAttributes(array('user_id'=>Yii::app()->user->id, 'name'=>$filter_key));
                }
                else
                {
                    $limit_tag = TQLLimitTag::model()->findByAttributes(array('name'=>$filter_key));
                }
                $attributes = '';
                $conditions = '';
                if (isset($filter_value['attributes']) && count($filter_value['attributes']) > 0)
                {
                    foreach ($filter_value['attributes'] as $attribute)
                    {
                        $attributes .= $attribute.";";
                    }
                    $attributes = rtrim($attributes, ";");
                }
                if (isset($filter_value['conditions']) && count($filter_value['conditions']) > 0)
                {
                    $conditions = '{';
                    foreach ($filter_value['conditions'] as $condition)
                    {
                        $tag = FileTag::model()->findByAttributes(array('name'=>$condition));
                        $conditions .= $tag->query.'$R'.",";
                    }
                    $conditions = substr($conditions, 0, -1).'}';
                }
                ?>
                <div class="sima-ui-dd-creator-type-body-item filter"
                     data-filter="<?php echo isset($filter_key)?$filter_key:''; ?>" 
                     data-alias="<?php echo isset($filter_value['alias'])?$filter_value['alias']:''; ?>" 
                     data-personality="<?php echo isset($filter_value['personality'])?$filter_value['personality']:''; ?>" 
                     data-filter_type="<?php echo isset($filter_value['type'])?$filter_value['type']:''; ?>" 
                     data-conditions="<?php echo isset($conditions)?$conditions:''; ?>" 
                     data-attributes="<?php echo isset($attributes)?$attributes:''; ?>">
                    <span class="sima-ui-dd-creator-type-body-item-text filter <?php echo ($limit_tag==null)?'disabled':''?>">
                        <?php
                            echo isset($filter_value['alias'])?$filter_value['alias']:'';
                        ?>
                    </span>
                </div>
                <?php
            }
        ?>
        <div class='clearfix'></div>
    </div>
</div>
<div class="sima-ui-dd-creator-types">
    <?php
        foreach ($types as $key => $value)
        {
            $attributes = '';
            $filter_attributes = '';
            $conditions = '';
            if (isset($value['attributes']) && count($value['attributes']) > 0)
            {
                foreach ($value['attributes'] as $attribute)
                {
                    $attributes .= $attribute.";";
                }
                $attributes = rtrim($attributes, ";");
            }
            if (isset($value['filter']['attributes']) && count($value['filter']['attributes']) > 0)
            {
                foreach ($value['filter']['attributes'] as $attribute)
                {
                    $filter_attributes .= $attribute.";";
                }
            }
            if (isset($value['conditions']) && count($value['conditions']) > 0)
            {
                $conditions = '{';
                foreach ($value['conditions'] as $condition)
                {
                    $tag = FileTag::model()->findByAttributes(array('name'=>$condition));
                    $conditions .= $tag->query.'$R'.",";
                }
                $conditions = substr($conditions, 0, -1).'}';
            }
            ?>
            <div class="sima-ui-dd-creator-type">
                <div class="sima-ui-dd-creator-type-header-wrapper">
                    <div class="sima-ui-dd-creator-type-header" 
                        data-alias="<?php echo isset($value['alias'])?$value['alias']:''; ?>" data-conditions="<?php echo isset($conditions)?$conditions:''; ?>" data-attributes="<?php echo isset($attributes)?$attributes:''; ?>" data-type='<?php echo isset($value['type'])?$value['type']:''; ?>' data-model='<?php echo isset($key)?$key:''; ?>'>
                        <span class="sima-ui-dd-creator-type-header-text <?php echo (isset($value['disabled'])&&$value['disabled'])?'disabled':''?>">
                            <?php
                                echo isset($value['alias'])?$value['alias']:'';
                            ?>
                        </span>
                    </div>
                    <?php if (isset($value['filter'])) {
                        $filter_table_name = $key::model()->tableName();
                        $tag = FileTag::model()->findByAttributes(array('name'=>$filter_table_name));
                        ?>
                        <div class="sima-ui-dd-creator-type-body-item filter"
                            data-filter="<?php echo isset($key)?$key:''; ?>" 
                            data-alias="<?php echo isset($value['filter']['alias'])?$value['filter']['alias']:''; ?>" 
                            data-personality="<?php echo isset($value['filter']['personality'])?$value['filter']['personality']:''; ?>" 
                            data-filter_type="<?php echo isset($value['filter']['type'])?$value['filter']['type']:''; ?>" 
                            data-attributes="<?php echo isset($filter_attributes)?$filter_attributes:''; ?>">
                            <span class="sima-ui-dd-creator-type-body-item-text filter <?php echo ($tag==null)?'disabled':''?>" style="margin-top: 3px;">
                               <?php
                                   echo isset($value['filter']['alias'])?$value['filter']['alias']:'';
                               ?>
                           </span>
                       </div>
                    <?php } ?>
                    <div class='clearfix'></div>
                </div>
                <div class="sima-ui-dd-creator-type-body">
                    <?php
                        if (isset($value['types']) && count($value['types']) > 0)
                        {
                            foreach ($value['types'] as $type_key => $type_value)
                            {
                                $type_attributes = '';
                                $type_conditions = '';
                                if (isset($type_value['attributes']) && count($type_value['attributes']) > 0)
                                {
                                    foreach ($type_value['attributes'] as $attribute_value)
                                    {
                                        $type_attributes .= $attribute_value.";";
                                    }
                                    $type_attributes = rtrim($type_attributes, ";");
                                }
                                if (isset($type_value['conditions']) && count($type_value['conditions']) > 0)
                                {
                                    $type_conditions = '{';
                                    foreach ($type_value['conditions'] as $type_condition)
                                    {
                                        $type_conditions .= $type_condition.",";
                                    }
                                    $type_conditions = substr($type_conditions, 0, -1).'}';
                                }
                                ?>
                                <div style="display:inline-block;" class="sima-ui-dd-creator-type-body-item" data-alias="<?php echo isset($type_value['alias'])?$type_value['alias']:''; ?>" data-conditions="<?php echo isset($type_conditions)?$type_conditions:''; ?>" data-attributes="<?php echo isset($type_attributes)?$type_attributes:''; ?>" data-model='<?php echo isset($key)?$key:''; ?>' data-type='<?php echo isset($type_value['type'])?$type_value['type']:''; ?>' data-type_column='<?php echo isset($type_key)?$type_key:''; ?>'>
                                    <?php
                                    if (isset($type_value['alias']))
                                    {
                                    ?>
                                        <span class="sima-ui-dd-creator-type-body-item-text">
                                            <?php
                                                echo $type_value['alias'];
                                            ?>
                                            <?php if (isset($type_value['filter_type']) && isset($type_value['filter_type'][0])) {?>
                                                <span data-filter_type="<?php echo $type_value['filter_type'][0]; ?>" data-filter_model="<?php echo isset($type_value['filter_type'][1])?$type_value['filter_type'][1]:''; ?>" class='sima-icon _16 add_individual_filter'></span>
                                            <?php } ?>
                                        </span>
                                        <?php
                                    }
                                        ?>
                                </div>
                                <?php
                            }
                        }
                    ?>
                    <div class='clearfix'></div>
                </div>
            </div>
    <?php
        }
    ?>
</div>