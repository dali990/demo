<?php

return [
    'SaveDefinition' => 'Sačuvaj definiciju',
    'ShowUnlisted' => 'Prikazi neizlistane',
    'NullName' => 'Podrazumevano ime foldera',
    'Recursive' => 'Rekurzivan',
    'SecurityFilters' => 'Bezbednosni filteri',
    'PersonalFilters' => 'Lični filteri',
    'Filters' => 'Filteri'
];