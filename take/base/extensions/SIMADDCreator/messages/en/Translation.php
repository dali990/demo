<?php

return [
    'SaveDefinition' => 'Save definition',
    'ShowUnlisted' => 'Show unlisted',
    'NullName' => 'Default folder name',
    'SecurityFilters' => 'Security filters',
    'PersonalFilters' => 'Personal filters'
];