/* global sima */

(function($){
    
    var methods = {
        init: function(params){
            var _this = this;
            
            var tree = _this.find(".sima-ui-dd-creator-tree");
            var types = _this.find('.sima-ui-dd-creator-types');
            

            this.data('tree_json', params.tree_json!==''?jQuery.parseJSON(params.tree_json):{});
            this.data('name', params.name);
            this.data('tree', tree);
            this.data('types', types);
            
            makeTagElementsDraggable(_this);
            makeTreeDroppable(_this);
            
            _this.on('click', '.sima-ui-dd-creator-tree-options .button',{tree:tree, localThis:_this}, saveDirectoryDefinitionHandler);
            _this.data('tree').on('click', '.sima-ui-dd-creator-tree-list .remove', {tree:tree, localThis:_this}, removeLi);
            _this.data('tree').on('click', '.sima-ui-dd-creator-tree-list .title', {tree:tree, localThis:_this}, setAttributes);
            _this.find('.sima-ui-dd-creator-tree-attributes input[type=checkbox]').on('click', {tree:tree, localThis:_this}, attributeCheckBoxChange);
            _this.find('.sima-ui-dd-creator-tree-attributes input[type=input]').on('input', {tree:tree, localThis:_this}, attributeInputChange);
            _this.find('.sima-ui-dd-creator-types .add_individual_filter').on('click', {tree:tree, localThis:_this}, individualFilter);

            if(typeof this.data('tree_json') !== 'undefined' 
                && this.data('tree_json') !== null
                && this.data('tree_json') !== '')
            {
                loadTree(_this);
            }
        },
        loadTree: function(params)
        {
            var _this = this;
            
            _this.data('tree_json', jQuery.parseJSON(params.tree_json));
            _this.data('name', params.tree_name);
            
            loadTree(_this);
        },
        enableTree: function()
        {
            var _this = this;
            enableTree(_this);
        },
        disableTree: function()
        {
            var _this = this;
            disableTree(_this);
        },
        refreshSecurityFilters: function(user_id)
        {
            var _this = this;
            sima.ajax.get('ddcreator/refreshSecurityFilters', {
                data: {
                    user_id: user_id
                },
                success_function: function(response) {
                    _this.find('.sima-ui-dd-creator-filters.security .sima-ui-dd-creator-filters-body').html(response.html);
                    makeTagElementsDraggable(_this);
                }
            });
        },
        refreshPersonalFilters: function(user_id)
        {
            var _this = this;
            sima.ajax.get('ddcreator/refreshPersonalFilters', {
                data: {
                    user_id: user_id
                },
                success_function: function(response) {
                    _this.find('.sima-ui-dd-creator-filters.personal .sima-ui-dd-creator-filters-body').html(response.html);
                    makeTagElementsDraggable(_this);
                }
            });
        }
    };
    
    function makeTagElementsDraggable(_this)
    {
        var tag_elemets = _this.find('.sima-ui-dd-creator-type-header-text:not(.disabled), .sima-ui-dd-creator-type-body-item-text:not(.disabled)');
        tag_elemets.draggable({
            delay: 200,
            revert: 'invalid',
            appendTo: _this,
            helper: 'clone',
            containment: 'window',
            scroll: false,
            start: function(e, ui) {
                $(ui.helper).addClass('sima-ui-dd-creator-drag');
            },
            stop: function(e, ui) {
                $(ui.helper).removeClass('sima-ui-dd-creator-drag');
            }
        });
    }
    
    //ukljucuje drop za celo stablo, tj ako se spusti element negde na povrsinu stabla(van postojecih elementa) 
    //on ga automatski dodaje u novi nivo
    function makeTreeDroppable(_this)
    {
        _this.data('tree').droppable({
            greedy: true,
            tolerance: "pointer",
            disabled: true,
            over: function(event, ui) {
                if (
//                        ui.draggable.parent().hasClass('directory_definition') 
//                        || 
                    $(ui.draggable).hasClass('sima-ui-dd-creator-type-header-text') 
                    || $(ui.draggable).hasClass('sima-ui-dd-creator-type-body-item-text') 
                    || $(ui.draggable).parent().hasClass('sima-ui-dd-creator-tree-list-item'))
                {
                    _this.find('.sima-ui-dd-creator-drop-hover').removeClass('sima-ui-dd-creator-drop-hover');
                    $(this).addClass('sima-ui-dd-creator-drop-hover');
                }
            },
            out: function(event, ui) {
                $(this).removeClass('sima-ui-dd-creator-drop-hover');
            },
            drop: function(event, ui) {
                //vraca false ako se pokazivac na definiciju vrti u krug
//                    if (
//                            ui.draggable.parent().hasClass('directory_definition') 
//                            && isDefinitionInCircle(ui.draggable.parent().data('id'), _this.find('.sima-ui-dd-creator-directory_definitions li.active').data('id')) === true)
//                    {
//                        var definition_drag = ui.draggable.text();
//                        var definition_drop = _this.find('.sima-ui-dd-creator-directory_definitions li.active .name').text();
//                        sima.dialog.openInfo('Definicija "'+definition_drag+'" ne moze da bude pokazivac u definiciji "'+definition_drop+'" jer se \n\
//                        u definiciji "'+definition_drag+'" ili njenoj deci nalazi vec definicija "'+definition_drop+'" kao pokazivac.');
//                        $(this).removeClass('sima-ui-dd-creator-drop-hover');
//                        return false;
//                    }

                var parent = _this.data('tree').find('.sima-ui-dd-creator-tree-list');

                var level_num = GetParentHighestLevelNum(parent);
                level_num++;
                var for_drop = createUlLevel(parent, level_num);
                parent.data('highest_level_num', level_num);

                var recheck_parent = true;

                if (ui.draggable.parent().hasClass('sima-ui-dd-creator-tree-list-item'))
                {
                    recheck_parent = false;
                    var old_parent = $(ui.draggable).parent().parent().parent().parent();
                    $(ui.draggable).parent().hide('slow', function(){
                        for_drop.append($(ui.draggable).parent().show('slow'));
                        recheckLevels(old_parent);
                        recheckLevels(parent);
                    });
                }
                else if (ui.draggable.hasClass('sima-ui-dd-creator-type-header-text'))
                {
                    recheck_parent = false;
                    createLi(for_drop, $(ui.draggable).parent());
                    _this.data('tree').find('.sima-ui-dd-creator-tree-list li:last').hide().show('slow');
                }
                else if (ui.draggable.hasClass('sima-ui-dd-creator-type-body-item-text'))
                {
                    createLi(for_drop, $(ui.draggable).parent());
                    _this.data('tree').find('.sima-ui-dd-creator-tree-list li:last').hide().show('slow');
                }
//                else if (ui.draggable.parent().hasClass('directory_definition'))
//                {
//                    createLi(for_drop, $(ui.draggable).parent());
//                    _this.data('tree').find('.sima-ui-dd-creator-tree-list li:last').hide().show('slow');
//                }
                if (
//                    ui.draggable.parent().hasClass('directory_definition') 
//                    || 
                    ui.draggable.parent().hasClass('sima-ui-dd-creator-tree-list-item') 
                    || ui.draggable.hasClass('sima-ui-dd-creator-type-header-text') 
                    || ui.draggable.hasClass('sima-ui-dd-creator-type-body-item-text'))
                {
                    enableTree(_this);
                    makeLiDraggable(_this);
                    makeLiDroppable(_this);
                    makeLevelDropable(_this);
                    $(this).removeClass('sima-ui-dd-creator-drop-hover');
                    onChangeTree(_this);
                }

                if(recheck_parent === true)
                {
                    recheckLevels(parent);
                }
            }
        });
    }
    
    function removeLi(event)
    {
        if(!event.data.localThis.data('tree').hasClass('ui-state-disabled'))
        {
            var local = $(this);
            local.parent().hide('slow', function(){
                var level_parent = local.parent().parent().parent().parent();
                local.parent().parent().parent().remove(); 
                if (event.data.tree.find('.sima-ui-dd-creator-tree-list').children().length === 0)
                {
//                    if (event.data.localThis.find('.sima-ui-dd-creator-directory_definitions li.active').length === 0)
//                    {
//                        disableTree(event.data.localThis);
//                    }
                }
                if (local.parent().children('.title.active').length !== 0)
                {
                    event.data.localThis.find('.sima-ui-dd-creator-tree-attributes').removeClass('active').hide();
                }
                recheckLevels(level_parent);
            });
            onChangeTree(event.data.localThis);
        }
    }
    
    function setAttributes(event)
    {
        var attributes = event.data.localThis.find('.sima-ui-dd-creator-tree-attributes');
        if ($(this).hasClass('active'))
        {
            $(this).removeClass('active');
            attributes.fadeOut('slow', function(){
                attributes.removeClass('active');
                sima.layout.allignObject(attributes.parents('.sima-ui-dd-creator-left-side:first'));
            });
        }
        else
        {
            if(!event.data.localThis.data('tree').hasClass('ui-state-disabled'))
            {
                event.data.tree.find('li .title.active').removeClass('active');
                $(this).addClass('active');
                attributes.removeClass('active').hide();
                loadAttributes(event.data.localThis, $(this).parent());
            }
        }
    }
    
    function saveDirectoryDefinitionHandler(event)
    {
        if(!event.data.localThis.data('tree').hasClass('ui-state-disabled'))
        {
            var treeObject = createTreeObjectForSave(event.data.localThis.find('.sima-ui-dd-creator-tree-list'));
            event.data.localThis.trigger('saveDefinition', [treeObject]);
        }
    }
    
    function attributeCheckBoxChange(event)
    {
        var li = event.data.tree.find('.sima-ui-dd-creator-tree-list-item .title.active').parent();   
        if ($(this).attr('type') === 'checkbox')
        {
            if ($(this).is(':checked'))
            {                
                li.data('attributes')[$(this).attr('id')]= 'true';
            }
            else
            {
                li.data('attributes')[$(this).attr('id')]= 'false';
            }
        }
        onChangeTree(event.data.localThis);
    }
    
    function attributeInputChange(event)
    {
        var li = event.data.tree.find('.sima-ui-dd-creator-tree-list-item .title.active').parent();
        if ($(this).attr('type') === 'input')
        {
            li.data('attributes')[$(this).attr('id')] = $(this).val();
        }
        onChangeTree(event.data.localThis);
    }
    
    function individualFilter(event)
    {
        var _this = event.data.localThis;
        var curr = $(this).parent().parent().clone();
        var type = $(this).data('filter_type');
        var model = $(this).data('filter_model');
        var yes_function = function(obj){
            var display_name;
            var value;
            if (typeof (obj) === 'string')
            {
                display_name = obj;
                value = obj;
            }
            else if (typeof (obj) === 'object')
            {
                display_name = obj.display_name;
                value = obj.id;
            }
            var data = curr.data();
            var name = curr.data('alias')+'('+display_name+')';
            var newAttributes = data.attributes;
            if (newAttributes.indexOf("null_name") === -1)
            {
                newAttributes += newAttributes + ';null_name';
            }
            curr.addClass('filter');
            curr.removeData();
            var newData = {};
            newData.filter = 'Individual';
            newData.alias = name;
            newData.attributes = newAttributes;
            newData.filter_type = 'individual';
            newData.value = value;
            newData.individual_model = data.model;
            newData.individual_column = data.type_column;
            newData.type = data.type;
            curr.data(newData);
            var parent = event.data.tree.find('ul:first');
            var level_num = GetParentHighestLevelNum(parent);
            var new_level_num = level_num+1;
            var level_ul = createUlLevel(parent, new_level_num);
            parent.data('highest_level_num', new_level_num);
            createLi(level_ul, curr, name);
            makeLiDraggable(_this);
            onChangeTree(_this);
        };
        if (type === 'searchModel')
        {
            sima.model.choose(model, yes_function);
        }
        else if (type === 'text')
        {
            var params = {};
            params.type = 'input';
            params.text = 'Unesite vrednost:';
            sima.dialog.openPrompt('Unesite vrednost',yes_function,params);
        }
    }
    
    function loadTree(localThis)
    {
        var tree_json = localThis.data('tree_json');
        var name = localThis.data('name');
        setCurrentDefinitionDisplayName(localThis, name);
        var tree = localThis.find('.sima-ui-dd-creator-tree-list').empty();
        tree.data('highest_level_num', 0);
        if(tree_json.length > 0 && tree_json[0].length > 0)
        {
            createTreeFromJson(tree, tree_json);
        }
        else if(!sima.isEmpty(tree_json.children)) /// privremen fix za definicije koje nisu htele da se prikazuju (npr nenadu markovicu nova mala, nova velika i stara nisu htele)
        {
            createTreeFromJson(tree, tree_json.children);
        }
        enableTree(localThis);
        localThis.find('.sima-ui-dd-creator-tree-attributes').removeClass('active').hide();
//                makeDirectoryDefinitionDraggable(localThis);
        makeLiDraggable(localThis);
        makeLiDroppable(localThis);
        makeLevelDropable(localThis);
    }
    
    function setCurrentDefinitionDisplayName(localThis, name)
    {
        localThis.find('.sima-ui-dd-creator-tree-options .directory_definition_name').val(name);
    }
    
    function createTreeFromJson(parent, children)
    {
        var count = 0;
        $.each(children, function(i, level) {
            count++;
            var level_ul = createUlLevel(parent, i+1);
            $.each(level, function(i, sub_item) {
                var li = createLiElement(sub_item);
                level_ul.append(li);
                if (sub_item.children !== undefined)
                {
                    createTreeFromJson(li, sub_item.children);
                    delete(sub_item.children);
                }
            });
       });
       
       parent.data('highest_level_num', count);
    }
    
    function disableTree(localThis)
    {
        setCurrentDefinitionDisplayName(localThis, '');
        localThis.find('.sima-ui-dd-creator-tree-list').empty();
        localThis.find('.sima-ui-dd-creator-tree-options .button').addClass('disabled');
        localThis.find('.sima-ui-dd-creator-tree').droppable( "option", "disabled", true );
        localThis.find('.sima-ui-dd-creator-tree-startup_message').show();
    }
    
    function enableTree(localThis)
    {
        localThis.find('.sima-ui-dd-creator-tree-startup_message').hide();
        localThis.find('.sima-ui-dd-creator-tree-options .button').removeClass('disabled');
        localThis.find('.sima-ui-dd-creator-tree').droppable( "option", "disabled", false );
    }
    
    //dodaje drag za elemente stabla
    function makeLiDraggable(_this)
    {
        _this.data('tree').find('li span.icon, li span.title').draggable({
            delay: 200,
            revert: 'invalid',
            appendTo: _this,
            helper: 'clone',
            containment: 'window',
            scroll: false,				
            start: function(e, ui) {
                $(ui.helper).addClass('sima-ui-dd-creator-drag');
            },
            stop: function(e, ui) {
                $(ui.helper).removeClass('sima-ui-dd-creator-drag');
            }
        });
    }
    
    //dodaje drop za elemente stabla
    function makeLiDroppable(_this)
    {
        _this.data('tree').find('.sima-ui-dd-creator-tree-list li span.title').droppable({
                greedy: true,
                tolerance: "pointer",
                over: function(event, ui) {
                    if (
//                        ui.draggable.parent().hasClass('directory_definition') 
//                        || 
                        $(ui.draggable).hasClass('sima-ui-dd-creator-type-header-text') 
                        || $(ui.draggable).hasClass('sima-ui-dd-creator-type-body-item-text') 
                        || $(ui.draggable).parent().hasClass('sima-ui-dd-creator-tree-list-item'))
                    {
                        if (!$(this).parent().hasClass('directory_definition'))
                        {
                            _this.find('.sima-ui-dd-creator-drop-hover').removeClass('sima-ui-dd-creator-drop-hover');
                            $(this).addClass('sima-ui-dd-creator-drop-hover');
                        }
                    }
                },
                out: function(event, ui) {
                    $(this).removeClass('sima-ui-dd-creator-drop-hover');
                },
                drop: function(event, ui) {
                    var actual_parent = $(this).parent().parent().parent();
                    if (checkIfDefinitionExist(actual_parent, ui.draggable.parent()))
                    {
                        $(this).removeClass('sima-ui-dd-creator-drop-hover');
                        sima.dialog.openInfo('Već postoji ova definicija!');
                        return false;
                    }
//                    //pokazivac na neku definiciju ne moze da bude droppable
//                    if ($(this).parent().hasClass('directory_definition'))
//                    {
//                        return false;
//                    }
//                    //vraca false ako se pokazivac na definiciju vrti u krug
//                    if (ui.draggable.parent().hasClass('directory_definition') 
//                            && isDefinitionInCircle(ui.draggable.parent().data('id'), _this.find('.sima-ui-dd-creator-directory_definitions li.active').data('id')) === true)
//                    {
//                        var definition_drag = ui.draggable.text();
//                        var definition_drop = _this.find('.sima-ui-dd-creator-directory_definitions li.active .name').text();
//                        sima.dialog.openInfo('Definicija "'+definition_drag+'" ne moze da bude pokazivac u definiciji "'+definition_drop+'" jer se \n\
//                        u definiciji "'+definition_drag+'" ili njenoj deci nalazi vec definicija "'+definition_drop+'" kao pokazivac.');
//                        $(this).removeClass('sima-ui-dd-creator-drop-hover');
//                        return false;
//                    }
                    
                    var parent = $(this).parent();
                    var level_num = GetParentHighestLevelNum(parent);
                    level_num++;
                    var for_drop = createUlLevel(parent, level_num);
                    parent.data('highest_level_num', level_num);
                    
                    var recheck_parent = true;
                    
                    if (ui.draggable.parent().hasClass('sima-ui-dd-creator-tree-list-item'))
                    {
                        recheck_parent = false;
                        var local = $(this);
                        var old_parent = $(ui.draggable).parent().parent().parent().parent();
                        $(ui.draggable).parent().hide('slow', function(){
                            if (for_drop.is(':visible') === false)
                            {
                                local.removeClass('sima-ui-dd-creator-drop-hover');
                                $(this).show();
                                return false;
                            }
                            else
                            {
                                for_drop.append($(ui.draggable).parent().show('slow'));
                                recheckLevels(old_parent);
                            }
                        });
                    }
                    else if (ui.draggable.hasClass('sima-ui-dd-creator-type-header-text'))
                    {
                        createLi(for_drop, $(ui.draggable).parent());
                        $(this).parent().find('ul:first li:last').hide().show('slow');
                    }
                    else if (ui.draggable.hasClass('sima-ui-dd-creator-type-body-item-text'))
                    {
                        createLi(for_drop, $(ui.draggable).parent());
                        $(this).parent().find('ul:first li:last').hide().show('slow');
                    }
//                    else if (ui.draggable.parent().hasClass('directory_definition'))
//                    {
//                        if (isDefinitionInCircle(ui.draggable.parent().data('id'), _this.find('.sima-ui-dd-creator-directory_definitions li.active').data('id')) === false)
//                        {
//                            createLi(for_drop, $(ui.draggable).parent());
//                            $(this).parent().find('ul:first li:last').hide().show('slow');
//                        }
//                    }
                    
                    if (
//                        ui.draggable.parent().hasClass('directory_definition') 
//                        || 
                        ui.draggable.parent().hasClass('sima-ui-dd-creator-tree-list-item') 
                        || ui.draggable.hasClass('sima-ui-dd-creator-type-header-text') 
                        || ui.draggable.hasClass('sima-ui-dd-creator-type-body-item-text'))
                    {
                        makeLiDraggable(_this);
                        makeLiDroppable(_this);
                        makeLevelDropable(_this);
                        $(this).removeClass('sima-ui-dd-creator-drop-hover');
                        onChangeTree(_this);
                    }
                    
                    if(recheck_parent)
                    {
                        recheckLevels(parent);
                    }
                }
        });
    }
    
    function makeLevelDropable(_this)
    {
        _this.data('tree').find('.sima-ui-dd-creator-tree-list-level-name').droppable({
            greedy: true,
            tolerance: "pointer",
            over: function(event, ui) {
                if (
//                    ui.draggable.parent().hasClass('directory_definition') 
//                    || 
                    $(ui.draggable).hasClass('sima-ui-dd-creator-type-header-text') 
                    || $(ui.draggable).hasClass('sima-ui-dd-creator-type-body-item-text') 
                    || $(ui.draggable).parent().hasClass('sima-ui-dd-creator-tree-list-item'))
                {
                    if (!$(this).parent().hasClass('directory_definition'))
                    {
                        _this.find('.sima-ui-dd-creator-drop-hover').removeClass('sima-ui-dd-creator-drop-hover');
                        $(this).addClass('sima-ui-dd-creator-drop-hover');
                    }
                }
            },
            out: function(event, ui) {
                $(this).removeClass('sima-ui-dd-creator-drop-hover');
            },
            drop: function(event, ui) {                
                var parent = $(this).parent().parent();
                var for_drop = $(this).siblings("ul");
                
                var recheck_parent = true;

                if (ui.draggable.parent().hasClass('sima-ui-dd-creator-tree-list-item'))
                {
                    recheck_parent = false;
                    var local = $(this);
                    var old_parent = $(ui.draggable).parent().parent().parent().parent();
                    $(ui.draggable).parent().hide('slow', function(){
                        if (for_drop.is(':visible') === false)
                        {
                            local.removeClass('sima-ui-dd-creator-drop-hover');
                            $(this).show();
                            return false;
                        }
                        else
                        {
                            for_drop.append($(ui.draggable).parent().show('slow'));
                            recheckLevels(old_parent);
                        }
                    });
                }
                else if (ui.draggable.hasClass('sima-ui-dd-creator-type-header-text'))
                {
                    createLi(for_drop, $(ui.draggable).parent());
                    $(this).parent().find('ul:first li:last').hide().show('slow');
                }
                else if (ui.draggable.hasClass('sima-ui-dd-creator-type-body-item-text'))
                {
                    createLi(for_drop, $(ui.draggable).parent());
                    $(this).parent().find('ul:first li:last').hide().show('slow');
                }
//                else if (ui.draggable.parent().hasClass('directory_definition'))
//                {
//                    if (isDefinitionInCircle(ui.draggable.parent().data('id'), _this.find('.sima-ui-dd-creator-directory_definitions li.active').data('id')) === false)
//                    {
//                        createLi(for_drop, $(ui.draggable).parent());
//                        $(this).parent().find('ul:first li:last').hide().show('slow');
//                    }
//                }

                if (
//                    ui.draggable.parent().hasClass('directory_definition') 
//                    || 
                    ui.draggable.parent().hasClass('sima-ui-dd-creator-tree-list-item') 
                    || ui.draggable.hasClass('sima-ui-dd-creator-type-header-text') 
                    || ui.draggable.hasClass('sima-ui-dd-creator-type-body-item-text'))
                {
                    makeLiDraggable(_this);
                    makeLiDroppable(_this);
                    makeLevelDropable(_this);
                    $(this).removeClass('sima-ui-dd-creator-drop-hover');
                    onChangeTree(_this);
                }

                if(recheck_parent)
                {
                    recheckLevels(parent);
                }
            }
        });
    }
    
    function createUlLevel(parent, num)
    {
        var name = num;
        var div = $("<div class='sima-ui-dd-creator-tree-list-level-div'></div>");
        div.data('level_num', num);
        
        var span = $("<span class='sima-ui-dd-creator-tree-list-level-name'>"+name+"</span>");
        var ul = $("<ul class='sima-ui-dd-creator-tree-list-level-ul'>"
                    +"</ul>");
            
        div.append(span);
        div.append(ul);

        parent.append(div);

        return ul;
    }
    
    function createLiElement(data)
    {
        var name = '';
        var icon = 'icon';
        if (data.name !== undefined)
        {
            name = data.name;
            icon = 'icon directory_definition';
        }
        else if (data.alias !== undefined)
        {
            name = data.alias;
        }
        var li = $("<li class='sima-ui-dd-creator-tree-list-item "+data.clas+"'><span class='"+icon+" closed'></span><span class='title'>"+name+"</span><span class='remove'></span></li>");
        li.data(data);
        
        if (data.definition_id !== undefined)
        {
            li.data('id', data.definition_id);
        }

        return li;
    }
    
    function createLi(parent, obj, name)
    {
        var data = jQuery.extend({}, obj.data());
        var clas = '';
        var text = obj.data('alias');
        var icon = 'icon';
        if (obj.data('attributes') !== undefined)
        {
            var attributes = obj.data('attributes').toString().split(";");
            data.attributes = {};
            for (var i=0; i<attributes.length; i++)
            {
                if (attributes[i] === 'min_file_listed')
                    data.attributes[attributes[i]] = "0";
                else if (attributes[i] === 'null_name')
                    data.attributes[attributes[i]] = "";
                else if (attributes[i] === 'tag_group_tree_view')
                    data.attributes[attributes[i]] = 'false';
                else if (attributes[i] === 'show_unlisted')
                {
                    data.attributes[attributes[i]] = 'false';
                }
                else
                    data.attributes[attributes[i]] = 'true';
            }
        }
        if (obj.hasClass('filter'))
        {
            if (obj.hasClass('personal'))
            {
                clas = "personal";
            }
            else if (obj.hasClass('security'))
            {
                clas = "security";
            }
            else
            {
                clas = "filter";
            }
            data.attributes['skip'] = false;
        }
        else if (obj.hasClass('sima-ui-dd-creator-type-header'))
        {
            clas = "group_table";
        }
        else if (obj.hasClass('sima-ui-dd-creator-type-body-item'))
        {
            clas = "group_attribute";
        }
//        else if (obj.hasClass('directory_definition'))
//        {
//            text = $.trim(obj.find('span.name').text());
//            clas = "directory_definition";
//            icon = 'icon directory_definition';
//            data.name = text;
//            data.definition_id = obj.data('id');
//        }
        if (typeof name !== 'undefined')
        {
            text = name;
        }
        data.clas = clas;
        var li = $("<li class='sima-ui-dd-creator-tree-list-item "+clas+"'><span class='"+icon+" closed'></span><span class='title'>"+text+"</span><span class='remove'></span></li>");
        li.data(data);
        var actual_parent = parent.parent().parent();
        if (!checkIfDefinitionExist(actual_parent, li))
            parent.append(li);
        else
        {
            sima.dialog.openInfo('Već postoji ova definicija!');
            recheckLevels(actual_parent);
        }
    }
    
    function checkIfDefinitionExist(parent, def)
    {
        var found = false;
        parent.children("div").each(function(){
            $(this).children("ul").children("li").each(function(){
                if ($(this).hasClass('directory_definition'))
                {
                    if ($(this).data('id') === def.data('id'))
                    {
                        found = true;
                        return false;
                    }
                }
                else
                {
                    if (
                            $(this).data('model')===def.data('model')
                            && $(this).data('alias')===def.data('alias')
                            && $(this).data('type')===def.data('type')
                            && $(this).data('clas')===def.data('clas')
                       )
                    {
                        found = true;
                        return false;
                    }
                }
            });
        });
        
        return found;
    }
    
    function createTreeObjectForSave(tree)
    {
        var treeObject = [];
        var count_level = 0;
        tree.children("div").each(function(){
            count_level++;
            var level = [];
            var count_li = 0;
            $(this).children("ul").children("li").each(function(){
                count_li++;
                var temp = [];
                temp = $(this).data();
                temp.children = createTreeObjectForSave($(this));
                level.push(temp);
            });
            
            if(!sima.isEmpty(level))
            {
                treeObject.push(level);
            }
        });
                
        return treeObject;
    }
    
    function loadAttributes(localThis, li)
    {
        var attributes = localThis.find('.sima-ui-dd-creator-tree-attributes');
        var data = li.data('attributes');

        if (data !== undefined && data.length !== 0)
        {
            if (!attributes.hasClass('active'))
            {
                attributes.fadeIn('slow', function(){
                    attributes.addClass('active');
                    sima.layout.allignObject(attributes.parents('.sima-ui-dd-creator-left-side:first'));
                });
            }

            attributes.find('input').each(function(){
                $(this).parent().css({'border-right':'1px solid black'});
                $(this).parent().hide();
            });
            $.each(data, function(key, value) {
                attributes.find('#'+key).parent().show();
                if (attributes.find('#'+key).attr('type') === 'checkbox')
                {
                    if (value === 'true')
                    {
                        attributes.find('#'+key).prop('checked', true);
                    }
                    else if (value === 'false')
                    {
                        attributes.find('#'+key).prop('checked', false);
                    }
                }
                else if (attributes.find('#'+key).attr('type') === 'input')
                {
                    attributes.find('#'+key).val(value);
                }
            });
            attributes.find('.attribute_item:visible:last').css({'border-right':'none'});
        }
    }

    /**
     * uklanja nepotrebne nivoe sa pocetka
     * @param {type} parent
     * @returns {undefined}
     */
    function recheckLevels(parent)
    {
        var had_intervention = false;

        var parent_count = 0;
        $.each(parent.children("div"), function(i, item) {
            parent_count++;
            var itemObj = $(item);
            var ul = itemObj.children('ul');
            var ul_children = ul.children();
            var len = ul_children.size();
            
            if(len === 0)
            {
                itemObj.remove();

                if(had_intervention === false)
                {
                    had_intervention = true;
                }
            }
        });
        
        if(had_intervention === true)
        {
            var count = 0;
            $.each(parent.children("div"), function(i, item) {
                count++;
                var itemObj = $(item);
                itemObj.data('level_num', count);
                itemObj.children("span").text(count);
            });
            
            parent.data('highest_level_num', count);
        }
        else
        {
            parent.data('highest_level_num', parent_count);
        }
    }
    
    function GetParentHighestLevelNum(parent)
    {
        var level_num = 0;
        if(typeof parent.data('highest_level_num') !== 'undefined')
        {
            level_num = parent.data('highest_level_num');
        }
        return level_num;
    }
    
    function onChangeTree(localThis)
    {
        localThis.trigger('changeTree');
    }
    
    $.fn.ddCreator = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.ddCreator');
            }
        });
        return ret;
    };
    
})( jQuery );