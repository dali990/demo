<?php

/**
 * widget smatra da su svi elementi iste sirine i visine
 * 
 * primer upotrebe:
    $this->widget('SIMAPartialDisplay', [
        'items' => [
            [
                'priority' => 1,
                'html' => '11111111111111111'
            ],
            [
                'priority' => 2,
                'html' => '22222222222222222'
            ],
            [
                'priority' => 3,
                'html' => '33333333333333333'
            ],
            [
                'priority' => 4,
                'html' => '4444444444444444'
            ],
            [
                'priority' => 5,
                'html' => '5555555555555555'
            ]
        ]
    ]);
 */

class SIMAPartialDisplay extends CWidget 
{
    public $id = null;
    public $number_of_rows = 1;
    public $items = []; /// niz kome je svaki element oblika ['priority'=>int, 'html'=>str]
    
    public function run() 
    {
        if(empty($this->id))
        {
            $this->id = SIMAHtml::uniqid().'_spd';
        }
        
        if(empty($this->items))
        {
            throw new Exception('empty items');
        }
        
        echo $this->render('index', [
            'id' => $this->id,
            'items' => $this->items
        ]);
        
        $params = [
            'number_of_rows' => $this->number_of_rows
        ];
        $json = CJSON::encode($params);
        $register_script = "$('#" . $this->id . "').partialDisplay('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
    
    public static function registerManual() 
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl .'/jquery.partialDisplay.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($baseUrl .'/simaPartialDisplay.js' , CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/simaPartialDisplay.css');
    }
}