<div id="<?=$id;?>" class="sima-partial-display sima-layout-panel">
    <?php
    foreach($items as $item)
    {
    ?>
        <div class="sima-partial-display-element" data-sima-partial-display='<?=CJSON::encode([
                'priority' => $item['priority']
            ])?>'>
            <?=$item['html']?>
        </div>
    <?php
    }
    ?>
    <div class="sima-partial-display-three-dots">
    <?php
        $this->widget('SIMASpanWithInfoBox', [
            'span_text' => '...',
            'info_box_classes' => 'sima-partial-display-info-box'
        ]);
    ?>
    </div>
</div>
