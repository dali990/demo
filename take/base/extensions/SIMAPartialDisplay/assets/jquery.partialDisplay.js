/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global sima */

(function($){
    var methods = {
        init: function(params){
            var localThis = this;
            
            localThis.data('number_of_rows', params.number_of_rows);
            
            var three_dots = localThis.children('.sima-partial-display-three-dots');
            three_dots.hide();
        },
        realign_children: function(width)
        {
            var localThis = this;
            
            var three_dots = localThis.children('.sima-partial-display-three-dots');
            three_dots.hide();
            
            var children = localThis.children('.sima-partial-display-element');
            $.each(children, function(index, child){
                var childObj = $(child);
                childObj.show();
            });
            
            var number_of_rows = localThis.data('number_of_rows');
                        
            if(number_of_rows > 1)
            {
                multirowRealign(localThis, width);
            }
            else
            {
                singlerowRealign(localThis, width);
            }
            
            var info_box_html = '';
            children.each(function(index, child){
                var childObj = $(child);
                if(childObj.is(':visible') === false)
                {
                    info_box_html += childObj.html()+'</br>';
                }
            });
            if(!sima.isEmpty(info_box_html))
            {
                var three_dots = localThis.children('.sima-partial-display-three-dots');
                var three_dots_span = three_dots.children('span');
                three_dots_span.spanWithInfoBox('set_info_box_html', info_box_html);
                three_dots.show();
            }
        }
    };
    
    function singlerowRealign(localThis, width)
    {
        var children = localThis.children('.sima-partial-display-element');
        var three_dots = localThis.children('.sima-partial-display-three-dots');
        var three_dots_width = three_dots.outerWidth(true);

        var children_width = 0;
        $.each(children, function(index, child){
            var childObj = $(child);
            children_width += childObj.outerWidth(true);
        });
        
        if(children_width>width)
        {
            var number_of_elements_to_hide = Math.ceil((children_width+three_dots_width-width)/$(children[0]).outerWidth(true));
            hideNElements(localThis, number_of_elements_to_hide);
        }
    };
    
    function multirowRealign(localThis, width)
    {
        var number_of_rows = localThis.data('number_of_rows');
        
        var children = localThis.children('.sima-partial-display-element');

        var number_of_elements_in_row = 0;
        children.each(function() {
            if($(this).prev().length > 0) {
                if($(this).position().top !== $(this).prev().position().top) return false;
                number_of_elements_in_row++;
            }
            else {
                number_of_elements_in_row++;   
            }
        });
        var number_of_elements_in_last_row = children.length%number_of_elements_in_row;
        if(number_of_elements_in_last_row === 0)
        {
            number_of_elements_in_last_row = number_of_elements_in_row;
        }
        var number_of_rows_elements_take = Math.ceil(children.length/number_of_elements_in_row);
        
        if(number_of_rows_elements_take > number_of_rows)
        {
            /// hide all elements in extra rows
                /// hide in last row
                /// hide in rows before last
            /// hide last element in visible row to make room for three dots, if needed

            var number_of_extra_rows = number_of_rows_elements_take-number_of_rows;
            
            hideNElements(localThis, number_of_elements_in_last_row);
            
            if(number_of_extra_rows>1)
            {
                var number_of_elements_to_hide = (number_of_extra_rows-1)*number_of_elements_in_row;
                hideNElements(localThis, number_of_elements_to_hide);
            }
            
            var children_row_width = number_of_elements_in_row * $(children[0]).outerWidth(true);
            var three_dots_width = localThis.children('.sima-partial-display-three-dots').outerWidth(true);
            if(children_row_width+three_dots_width >= width)
            {
                hideNElements(localThis, 1);
            }
        }
    };
    
    function getNextVisibleElementWithLowestPriorityToHide(localThis)
    {        
        var child_obj_to_hide = null;
        var cur_min_priority = -1;
        localThis.children('.sima-partial-display-element').each(function(index, child){
            var childObj = $(child);
            if(childObj.is(':visible'))
            {
                var child_priority = childObj.data('sima-partial-display').priority;

                if(index === -1 || cur_min_priority === -1 || cur_min_priority > child_priority)
                {
                    cur_min_priority = child_priority;
                    child_obj_to_hide = childObj;
                }
            }
        });
        
        return child_obj_to_hide;
    }
    
    function hideNElements(localThis, n)
    {
        for(var i=0; i<n; i++)
        {
            var child_obj_to_hide = getNextVisibleElementWithLowestPriorityToHide(localThis);
            
            if(sima.isEmpty(child_obj_to_hide))
            {
                break;
            }

            child_obj_to_hide.hide();
        }
    }
    
    $.fn.partialDisplay = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.partialDisplay');
            }
        });
        return ret;
    };
})( jQuery );
