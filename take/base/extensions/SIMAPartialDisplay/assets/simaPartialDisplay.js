
/* global sima */

$('body').on('sima-layout-allign', '.sima-partial-display', function(event, height, width, source){
    if (event.target === this)
    {
        event.stopPropagation();
        
        if (sima.layout.log())
        {
            console.log(source);
            console.log('HANDLER_sima_partial_display');
        }
        
        var localThis = $(this);
        localThis.css('height', height+'px');
        localThis.css('width', width+'px');
        
        localThis.partialDisplay('realign_children', width);
    }
});