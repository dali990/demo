<?php

return [
    'Current page must be a positive number!' => 'Trenutna strana mora biti pozitivan broj!',
    'Current page can not be empty!' => 'Trenutna strana ne može biti prazna!',
    'Current page can not be less than 1!' => 'Trenutna strana ne može biti manja od 1!',
    'Current page can not be larger than the total number of pages!' => 'Trenutna strana ne može biti veća od ukupnog broja strana!',
    'Current page' => 'Trenutna strana',
    'First page' => 'Prva strana',
    'Previous page' => 'Prethodna strana',
    'Next page' => 'Sledeća strana',
    'Last page' => 'Poslednja strana',
    'Total number of items' => 'Ukupan broj stavki',
    'Total number of pages' => 'Ukupan broj strana'
];

