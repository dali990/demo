
/* global sima */

(function($){
    
    var methods = {
        init: function(params) {
            
            var _this = $(this);
            _this.data('translations', params.translations);
            _this.data('curr_page_num', params.curr_page_num);
            _this.data('total_page_num', params.total_page_num);
            _this.data('page_size', params.page_size);
            _this.data('curr_page_num_input', _this.find('.sima-pagination-curr-page-num'));
            _this.data('total_page_num_input', _this.find('.sima-pagination-total-page-num'));
            
            enableDisableButtons(_this);            
            
            _this.data('curr_page_num_input').keyup(function(e){
                if(e.keyCode === 13)//enter
                {
                    var set_curr_page_to_default = false;
                    var curr_page_num = $.trim(_this.data('curr_page_num_input').val());
                    var total_page_num = parseInt(_this.data('total_page_num_input').val()); 
                    if (/^[0-9]+$/.test(curr_page_num) === false)
                    {
                        set_curr_page_to_default = true;
                        sima.dialog.openWarn(getTranslation(_this, 'CurrentPageMustBePositiveNumber'));
                    }
                    else
                    {
                        curr_page_num = parseInt(curr_page_num);                       
                        if (curr_page_num === '')
                        {
                            set_curr_page_to_default = true;
                            sima.dialog.openWarn(getTranslation(_this, 'CurrentPageCanNotBeEmpty'));
                        }
                        else if (curr_page_num < 1)
                        {
                            set_curr_page_to_default = true;
                            sima.dialog.openWarn(getTranslation(_this, 'CurrentPageCanNotBeLessThan1'));
                        }
                        else if (curr_page_num > total_page_num)
                        {
                            set_curr_page_to_default = true;
                            sima.dialog.openWarn(getTranslation(_this, 'CurrentPageCanNotBeLargerThanTheTotalNumberOfPages'));
                        }
                    }
                    if (set_curr_page_to_default === true)
                    {
                        var curr_page_num = 1;
                        if (total_page_num === 0)
                        {
                            curr_page_num = 0;
                        }
                        _this.data('curr_page_num', curr_page_num);
                        _this.data('curr_page_num_input').val(curr_page_num);
                    }
                    else
                    {
                        _this.data('curr_page_num', curr_page_num);
                        checkPageNumbers(_this);
                        enableDisableButtons(_this);
                        
                        var limit_offset_num = getLimitAndOffset(_this);
                        _this.trigger('pagin_event', [{limit:limit_offset_num.limit, offset:limit_offset_num.offset, pagin_event_type:'enter'}]);
                    }
                }
            });
        },
        onFirst: function(a) {            
            var _this = $(this);
            
            _this.data('curr_page_num', 1);
            _this.data('curr_page_num_input').val(1);
            
            checkPageNumbers(_this);
            enableDisableButtons(_this);
            
            var limit_offset_num = getLimitAndOffset(_this);
            _this.trigger('pagin_event', [{limit:limit_offset_num.limit, offset:limit_offset_num.offset, pagin_event_type:'first'}]);
        },
        onPrevious: function() {            
            var _this = $(this);
            
            var new_value = parseInt(_this.data('curr_page_num'))-1;
            _this.data('curr_page_num', new_value);
            _this.data('curr_page_num_input').val(new_value);
            
            checkPageNumbers(_this);
            enableDisableButtons(_this);            
            
            var limit_offset_num = getLimitAndOffset(_this);
            _this.trigger('pagin_event', [{limit:limit_offset_num.limit, offset:limit_offset_num.offset, pagin_event_type:'previous'}]);
        },
        onNext: function() {
            var _this = $(this);
            
            var new_value = parseInt(_this.data('curr_page_num'))+1;
            _this.data('curr_page_num', new_value);
            _this.data('curr_page_num_input').val(new_value);
            
            checkPageNumbers(_this);
            enableDisableButtons(_this);

            var limit_offset_num = getLimitAndOffset(_this);  
            _this.trigger('pagin_event', [{limit:limit_offset_num.limit, offset:limit_offset_num.offset, pagin_event_type:'next'}]);
        },
        onLast: function() {            
            var _this = $(this);
            
            var new_value = parseInt(_this.data('total_page_num'));
            _this.data('curr_page_num', new_value);
            _this.data('curr_page_num_input').val(new_value);
            
            checkPageNumbers(_this);
            enableDisableButtons(_this);
            
            var limit_offset_num = getLimitAndOffset(_this);
            _this.trigger('pagin_event', [{limit:limit_offset_num.limit, offset:limit_offset_num.offset, pagin_event_type:'last'}]);
        },
        getCurrPageNum: function() {
            var _this = $(this);            
            return _this.data('curr_page_num');
        },
        setCurrPageNum: function(value) {
            var _this = $(this);
            _this.data('curr_page_num', value);
            _this.data('curr_page_num_input').val(value);
        },
        getTotalPageNum: function() {
            var _this = $(this);
            return _this.data('total_page_num');
        },
        setTotalPageNum: function(value) {
            var _this = $(this);
            _this.data('total_page_num', value);
            _this.data('total_page_num_input').val(value);
        },
        resetPagination: function(totalDataCount, offset) {            
            var _this = $(this);
            totalDataCount = parseInt(totalDataCount);
            var total_page_num = (totalDataCount === 0) ? 0 : Math.ceil(totalDataCount / _this.data('page_size'));
            var curr_page_num = 1;
            if (typeof offset !== 'undefined')
            {                
                curr_page_num = getCurrPageNumberByOffset(_this, offset);
            }
            if (total_page_num === 0)
            {
                curr_page_num = 0;
            }
            _this.data('curr_page_num', curr_page_num);
            _this.data('curr_page_num_input').val(curr_page_num);
            _this.data('total_page_num', total_page_num);
            _this.data('total_page_num_input').val(total_page_num);
            checkPageNumbers(_this);
            enableDisableButtons(_this);
        },
        getLimitAndOffset: function()
        {
            return getLimitAndOffset($(this));
        }
    };
    
    function getLimitAndOffset(_this)
    {
        var curr_page_num = _this.data('curr_page_num');
        var limit = _this.data('page_size');
        var offset = (curr_page_num - 1) * limit;
        
        return {
            limit:limit,
            offset:offset
        };
    }
    
    function getCurrPageNumberByOffset(_this, offset)
    {        
        return Math.ceil(offset / _this.data('page_size')) + 1;        
    }
    
    function enableDisableButtons(_this)
    {
        var curr_page_num = parseInt(_this.data('curr_page_num'));
        var total_page_num = parseInt(_this.data('total_page_num'));
        
        sima.vue.objRemoveClass(_this.find('.sima-pagination-first'), '_disabled');
        sima.vue.objRemoveClass(_this.find('.sima-pagination-previous'), '_disabled');
        sima.vue.objRemoveClass(_this.find('.sima-pagination-next'), '_disabled');
        sima.vue.objRemoveClass(_this.find('.sima-pagination-last'), '_disabled');
        
        if (curr_page_num <= 1)
        {
            sima.vue.objAddClass(_this.find('.sima-pagination-first'), '_disabled');
            sima.vue.objAddClass(_this.find('.sima-pagination-previous'), '_disabled');
        }
        if (curr_page_num === total_page_num)
        {
            sima.vue.objAddClass(_this.find('.sima-pagination-next'), '_disabled');
            sima.vue.objAddClass(_this.find('.sima-pagination-last'), '_disabled');
        }
    }
    
    function checkPageNumbers(_this)
    {
        var curr_page_num = parseInt(_this.data('curr_page_num'));
        var total_page_num = parseInt(_this.data('total_page_num'));
        
        if (curr_page_num > total_page_num)
        {
            _this.data('curr_page_num', total_page_num);
            _this.data('curr_page_num_input').val(total_page_num);
            sima.dialog.openWarn(getTranslation(_this, 'CurrentPageCanNotBeLargerThanTheTotalNumberOfPages'));
        }
    }
    
    function getTranslation(_this, code)
    {
        var translations = _this.data('translations');
        
        return (!sima.isEmpty(translations[code])) ? translations[code] : code;
    }
    
    $.fn.simaPagination = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.jquery.simaPagination');
            }
        });
        return ret;
    };
})(jQuery);