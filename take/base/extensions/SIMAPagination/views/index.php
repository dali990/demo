
<div id='<?php echo $this->id; ?>' class="sima-pagination <?php echo $this->class; ?>" <?php $html_options; ?>>
    <?php
    if(SIMAMisc::isVueComponentEnabled())
    {
        $this->widget(SIMAButtonVue::class,[
            'half' => true,
            'icon' => '_start-arrow',
            'iconsize' => 18,
            'additional_classes' => ['sima-pagination-first'],
            'tooltip'=>Yii::t('SIMAPagination.Pagination', 'First page'),
            'onclick'=>["sima.callPluginMethod", "#{$this->id}", "simaPagination", "onFirst"]
        ]);
        $this->widget(SIMAButtonVue::class,[
            'half' => true,
            'icon' => '_left-arrow',
            'iconsize' => 18,
            'additional_classes' => ['sima-pagination-previous'],
            'tooltip'=>Yii::t('SIMAPagination.Pagination', 'Previous page'),
            'onclick'=>["sima.callPluginMethod", "#{$this->id}", "simaPagination", "onPrevious"]
        ]);
    }
    else
    {
        $this->widget('base.extensions.SIMAButton.SIMAButton',[
            'class'=>'_half sima-pagination-first',
            'action'=>[                
                'icon'=>'sima-icon _start-arrow _18',
                'tooltip'=>Yii::t('SIMAPagination.Pagination', 'First page'),
                'onclick'=>["sima.callPluginMethod", "#{$this->id}", "simaPagination", "onFirst"]
            ]
        ]);
        $this->widget('base.extensions.SIMAButton.SIMAButton',[
            'class'=>'_half sima-pagination-previous',
            'action'=>[
                'icon'=>'sima-icon _left-arrow _18',
                'tooltip'=>Yii::t('SIMAPagination.Pagination', 'Previous page'),
                'onclick'=>["sima.callPluginMethod", "#{$this->id}", "simaPagination", "onPrevious"]
            ]
        ]);
    }
    ?>
    <div class="sima-pagination-page-num-wrap">
        <input class="sima-pagination-curr-page-num" title="<?php echo Yii::t('SIMAPagination.Pagination', 'Current page'); ?>" value="<?php echo $this->currPageNum; ?>" />
        <span class="sima-pagination-page-num-separator">/</span>
        <input class="sima-pagination-total-page-num" title="<?php echo Yii::t('SIMAPagination.Pagination', 'Total number of pages'); ?>" value="<?php echo $total_page_num; ?>" readonly />
    </div>
    <?php
    if(SIMAMisc::isVueComponentEnabled())
    {
        $this->widget(SIMAButtonVue::class,[
            'half' => true,
            'icon' => '_right-arrow',
            'iconsize' => 18,
            'additional_classes' => ['sima-pagination-next'],
            'tooltip'=>Yii::t('SIMAPagination.Pagination', 'Next page'),
            'onclick'=>["sima.callPluginMethod", "#{$this->id}", "simaPagination", "onNext"]
        ]);
        $this->widget(SIMAButtonVue::class,[
            'half' => true,
            'icon' => '_end-arrow',
            'iconsize' => 18,
            'additional_classes' => ['sima-pagination-last'],
            'tooltip'=>Yii::t('SIMAPagination.Pagination', 'Last page'),
            'onclick'=>["sima.callPluginMethod", "#{$this->id}", "simaPagination", "onLast"]
        ]);
    }
    else
    {
        $this->widget('base.extensions.SIMAButton.SIMAButton',[
            'class'=>'_half sima-pagination-next',
            'action'=>[                
                'icon'=>'sima-icon _right-arrow _18',
                'tooltip'=>Yii::t('SIMAPagination.Pagination', 'Next page'),
                'onclick'=>["sima.callPluginMethod", "#{$this->id}", "simaPagination", "onNext"]
            ]
        ]);
        $this->widget('base.extensions.SIMAButton.SIMAButton',[
            'class'=>'_half sima-pagination-last',
            'action'=>[                
                'icon'=>'sima-icon _end-arrow _18',
                'tooltip'=>Yii::t('SIMAPagination.Pagination', 'Last page'),
                'onclick'=>["sima.callPluginMethod", "#{$this->id}", "simaPagination", "onLast"]
            ]
        ]);
    }
    ?>
    <div class="sima-pagination-total-items-num" title="<?php echo Yii::t('SIMAPagination.Pagination', 'Total number of items'); ?>">
        
    </div>
</div>