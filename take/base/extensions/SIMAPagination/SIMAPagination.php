<?php

class SIMAPagination extends CWidget 
{
    public $id = null;
    public $class = '';    
    public $htmlOptions = [];
    public $currPageNum = 1;
    public $pageSize = null;
    public $totalDataCount = 0;
    
    const pageSize = 50;

    public function run() 
    {
        if(empty($this->id))
        {
            $this->id = SIMAHtml::uniqid();
        }
        if (empty($this->pageSize))
        {
            $this->pageSize = SIMAPagination::pageSize;
        }
        $html_options = '';
        foreach ($this->htmlOptions as $option_key=>$option_value)
        {
            $html_options .= " $option_key='$option_value'";
        }
        
        $total_page_num = ($this->totalDataCount === 0) ? 0 : ceil($this->totalDataCount / $this->pageSize);
        if ($this->currPageNum > $total_page_num)
        {
            $this->currPageNum = $total_page_num;
        }

        echo $this->render('index', [
            'html_options'=>$html_options,
            'total_page_num'=>$total_page_num
        ]);
        $params = [
            'curr_page_num'=>$this->currPageNum,
            'total_page_num'=>$total_page_num,
            'page_size' => $this->pageSize
        ];
        
        $params['translations'] = [
            'CurrentPageMustBePositiveNumber' => Yii::t('SIMAPagination.Pagination', 'Current page must be a positive number!'),
            'CurrentPageCanNotBeEmpty' => Yii::t('SIMAPagination.Pagination', 'Current page can not be empty!'),
            'CurrentPageCanNotBeLessThan1' => Yii::t('SIMAPagination.Pagination', 'Current page can not be less than 1!'),
            'CurrentPageCanNotBeLargerThanTheTotalNumberOfPages' => Yii::t('SIMAPagination.Pagination', 'Current page can not be larger than the total number of pages!'),
        ];
        
        $json = CJSON::encode($params);
        $register_script = "$('#" . $this->id . "').simaPagination('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }    
    
    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.simaPagination.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaPagination.css');
    }
}