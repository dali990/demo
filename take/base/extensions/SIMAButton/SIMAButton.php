<?php

/**
 * Primer zadavanja:
 * $this->widget('base.extensions.SIMAButton.SIMAButton',[
    'id'=>'neki id',
    'htmlOptions'=>[
        'atribut'=>'vrednost'
        ...
    ],
    'class'=>'test klasa',
    'action'=>[
        'title'=>'akcija1',
        'icon'=>'sima-icon _add _16',
        'tooltip'=>'test tooltip',
        'onclick'=>['sima.notImplemented', true, 123],
        'subactions_position'=>'bottom', //right, left, top, default je bottom
        'subactions_order'=>'vertical', //horizontal, default je vertical
        'subactions'=>[ //rekurzivan niz akcija
            [
                'title'=>'subakcija1',
                'subactions_position'=>'right',
                'subactions'=>[
                    [
                        'title'=>'subakcija1.1',
                        'subactions'=>[
                            [
                                'title'=>'subakcija1.1.1'
                            ],
                            [
                                'title'=>'subakcija1.1.2'
                            ]
                        ]
                    ],
                    [
                        'title'=>'subakcija1.2'
                    ]
                ]
            ],
            [
                'title'=>'subakcija2',
            ],
            [
                'title'=>'subakcija3',
                'onclick'=>['sima.notImplemented'],
                'tooltip'=>'test tooltip 1',
                'subactions'=>[
                    [
                        'title'=>'subakcija3.1'
                    ],
                    [
                        'title'=>'subakcija3.2'
                    ]
                ]
            ]
        ]
    ]
]);
 */


class SIMAButton extends CWidget 
{
    public $id = null;
    public $class = '';    
    public $htmlOptions = [];
    public $action = [];
    
    public function run() 
    {   
        if(SIMAMisc::isVueComponentEnabled())
        {
            $this->returnVueButton();
            return;
        }
        
        if(empty($this->id))
        {
            $this->id = SIMAHtml::uniqid();
        }      
        
        if(empty($this->action['icon']) && empty($this->action['title']))
        {
            throw new SIMAException('Nepravilno zadato dugme. Ne mogu i tekst i slika biti prazni.');
        }
        
        $html_options = $this->parseHtmlOptions();
        $this->parseClasses();
        
        $dropdown_box = '';
        if (!empty($this->action['subactions']))
        {
            $dropdown_box .= $this->renderActionsRecursive($this->action['subactions'], 
                    !empty($this->action['subactions_position'])?$this->action['subactions_position']:null,
                    !empty($this->action['subactions_order'])?$this->action['subactions_order']:null
            );
        }
                
        echo $this->render('index', [
            'html_options'=>$html_options
        ]);
                
        $params = [
            'dropdown_box'=>$dropdown_box
        ];
        $json = CJSON::encode($params);
        $register_script = "$('#" . $this->id . "').simaButton('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
    
    private function parseHtmlOptions()
    {
        $html_options = '';
        foreach ($this->htmlOptions as $option_key=>$option_value)
        {
            if($option_key === 'class')
            {
                continue;
            }
            
            $html_options .= " $option_key='$option_value'";
        }
        if(!empty($this->action['htmlOptions']))
        {
            foreach ($this->action['htmlOptions'] as $option_key=>$option_value)
            {
                if($option_key === 'class')
                {
                    continue;
                }
            
                $html_options .= " $option_key='$option_value'";
            }
        }
        
        return $html_options;
    }
    
    private function parseClasses()
    {
        foreach ($this->htmlOptions as $option_key=>$option_value)
        {
            if($option_key !== 'class')
            {
                continue;
            }
            
            $this->class .= " $option_value";
        }
        if(!empty($this->action['htmlOptions']))
        {
            foreach ($this->action['htmlOptions'] as $option_key=>$option_value)
            {
                if($option_key !== 'class')
                {
                    continue;
                }
            
                $this->class .= " $option_value";
            }
        }
    }
    
    private function renderActionsRecursive($actions, $position_type, $position_order)
    {
        if (empty($position_type))
        {
            $position_type = 'bottom';
        }
        if (empty($position_order))
        {
            $position_order = 'vertical';
        }
        $actions_html = "<div class='btn-dropdown-box _$position_type _$position_order' data-position='$position_type'><ul>";
        foreach ($actions as $action) 
        {
            if(isset($action['html']))
            {
                if(!empty($action['html']))
                {
                    $actions_html .= '<li>'.$action['html'].'</li>';
                }
            }
            else
            {
                $subactions_html = '';
                if (!empty($action['subactions']))
                {
                    $subactions_html = $this->renderActionsRecursive($action['subactions'], 
                            !empty($action['subactions_position'])?$action['subactions_position']:null,
                            !empty($action['subactions_order'])?$action['subactions_order']:null
                    );
                }
                
                $html_options = '';
                if(!empty($action['htmlOptions']))
                {
                    foreach ($action['htmlOptions'] as $option_key=>$option_value)
                    {
                        $html_options .= " $option_key='$option_value'";
                    }
                }
                
                $actions_html .= $this->render('action', [
                    'action'=>$action,
                    'subactions_html'=>$subactions_html,
                    'html_options' => $html_options
                ], true);
            }
        }
        $actions_html .= "</ul></div>";
        
        return $actions_html;
    }
    
    private function returnVueButton()
    {
        $e = new Exception;
        error_log(__METHOD__.' - need to use vue button - '.$e->getTraceAsString());
        error_log(__METHOD__.' - $class - "'.$this->class.'"');
        error_log(__METHOD__.' - $htmlOptions - '.SIMAMisc::toTypeAndJsonString($this->htmlOptions));
        error_log(__METHOD__.' - $action - '.SIMAMisc::toTypeAndJsonString($this->action));

        $vue_params = [];

        if(strpos($this->class, '_half ') !== false)
        {
            $vue_params['half'] = true;
            $this->class = str_replace('_half ', ' ', $this->class);
        }
        if($this->class === '_half')
        {
            $vue_params['half'] = true;
            $this->class = '';
        }
        if(strpos($this->class, ' _16 ') !== false)
        {
            $vue_params['iconsize'] = 16;
            $this->class = str_replace(' _16 ', ' ', $this->class);
        }
        if(strpos($this->class, ' _no_border ') !== false)
        {
            $vue_params['no_border'] = true;
            $this->class = str_replace(' _no_border ', ' ', $this->class);
        }
        if(strpos($this->class, ' _transparent_background ') !== false)
        {
            $vue_params['transparent_background'] = true;
            $this->class = str_replace(' _transparent_background ', ' ', $this->class);
        }
        if(strpos($this->class, ' _hover_display_subactions ') !== false)
        {
            $vue_params['hover_display_subactions'] = true;
            $this->class = str_replace(' _hover_display_subactions ', ' ', $this->class);
        }
        if(strpos($this->class, ' _hover_background_color') !== false)
        {
            $number_of_chars_in_code = 6;
            $color_code = substr($this->class, strpos($this->class, ' _hover_background_color'), strlen(' _hover_background_color'), $number_of_chars_in_code);
            $vue_params['hover_background_color'] = $color_code;
            $this->class = str_replace(' _hover_background_color'.$color_code, ' ', $this->class);
        }
        $vue_params['icon'] = trim($this->class);
        if(isset($this->action['icon']))
        {
            $action_icon =  str_replace('sima-icon', ' ', $this->action['icon']);
            if(strpos($action_icon, '_16') !== false)
            {
                $vue_params['iconsize'] = 16;
                $action_icon = str_replace('_16', ' ', $action_icon);
            }
            else if(strpos($action_icon, '_18') !== false)
            {
                $vue_params['iconsize'] = 18;
                $action_icon = str_replace('_18', ' ', $action_icon);
            }
            if(isset($vue_params['icon']) && !empty($vue_params['icon']))
            {
                $vue_params['icon'] .= ' ';
            }
            $vue_params['icon'] .= trim($action_icon);
        }

        if(isset($this->action['subactions']))
        {
            $vue_params['subactions_data'] = $this->action['subactions'];
        }
        if(isset($this->action['subactions_position']))
        {
            $vue_params['subactions_position'] = $this->action['subactions_position'];
        }
        if(isset($this->action['title']))
        {
            $vue_params['title'] = $this->action['title'];
        }
        if(isset($this->action['onclick']))
        {
            $vue_params['onclick'] = $this->action['onclick'];
        }
        error_log(__METHOD__.' - $vue_params - '.SIMAMisc::toTypeAndJsonString($vue_params));

        Yii::app()->controller->widget(SIMAButtonVue::class, $vue_params);
    }
    
    public function generateFunctionFromArray($func_as_array)
    {        
        $func_name = $func_as_array[0];
        array_shift($func_as_array);
        $func_params = [];        
        foreach ($func_as_array as $value) 
        {
            if (gettype($value) === 'integer' || $value === '$(this)' || $value === 'event')
            {
                $func_params[] = $value;
            }
            else if (gettype($value) === 'boolean')
            {                
                $func_params[] = ($value)?'true':'false';                
            }
            else if (is_array($value))
            {
                $func_params[] = htmlspecialchars(CJSON::encode($value));
            }
            else
            {
                $value = addslashes($value);
                $func_params[] = htmlspecialchars("'$value'");
            }
        }
        
        return $func_name.'('.implode(',',$func_params).');';
    }
    
    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/simaButtonEvents.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),"sima.buttonEvents = new SIMAButtonEvents();", CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.simaButton.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaButton.css');
    }
}