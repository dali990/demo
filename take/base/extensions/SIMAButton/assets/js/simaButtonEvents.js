
/* global sima */

function SIMAButtonEvents()
{    
    $(function(){
        //klik na dugme
        $('body').on('click', '.sima-old-btn', function(e){            
            e.stopPropagation();
            var dropdown_box = $(this).data('btn_dropdown_box');
            sima.buttonEvents.onActionClick(e, $(this), dropdown_box);
        });
        //klik na dugme kao podmeni
        $('body').on('click', '.btn-dropdown-box ul > li', function(e){
            e.stopPropagation();
            sima.buttonEvents.onActionClick(e, $(this), $(this).children('.btn-dropdown-box'));
        });
        
        //klik na strelicu za toggle dropdown-a
        $('body').on('click', '.sima-old-btn._dropdown._main', function(e){
            var dropdown_box = $(this).parent().data('btn_dropdown_box');
            if (typeof dropdown_box !== 'undefined')
            {
                $(this).toggleClass('_expanded');
                sima.buttonEvents.toggleDropDownBox(dropdown_box);
                dropdown_box.simaPopup('refresh');                    
                e.stopPropagation();
                e.preventDefault();
            }
        });
        //klik na strelicu za toggle dropdown-a. Slucaj kada imamo dropdown unutar dropdowna(mora posebno da obradi jer prvi dropdown nije vezan 
        //za dugme na koje se otvara vec za body, a svi ostali unutar njega su vezani za dugme na koje se otvaraju)        
        $('body').on('click', '.btn-dropdown-box._main .sima-old-btn._dropdown', function(e){
            var dropdown_box = $(this).parent().children('.btn-dropdown-box');
            if (typeof dropdown_box !== 'undefined')
            {
                $(this).toggleClass('_expanded');
                sima.buttonEvents.toggleDropDownBox(dropdown_box);
                dropdown_box.simaPopup('refresh');                    
                e.stopPropagation();
                e.preventDefault();
            }
        });
        
        //dodajemo evente za sve dugmice cije se opcije prikazuju na hover
        $('body').on('mouseenter', '.sima-old-btn._hover_display_subactions', function(e){
            var dropdown_box = $(this).data('btn_dropdown_box');
            if(typeof dropdown_box !== 'undefined' && !dropdown_box.is(":visible"))
            {
                sima.buttonEvents.showDropdownBox(dropdown_box);
                dropdown_box.simaPopup('refresh');
            }
        });
        $('body').on('mouseleave', '.sima-old-btn._hover_display_subactions', function(e){
            var dropdown_box = $(this).data('btn_dropdown_box');
            setTimeout(function(){
                if(typeof dropdown_box !== 'undefined' && dropdown_box.data('hovered') !== true)
                {
                    sima.buttonEvents.hideDropdownBox(dropdown_box);
                }
            }, 1);
        });
        $('body').on('mouseenter', '.btn-dropdown-box', function(e){            
            var btn = null;
            if ($(this).hasClass('_main'))
            {
                btn = $(this).data('btn');
            }
            else
            {
                btn = $(this).parents('.btn-dropdown-box._main:first').data('btn');
            }
            
            if (!sima.isEmpty(btn) && btn.hasClass('_hover_display_subactions'))
            {
                $(this).data('hovered', true);
            }
        });
        $('body').on('mouseleave', '.btn-dropdown-box', function(e){ 
            var btn = null; //glavno dugme
            var action_obj = null; //akcija(li objekat u podmenijima)
            if ($(this).hasClass('_main'))
            {
                btn = $(this).data('btn');
                action_obj = btn;
            }
            else
            {
                btn = $(this).parents('.btn-dropdown-box._main:first').data('btn');
                action_obj = $(this).parent();
            }
            if (!sima.isEmpty(btn) && !sima.isEmpty(action_obj) && btn.hasClass('_hover_display_subactions'))
            {
                $(this).data('hovered', false);
                sima.buttonEvents.hideDropdownBox($(this));
            }
        });
    });
    
    this.dropDownBoxOnMouseOutside = function(e, action_obj, dropdown_box)
    {
        if (typeof action_obj.data('onclick') === 'undefined')
        {            
            if (!action_obj.is(e.target) && !$.contains(action_obj[0], e.target))
            {                    
                dropdown_box.hide();
            }
        }
        else if (!action_obj.children('.sima-old-btn._dropdown').is(e.target))
        {                        
            dropdown_box.hide();
        }
        e.stopPropagation();
        e.preventDefault();
    };
    
    this.onActionClick = function(e, action_obj, dropdown_box)
    {
        if (typeof action_obj.data('onclick') === 'undefined')
        {
            if (typeof dropdown_box !== 'undefined')
            {
                sima.buttonEvents.toggleDropDownBox(dropdown_box);                
            }                        
        }
        else
        {
            if (!sima.isEmpty(action_obj.data('security_questions')))
            {
                sima.dialog.openYesNo(action_obj.data('security_questions') + '?', 
                    function(){
                        sima.dialog.close();
                        onActionclick(e, action_obj);
                    }
                );
            }
            else
            {
                onActionclick(e, action_obj);
            }
        }
        
        if (typeof dropdown_box !== 'undefined')
        {
            dropdown_box.simaPopup('refresh');
        }
    };
    
    function onActionclick(e, action_obj)
    {
        action_obj.addClass('_autodisabled');
        setTimeout(function() {
            action_obj.removeClass('_autodisabled');
        }, 1000);
        
        /// toggle (zatvori) prvog popup roditelja
        action_obj.closest('.sima-popup._sima-popup-shown').simaPopup('toggle');

        action_obj.temp_onclick = function(event){
            eval(action_obj.data('onclick'));
        };
        action_obj.temp_onclick(e);
        action_obj.temp_onclick = null;
    }
    
    this.toggleDropDownBox = function(dropdown_box)
    {
        dropdown_box.toggle();
    };
    
    this.showDropdownBox = function(dropdown_box)
    {
        dropdown_box.show();
    };
    
    this.hideDropdownBox = function(dropdown_box)
    {
        dropdown_box.hide();
    };
}