
/* global sima */

(function($){
    
    var methods = {
        init: function(params) {            
            var _this = $(this);
            if (typeof params.dropdown_box !== 'undefined')
            {
                var dropdown_box = $(params.dropdown_box);
                _this.data('dropdown_box', dropdown_box);
                dropdown_box.simaPopup('init',$('body'),_this,dropdown_box.data('position'));
                dropdown_box.find('.btn-dropdown-box').each(function(){
                    $(this).simaPopup('init',null,$(this).parent(),$(this).data('position'));
                });
                dropdown_box.addClass('_main');
                _this.data('btn_dropdown_box', dropdown_box);
                dropdown_box.data('btn', _this);
                
                //sledece bind-ove nisam mogao da prebacim u simaButtonEvents jer nisam uspeo da pretvorim bind u on.
                //To mi je trebalo jer u startu nisu vidljivi dugmici, i ja te evente moram da vezem za body sa dodatnim selektorom za dugmice
                //pocetni btn-dropdown-box
                dropdown_box.bind("mousedownoutside", function(e){
                    sima.buttonEvents.dropDownBoxOnMouseOutside(e, _this, $(this));
                });
                //svi btn-dropdown-box unutar pocetnog
                dropdown_box.find('.btn-dropdown-box').bind("mousedownoutside", function(e){                    
                    sima.buttonEvents.dropDownBoxOnMouseOutside(e, $(this).parent(), $(this));
                });
                _this.bind('destroyed', function() {                    
                    dropdown_box.remove();                
                });
            }                   
        },
        disable: function()
        {
            var _this = $(this);
            if(!_this.hasClass('_disabled'))
            {
                _this.addClass('_disabled');
            }
        },
        enable: function()
        {
            var _this = $(this);
            if(_this.hasClass('_disabled'))
            {
                _this.removeClass('_disabled');
            }
        },
        disableSubActionsByCode: function(code)
        {
            var _this = $(this);
            _this.data('dropdown_box').find('li').each(function(index, value){
                var valueObj = $(value);
                if(valueObj.data('code') === code)
                {
                    valueObj.addClass('_disabled');
                }
            });
        },
        enableSubActionsByCode: function(code)
        {
            var _this = $(this);
            _this.data('dropdown_box').find('li').each(function(index, value){
                var valueObj = $(value);
                if(valueObj.data('code') === code)
                {
                    valueObj.removeClass('_disabled');
                }
            });
        },
        setTitle: function(new_title)
        {
            var _this = $(this);
            var title_span = _this.find('span.sima-old-btn-title');
            $(title_span).html(new_title);
        },
        setTooltip: function(new_tooltip)
        {
            var _this = $(this);
            var title_span = _this.find('span.sima-old-btn-title');
            $(title_span).attr('title', new_tooltip);
        }
    };
    
    $.fn.simaButton = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.simaButton');
            }
        });
        return ret;
    };
})(jQuery);