<?php $tooltip = (!empty($this->action['tooltip']))?$this->action['tooltip']:(!empty($this->action['title'])?$this->action['title']:''); ?>
<div id='<?php echo $this->id; ?>' onclick="" class="sima-old-btn <?php echo $this->class; ?>" <?php echo $html_options; ?> 
     title="<?php echo htmlentities($tooltip); ?>" 
     <?php echo (!empty($this->action['onclick'])) ? 'data-onclick="'.$this->generateFunctionFromArray($this->action['onclick']).'"' : ''; ?> 
     <?php echo (!empty($this->action['security_questions'])) ? 'data-security_questions="'.$this->action['security_questions'].'"' : ''; ?> 
 >
    <?php if (!empty($this->action['icon'])) { ?>
        <span onclick="" class='sima-old-btn-icon <?php echo $this->action['icon']; ?>'></span>
    <?php } ?>
    <?php if (!empty($this->action['title'])) { ?>
        <span onclick="" class="sima-old-btn-title" title='<?php echo $tooltip; ?>'><?php echo $this->action['title']; ?></span>
    <?php } ?>
    <?php if (!empty($this->action['onclick']) && !empty($this->action['subactions']) && count($this->action['subactions']) > 0) { ?>
        <div onclick="" class="sima-old-btn _half _dropdown _black _18 _main"></div>
    <?php } ?>
</div>
