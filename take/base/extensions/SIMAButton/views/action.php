<?php $tooltip = (!empty($action['tooltip']))?$action['tooltip']:(!empty($action['title'])?$action['title']:''); ?>
<li 
    <?php echo (!empty($action['onclick'])) ? 'data-onclick="'.$this->generateFunctionFromArray($action['onclick']).'"' : ''; ?> 
    <?php echo (!empty($action['security_questions'])) ? 'data-security_questions="'.$action['security_questions'].'"' : ''; ?> 
    title="<?php echo htmlentities($tooltip); ?>"
    class="<?php 
        if(isset($action['class']))
        {
            echo $action['class']; 
        }
    ?>" 
    <?php echo $html_options; ?> 
>
        <?php if (!empty($action['icon'])) { ?>
            <span class='sima-old-btn-icon <?php echo $action['icon']; ?>'></span>
        <?php } ?>
        <?php if (!empty($action['title'])) { ?>
            <span class="sima-old-btn-title" title='<?php echo $tooltip; ?>'><?php echo $action['title']; ?></span>
        <?php } ?>
        <?php if (!empty($action['onclick']) && !empty($action['subactions']) && count($action['subactions']) > 0) { ?>
            <div class="sima-old-btn _half _dropdown _black _18"></div>
        <?php } ?>
        <?php
            if (!empty($subactions_html))
            {
                echo $subactions_html;
            }
        ?>
</li>