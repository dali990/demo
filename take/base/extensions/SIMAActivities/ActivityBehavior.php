<?php

class ActivityBehavior extends SIMAActiveRecordBehavior
{
    protected $_simaActivity_priorityNumber = 0;
    protected $_simaActivity_icon_class = '';
    
    public function simaActivity_validate()
    {
        $owner = $this->owner;
                
        if(!isset($owner->_simaActivity_hourly) && !isset($owner->_simaActivity_daily))
        {
            throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ParameterNotSet', [
                '{class_name}' => get_class($owner),
                '{parameter}' => '"_simaActivity_hourly" '.Yii::t('BaseModule.Common', 'or').' "_simaActivity_daily"'
            ]));
        }
        
        if(!isset($owner->_simaActivity_date_filter))
        {
            throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ParameterNotSet', [
                '{class_name}' => get_class($owner),
                '{parameter}' => '"_simaActivity_date_filter"'
            ]));
        }
        
        if(isset($this->_simaActivity_condition))
        {
            if(!is_array($this->_simaActivity_condition))
            {
                throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ConditionNotArray', [
                    '{class_name}' => get_class($owner)
                ]));
            }
            
            if(!isset($this->_simaActivity_condition['comparison']))
            {
                throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ParameterNotSet', [
                    '{class_name}' => get_class($owner),
                    '{parameter}' => '"_simaActivity_condition"."comparison"'
                ]));
            }
        }
        
        if(isset($this->_simaActivity_confirmed_condition_scope))
        {
            if(!is_bool($this->_simaActivity_confirmed_condition_scope))
            {
                throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ParameterConfirmedConditionScopeNotBool', [
                    '{class_name}' => get_class($owner),
                    '{ccs}' => CJSON::encode($this->_simaActivity_confirmed_condition_scope)
                ]));
            }
        }
        
        if(isset($this->_simaActivity_from_to_times))
        {
            if(!is_array($this->_simaActivity_from_to_times))
            {
                throw new Exception(Yii::t('SIMAActivityManager.Activity', 'FromToTimesNotArray', [
                    '{class_name}' => get_class($owner)
                ]));
            }
            
            if(!isset($this->_simaActivity_from_to_times['begin']))
            {
                throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ParameterNotSet', [
                    '{class_name}' => get_class($owner),
                    '{parameter}' => '"_simaActivity_from_to_times"."begin"'
                ]));
            }
            if(!isset($this->_simaActivity_from_to_times['end']))
            {
                throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ParameterNotSet', [
                    '{class_name}' => get_class($owner),
                    '{parameter}' => '"_simaActivity_from_to_times"."end"'
                ]));
            }
        }
        
        if(!isset($this->_simaActivity_display_class))
        {
            throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ParameterNotSet', [
                '{class_name}' => get_class($owner),
                '{parameter}' => '"_simaActivity_display_class"'
            ]));
        }
        
        if(isset($this->_simaActivity_create_action) && !is_string($this->_simaActivity_create_action))
        {
            throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ParameterNotString', [
                '{class_name}' => get_class($owner),
                '{parameter}' => '"_simaActivity_create_action"'
            ]));
        }
        
        if(isset($this->_simaActivity_personal) && !is_string($this->_simaActivity_personal))
        {
            throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ParameterNotString', [
                '{class_name}' => get_class($owner),
                '{parameter}' => '"_simaActivity_personal"'
            ]));
        }
    }
    
    public function simaActivity_getActivityHours(Day $day)
    {
        $owner = $this->owner;
        throw new Exception(Yii::t('SIMAActivityManager.Activity', 'simaActivity_getActivityHoursNotImplemented', [
            '{class_name}' => get_class($owner)
        ]));
//        return 0;
    }
    
    public function simaActivity_getCalendarShortText()
    {
        $owner = $this->owner;
        throw new Exception(Yii::t('SIMAActivityManager.Activity', 'simaActivity_getCalendarShortTextNotImplemented', [
            '{class_name}' => get_class($owner)
        ]));
//        $owner = $this->owner;
//        return $owner->DisplayName;
    }
    
    public function simaActivity_confirmedConditionScope($person_id)
    {
        $owner = $this->owner;
        throw new Exception(Yii::t('SIMAActivityManager.Activity', 'simaActivity_confirmedConditionScopeNotImplemented', [
            '{class_name}' => get_class($owner)
        ]));
//        return [];
    }
    
    public function simaActivity_isPersonal()
    {
        return isset($this->_simaActivity_personal);
    }
    
    public function simaActivity_isDaily()
    {
        $result = false;
        
        if((isset($this->_simaActivity_hourly) && $this->_simaActivity_hourly === false)
                || (isset($this->_simaActivity_daily) && $this->_simaActivity_daily === true))
        {
            $result = true;
        }
        
        return $result;
    }
    
    public function simaActivity_haveFromToTimes()
    {
        return isset($this->_simaActivity_from_to_times);
    }
    
    public function simaActivity_haveCreateAction()
    {
        return isset($this->_simaActivity_create_action);
    }
    
    public function simaActivity_haveConfirmedConditionScope()
    {
        return isset($this->_simaActivity_confirmed_condition_scope) 
            && $this->_simaActivity_confirmed_condition_scope === true;
    }
    
    public function simaActivity_getPersonal()
    {
        return $this->_simaActivity_personal;
    }
    
    public function simaActivity_getDateFilter()
    {
        return $this->_simaActivity_date_filter;
    }
    
    public function simaActivity_getDisplayClass()
    {
        return $this->_simaActivity_display_class;
    }
    
    public function simaActivity_getIconClass()
    {
        return $this->_simaActivity_icon_class;
    }
    
    public function simaActivity_getBeginEndTimes()
    {
        $result = [
            'begin' => $this->_simaActivity_from_to_times['begin'],
            'end' => $this->_simaActivity_from_to_times['end']
        ];
        
        return $result;
    }
    
    public function simaActivity_getCreateAction()
    {
        return $this->_simaActivity_create_action;
    }
    
    public function simaActivity_getPriority()
    {
        return $this->_simaActivity_priorityNumber;
    }
    
    /**
     * uporedjuje dva odsustva i odredjuje prioritet
     * prioritet znaci koje odsustvo se racuna kao aktivno na datum ako su oba aktivna
     * 
     * primer: drzavni praznik ima veci prioritet od godisnjeg odmora
     * 
     * bolovanje > drzavni praznik > verski praznik > placeno odsustvo > godisnji odmor
     * 
     * AbsenceAnnualLeave - 10
     * AbsenceFreeDays - 30
     * NationalEventReligiousHoliday - 40
     * NationalEventStateHoliday - 50
     * AbsenceSickLeave - 60
     * 
     * povratne vrednosti su:
     *  -1  -> ima manji prioritet od prosledjene aktivnosti
     *  0   -> ima isti prioritet
     *  1   -> ima veci prioritet
     * 
     * 
     * @param SIMAActiveRecord $activity
     * @return int
     */
    public function comparePriorities(SIMAActiveRecord $activity)
    {
        $result = 0;
        
        $owner = $this->owner;
        
        if($owner->simaActivity_getPriority() > $activity->simaActivity_getPriority())
        {
            $result = 1;
        }
        else if($owner->simaActivity_getPriority() < $activity->simaActivity_getPriority())
        {
            $result = -1;
        }
        
        return $result;
    }
    
    public function afterSave($event)
    {
        parent::afterSave($event);
        
        $this->simaActivity_afterSave();
    }
    
    public function simaActivity_afterSave()
    {
        
    }
    
    public function afterDelete($event)
    {
        parent::afterSave($event);
        
        $this->simaActivity_afterDelete();
    }
    
    public function simaActivity_afterDelete()
    {
        
    }
    
    public function activityFilter_forThemes($theme_ids)
    {
        return [
            'ids' => '-1'
        ];
    }

    public function activityFilter_fromPersonForThemes($theme_ids)
    {
        return [
            'ids' => '-1'
        ];
    }
    
    public function activityFilter_fromPersonOnDateRange($date_range)
    {
        return [
            'ids' => '-1'
        ];
    }
}