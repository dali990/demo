<?php

class ActivityController extends SIMAController 
{
    private function localRenderPartial($view, $data=NULL, $return=false)
    {
        $view = 'base.extensions.SIMAActivities.views.'.$view;

        if($return)
        {
            return parent::renderPartial($view, $data, $return, false);
        }
        else
        {
//            echo parent::renderPartial($view, $data, $return, false);
            $errmsg = __METHOD__.' - nije trebalo doci dovde';
            error_log($errmsg);
            Yii::app()->errorReport->createAutoClientBafRequest($errmsg);
        }
    }
    
    public function actionCalendarIndex($calendar_id, $person_id)
    {
        $html = $this->localRenderPartial('calendar_index', [
            'calendar_id' => $calendar_id,
            'person_id' => $person_id
        ], true, true);
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionRebuildCalendarForMonth($month, $year, $person_id, $confirmationFilter)
    {        
        $person = ModelController::GetModel('Person', $person_id);
        
        $monthModel = Month::get($month, $year);
        
        $activities_per_day_data = $this->getActivitiesPerDayDataForMonth($person, $monthModel, $confirmationFilter);
        
        $calendar = $this->generateMonthCalendar($month, $year, $activities_per_day_data);
        
        $title = Yii::t('SIMAActivityManager.Activity', 'MonthDisplay'.$month).' '.$year.'.';
        
        $activities_per_day_data_string = json_encode($activities_per_day_data);
                
        $this->respondOK([
            'html' => $calendar,
            'title' => $title,
            'activities_per_day_data_string' => $activities_per_day_data_string,
        ]);
    }
    
    public function actionRebuildCalendarForMultiDay($person_id)
    {
        $selected_dates = $this->filter_post_input('selected_dates', false);
        
        $person = ModelController::GetModel('Person', $person_id);
        
        if(empty($selected_dates))
        {
            $selected_dates = [date('d.m.Y.')];
        }
        
        $today_day = date("d");
        $today_month = date("m");
        $today_year = date("Y");
        $todayModel = Day::get($today_day, $today_month, $today_year);
        
        $day_calendars_html = [];
        
        $calendar_activities = [];
        
        foreach($selected_dates as $selected_date)
        {
            $dayModel = Day::getByDate($selected_date);
            $day_calendar_data = $this->getDayCalendar($person, $dayModel, $todayModel);
            $day_calendars_html[] = $day_calendar_data['html'];
            $calendar_activities[$selected_date] = $day_calendar_data['activities'];
        }
        
        $html = $this->localRenderPartial('calendar.multiday.calendar', [
            'day_calendars' => $day_calendars_html,
        ], true, true);
        
        $title = $selected_dates[0];
        $selected_dates_count = count($selected_dates);
        if($selected_dates_count > 1)
        {
            $title .= ' - * - '.end($selected_dates);
        }
        
        $this->respondOK([
            'html' => $html,
            'title' => $title,
            'calendar_activities' => $calendar_activities
        ]);
    }
    
    public function getActivitiesPerDayDataInRange(Person $person, Day $fromDay, Day $toDay, $confirmationFilter)
    {
        $daysModels = Day::getBetweenDates($fromDay, $toDay);
        
        $activities_per_day_data = $this->getActivitiesPerDayData($person, $daysModels, $confirmationFilter);
        
        return $activities_per_day_data;
    }
    
    private function getActivitiesPerDayDataForMonth(Person $person, Month $month, $confirmationFilter)
    {
        $confirmationType = null;
        if($confirmationFilter === 'ALL')
        {
            $confirmationType = null;
        }
        else if($confirmationFilter === 'CONFIRMED')
        {
            $confirmationType = true;
        }
        else
        {
            throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ConfirmationFilterValueNotValid'));
        }
        
        $person_activities_in_month = Yii::app()->activityManager->GetActivities(
                $person, 
                null,
                $month,
                $confirmationType
        );
        
        $dayModels = $month->getDays();
        
        $person_activities_in_month_per_day = [];
        
        foreach($dayModels as $dayModel)
        {
            $activities_in_this_day = [];
            foreach($person_activities_in_month as $person_activity)
            {
                if($person_activity->simaActivity_isActiveOnDay($dayModel, $confirmationType))
                {
                    $activities_in_this_day[] = $person_activity;
                }
            }
            
            $person_activities_in_month_per_day[] = [
                'dayModel' => $dayModel,
                'person_activities_in_day' => $activities_in_this_day
            ];
        }
        
        $activities_per_day_data = $this->parsePerDayActivitiesIntoData($person, $person_activities_in_month_per_day);
        
        return $activities_per_day_data;
    }
    
    private function parsePerDayActivitiesIntoData(Person $person, $days, Day $todayModel=null)
    {
        $activities_per_day_data = [];
        
        if(empty($todayModel))
        {
            $todayModel = Day::get(date("d"), date("m"), date("Y"));
        }
        $last_confirmed_day = null;
        $last_boss_confirmed_day = null;
        if(isset($person->user))
        {
            $last_confirmed_day = $person->user->last_report_day;
            $last_boss_confirmed_day = $person->user->last_boss_confirm_day;
        }
        $last_pay_day = PaycheckPeriodComponent::GetLastPayOutDayInFinalPeriod();
        
        foreach($days as $day)
        {
            $activities = [];
            $workedHours = 0;
            
            $person_activities_in_day = $day['person_activities_in_day'];
            $dayModel = $day['dayModel'];
            
            $in_work_relation_classes = '';
            if(!empty($person->employee))
            {
                $in_work_relation_classes = ' _not_in_work_relation ';
                if($person->employee->isActiveAt($dayModel))
    //            if(count($person->work_positions) > 0) //// TO DO: da li postojanje ugovora ili aktivnost na dan;
                {
                    $in_work_relation_classes = ' _in_work_relation ';
                }
                else
                {
                    $activities[] = [
                        'classes' => '_no_work_contract_18',
                        'title' => Yii::t('SIMAActivityManager.Activity', 'NoActiveWorkContract')
                    ];
                }
            }
            
            $person_activities_used_classes = [];
            foreach ($person_activities_in_day as $person_activity_in_day)
            {
                $activity_hours = $person_activity_in_day->simaActivity_getActivityHours($dayModel);
                
                if($person_activity_in_day->simaActivity_isDaily())
                {
                }
                else
                {
                    $workedHours += $activity_hours;
                }

                $activity_class = $person_activity_in_day->simaActivity_getDisplayClass();
                $icon_class = $person_activity_in_day->simaActivity_getIconClass();                
                if(!isset($person_activities_used_classes[$activity_class]))
                {
                    $person_activities_used_classes[$activity_class] = true;
                    $activities[] = [
                        'classes' => $activity_class,
                        'icon_class' => $icon_class,
                        'title' => $person_activity_in_day->modelLabel()
                    ];
                }
            }
            
            $entryDoorLogHours = $this->getEntryDoorLogHours($dayModel, $person);
            
            $confirmed_classes = ' _not_confirmed ';
            if(!is_null($last_confirmed_day) && $dayModel->compare($last_confirmed_day) <= 0)
            {
                $confirmed_classes = ' _confirmed ';
            }
            
            $boss_confirmed_classes = ' _boss_not_confirmed ';
            if(!is_null($last_boss_confirmed_day) && $dayModel->compare($last_boss_confirmed_day) <= 0)
            {
                $boss_confirmed_classes = ' _boss_confirmed ';
            }
            
            $needed_work_hours = Yii::app()->activityManager->GetNeededWorkHoursInDay($dayModel);
            $regular_classes = ' _regular ';
            if((string)$workedHours !== (string)$needed_work_hours)
            {
                $regular_classes = ' _irregular ';
            }
            
            $today_classes = ' _today ';
            $day_compare_today = $dayModel->compare($todayModel);
            if($day_compare_today < 0)
            {
                $today_classes = ' _before_today ';
            }
            else if($day_compare_today > 0)
            {
                $today_classes = ' _after_today ';
            }
            
            $payday_classes = ' _before_last_pay_day ';
            if(!isset($last_pay_day) || (isset($last_pay_day) && $dayModel->compare($last_pay_day) > 0))
            {
                $payday_classes = ' _after_last_pay_day ';
            }
            
            $i = $dayModel->getdayInMonthNumber();
            
            $activities_per_day_data[$i]['activities'] = $activities;
            $activities_per_day_data[$i]['activities_models'] = $person_activities_in_day;
            $activities_per_day_data[$i]['workedHours'] = $workedHours;
            $activities_per_day_data[$i]['entryDoorLogHours'] = $entryDoorLogHours;
            $activities_per_day_data[$i]['confirmed_classes'] = $confirmed_classes;
            $activities_per_day_data[$i]['regular_classes'] = $regular_classes;
            $activities_per_day_data[$i]['today_classes'] = $today_classes;
            $activities_per_day_data[$i]['boss_confirmed_classes'] = $boss_confirmed_classes;
            $activities_per_day_data[$i]['payday_classes'] = $payday_classes;
            $activities_per_day_data[$i]['in_work_relation_classes'] = $in_work_relation_classes;
            $activities_per_day_data[$i]['day_model'] = $dayModel;
        }
        
        return $activities_per_day_data;
    }
    
    private function getEntryDoorLogHours(Day $dayModel, Person $person)
    {
        $entry_door_log_hours = null;
        
        $entryDoorLogCriteria = new SIMADbCriteria([
            'Model' => EntryDoorLog::model(),
            'model_filter' => [
                'at_day' => $dayModel,
                'partner' => [
                    'ids' => $person->id
                ],
                'display_scopes' => [
                    'orderByTimeAsc'
                ]
            ]
        ]);
        $entry_door_logs = EntryDoorLog::model()->findAll($entryDoorLogCriteria);
        $previous_entries = [];
        $entryDoorLogSeconds = 0;
        $sub_seconds = [];
        foreach($entry_door_logs as $entry_door_log)
        {
            if($entry_door_log->type === EntryDoorLog::$TYPE_ENTRY)
            {
                $timeDate = new DateTime($entry_door_log->time);
                $previous_entries[] = $timeDate->getTimestamp();
            }
            else if ($entry_door_log->type === EntryDoorLog::$TYPE_EXIT && !empty($previous_entries))
            {                    
                $exitDate = new DateTime($entry_door_log->time);
                $exitTimestamp = $exitDate->getTimestamp();
                
                $prevEntryTimestamp = array_pop($previous_entries);

                $secondsDiff = $exitTimestamp - $prevEntryTimestamp;

                if(!empty($previous_entries))
                {
                    $sub_seconds[] = $secondsDiff;
                }
                else
                {
                    $entryDoorLogSeconds += $secondsDiff;
                    array_pop($sub_seconds);
                }
            }
        }
        if(!empty($sub_seconds))
        {
            foreach($sub_seconds as $sub_second)
            {
                $entryDoorLogSeconds += $sub_second;
            }
        }
        if($entryDoorLogSeconds !== 0)
        {
            $entry_door_log_hours = round($entryDoorLogSeconds/(60*60), 1);
        }
        
        return $entry_door_log_hours;
    }
    
    private function getActivitiesPerDayData(Person $person, array $daysModels, $confirmationFilter)
    {
        $confirmationType = null;
        if($confirmationFilter === 'ALL')
        {
            $confirmationType = null;
        }
        else if($confirmationFilter === 'CONFIRMED')
        {
            $confirmationType = true;
        }
        else
        {
            throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ConfirmationFilterValueNotValid'));
        }
        
        $activities_per_day = [];
        foreach($daysModels as $dayModel)
        {
            $person_activities_in_day = Yii::app()->activityManager->GetActivities(
                    $person, 
                    null,
                    [$dayModel, $dayModel],
                    $confirmationType
            );
            
            $activities_per_day[] = [
                'dayModel' => $dayModel,
                'person_activities_in_day' => $person_activities_in_day
            ];
        }
        
        $activities_per_day_data = $this->parsePerDayActivitiesIntoData($person, $activities_per_day);
                
        return $activities_per_day_data;
    }
    
    private function generateMonthCalendar($month, $year, array $activities_per_day)
    {        
        $firstDay = Day::get(01, $month, $year);
        
        $start_day_date = new DateTime($firstDay);
        $daysInMonth = $start_day_date->format('t');
        
        $lastDay = Day::get($daysInMonth, $month, $year);
                
        $start_day_week_num = $firstDay->getWeekNumber();
        $end_day_week_num = $lastDay->getWeekNumber();
        $weeksInMonth = $end_day_week_num - $start_day_week_num + 1;
        
        $weeks_in_month_with_days = [];
        $currentDay = 0;
        $daysBeforeMonth = 0;
//        $daysAfterMonth = 0;
        
        $prev_month = $month-1;
        $prev_year = $year;
        if($prev_month < 1)
        {
            $prev_month = 12;
            $prev_year--;
        }
        $daysInPrevMonth = date('t',strtotime($prev_year.'-'.$prev_month.'-01'));
        
        for( $i=0; $i<$weeksInMonth; $i++ )
        {
            $week = [];
            //Create days in a week
            for($j=1;$j<=7;$j++)
            {
                $day_num = $i*7+$j;
                
                $additionalClasses = '';
                $workedHours = null;
                $entryDoorLogHours = null;
                $inCurrentMonth = false;
        
                $current_month_for_date = $month;
                $current_year_for_date = $year;
                $current_day_for_date = $currentDay;
                
                if($currentDay==0)
                {
                    $firstDayOfTheWeek = date('N',strtotime($year.'-'.$month.'-01'));
                    if(intval($day_num) == intval($firstDayOfTheWeek))
                    {
                        $currentDay=1;
                    }
                    else
                    {
                        $daysBeforeMonth = $firstDayOfTheWeek-$day_num;
                    }
                }
                
                if( ($currentDay!=0)&&($currentDay<=$daysInMonth) )
                {  
                    $inCurrentMonth = true;
                    
                    $additionalClasses .= ' _this_month ';
                    
                    $current_day_for_date = $currentDay;
                    $currentDay++;  
                }
                else
                {
                    if($currentDay<=$daysInMonth)
                    {
                        $additionalClasses .= ' _previous_month ';

                        $current_month_for_date--;
                        
                        $current_day_for_date = $daysInPrevMonth-$daysBeforeMonth+1;
                        $daysBeforeMonth++;
                    }
                    else 
                    {
                        $additionalClasses .= ' _next_month ';
                        
                        $current_month_for_date++;
                        $current_day_for_date = $currentDay-$daysInMonth;
                        $currentDay++;
                    }
                }
                
                $currentDate = date('d.m.Y.',strtotime($current_year_for_date.'-'.$current_month_for_date.'-'.$current_day_for_date));
                
                if($inCurrentMonth)
                {
                    $workedHours = 0;
                    
                    if(isset($activities_per_day[$current_day_for_date]))
                    {
                        $activities_in_day_data = $activities_per_day[$current_day_for_date];
                        $workedHours = $activities_in_day_data['workedHours'];
                        
                        if(isset($activities_in_day_data['entryDoorLogHours']))
                        {
                            $entryDoorLogHours = $activities_in_day_data['entryDoorLogHours'];
                        }

                        $additionalClasses .= $activities_in_day_data['confirmed_classes'];
                        $additionalClasses .= $activities_in_day_data['regular_classes'];
                        $additionalClasses .= $activities_in_day_data['today_classes'];
                        $additionalClasses .= $activities_in_day_data['boss_confirmed_classes'];
                        $additionalClasses .= $activities_in_day_data['payday_classes'];
                        $additionalClasses .= $activities_in_day_data['in_work_relation_classes'];
                    }
                }
                
                $week[] = $this->localRenderPartial('calendar.month.day_cell', [
                    'workedHours' => $workedHours,
                    'entryDoorLogHours' => $entryDoorLogHours,
                    'date' => $currentDate,
                    'additionalClasses' => $additionalClasses,
                    'day' => $current_day_for_date,
                    'month' => $current_month_for_date,
                    'year' => $current_year_for_date
                ], true, true);
            }
            
            $weeks_in_month_with_days[] = $week;
        }
        $calendar = $this->localRenderPartial('calendar.month.calendar', [
            'weeks_in_month_with_days' => $weeks_in_month_with_days
        ], true, true);
        
        return $calendar;
    }
    
    public function actionRenderDailyActivitiesInfo()
    {        
        $calendar_id = $this->filter_post_input('calendar_id');
        $dates_stringify = $this->filter_post_input('dates_stringify');
        $person_id = $this->filter_post_input('person_id');
        $confirmationFilter = $this->filter_post_input('confirmationFilter');
        
        $is_boss_view = false;
        if((int)$person_id !== (int)Yii::app()->user->id)
        {
            $is_boss_view = true;
        }
        
        $html = '';
        
        $personModel = ModelController::GetModel('Person', $person_id);
        
        $dates = json_decode($dates_stringify);
        
        if(empty($dates))
        {
            $dates = [
                date('d.m.Y.')
            ];
        }
        
        if(count($dates) === 1)
        {            
            $date = $dates[0];
            $dayModel = Day::getByDate($date);
            $last_confirmed_day = null;
            $last_boss_confirmed_day = null;
            if(!is_null($personModel->user))
            {
                $last_confirmed_day = $personModel->user->last_report_day;
                $last_boss_confirmed_day = $personModel->user->last_boss_confirm_day;
            }
            $last_pay_day = PaycheckPeriodComponent::GetLastPayOutDayInFinalPeriod();
            
            $activities_per_day_data = $this->getActivitiesPerDayDataInRange($personModel, $dayModel, $dayModel, $confirmationFilter);
            
            $activities_data_in_day = [];
            if(isset($activities_per_day_data[$dayModel->getdayInMonthNumber()]))
            {
                $activities_data_in_day = $activities_per_day_data[$dayModel->getdayInMonthNumber()];
            }

            $activities = '';
            
            foreach($activities_data_in_day['activities_models'] as $model)
            {
                if($model->simaActivity_isActiveOnDay($dayModel))
                {
                    $activities .= $this->renderSingleActivityInfo($model, $dayModel);
                }
            }
            
            /// ulasci-izlasci
            $entryDoorLogCriteria = new SIMADbCriteria([
                'Model' => EntryDoorLog::model(),
                'model_filter' => [
                    'at_day' => $dayModel,
                    'partner' => [
                        'ids' => $personModel->id
                    ],
                    'display_scopes' => [
                        'orderByTimeAsc'
                    ]
                ]
            ]);
            $entry_door_logs = EntryDoorLog::model()->findAll($entryDoorLogCriteria);
            foreach($entry_door_logs as $entry_door_log)
            {
                $text = ' - '.EntryDoorLog::getTypeData()[$entry_door_log->type].' - '.$entry_door_log->time;
                $parameters = [
                    'additionalClasses' => '',
                    'title' => $entry_door_log->modelLabel(),
                    'text' => $text,
                    'model_path2' => '',
                    'model_id' => '',
                    'icon_class' => ''
                ];
                $activities .= $this->localRenderPartial('calendar.month.activity_basicInfo', $parameters, true, true);
            }

            /// da li je potvrdjen
            $day_confirmed = false;
            if(!is_null($last_confirmed_day) && $dayModel->compare($last_confirmed_day) <= 0)
            {
                $day_confirmed = true;
            }
            $boss_confirmed = false;
            if(!is_null($last_boss_confirmed_day) && $dayModel->compare($last_boss_confirmed_day) <= 0)
            {
                $boss_confirmed = true;
            }
            $after_last_payday = false;
            if(!isset($last_pay_day) || (isset($last_pay_day) && $dayModel->compare($last_pay_day) > 0))
            {
                $after_last_payday = true;
            }
            
            $in_work_relation = false;
            if(!empty($personModel->employee) && $personModel->employee->isActiveAt($dayModel))
            {
                $in_work_relation = true;
            }
            
            $have_confirm_button = false;
            if(
                !empty($personModel->user) /// TODO: trebalo bi izbaciti da nije u user-u
                &&
                (
                    ($is_boss_view === false && $day_confirmed === false)
                    || ($is_boss_view === true && $boss_confirmed === false 
                            && (is_null($last_confirmed_day) || (!is_null($last_confirmed_day) && $dayModel->compare($last_confirmed_day) <= 0))
                        )
                )
                && $after_last_payday === true 
//                && $in_work_relation === true
            )
            {
                $have_confirm_button = true;
            }
            
            $have_revert_confirm_button = false;
            if(
                !empty($personModel->user) /// TODO: trebalo bi izbaciti da nije u user-u
                &&
                $have_confirm_button === false && $after_last_payday === true 
//                    && $in_work_relation === true
                && (
                    ($is_boss_view === false && $boss_confirmed === false)
                    || 
                    ($is_boss_view === true && $day_confirmed === true)
                )
            )
            {
                $have_revert_confirm_button = true;
            }
            
            $additionalClasses = '';
            $additionalClasses .= $activities_data_in_day['confirmed_classes'];
            $additionalClasses .= $activities_data_in_day['regular_classes'];
            $additionalClasses .= $activities_data_in_day['today_classes'];
            $additionalClasses .= $activities_data_in_day['boss_confirmed_classes'];
            $additionalClasses .= $activities_data_in_day['payday_classes'];
            $additionalClasses .= $activities_data_in_day['in_work_relation_classes'];
            
            $html = $this->localRenderPartial('calendar.month.daily_activities_info', [
                'date' => $dayModel->day_date,
                'personModel' => $personModel,
                'activities' => $activities,
                'calendar_id' => $calendar_id,
                'additionalClasses' => $additionalClasses,
                'have_confirm_button' => $have_confirm_button,
                'is_boss_view' => $is_boss_view,
                'have_revert_confirm_button' => $have_revert_confirm_button
            ], true, true);
            
        }
        else
        {
            $dayModels = [];
            foreach($dates as $date)
            {
                $dayModels[] = Day::getByDate($date);
            }
            $html = $this->renderSummedActivities($personModel, $dayModels, $confirmationFilter);
        }
                
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    private function renderSingleActivityInfo(SIMAActiveRecord $activity, Day $day=null, array $render_params=[])
    {
        $additonalClasses = '';

        if(get_class($activity) === Theme::class)
        {
            $task_htmls = '';
            foreach($render_params as $task_array)
            {
                $task_htmls .= $this->renderSingleActivityInfo($task_array['model'], null, [
                    'hours' => $task_array['hours']
                ]);
            }
            
            $html = $this->localRenderPartial('calendar.month.activity_theme_basicInfo', [
                'task_htmls' => $task_htmls,
                'theme' => $activity
            ], true, true);
        }
        else
        {
            $additonalClasses .= ' '.$activity->simaActivity_getDisplayClass();
            $title = $activity->modelLabel();
            $text = $activity->simaActivity_getCalendarShortText();

            $parameters = [
                'additionalClasses' => $additonalClasses,
                'title' => $title,
                'text' => $text,
                'model_path2' => $activity->getDisplayAction(),
                'model_id' => $activity->id,
                'icon_class' => $activity->simaActivity_getIconClass()
            ];
            
            if($activity->hasDisplay())
            {
                $parameters['model_id'] = '{"model_name":"'.get_class($activity).'" ,"model_id":'.$activity->id.'}';
            }

            if(!is_null($day))
            {
                $parameters['hours'] = $activity->simaActivity_getActivityHours($day).'h';
            }
            else if(isset($render_params['hours']))
            {
                $parameters['hours'] = $render_params['hours'].'h';
            }

            $html = $this->localRenderPartial('calendar.month.activity_basicInfo', $parameters, true, true);
        }
        
        return $html;
    }
    
    public function actionGetLastConfirmedDayMonth($person_id)
    {
        $personModel = ModelController::GetModel('Person', $person_id);
        
        $status = 'false';
        $message = 'Nema potvrdjenog dana';
        $day = null;
        $month = null;
        $year = null;
        
        if(isset($personModel->user) && isset($personModel->user->last_report_day))
        {
            $status = true;
            $message = 'Uspesno';
            
            $day = $personModel->user->last_report_day->getDayInMonthNumber();
            $month = $personModel->user->last_report_day->getMonthNumber();
            $year = $personModel->user->last_report_day->getYearNumber();
        }
        $this->respondOK([
            'status' => $status,
            'message' => $message,
            'day' => $day,
            'month' => $month,
            'year' => $year
        ]);
    }
    
    public function actionOnConfirmDate($date, $person_id, $calendar_id, $boss_confirm)
    {
        $inputDayModel = Day::getByDate($date);
        $personModel = ModelController::GetModel('Person', $person_id);
        $boss_confirm_parsed = SIMAMisc::filter_bool_var($boss_confirm);
                
        $fromToDates = $this->actionOnConfirmDate_getFromToDatesString($personModel, $inputDayModel, $boss_confirm_parsed);
        $additional_text = $this->actionOnConfirmDate_getAdditionalText($personModel, $inputDayModel, $boss_confirm_parsed);
        
        $html = $this->localRenderPartial('calendar.confirm_dialog', [
            'fromToDates' => $fromToDates,
            'date' => $date,
            'personModelId' => $personModel->id,
            'calendar_id' => $calendar_id,
            'additional_text' => $additional_text,
            'boss_confirm' => $boss_confirm
        ], true, true);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionOnRevertConfirmDate($date)
    {
        $html = $this->localRenderPartial('calendar.revert_confirm_dialog', [
            'date' => $date
        ], true, true);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    private function actionOnConfirmDate_getFromToDatesString(Person $personModel, Day $inputDayModel, $boss_confirm)
    {
        $fromToDates = '';
        
        $last_confirmed_day = null;
        if(!empty($personModel->user))
        {
            if($boss_confirm === true)
            {
                $last_confirmed_day = $personModel->user->last_boss_confirm_day;
            }
            else
            {
                $last_confirmed_day = $personModel->user->last_report_day;
            }
        }
        
        if(empty($last_confirmed_day))
        {
            $fromToDates = '∞ - '.$inputDayModel->day_date;
        }
        else
        {
            $first_day_to_confirm = $last_confirmed_day->next();
            $fromToDates = $first_day_to_confirm->day_date;
            if($first_day_to_confirm->id !== $inputDayModel->id)
            {
                $fromToDates .= ' - '.$inputDayModel->day_date;
            }
        }
        
        return $fromToDates;
    }
    
    private function actionOnConfirmDate_getAdditionalText(Person $personModel, Day $inputDayModel, $boss_confirm)
    {
        $additional_text = '';
        
        $last_confirmed_day = null;
        if(!empty($personModel->user))
        {
            if($boss_confirm === true)
            {
                $last_confirmed_day = $personModel->user->last_boss_confirm_day;
            }
            else
            {
                $last_confirmed_day = $personModel->user->last_report_day;
            }
        }
//        if(empty($last_confirmed_day))
//        {
//            $additional_text = Yii::t('SIMAActivityManager.Activity', 'HaveNoConfirmedDay');
//        }
        
        if($boss_confirm !== true && !empty($last_confirmed_day))
        {
            $first_day_to_confirm = $last_confirmed_day->next();

            while(empty($additional_text) && $first_day_to_confirm->compare($inputDayModel) <= 0)
            {
                if(Yii::app()->activityManager->IsIrregularDayForPerson($personModel, $first_day_to_confirm) === true)
                {
                    $additional_text = Yii::t('SIMAActivityManager.Activity', 'HaveIrregularDay');
                    break;
                }

                $first_day_to_confirm = $first_day_to_confirm->next();
            }
        }
        
        return $additional_text;
    }
    
    public function actionOnDetailedConfirmDate()
    {
        set_time_limit(5);
        
        $data = $this->setEventHeader();
        
        $eventSourceStepsSum = 0.0;
        Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
        
        $date = $data['date'];
        $person_id = $data['person_id'];
        $boss_confirm = $data['boss_confirm'];
        
        $inputDayModel = Day::getByDate($date);
        $personModel = ModelController::GetModel('Person', $person_id);
        
        $boss_confirm_parsed = filter_var($boss_confirm, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if(is_null($boss_confirm_parsed))
        {
            throw new Exception(Yii::t('SIMAActivityManager, Activity', 'BossConfirmNotBoolean'));
        }
        
        $last_confirmed_day = null;
        
        if(!empty($personModel->user))
        {
            if($boss_confirm_parsed === true)
            {
                $last_confirmed_day = $personModel->user->last_boss_confirm_day;
            }
            else
            {
                $last_confirmed_day = $personModel->user->last_report_day;
            }
        }
        
        $days_between_dates = [];
        if(is_null($last_confirmed_day))
        {
            $first_report_day = null;
            if(!empty($personModel->user))
            {
                $first_report_day = $personModel->user->first_report_day;
            }
            if(is_null($first_report_day))
            {
                $days_between_dates = [$inputDayModel];
            }
            else
            {
                $days_between_dates = Day::getBetweenDates($first_report_day, $inputDayModel);
            }
        }
        else
        {
            $days_between_dates = Day::getBetweenDates($last_confirmed_day->next(), $inputDayModel);
        }
                
        $html = 'Da li ste sigurni da zelite da potvrdite dane:</br>';
        
        $days_between_dates_count = count($days_between_dates);
        if($days_between_dates_count < 1)
        {
            $days_between_dates_count = 1;
        }
        $eventSourceSteps = (100/$days_between_dates_count);
        
        foreach($days_between_dates as $day)
        {
            set_time_limit(5);
            
            $eventSourceStepsSum += $eventSourceSteps;
            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            
            $additional_classes = '';
            $additional_text = '';
            if(Yii::app()->activityManager->IsIrregularDayForPerson($personModel, $day) === true)
            {
                $additional_classes = '_irregular';
                $additional_text = 'Neregularan broj sati!';
            }
            
            $workedHours = Yii::app()->activityManager->getPersonActivityHoursInDay(
                    $personModel,
                    $day
            );
            
            $html .= $this->localRenderPartial('confirmation_date', [
                'additional_classes' => $additional_classes,
                'date' => $day->day_date,
                'hours' => $workedHours.'/'.Yii::app()->activityManager->GetNeededWorkHoursInDay($day),
                'additional_text' => $additional_text
            ], true, true);
        }
        
        $this->sendEndEvent([
            'html' => $html
        ]);
    }
    
    public function actionConfirmDate($date, $person_id, $boss_confirm)
    {
        $inputDayModel = Day::getByDate($date);
        $personModel = Person::model()->findByPkWithCheck($person_id);
        $user = $personModel->user;
        
        if(empty($user))
        {
            throw new SIMAException('user empty');
        }
        
        $boss_confirm_parsed = SIMAMisc::filter_bool_var($boss_confirm);
        
        if($boss_confirm_parsed === true)
        {
            $user->last_boss_confirm_day_id=$inputDayModel->id;
        }
        else
        {
            $user->last_report_day_id=$inputDayModel->id;
        }
        $user->save();
        
        if (isset($user->person->employee))
        {
            $directors = $user->person->getSectorDirectors(true);
            foreach ($directors as $boss_id)
            {
                 if (
                         User::model()->count([
                            'model_filter' => [
                                'ids' => $boss_id
                            ]
                        ]) > 0
                    )
                 {
                    Yii::app()->warnManager->sendWarns($boss_id, 'WARN_BOSS_UNCONFIRMED_DAYS');
                 }
            }
        }
        
        $this->respondOK();
    }
    
    public function actionRevertConfirmDate($date, $person_id, $boss_confirm)
    {
        $inputDayModel = Day::getByDate($date);
        $personModel = ModelController::GetModel('Person', $person_id);
        $user = $personModel->user;
        
        if(empty($user))
        {
            throw new SIMAException('empty user');
        }
        
        $boss_confirm_parsed = SIMAMisc::filter_bool_var($boss_confirm);
        
        $confirmedDayModel = $inputDayModel->prev();
        
        if($boss_confirm_parsed === true)
        {
            $user->last_boss_confirm_day_id=$confirmedDayModel->id;
        }
        else
        {
            $user->last_report_day_id=$confirmedDayModel->id;
        }
        $user->save();
        
        $this->respondOK();
    }
    
    private function getDayCalendar(Person $person, Day $dayModel, Day $todayModel=null)
    {
        $confirmationType = null;
        $person_activities_in_day = Yii::app()->activityManager->GetActivities(
                $person, 
                null,
                $dayModel,
                $confirmationType
        );
        $person_activities_in_month_per_day = [
            [
                'dayModel' => $dayModel,
                'person_activities_in_day' => $person_activities_in_day
            ]
        ];
        $activities_per_day_data = $this->parsePerDayActivitiesIntoData($person, $person_activities_in_month_per_day, $todayModel);
        $activities_data_in_day = $activities_per_day_data[$dayModel->getDayInMonthNumber()];
        
        $person_activities = $activities_data_in_day['activities_models'];
        
        $allDayActivities = [];
        $activities = [];
        
        if(trim($activities_data_in_day['in_work_relation_classes']) === '_not_in_work_relation')
        {
            $allDayActivities['_no_work_contract_18'] = [
                'classes' => '_no_work_contract_18',
                'tooltip' => Yii::t('SIMAActivityManager.Activity', 'NoActiveWorkContract'),
                'title' => ''
            ];
        }
        
        foreach($person_activities as $person_activity)
        {            
            if($person_activity->simaActivity_haveFromToTimes() === false)
            {
                $display_class = $person_activity->simaActivity_getDisplayClass();
                if(empty($allDayActivities[$display_class]))
                {
                    $allDayActivities[$display_class] = [
                        'classes' => $display_class,
                        'title' => $person_activity->modelLabel()
                    ];
                }
            }
            else
            {
                $beginend_params = $person_activity->simaActivity_getBeginEndTimes();
                $start_time_param = $beginend_params['begin'];
                $end_time_param = $beginend_params['end'];

                $start_date_time_value = $person_activity->$start_time_param;
                $start_dt = new DateTime($start_date_time_value);

//                $start_time_day_date = $start_dt->format('d.m.Y.');

                $start_day = Day::getByDate($start_date_time_value);

                $start_time_result = '0';
                $end_time_result = '0';

                if($start_day->compare($dayModel) >= 0)
                {
                    $start_time_result = $start_dt->format('H');

                    $start_time_minutes = $start_dt->format('i');
                    if($start_time_minutes > 0)
                    {
                        $start_time_result .= ':'.$start_time_minutes;
                    }
                }

                $end_date_time_value = $person_activity->$end_time_param;
                $end_dt = new DateTime($end_date_time_value);

                $end_day = Day::getByDate($end_date_time_value);
                $end_day_compare = $end_day->compare($dayModel);
                if($end_day_compare === 0)
                {
                    $end_time_result = $end_dt->format('H');

                    $end_time_minutes = $end_dt->format('i');
                    if($end_time_minutes > 0)
                    {
                        $end_time_result .= ':'.$end_time_minutes;
                    }
                }
                else if($end_day_compare > 0)
                {
                    $end_time_result = '24';
                }

                $additional_classes = $person_activity->simaActivity_getDisplayClass();
                $activity_html = $this->localRenderPartial('calendar.multiday.activity_in_calendar', [
                    'activity' => $person_activity,
                    'additional_classes' => $additional_classes
                ], true, true);

                $activities[] = [
                    'start_time' => $start_time_result,
                    'end_time' => $end_time_result,
                    'html' => $activity_html
                ];
            }
        }
        
        $additionalClasses = '';
        $additionalClasses .= $activities_data_in_day['confirmed_classes'];
        $additionalClasses .= $activities_data_in_day['regular_classes'];
        $additionalClasses .= $activities_data_in_day['today_classes'];
        $additionalClasses .= $activities_data_in_day['boss_confirmed_classes'];
        $additionalClasses .= $activities_data_in_day['payday_classes'];
        $additionalClasses .= $activities_data_in_day['in_work_relation_classes'];
                
        $html = $this->localRenderPartial('calendar.multiday.day', [
            'additionalClasses' => $additionalClasses,
            'date' => $dayModel->day_date,
            'day' => $dayModel->getDayInMonthNumber(),
            'month' => $dayModel->getMonthNumber(),
            'year' => $dayModel->getYearNumber(),
            'allDayActivities' => $allDayActivities
        ], true, true);
        
        $title = $dayModel->getdayInMonthNumber().'. '
                .Yii::t('SIMAActivityManager.Activity', 'MonthDisplay'.$dayModel->getMonthNumber()).' '
                .$dayModel->getYearNumber().'.';
        
        $result = [
            'html' => $html,
            'title' => $title,
            'activities' => $activities,
            
        ];
        
        return $result;
    }
    
    public function actionRenderActivitiesSumInfoForMonth($month, $year, $person_id, $confirmationFilter)
    {
        $person = ModelController::GetModel('Person', $person_id);
        $monthModel = Month::get($month, $year);
        
        $dayModels = $monthModel->getDays();
        $activities_per_day_data = $this->getActivitiesPerDayDataForMonth($person, $monthModel, $confirmationFilter);
        
        $html = $this->renderSummedActivities($person, $dayModels, $confirmationFilter, true, $activities_per_day_data);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    private function renderSummedActivities(Person $person, array $dayModels, $confirmationFilter, $without_individual_activities=false, $activities_per_day_data=null)
    {
//        $activities = '';
        $sums = [
            'total' => [
                'class' => '_total',
                'title' => Yii::t('SIMAActivityManager.Activity', 'SumTotal'),
                'value' => 0
            ],
            'overtime' => [
                'class' => '_overtime',
                'title' => Yii::t('SIMAActivityManager.Activity', 'Overtime'),
                'value' => 0
            ],
            'undertime' => [
                'class' => '_undertime',
                'title' => Yii::t('SIMAActivityManager.Activity', 'Undertime'),
                'value' => 0
            ]
        ];

        $total_value = 0;
        $overtime_value = 0;
        $undertime_value = 0;

//        $super_grouped_activities = [];
        
        $grouped_activities = [];
        
        if(empty($activities_per_day_data))
        {
            $activities_per_day_data = $this->getActivitiesPerDayData($person, $dayModels, $confirmationFilter);
        }
        
        $current_day_model = Day::get();
        
        foreach($activities_per_day_data as $activity_per_day_data)
        {
            $worked_hours_per_day = 0;
            $dayModel = $activity_per_day_data['day_model'];
            
            foreach($activity_per_day_data['activities_models'] as $activity)
            {
                $activity_class = $activity->simaActivity_getDisplayClass();
                
                if(!isset($grouped_activities[$activity_class]))
                {
                    $grouped_activities[$activity_class] = [
                        'title' => $activity->modelLabel(true),
                        'models' => [],
                        'sum' => 0
                    ];
                }
                
                $activity_hours = $activity->simaActivity_getActivityHours($dayModel);
                
                $grouped_activities[$activity_class]['sum'] = $grouped_activities[$activity_class]['sum'] + $activity_hours;
                
                $total_value += $activity_hours;
                $worked_hours_per_day += $activity_hours;
                
                if($without_individual_activities === false && !isset($grouped_activities[$activity_class]['models'][$activity->id]))
                {
//                    $grouped_activities[$activity_class]['models'][$activity->id] = $activity;
                    $model = $activity;
                    if(get_class($activity) === TaskReport::class)
                    {
                        $model = $activity->task->theme;
                    }
                    else if(get_class($activity) === WorkedHoursReport::class)
                    {
                        $model = $activity->worked_hours_evidence->theme;
                    }
                    
                    if(!isset($grouped_activities[$activity_class]['models'][$model->id]))
                    {
                        $grouped_activities[$activity_class]['models'][$model->id] = [
                            'model' => $model,
                            'render_params' => []
                        ];
                    }
                    
                    if(get_class($activity) === TaskReport::class)
                    {
                        $new_render_params = $grouped_activities[$activity_class]['models'][$model->id]['render_params'];
                        $task_id_key = 'task_id_'.$activity->task->id;
                        if(isset($new_render_params[$task_id_key]))
                        {
                            $new_render_params[$task_id_key]['hours'] += $activity_hours;
                        }
                        else
                        {
                            $new_render_params[$task_id_key] = [
                                'model' => $activity->task,
                                'hours' => $activity_hours
                            ];
                        }
                        $grouped_activities[$activity_class]['models'][$model->id]['render_params'] = $new_render_params;
                    }
                    else if(get_class($activity) === WorkedHoursReport::class)
                    {
                        $new_render_params = $grouped_activities[$activity_class]['models'][$model->id]['render_params'];
                        $worked_hours_evidence_id_key = 'worked_hours_evidence_id_'.$activity->worked_hours_evidence->id;
                        if(isset($new_render_params[$worked_hours_evidence_id_key]))
                        {
                            $new_render_params[$worked_hours_evidence_id_key]['hours'] += $activity_hours;
                        }
                        else
                        {
                            $new_render_params[$worked_hours_evidence_id_key] = [
                                'model' => $activity->worked_hours_evidence,
                                'hours' => $activity_hours
                            ];
                        }
                        $grouped_activities[$activity_class]['models'][$model->id]['render_params'] = $new_render_params;
                    }
                }
            }
            
            /// za sve danje manje od danasnjeg
            if($current_day_model->compare($dayModel) > 0)
            {
                $needed_work_hours = Yii::app()->activityManager->GetNeededWorkHoursInDay($dayModel);
                if($worked_hours_per_day > $needed_work_hours)
                {
                    $overtime_value += ($worked_hours_per_day-$needed_work_hours);
                }
                else
                {
                    $undertime_value += ($needed_work_hours-$worked_hours_per_day);
                }
            }
        }
        
        $activities_html = '';
        foreach($grouped_activities as $key => $value)
        {
            $sums[] = [
                'class' => $key,
                'title' => $value['title'],
                'value' => $value['sum']
            ];
            
            if($without_individual_activities === false)
            {
                foreach($value['models'] as $model_array)
                {
                    $activities_html .= $this->renderSingleActivityInfo($model_array['model'], null, $model_array['render_params']);
                }
            }
        }

        $sums['total']['value'] = $total_value;
        $sums['overtime']['value'] = $overtime_value;
        $sums['undertime']['value'] = $undertime_value;

        $additionalClasses = '';
        
        $firstDayModel = reset($dayModels);
        $dates =$firstDayModel->day_date;
        $selected_dates_count = count($dayModels);
        if($selected_dates_count > 1)
        {
            $end_day = end($dayModels);
            $dates .= ' - * - '.$end_day->day_date;
        }
        
        $html = $this->localRenderPartial('calendar.month.multiday_activities_info', [
            'sums' => $sums,
            'activities' => $activities_html,
            'additionalClasses' => $additionalClasses,
            'dates' => $dates
        ], true, true);
        
        return $html;
    }
    
    public function actionRenderSingleActivityInfo($activity_class, $activity_id)
    {
        $activity = ModelController::GetModel($activity_class, $activity_id);
        
        $html = $this->renderSingleActivityInfo($activity);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
}