<?php

class SIMAActivityManager
{
    private $_registered_activities = [];
    
    public function init()
    {
    }
    
    public function registrationActivity($className)
    {
        if(!is_string($className))
        {
            throw new Exception(Yii::t('SIMAActivityManager.Activity', 'RegisterClassNotAsString', [
                '{class_name}' => CJSON::encode($className)
            ]));
        }
        
        if(!class_exists($className))
        {
            throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ClassDoesNotExist', [
                '{class_name}' => $className
            ]));
        }
        
        if(in_array($className, $this->_registered_activities))
        {
//            throw new Exception(Yii::t('SIMAActivityManager.Activity', 'ClassAlreadyRegistered', [
//                '{class_name}' => $className
//            ]));
        }
        else
        {
            if(!(Yii::app()->simaDemo->IsDemoISSIMA() && Yii::app()->isConsoleApplication()) && Yii::app()->validate_activities === true)
            {
                $className::model()->simaActivity_validate();
            }

            $this->_registered_activities[] = $className;
        }
    }
    
    public function GetActivities(
            Person $person=null, 
            $class_names=[], 
            $date_interval=null, 
            $condition_satisfied=null,
            $filter_functions = []
        )
    {
        $cashe_key = __METHOD__.CJSON::encode($person)
                .CJSON::encode($class_names).CJSON::encode($date_interval).CJSON::encode($condition_satisfied).CJSON::encode($filter_functions);
        $activities = Yii::app()->cache->get($cashe_key);
        if ($activities===FALSE)
        {
            $activities = [];

            foreach($this->_registered_activities as $registeredClass)
            {
                if(empty($class_names) || in_array($registeredClass, $class_names))
                {                    
                    $activities = array_merge(
                        $activities, 
                        $this->GetActivitiesByConf($registeredClass, $person, $date_interval, $condition_satisfied, $filter_functions)
                    );
                }
            }
            
            Yii::app()->cache->set($cashe_key, $activities, 10);//10sec
        }
        
        return $activities;
    }
    
    private function GetActivitiesByConf(
            $registeredClass, 
            Person $person=null,
            $date_interval=null, 
            $condition_satisfied=null,
            $filter_functions = []
        )
    {
        $model_filter = [];

        if(!is_null($person) && $registeredClass::model()->simaActivity_isPersonal())
        {
            $this->parseParamToModelFilter($registeredClass::model()->simaActivity_getPersonal(), $model_filter, $person->id);
        }

        if(!is_null($date_interval))
        {
            $model_filter[$registeredClass::model()->simaActivity_getDateFilter()] = $date_interval;
        }
        
        if(!is_null($condition_satisfied) 
                && $condition_satisfied === true 
                && $registeredClass::model()->simaActivity_haveConfirmedConditionScope())
        {
            $filter_scopes = [
                'simaActivity_ConfirmedConditionScope'
            ];
            if(!is_null($person))
            {
                $filter_scopes = [
                    'simaActivity_ConfirmedConditionScope' => $person->id
                ]; 
            }
            $model_filter['filter_scopes'] = $filter_scopes;
        }

        $criteria = new SIMADbCriteria([
            'Model' => $registeredClass,
            'model_filter' => $model_filter
        ]);

        foreach ($filter_functions as $filter_function => $filter_params)
        {
            $full_filter_function = "activityFilter_$filter_function";
            $filter_params_value = $filter_params;
            if (is_array($filter_params) && isset($filter_params['value']))
            {
                $filter_params_value = $filter_params['value'];
            }

            $temp_criteria = new SIMADbCriteria([
                'Model' => $registeredClass,
                'model_filter' => $registeredClass::model()->$full_filter_function($filter_params_value)
            ]);
            $criteria->mergeWith($temp_criteria);
        }

        $activities = $registeredClass::model()->findAll($criteria);
        
        return $activities;
    }
    
    private function parseParamToModelFilter($param, &$model_filter, $final_param)
    {
        $x_pos = strpos($param, '.');
        if($x_pos !== false)
        {
            $key = substr($param, 0, $x_pos);
            $rest = substr($param, $x_pos+1);
            
            $model_filter[$key] = [];
            $this->parseParamToModelFilter($rest, $model_filter[$key], $final_param);
        }
        else
        {
            $model_filter[$param] = [
                'ids' => $final_param
            ];
        }
    }

    public function getPersonActivityHoursInDay(Person $person, Day $day, $condition_satisfied=null, $activityClasseNames=[])
    {        
        $person_activities = Yii::app()->activityManager->GetActivities(
                $person, 
                $activityClasseNames,
                [$day, $day],
                $condition_satisfied
        );
        
        /// oni koji su hourly idu u active hours
        /// oni koji su daily stavljaju na 8
        
        $activity_hours = 0;
        
        $have_daily = false;
        foreach($person_activities as $person_activity)
        {
            if($person_activity->simaActivity_isDaily())
            {
                $have_daily = true;
                break;
            }
            else
            {
                $activity_hours += $person_activity->simaActivity_getActivityHours($day);
            }
        }
        
        if($have_daily === true)
        {
            if(!$day->IsWeekend)
            {
                $activity_hours += 8;
            }
        }
        
        return $activity_hours;
    }
    
    public function GetNeededWorkHoursInDay(Day $day)
    {
        $HOURS_PER_DAY = 8;
        
        $result = $HOURS_PER_DAY;
        if(
            $day->IsWeekend
            || 
            !is_null(NationalEventStateHolidayToDay::model()->find([
                'model_filter' => [
                    'on_date' => $day
                ]
            ]))
        )
        {
            $result = 0;
        }
        
        return $result;
    }
    
    public function IsIrregularDayForPerson(Person $person, Day $day)
    {
        $result = false;
        
        $workedHours = Yii::app()->activityManager->getPersonActivityHoursInDay(
                $person,
                $day
        );
        
        $neededWorkHoursInDay =  Yii::app()->activityManager->GetNeededWorkHoursInDay($day);

        if(floatval($workedHours) !== floatval($neededWorkHoursInDay))
        {
            $result = true;
        }
        
        return $result;
    }
    
    public function getRegisteredActivities()
    {
        return $this->_registered_activities;
    }
}

