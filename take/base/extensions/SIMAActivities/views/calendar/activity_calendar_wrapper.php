<div id='<?=$this->id?>' class='sima-layout-panel _splitter _vertical'>
    <div class="sima-layout-panel sima-b-ac-l-wrapper _horizontal"
        data-sima-layout-init='<?=CJSON::encode([
            'min_width' => 560,
        ])?>'
    >
        <div class="sima-b-ac-header sima-layout-fixed-panel">
            <div class="sima-b-ac-header-title">
                <div class="sima-b-ac-header-title-left">
                    <?php
                    if(SIMAMisc::isVueComponentEnabled())
                    {
                        $this->widget(SIMAButtonVue::class, [
                            'additional_classes' => ['_header_button', '_float_left'],
                            'title' => Yii::t('SIMAActivityManager.Activity', 'Today'),
                            'onclick' => ["sima.callPluginMethod", "#".$this->id, "simaActivityCalendar", "jumpToday"],
                            'subactions_data' => [
                                [
                                    'title' => Yii::t('SIMAActivityManager.Activity', 'LastConfirmedDay'),
                                    'onclick' => ["sima.callPluginMethod", "#".$this->id, "simaActivityCalendar", "lastConfirmedDay"]
                                ]
                            ]
                        ]);
                        
                    }
                    else
                    {
                        $this->widget('SIMAButton', [
                            'class' => '_header_button _float_left',
                            'action' => [
                                'title' => Yii::t('SIMAActivityManager.Activity', 'Today'),
                                'onclick' => ["sima.callPluginMethod", "#".$this->id, "simaActivityCalendar", "jumpToday"],
                                'subactions' => [
                                    [
                                        'title' => Yii::t('SIMAActivityManager.Activity', 'LastConfirmedDay'),
                                        'onclick' => ["sima.callPluginMethod", "#".$this->id, "simaActivityCalendar", "lastConfirmedDay"]
                                    ]
                                ]
                            ]
                        ]);
                    }
                    ?>
                    <?php
                        echo SIMAHtml::dropDownList(
                                Yii::t('SIMAActivityManager.Activity', 'FilterByConfirm'), 
                                'ALL', 
                                [
                                    'ALL' => Yii::t('SIMAActivityManager.Activity', 'AllActivities'),
                                    'CONFIRMED' => Yii::t('SIMAActivityManager.Activity', 'ConfirmedActivities'),
                                ],
                                [
                                    'class' => 'sima-b-ac-filter-by-confirm _float_left',
                                    'onchange' => '$("#' . $this->id . '").simaActivityCalendar("filterByConfirmChange",$(this));'
                                ]
                        )
                    ?>
                    <?php 
                    if(SIMAMisc::isVueComponentEnabled())
                    {
                        $this->widget(SIMAButtonVue::class, [
                            'additional_classes' => ['_header_button', '_float_left'],
                            'icon' => '_refresh',
                            'iconsize' => 16,
                            'onclick' => ["sima.callPluginMethod", "#".$this->id, "simaActivityCalendar", "reloadCalendar"]
                        ]);
                    }
                    else
                    {
                        $this->widget('SIMAButton', [
                            'class' => '_header_button _float_left',
                            'action' => [
                                'icon' => 'sima-icon _refresh _16',
                                'onclick' => ["sima.callPluginMethod", "#".$this->id, "simaActivityCalendar", "reloadCalendar"]
                            ]
                        ]);
                    }
                    ?>
                </div>
                <div class="sima-b-ac-header-title-right">
                    <div class='sima-b-ac-header-title-right-buttons'>
                        <?php 
                        if(SIMAMisc::isVueComponentEnabled())
                        {
                            $this->widget(SIMAButtonVue::class, [
                                'additional_classes' => ['_header_button'],
                                'icon' => '_left-arrow',
                                'iconsize' => 18,
                                'onclick' => ["sima.callPluginMethod", "#".$this->id, "simaActivityCalendar", "navigationClick", 'left']
                            ]);
                            $this->widget(SIMAButtonVue::class, [
                                'additional_classes' => ['_header_button'],
                                'icon' => '_right-arrow',
                                'iconsize' => 18,
                                'onclick' => ["sima.callPluginMethod", "#".$this->id, "simaActivityCalendar", "navigationClick", 'right'],
                            ]);
                        }
                        else
                        {
                            $this->widget('SIMAButton', [
                                'class' => '_header_button',
                                'action' => [
                                    'icon' => 'sima-icon _left-arrow _18',
                                    'onclick' => ["sima.callPluginMethod", "#".$this->id, "simaActivityCalendar", "navigationClick", 'left']
                                ]
                            ]);
                            $this->widget('SIMAButton', [
                                'class' => '_header_button',
                                'action' => [
                                    'icon' => 'sima-icon _right-arrow _18',
                                    'onclick' => ["sima.callPluginMethod", "#".$this->id, "simaActivityCalendar", "navigationClick", 'right'],
                                ]
                            ]);
                        }
                        ?>
                    </div>
                    <div class='sima-b-ac-header-title-right-month'></div>
                </div>
            </div>
        </div>
        <div class="sima-b-ac-content sima-layout-panel">
            <?php
                $this->widget('SIMATabs2', $tabs_params);
            ?>
        </div>
    </div>
    <div class="sima-layout-panel sima-b-sima-b-ac-activities-info-l-wrapper"
        data-sima-layout-init='<?=CJSON::encode([
            'min_width' => 400,
        ])?>'
    >
        <div class="sima-b-ac-activities-info-multiday"></div>
        <div class="sima-b-ac-activities-info-month"></div>
    </div>
</div>
