<div class="sima-b-ac-cd">
    <div class="sima-b-ac-cd-title">
        <?php echo Yii::t('SIMAActivityManager.Activity', 'AreYouSureConfirm');?>
    </div>
    <div class="sima-b-ac-cd-from-to-dates">
        <?php echo $fromToDates; ?>
    </div>
    <div class="sima-b-ac-cd-additional-text">
        <?php echo $additional_text; ?>
    </div>
    <div class="sima-b-ac-cd-details">
        <?php
        if(SIMAMisc::isVueComponentEnabled())
        {
            Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=>Yii::t('SIMAActivityManager.Activity', 'Detailed'),
                'onclick'=>['sima.activities.onDetailedConfirmDate', "$(this)", $date, $personModelId, $calendar_id, $boss_confirm]
            ]);
        }
        else
        {
            Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=>Yii::t('SIMAActivityManager.Activity', 'Detailed'),
                    'onclick'=>['sima.activities.onDetailedConfirmDate', "$(this)", $date, $personModelId, $calendar_id, $boss_confirm]
                ]
            ]);
        }
        ?>
    </div>
</div>

