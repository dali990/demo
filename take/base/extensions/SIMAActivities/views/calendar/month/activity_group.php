<div class='sima-b-ac-activity-group <?php echo $additonalClasses; ?>'>
    <div class='sima-b-ac-activity-group-header'>
        <div class='sima-b-ac-activity-group-icon'>
        </div>
        <div class='sima-b-ac-activity-group-title'>
            <?php echo $title; ?>
        </div>
    </div>
    <div class='sima-b-ac-activity-group-activities'>
        <?php
        foreach($activities as $activity)
        {
            echo $activity;
        }
        ?>
    </div>
</div>

