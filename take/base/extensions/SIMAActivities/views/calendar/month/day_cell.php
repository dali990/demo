
<div class='sima-b-ac-day <?php echo $additionalClasses; ?>' 
    data-day='<?php echo $day; ?>' 
    data-month='<?php echo $month; ?>' 
    data-year='<?php echo $year; ?>' 
    data-date='<?php echo $date; ?>' 
>
    <div class='sima-b-ac-day-header'>
        <div class='sima-b-ac-day-number'>
            <?php echo $day; ?>
        </div>
        <div class='sima-b-ac-day-worked-hours-wrapper'>
            <div class="sima-b-ac-day-worked-hours-number">
                <?php 
                    if(isset($workedHours))
                    {
                        echo $workedHours.'h'; 
                    }
                    if(isset($entryDoorLogHours))
                    {
                        echo '('.$entryDoorLogHours.')'; 
                    }
                ?>
            </div>
        </div>
    </div>
    <div class='sima-b-ac-day-activities'>
    </div>
</div>