<div class='sima-b-ac-day-acitivities-info <?php echo $additionalClasses ?>'>
    <div class='sima-b-ac-day-acitivities-header'>
        <div class='sima-b-ac-day-acitivities-title'>
            <?php echo Yii::t('SIMAActivityManager.Activity', 'DailyActivitiesForDate', [
                '{date}' => $date
            ]) ?>
        </div>
        <div class='sima-b-ac-day-acitivities-buttons'>
        <?php
            if($have_confirm_button === true)
            {
                $this->widget(SIMAButtonVue::class, [
                    'additional_classes' => ['_header_button', '_float_left'],
                    'title' => Yii::t('SIMAActivityManager.Activity', 'ConfirmDay'),
                    'onclick' => [
                        'sima.activities.onConfirmDate', $date, $personModel->id, $calendar_id, $is_boss_view
                    ],
                ]);
            }
            if($have_revert_confirm_button === true)
            {
                $this->widget(SIMAButtonVue::class, [
                    'additional_classes' => ['_header_button', '_float_left'],
                    'title' => Yii::t('SIMAActivityManager.Activity', 'RevertConfirmDay'),
                    'onclick' => [
                        'sima.activities.onRevertConfirmDate', $date, $personModel->id, $calendar_id, $is_boss_view
                    ],
                ]);
            }
        ?>
        </div>
    </div>
    <div class='sima-b-ac-day-acitivities-content'>
        <?php echo $activities; ?>
    </div>
</div>
