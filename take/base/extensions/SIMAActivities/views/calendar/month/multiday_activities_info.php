<div class='sima-b-ac-day-acitivities-info <?php echo $additionalClasses ?>'>
    <div class='sima-b-ac-day-acitivities-header'>
        <div class='sima-b-ac-day-acitivities-title'>
            <?php echo Yii::t('SIMAActivityManager.Activity', 'DailyActivitiesSumForDate', [
                '{dates}' => $dates
            ]) ?>
        </div>
        <div class='sima-b-ac-day-acitivities-buttons'>
        </div>
    </div>
    <div class='sima-b-ac-day-acitivities-content'>
        <div class='sima-b-ac-day-acitivities-sums'>
            <?php
            foreach($sums as $sum)
            {
            ?>
            <div class='sima-b-ac-day-acitivities-sum'>
                <div class='sima-b-ac-day-acitivities-sum-icon <?php echo $sum['class'] ?>'>
                    
                </div>
                <div class='sima-b-ac-day-acitivities-sum-title'>
                    <?php echo $sum['title'] ?>
                </div>
                <div class='sima-b-ac-day-acitivities-sum-value'>
                    <?php echo $sum['value'] ?>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
        <div class="sima-b-ac-day-acitivities-groups">
            <?php echo $activities; ?>
        </div>
    </div>
</div>
