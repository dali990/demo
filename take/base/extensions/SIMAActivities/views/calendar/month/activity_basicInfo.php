<div class='sima-b-ac-t-month-activity-basic-info <?php echo $additionalClasses; ?>'
    data-model_path2="<?php echo $model_path2; ?>"
    data-model_id='<?php echo $model_id; ?>'
     >
    <div class='_icon <?=$icon_class?>'></div>
    <?php if(isset($hours)) { ?>
    <div class='_hours'><?php echo $hours ?></div>
    <?php } ?>
    <div class='_title'><?php echo $title ?></div>
    <div class='_text'><?php echo $text ?></div>
</div>
