<div class='sima-b-ac-month'>
    <div class='sima-b-ac-week-labels'>
        <div class='sima-b-ac-week-label-day'>
            <?php echo Yii::t('SIMAActivityManager.Activity', 'WeekDayLabelMon'); ?>
        </div>
        <div class='sima-b-ac-week-label-day'>
            <?php echo Yii::t('SIMAActivityManager.Activity', 'WeekDayLabelTue'); ?>
        </div>
        <div class='sima-b-ac-week-label-day'>
            <?php echo Yii::t('SIMAActivityManager.Activity', 'WeekDayLabelWed'); ?>
        </div>
        <div class='sima-b-ac-week-label-day'>
            <?php echo Yii::t('SIMAActivityManager.Activity', 'WeekDayLabelThu'); ?>
        </div>
        <div class='sima-b-ac-week-label-day'>
            <?php echo Yii::t('SIMAActivityManager.Activity', 'WeekDayLabelFri'); ?>
        </div>
        <div class='sima-b-ac-week-label-day'>
            <?php echo Yii::t('SIMAActivityManager.Activity', 'WeekDayLabelSat'); ?>
        </div>
        <div class='sima-b-ac-week-label-day'>
            <?php echo Yii::t('SIMAActivityManager.Activity', 'WeekDayLabelSun'); ?>
        </div>
    </div>
    <div class='sima-b-ac-content-days-wrapper'>
        <?php
        foreach($weeks_in_month_with_days as $week_in_month_with_days)
        {
        ?>
            <div class='sima-b-ac-week-wrapper'>
            <?php
            foreach($week_in_month_with_days as $day)
            {
                echo $day;
            }
            ?>
            </div>
        <?php
        }   
        ?>
    </div>
</div>