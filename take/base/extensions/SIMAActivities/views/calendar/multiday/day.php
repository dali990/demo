<div class='sima-b-ac-type-day <?php echo $additionalClasses; ?>' 
    data-date="<?php echo $date; ?>"
    data-day='<?php echo $day; ?>' 
    data-month='<?php echo $month; ?>' 
    data-year='<?php echo $year; ?>' 
    >
    <div class='sima-b-ac-type-day-header <?php echo $additionalClasses; ?>'>
        <?php echo $date; ?>
    </div>
    <div class="sima-b-ac-t-multiday-allday-activities">
        <?php 
        foreach($allDayActivities as $allDayActivity)
        {
        ?>
        <div class="sima-b-ac-t-multiday-allday-activity <?php echo $allDayActivity['classes']; ?>"
            title="<?php if(isset($allDayActivity['tooltip'])){ echo $allDayActivity['tooltip']; } ?>">
            <div class="_icon"></div>
            <div class="_title"><?php echo $allDayActivity['title']; ?></div>
        </div>
        <?php
        }
        ?>
    </div>
    <div class='sima-b-ac-type-day-hour-bar'>
        <div class='sima-b-ac-type-day-hour-bar-hours'>
            <?php
            for($hour=0; $hour<=24; $hour++)
            {
            ?>
            <div class="_hour"><?php echo $hour.'h'; ?></div>
            <?php
            }
            ?>
        </div>
        <div class='sima-b-ac-type-day-hour-bar-activities'>
            <?php
            for($hour=0; $hour<=24; $hour++)
            {
            ?>
            <div class="_hour">
                <?php if($hour !== 24)
                {
                ?>
                <div class="_half_hour"></div>
                <?php 
                }
                ?>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
