<p class='sima-b-ac-confirmation <?php echo $additional_classes; ?>'>
    <span class='sima-b-ac-confirmation-date'><?php echo $date; ?></span>
    <span class='sima-b-ac-confirmation-hours'><?php echo $hours; ?></span>
    <span class='sima-b-ac-confirmation-additional-text'><?php echo $additional_text; ?></span>
</p>
