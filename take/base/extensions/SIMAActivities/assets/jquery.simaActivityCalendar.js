
/* global sima */

(function($){
    var methods = {
        init: function(params)
        {
            var localThis = this;
            
            localThis.data('day', parseInt(params.day));
            localThis.data('month', parseInt(params.month));
            localThis.data('year', parseInt(params.year));
            localThis.data('person_id', parseInt(params.person_id));
            localThis.data('type', params.initial_type);
            localThis.data('simaTabs2id', params.simaTabs2id);
            localThis.data('create_activity_button_id', params.create_activity_button_id);
            localThis.data('last_selected_day_number', null);
            localThis.data('selected_dates', []);
            localThis.data('force_reload_multiday', true);
            localThis.data('force_reload_month', true);
            localThis.data('month_title', '');
            localThis.data('multiday_title', '');
            localThis.data('confirmationFilter', 'ALL');
            
            localThis.find('.sima-b-ac-type-button[data-type="'+params.initial_type+'"]').addClass('_active');
            
            validateCalendarType(params.initial_type);
            
            localThis.on('click', '.sima-b-ac-day._this_month', function(event){
                onDayClick(localThis, $(this), event);
            });
            
            localThis.on('click', '.sima-b-ac-type-day-activity', function(){

                var _this = $(this);
                if(_this.hasClass('_selected'))
                {
                    deselectActivityInMultidayView(localThis, _this);
                }
                else
                {
                    localThis.find('.sima-b-ac-type-day-activity').removeClass('_selected');
                    _this.addClass('_selected');
                    
                    var activity_class = $(this).data('activity_class');
                    var activity_id = $(this).data('activity_id');
                    sima.ajax.get('activity/renderSingleActivityInfo',{
                        get_params: {
                            activity_class: activity_class,
                            activity_id: activity_id
                        },
                        async: false,
                        success_function:function(response)
                        {
                            var multiday_info = localThis.find('.sima-b-ac-activities-info-multiday');
                            var month_info = localThis.find('.sima-b-ac-activities-info-month');
                            
                            multiday_info.html(response.html);
                            
                            month_info.hide();
                            multiday_info.show();
                        }
                    });
                }
            });
            
            localThis.on('click', '.sima-b-ac-type-day-hour-bar-table-row', function(){
                
                var multiday_content = localThis.find('.sima-b-ac-l-wrapper').find('.sima-b-ac-content-multiday');
                
                var selected_activity = multiday_content.find('.sima-b-ac-type-day-activity._selected');
                if(!sima.isEmpty(selected_activity))
                {
                    deselectActivityInMultidayView(localThis, selected_activity);
                }
            });
            
            localThis.on('click', '.sima-b-ac-t-month-activity-basic-info', function(){
                var _this = $(this);
                
                var model_path2 = _this.data('model_path2');
                var model_id = _this.data('model_id');
                
                if(sima.isEmpty(model_path2) || (sima.isEmpty(model_id) && model_id !== 0))
                {
                    return;
                }
                
                sima.dialog.openModel(model_path2, model_id);
            });
            
            localThis.on('click', '.sima-b-ac-type-day-header', function(){
                var _this = $(this);
                                
                var dates = [];
                
                if(_this.hasClass('_selected'))
                {
                    _this.removeClass('_selected');
                    
                    var selected_days = localThis.find('.sima-b-ac-day._selected');
                    $.each(selected_days, function( index, value ) {
                        dates.push($(value).data('date'));
                    });
                }
                else
                {
                    localThis.find('.sima-b-ac-type-day-header._selected').removeClass('_selected');
                    
                    _this.addClass('_selected');
                    
                    dates.push(_this.parent().data('date'));
                }
                
                renderDailyActivitiesInfo(localThis, dates);
            });
            
            $('#'+localThis.data('simaTabs2id')).on('displaySelectedTab', {localThis:localThis}, onDisplaySelectedTab);
            var selected_code = $('#'+localThis.data('simaTabs2id')).simaTabs2('get_selected_code');
            displaySelectedTabByCode(localThis, selected_code);
        },
        
        reloadCalendar: function()
        {
            var localThis = this;
            rebuildCalendar(localThis);
        },
        
        reloadDate: function(date)
        {
            sima.notImplemented('reloadDate');
        },
        
        getSelectedDateData: function()
        {
            var localThis = this;
            
            var result = {
                day: localThis.data('day'),
                month: localThis.data('month'),
                year: localThis.data('year')
            };
            
            return result;
        },
        
        jumpToday: function()
        {
            var localThis = this;
            
            var d = new Date();
            var new_day = d.getDate();
            var new_month = d.getMonth()+1;
            var new_year = d.getFullYear();

            localThis.data('day', parseInt(new_day));
            localThis.data('month', parseInt(new_month));
            localThis.data('year', parseInt(new_year));

            localThis.data('last_selected_day_number', parseInt(new_day));

            rebuildCalendar(localThis);
        },
        
        lastConfirmedDay: function()
        {            
            var localThis = this;
            
            sima.ajax.get('activity/getLastConfirmedDayMonth',{
                get_params: {
                    person_id: localThis.data('person_id')
                },
                async: false,
                success_function:function(response)
                {                    
                    if(response.status === true)
                    {
                        var new_day = response.day;
                        var new_month = response.month;
                        var new_year = response.year;

                        localThis.data('day', parseInt(new_day));
                        localThis.data('month', parseInt(new_month));
                        localThis.data('year', parseInt(new_year));
                        
                        localThis.data('last_selected_day_number', parseInt(new_day));

                        rebuildCalendar(localThis);
                    }
                    else
                    {
                        sima.dialog.openInfo(response.message);
                    }
                }
            });
        },
        
        navigationClick: function(side)
        {
            var localThis = this;
            
            var rebuild = false;

            var type = localThis.data('type');
            var is_left_button = false;
            if(side === 'left')
            {
                is_left_button = true;
            }

            if(type === 'month')
            {
                var old_day = localThis.data('day');
                var old_month = localThis.data('month')-1;
                var old_year = localThis.data('year');

                var d = new Date(old_year, old_month, old_day);
                
                var addition_to_month = 1;
                if(is_left_button === true)
                {
                    addition_to_month = -1;
                }
                
                var switch_month = d.getMonth() + addition_to_month;

                d.setMonth(switch_month);
                
                if(d.getMonth() !== switch_month)
                {
                    d = new Date(old_year, switch_month, 1);
                }

                var new_day = d.getDate();
                var new_month = d.getMonth()+1;
                var new_year = d.getFullYear();

                localThis.data('day', parseInt(new_day));
                localThis.data('month', parseInt(new_month));
                localThis.data('year', parseInt(new_year));

                rebuild = true;
            }
            else if(type === 'multiday')
            {
                var new_day = localThis.data('day');
                var new_month = localThis.data('month');
                var new_year = localThis.data('year');
                var date_from_which_to_count = new_day+'.'+new_month+'.'+new_year+'.';
                var number_of_selected_dates = 1;

                var selected_dates = localThis.data('selected_dates');
                if(!sima.isEmpty(selected_dates))
                {
                    if(is_left_button === true)
                    {
                        date_from_which_to_count = selected_dates[0];
                    }
                    else
                    {
                        date_from_which_to_count = selected_dates[selected_dates.length-1];
                    }

                    number_of_selected_dates = selected_dates.length;
                }
                
                var date_from_which_to_count_split = date_from_which_to_count.split('.');

                var dateObj = new Date(parseInt(date_from_which_to_count_split[2]), parseInt(date_from_which_to_count_split[1])-1, parseInt(date_from_which_to_count_split[0]));
                localThis.data('day', parseInt(date_from_which_to_count_split[0]));
                localThis.find('.sima-b-ac-day._selected').removeClass('_selected');
                localThis.find('.sima-b-ac-day._multiselect_first').removeClass('_multiselect_first');
                localThis.find('.sima-b-ac-day._multiselect_last').removeClass('_multiselect_last');
                localThis.find('.sima-b-ac-day._multiselect_middle').removeClass('_multiselect_middle');
                
                var new_selected_dates = [];
                
                var date_to_select = null;

                for(var i=0; i<number_of_selected_dates; i++)
                {
                    if(is_left_button === true)
                    {
                        dateObj.setDate(dateObj.getDate()-1);
                    }
                    else
                    {
                        dateObj.setDate(dateObj.getDate()+1);
                    }

                    var new_day = dateObj.getDate();
                    var new_month = dateObj.getMonth()+1;
                    var new_year = dateObj.getFullYear();
                    
                    if(parseInt(new_month) === parseInt(localThis.data('month')))
                    {
                        if(new_month < 10)
                        {
                            new_month = '0'+new_month;
                        }

                        date_to_select = localThis.find('.sima-b-ac-day._this_month[data-day="'+new_day+'"]');
                        date_to_select.addClass('_selected');

                        if(number_of_selected_dates>1)
                        {
                            if(i===0)
                            {
                                date_to_select.addClass('_multiselect_first');
                            }
                            else if(i===number_of_selected_dates-1)
                            {
                                date_to_select.addClass('_multiselect_last');
                            }
                            else
                            {
                                date_to_select.addClass('_multiselect_middle');
                            }
                        }

                        var new_date = new_day+'.'+new_month+'.'+new_year+'.';
                        if(is_left_button === true)
                        {
                            new_selected_dates.unshift(new_date);
                        }
                        else
                        {
                            new_selected_dates.push(new_date);
                        }
                    }
                }
                
                if(sima.isEmpty(new_selected_dates))
                {
                    new_selected_dates.push(date_from_which_to_count);
                }
                
                if(number_of_selected_dates === 1 && !sima.isEmpty(date_to_select))
                {
                    afterDaySelected(localThis, date_to_select);
                }
                else
                {
                    $('#'+localThis.data('create_activity_button_id')).simaButton('disable');
                }

                localThis.data('selected_dates', new_selected_dates);
                
                rebuild = true;
            }

            if(rebuild === true)
            {
                rebuildCalendar(localThis);
            }
        },
        
        filterByConfirmChange: function(dropdownObj)
        {
            var localThis = this;
            
            var value = dropdownObj.val();
            
            localThis.data('confirmationFilter', value);
            
            rebuildCalendar(localThis);
        }
    };
    
    function deselectActivityInMultidayView(localThis, activityObj)
    {
        activityObj.removeClass('_selected');
                                        
        var dates = [];
        var selected_days = localThis.find('.sima-b-ac-day._selected');
        $.each(selected_days, function( index, value ) {
            dates.push($(value).data('date'));
        });

        renderDailyActivitiesInfo(localThis, dates);
    }
    
    function validateCalendarType(type)
    {
        if(type !== 'month' && type !== 'multiday')
        {
            var func_name = 'jquery.simaActivityCalendar.js - validateCalendarType';
            console.log(func_name+' - '+type);
            sima.addError(func_name, 'Kalendar nevalidnog tipa');
        }
    }
    
    function validateConfirmationFilter(confirmationFilter)
    {
        var result = true;
        if(confirmationFilter !== 'ALL' && confirmationFilter !== 'CONFIRMED')
        {
            var func_name = 'jquery.simaActivityCalendar.js - validateConfirmationFilter';
            console.log(func_name+' - '+confirmationFilter);
            sima.addError(func_name, 'Nevalidna vrednost filtera po potvrdi:</br> '+JSON.stringify(confirmationFilter));
            result = false;
        }
        return result;
    }
    
    function onDayClick(localThis, day_obj, event)
    {
        /// ukoliko se kliknulo na dan + shift/ctrl
        if(typeof event !== 'undefined' && (event.ctrlKey || event.shiftKey))
        {
            event.stopPropagation();
            
            if(event.shiftKey)
            {
                localThis.find('.sima-b-ac-day._selected').removeClass('_selected');
                localThis.find('.sima-b-ac-day._multiselect_first').removeClass('_multiselect_first');
                localThis.find('.sima-b-ac-day._multiselect_last').removeClass('_multiselect_last');
                localThis.find('.sima-b-ac-day._multiselect_middle').removeClass('_multiselect_middle');
                
                var last_selected_day_number = localThis.data('last_selected_day_number');
                if(!sima.isEmpty(last_selected_day_number))
                {
                    var last_selected_day = localThis.find('.sima-b-ac-day._this_month[data-day="'+last_selected_day_number+'"]');
                    last_selected_day.addClass('_selected');
                    var start_date_num = last_selected_day.data('day');
                    var end_date_num = day_obj.data('day');

                    if(start_date_num > end_date_num)
                    {
                        var temp_date_num = end_date_num;
                        end_date_num = start_date_num;
                        start_date_num = temp_date_num;
                        
                        day_obj.addClass('_multiselect_first');
                        last_selected_day.addClass('_multiselect_last');
                    }
                    else
                    {
                        day_obj.addClass('_multiselect_last');
                        last_selected_day.addClass('_multiselect_first');
                    }
                    
                    for(var i=start_date_num+1; i<end_date_num; i++)
                    {
                        var date_to_select = localThis.find('.sima-b-ac-day._this_month[data-day="'+i+'"]');
                        if(!date_to_select.hasClass('_selected'))
                        {
                            date_to_select.addClass('_selected');
                        }
                        if(!date_to_select.hasClass('_multiselect_middle'))
                        {
                            date_to_select.addClass('_multiselect_middle');
                        }
                    }
                }
            }
        }
        else /// inace, bez ctrl/shift
        {
            /// ukoliko je do sada samo jedan selektovan i to je taj isti dan
            /// nista kako bi se kasnije skinula selekcija
            var selected_days = localThis.find('.sima-b-ac-day._selected');
            if(selected_days.length === 1 
                    && $(selected_days[0]).data('date') === day_obj.data('date'))
            {
                
            }
            else /// bilo je selektovano vise, treba svima skinuti selekciju
            {
                localThis.find('.sima-b-ac-day._selected').removeClass('_selected');
            }
        }

        var activities_info = localThis.find('.sima-b-ac-activities-info-month');
        activities_info.empty();

        day_obj.toggleClass('_selected');

        var dates = [];
        var selected_days = localThis.find('.sima-b-ac-day._selected');
        $.each(selected_days, function( index, value ) {
            dates.push($(value).data('date'));
        });

        if(typeof event !== 'undefined' && event.shiftKey)
        {
            $('#'+localThis.data('create_activity_button_id')).simaButton('disable');
        }
        else if(dates.length > 0)
        {
            localThis.data('last_selected_day_number', day_obj.data('day'));
            localThis.data('day', day_obj.data('day'));
            
            afterDaySelected(localThis, day_obj);
        }
        else
        {
            localThis.data('last_selected_day_number', null);
        }
        
        localThis.data('selected_dates', dates);

        if(dates.length > 0)
        {
            renderDailyActivitiesInfo(localThis, dates);
        }
        else
        {
            renderActivitiesSumInfoForMonth(localThis);
        }
        
        localThis.data('force_reload_multiday', true);
    };
    
    function renderDailyActivitiesInfo(localThis, dates)
    {
        var confirmationFilter = localThis.data('confirmationFilter');
        if(validateConfirmationFilter(confirmationFilter) !== true)
        {
            return;
        }
        
        var dates_stringify = JSON.stringify(dates);
        
        sima.ajax.get('activity/renderDailyActivitiesInfo',{
            data: {
                dates_stringify: dates_stringify,
                person_id: localThis.data('person_id'),
                calendar_id: localThis.attr('id'),
                confirmationFilter: confirmationFilter
            },
            async: true,
            loadingCircle: localThis,
            success_function:function(response)
            {
                var month_info = localThis.find('.sima-b-ac-activities-info-month');
                var multiday_info = localThis.find('.sima-b-ac-activities-info-multiday');
                
                var type = localThis.data('type');
                
                if(type === 'month')
                {
                    month_info.html(response.html);

                    multiday_info.hide();
                    month_info.show();
                }
                else if(type === 'multiday')
                {
                    multiday_info.html(response.html);

                    month_info.hide();
                    multiday_info.show();
                }
            }
        });
    }
    
    function renderActivitiesSumInfoForMonth(localThis)
    {
        var confirmationFilter = localThis.data('confirmationFilter');
        if(validateConfirmationFilter(confirmationFilter) !== true)
        {
            return;
        }
        
        sima.ajax.get('activity/renderActivitiesSumInfoForMonth',{
            get_params: {
                person_id: localThis.data('person_id'),
                month: localThis.data('month'),
                year: localThis.data('year'),
                confirmationFilter: confirmationFilter
            },
            async: true,
            loadingCircle: localThis,
            success_function:function(response)
            {
                var month_info = localThis.find('.sima-b-ac-activities-info-month');
                var multiday_info = localThis.find('.sima-b-ac-activities-info-multiday');
                
                month_info.html(response.html);
                
                multiday_info.hide();
                month_info.show();
            }
        });
    }
    
    function resizeTodayFillingDiv(localThis, filling_resize_key)
    {        
        if(localThis.data('filling_resize_key') === filling_resize_key)
        {            
            var month_today_day_div = localThis.find('.sima-b-ac-day._today');
            var multiday_today_day_div = localThis.find('.sima-b-ac-type-day._today');
            var current_day = -1;
            var current_month = -1;
            var current_year = -1;
            if(!sima.isEmpty(month_today_day_div))
            {
                current_day = month_today_day_div.data('day');
                current_month = month_today_day_div.data('month');
                current_year = month_today_day_div.data('year');
            }
            else
            {
                current_day = multiday_today_day_div.data('day');
                current_month = multiday_today_day_div.data('month');
                current_year = multiday_today_day_div.data('year');
            }
            
            var d = new Date();
            var today_day = d.getDate();
            var today_month = d.getMonth()+1;
            var today_year = d.getFullYear();
            if(parseInt(today_day) !== parseInt(current_day)
                    || parseInt(today_month) !== parseInt(current_month)
                    || parseInt(today_year) !== parseInt(current_year))
            {                
                var rebuild_filling = false;
                
                localThis.find('.sima-b-ac-month-today-filling').remove();
                localThis.find('.sima-b-ac-multiday-today-filling').remove();
                
                month_today_day_div.removeClass('_today');
                var new_month_today_day_div = localThis.find('.sima-b-ac-day[data-day="'+today_day+'"][data-month="'+today_month+'"][data-year="'+today_year+'"]');
                if(!sima.isEmpty(new_month_today_day_div))
                {
                    month_today_day_div.addClass('_today');
                    rebuild_filling = true;
                }
                
                localThis.find('.sima-b-ac-type-day-header._today').removeClass('_today');
                var new_multiday_today_day_div = localThis.find('.sima-b-ac-type-day[data-day="'+today_day+'"][data-month="'+today_month+'"][data-year="'+today_year+'"]');
                if(!sima.isEmpty(new_multiday_today_day_div))
                {
                    new_multiday_today_day_div.addClass('_today');
                    rebuild_filling = true;
                }
                
                if(rebuild_filling === true)
                {
                    buildFillingDivForToday(localThis);
                }
            }
            else
            {                
                var to_recall = false;
                
                var todayDate = new Date();
                var hours = todayDate.getHours();
                var minutes = todayDate.getMinutes();
                var today_filling_width_percent = (hours/24)*100;
                                
                var month_filling = localThis.find('.sima-b-ac-month-today-filling');
                
                if(!sima.isEmpty(month_filling))
                {
                    month_filling.width(today_filling_width_percent+'%');
                    to_recall = true;
                }
                
                var multiday_filling = localThis.find('.sima-b-ac-multiday-today-filling');
                if(!sima.isEmpty(multiday_filling))
                {                    
                    var height_per_hour = 40;
                    var height_per_minute = height_per_hour/60;
                    var filling_height = hours * height_per_hour + height_per_minute * minutes;

                    multiday_filling.height(filling_height+'px');
                    to_recall = true;
                }
                
                if(to_recall === true)
                {
                    var timeout_time_ms = 1000 * 60 * 10; /// 10min
                    setTimeout(function() { resizeTodayFillingDiv(localThis, filling_resize_key); }, timeout_time_ms);
                }
            }
        }
    }
    
    function buildFillingDivForToday(localThis)
    {
        var to_call_resizeTodayFillingDiv = false;
        var month_today_day_div = localThis.find('.sima-b-ac-day._today');
        if(!sima.isEmpty(month_today_day_div))
        {
            var today_filling_div = $('<div class="sima-b-ac-month-today-filling _month_day_cell"></div>');
            var worked_hours_filling_div = $('<div class="sima-b-ac-month-today-filling _worked_hours"></div>');
            month_today_day_div.prepend(today_filling_div);
            month_today_day_div.find('.sima-b-ac-day-worked-hours-wrapper').prepend(worked_hours_filling_div);

            to_call_resizeTodayFillingDiv = true;
        }
        
        var multiday_today_div_header = localThis.find('.sima-b-ac-type-day-header._today');
        if(!sima.isEmpty(multiday_today_div_header))
        {
            var today_filling_div = $('<div class="sima-b-ac-multiday-today-filling"></div>');
            var hour_bar = multiday_today_div_header.parent().find('.sima-b-ac-type-day-hour-bar');
            hour_bar.prepend(today_filling_div);
            
            to_call_resizeTodayFillingDiv = true;
        }
        
        if(to_call_resizeTodayFillingDiv === true)
        {
            var filling_resize_key = sima.uniqid();
            localThis.data('filling_resize_key', filling_resize_key);
            resizeTodayFillingDiv(localThis, filling_resize_key);
        }
    }
    
    function rebuildCalendar(localThis)
    {
        localThis.find('.sima-b-ac-month-year').text(localThis.data('month')+'. '+localThis.data('year')+'.');
        var title_span = localThis.find('.sima-b-ac-header-title-right-month');
        var activities_info = localThis.find('.sima-b-ac-activities-info-'+localThis.data('type'));
        activities_info.empty();
        
        var type = localThis.data('type');
        validateCalendarType(type);
        
        var confirmationFilter = localThis.data('confirmationFilter');
        if(validateConfirmationFilter(confirmationFilter) !== true)
        {
            return;
        }
        
        var month_content = localThis.find('.sima-b-ac-content-month');
        var multiday_content = localThis.find('.sima-b-ac-content-multiday');
            
        if(type === 'month')
        {
            month_content.empty();
            sima.ajax.get('activity/rebuildCalendarForMonth',{
                get_params: {
                    month: localThis.data('month'),
                    year: localThis.data('year'),
                    person_id: localThis.data('person_id'),
                    confirmationFilter: confirmationFilter
                },
                async: true,
                loadingCircle: localThis,
                relative_elements: [localThis],
                success_function:function(response)
                {
                    month_content.html(response.html);
                    
                    localThis.data('month_title', response.title);
                    title_span.html(response.title);

                    var activities_per_day_data_string = response.activities_per_day_data_string;
                    var activities_per_day_data = JSON.parse(activities_per_day_data_string);
                    $.each(activities_per_day_data, function( index, day_activity_data ) {
                        if(day_activity_data.activities.length > 0)
                        {
                            var day_div = month_content.find('.sima-b-ac-day._this_month[data-day="'+index+'"]');
                            var day_div_activities = day_div.find('.sima-b-ac-day-activities');
                            $.each(day_activity_data.activities, function( index, value ) {
                                var activity_div = $("<div class='sima-b-ac-day-activity "+value.classes+" "+value.icon_class+"' "
                                        +"title='"+value.title+"'>"
                                    +"</div>");

                                day_div_activities.append(activity_div);
                            });
                        }
                    });
                    
                    buildFillingDivForToday(localThis);
                    
                    var last_selected_day_number = localThis.data('last_selected_day_number');

                    if(!sima.isEmpty(last_selected_day_number))
                    {
                        var last_selected_day = month_content.find('.sima-b-ac-day._this_month[data-day="'+last_selected_day_number+'"]');
                        onDayClick(localThis, last_selected_day);
                    }
                    else
                    {
                        renderActivitiesSumInfoForMonth(localThis);
                    }
                    
                    localThis.data('force_reload_month', false);
                }
            });
        }
        
        else if(type === 'multiday')
        {
            multiday_content.empty();

            var selected_dates = localThis.data('selected_dates');

            sima.ajax.get('activity/rebuildCalendarForMultiDay',{
                get_params: {
                    person_id: localThis.data('person_id'),
                    confirmationFilter: confirmationFilter
                },
                data: {
                    selected_dates: selected_dates
                },
                async: true,
                loadingCircle: localThis,
                success_function:function(response)
                {
                    multiday_content.html(response.html);
                    
                    localThis.data('multiday_title', response.title);
                    title_span.html(response.title);

                    var height_per_hour = 2 * 20; /// broj tr-ova po satu * visina po tr-u;
                    var height_per_minute = height_per_hour / (24 * 60); 

                    $.each(response.calendar_activities, function( date, activities ) {

                        var find_criteria = '.sima-b-ac-type-day[data-date="'+date+'"]';

                        var day_div = multiday_content.find(find_criteria);
                        
                        var hour_bar = day_div.find('.sima-b-ac-type-day-hour-bar');

                        $.each(activities, function( index, value ) {                            
                            var start_time = value.start_time;
                            var end_time = value.end_time;

                            var start_time_split = start_time.split(":");
                            var end_time_split = end_time.split(":");

                            var start_time_hours = parseInt(start_time_split[0]);
                            var start_time_minutes = 0;
                            if(start_time_split.length === 2)
                            {
                                start_time_minutes = parseInt(start_time_split[1]);
                            }
                            var end_time_hours = parseInt(end_time_split[0]);
                            var end_time_minutes = 0;
                            if(end_time_split.length === 2)
                            {
                                end_time_minutes = parseInt(end_time_split[1]);
                            }

                            var start_position = start_time_hours * height_per_hour + start_time_minutes * height_per_minute;

                            var height = ((end_time_hours - start_time_hours) * height_per_hour) 
                                    + ((end_time_minutes - start_time_minutes) * height_per_minute);
                            
                            var activity_div = $(value.html);

                            activity_div.css('top', start_position+'px');
                            activity_div.outerHeight(height+'px');
                            hour_bar.append(activity_div);
                            activity_div.show();
                        });
                    });
                    
                    buildFillingDivForToday(localThis);
                    
                    var dates = [];
                    var selected_days = localThis.find('.sima-b-ac-day._selected');
                    $.each(selected_days, function( index, value ) {
                        dates.push($(value).data('date'));
                    });
                    renderDailyActivitiesInfo(localThis, dates);
                    
                    localThis.data('force_reload_multiday', false);
                }
            });
        }
    }
    
    function onDisplaySelectedTab(event, event_message)
    {
        var localThis = event.data.localThis;
        
        var type = event_message.selected_code;
        
        displaySelectedTabByCode(localThis, type);
    }
    
    function displaySelectedTabByCode(localThis, type)
    {
        var to_reload = false;

        var multiday_content = localThis.find('.sima-b-ac-content-multiday');
        var multiday_info = localThis.find('.sima-b-ac-activities-info-multiday');
        var month_content = localThis.find('.sima-b-ac-content-month');
        var month_info = localThis.find('.sima-b-ac-activities-info-month');
        
        var title_span = localThis.find('.sima-b-ac-header-title-right-month');
        var title_value = '';
        
        if(type === 'month')
        {
            if(month_content.html().length === 0 || localThis.data('force_reload_month') === true)
            {
                to_reload = true;
            }

            multiday_info.hide();
            month_info.show();
            title_value = localThis.data('month_title');
        }
        else if(type === 'multiday')
        {
            if(multiday_content.html().length === 0 || localThis.data('force_reload_multiday') === true)
            {
                to_reload = true;
            }

            month_info.hide();
            multiday_info.show();
            title_value = localThis.data('multiday_title');
        }
                
        localThis.data('type', type);
        
        title_span.html(title_value);

        if(to_reload === true)
        {
            rebuildCalendar(localThis);
        }
    }
    
    function afterDaySelected(localThis, dayObj)
    {
//        if(dayObj.hasClass('_before_last_pay_day') || dayObj.hasClass('_not_in_work_relation') || dayObj.hasClass('_confirmed'))
        if(dayObj.hasClass('_before_last_pay_day') || dayObj.hasClass('_confirmed'))
        {
            $('#'+localThis.data('create_activity_button_id')).simaButton('disable');
        }
        else
        {
            $('#'+localThis.data('create_activity_button_id')).simaButton('enable');
        }
    }
    
    $.fn.simaActivityCalendar = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.vmenu');
            }
        });
        return ret;
    };
})( jQuery );


