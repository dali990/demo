/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global sima */

function SIMAActivities()
{
    this.onConfirmDate = function(date, person_id, calendar_id, boss_confirm)
    {        
        sima.ajax.get('activity/onConfirmDate',{
            get_params:{
                date:date,
                person_id: person_id,
                calendar_id: calendar_id,
                boss_confirm: boss_confirm
            },
            success_function:function(response)
            {
                sima.dialog.openYesNo(response.html,function(){
                    sima.dialog.close();
                    confirmDate(date, person_id, calendar_id, boss_confirm);
                });
            }
        });
    };
    
    this.onRevertConfirmDate = function(date, person_id, calendar_id, boss_confirm)
    {
        sima.ajax.get('activity/onRevertConfirmDate',{
            get_params:{
                date:date
            },
            success_function:function(response)
            {
                sima.dialog.openYesNo(response.html,function(){
                    sima.dialog.close();
                    sima.ajax.get('activity/revertConfirmDate',{
                        get_params:{
                            date:date,
                            person_id: person_id,
                            boss_confirm: boss_confirm
                        },
                        success_function:function()
                        {
                            $('#'+calendar_id).simaActivityCalendar('reloadCalendar');
                        }
                    });
                });
            }
        });
    };
    
    this.onDetailedConfirmDate = function(_this, date, person_id, calendar_id, boss_confirm)
    {
        sima.dialog.close();
        
        sima.ajaxLong.start('activity/onDetailedConfirmDate', {
            data: {
                date:date,
                person_id: person_id,
                boss_confirm: boss_confirm
            },
            showProgressBar: $('#'+calendar_id),
            onEnd: function(response){
                sima.dialog.openYesNo(response.html,function(){
                    sima.dialog.close();
                    confirmDate(date, person_id, calendar_id, boss_confirm);
                });
            },
            failure_function: function(response){
                sima.dialog.openWarn(response);
                sima.misc.refreshUiTabForObject($('#'+calendar_id));
            }
        });
    }
    
    function confirmDate(date, person_id, calendar_id, boss_confirm)
    {
        sima.ajax.get('activity/confirmDate',{
            get_params:{
                date:date,
                person_id: person_id,
                boss_confirm: boss_confirm
            },
            success_function:function(response)
            {
                $('#'+calendar_id).simaActivityCalendar('reloadCalendar');
            }
        });
    }
}
