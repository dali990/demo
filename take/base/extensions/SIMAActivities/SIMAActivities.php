<?php

class SIMAActivities extends CWidget 
{
    public function init()
    {
    }
    
    public function registerManual() 
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl .'/jquery.simaActivityCalendar.js');
        Yii::app()->clientScript->registerScriptFile($baseUrl .'/simaActivities.js');
        Yii::app()->clientScript->registerCssFile($baseUrl . '/activityCalendar.css');

        Yii::app()->clientScript->registerScript(
            SIMAHtml::uniqid(),
            "sima.activities = new SIMAActivities();", 
            CClientScript::POS_READY
        );
    }
}

