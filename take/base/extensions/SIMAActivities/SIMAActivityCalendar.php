<?php

class SIMAActivityCalendar extends CWidget 
{
    public $id = null;
    public $day = null;
    public $month = null;
    public $year = null;
    public $person_id = null;
    public $initial_type = null; /// day, week, month
    public $create_activity_button_id = null;
    
    public function run() 
    {
        if (empty($this->id))
        {
            $this->id = 'sima_activity_calendar_'.SIMAHtml::uniqid();
        }
        if(empty($this->day))
        {
            $this->day = date('d');
        }
        if(empty($this->month))
        {
            $this->month = date('m');
        }
        if(empty($this->year))
        {
            $this->year = date('Y');
        }
        if(empty($this->person_id))
        {
            throw new Exception(Yii::t('SIMAActivityManager.Activity', 'CalendarPersonIdEmpty'));
        }
        if(empty($this->initial_type))
        {
            $this->initial_type = 'month';
//            $this->initial_type = 'multiday';
        }
        else if($this->initial_type !== 'multiday'
                && $this->initial_type !== 'month')
        {
            throw new Exception(Yii::t('SIMAActivityManager.Activity', 'CalendarInitialTypeInvalid', [
                '{initial_type}' => CJSON::encode($this->initial_type)
            ]));
        }
        
        $simaTabs2id = SIMAHtml::uniqid().'_sima_tabs_2';
        $view = 'base.extensions.SIMAActivities.views.calendar.activity_calendar_wrapper';
        $tabs_params = [
            'id' => $simaTabs2id,
            'tabs' => [
                [
                    'title' => Yii::t('SIMAActivityManager.Activity', 'MultiDay'),
                    'html' => '<div class="sima-b-ac-tab-content sima-b-ac-content-multiday"></div>',
                    'code' => 'multiday'
                ],
                [
                    'title' => Yii::t('SIMAActivityManager.Activity', 'Month'),
                    'html' => '<div class="sima-b-ac-tab-content sima-b-ac-content-month"></div>',
                    'code' => 'month'
                ]
            ],
            'selected_code' => 'month'
        ];
        echo $this->render($view, [
            'tabs_params' => $tabs_params
        ]);
        
        $params = [
            'day' => $this->day,
            'month' => $this->month,
            'year' => $this->year,
            'person_id' => $this->person_id,
            'initial_type' => $this->initial_type,
            'simaTabs2id' => $simaTabs2id,
            'create_activity_button_id' => $this->create_activity_button_id
        ];
        $json = CJSON::encode($params);
        $register_script = "$('#" . $this->id . "').simaActivityCalendar('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
}

