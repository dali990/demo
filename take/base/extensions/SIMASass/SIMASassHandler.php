<?php

require_once dirname(__FILE__) . '/vendor/autoload.php';

/**
 * Primer registrovanje .scss fajla:
 * Yii::app()->sass->register('path-to-scss-file.scss', '', 'base.assets', 'css');
 * Treci parametar se zadaje kao alias putanje vec publisovanog direktorijuma u koji hocemo da publisujemo ovaj fajl, inace ce biti publisovan odvojeno.
 * Cetvrti parametar je podfolder vec publisovanog direktorijuma
 */
class SIMASassHandler extends SassHandler
{
    
}

