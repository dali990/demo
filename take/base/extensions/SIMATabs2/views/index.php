<div id='<?php echo $this->id?>' class='sima-ui-tabs2 sima-layout-panel _horizontal <?php echo $this->class; ?>'>
    <div class='sima-ui-tabs2-nav sima-layout-fixed-panel'>
        <!--<ul>-->
            <?php
            foreach($tabs as $tab)
            {
                $additional_classes = '';
                if($tab['code'] === $selected_code)
                {
                    $additional_classes = ' _selected ';
                }
                if(!empty($tab['class']))
                {
                    $additional_classes .= ' '.$tab['class'];
                }
            ?>
                <div 
                    class="sima-ui-tabs2-tab <?php echo $additional_classes; ?>" 
                    data-code="<?php echo $tab['code']; ?>">
                    <div class="_selected_marker"></div>
                    <div class="_title"><?php echo $tab['title']; ?></div>
                </div>
            <?php
            }
            ?>
        <!--</ul>-->
    </div>
    <?php
    foreach($tabs as $tab)
    {
        ?>
    <div class="sima-ui-tabs2-tab-content _selected sima-layout-panel <?php echo isset($tab['content_class'])?$tab['content_class']:''; ?>" 
         data-code="<?php echo $tab['code']; ?>"><?php echo $tab['html']; ?></div>
        <?php
    }
    ?>
</div>