<?php

/**
 * Primer zadavanja:

$this->widget('SIMATabs2', [
    'tabs' => [
        [
            'title' => 'title1',
            'code' => 'code1',
            'html' => 'html1'
        ],
        [
            'title' => 'title2',
            'code' => 'code2',
            'html' => 'html2'
        ]
    ]
]);

 */

class SIMATabs2 extends CWidget 
{
    public $id = null;
    public $class = '';
    public $tabs = null;
    public $selected_code = null;
    
    public function run() 
    {
        if(empty($this->tabs))
        {
            throw new Exception(Yii::t('SIMATabs2.Common', 'TabsParameterEmpty'));
        }
        if(empty($this->id))
        {
            $this->id = SIMAHtml::uniqid().'_sima_tabs_2';
        }
        if(empty($this->selected_code))
        {
            $this->selected_code = $this->tabs[0]['code'];
        }
        
        $view_params = [
            'id' => $this->id,
            'tabs' => $this->tabs,
            'selected_code' => $this->selected_code
        ];
        
        echo $this->render('index', $view_params);
        
        $js_params = array(
        );
        $json_params = CJSON::encode($js_params);
        $register_script = "$('#" . $this->id . "').simaTabs2('init',$json_params);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
    
    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.simaTabs2.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/layoutAllignHandler.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaTabs2.css');
    }
}

