
/* global sima */

(function($){
    var methods = {
        init: function(params){  
            var localThis = $(this);
            
            localThis.on('click','.sima-ui-tabs2-tab',{tabs2:localThis}, on_click);
            initTabs(localThis);
        },
        get_selected_code: function()
        {
            var localThis = $(this);
            var selected_tab = localThis.find('.sima-ui-tabs2-tab._selected');
            var selected_code = selected_tab.data('code');
            return selected_code;
        }
    };
    
    function on_click(event)
    {
        var _this = $(this);
        var tabs2 = event.data.tabs2;  
        if(!_this.hasClass('_selected'))
        {
            tabs2.find('.sima-ui-tabs2-tab').removeClass('_selected');
            _this.addClass('_selected');
            displaySelectedTab(tabs2);
        }
    };
    
    function displaySelectedTab(localThis)
    {
        var selected_tab = localThis.find('.sima-ui-tabs2-tab._selected');
        var selected_code = selected_tab.data('code');
        var contents = localThis.children('.sima-ui-tabs2-tab-content');

        $.each(contents, function(){
            var _this = $(this);
            var contet_code = _this.data('code');
            
            if(contet_code === selected_code)
            {
                _this.addClass('_selected');
                _this.removeClass('sima-layout-broken');
                sima.layout.allignFirstLayoutParent(_this,'TRIGGER_displaySelectedTab');
            }
            else
            {
                _this.removeClass('_selected');
            }
        });
                
        localThis.trigger('displaySelectedTab', [{
            selected_code: selected_code
        }]);
    };
    function initTabs(localThis)
    {
        //bolje je children ako imaju tabovi u tabovima
        var tabs_nav = localThis.children('.sima-ui-tabs2-nav');
        var tabs = tabs_nav.children('.sima-ui-tabs2-tab');
        var number_of_tabs = tabs.length;
        
        var width_percent = 100/number_of_tabs;
        
        tabs.css('width',width_percent+'%');
        displaySelectedTab(localThis);
    };
    
    $.fn.simaTabs2 = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.simaTabs2');
            }
        });
        return ret;
    };
})( jQuery );
