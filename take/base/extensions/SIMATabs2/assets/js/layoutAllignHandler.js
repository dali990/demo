
/* global sima */

$('body').on('sima-layout-allign','.sima-ui-tabs2',function(event, height, width, source){
    if (event.target === this)
    {
        if (sima.layout.log())
        {
            console.log(source);
            console.log('HANDLER_sima_tabs2_simaresize');
        }

        var _tabs2 = $(this);
        _tabs2.height(height);
        _tabs2.width(width);

        var fixed_height = 0;
        _tabs2.children('.sima-ui-tabs2-nav').each(function(){
            fixed_height += $(this).outerHeight(true);
        });

        var _panels_height = height - fixed_height;
        _tabs2.children('div.sima-ui-tabs2-tab-content').addClass('sima-layout-broken');
        _tabs2.children('div.sima-ui-tabs2-tab-content').each(function(){
            var _panel = $(this);
            _panel.height(_panels_height);
            _panel.width(width);
        });
        var selected_tab = _tabs2.children('.sima-ui-tabs2-tab-content._selected');
        selected_tab.removeClass('sima-layout-broken');
        sima.layout.setLayoutSize(selected_tab,_panels_height,width,'TRIGGER_sima_tabs2_resize');

        event.stopPropagation();
    }
});