<?php

class SIMASpanWithInfoBox extends CWidget 
{
    public $id = null;
    public $info_box_id = null;
    public $span_text = null;
    public $infoBoxHTML = '';
    public $span_classes = '';
    public $info_box_classes = '';
    public $mouseoverAction = null;
    
    public function run() 
    {
        $uniq = SIMAHtml::uniqid();
        
        if(empty($this->id))
        {
            $this->id = $uniq.'_swib';
        }
        if(empty($this->info_box_id))
        {
            $this->info_box_id = $uniq.'_swib_ib';
        }
        
        if(empty($this->span_text))
        {
            throw new Exception('span text empty');
        }
        
        echo $this->render('index', [
            'id' => $this->id,
            'spanText' => $this->span_text,
            'infoBoxHTML' => $this->infoBoxHTML,
            'span_classes' => $this->span_classes,
            'info_box_classes' => $this->info_box_classes,
            'info_box_id' => $this->info_box_id
        ]);
        
        $params = [
            'info_box_id' => $this->info_box_id
        ];
        $json = CJSON::encode($params);
        $register_script = "$('#" . $this->id . "').spanWithInfoBox('init',$json);";
        Yii::app()->clientScript->registerScript($uniq, $register_script, CClientScript::POS_READY);
    }
    
    public static function registerManual() 
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl .'/jquery.spanWithInfoBox.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/spanWithInfoBox.css');
    }
}
