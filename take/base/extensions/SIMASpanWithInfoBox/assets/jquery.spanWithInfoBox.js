/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global sima */

(function($){
    var methods = {
        init: function(params){
            var localThis = this;
            
            localThis.data('info_box_id', params.info_box_id);
            
            var popup_obj = $('#'+params.info_box_id);
            popup_obj.simaPopup('init',$('body'),localThis);
            popup_obj.hide();
            localThis.on('mouseenter', function(){
                if(!popup_obj.is(":visible"))
                {
                    popup_obj.show();
                    popup_obj.simaPopup('refresh');
                }
            });
            localThis.on('mouseleave', function(){
                setTimeout(function(){
                    if(popup_obj.data('hovered') !== true)
                    {
                        popup_obj.hide('fast');
                    }
                }, 1);
            });
            popup_obj.on('mouseenter', function(){
                $(this).data('hovered', true);
            });
            popup_obj.on('mouseleave', function(){
                $(this).data('hovered', false);
                popup_obj.hide('fast');
            });
        },
        set_info_box_html: function(html)
        {
            var localThis = this;
            
            var popup_obj = $('#'+localThis.data('info_box_id'));
            
            popup_obj.html(html);
        }
    };
    
    $.fn.spanWithInfoBox = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.spanWithInfoBox');
            }
        });
        return ret;
    };
})( jQuery );
