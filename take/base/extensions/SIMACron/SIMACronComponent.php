<?php

class SIMACronComponent
{
    private $jobs = [];
    
    public function init()
    {
        
    }
    
    public function getJobs()
    {
        return $this->jobs;
    }
    
    public function includeJobs($new_jobs, $rethrow_exceptions=false)
    {
        /// samo kad je konzola i to eventrelay timecheck akcija
        
        $jobs_to_add = $this->includeJobs_parse($new_jobs, $rethrow_exceptions);
        
        if(!empty($jobs_to_add))
        {
            $this->jobs = array_merge_recursive($this->jobs, $jobs_to_add);
        }
    }
    
    /**
     * forsira da se za sve izracuna ponovo vreme novog izvrsavanja
     */
    public function recalculateNewIntervalForAll()
    {
        $jobs = $this->jobs;
        $new_jobs = $this->recalculateNewIntervalForArray($jobs);
        $this->jobs = $new_jobs;
    }
    
    /**
     * forsira da se za sve izracuna ponovo vreme novog izvrsavanja
     */
    public function recalculateNewIntervalForArray(array $jobs=[], $rethrow_exceptions=false)
    {
        $parsed_jobs = [];
                
        try
        {
            foreach($jobs as $job_interval => $job_classes)
            {
                $parsed_jobs_for_interval = $this->recalculateNewIntervalForArray_classes($job_interval, $job_classes, $rethrow_exceptions);
                $parsed_jobs[$job_interval] = $parsed_jobs_for_interval;
            }
        }
        catch(Exception $e)
        {
            if($rethrow_exceptions === true)
            {
                throw $e;
            }
            error_log(__METHOD__.' - $e message: '.$e->getMessage());
        }
        
        return $parsed_jobs;
    }
    
    public function setJobClassIntervalNewNextWorkInterval($job_interval, $job_class, $next_work_interval)
    {
        $this->jobs[$job_interval][$job_class] = $next_work_interval;
    }
    
    private function recalculateNewIntervalForArray_classes($job_interval, $job_classes, $rethrow_exceptions=false)
    {
        $parsed_jobs_for_interval = [];
        
        try
        {
            foreach($job_classes as $job_class => $old_interval)
            {
                if(!is_subclass_of($job_class, SIMACron::class))
                {
                    throw new Exception("$job_class does not extend SIMACron");
                }

                $calc = new SIMACronNextIntervalCalc($job_interval);
                $calc->calc();
                $new_interval = $calc->getResult();

                $parsed_jobs_for_interval[$job_class] = $new_interval;
            }
        }
        catch(Exception $e)
        {
            if($rethrow_exceptions === true)
            {
                throw $e;
            }
            error_log(__METHOD__.' - $e message: '.$e->getMessage());
        }
        
        return $parsed_jobs_for_interval;
    }
    
    private function includeJobs_parse(array $jobs=[], $rethrow_exceptions=false)
    {
        $parsed_jobs = [];
                
        try
        {
            foreach($jobs as $job)
            {
                if(!is_array($job))
                {
                    throw new Exception('job not array - $job: '.SIMAMisc::toTypeAndJsonString($job));
                }
                
                if(count($job) !== 1)
                {
                    throw new Exception('job not having one element - $job: '.SIMAMisc::toTypeAndJsonString($job));
                }
                
                $job_array_keys = array_keys($job);
                $job_interval = $job_array_keys[0];
                $job_class = $job[$job_interval];
                                
                if(!is_subclass_of($job_class, SIMACron::class))
                {
                    throw new Exception("$job_class does not extend SIMACron");
                }
                
                if(!isset($parsed_jobs[$job_interval]))
                {
                    $parsed_jobs[$job_interval] = [];
                }
                
                $parsed_jobs[$job_interval][$job_class] = -1;
            }
        }
        catch(Exception $e)
        {
            if($rethrow_exceptions === true)
            {
                throw $e;
            }
            error_log(__METHOD__.' - $e message: '.$e->getMessage());
        }
        
        return $parsed_jobs;
    }
}

