<?php

class SIMACronNextIntervalCalc
{
    private $_current_timestamp;
    private $_result;
    private $_interval;
    private $_interval_exploded;
    private $_interval_seconds_part;
    private $_interval_minutes_part;
    private $_interval_hours_part;
    private $_interval_day_of_month_part;
    private $_interval_month_of_year_part;
    private $_interval_day_of_week_part;
    private $_interval_year_part;
    
    public function __construct($interval)
    {
        $this->_current_timestamp = time();
        
        /// koristi se samo u testovima, ne bi smelo nigde vise
        if(isset($_SESSION['SIMACronNextIntervalCalc_fixed_time']))
        {
            $this->_current_timestamp = $_SESSION['SIMACronNextIntervalCalc_fixed_time'];
        }
        
        $this->_interval = $interval;
        $this->_result = $this->_current_timestamp;
        
        $this->_interval_exploded = explode(' ', $this->_interval);
        if(count($this->_interval_exploded) !== 7)
        {
            throw new Exception("interval nepravilnog oblika: '$interval'");
        }
        
        $this->_interval_seconds_part = $this->_interval_exploded[0];
        $this->_interval_minutes_part = $this->_interval_exploded[1];
        $this->_interval_hours_part = $this->_interval_exploded[2];
        $this->_interval_day_of_month_part = $this->_interval_exploded[3];
        $this->_interval_month_of_year_part = $this->_interval_exploded[4];
        $this->_interval_day_of_week_part = $this->_interval_exploded[5];
        $this->_interval_year_part = $this->_interval_exploded[6];
    }
    
    public function calc()
    {
        $this->calcSeconds();
        $this->calcMinutes();
        $this->calcHours();
        $this->calcDayOfMonth();
        $this->calcMonth();
        $this->calcDayOfWeek();
        $this->calcYear();
    }
    
    public function getResult()
    {
        return $this->_result;
    }
    
    private function calcSeconds()
    {
//        error_log(__METHOD__.' - $this->_interval_seconds_part: '.SIMAMisc::toTypeAndJsonString($this->_interval_seconds_part));
//        error_log(__METHOD__.' - is_int($this->_interval_seconds_part): '.is_int($this->_interval_seconds_part));
        if($this->_interval_seconds_part === '*')
        {
            /// TODO: provera da li su svi ostali sa *, to ne sme jer bi bila provera na svakih sekund
            throw new Exception('not implemented');
        }
        else if(ctype_digit($this->_interval_seconds_part))
        {
            $interval_seconds_part_int = (int)$this->_interval_seconds_part;
            
            if($interval_seconds_part_int < 5)
            {
                /// TODO: provera da li su svi ostali sa *, to ne sme jer bi bila provera na svakih manje od 5 sekundi
//                throw new Exception('$interval_seconds_part ne moze biti manji od 5 - $interval: '.$interval);
            }

            $this->_result = (int)(floor($this->_current_timestamp/$interval_seconds_part_int)*$interval_seconds_part_int);
//            error_log(__METHOD__.' - $this->_result: '.SIMAMisc::toTypeAndJsonString($this->_result));
            $this->_result += $interval_seconds_part_int;
        }
        else if(strpos($this->_interval_seconds_part, '*/') !== false)
        {
            $number_part = substr($this->_interval_seconds_part, 2);
            
            if($number_part < 5)
            {
                /// TODO: provera da li su svi ostali sa *, to ne sme jer bi bila provera na svakih manje od 5 sekundi
            }
            
            $this->_result = (int)(floor($this->_current_timestamp/$number_part)*$number_part);
            $this->_result += $number_part;
        }
        else if(strpos($this->_interval_seconds_part, '-') !== false)
        {
//            /// TODO: not implemented
            throw new Exception('not implemented');
        }
    }
    
    private function calcMinutes()
    {
        if($this->_interval_minutes_part !== '*')
        {
            throw new Exception('minutes not implemented');
        }
    }
    
    private function calcHours()
    {
        if($this->_interval_hours_part !== '*')
        {
            throw new Exception('hours not implemented');
        }
    }
    
    private function calcDayOfMonth()
    {
        if($this->_interval_day_of_month_part !== '*')
        {
            throw new Exception('day of month not implemented');
        }
    }
    
    private function calcMonth()
    {
        if($this->_interval_month_of_year_part !== '*')
        {
            throw new Exception('month of year not implemented');
        }
    }
    
    private function calcDayOfWeek()
    {
        if($this->_interval_day_of_week_part !== '*')
        {
            throw new Exception('day of week not implemented');
        }
    }
    
    private function calcYear()
    {
        if($this->_interval_year_part !== '*')
        {
            throw new Exception('year not implemented');
        }
    }
}

