<?php
foreach($basic_informations as $basic_information)
{
    ?>
    <div class='s-ei-row'>
    <?php
    if($basic_information['type'] === SIMAExpandableInformations::$TYPE_LABEL_VALUE)
    {
    ?>
            <div class='s-ei-label'>
                <?php echo $basic_information['label'].':'; ?>
            </div>
            <div class="s-ei-pre_value">
                <?php
                    if(isset($basic_information['pre_value']))
                    {
                        echo $basic_information['pre_value'];
                    }
                ?>
            </div>
            <div class='s-ei-value sima-text-dots' title="<?php // echo $basic_information['value']; ?>">
                <?php echo $basic_information['value']; ?>
            </div>
            <div class="s-ei-buttons <?php if(isset($basic_information['buttons_params_always_visible']) && $basic_information['buttons_params_always_visible']===true) echo '_always_visible' ?>">
                <?php
                    if(isset($basic_information['buttons_params']))
                    {
                        foreach($basic_information['buttons_params'] as $button_params)
                        {
                            $this->widget(SIMAButton::class, $button_params);
                        }
                    }
                    
                    if(isset($basic_information['copy_button_value']))
                    {
                        if(SIMAMisc::isVueComponentEnabled())
                        {
                            $this->widget(SIMAButtonVue::class, [
                                'icon'=>'_copy',
                                'iconsize' => 18,
                                'additional_classes' => ['sima-b-info-btn-copy'],
                                'icon_additional_classes' => ['_black'],
                                'onclick'=>['sima.misc.copyToClipboard', $basic_information['copy_button_value']],
                            ]);
                        }
                        else
                        {
                            $this->widget(SIMAButton::class, [
                                'class' => 'sima-b-info-btn-copy',
                                'action'=>[
                                    'icon'=>'_copy _black _18',
                                    'onclick'=>['sima.misc.copyToClipboard', $basic_information['copy_button_value']],
                                ]
                            ]);
                        }
                    }
                ?>
            </div>
    <?php
    }
    else
    {
        $title = strip_tags($basic_information['html']);
        if(isset($basic_information['no_title']) && $basic_information['no_title'] == true)
        {
            $title = '';
        }
//        throw new Exception('not implemented');
        ?>
            <div class='s-ei-value sima-text-dots' title="<?=$title?>">
                <?=$basic_information['html']?>
            </div>
        <?php
    }
    ?>
    </div>
    <?php
}