<?php
$additional_classes = '';

if($expanded_informations_absolute === true)
{
//    $additional_classes = '_absolute _hidden';
}
else
{
    $additional_classes = '_not_absolute';
}
?>

<div id='<?=$expanded_informations_id?>' class="s-ei-ei <?=$additional_classes?>">
<?php
    foreach($expanded_informations as $expanded_information)
    {
        ?>
        <div class='s-ei-inner-row'>
        <?php
        if($expanded_information['type'] === SIMAExpandableInformations::$TYPE_LABEL_VALUE)
        {
        ?>
                <div class='s-ei-label <?=isset($expanded_information['label_additional_classes'])?$expanded_information['label_additional_classes']:''?>'>
                    <?php echo $expanded_information['label'].':'; ?>
                </div>
                <div class="s-ei-pre_value">
                    <?php
                        if(isset($expanded_information['pre_value']))
                        {
                            echo $expanded_information['pre_value'];
                        }
                    ?>
                </div>
                <div class='s-ei-value sima-text-dots'>
                    <?php echo $expanded_information['value']; ?>
                </div>
                <div class="s-ei-buttons">
                    <?php
                        if(isset($expanded_information['buttons_params']))
                        {
                            foreach($expanded_information['buttons_params'] as $button_params)
                            {
                                $this->widget(SIMAButton::class, $button_params);
                            }
                        }
                        
                        if(isset($expanded_information['copy_button_value']))
                        {
                            if(SIMAMisc::isVueComponentEnabled())
                            {
                                $this->widget(SIMAButtonVue::class, [
                                    'additional_classes' => ['sima-b-info-btn-copy'],
                                    'icon' => '_copy',
                                    'icon_additional_classes' => ['_black'],
                                    'iconsize' => 18,
                                    'onclick'=>['sima.misc.copyToClipboard', $expanded_information['copy_button_value']]
                                ]);
                            }
                            else
                            {
                                $this->widget(SIMAButton::class, [
                                    'class' => 'sima-b-info-btn-copy',
                                    'action'=>[
                                        'icon'=>'_copy _black _18',
                                        'onclick'=>['sima.misc.copyToClipboard', $expanded_information['copy_button_value']]
                                    ]
                                ]);
                            }
                        }
                    ?>
                </div>
        <?php
        }
        else
        {
            echo $expanded_information['html'];
        }
        ?>
        </div>
        <?php
    }
?>
</div>