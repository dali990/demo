<?php
class SIMAExpandableInformationsBasicInformationsEmpty extends SIMAException
{
    public function __construct()
    {
        $err_msg = 'basic info empty';
        parent::__construct($err_msg);
    }
}

