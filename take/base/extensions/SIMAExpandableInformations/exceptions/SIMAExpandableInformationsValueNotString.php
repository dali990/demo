<?php

class SIMAExpandableInformationsValueNotString extends SIMAException
{
    public function __construct($value)
    {
        error_log(__METHOD__.' - $value: '.CJSON::encode($value));
        
        $err_msg = 'value not string - '.gettype($value);
        parent::__construct($err_msg);
    }
}
