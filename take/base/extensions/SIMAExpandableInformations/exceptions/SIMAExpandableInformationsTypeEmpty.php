<?php

class SIMAExpandableInformationsTypeEmpty extends SIMAException
{
    public function __construct($information)
    {        
        $err_msg = 'type empty - $information: '.CJSON::encode($information);
        parent::__construct($err_msg);
    }
}
