<?php

class SIMAExpandableInformationsItemNotArray extends SIMAException
{
    public function __construct($item)
    {        
        $err_msg = 'item not array. type: '.gettype($item).'. item: '.CJSON::encode($item);
        parent::__construct($err_msg);
    }
}
