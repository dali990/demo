/* global sima */

(function($)
{
    var methods = {
        init: function({
            expanded_informations_absolute: expanded_informations_absolute,
            expanded_informations_id: expanded_informations_id
        })
        {            
            var _this = $(this);
            
            _this.data('expanded_informations_absolute', expanded_informations_absolute);
            _this.data('expanded_informations_id', expanded_informations_id);
            _this.data('expanded_shown', false);
            
            if(!sima.isEmpty(expanded_informations_id))
            {
                if(_this.data('expanded_informations_absolute') === true)
                {
                        var parent_to_bind_to = null;
                        if(sima.misc.isInDialog(_this))
                        {
                            parent_to_bind_to = sima.misc.getDialogParent(_this);
                        }
                        else
                        {
                            parent_to_bind_to = sima.misc.getMainTabContentForObject(_this);
                        }

                        var expanded_informations = $('#'+_this.data('expanded_informations_id'));
                        expanded_informations.simaPopup('init',parent_to_bind_to, _this.find('.s-ei-expand_button'), 'left', {
                            //MilosS(13.3.2018.): mislim da ne mora da se pomera, dovoljno je da se skloni, a MOVE opterecuje
//                            on_position_changed_reaction: 'MOVE',
//                            on_position_changed_reaction: 'HIDE',//default
                            on_position_changed_container: _this.getScrollParent()
                        });
                        expanded_informations.hide();
                        expanded_informations.on('sima-popup-shown', {_this:_this}, onSimaPopupShown);
                        expanded_informations.on('sima-popup-hidden', {_this:_this}, onSimaPopupHidden);
                        expanded_informations.bind("mousedownoutside", function(e){
                            if(!_this.is(e.target) && !$.contains(_this[0], e.target) && _this.data('expanded_shown') === true)
                            {
                                expandContract_toggleExpandInformations(_this);
                            }
                        });
                }
                else
                {
                    var expanded_informations = $('#'+_this.data('expanded_informations_id'));
                    expanded_informations.hide();
                }
            }
        },
        expandContract: function()
        {
            var _this = $(this);
            
            if(_this.data('expanded_informations_absolute') === false)
            {
                expandContract_changeIcon(_this);
            }
            expandContract_toggleExpandInformations(_this);
        }
    };
    
    function onSimaPopupShown(event)
    {
        expandContract_changeIcon(event.data._this);
    }
    function onSimaPopupHidden(event)
    {
        expandContract_changeIcon(event.data._this);
    }
    
    function expandContract_changeIcon(_this)
    {
        if(_this.data('expanded_shown') === true)
        {
            _this.data('expanded_shown', false);
            _this.find('.s-ei-expand_button').removeClass('_close _black _18').addClass('_expand-arrows _18');
        }
        else
        {
            _this.data('expanded_shown', true);
            _this.find('.s-ei-expand_button').removeClass('_expand-arrows _18').addClass('_close _black _18');
        }
    }
    
    function expandContract_toggleExpandInformations(_this)
    {
        if(_this.data('expanded_informations_absolute') === true)
        {
            if(!sima.isEmpty(_this.data('expanded_informations_id')))
            {
                var expanded_informations = $('#'+_this.data('expanded_informations_id'));
                expanded_informations.simaPopup('toggle');

                var current_width = expanded_informations.width();
                var new_width = _this.find('.s-ei-data').width();

                if(current_width !== new_width)
                {
                    expanded_informations.width(new_width);
                }

                expanded_informations.simaPopup('refresh');
            }
        }
        else
        {
            var expanded_informations = $('#'+_this.data('expanded_informations_id'));
            var basic_informations = _this.find('.s-ei-data');
            basic_informations.toggle('fast');
            expanded_informations.toggle('fast');
        }
    }
    
    $.fn.simaExpandableInformations = function(methodOrOptions) 
    {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.simaExpandableInformations');
            }
        });
        return ret;
    };
})( jQuery );
