<?php

class SIMAExpandableInformations extends CWidget 
{
    public static $TYPE_HTML = 'html';
    public static $TYPE_LABEL_VALUE = 'label_value';
    
    public $id = null;
    public $basic_informations = [];
    public $expanded_informations = [];
    public $expanded_informations_absolute = true;
    
    public function run()
    {
        if ($this->id == '')
        {
            $this->id = SIMAHtml::uniqid();
        }
        
        $this->validateInput();
        
        $basic_informations_html = $this->local_render('basic_informations', [
            'basic_informations' => $this->basic_informations
        ]);
        
        $have_expanded_informations = !empty($this->expanded_informations);
        $expanded_informations_id = SIMAHtml::uniqid();
        
        $view_params = [
            'id' => $this->id,
            'basic_informations_html' => $basic_informations_html,
            'have_expanded_informations' => $have_expanded_informations
        ];
        
        if($have_expanded_informations)
        {
            $view_params['expanded_informations_html'] = $this->local_render('expanded_informations', [
                'id' => $this->id,
                'expanded_informations' => $this->expanded_informations,
                'expanded_informations_absolute' => $this->expanded_informations_absolute,
                'expanded_informations_id' => $expanded_informations_id
            ]);
        }
        
        echo $this->render('index', $view_params, true);
        
        $params = [
//            'form_id' => $this->form_id,
//            'form_input_data_id' => $this->form_input_data_id,
//            'repmanager_upload' => $this->repmanager_upload,
//            'allowed_extansions'=>$this->allowed_extansions
            'expanded_informations_absolute' => $this->expanded_informations_absolute,
            'expanded_informations_id' => $expanded_informations_id
        ];
        if($have_expanded_informations)
        {
//            $params['expanded_informations_html'] = $this->local_render('expanded_informations', [
//                'expanded_informations' => $this->expanded_informations,
//                'expanded_informations_absolute' => $this->expanded_informations_absolute,
//                'expanded_informations_id' => $expanded_informations_id
//            ]);
        }
        $json = CJSON::encode($params);
        $register_script = "$('#" . $this->id . "').simaExpandableInformations('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
    
    public function registerManual()
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        
        Yii::app()->clientScript->registerScriptFile($baseUrl .'/jquery.simaExpandableInformations.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/style.css');
    }
    
    private function validateInput()
    {
        if(empty($this->basic_informations))
        {
            throw new SIMAExpandableInformationsBasicInformationsEmpty();
        }
        
        if(!is_array($this->basic_informations))
        {
            throw new SIMAExpandableInformationsBasicInformationsNotArray();
        }
        
        foreach($this->basic_informations as $basic_information)
        {
            $this->validateInput_singleInformation($basic_information);
        }
        
        if(!empty($this->expanded_informations))
        {
            if(!is_array($this->expanded_informations))
            {
                throw new SIMAExpandableInformationsExpandedInformationsNotArray();
            }
            
            foreach($this->expanded_informations as $expandable_information)
            {
                $this->validateInput_singleInformation($expandable_information);
            }
        }
    }
    private function validateInput_singleInformation($information)
    {
        if(!is_array($information))
        {
            throw new SIMAExpandableInformationsItemNotArray($information);
        }

        if(!isset($information['type']))
        {
            throw new SIMAExpandableInformationsTypeEmpty($information);
        }

        if(
            $information['type'] !== SIMAExpandableInformations::$TYPE_HTML
            &&
            $information['type'] !== SIMAExpandableInformations::$TYPE_LABEL_VALUE
        )
        {
            throw new SIMAExpandableInformationsTypeInvalid($information['type']);
        }

        if($information['type'] === SIMAExpandableInformations::$TYPE_HTML)
        {
//            throw new Exception('not implemented');
            if(!isset($information['html']))
            {
                throw new SIMAExpandableInformationsHtmlEmpty();
            }

            if(!is_string($information['html']))
            {
                throw new SIMAExpandableInformationsHtmlNotString();
            }
        }
        else if($information['type'] === SIMAExpandableInformations::$TYPE_LABEL_VALUE)
        {
            if(!isset($information['label']))
            {
                throw new SIMAExpandableInformationsLabelEmpty();
            }
            if(!is_string($information['label']))
            {
                throw new SIMAExpandableInformationsLabelNotString();
            }

            if(isset($information['value']) && !is_string($information['value']))
            {
                throw new SIMAExpandableInformationsValueNotString($information['value']);
            }
        }
    }
    
    private function local_render($view, $params)
    {
        $view = 'base.extensions.SIMAExpandableInformations.views.'.$view;
                
        return $this->render($view, $params, true);
    }
}
