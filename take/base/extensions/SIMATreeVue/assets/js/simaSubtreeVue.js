
/* global Vue, sima */

Vue.component('sima-subtree', {
    template: '#sima-subtree-template',
    props: {
        main_vue_obj: Object,
        tree_data: Array,
        subtree_level: Number
    },
    data: function () {
        return {
            ordered_tree_data: this.orderTreeData(this.tree_data)
        };
    },
    watch: {
        tree_data: function () {
            this.ordered_tree_data = this.orderTreeData(this.tree_data);
        }
    },
    methods: {
        expandRecursively: function() {
            if (typeof this.$parent.expandRecursively === 'function')
            {
                this.$parent.expandRecursively();
            }
        },
        isSelectedItemInside: function() {
            if (this.$children.length > 0)
            {
                var is_inside = false;
                $.each(this.$children, function(index, value) {
                    if (typeof this.isSelectedItemInside === 'function' && this.isSelectedItemInside() === true)
                    {
                        is_inside = true;
                        return true;
                    }
                });

                return is_inside;
            }
            
            return false;
        },
        selectFirstSelectableParent: function(event) {
            if (this.$parent.is_selectable === true)
            {
                this.$parent.select(event, false);
            }
            else if (typeof this.$parent.selectFirstSelectableParent === 'function')
            {
                this.$parent.selectFirstSelectableParent(event);
            }
            else
            {
                //hvatamo prvi children
                if(typeof this.$children[0] !== 'undefined' && this.$children[0].is_selectable === true)
                {
                    this.$children[0].select(event, false);
                }
                else if (!sima.isEmpty(this.main_vue_obj.selected_item))
                {
                    this.main_vue_obj.selected_item.unselect(event);
                }
            }
        },
        selectItemByCode: function(code) {
            this.$parent.selectItemByCode(code);
        },
        orderTreeData: function(tree_data) {
            var _ordered_tree_data = [];
            $.each(tree_data, function(index, value) {
                
                if (typeof value.order === 'undefined')
                {
                    _ordered_tree_data.push(value);
                }
                else
                {
                    var corresponding_index = null;
                    $.each(_ordered_tree_data, function(index1, value1) {
                        if (corresponding_index === null)
                        {
                            if (typeof value1.order !== 'undefined')
                            {
                                if (parseInt(value.order) < parseInt(value1.order))
                                {
                                    corresponding_index = index1;
                                }
                            }
                            else
                            {
                                corresponding_index = index1;
                            }
                        }
                    });

                    if (corresponding_index !== null)
                    {
                        _ordered_tree_data.splice(corresponding_index, 0, value);
                    }
                    else
                    {
                        _ordered_tree_data.push(value);
                    }
                }
            });
            
            return _ordered_tree_data;
        }
    }
});