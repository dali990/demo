
/* global Vue, sima */

Vue.component('sima-tree', {
    template: '#sima-tree-template',
    props: {
        tree_data: {
            type: Array,
            default: function() {
                return [];
            }
        },
        load_on_init: {
            type: Boolean,
            default: true
        },
        item_load_action: {
            type: String,
            default: ''
        },
        item_load_params: {
            type: Array,
            default: function() {
                return [];
            }
        },
        default_selected: {
            type: String,
            default: ''
        },
        show_subtree_cnt: {
            type: Boolean,
            default: false
        },
        disable_unselect_item: {
            type: Boolean,
            default: false
        },
        hide_item_without_content: {
            type: Boolean,
            default: false
        },
        additional_class: String
    },
    data: function () {
        return {
            main_vue_obj: new Vue({
                data: {
                    selected_item: null, //pamtimo trenutno selektovani item
                    item_load_action: this.item_load_action,
                    item_load_params: this.item_load_params,
                    disable_unselect_item: this.disable_unselect_item,
                    show_subtree_cnt: this.show_subtree_cnt,
                    hide_item_without_content: this.hide_item_without_content
                }
            }),
            local_tree_data: this.tree_data,
            repacked_tree_data: this.repackTreeData(this.tree_data),
            is_default_selected_applied: false,
            subtree_level: 1
        };
    },
    watch: {
        tree_data: function () {
            this.local_tree_data = this.tree_data;
        },
        local_tree_data: function() {
            var _this = this;

            this.repacked_tree_data = this.repackTreeData(this.local_tree_data);

            this.applyDefaultSelected();
            
            //nakon promene podataka moramo selektovati opet item koji je bio selektovan
            if (this.main_vue_obj.selected_item !== null)
            {
                var selected_item_code = this.main_vue_obj.selected_item.item_data.code;
                this.$nextTick(function() {
                    _this.selectItemByCode(selected_item_code);
                });
            }
        }
    },
    computed: {
        subtree_level_class: function() {
            return '_level' + this.subtree_level;
        }
    },
    mounted: function() {
        var _this = this;

        if (sima.isEmpty(_this.local_tree_data) && _this.load_on_init === true)
        {
            _this.load();
        }
        
        //ovde emitujemo sve evente koji mogu da se nakace spolja. Mora odavde da se vrsi emitovanje, jer je ovo glavna vue komponenta za sima tree.
        //sve podkomponente(tree subtree, tree item) emituju evente preko main_vue_obj, a ovde ih hvatamo i emitujemo opet nad this, kako bi bile dostupne spolja
        this.main_vue_obj.$on('afterTreeItemLoad', function(tree_item) {
            _this.$emit('afterTreeItemLoad', tree_item);
        });
        this.main_vue_obj.$on('afterTreeItemExpand', function(tree_item) {
            _this.$emit('afterTreeItemExpand', tree_item);
        });
        this.main_vue_obj.$on('afterTreeItemCollapse', function(tree_item) {
            _this.$emit('afterTreeItemCollapse', tree_item);
        });
        this.main_vue_obj.$on('afterTreeItemSelect', function(tree_item) {
            _this.$emit('afterTreeItemSelect', tree_item);
        });
        this.main_vue_obj.$on('afterTreeItemUnselect', function(tree_item) {
            _this.$emit('afterTreeItemUnselect', tree_item);
        });
        this.main_vue_obj.$on('afterTreeItemSelectionChanged', function(tree_item, selection_type) {
            _this.$emit('afterTreeItemSelectionChanged', tree_item, selection_type);
        });
        
        _this.applyDefaultSelected();

        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        load: function(success_func) {
            if (!sima.isEmpty(this.item_load_action))
            {
                var _this = this;
                sima.ajax.get(this.item_load_action, {
                    get_params: {
                        parent_code: null
                    },
                    data: {
                        params: this.item_load_params,
                        item_data: {}
                    },
                    async: true,
                    loadingCircle: $(this.$el),
                    success_function:function(response)
                    {
                        _this.local_tree_data = response.subtree;
                        _this.afterLoad(success_func);
                    }
                });
            }
            else
            {
                this.afterLoad(success_func);
            }
        },
        afterLoad: function(success_func) {
            if (typeof success_func !== 'undefined')
            {
                sima.executeFunction(success_func);
            }
            
            this.$emit("afterTreeLoad");
        },
        isSelectedItemInside: function() {
            if (this.$children.length > 0)
            {
                var is_inside = false;
                $.each(this.$children, function(index, value) {
                    if (typeof this.isSelectedItemInside === 'function' && this.isSelectedItemInside() === true)
                    {
                        is_inside = true;
                        return true;
                    }
                });

                return is_inside;
            }
            
            return false;
        },
        selectFirstSelectableParent: function(event) {
            //hvatamo prvi children
            if(typeof this.$children[0] !== 'undefined' && this.$children[0].is_selectable === true)
            {
                this.$children[0].select(event, false);
            }
            else if (!sima.isEmpty(this.main_vue_obj.selected_item))
            {
                this.main_vue_obj.selected_item.unselect(event);
            }
        },
        reload: function(tree_data) {
            this.local_tree_data = tree_data;
        },
        selectItemByCode: function(code) {
            this.main_vue_obj.$emit('selectByCode', code);
        },
        repackTreeData: function(tree_data) {
            var _this = this;
            var repacked_tree_data = $.extend(true, [], tree_data);
            
            var items_with_pre_path = [];
            this.getItemsWithPrePath(repacked_tree_data, items_with_pre_path);
            items_with_pre_path.sort(function(a, b) {
                var a_pre_path_length = a['pre_path'].split('.').length;
                var b_pre_path_length = b['pre_path'].split('.').length;

                return a_pre_path_length > b_pre_path_length;
            });

            $.each(items_with_pre_path, function(key, item_with_pre_path) {
                _this.moveItem(repacked_tree_data, item_with_pre_path);
            });
            
            return repacked_tree_data;
        },
        getItemsWithPrePath: function(tree_data, items_with_pre_path) {
            var _this = this;
            
            $.each(tree_data, function(key, tree_data_item) {
                if (!sima.isEmpty(tree_data_item.pre_path))
                {
                    items_with_pre_path.push(tree_data_item);
                }
                if (typeof tree_data_item.subtree !== 'undefined')
                {
                    _this.getItemsWithPrePath(tree_data_item.subtree, items_with_pre_path);
                }
            });
        },
        moveItem: function(tree_data, item_with_pre_path) {
            this.removeItem(tree_data, item_with_pre_path.code);
            this.addItem(tree_data, item_with_pre_path);
        },
        removeItem: function(tree_data, item_code) {
            var _this = this;

            $.each(tree_data, function(key, tree_data_item) {
                if(!sima.isEmpty(tree_data_item))
                {
                    var curr_item_code = tree_data_item.code;
                    if (!sima.isEmpty(tree_data_item.pre_path))
                    {
                        curr_item_code = tree_data_item.pre_path + '.' + tree_data_item.code;
                    }

                    if (tree_data_item.code === item_code || curr_item_code === item_code)
                    {
                        tree_data.splice(key, 1);
                    }
                    else if (typeof tree_data_item.subtree !== 'undefined')
                    {
                        _this.removeItem(tree_data_item.subtree, item_code);
                    }
                }
            });
        },
        addItem: function(tree_data, item) {
            var _this = this;
            var item_pre_path = item.pre_path;

            $.each(tree_data, function(key, tree_data_item) {
                var curr_item_code = tree_data_item.code;
                if (!sima.isEmpty(tree_data_item.pre_path))
                {
                    curr_item_code = tree_data_item.pre_path + '.' + tree_data_item.code;
                }

                if (tree_data_item.code === item_pre_path || curr_item_code === item_pre_path)
                {
                    if (typeof tree_data_item.subtree === 'undefined')
                    {
                        tree_data_item.subtree = [];
                    }
                    tree_data_item.subtree.push(item);
                }
                else if (typeof tree_data_item.subtree !== 'undefined')
                {
                    _this.addItem(tree_data_item.subtree, item);
                }
            });
        },
        applyDefaultSelected: function() {
            var _this = this;

            if (
                    this.is_default_selected_applied === false && 
                    !sima.isEmpty(this.default_selected) && 
                    !sima.isEmpty(this.local_tree_data)
                )
            {
                this.$nextTick(function() {
                    _this.selectItemByCode(_this.default_selected);
                    _this.is_default_selected_applied = true;
                });
            }
        },
        setNewTreeData: function(tree_data) {
            this.local_tree_data = tree_data;
        }
    }
});