
/* global Vue, sima */

Vue.component('sima-tree-item', {
    template: '#sima-tree-item-template',
    props: {
        main_vue_obj: Object,
        item_data: Object,
        subtree_level: Number
    },
    data: function () {
        return {
            expanded: false,
            selected: false,
            loaded: false,
            item_load_action: this.main_vue_obj.item_load_action,
            item_load_params: this.main_vue_obj.item_load_params,
            disable_unselect_item: this.main_vue_obj.disable_unselect_item,
            show_subtree_cnt: this.main_vue_obj.show_subtree_cnt,
            hide_item_without_content: this.main_vue_obj.hide_item_without_content
        };
    },
    watch: {
        main_vue_obj: function() {
            this.item_load_action = this.main_vue_obj.item_load_action;
            this.item_load_params = this.main_vue_obj.item_load_params;
            this.disable_unselect_item = this.main_vue_obj.disable_unselect_item;
            this.show_subtree_cnt = this.main_vue_obj.show_subtree_cnt;
            this.hide_item_without_content = this.main_vue_obj.hide_item_without_content;
        }
    },
    computed: {
        has_subtree: function() {
            return !sima.isEmpty(this.item_data.subtree);
        },
        is_expanded: function() {
            return this.expanded;
        },
        is_selected: function() {
            return this.selected;
        },
        is_loaded: function() {
            return this.loaded || this.has_subtree;
        },
        is_selectable: function() {
            return typeof this.item_data.selectable === 'undefined' || this.item_data.selectable === true;
        },
        is_hidden: function() {
            return (
                this.item_data.visible === false ||
                (
                    this.has_subtree === false &&
                    (
                        (
                            this.hide_item_without_content === true && 
                            this.has_content === false
                        ) || 
                        this.is_selectable === false
                    )
                )
            );
        },
        has_content: function() {
            return typeof this.item_data.content_html !== 'undefined' || typeof this.item_data.content_action !== 'undefined';
        },
        expanded_class: function() {
            return this.is_expanded ? '_expanded' : '';
        },
        selected_class: function() {
            return this.is_selected ? '_selected' : '';
        },
        loaded_class: function() {
            return this.is_loaded ? '_loaded' : '';
        },
        subtree_level_class: function() {
            return '_level' + this.subtree_level;
        },
        visible_parts_before_title: function() {
            return this.visibleTitleParts('before');
        },
        visible_parts_after_title: function() {
            return this.visibleTitleParts('after');
        },
        item_wrap_padding_left: function() {
            return ((this.subtree_level - 1) * 20) + 'px'; //-1 jer na prvom nivou ne treba da uvlacimo
        },
        item_title_text: function() {
            return (typeof this.item_data.title === 'object') ? this.item_data.title.text : this.item_data.title;
        },
        item_title_tooltip: function() {
            return ((typeof this.item_data.title === 'object') && (typeof this.item_data.title.tooltip !== 'undefined')) ? this.item_data.title.tooltip : this.item_title_text;
        },
        subtree_cnt_tooltip: function() {
            return sima.translate('SIMATreeItemSubtreeCntTooltip');
        }
    },
    mounted: function() {
        var _this = this;
        
        this.main_vue_obj.$on('selectByCode', function (code) {
            if (_this.item_data.code === code)
            {
                //ekspanduje se sve do root parenta-a, tako sto parent dalje zove rekurzivno expandRecursively()
                _this.$parent.expandRecursively();
                _this.select();
            }
        });
    },
    destroyed: function() {
        if (this.is_selected === true)
        {
            this.unselect();
        }
    },
    methods: {
        toggleExpand: function (event) {
            if (this.is_expanded === false)
            {
                this.expand(event);
            }
            else
            {
                this.collapse(event);
            }
        },
        expand: function(event) {
            if (this.is_expanded === false)
            {
                var item_obj = $(this.$el);
                
                this.expanded = true;
                item_obj.find('.sima-tree-vue-item-subtree:first').slideDown();

                this.main_vue_obj.$emit("afterTreeItemExpand", this);
            }
        },
        expandRecursively: function() {
            this.$parent.expandRecursively();
            this.expand();
        },
        collapse: function(event) {
            if (this.is_expanded === true)
            {
                var item_obj = $(this.$el);

                this.expanded = false;
                item_obj.find('.sima-tree-vue-item-subtree:first').slideUp();

                this.main_vue_obj.$emit("afterTreeItemCollapse", this);

                //ako je selektovan item unutar itema koji se zatvara, onda se select prebacuje na item koji se zatvara
                //ili ako nije selektibilan onda na prvi parent koji je selektibilan
                if (this.is_selected === false && this.isSelectedItemInside() === true)
                {
                    if (this.is_selectable === true)
                    {
                        this.select(event, false);
                    }
                    else
                    {
                        this.selectFirstSelectableParent(event);
                    }
                }
            }
        },
        toggleSelect: function(event) {
            if (this.is_selected === true)
            {
                if (this.disable_unselect_item === false)
                {
                    this.unselect(event);
                }
            }
            else if (this.is_expanded === true && this.is_selectable === false)
            {
                this.collapse(event);
            }
            else
            {
                this.select(event);
            }
        },
        select: function(event, with_expand) {
            var _this = this;

            if (this.is_selected === false)
            {
                if (typeof with_expand === 'undefined')
                {
                    with_expand = true;
                }

                if (this.is_selectable === true)
                {
                    if (!sima.isEmpty(this.main_vue_obj.selected_item))
                    {
                        this.main_vue_obj.selected_item.unselect(null, false);
                    }
                    this.main_vue_obj.selected_item = this;
                    this.selected = true;

                    if (this.is_loaded === false)
                    {
                        this.load(event, function() {
                            _this.main_vue_obj.$emit("afterTreeItemSelect", _this);
                            _this.main_vue_obj.$emit("afterTreeItemSelectionChanged", _this, 'SELECTED');
                        });
                    }
                    else
                    {
                        if (with_expand === true)
                        {
                            this.expand(event);
                        }
                        this.main_vue_obj.$emit("afterTreeItemSelect", this);
                        this.main_vue_obj.$emit("afterTreeItemSelectionChanged", this, 'SELECTED');
                    }
                }
                else if (
                    this.has_subtree && 
                    typeof this.item_data.select_sub_tree_item_code !== 'undefined'
                )
                {
                    if (with_expand === true)
                    {
                        this.expand(event);
                    }
                    this.selectItemByCode(this.item_data.code + '.' + this.item_data.select_sub_tree_item_code);
                }
                else if (with_expand === true)
                {
                    this.expand(event);
                }
            }
        },
        unselect: function(event, call_selection_changed) {
            if (this.is_selected === true)
            {
                this.selected = false;
                this.main_vue_obj.selected_item = null;
                
                this.main_vue_obj.$emit("afterTreeItemUnselect", this);
                if (typeof call_selection_changed === 'undefined' || call_selection_changed === true)
                {
                    this.main_vue_obj.$emit("afterTreeItemSelectionChanged", this, 'UNSELECTED');
                }
            }
        },
        load: function(event, success_func) {
            if (this.is_loaded === false)
            {
                if (!sima.isEmpty(this.item_load_action))
                {
                    var _this = this;
                    var item_obj = $(this.$el);
                    sima.ajax.get(this.item_load_action, {
                        get_params: {
                            parent_code: this.item_data.code
                        },
                        data: {
                            params: this.item_load_params
                        },
                        async: true,
                        loadingCircle: item_obj,
                        success_function:function(response)
                        {
                            Vue.set(_this.item_data, 'subtree', response.subtree);
                            //mora $nextTick jer Vue.set treba malo vremena da postavi vrednost
                            _this.$nextTick(function() {
                                _this.afterLoad(event, success_func);
                            });
                        }
                    });
                }
                else
                {
                    this.afterLoad(event, success_func);
                }
            }
        },
        afterLoad: function(event, success_func) {
            
            this.loaded = true;
            this.expand(event);
            
            if (typeof success_func !== 'undefined')
            {
                sima.executeFunction(success_func);
            }

            this.main_vue_obj.$emit("afterTreeItemLoad", this);
        },
        isSelectedItemInside: function() {
            if (this.is_selected === true)
            {
                return true;
            }
            else if (this.$children.length > 0)
            {
                var is_inside = false;
                $.each(this.$children, function(index, value) {
                    if (typeof this.isSelectedItemInside === 'function' && this.isSelectedItemInside() === true)
                    {
                        is_inside = true;
                        return true;
                    }
                });
                
                return is_inside;
            }
            
            return false;
        },
        selectFirstSelectableParent: function(event) {
            if (this.$parent.is_selectable === true)
            {
                this.$parent.select(event, false);
            }
            else
            {
                this.$parent.selectFirstSelectableParent(event);
            }
        },
        selectItemByCode: function(code) {
            this.$parent.selectItemByCode(code);
        },
        visibleTitleParts: function(position) {
            var parts = [];
            
            if (typeof this.item_data.title.parts !== 'undefined')
            {
                $.each(this.item_data.title.parts, function(index, part) {
                    if (part.position === position && (typeof part.visible === 'undefined' || part.visible === true))
                    {
                        parts.push(part);
                    }
                });
            }

            return parts;
        },
        getPartTooltip: function(part) {
            return typeof part.tooltip !== 'undefined' ? part.tooltip : part.text;
        }
    }
});