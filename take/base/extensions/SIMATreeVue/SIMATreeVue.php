<?php

class SIMATreeVue
{
    public function registerManual()
    {
        $dirname = dirname(__FILE__);
        $assets = $dirname . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        $uniq = SIMAHtml::uniqid();

        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/simaTreeVue.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/simaSubtreeVue.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/simaTreeItemVue.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaTreeVue.css');
        Yii::app()->clientScript->registerScript("sima_tree_$uniq", include($dirname.'/templates/sima_tree.php'), CClientScript::POS_END);
        Yii::app()->clientScript->registerScript("sima_subtree_$uniq", include($dirname.'/templates/sima_subtree.php'), CClientScript::POS_END);
        Yii::app()->clientScript->registerScript("sima_tree_item_$uniq", include($dirname.'/templates/sima_tree_item.php'), CClientScript::POS_END);
    }
}
