<script type="text/x-template" id="sima-tree-template">
    <div 
        class='sima-tree-vue sima-layout-panel _horizontal'
        v-bind:class="additional_class"
    >
        <div class='sima-tree-vue-options sima-layout-fixed-panel'>
        </div>
        <sima-subtree 
            class="sima-tree-vue-list sima-layout-panel"
            :main_vue_obj="main_vue_obj"
            :tree_data="repacked_tree_data"
            :subtree_level="subtree_level"
            v-bind:class="subtree_level_class"
        >
        </sima-subtree>
    </div>
</script>