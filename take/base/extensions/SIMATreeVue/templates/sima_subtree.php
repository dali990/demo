<script type="text/x-template" id="sima-subtree-template">
    <ul>
        <sima-tree-item
            :main_vue_obj="main_vue_obj"
            :item_data="item"
            :subtree_level="subtree_level"
            :key="item.code"
            v-for="item in ordered_tree_data"
        >
        </sima-tree-item>
    </ul>
</script>