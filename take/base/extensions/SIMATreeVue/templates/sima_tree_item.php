<script type="text/x-template" id="sima-tree-item-template">
    <li 
        class="sima-tree-vue-item"
        v-bind:class="[item_data.class, expanded_class, selected_class, loaded_class]"
        v-bind:data-code="item_data.code"
        v-if="!is_hidden"
    >
        <div 
            class="sima-tree-vue-item-wrap"
            v-bind:style="{ 'padding-left': item_wrap_padding_left }"
        >
            <span 
                class="sima-tree-vue-item-expand-icon sima-icon _16"
                v-bind:class="{'_no-expand': !has_subtree, '_filled-arrow-right': (has_subtree && !is_expanded), '_filled-arrow-down': (has_subtree && is_expanded)}"
                @click="toggleExpand($event)"
            >
            </span>

            <span 
                class="sima-tree-vue-item-title-part"
                v-bind:class="part.class"
                v-bind:title="getPartTooltip(part)"
                v-for="part in visible_parts_before_title"
                v-on:click="typeof part.onclick !== 'undefined' ? part.onclick() : ''"
            >
                {{ part.text }}
            </span>

            <div 
                class="sima-tree-vue-item-title sima-text-dots"
                v-bind:title="item_title_tooltip"
                @click="toggleSelect($event)"
            >
                {{ item_title_text }}
            </div>

            <span 
                class="sima-tree-vue-item-title-part"
                v-bind:class="part.class"
                v-bind:title="getPartTooltip(part)"
                v-for="part in visible_parts_after_title"
                v-on:click="typeof part.onclick !== 'undefined' ? part.onclick() : ''"
            >
                {{ part.text }}
            </span>

            <span 
                class="sima-tree-vue-item-subtree-cnt"
                v-bind:title="subtree_cnt_tooltip"
                v-if="show_subtree_cnt && has_subtree" 
            >
                {{ item_data.subtree.length }}       
            </span>
        </div>

        <sima-subtree 
            class="sima-tree-vue-item-subtree"
            :main_vue_obj="main_vue_obj"
            :tree_data="item_data.subtree"
            :subtree_level="subtree_level + 1"
            v-bind:class="subtree_level_class"
            v-show="false" 
            v-if="has_subtree"
        >
        </sima-subtree>
    </li>
</script>

