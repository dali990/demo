<?php

class SIMAPhoneField extends CWidget 
{
    public $id                 = null;   
    public $model              = null;    
    public $country            = null;
    public $provider_number    = null;
    public $phone_number       = null;
    public $provider_delimiter = null;
    public $phone_delimiter    = null;
    
    public function run() 
    {
        
        if(empty($this->id))
        {
            $this->id = SIMAHtml::uniqid();
        }
        
        if (!empty($this->model))
        {
            $this->country         = $this->model->country;
            $this->provider_number = $this->model->provider_number;
            $this->phone_number    = $this->model->phone_number;
        }
        
        if (empty($this->country))
        {
            $system_country_id = Yii::app()->configManager->get('base.system_country_id', false);
            if (!empty($system_country_id))
            {
                $this->country = Country::model()->findByPkWithCheck($system_country_id);
            }
            else
            {
                $this->country = Country::model();
            }
        }
        
        if (empty($this->provider_delimiter))
        {
            $this->provider_delimiter = PhoneNumber::PROVIDER_DELIMITER;
        }
        
        if (empty($this->phone_delimiter))
        {
            $this->phone_delimiter = PhoneNumber::PHONE_DELIMITER;
        }
        
        
        echo $this->render('phone_field', []);
                        
        $params = [
            "country_display_name" => $this->country->DisplayName,
            "country_id"           => $this->country->id,
            "phone_delimiter"      => $this->phone_delimiter,
            "provider_delimiter"   => $this->provider_delimiter
        ];
        $json = CJSON::encode($params);
        $register_script = "$('#".$this->id."').simaPhoneField('init',$json);";
        Yii::app()->clientScript->registerScript("register_script_{$this->id}", $register_script, CClientScript::POS_READY);
    }
    
    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';

        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.simaPhoneField.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/phoneField.css');
    }
}