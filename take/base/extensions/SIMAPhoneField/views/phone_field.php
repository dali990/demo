<div id="<?php echo $this->id; ?>" class="sima-phone-field">
    <?php
        Yii::app()->controller->widget('SIMASearchField', [
            'model' => $this->country,
            'name'  => 'PhoneNumber[country_id]',
            'htmlOptions' => [
                'class' => 'country-number-search-field'
            ]
        ]);
    ?>
    <input type="text" class="provider-number" name="PhoneNumber[provider_number]"  value="<?=$this->provider_number?>">
    <input type="text" class="phone-number" name="" value="<?=$this->phone_number?>">
    <input type="hidden" class="phone-number-only" name="PhoneNumber[phone_number]" value="<?=$this->phone_number?>">
</div>

