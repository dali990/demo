/* global sima */

(function($){
    var methods = {
        init: function(options)
        {
            var _tree = $(this);
            _tree.data('list_tree_action',options.list_tree_action);
            _tree.data('list_tree_params',!sima.isEmpty(options.list_tree_params)?options.list_tree_params:[]);
            _tree.data('show_subtree_cnt',options.show_subtree_cnt);
            _tree.data('disable_unselect_item', options.disable_unselect_item);
            _tree.data('hide_item_without_content', options.hide_item_without_content);
            
            initTree(_tree);

            //ako su prosledjeni podaci onda popunimo drvo njima
            if (!sima.isEmpty(options.list_tree_data))
            {
                parseJSONIntoTree(_tree, _tree.find('.sima-tree-list').children('ul'), options.list_tree_data);
                //inicijalno selektujemo item
                if (!sima.isEmpty(options.default_selected))
                {
                    _tree.simaTree2('selectItem', options.default_selected);
                }
            }
            //inace ako je zadatak akcija popunimo drvo podacima iz akcije
            else if (!sima.isEmpty(_tree.data('list_tree_action')) && options.load_on_init === true)
            {
                loadTree(_tree, null, function(){
                    if (!sima.isEmpty(options.default_selected))
                    {
                        _tree.simaTree2('selectItem', options.default_selected);
                    }
                });
            }
        },
        reloadTree: function(new_tree_data)
        {
            var _tree = $(this);
            if (typeof new_tree_data !== 'undefined')
            {
                var removed_selected_code = getSelectedTreeItem(_tree).data('code');
                parseJSONIntoTree(_tree, _tree.find('.sima-tree-list').children('ul'), new_tree_data);
                _tree.simaTree2('selectItem', removed_selected_code);//ako nestane, samo ce biti selektovane osnovne informacije
            }
            else if (!sima.isEmpty(_tree.data('list_tree_action')))
            {
                loadTree(_tree);
            }
        },
        getListTreeParams: function()
        {
            var _tree = $(this);
            return _tree.data('list_tree_params');
        },
        setListTreeParams: function(new_list_tree_params)
        {
            var _tree = $(this);
            _tree.data('list_tree_params', new_list_tree_params);
        },
        getSelectedItem: function()
        {
            var _tree = $(this);
            return getSelectedTreeItem(_tree);
        },
        getSelectedItemData: function()
        {
            var _tree = $(this);
            var selected_tree_item = getSelectedTreeItem(_tree);
            
            return selected_tree_item.data();
        },
        expandItem: function(code)
        {
            var _tree = $(this);
            var tree_item = getTreeItemByCode(_tree, code);
            if(!sima.isEmpty(tree_item) && !tree_item.hasClass('_opened'))
            {
                expandAllTreeItemParents(_tree, tree_item);
                expandTreeItem(_tree, tree_item);
            }
        },
        collapseItem: function(code)
        {
            var _tree = $(this);
            var tree_item = getTreeItemByCode(_tree, code);
            if(!sima.isEmpty(tree_item) && tree_item.hasClass('_opened'))
            {
                collapseTreeItem(_tree, tree_item);
            }
        },
        selectItem: function(code)
        {
            var _tree = $(this);
            var tree_item = getTreeItemByCode(_tree, code);
            if(!sima.isEmpty(tree_item) && !tree_item.hasClass('_selected'))
            {
                expandAllTreeItemParents(_tree, tree_item);
                selectTreeItem(_tree, tree_item);
            }
        },
        unselectItem: function(code)
        {
            var _tree = $(this);
            var tree_item = getTreeItemByCode(_tree, code);
            if(!sima.isEmpty(tree_item) && tree_item.hasClass('_selected') && _tree.data('disable_unselect_item') === false)
            {
                unselectTreeItem(_tree, tree_item);
            }
        },
        getTreeItemByCode: function(code)
        {
            var _tree = $(this);

            return getTreeItemByCode(_tree, code);
        }
    };
    
    function initTree(_tree)
    {
        _tree.on('click','.sima-tree-item-wrap', function(){
            var tree_item = $(this).parent();
            if(tree_item.hasClass('_selected') && _tree.data('disable_unselect_item') === false)
            {
                unselectTreeItem(_tree, tree_item);
            }
            else if (tree_item.hasClass('_opened') && isSelectable(tree_item) === false)
            {
                collapseTreeItem(_tree, tree_item);
            }
            else 
            {
                selectTreeItem(_tree, tree_item);
            }
        });
        
        _tree.on('click', '.sima-tree-item-expand-icon', function(event){
            if (!$(this).hasClass('_no-expand'))
            {
                var tree_item = $(this).parents('.sima-tree-item:first');
                if(tree_item.hasClass('_opened'))
                {
                    collapseTreeItem(_tree, tree_item);
                }
                else
                {
                    expandTreeItem(_tree, tree_item);
                }
                event.stopPropagation();
            }
        });
    }
    
    function selectTreeItem(_tree, tree_item)
    {
        if (!tree_item.hasClass('_selected'))
        {
            var is_selected = false;
            //ako nije zadat selectable ili ako je selectable true onda selektujemo red
            if (isSelectable(tree_item) === true)
            {
                var selected_tree_item = getSelectedTreeItem(_tree);
                if (!sima.isEmpty(selected_tree_item))
                {
                    unselectTreeItem(_tree, selected_tree_item, false);
                }
                tree_item.addClass('_selected');
                if(!tree_item.hasClass('_loaded'))
                {
                    loadTree(_tree, tree_item, function(){
                        _tree.trigger('selectItem', [tree_item]);
                        _tree.trigger('selectionChanged', ['SELECTED',tree_item]);
                    });
                }
                else
                {
                    expandTreeItem(_tree, tree_item);
                    _tree.trigger('selectItem', [tree_item]);
                    _tree.trigger('selectionChanged', ['SELECTED',tree_item]);
                }
                is_selected = true;
            }
            //ako je zadat pod item da se selektuje
            else if (typeof tree_item.data('select_sub_tree_item_code') !== 'undefined')
            {
                var sub_tree_item = getTreeItemByCode(_tree, tree_item.data('code') + '.' + tree_item.data('select_sub_tree_item_code'));
                if (!sima.isEmpty(sub_tree_item))
                {
                    selectTreeItem(_tree, sub_tree_item);
                }
            }

            //inace ga samo ekspandujemo
            if (is_selected === false)
            {
                expandTreeItem(_tree, tree_item);
            }
        }
    }
    
    function unselectTreeItem(_tree, tree_item, call_selection_changed)
    {
        if (tree_item.hasClass('_selected'))
        {
            tree_item.removeClass('_selected');
            _tree.trigger("unselectItem", [tree_item]);
            if (typeof call_selection_changed === 'undefined' || call_selection_changed === true)
            {
                _tree.trigger('selectionChanged', ['UNSELECTED',tree_item]);
            }
        }
    }
    
    function expandTreeItem(_tree, tree_item)
    {
        if (!tree_item.hasClass('_opened'))
        {
            tree_item.addClass('_opened');
            tree_item.find('ul:first').slideDown();
            _tree.trigger("openItem", [tree_item]);
            if (tree_item.find('ul:first').find('li:first').length > 0)
            {
                tree_item.find('.sima-tree-item-expand-icon:first').removeClass('_filled-arrow-right _no-expand').addClass('_filled-arrow-down');
            }
            else
            {
                tree_item.find('.sima-tree-item-expand-icon:first').removeClass('_filled-arrow-right').removeClass('_filled-arrow-down').addClass('_no-expand');
            }
        }
    }

    function expandAllTreeItemParents(_tree, tree_item)
    {
        var parents = tree_item.parents('.sima-tree-item').toArray().reverse();
        $.each(parents, function(index, value) {
            expandTreeItem(_tree, $(value));
        });
    }

    function collapseTreeItem(_tree, tree_item)
    {
        if (tree_item.hasClass('_opened'))
        {
            tree_item.removeClass('_opened');
            tree_item.find('.sima-tree-item-expand-icon:first').removeClass('_filled-arrow-down _no-expand').addClass('_filled-arrow-right');
            tree_item.find('ul:first').slideUp();
            _tree.trigger("closeItem", [tree_item]);
            //ako je selektovan item unutar itema koji se zatvara, onda se select prebacuje na item koji se zatvara
            //ili ako nije selektibilan onda na prvi parent koji je selektibilan. Ako ne postoji takav parent onda se selektuje full info
            var selected_tree_item = tree_item.find('.sima-tree-item._selected');
            if (!sima.isEmpty(selected_tree_item))
            {
                unselectTreeItem(_tree, selected_tree_item);
                var first_selectable_parent = getTreeItemFirstSelectableParent(tree_item);
                if (first_selectable_parent !== null)
                {
                    first_selectable_parent.addClass('_selected');
                    _tree.trigger('selectItem', [first_selectable_parent]);
                }
            }
        }
    }
    
    function getTreeItemFirstSelectableParent(tree_item)
    {
        if (isSelectable(tree_item) === true)
        {
            return tree_item;
        }
        else if (!sima.isEmpty(tree_item.parents('.sima-tree-item:first')))
        {
            return getTreeItemFirstSelectableParent(tree_item.parents('.sima-tree-item:first'));
        }
        
        return null;
    }
    
    function getSelectedTreeItem(_tree)
    {
        return _tree.find('.sima-tree-item._selected');
    }
    
    function getTreeItemByCode(_tree, code)
    {
        return _tree.find(".sima-tree-item[data-code='" + code + "']");
    }

    function loadTree(_tree, tree_item, success_func)
    {
        if (!sima.isEmpty(_tree.data('list_tree_action')))
        {
            var subtreeUL = _tree.find('.sima-tree-list').children('ul');
            var loading_circle = _tree;
            var item_data = {};
            var parent_code = null;

            if (!sima.isEmpty(tree_item))
            {
                loading_circle = tree_item;
                subtreeUL = tree_item.find('ul:first');
                item_data = tree_item.data();
                parent_code = tree_item.data('code');
            }

            sima.ajax.get(_tree.data('list_tree_action'),{
                get_params:{
                    parent_code: parent_code
                },
                data: {
                    params: _tree.data('list_tree_params'),
                    item_data: item_data
                },
                async:true,
                loadingCircle:loading_circle,
                success_function:function(response)
                {
                    parseJSONIntoTree(_tree, subtreeUL, response.subtree);
                    loadSuccessFunc(_tree, tree_item, response.data, success_func);
                }
            });
        }
        else
        {
            loadSuccessFunc(_tree, tree_item, null, success_func);
        }
    }
    
    function loadSuccessFunc(_tree, tree_item, data, success_func)
    {
        if (sima.isEmpty(tree_item))
        {
            _tree.trigger("loadTree", (!sima.isEmpty(data)) ? [data] : []);
        }
        else
        {
            tree_item.addClass('_loaded');
            expandTreeItem(_tree, tree_item);
            _tree.trigger("loadItem", (!sima.isEmpty(data)) ? [tree_item, data] : [tree_item]);
        }

        if (typeof success_func === 'function')
        {
            success_func();
        }
    }
    
    function parseJSONIntoTree(_tree, subtreeUL, subtreeJson)
    {
        var subtreeUL_lis = subtreeUL.children('li');
        /// remove lis from subtreeUL that are not in subtreeJson
        $.each(subtreeUL_lis, function(index, li) {
            var _this = $(li);

            var exists = false;
            $.each(subtreeJson, function(index, data) {
                var hide_item = hideTreeItem(_tree, data);
                if (!hide_item && (typeof data.visible === 'undefined' || data.visible === true))
                {
                    if (data.code === _this.data('code'))
                    {
                        exists = true;
                        
                        var value_data = data.data;

                        for(var k in value_data)
                        {
                            _this.data(k, value_data[k]);
                        }

                        if(_this.hasClass('_loaded') && !sima.isEmpty(data.subtree))
                        {
                            parseJSONIntoTree(_tree, _this.find('ul:first'), data.subtree);
                        }

                        return false;
                    }
                }
            });
                        
            if(exists === false)
            {
                var removing_selected_item = false;
                var selected_item = null;
                
                if(_this.hasClass('_selected'))
                {
                    removing_selected_item = true;
                    selected_item = _this;
                }
                else if(!sima.isEmpty(_this.find('._selected')))
                {
                    removing_selected_item = true;
                    selected_item = _this.find('._selected');
                }
                
                _this.remove();
                
                if(removing_selected_item === true)
                {
                    _tree.trigger("unselectItem", [selected_item]);
                }
            }
        });

        /// add lis that are in subtreeJson and not in subtreeUL
        var previous_elem = null;
        $.each(subtreeJson, function(index, data) {
            var hide_item = hideTreeItem(_tree, data);
            if (!hide_item && (typeof data.visible === 'undefined' || data.visible === true))
            {
                var exists = false;
                $.each(subtreeUL_lis, function(index, li) {
                    var _this = $(li);
                    if(_this.data('code') === data.code)
                    {
                        exists = true;

                        previous_elem = _this;

                        return false;
                    }
                });

                if(exists === false)
                {
                    var expand_icon = '_no-expand';
                    var subtree_cnt_html = '';
                    var subtree_cnt = 0;

                    if (!sima.isEmpty(data.subtree))
                    {
                        var subtree = data.subtree;
                        if (subtree.length > 0)
                        {
                            expand_icon = '_filled-arrow-right';
                            subtree_cnt = subtree.length;
                        }
                    }

                    if (_tree.data('show_subtree_cnt') === true && subtree_cnt > 0)
                    {
                        subtree_cnt_html = '<span class="sima-tree-item-subtree-cnt" title="' + subtree_cnt + '">' + subtree_cnt + '</span>';
                    }
                    
                    var data_selectable = '';
                    if (typeof data.selectable !== 'undefined')
                    {
                        data_selectable = 'data-selectable="' + data.selectable + '"';
                    }
                    
                    var data_order = '';
                    if (typeof data.order !== 'undefined')
                    {
                        data_order = 'data-order="' + data.order + '"';
                    }
                    
                    var li_item_class = (typeof data.class !== 'undefined') ? data.class : '';

                    var li = $('<li class="sima-tree-item '+li_item_class+'" data-code="' + data.code + '" ' + data_selectable + ' ' + data_order + '>'
                        + '<div class="sima-tree-item-wrap">'
                            + '<span class="sima-tree-item-expand-icon sima-icon ' + expand_icon + ' _16"></span>'
                            + subtree_cnt_html
                            + renderItemTitle(data.title)
                            + '<div class="clearfix"></div>'
                        + '</div>'
                        + '<ul class="sima-tree-item-subtree"></ul>'
                    +'</li>');

                    var value_data = data.data;

                    for(var k in value_data)
                    {
                        li.data(k, value_data[k]);
                    }
                    
                    if (typeof data.select_sub_tree_item_code !== 'undefined')
                    {
                        li.data('select_sub_tree_item_code', data.select_sub_tree_item_code);
                    }
                    
                    if (typeof data.order !== 'undefined')
                    {
                        var curr_order = parseInt(data.order);
                        var prev_li_with_order = null;
                        $(subtreeUL.children('li').get().reverse()).each(function(){
                            if (sima.isEmpty(prev_li_with_order) && typeof $(this).data('order') !== 'undefined')
                            {
                                if (parseInt($(this).data('order')) < curr_order)
                                {
                                    prev_li_with_order = $(this);
                                }
                            }
                        });
                        if (!sima.isEmpty(prev_li_with_order))
                        {
                            li.insertAfter(prev_li_with_order);
                        }
                        else
                        {
                            subtreeUL.prepend(li);
                        }
                    }
                    else
                    {
                        subtreeUL.append(li);
                    }

                    var children_list = li.find('.sima-tree-item-subtree:first');
                    if (typeof children_list !== 'undefined')
                    {
                        var new_level_num = 1;
                        if (typeof li.parent().data('level') !== 'undefined')
                        {
                            new_level_num = parseInt(li.parent().data('level')) + 1;
                        }
                        children_list.data('level', new_level_num);
                        children_list.addClass('_level'+new_level_num);
                    }

                    previous_elem = li;
                }

                if (!sima.isEmpty(data.subtree))
                {
                    parseJSONIntoTree(_tree, previous_elem.find('ul:first'), data.subtree);
                }
            }
        });
    }
    
    function hideTreeItem(_tree, json_obj)
    {
        var hide_item = false;
        if (sima.isEmpty(json_obj.subtree))
        {
            if (_tree.data('hide_item_without_content') === true && hasContent(json_obj) === false)
            {
                hide_item = true;
            }
            else if (isSelectable(json_obj) === false)
            {
                hide_item = true;
            }
        }
        
        return hide_item;
    }
    
    function renderItemTitle(title)
    {
        if (typeof title === 'object')
        {
            var _title = '<div class="sima-tree-item-title-text sima-text-dots '+title.class+'">' + title.text + '</div>' + '<div class="clearfix"></div>';
            if (typeof title.parts !== 'undefined')
            {
                var before_parts = '<div class="sima-tree-item-title-parts _before">';
                var after_parts = '<div class="sima-tree-item-title-parts _after">';
                $.each(title.parts, function(index, value) {
                    var position = 'before';
                    if (typeof value.position !== 'undefined')
                    {
                        position = value.position;
                    }
                    var part_text = '';
                    var part_class = '';
                    if (typeof value.text !== 'undefined')
                    {
                        part_text = value.text;
                    }
                    if (typeof value.class !== 'undefined')
                    {
                        part_class = value.class;
                    }
                    var part_html = '<span class="sima-tree-item-title-part ' + part_class + '" title="' + part_text + '">' + part_text + '</span>';
                    if (position === 'before')
                    {
                        before_parts += part_html;
                    }
                    else if (position === 'after')
                    {
                        after_parts += part_html;
                    }
                });
                before_parts += '</div>';
                after_parts += '</div>';
                _title = before_parts + after_parts + _title;
            }
            
            return '<div class="sima-tree-item-title" title="' + title.text + '">' + _title + '</div>';
        }
        else
        {
            var title_attr = '';
            if (!sima.isHTML(title))
            {
                title_attr = title;
            }
            
            return '<div class="sima-tree-item-title sima-text-dots" title="' + title_attr + '">' + title + '</div>';
        }
    }
    
    function isSelectable(tree_item)
    {
        var item_code = null;
        var item_data = {};
        var is_selectable = false;
        if (tree_item instanceof jQuery)
        {
            if (typeof tree_item.data('selectable') === 'undefined' || tree_item.data('selectable') === true)
            {
                is_selectable = true;
            }
            item_code = tree_item.data('code');
            item_data = tree_item.data();
        }
        else if (!sima.isEmpty(tree_item))
        {
            if (typeof tree_item.selectable === 'undefined' || tree_item.selectable === true)
            {
                is_selectable = true;
            }
            item_code = tree_item.code;
            item_data = tree_item;
        }
        
        //Sasa A. - privremeni kod koji salje dodatne podatke o gresci, jer kodovi belongs i others su uvek is_selectable=false, ali se desi ponekad da budu
        //true, a ne bi trebalo jer su zadati kao false...
        if ((item_code === 'others' || item_code === 'belongs') && is_selectable)
        {
            sima.errorReport.createAutoClientBafRequest(
                'Greska prilikom otvaranja taba u fajlu sa kodom others ili belongs. Selectable za te tabove je false ali iz nekog razloga se preracuna da je true.<br />' + 
                JSON.stringify({
                    item: tree_item,
                    item_code: item_code,
                    item_data: item_data,
                    js_stack_trace: sima.getStackTrace()
                })
            );
        }

        return is_selectable;
    }
    
    function hasContent(tree_item)
    {
        var has_content = false;
        
        if (isSelectable(tree_item) === true)
        {
            if (tree_item instanceof jQuery)
            {
                if (typeof tree_item.data('content_html') !== 'undefined' || typeof tree_item.data('content_action') !== 'undefined')
                {
                    has_content = true;
                }
            }
            else if (!sima.isEmpty(tree_item))
            {
                var data = tree_item.data;
                if (
                        typeof data !== 'undefined' && 
                        (typeof data.content_html !== 'undefined' || typeof data.content_action !== 'undefined')
                   )
                {
                    has_content = true;
                }
            }
        }
        
        return has_content;
    }
    
    $.fn.simaTree2 = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.simaTree2');
            }
        });
        return ret;
    };
})( jQuery );
