<?php

/*
 * Primer zadavanja list_tree_data:
 * 
 * [
        'title'=>[
            'text'=>'fsdfsd',
            'parts'=>[
                [
                    'text'=>'fhfg',
                    'position'=>'after'
                ],
                [
                    'class'=>'neka klasa',
                    'position'=>'after'
                ]
            ]
        ]
        'code'=>'1',
        'visible'=>true/false,
        'selectable'=>true/false,
        'subtree'=>[ //rekurzivno se zadaje isto
            [
                'title'=>'ghhfghfgh',
                'code'=>'1.1'
            ]
        ],
        'data'=>[
            'content_html'=>'neki html',
            'content_action'=>[
                'action'=>'neka akcija',
                'get_params'=>['a'=>1,'b'=>2],
                'post_params'=>['c'=>3,'d'=>4],
            ]
        ]
        'select_sub_tree_item_code'=>'code'
    ],
    [
        'title'=>'kljkljklh',
        'code'=>'2',
        'data'=>[
            'content_html'=>'sfogndfoghdofgfd'
        ]
    ],
 */

class SIMATree2 extends CWidget 
{
    public $id = null;
    public $class = '';
    public $list_tree_data = null;
    public $list_tree_action = '';
    public $list_tree_params = [];
    public $show_subtree_cnt = false;
    public $load_on_init = true;
    public $default_selected = null;
    public $disable_unselect_item = false;
    public $hide_item_without_content = false; //(default false - jer je moguce da se content stavke ucitava nezavisno od sima tree, znaci prvo se izrenderuje tree
                                          //pa se onda zadaje klik na tree item koji dovlaci content za taj item. Tako da prilikom renderovanja tree ne treba da se sakriva
                                          //tree item jer se ne zna da li ce content-a biti kasnije. Jedino ako se ekspicitno kaze hide_without_content=>true onda se sakriva.
                                          //Ako je ovaj parametar setovan na true i ako ima dece onda se ne sakriva.)

    public function run() 
    {
        if (empty($this->id))
        {
            $this->id = SIMAHtml::uniqid();
        }

        if (is_null($this->list_tree_data) && empty($this->list_tree_action))
        {
            throw new SIMAException('SIMATree2 - bar jedan od sledecih parametra mora biti zadat: list_tree_data, list_tree_action');
        }
        
        echo $this->render('index',[]);

        if(!is_null($this->list_tree_data))
        {
            SIMATree2::repackTree($this->list_tree_data);
        }
        
        $params = [
            'list_tree_data'=>$this->list_tree_data,
            'list_tree_action'=>$this->list_tree_action,
            'list_tree_params'=>$this->list_tree_params,
            'show_subtree_cnt'=>$this->show_subtree_cnt,
            'load_on_init'=>$this->load_on_init,
            'disable_unselect_item'=>$this->disable_unselect_item,
            'hide_item_without_content'=>$this->hide_item_without_content
        ];
        if (!empty($this->default_selected))
        {
            $params['default_selected'] = $this->default_selected;
        }
        $json_params = CJSON::encode($params);
        $register_script = "$('#$this->id').simaTree2('init',$json_params);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
    
    public static function repackTree(&$tree_data)
    {
        $tree_items_with_pre_path = [];
        SIMATree2::getTreeItemsWithPrePath($tree_data, $tree_items_with_pre_path);
        usort($tree_items_with_pre_path, function($a, $b)
        {
            $a_pre_path_length = count(explode('.', $a['pre_path']));
            $b_pre_path_length = count(explode('.', $b['pre_path']));
            return $a_pre_path_length > $b_pre_path_length;
        });

        foreach ($tree_items_with_pre_path as $tree_item_with_pre_path)
        {
            SIMATree2::moveTreeItem($tree_data, $tree_item_with_pre_path);
        }
    }
    
    private static function moveTreeItem(&$tree_data, &$tree_item_with_pre_path)
    {
        SIMATree2::removeTreeItem($tree_data, $tree_item_with_pre_path['code']);
        SIMATree2::addTreeItem($tree_data, $tree_item_with_pre_path, $tree_item_with_pre_path['pre_path']);
    }

    private static function removeTreeItem(&$tree_data, $tree_item_code)
    {
        foreach ($tree_data as $key => $tree_data_item)
        {
            $curr_tree_item_code = $tree_data_item['code'];
            if (!empty($tree_data_item['pre_path']))
            {
                $curr_tree_item_code = $tree_data_item['pre_path'].'.'.$tree_data_item['code'];
            }
            if ($tree_item_code === $tree_data_item['code'] || $tree_item_code === $curr_tree_item_code)
            {
                unset($tree_data[$key]);
            }
            else if (isset($tree_data_item['subtree']))
            {
                SIMATree2::removeTreeItem($tree_data_item['subtree'], $tree_item_code);
            }
        }
    }
    
    private static function addTreeItem(&$tree_data, $tree_item, $tree_item_pre_path)
    {
        foreach ($tree_data as $key => $tree_data_item)
        {
            $curr_tree_item_pre_path = $tree_data_item['code'];
            if (!empty($tree_data_item['pre_path']))
            {
                $curr_tree_item_pre_path = $tree_data_item['pre_path'].'.'.$tree_data_item['code'];
            }
            if ($tree_item_pre_path === $tree_data_item['code'] || $tree_item_pre_path === $curr_tree_item_pre_path)
            {
                if (!isset($tree_data_item['subtree']))
                {
                    $tree_data[$key]['subtree']=[];
                }
                array_push($tree_data[$key]['subtree'], $tree_item);
            }
            else if (isset($tree_data_item['subtree']))
            {
                SIMATree2::addTreeItem($tree_data_item['subtree'], $tree_item, $tree_item_pre_path);
            }
        }
    }
    
    private static function getTreeItemsWithPrePath(&$tree_data, &$tree_items_with_pre_path = [])
    {
        foreach ($tree_data as $tree_data_item)
        {
            if (!empty($tree_data_item['pre_path']))
            {
                array_push($tree_items_with_pre_path, $tree_data_item);
            }
            if (isset($tree_data_item['subtree']))
            {
                SIMATree2::getTreeItemsWithPrePath($tree_data_item['subtree'], $tree_items_with_pre_path);
            }
        }
    }
    
    public function registerManual()
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.simaTree2.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaTree2.css');
    }
}
