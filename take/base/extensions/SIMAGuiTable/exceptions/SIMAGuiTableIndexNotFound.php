<?php

class SIMAGuiTableIndexNotFound extends SIMAException
{
    public function __construct($module_name, $index)
    {
        parent::__construct('SIMAGuiTableDefinition index not found - $module_name: '.$module_name.' - $index: '.$index);
    }
}

