<?php 
    $_trace_cat = 'SIMA.GuiTableController.ATModel.row';
    Yii::beginProfile('whole',$_trace_cat);
    Yii::beginProfile('part1',$_trace_cat);
    $row_has_display = $row->hasDisplay();
    $display_model_class = '';
    if ($row_has_display)
    {
        $display_model = $row->getDisplayModel();
        $display_model_class = !empty($display_model) ? get_class($display_model) : '';
    }
?>

<tr class='sima-guitable-row <?php echo $row->getClasses()?> 
		<?php if (isset($expand_child)) echo $expand_child;?>'
                model="<?php echo get_class($row)?>"
                model_id="<?php echo $row->id?>"
                order_scan="<?php echo (isset($row->scan_order)&&($row->scan_order == true))?'true':'false'; ?>"
                has_display="<?=$row_has_display ? 'true' : 'false'?>"
                display_model="<?=$display_model_class?>"
                path2="<?php echo isset($path2)?$path2:''; ?>"
	<?php echo $row->getAdditionalInfo()?>>
        <?php if (!isset($without_order_number) || $without_order_number === false) { ?>
            <td class="order-number"></td>
        <?php } 
        Yii::endProfile('part1',$_trace_cat);
        Yii::beginProfile('part2',$_trace_cat);
        ?>
	<td class='options'>

                    <?php 
                        Yii::beginProfile('part2.statuses',$_trace_cat);
                        echo $row->getAttributeDisplay('statuses');
//                        Logger::ttEndStat('Rows21',$_stats);
                        Yii::endProfile('part2.statuses',$_trace_cat);
                        Yii::beginProfile('part2.modelIcons',$_trace_cat);
//                        echo SIMAHtml::modelIcons($row,16); 
//                        echo SIMAHtml::modelIconsContracted($row, 16, [], '_hover_display_subactions _transparent_background _no_hover_border _no_border _hundred_percent_width_height _hover_background_e0e0e0'); 
                        echo SIMAHtml::modelIconsContracted($row, 16, [], '_hover_display_subactions _transparent_background _no_hover_border _no_border _hover_background_e0e0e0'); 
                        Yii::endProfile('part2.modelIcons',$_trace_cat);
//                        Logger::ttEndStat('Rows22',$_stats);
                    ?>

	</td>
        <?php 
        Yii::endProfile('part2',$_trace_cat);
        Yii::beginProfile('part3',$_trace_cat);
        
        foreach ($columns as $column) {
            Yii::beginProfile('part3.'.$column,$_trace_cat);
            
            ?>
            <td class='<?php echo str_replace('.', '-', $column);?>' column="<?php echo $column; ?>" 
                <?php 
                
                if (isset($column_widths[$column])) {?>
                    
                    style="width: <?=$column_widths[$column]?>px;" 
                <?php }?>  
                
            >
                    <?php
                        $column_display_value = $row->getAttributeDisplay($column);
                        if (is_array($column_display_value))
                        {
                            error_log("Funkcija getAttributeDisplay je vratila niz za model ".get_class($row)." i za kolonu $column, a treba string.");
                        }
                        echo $column_display_value;
                    ?>
		</td>
	<?php 
        
            Yii::endProfile('part3.'.$column,$_trace_cat);
        
                }?>
</tr>


<?php 

    Yii::endProfile('part3',$_trace_cat);


Yii::endProfile('whole',$_trace_cat); ?>


