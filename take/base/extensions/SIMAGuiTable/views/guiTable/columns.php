
<div class="sima-guitable-settings sima-layout-panel">
    <div id="<?php echo $id; ?>" class='columns_order_settings sima-layout-panel _horizontal'>
        <div class="sima-layout-fixed-panel sima-guitable-settings-header">
            <div style="padding:15px 10px 0px 10px;"><?php echo Yii::t('SIMAGuiTable.Translation', 'SettingsColumns'); ?></div>
        </div>
        <div class="sima-layout-panel">
            <ul class="columns_list">
                <?php
                foreach ($columns as $column)
                {
                    $checked = 'checked';
                    $disabled = '';
                    if (isset($column['hidden']) && $column['hidden'] == 'true')
                    {
                        $checked = '';
                    }
                    if (isset($column['disabled']) && $column['disabled'] == 'true')
                    {
                        $disabled = 'disabled';
                    }
                    $column = $column['name'];
                ?>
                    <div class="columns-list-item" data-column='<?php echo $column; ?>'>
                        <li><?php echo str_replace("<br />", "", $model->getAttributeLabel($column)); ?></li>
                        <input type="checkbox" name="<?php echo $column; ?>" onclick="column_check_box_clicked<?php echo $id; ?>($(this))" <?php echo $checked; ?> <?php echo $disabled; ?>>    
                        <?php
                            if ($disabled !== '')
                            {
                                ?>
                                <span class="delete_filter" style="font-size:10px;">
                                    <?php echo Yii::t('SIMAGuiTable.Translation', 'SettingsFilterIsSet'); ?>.
                                    <button style="font-size:10px !important;" class="sima-ui-button" title="<?php echo Yii::t('SIMAGuiTable.Translation', 'SettingsDeleteFilter'); ?>" onclick="delete_filter<?php echo $id; ?>($(this))"><?php echo Yii::t('SIMAGuiTable.Translation', 'SettingsDeleteFilter'); ?></button>
                                </span>
                                <?php
                            }
                        ?>
                    </div>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>

<script>
    $("#<?php echo $id; ?>").find('.columns_list').sortable();
    function column_check_box_clicked<?php echo $id; ?>(obj)
    {
        var count = 0;
        var checked;
        obj.parents('.columns_list:first').find('input').each(function() {
            if ($(this).is(":checked"))
            {
                count++;
                checked = $(this);
            }
        });
        if (count == 0)
        {
            obj.prop('checked', true);
        }
    }
    function delete_filter<?php echo $id; ?>(obj)
    {
        var div = obj.parents('.columns-list-item:first');
        div.find('input').prop("disabled", false);
        div.find('.delete_filter').hide();
        $('#<?php echo $table_id; ?>').simaGuiTable('deleteFilter', div.data('column'));
    }
    
</script>