<?php if (isset($sum_row)){
        ?>
	<tr class='sum <?php //echo $sum_row->getClasses()?>'>
                <?php if (!isset($without_order_number) || $without_order_number === false) { ?>
                    <td class="order-number"></td>
                <?php } ?>
		<td class='options'></td>
		<?php foreach ($columns as $column) {?>
			<td class='<?php echo str_replace('.', '-', $column)?>'>
				<?php 
                                    if ($model->isSum($column, $columns_type)) 
                                    {	
                                        $sum_column = $sum_row->getSumAttributeDisplay($column);
                                        if(is_numeric($sum_column))
                                        {
                                            echo SIMAHtml::number_format($sum_row->$column);
                                        }
                                        else
                                        {
                                            echo $sum_column;
                                        }
                                    }
                                ?>
			</td>
		<?php }?>
	</tr>
<?php }?>