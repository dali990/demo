<div class="sima-guitable-export">
    <div class="sima-export-title-dialog"><?php echo Yii::t('SIMAGuiTable.SIMAGuiTable', 'ChooseFormatYouWantToExportTo');?></div>
    
    <?php echo SIMAHtml::dropDownList('export-options', 'pdf', $export_list,[
        'class'=>'sima-document-export-dropdown',
        'id'=>'exportFormat'
    ]); ?>
    
    <div class="clearfix"></div>

    <div id='<?php echo $table_id ?>columns'>
        <div id="<?php echo $id; ?>" class='columns_order_settings scroll-container'>
            <div style="text-align: left; padding: 0; margin: 0;">
                <div class="sima-export-title-dialog"><?php echo Yii::t('SIMAGuiTable.SIMAGuiTable', 'ChooseColumnsToShowInTable');?></div>
                <div class="sima-export-info-help"><?php echo Yii::t('SIMAGuiTable.SIMAGuiTable', 'MoveRowsToMakeDifferentDisplayOrderOfColumnsToExport');?></div>
            </div>
            <ul class="columns_list">
                <?php
                foreach ($columns as $column)
                {
                    $checked = 'checked';
                    $disabled = '';
                    if (isset($column['hidden']) && $column['hidden'] == 'true')
                    {
                        $checked = '';
                    }
    //                if (isset($column['disabled']) && $column['disabled'] == 'true')
    //                {
    //                    $disabled = 'disabled';
    //                }
                    $column = $column['name'];
                ?>
                    <div class="columns-list-item" data-column='<?php echo $column; ?>'>
                        <li>
                            <input type="checkbox" name="<?php echo $column; ?>" onclick="column_check_box_clicked<?php echo $id; ?>($(this))" <?php echo $checked; ?> <?php echo $disabled; ?>>
                            <?php echo str_replace("<br />", "", $model->getAttributeLabel($column)); ?></li>
                            
                        <?php
                           // if ($disabled !== '')
                           // {
                                ?>
                                <!--<span class="delete_filter" style="font-size:10px;">-->
                                    <!--Imate postavljen filter za ovu kolonu.<button style="font-size:10px !important;" class="sima-ui-button" title="Obriši filter" onclick="delete_filter<?php echo $id; ?>($(this))">Obriši filter</button>-->
                                <!--</span>-->
                                <?php
                            //}
                        ?>
                    </div>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>

<script>
    $("#<?php echo $id; ?>").find('.columns_list').sortable();
    function column_check_box_clicked<?php echo $id; ?>(obj)
    {
        var count = 0;
        var checked;
        obj.parents('.columns_list:first').find('input').each(function() {
            if ($(this).is(":checked"))
            {
                count++;
                checked = $(this);
            }
        });
        if (count == 0)
        {
            obj.prop('checked', true);
        }
    }
    function delete_filter<?php echo $id; ?>(obj)
    {
        var div = obj.parents('.columns-list-item:first');
        div.find('input').prop("disabled", false);
        div.find('.delete_filter').hide();
        $('#<?php echo $table_id; ?>').simaGuiTable('deleteFilter', div.data('column'));
    }
    
</script>

