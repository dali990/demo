
<div id='<?=$id ?>' class='sima-guitable sima-layout-panel _horizontal <?=$class ?>' model='<?php echo get_class($model) ?>' columns_type='<?php echo $columns_type; ?>' 
     table_string='<?php echo $table_string; ?>' style="<?php echo ($this->auto_height===false)?"margin-bottom:20px;":''; ?>">            
    <div class='sima-guitable-options sima-layout-fixed-panel'>
        <?php foreach ($headerOptionRows as $headerOptionRow) { ?>      
                <div class="sima-guitable-header-row">
                    <?php
                        foreach ($headerOptionRow as $key => $option)
                        {                            
                            switch ($option)
                            {
                                case 'title':
                                    //ispisujemo title
                                    if (!empty($this->title)) { ?>
                                        <div class="sima-guitable-header-row-item sima-gui-table-title">
                                            <?php echo $this->title; ?>
                                        </div>                                
                                    <?php }
                                    break;
                                case 'table_settings':
                                    //ispisujemo table settings
                                    if ($this->table_settings === true) { ?>
                                        <div class='sima-guitable-header-row-item sima-guitable-settings-button-wrapper' style="margin-top: 6px;">
                                            <span class="sima-guitable-settings-button" onclick="$('#<?php echo $id ?>').simaGuiTable('table_settings')"></span>
                                        </div>
                                    <?php }
                                    break;
                                case 'show_add_button':
                                    //ispisujemo add_button
                                    if ($this->show_add_button === true) { ?>
                                        <div class="sima-guitable-header-row-item add_buttons">
                                            <?php
                                                foreach ($add_buttons as $key=>$add_button)
                                                {                
                                                    $onhover = $add_button['onhover'];
                                                    $classes = $add_button['classes'];
                                                    $onclick = $add_button['onclick'];

                                                    echo "<span title='$onhover' class='sima-gui-button add_button button $classes access' onclick='$onclick'></span>";                
                                                }
                                            ?>
                                        </div>
                                    <?php }
                                    break;
                                case 'pagination':
                                    //ispisujemo paginaciju
                                    if ($this->pagination === true) { ?>
                                        <div class='sima-guitable-header-row-item page_links'>
                                            <a class='first_link left' href="#"><span class='sima-guitable-navi-link button'></span></a> 
                                            <a class='prev_link left' href="#"><span class='sima-guitable-navi-link button'></span></a> 
                                            <div class="page_number">
                                                <input type="text" name="goto_page" value="1"> <span class='separator'>/</span>
                                                <span class='max_page_number'></span>
                                            </div>
                                            <a class='sima-guitable-navi-link next_link right' href="#"><span class='button'></span></a>
                                            <a class='sima-guitable-navi-link last_link right' href="#"><span class='button'></span></a>	
                                        </div>
                                    <?php }
                                    break;
                                case 'filters_in':
                                    //ispisujemo dugmice za filtere u tabeli
                                    if ($this->showFiltersIn() === true) {                                        
                                    ?>
                                        <div class='sima-guitable-header-row-item filters_in buttons'>
                                            <div class="sima-ui-button-group-horizontal-round">
                                                <button class="sima-ui-button" onclick="$('#<?php echo $id ?>').simaGuiTable('inst_populate')"><?php echo Yii::t('BaseModule.GuiTable', 'ApplyFilterLowLetter'); ?></button>
                                                <button class="sima-ui-button" onclick="$('#<?php echo $id ?>').simaGuiTable('reset')"><?php echo Yii::t('BaseModule.GuiTable', 'RemoveFilterLowLetter'); ?></button>
                                            </div>
                                        </div>
                                    <?php }
                                    break;
                                case 'export':
                                     $uniq = SIMAHtml::uniqid();
                                    //ispisujemo export dugmice (za sada je samo pdf)
                                    if (is_array($this->export) && count($this->export) > 0) { ?>
                                        <div class='sima-guitable-header-row-item pdf buttons'>
                                            <div class="sima-ui-button-group-horizontal-round">
                                                <?php // foreach ($this->export as $export) {
                                                    //if ($export === 'pdf') { ?>
<!--                                                        <button class="sima-ui-button" onclick="$('#<?php //echo $id ?>').simaGuiTable('table_to_pdf')">pdf</button>-->
                                                
                                                <button class="sima-ui-button" onclick='$("#<?php echo $id ?>").simaGuiTable("export",<?php echo CJSON::encode($this->export); ?>);'>
                                                    <?php echo Yii::t('SIMAGuiTable.SIMAGuiTable', 'export');?>
                                                </button>
                                                
                                                    <?php // }
                                               // } ?>
                                                
                                            </div>
                                        </div>
                                    <?php }
                                    break;
                                case 'custom_buttons':
                                    //ispisujemo custom dugmice
                                    if (is_array($this->custom_buttons) && count($this->custom_buttons) > 0) {
                                        $custom_buttons = $this->custom_buttons;
                                        $custom_buttons_group = (isset($custom_buttons['horizontal_group']) && $custom_buttons['horizontal_group']===true)?true:false;
                                        ?>
                                        <div class="sima-guitable-header-row-item custom_buttons buttons">
                                            <div class="buttons <?php echo ($custom_buttons_group===true)?'sima-ui-button-group-horizontal-round':''; ?>">
                                                <?php foreach ($this->custom_buttons as $key => $custom_button) {
                                                    if (is_array($custom_button)) {
                                                        ?>
                                                           <button class="sima-ui-button <?php echo isset($custom_button['class'])?$custom_button['class']:''; ?>" 
                                                                   onclick="var _this = $(this); _this.addClass('_disabled'); setTimeout(function(){_this.removeClass('_disabled');},500);<?php echo $custom_button['func']; ?>" 
                                                               title="<?php echo isset($custom_button['title'])?$custom_button['title']:''; ?>">
                                                               <?php echo $key; ?>
                                                           </button>
                                                        <?php
                                                    }
                                                 } ?>
                                            </div>
                                        </div>
                                    <?php }
                                    break;
                                case 'additional_options_html':
                                    if (!empty($this->additional_options_html)) { ?>
                                        <div class="sima-guitable-header-row-item additional-options">
                                            <?=$this->additional_options_html?>
                                        </div>
                                    <?php }
                                    break;
                                case 'filters_out':
                                    //ispisujemo filtere van gui tabele
                                    if ($this->filters_out === true && !empty($guitable_filters)) { ?>
                                        <div class="sima-guitable-header-row-item sima-gui-table-filters">
                                            <?php
                                            //ispisuje sve filtere koji se prikazuju van tabele
                                            foreach ($guitable_filters as $column => $guitable_filter)
                                            {                
                                                ?>
                                                <div class="sima-gui-table-filter" column='<?php echo $column; ?>'>
                                                    <?php
                                                    if ($column === 'text')
                                                    {
                                                        if (count($model->modelSettings('textSearch')) > 0)
                                                        {
                                                            ?>
                                                            <div class="searchModelField">
                                                                <?php
                                                                echo CHtml::textField('Text', '', array(
                                                                    'id' => 'SearchModel' . $id,
                                                                    'name' => get_class($model) . '[text]',
                                                                    'onkeyup' => "sima.misc.searchModelText($(this))",
                                                                    'placeholder' => 'Pretraži',
                                                                ));
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    else
                                                    {                        
                                                        echo $guitable_filter;
                                                    }
                                                    ?>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    <?php }
                                    break;
                                default:
                                    break;
                            }
                        }
                    ?>
                    <div class="clearfix"></div>
                </div>
        <?php } ?>
    </div>
    <div class='sima-guitable-head sima-layout-fixed-panel'>
        <div class='table_resize_wrapper' style = "width: 50000px">
            <div class="sima-guitable-head-left"></div>
            <!--<div class="sima-guitable-head-right"></div>-->
            <table id='<?php echo $id ?>_list_head_table'>
                <thead>
                    <tr class='titles'>
                        <?php if (!isset($without_order_number) || $without_order_number === false) { ?>
                            <th class="order-number">#</th>
                        <?php } ?>
                        <th class='options' style="width: <?=26+count($model->modelSettings('statuses'))*16?>px;">
                            <span class="options_name">
                                <?php                                
                                    foreach ($model->modelSettings('statuses') as $status_key => $status_value)
                                    {
                                        $column_allow_null = $model->isColumnAllowNull($status_key);
                                        ?>
                                        <span type="<?php echo $status_key; ?>" title="<?php echo $status_value['title']; ?>" 
                                              class="sima-icon _16 _status-filter _status-initial <?php echo $status_key; ?>" 
                                              name="<?php echo get_class($model) . "[" . $status_key . "]" ?>" value="" 
                                              data-allow_null="<?php echo $column_allow_null?'true':'false'; ?>"
                                        >
                                        </span>
                                        <?php
                                    }
                                ?>
<!--                                <span style="display:inline-block; height: 16px; line-height: 16px;"><?php // echo Yii::t('BaseModule.Common', 'Options') ?></span>
                                <span class="clearfix"></span>-->
                            </span>
                            <span class="multiselect_icons">
                                <?php
                                echo SIMAHtml::modelMultiselectIconsContracted($model, $id, $multiselect_options, 16);
                                ?>
                            </span>
                        </th>
                        <?php
                        //prolazi kroz svaku kolonu
                        foreach ($columns as $column)
                        {
                            ?>
                            <th style="width: <?=$column['width']?>px;" class="<?php echo str_replace('.', '-', $column['name']);
                                echo $column['hidden'] == 'true' ? ' _hidden' : ''; ?>" 
                                column="<?=$column['name']?>" 
                                edit="<?=$column['edit']; ?>"  
                                number_field='<?=$column['number_field'] ? 'true' : 'false'?>' 
                                data-min_width='<?=isset($column['min_width'])?$column['min_width']:''?>'
                                data-max_width='<?=isset($column['max_width'])?$column['max_width']:''?>'
                                data-column_filter_type='<?=$column['column_filter_type']?>'
                                title="<?=$column['label']?>"
                            >
                                <?php                                     
                                    if (in_array($column['name'], $order_by))
                                    {
                                        echo "<span class='order_by button' value='".$column['name']."'></span>";
                                    }
                                    if ($column['filter_type'] === null || $column['filter_type'] == '')
                                    {
                                        if ($column['name'] == 'statuses')
                                        {                                            
                                            foreach ($column['model']->modelSettings('statuses') as $status_key => $status_value)
                                            {
                                                $column_allow_null = $column['model']->isColumnAllowNull($status_key);
                                                ?>
                                                <span title="<?php echo $status_value['title']; ?>" 
                                                      class="sima-icon _16 _status-filter _status-initial" 
                                                      name="<?php echo str_replace('statuses', $status_key, $column['input_name']); ?>" value="" 
                                                      data-allow_null="<?php echo $column_allow_null?'true':'false'; ?>"
                                                >
                                                </span>
                                                <?php
                                            }
                                        }
                                        echo "<span class='text'>" . $column['label'] . "</span>";
                                    }
                                    else if ($column['hide_filter'] == 'true' || !isset($column['filter']))
                                    {
                                        echo "<span class='text'>" . $column['label'] . "</span>";
                                    }
                                    else
                                    {
                                        echo $column['filter'];
                                    }
                                    
                                ?>
                            </th>
                            <?php                         
                        } 
                            ?>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="sima-guitable-loading"></div>
    </div>
    <div class='sima-guitable-body <?php echo ($this->auto_height===true) ? 'sima-layout-panel' : ''; ?>' 
            style="<?php echo ($this->auto_height===true)?"min-height:100px;":''; ?>">
        <div class='table_resize_wrapper'>
            <div class='loading_circle 'style="position: absolute; z-index: 100; display: none">
                <img src="images/loading-circle.gif"/>
            </div>
            <div class='load_less'>
                <a href="#"> Učitaj prethodne...</a>
            </div>

            <table id='<?php echo $id ?>_list_body_table'>
                <tbody class='tbody'></tbody>
            </table>

            <div class='load_more'>
                <a href="#"> Učitaj sledeće...</a>
            </div>

        </div>
    </div>
    <div class='sima-guitable-foot sima-layout-fixed-panel'>
        <div class='table_resize_wrapper'>
            <table>
                <tfoot></tfoot>
            </table>
        </div>
    </div>
</div>
