/* global sima */

/**
 *  Document   : jquery.simaGuiTable
 Created on : Aug 13, 2013, 3:14:50 PM
 Author     : sasa
 Description:
 
 * @param {type} $
 * @returns {undefined}
 */
(function($) {
    var methods = {
        init: function(params)
        {           
            var _table = this;
            
            _table.data('search_text', null);
            _table.data('search_model_text', null);

            _table.data('selected_sum_status', false);
//            _table.data('selected', null);
            _table.data('multiselect', false);
            _table.data('selectRow', true);
            
//            _table.data('filter', null);            

            _table.data('fixed_filter', {});
            _table.data('select_filter', {});
            _table.data('populate_group', sima.uniqid());
            
            _table.data('curr_page', 1);
            _table.data('max_page', 1);

            _table.data('globalTimeout', null);
            _table.data('last_id', 0);
            
            _table.simaGuiTable('setMaxPage', _table.data('max_page'));

            _table.data('_header', _table.find('div.sima-guitable-head'));
            _table.data('_body', _table.find('div.sima-guitable-body'));
            _table.data('_footer', _table.find('div.sima-guitable-foot'));

            _table.data('selection_change_func', []);
            _table.data('row_select_func', []);
            _table.data('row_unselect_func', []);
            _table.data('row_dblclick_func', []);
            _table.data('multiselect_func', []);

            _table.data('refresh_row_trigger_func', []);
            _table.data('append_row_trigger_func', []);

            _table.data('state', []);
            _table.data('globalTimeout', null);
             
            _table.data('group_by', '');
            _table.data('order_by', '');
            
            _table.data('shiftFirstSelected', null); //koristi se kod shift selektovanje
            _table.data('ctrlLastSelected', null);
            
            _table.data('column_edit', null);
            _table.data('column_edit_old_value', null);
            _table.data('add_inline_row', null);
            
            _table.data('table_settings', params.table_settings); //promenljiva koja oznacava da li je vidljivo dugme settings
            
            _table.data('file_tag_filter_recursive', _table.find('.file_tag_filter_recursive'));
            _table.data('span_group_by', _table.find('span.group_by'));
            _table.data('span_order_by', _table.find('span.order_by'));
            _table.data('first_link', _table.find('.first_link'));
            _table.data('next_link', _table.find('.next_link'));
            _table.data('prev_link', _table.find('.prev_link'));
            _table.data('last_link', _table.find('.last_link'));
            _table.data('load_more', _table.find('.load_more'));
            _table.data('load_less', _table.find('.load_less'));
            _table.data('goto_page', _table.find('input[name=goto_page]'));

            _table.data('file_tag_filter_recursive').on('click', {table: _table}, file_tag_filter_recursive);
            _table.data('span_group_by').on('click', {table: _table}, group_by);
            _table.data('span_order_by').on('click', {table: _table}, order_by);
            _table.data('first_link').on('click', {table: _table}, first_link);
            _table.data('next_link').on('click', {table: _table}, next_link);
            _table.data('prev_link').on('click', {table: _table}, prev_link);
            _table.data('last_link').on('click', {table: _table}, last_link);
            _table.data('load_more').on('click', {table: _table}, load_more);
            _table.data('load_less').on('click', {table: _table}, load_less);
            _table.data('goto_page').on('keydown', {table: _table}, goto_page);
            _table.data('_body').on('scroll', {table: _table}, body_scroll);
            _table.data('_header').on('click', '._status-filter', {table: _table}, statuses);       
            $(document).on('keydown', {table: _table}, onKeyPressed);
            
            params = params || {};

            _table.data('_calc_sum', (typeof (params.has_sum) !== 'undefined') && params.has_sum);
            _table.data('disableMultiSelect', params.disableMultiSelect);
            _table.data('selectRowOnAppend', params.selectRowOnAppend);
            _table.data('selectRowOnAppendAndLaunchAddButton', params.selectRowOnAppendAndLaunchAddButton);
            _table.data('launchAddButtonOnLoad', params.launchAddButtonOnLoad);
            _table.data('you_can_not_export_empty_table', params.translation.you_can_not_export_empty_table);
            _table.data('export_options', params.translation.export_options);
            _table.data('you_must_choose_format', params.translation.you_must_choose_format);
            _table.data('choose_at_least_one_column', params.translation.choose_at_least_one_column);
            _table.data('export', params.translation.export);
            _table.data('column_max_width', params.column_max_width);
            _table.data('column_min_width', params.column_min_width);
            
            _table.data('without_order_number', params.without_order_number);
            
            if (typeof(params.on_populate_func) !== 'undefined')
            {
                _table.data('on_populate_func', params.on_populate_func);
            }
            
            if (typeof(params.add_button) !== 'undefined')
            {
                _table.simaGuiTable('setAddButton',params.add_button);
            }
            else
            {
                _table.simaGuiTable('setAddButton');
            }
            if (typeof(params.fixed_filter) !== 'undefined')
            {
                _table.simaGuiTable('setFixedFilter', params.fixed_filter);
            }
            if (typeof(params.select_filter) !== 'undefined')
            {
                _table.simaGuiTable('setSelectFilter', params.select_filter);
            }
            if (typeof(params.setRowDblClick) !== 'undefined')
            {
                _table.simaGuiTable('setRowDblClick',params.setRowDblClick);
            }
            if (typeof(params.setRowSelect) !== 'undefined')
            {
                _table.simaGuiTable('setRowSelect',params.setRowSelect);
            }
            if (typeof(params.setRowUnSelect) !== 'undefined')
            {
                _table.simaGuiTable('setRowUnSelect',params.setRowUnSelect);
            }
            if (typeof(params.onSelectionChange) !== 'undefined')
            {
                _table.simaGuiTable('onSelectionChange',params.onSelectionChange);
            }
            if (typeof(params.setAppendRowTrigger) !== 'undefined')
            {
                _table.simaGuiTable('setAppendRowTrigger',params.setAppendRowTrigger);
            }
            if (typeof(params.setRefreshRowTrigger) !== 'undefined')
            {
                _table.simaGuiTable('setRefreshRowTrigger',params.setRefreshRowTrigger);
            }
            if (typeof(params.setMultiSelect) !== 'undefined')
            {
                _table.simaGuiTable('setMultiSelect',params.setMultiSelect);
            }

            if (typeof(params.populate) !== 'undefined')
            {
                var _par = {};
                if (typeof params.default_selected_row_id !== 'undefined' && params.default_selected_row_id !== null)
                {
                    _par.default_selected_row_id = params.default_selected_row_id;                   
                }
                _table.simaGuiTable('populate', _par);
            }
            
            var alreadyclickedTimeout;
            var alreadyclicked = false, clickDelay = 300; //milliseconds //TODO: moze da se postavi kao podesavanje
            _table.on('click', '.row', function(event) {
                var row = $(this);
                //Sasa A. - proverava se samo attr onclick jer nije jednostavno da se proveri za neki element da li je zakacen click event
                // MilosJ - samo ako nije otvaranje modela ctrl+click
                if(sima.getModelDisplayhtmlClickOpenConf() === 'klik' && !$(event.target).is('td') && typeof $(event.target).attr('onclick') !== 'undefined')
                {
                    return;
                }

                if ((!$(event.target).is('span') || ($(event.target).is('span') && !$(event.target).parent().hasClass('options'))) && !$(event.target).hasClass('options'))
                {                    
                    if (alreadyclicked)
                    {
                        alreadyclicked = false; // reset
                        clearTimeout(alreadyclickedTimeout); // prevent this from happening
                        dblclick_row(_table, event, row);
                    }
                    else
                    {
                        alreadyclicked = true;
                        alreadyclickedTimeout = setTimeout(function() {
                            alreadyclicked = false; // reset when it happens
                            if (_table.data('selectRow')) {
                                var type = (event.ctrlKey)?'ctrlKey':((event.shiftKey)?'shiftKey':'');
                                click_row(_table, row, $(event.target), type);
                            }
                        }, clickDelay); // <-- dblclick tolerance here
                    }
                }
            });
            _table.data('_body').scroll(function() {
                _table.data('_header').scrollLeft(_table.data('_body').scrollLeft());
                _table.data('_footer').scrollLeft(_table.data('_body').scrollLeft());
                resetSliderPositions(_table);
            });
            _table.data('_footer').scroll(function() {
                _table.data('_header').scrollLeft(_table.data('_footer').scrollLeft());
                _table.data('_body').scrollLeft(_table.data('_footer').scrollLeft());
                resetSliderPositions(_table);
            });            
            _table.data('_header').find('table').each(function()
            {
                makeResizable(_table, $(this));
            });
            _table.data('_header').css('overflow-x', 'hidden');
            _table.data('_header').css('overflow-y', 'scroll');

            _table.data('_body').css('overflow-y', 'auto');

            if (typeof (params.has_sum) === 'undefined')
            {
                _table.data('_body').css('overflow-x', 'auto');
                _table.data('_footer').hide();
            } 
            else
            {
                _table.data('_body').css('overflow-x', 'hidden');
                _table.data('_footer').css('overflow-x', 'auto');
                _table.data('_footer').css('overflow-y', 'auto');
            }
            
            setThIcons(_table);
            setThMaxMinWidth(_table);
            //kod koji uklanja border oko table td na klik kada se drzi ctrl ili shift(bugfix za firefox, chrome je ok)
            $('table').mousedown(function (event) {
                if (event.ctrlKey) 
                {
                    event.preventDefault();
                }
                if (event.shiftKey) 
                {
                    event.preventDefault();
                }
            });
//            setTimeout(function(){
                _table.trigger('sima-layout-allign',_table.height(),_table.width(),'TRIGGER_guitable_adjust_cell_widths');
//            },10);
            //pokrece add button ako je inicijalno zadato            
            if (_table.data('launchAddButtonOnLoad') === true)
            {                
                _table.find('.sima-gui-button.add_button').trigger('click');
            }
            
            sima.pushActiveFunctionalElement(_table);
            
            _table.data('loading_obj',  new Nanobar({
                target: _table.find('.sima-guitable-loading')[0]
            }));
            
        },
        getSelected: function() {
            return this.data('_body').find('tr.selected');
        },
        getSelectedIds: function(){
            var selected_ids = [];
            var selected_rows = this.data('_body').find('tr.selected');
            
            selected_rows.each(function(){
                var _this = $(this);
                var model_id = _this.attr('model_id');
                selected_ids.push(model_id);
            });
            
            return selected_ids;
        },
        getFixedFilter: function() {
            return $(this).data('fixed_filter');
        },
        /**
         * postavlja fiksni filter
         * @param {object} filter
         * @param {type} repopulate
         */
        setFixedFilter: function(filter, repopulate)
        {            
            var _table = this;
            repopulate = repopulate || false;
            _table.data('fixed_filter', filter);
            setSelectFixedFilter(_table, filter, '', true);
            if (repopulate)
            {
                _table.simaGuiTable('unselect_rows');
                _table.simaGuiTable('populate');                
            }
        },
        getAllFilters: function() {
            return collect_data($(this));
        },
        setSelectFilter: function(select_filter, repopulate) 
        {
            var _table = this;
            repopulate = repopulate || false;
            _table.data('select_filter', select_filter);
            setSelectFixedFilter(_table, select_filter, '', false);                        
            if (repopulate)
            {
                _table.simaGuiTable('unselect_rows');
                _table.simaGuiTable('populate');                
            }
        },
        setUnselectAllRows: function() 
        {
            var _table = this;
            _table.simaGuiTable('unselect_rows');
        },
        resetFixedFilter: function() {
            this.data('fixed_filter', {});
        },
        setMaxPage: function(new_max_page) {
            this.data('max_page', new_max_page);
            this.find('input[name=goto_page]').val(this.data('curr_page'));
            this.find('span.max_page_number').html(this.data('max_page'));
        },
        setRowDblClick: function(func) {
            this.data('row_dblclick_func').push(func);
        },
        onSelectionChange: function(func) {
            this.data('selection_change_func').push(func);
        },
        setRowSelect: function(func) {
            this.data('row_select_func').push(func);
        },
        setAppendRowTrigger: function(func) {
            this.data('append_row_trigger_func').push(func);
        },
        setRefreshRowTrigger: function(func) {
            this.data('refresh_row_trigger_func').push(func);
        },
        setRowUnSelect: function(func) {
            this.data('row_unselect_func').push(func);
        },
        setMultiSelect: function(func) {
            this.data('multiselect_func').push(func);
        },
        setTextSearch: function(ts) {
            this.data('search_text', ts);
        },
        disableMultiSelect: function() {            
            disableMultiselectOptions(this);
        },
        /**
         * metod koji iskljucuje mogucnost selektovanja redova u tabeli
         */
        disable_row_click: function() {
            this.data('selectRow', false);
        },
        /**
         * metod selektuje red u tabeli koji ima klasu koja je ulazni parametar funkcije , tag
         * @param {type} tag
         */
        select_row: function(tag) {
            click_row(this, this.find('.' + tag));
        },
        select_rows: function(tags) {
            var _this = $(this);
            var ids = [];
            for (var i = 0; i < tags.length; i++) 
            {
                var row = _this.find('.sima-guitable-row.' + tags[i]);
                if (!sima.isEmpty(row))
                {
                    ids.push(row.attr('model_id'));
                }
            }
            _select_rows(_this, ids);
        },
        /**
         *  metod deselektuje sve redove u tabeli koji su selektovani, tj. koji imaju klasu 'selected' i polje koje selektovano
         */
        unselect_rows: function() {
            var _table = this;
            _table.find('tr td.field_selected').removeClass('field_selected');
            _table.find('tr.selected').each(function() {
                var temp = $(this);
                temp.removeClass('selected');
                for (var i = 0; i < _table.data('row_unselect_func').length; i++)
                {
                    sima.executeFunction(_table.data('row_unselect_func')[i], [temp]);
                }

            });
            //ako je bio multiselect onda se vraca na stanje koje nije multiselect
            disableMultiselectOptions(_table);
        },
        /**
         * 
         *  metod koji niz funkcija za dupli klik postavlja da bude prazan
         */
        clean_dblclick_func: function() {
            this.data('row_dblclick_func', []);
        },
        populate: function(params) {            
            var _table = this;
            var last_id = _table.data('last_id');
            var local_id = ++last_id;
            _table.data('last_id', last_id);
            if (_table.data('globalTimeout') !== null)
                clearTimeout(_table.data('globalTimeout'));
            _table.data('globalTimeout', setTimeout(function()
            {
//                if (local_id == last_id)
                if (parseInt(local_id) === parseInt(last_id))
                {
                    _table.data('curr_page', 1);
                    populate(_table, params);
                }
            }, 500));
            return false;
        },
        append_row: function(model_ids, params) {
            var _table = this;

            if (!$.isArray(model_ids))
            {
                model_ids = [model_ids];
            }
            
            if (typeof (params) !== 'object')
            {
                params = {};
            }
            
            var inline = (typeof params.inline !== 'undefined') ? params.inline : false;

            var data = collect_data(_table);
            data[_table.attr('model')+"[ids]"] = model_ids;
            data['model_ids'] = model_ids;

            addFakeRow(_table, true);

            sima.ajax.get('guitable/getRow', {
                get_params: {
                    model: _table.attr('model')
                },
                data: data,
                async: true,
                success_function: function(response) {
                    if (sima.inDOM(_table))
                    {
                        removeFakeRow(_table);
                        
                        var rows = response.rows;
                        $.each(rows, function(key, value) {
                            if (_table.data('_body').find("tr[model_id='"+value.model_id+"']").length === 0)
                            {
                                if (inline)
                                {
                                    _table.data('_body').find('tr.sima-guitable-row:not(.inline_row):last').after(value.row);
                                }
                                else
                                {
                                    _table.data('_body').find('.page_break:first').after(value.row);
                                }
                                
                                var added_row = _table.data('_body').find("tr[model_id='"+value.model_id+"']");
                                added_row.addClass('append_row_highlight', 2000).effect(
                                        "highlight", {}, 5000, function(){
                                            added_row.removeClass('append_row_highlight', 2000);
                                            if (typeof params.addInlineRowAgain !== 'undefined' && params.addInlineRowAgain === true
                                                    && _table.find('.sima-guitable-row.inline_row').length > 0)
                                            {                                
                                                added_row.css({opacity:0.3});                                
                                            }
                                        });
                                if (_table.data('selectRowOnAppend') === true)
                                {
                                    click_row(_table, added_row);
                                }
                                if (_table.data('selectRowOnAppendAndLaunchAddButton') === true)
                                {
                                    click_row(_table, added_row, null, null, true);
                                }
                                
                                //pozivaju se sve append row trigger funkcije
                                for (var i = 0; i < _table.data('append_row_trigger_func').length; i++)
                                {
                                    sima.executeFunction(_table.data('append_row_trigger_func')[i], [added_row]);
                                }
                            }
                        });
                        
                        setNumberFieldClass(_table);
                        
                        setColumnsOrderNumber(_table);
                        
                        checkLoadMoreMessage(_table);
                        checkLoadLessMessage(_table);
                        checkEmptyTableMessage(_table);
                        
                        if (!_table.data('_calc_sum'))
                        {
                            adjust_cell_widths(_table);
                        }
                        else
                        {
                            if (_table.data('multiselect'))
                            {
                                populate_selected_sum(_table);
                            }
                            else
                            {
                                populate_sum(_table);
                            }
                        }
                    }
                    else
                    {
                        console.warn('nije pronadjena tabela kod append row');
                    }
                }
            });
        },
        remove_row: function(model_id)
        {
            var _table = this;
            _table.data('_body').find('.sima-guitable-row[model_id=' + model_id + ']').remove();
        },
        //refresuje celu tabelu, podesava sirinu kolona i visinu tabele
        refresh: function()
        {
            var _table = this;
            adjust_cell_widths(_table, '', true);
        },
        refresh_row: function(model_ids) {            
            var _table = this;
            var data = collect_data(_table);
            data[_table.attr('model')+"[ids]"] = model_ids;
            data['model_ids'] = model_ids;
            
            //disable-ujemo sve redove za koje se radi refresh
            $.each(model_ids, function(key, model_id) {
                var table_row = _table.data('_body').find('.sima-guitable-row[model_id=' + model_id + ']');
                if (!sima.isEmpty(table_row))
                {
                    table_row.addClass('_disabled');
                }
            });
            
            sima.ajax.get('guitable/getRow', {
                get_params: {
                    model: _table.attr('model')
                },
                async: true,
                data: data,
                success_function: function(response) {    
                    if (sima.inDOM(_table)) 
                    {
                        var rows = response.rows;
                        $.each(rows, function(key, value) {
                            var old = _table.data('_body').find('.sima-guitable-row[model_id=' + value.model_id + ']');
                            //poziva refresh za sva searchField polja unutar reda koji se refresuje(to se radi zbog toga sto u nekim slucajevima result ostane)
                            old.find('.sima-ui-sf').each(function(){
                                $(this).simaSearchField('hideResult');
                            });
                            old.after(value.row);
                            if (old.hasClass('selected'))
                            {
                                if (value.row === '') //ako je brisanje onda deselektujemo red
                                {
                                    for (var i = 0; i < _table.data('row_unselect_func').length; i++)
                                    {
                                        sima.executeFunction(_table.data('row_unselect_func')[i], [old]);
                                    }
                                }
                                else
                                {
                                    var filed_selected_index = old.find('td.field_selected').index();
                                    if (old.next().length > 0)
                                    {
                                        old.next().addClass('selected').find('td').eq(filed_selected_index).addClass('field_selected');
                                    }
                                    else
                                    {
                                        _table.data('_body').find('.sima-guitable-row:first').addClass('selected').find('td').eq(filed_selected_index).addClass('field_selected');
                                    }
                                }
                            }
                            old.next().find('.order-number').html(old.find('.order-number').html());
                            old.remove();
                        });
                        for (var i = 0; i < _table.data('refresh_row_trigger_func').length; i++)
                        {
                            sima.executeFunction(_table.data('refresh_row_trigger_func')[i], [rows]);
                        }
                        if (!_table.data('_calc_sum'))
                        {
                            adjust_cell_widths(_table);
                        }
                        else
                        {
                            if (_table.data('multiselect'))
                                populate_selected_sum(_table);
                            else
                                populate_sum(_table);
                        }

                        //ako tabela koja se updejtuje nije u trenutno selektovanom tabu onda se tom tabu dodaje klasa broken
                        var current_panel = _table.parents('.sima-main-tabs-panel:first').not('.sima-ui-active');
                        if (current_panel.length > 0)
                        {
                            current_panel.addClass('sima-layout-broken');
                        }                    
                        setNumberFieldClass(_table);
                        
                        setColumnsOrderNumber(_table);

                        checkLoadMoreMessage(_table);
                        checkLoadLessMessage(_table);
                        checkEmptyTableMessage(_table);
    //                    $('body').append(response.processOutput);
    
                        //enable-ujemo sve redove za koje se radi refresh
                        $.each(model_ids, function(key, model_id) {
                            var table_row = _table.data('_body').find('.sima-guitable-row[model_id=' + model_id + ']');
                            if (!sima.isEmpty(table_row))
                            {
                                table_row.removeClass('_disabled');
                            }
                        });
                    }
                }
            });
        },
        inst_populate: function() {
            var _table = this;
            if (_table.data('globalTimeout') !== null)
                clearTimeout(_table.data('globalTimeout'));
            _table.data('globalTimeout', setTimeout(function()
            {
                populate(_table);
            }, 500));
        },
        reset: function() {
            var _table = this;
            _table.data('curr_page', 1);
            _table.find('.sima-guitable-options .sima-gui-table-filters .sima-ui-sf').each(function() {
                $(this).simaSearchField('empty');
            });
            _table.find('.sima-guitable-options .sima-gui-table-filters').find('input').each(function() {
                $(this).val('');
            });
            _table.find('.sima-guitable-options .sima-gui-table-filters').find('select').each(function() {
                $(this).val('');
            });
            _table.data('_header').find('.sima-ui-sf').each(function() {
                $(this).simaSearchField('empty');
            });
            _table.data('_header').find('input').each(function() {
                $(this).val('');
            });
            _table.data('_header').find('select').each(function() {
                $(this).val('');
            });
            _table.data('_header').find('._status-filter').removeClass('_ok').removeClass('_not-ok').removeClass('_intermediate').addClass('_status-initial').attr('value','');
//            if (_table.data('search_model_text') != null)
            if (_table.data('search_model_text') !== null)
                _table.data('search_model_text', null);
            if (_table.data('globalTimeout') !== null)
                clearTimeout(_table.data('globalTimeout'));
            _table.data('globalTimeout', setTimeout(function()
            {
                populate(_table);
            }, 500));
        },
        
        export: function(export_list){
            var _table = this;
            
            var number_of_rows = _table.find('.sima-guitable-row.row').length;
            if(number_of_rows === 0)
            {
                sima.dialog.openWarn(_table.data('you_can_not_export_empty_table'));
                return;
            }
            
            var uniq_id = sima.uniqid();
            var columns = [];
            var export_list_string = export_list;
            _table.data('_header').find('th:not(.options)').each(function() {
                var column = {};
                if ($(this).hasClass('order-number'))
                {
                    column.name = '#';
                }
                else
                {
                    column.name = $(this).attr('column');
                    column.hidden = ($(this).hasClass('_hidden'))?true:false;
                    column.disabled = (isSetFilter(_table, $(this).attr('column')))?true:false;
                }
                columns.push(column);
            });
            
            sima.ajax.get('guitable/exportOptions', {
                get_params: {
                    id:uniq_id,
                    model:_table.attr('model'),
                    table_id:_table.attr('id')
                },
                data: {columns:columns, export_list: export_list_string},
                async: true,
                loadingCircle: $('#body_sync_ajax_overlay'),
                success_function: function(response) {
                    var html = {};
                    var yes_func_params = {};
                    var no_func_params = {};
                    html.title = _table.data('export_options');
                    html.html = response.html;
                    var table_id = response.table_id;
                    yes_func_params.func = function(obj)
                    {
                        var columns = [];
                        $('#'+table_id+'columns').find('.columns-list-item input').each(function() 
                        {
                            if ($(this).is(':checked'))
                            {
                                columns.push($(this).parent().parent().data('column'));
                            }
                        });
                        var selected_option = $('#exportFormat option:selected').filter(':selected').text();
                        if (sima.isEmpty(selected_option))
                        {
                            sima.dialog.openWarn(_table.data('you_must_choose_format'));
                        }
                        if (sima.isEmpty(columns))
                        {
                            sima.dialog.openWarn(_table.data('choose_at_least_one_column'));
                        }
                        else
                        {
                            sima.dialog.close();
                            //long ajax poziv koji ce da genrise temporary file(pdf/csv) i vratice putanju do tog fajla, a u sucess od ajax poziva pozives download tog fajla
                            sima.ajaxLong.start('guitable/generateFileToExport', {
                                showProgressBar: _table.data('_body'),
                                data:{
                                    model: _table.attr('model'),
                                    columns_type: _table.attr('columns_type'),
                                    format: selected_option,
                                    columns: columns, 
                                    guitable_filter: collect_data(_table)
                                },
                                onEnd: function(response) {                                        
                                    sima.temp.download(response.temp_file_path, response.download_name);                                             
                                }
                            });
                        }
                    };
                    yes_func_params.title = _table.data('export');
                    no_func_params.hidden = true;
                    sima.dialog.openYesNo(html, yes_func_params, no_func_params);
                }
            });
        },
        
        putInFilter: function(column, data) {
            var _table = this;
            _table.data('_header').find('.' + column + ' input').val(data);
        },
        table_settings: function() {
            var _table = this;
            var uniq_id = sima.uniqid();
            var columns = [];
            _table.data('_header').find('th:not(.order-number):not(.options)').each(function() {
                var column = {};
                column.name = $(this).attr('column');
                column.hidden = ($(this).hasClass('_hidden'))?true:false;
                column.disabled = (isSetFilter(_table, $(this).attr('column')))?true:false;
                columns.push(column);
            });
            sima.ajax.get('guitable/getColumns', {
                get_params: {
                    id:uniq_id,
                    model:_table.attr('model'),
                    table_id:_table.attr('id')
                },
                data: {columns:columns},
                async: true,
                loadingCircle: $('#body_sync_ajax_overlay'),
                success_function: function(response) {
                    var html = {};
                    var yes_func_params = {};
                    var no_func_params = {};
                    html.title = sima.translate('Adjustment');
                    html.html = response.html;
                    yes_func_params.func = function saveSettings(obj)
                        {
                            var settingsJSON = new Array();
                            var j = 0;
                            $('#'+uniq_id).find('.columns-list-item input').each(function() {
                               var column = {};
                               column.name = $(this).attr('name');
                               column.width =  _table.data('_header').find("th[column='"+$(this).attr('name')+"']").attr('width');
                               column.hidden = (!$(this).is(':checked'))?true:false;                               
                               settingsJSON[j] = column;
                               j++;
                            });
                            _table.simaGuiTable('applyColumnSettings', settingsJSON);
                            sima.dialog.close();
                            _table.simaGuiTable('populate');
                        };
                    yes_func_params.title = sima.translate('Save');
                    no_func_params.hidden = true;
                    sima.dialog.openYesNo(html, yes_func_params, no_func_params);
                }
            });
        },
        applyColumnSettings: function(columns) {
            var _table = this;
            
            if (typeof columns !== 'undefined')
            {
//                if (columns != 'all')
                if (columns !== 'all')
                {
                    var temp_array = [];
                    $.each(columns, function(key, value) {
                        var column = value;
                        var hidden = false;
                        if (typeof value === 'object')
                        {
                            column = value.name;
//                            if (value.hidden == true)
                            if (value.hidden === true)
                            {
                                hidden = true;
                            }
                        }
                        var current_th = _table.data('_header').find("[column='"+column+"']");
                        _table.data('_header').find('.titles').append(current_th);
                        
//                        if (hidden == true)
                        if (hidden === true)
                        {
                            current_th.addClass('_hidden');
                        }
                        else
                        {
                            current_th.removeClass('_hidden');
                        }
                        
                        temp_array.push(column);
                    });
                    
                    //prolazimo kroz sve kolone i sakrivamo one koje se ne nalaze u kolonama koje su sacuvane u bazi
                    //to je slucaj da je dodata neka kolona u funkciji columnOrder u modelu a ne nalazi se u sacuvanim, onda mora da je sakrijemo
                    _table.data('_header').find('th').each(function(){
                        var column = $(this).attr('column');
                        if (typeof column !== 'undefined')
                        {
//                            if ($.inArray(column, temp_array) == -1)
                            if ($.inArray(column, temp_array) === -1)
                            {
                                $(this).addClass('_hidden');
                            }
                        }
                    });
                }
            }
        },
        add: function(key){
            if (typeof key === 'undefined')
            {
                key = 'button1';
            }
            var _table = this;
            var add_button = null;
            if (typeof this.data('add_button')!=='object' || typeof this.data('add_button')['button1'] === 'undefined')
            {                
                add_button = this.data('add_button');
            }
            else
            {                
                add_button = this.data('add_button')[key];
            }
            if (typeof(add_button)==='string')
            {
                eval(add_button);
            }
            else if (typeof add_button === 'function')
            {                
                sima.executeFunction(add_button);
            }
            else if (typeof(add_button)==='object' || (typeof(add_button)==='boolean' && add_button))
            {     
                if (typeof(add_button)==='boolean') add_button = {};
                
                var inline = (typeof(add_button.inline)!=='undefined')?add_button.inline:false;
                //ako je dodavanje inline reda
                if (inline)
                {
                    var init_data = {};
                    if (typeof add_button['init_data'][_table.attr('model')] !== 'undefined')
                        init_data = add_button['init_data'][_table.attr('model')];
                    addInlineRow(_table, init_data);
                }
                else
                {
                    add_button.onSave = function(response){
                                            _table.simaGuiTable('append_row', response.model_id);
                                        };
                    sima.model.form(_table.attr('model'), {}, add_button);
                }
            }
        },
        setAddButton: function(add_button)
        {
            if (typeof(add_button) !== 'undefined')
            {
                var new_buttons = this.data('add_button');
                if (typeof(new_buttons) === 'undefined')
                    new_buttons = {};
                if (typeof(add_button) === 'object' && typeof(add_button['button1']) !== 'undefined')
                {
                    for(var _i in add_button)
                    {
                        new_buttons[_i] = add_button[_i];
                        var button_span = this.find('.sima-gui-button.add_button.'+_i+':not(._no_access)');
                        if (new_buttons[_i]!==false && new_buttons[_i]!=='false' && new_buttons[_i]!=='')
                        {
                            button_span.removeClass('_disabled');
                        }
                        else
                        {
                            button_span.addClass('_disabled');
                        }
                    }
                }
                else
                {
                    new_buttons['button1'] = add_button;
                    var button_span = this.find('.sima-gui-button.add_button.button1:not(._no_access)');
                    if (add_button!==false && add_button!=='false' && add_button!=='')
                    {
                        button_span.removeClass('_disabled');
                    }
                    else
                    {
                        button_span.addClass('_disabled');
                    }
                }
                this.data('add_button',new_buttons);
            }
            else
                this.find('.sima-gui-button.add_button').addClass('_disabled');
        },
        deleteFilter: function(column)
        {
            var _table = this;
            _table.data('_header').find("th[column='"+column+"'] input").each(function() {
                $(this).val('');
            });
            _table.data('_header').find("th[column='"+column+"'] select").each(function() {
                $(this).val('');
            });
            _table.simaGuiTable('populate');
        },
        hideColumns: function(columns)
        {
            var _table = this;
            $.each(columns, function(key, value) {
                var column = value;
                var current_th = _table.data('_header').find("[column='"+column+"']");                
                current_th.addClass('_hidden');
            });
        },
        showColumns: function(columns)
        {
            var _table = this;
            $.each(columns, function(key, value) {
                var column = value;
                var current_th = _table.data('_header').find("[column='"+column+"']");                
                current_th.removeClass('_hidden');
            });
        },
        selectRow: function(model_id)
        {
            var _table = this;
            var row = _table.data('_body').find("tr[model_id='"+model_id+"']");
            if (typeof row !== 'undefined' && row.length > 0)
            {
                click_row(_table, row);
            }
        }
    };

    /**
     * proverava da li je postavljen neki select filter
     * @param {obj} _table
     * @param {string} column
     * @returns boolean
     */
    function isSetFilter(_table, column)
    {             
        var filter = _table.data('_header').find('[column="'+column+'"]');
        var is_set = false;
        if (filter.size()>0)
        {           
            if (filter.find('input:first').size() > 0)
            {
                if (filter.find('input:first').val() !== '')
                {
                    is_set = true;
                }
            }
            else if (filter.find('select:first').size() > 0)
            {
                if (filter.find('select:first').val() !== '')
                {
                    is_set = true;
                }
            }            
        }
        
        return is_set;
    }
    
    /**
     * funkcija postalja select i fixed filter u guitable-u
     * @param {type} _table
     * @param {type} obj
     * @param {type} prefix
     * @param {type} isFixedFilter
     * @returns {undefined}
     */
    function setSelectFixedFilter(_table, obj, prefix, isFixedFilter)
    {        
        var header = _table.data('_header');
        for (var i in obj)
        {            
            if (typeof obj[i] !== 'object')
            {                
                var search_string = '';
                var column = '';
                if (i==='ids' && prefix !== '')
                {
                    column = prefix;
                    
                }
                else
                {
                    column = (prefix==='')?i:(prefix+'.'+i);
                }
                search_string = '[column="'+column+'"]';
//                console.log('trazi za '+search_string);
                
                //postavljamo filtere unutar tabele
                if (header.find(search_string).size() > 0)
                {
                    var filter_in = header.find(search_string);
                    //ako je postavljen filter nad nekom kolonom onda je prikaz te kolone obavezan                    
                    filter_in.removeClass('_hidden');
                    var column_filter_type = filter_in.data('column_filter_type');
                    if (column_filter_type === 'numeric')
                    {
                        filter_in.find('.sima-ui-numeric-filter:first').simaNumericFilter('setValue', obj[i]);
                    }
                    else if (column_filter_type === 'relation')
                    {
                        setSearchFieldValue(filter_in, obj[i]);
                    }
                    else
                    {
                        filter_in.find('input:first').val(obj[i]);
                        filter_in.find('select:first').val(obj[i]);
                    }
                    
                    //ako je fiksni filter onda disable-ujemo filter
                    if (isFixedFilter)
                        disableTableFilter(filter_in);
                }
                //postavljamo filtere van tabele
                else if (_table.find('.sima-guitable-options .sima-gui-table-filters').find(search_string).size() > 0)
                {
                    var filter_out = _table.find('.sima-guitable-options .sima-gui-table-filters').find(search_string);
                    filter_out.find('input:first').val(obj[i]);
                    filter_out.find('select:first').val(obj[i]);
                    setSearchFieldValue(filter_out, obj[i]);
                    if (isFixedFilter)
                        disableTableFilter(filter_out);                        
                }
                //postavljamo statuse u opcijama
                else if (header.find('.'+column+'._status-filter').size() > 0)
                {
                    var filter_status = header.find('.'+column+'._status-filter');
                    filter_status.attr('value',obj[i]);                    
                    if (obj[i] === true || obj[i] === 'true')
                    {                        
                        filter_status.removeClass('_status-initial').removeClass('_intermediate').removeClass('_not-ok').addClass('_ok');
                    }
                    else if (obj[i] === false || obj[i] === 'false')
                    {                        
                        filter_status.removeClass('_status-initial').removeClass('_intermediate').removeClass('_ok').addClass('_not-ok');
                    }
                    else if (obj[i] === 'null' && filter_status.data('allow_null') === true)
                    {
                        filter_status.removeClass('_status-initial').removeClass('_ok').removeClass('_not-ok').addClass('_intermediate');
                    }
                    else
                    {
                        filter_status.removeClass('_ok').removeClass('_not-ok').removeClass('_intermediate').addClass('_status-initial');
                    }
                    
                    if (isFixedFilter)
                        disableTableFilter(filter_status);
                }
                //ako nije fiksni filter onda izbacujemo gresku da mora da je vidljiva kolona u guitable-u za koju je zadat select_filter
                else if (!isFixedFilter 
                        && column.indexOf('filter_scopes') !== 0 
                        && column.indexOf('display_scopes') !== 0  
                        && column.indexOf('scopes') !== 0
                )
                {                    
                    sima.dialog.openError(sima.uniqid(), 'Morate zadati kolonu '+column+' jer je za nju postavljen select_filter.');
                }                
            }
            else
            {                
                var new_obj = obj[i];
                var new_prefix = (prefix==='')?i:(prefix+'.'+i);
                setSelectFixedFilter(_table, new_obj, new_prefix, isFixedFilter);
            }
        }        
    }
    
    function disableTableFilter(obj)
    {
        if (obj.hasClass('_status-filter'))
        {
            obj.addClass('_disabled');
        }
        obj.find('input, select').each(function(){
            $(this).attr('disabled', true);            
        });
        obj.find('div, span').each(function(){            
            $(this).addClass('_disabled');
        });
    }
    
    function setSearchFieldValue(search_field, value)
    {
        if (search_field.children().hasClass('sima-ui-sf'))
        {
            search_field.find('.sima-ui-sf:first').simaSearchField('setDisplayName', value);
        }
    }
    
    function makeResizable(_table, table)
    {
        //get number of columns
        var numberOfColumns = table.find('TR:first TH').size();

        //id is needed to create id's for the draghandles
        var tableId = table.attr('id');

        //preskacemo options i order number
        for (var i = 3; i <= numberOfColumns; i++)
        {
            //enjoy this nice chain :)
            $('<div class="srt-draghandle" id="' + tableId + '_id' + i + '"></div>').insertBefore(table).data('tableid', tableId).data('myindex', i).draggable(
                    {axis: "x",
                        start: function()
                        {
                            var tableId = ($(this).data('tableid'));
                            $(this).toggleClass("dragged");
                            //set the height of the draghandle to the current height of the table, to get the vertical ruler
                            $(this).css({height: $('#' + tableId).height() + 'px'});
                        },
                        stop: function(event, ui)
                        {
                            var tableId = ($(this).data('tableid'));
                            $(this).toggleClass("dragged");
                            var oldPos = ($(this).data("ui-draggable").originalPosition.left);
                            var newPos = ui.position.left;
                            var index = $(this).data("myindex");
                            setNewColumnWidth(_table, newPos - oldPos, index - 1);
                        }
                    }
            );
        }

        resetSliderPositions(_table);
        return table;
    }

    function setNewColumnWidth(_table, change, columnIndex)
    {
        //poveca table_resize_wrapper za 15000 piksela
        _table.data('_header').find('.table_resize_wrapper').width(_table.data('_header').find('.table_resize_wrapper').width() + 15000);
        //povecava heder za promenu
        _table.data('_header').find('th').eq(columnIndex).innerWidth(_table.data('_header').find('th').eq(columnIndex).innerWidth() + change);
        //NOVO
        //        var _header_column = _table.data('_header').find('th').eq(columnIndex);
//        var _new_width = _header_column.innerWidth() + change;
//        var _min_width = _header_column.data('min_width') || _table.data('column_min_width');
//        var _max_width = _header_column.data('max_width') || _table.data('column_max_width');
//        if ( ((typeof _min_width) !== 'undefined') && (_new_width < _min_width))
//        {
//            _new_width = _min_width;
//        }
//        if (_new_width > _max_width)
//        {
//            _new_width = _max_width;
//        }
//        change = _new_width - _header_column.innerWidth();
//        _header_column.innerWidth(  _new_width  );
//        
//        
//        var _data_index = _table.data('_header').find('th:not(._hidden)').length - 1;// -1 je za options
//        
//var __W = _table.data('_body').find('tr:not(.page_break)').find('td').eq(_data_index).innerWidth();
//          _table.data('_body').find('tr:not(.page_break)').each(function() {
//              $(this).find('td').eq(_data_index).innerWidth(__W + change);
//          });
        //STARO
        
        var __W = _table.data('_body').find('tr:not(.page_break)').find('td').eq(columnIndex).innerWidth();
        _table.data('_body').find('tr:not(.page_break)').each(function() {
            $(this).find('td').eq(columnIndex).innerWidth(__W + change);
        });
               
        //KRAJ STAROG
        //vraca sirinu table_resize_wrapper na staro
        _table.data('_header').find('.table_resize_wrapper').width(_table.find('.table_resize_wrapper').width() + 5);

        adjust_cell_widths(_table, _table.data('_body').find('tr:not(.page_break)').find('td').eq(columnIndex).attr('column'));
    }

    function resetSliderPositions(_table)
    {
        var table = _table.data('_header').find('table');
        var tableId = table.attr('id');
        //put all sliders on the correct position
        table.find(' TR:first TH').each(function(index)
        {
            var td = $(this);
            var newSliderPosition = td.position().left + td.innerWidth();
            $("#" + tableId + "_id" + (index + 1)).css({left: newSliderPosition, height: table.height() + 'px'});
        });
    }

    function saveColumnSettings(_table)
    {
        var settingsJSON = new Array();
        var i = 0;
        _table.data('_header').find('th:not(.order-number):not(.options)').each(function() {
            var row_sett = new Object;
            row_sett.name = $(this).attr('column');
            row_sett.width = $(this).width();
            if ($(this).hasClass('_hidden'))
            {
                row_sett.hidden = true;
            }
            else
            {
                row_sett.hidden = false;
            }
            settingsJSON[i] = row_sett;
            i++;
        });
        
        sima.ajax.get('guitable/saveColumns', {
            async:true,
            data: {
                settings: settingsJSON, 
                table_string: _table.attr('table_string')
            }
        });
        
    }
    
    function click_row(_table, row, target, type, launchAddButton)
    {
        if (typeof type !== 'undefined' && type === 'ctrlKey' && !_table.data('disableMultiSelect'))
        {            
            _table.data('ctrlLastSelected', row.index());
            _table.data('selected_sum_status', true);
            _table.data('multiselect', true);
            disableOptions(_table);
            enableMultiselectOptions(_table);
        }
        else if (typeof type !== 'undefined' && type === 'shiftKey' && !_table.data('disableMultiSelect'))
        {
            var selected = _table.find('tr.selected');
            if (selected.size() >= 1)
            {
                var row_index = row.index();
                var first_index = _table.data('shiftFirstSelected');
//                if (selected.size() == 1)
                if (selected.size() === 1)
                {
                    _table.data('shiftFirstSelected', _table.find('tr.selected:first').index());
                    first_index = _table.data('shiftFirstSelected');
                }
//                else if (_table.data('ctrlLastSelected') != null)
                else if (_table.data('ctrlLastSelected') !== null)
                {
                    first_index = _table.data('ctrlLastSelected');
                    _table.data('shiftFirstSelected', first_index);
                }

                if (row_index >= first_index)
                {
                    unselectAll(_table);
                    for (var i=first_index; i<row_index; i++)
                            _table.find("tbody tr:eq("+i+"):not(.page_break)").addClass('selected');
                }
                else if (row_index <= first_index)
                {
                    unselectAll(_table);
                    for (var i=row_index+1; i<=first_index; i++)
                        _table.find("tbody tr:eq("+i+"):not(.page_break)").addClass('selected');
                }
            }
            _table.data('ctrlLastSelected', null);
            _table.data('selected_sum_status', true);
            _table.data('multiselect', true);
            disableOptions(_table);
            enableMultiselectOptions(_table);
        }
        else
        {
            var wasmultiselect = false;
            if (_table.data('multiselect'))
            {
                populate_sum(_table, '');
                wasmultiselect = true;
            }
            _table.data('selected_sum_status', false);
            var rowHasSelected = row.hasClass('selected');
            _table.data('multiselect', false);
            _table.find('tr.selected').each(function() {
                 $(this).removeClass('selected');
            });
            if (rowHasSelected && !wasmultiselect)
            {
                row.addClass('selected');
            }
            disableMultiselectOptions(_table);
        }

        select_row(_table, row, target, launchAddButton);

        if (_table.data('multiselect', true) && _table.find('tr.selected').size() <= 1)
        {
            _table.data('ctrlLastSelected', null);
            _table.data('selected_sum_status', false);
            _table.data('multiselect', false);
            disableMultiselectOptions(_table);
        }
    }
    
    function select_row(_table, row, target, launchAddButton)
    {
        if (typeof target === 'undefined' || target === null)
        {
            var selected_field = _table.find('td.field_selected');
            if (selected_field.length > 0)
            {
                var filed_selected_index = selected_field.index();
                target = row.find('td').eq(filed_selected_index);
            }
            else
            {
                target = row.find('td:not(.options):first');
            }
        }
        else if (typeof target !== 'undefined' && !target.is('td'))
        {
            target = target.parents('td:first');
        }
        var row_already_selected = false;
        //ako nije multiselect
        if (!_table.data('multiselect'))
        {
            //ako je kliknuti td vec selektovan onda deselektujemo i td i red u kome se on nalazi
            if (target.hasClass('field_selected'))
            {
                row.removeClass('selected');
                target.removeClass('field_selected');
            }
            //ako kliknuti td nije selektovan
            else
            {
                _table.data('_body').find('td').each(function(){
                    $(this).removeClass('field_selected');
                });
                target.addClass('field_selected');
                //ako red u kome se nalazi nije selektovan onda selektujemo i red
                if (!row.hasClass('selected'))
                {
                    row.addClass('selected');
                }
                //ako red u kome se nalazi je selektovan onda red ostaje selektovan
                else
                {
                    row_already_selected = true;
                }
            }
        }
        //ako je multiselect onda skidamo selektovano polje i selektujemo/deselektujemo samo redove
        else
        {
            _table.find('.field_selected').removeClass('field_selected');
            row.toggleClass('selected');
        }
        
        var globalTimeout = _table.data('globalTimeout');
        if(globalTimeout !== null) 
        {
            clearTimeout(globalTimeout);
            _table.data('globalTimeout', globalTimeout);
        } 
        _table.data('globalTimeout', setTimeout(function(){
            var selected_rows = _table.find('tr.selected');
            if (_table.data('multiselect'))
            {            
                for (var i = 0; i < _table.data('multiselect_func').length; i++)
                {
                    sima.executeFunction(_table.data('multiselect_func')[i], [selected_rows]);
                }
                if (_table.data('selected_sum_status'))
                {   
                    populate_selected_sum(_table);
                }
            }
            else
            {
                if (!row.hasClass('selected'))
                {
                    for (var i = 0; i < _table.data('row_unselect_func').length; i++)
                    {
                        sima.executeFunction(_table.data('row_unselect_func')[i], [row, selected_rows]);
                    }
                }
                else if (row.hasClass('selected') && !row_already_selected)
                {
                    for (var i = 0; i < _table.data('row_select_func').length; i++)
                    {
                        sima.executeFunction(_table.data('row_select_func')[i], [row, selected_rows, launchAddButton]);
                    }
                }
            }

            //promena selektovanih redova
            for (var i = 0; i < _table.data('selection_change_func').length; i++)
            {
                sima.executeFunction(_table.data('selection_change_func')[i], [selected_rows]);
            }
        },500));
    }

    function dblclick_row(_table, event, row, call_click_row)
    {
        var td = $(event.target);
        if (!td.is('td'))
        {
            td = td.parents('td:first');
        }
        
        var _call_click_row = (typeof call_click_row !== 'undefined') ? call_click_row : true;
        
        if (!td.hasClass('field_selected'))
        {
            //deselektujemo sve redove i polja, i selektujemo onaj red i polje na koji je izvrsen dupli klik
            _table.data('_body').find('tr').each(function () {
                $(this).removeClass('selected');
                $(this).find('td').each(function () {
                    $(this).removeClass('field_selected');
                });
            });
            row.addClass('selected');
            td.addClass('field_selected');
        }

        //ako je zadata funkcija za dupli klik, onda je pozivamo
        if (_table.data('row_dblclick_func').length > 0)
        {            
            var length = _table.data('row_dblclick_func').length;
            for (i = 0; i < length; i++)
            {
                sima.executeFunction(_table.data('row_dblclick_func')[i], [row]);
            }
        }
        else if (
                    td.is('td') && 
                    _table.data('_header').find('th[column="'+td.attr('column')+'"]') !== 'undefined' && 
                    _table.data('_header').find('th[column="'+td.attr('column')+'"]').attr('edit') == 'true'
                )
        {
            renderFilterForColumn(_table, td);
        }
        else if (row.attr('has_display') === true || row.attr('has_display') === 'true')
        {
            if (!sima.isEmpty(row.attr('display_model')))
            {
                sima.dialog.openActionInDialogAsync('defaultLayout', {
                    get_params:{
                        model_name:row.attr('display_model'),
                        model_id:parseInt(row.attr('model_id'))
                    }
                });
            }
            else
            {
                sima.dialog.openWarn(sima.translate('DoNotHaveAccessToInformations'));
            }
        }
        //ako je zadat path2 onda otvaramo dialog
        else if (!sima.isEmpty(row.attr('path2')))
        {            
            sima.dialog.openModel(row.attr('path2'), row.attr('model_id'), _table.data('fixed_filter'));
        }
        //ako nije zadata funkcija za double klik i nema path2 onda pozivamo funkciju za single klik(ako je zadata)
        else if (_table.data('selectRow') && _call_click_row === true)
        {            
            var type = (event.ctrlKey)?'ctrlKey':((event.shiftKey)?'shiftKey':'');
            click_row(_table, row, td, type);
        }
    }

    function get_columns(_table, all)
    {
        var _all = (typeof all !== 'undefined' && all===true) ? true: false;
        var _columns = [];
        if (typeof _table.data('_header') !== 'undefined')
        {
            var filter = '';            
            if (!_all)
            {
                filter = ':not(._hidden)';
            }
            _table.data('_header').find('th'+filter).not('.order-number').each(function() {
                var column = $(this).attr('column');
                if (typeof (column) === 'string' && column !== '')
                    _columns.push(column);
            });
        }
        return _columns;
    }
    
    function get_not_visible_columns(_table)
    {
        var _columns = [];
        if (typeof _table.data('_header') !== 'undefined')
        {
            _table.data('_header').find('th').each(function() {
                if ($(this).hasClass('_hidden'))
                {
                    var column = $(this).attr('column');
                    if (typeof (column) === 'string' && column !== '')
                        _columns.push(column);
                }
            });
        }
        return _columns;
    }

    function collect_data(_table)
    {                
        var data = new Object(); // podaci koji se salju
        data[_table.attr('model')] = {};
        if (_table.data('search_model_text') !== null)
        {
            mergeArrayAsStringToJsonObjAndSetValue(data[_table.attr('model')], "[text]", _table.data('search_model_text'));            
        }
        else if (_table.data('search_text') !== null)
        {
            mergeArrayAsStringToJsonObjAndSetValue(data[_table.attr('model')], "[text]", _table.data('search_text').val());            
        }

        //filteri van header-u
        _table.find('.sima-guitable-options .sima-gui-table-filters').find('input').each(function() {
//            if (typeof $(this).attr('name') != 'undefined')
            if (typeof $(this).attr('name') !== 'undefined')
            {
//                if ($(this).attr('name').indexOf(_table.attr('model') + '[') != -1 && $(this).val()!=='')
                if ($(this).attr('name').indexOf(_table.attr('model') + '[') !== -1 && $(this).val()!=='')
                {
                    mergeArrayAsStringToJsonObjAndSetValue(data[_table.attr('model')], $(this).attr('name'), $(this).val());                    
                }
            }
        });
        
        //kupimo podatke iz inputa, select-a...
        _table.find('.sima-guitable-options .sima-gui-table-filters').find('select').each(function() {
            if ($(this).val()!=='')
            {
                mergeArrayAsStringToJsonObjAndSetValue(data[_table.attr('model')], $(this).attr('name'), $(this).val());                
            }
        });
        
        if (typeof _table.data('_header') !== 'undefined')
        {
            //filteri u header-u
            _table.data('_header').find('input').each(function() {
                if (typeof $(this).attr('name') !== 'undefined')
                {
                    if ($(this).attr('name').indexOf(_table.attr('model') + '[') !== -1 && $(this).val()!=='')
                    {
                        mergeArrayAsStringToJsonObjAndSetValue(data[_table.attr('model')], $(this).attr('name'), $(this).val());                        
                    }
                }
            });

            _table.data('_header').find('select').each(function() {
                if ($(this).val()!=='')
                {
                    mergeArrayAsStringToJsonObjAndSetValue(data[_table.attr('model')], $(this).attr('name'), $(this).val());
                }
            });

            //proverava da li su postavljeni statusni filteri
            _table.data('_header').find('._status-filter').each(function() {                
//                if (typeof $(this).attr('name') != 'undefined')
                if (typeof $(this).attr('name') !== 'undefined')
                {
//                    if ($(this).attr('name').indexOf(_table.attr('model') + '[') != -1 && $(this).attr('value')!=='')
                    if ($(this).attr('name').indexOf(_table.attr('model') + '[') !== -1 && $(this).attr('value')!=='')
                    {
                        mergeArrayAsStringToJsonObjAndSetValue(data[_table.attr('model')], $(this).attr('name'), $(this).attr('value'));
                    }                
                }
            });
        }
        
        if (typeof _table.data('order_by') !== 'undefined' && _table.data('order_by') !== '')
        {
            mergeArrayAsStringToJsonObjAndSetValue(data[_table.attr('model')], "[order_by]", _table.data('order_by'));            
        }
        var select_filter = _table.data('select_filter');
        if (typeof select_filter !== 'undefined')
        {
            if (typeof select_filter.filter_scopes !== 'undefined')
            {
                mergeArrayAsStringToJsonObjAndSetValue(data[_table.attr('model')], "[filter_scopes]", select_filter.filter_scopes);                
            }
            if (typeof select_filter.display_scopes !== 'undefined')
            {
                mergeArrayAsStringToJsonObjAndSetValue(data[_table.attr('model')], "[display_scopes]", select_filter.display_scopes);                
            }
            if (typeof select_filter.scopes !== 'undefined')
            {
                mergeArrayAsStringToJsonObjAndSetValue(data[_table.attr('model')], "[scopes]", select_filter.scopes);                
            }
        }
       
        data.fixed_filter = _table.data('fixed_filter');        
        data.page = _table.data('curr_page');        
        data.columns = get_columns(_table);
        data.columns_type = _table.attr('columns_type');
        data.Model = _table.attr('model');
        data.without_order_number = _table.data('without_order_number');

        return data;
    }

    function mergeArrayAsStringToJsonObjAndSetValue(obj, array_as_string, final_value)
    {
        var words = array_as_string.match(/[^[\]]+(?=])/g);
        var curr_obj = obj;
        $.each(words, function(index, value) {            
            if(!curr_obj.hasOwnProperty(value))
            {
                curr_obj[value] = {};
            }
            if (index === (words.length-1))
            {
                curr_obj[value] = final_value;
            }
            else
            {
                curr_obj = curr_obj[value];
            }            
        });
        
        return curr_obj;
    }

    /**
     * 
     * @param {type} _table
     * @param {type} ids
     * @returns {undefined}
     */
    function _select_rows(_table, ids){
        var l = ids.length;
        for (var i=0; i<l; i++)
        {
            
            var new_selected = _table.find('tr:not(.sima-guitable-for-delete)[model_id="'+ids[i]+'"]');
            if (new_selected.size()>0)
            {
                new_selected.addClass('selected');
            }
            else
            {
                var old_selected = _table.find('tr.sima-guitable-for-delete[model_id="'+ids[i]+'"]');
                old_selected.removeClass('selected');
                for (var j = 0; j < _table.data('row_unselect_func').length; j++)
                {
                    sima.executeFunction(_table.data('row_unselect_func')[j], [old_selected]);
                }
            }
        }
    }

    function populate(_table, params)
    {
        if (typeof params === 'undefined' || typeof params !== 'object')
        {
            params = {};
        }
        
        if (typeof (params.no_sum) === 'undefined')
            params.no_sum = !_table.data('_calc_sum');

        if (typeof (params.text) !== 'undefined')
            _table.data('search_model_text', params.text);

//        _table.simaGuiTable('unselect_rows');
        
        var _to_unselect = [];//redovi koje treba unselektovati
        _table.find('tr.selected').each(function() {
            _to_unselect.push($(this).attr('model_id'));
        });
        //pamti se selektovano polje
        var selected_field_model = _table.find('td.field_selected').parents('tr:first').attr('model_id');
        var selected_field_index = _table.find('td.field_selected').index();     
        
        var first_tr = null;
        if (_table.find('.tbody tr:first').length > 0)
            first_tr = _table.find('.tbody tr:first');
        var last_tr = null;
        if (_table.find('.tbody tr:last').length > 0)
            last_tr = _table.find('.tbody tr:last');
        if (typeof (params.load_more) === 'undefined' && typeof (params.load_less) === 'undefined')
        {
            _table.find('.tbody').children().each(function(){
                if (!$(this).hasClass('page_break'))
                {
                    $(this).addClass('sima-guitable-for-delete').hide();
                }
            });
            _table.find('.tbody').empty();
            _table.find('.load_more').hide();
            _table.find('.load_less').hide();
        }
        if (sima.isElementInDom(_table))
        {
            _table.find('.empty-table-message').hide();
            _table.data('load_more').hide();
            _table.data('load_less').hide();
            addFakeRow(_table);
            
            //Sasa A. - mora prvo da se pozove adjust_cell_widths kako bi se setovala stvarna sirina za table_resize_wrapper, jer inace stoji 15000
            //Nakon toga vrsimo proveru da li je sirina kontenta tabele manja od sirine tabele i ako jeste setujemo novu sirinu loading divu(jer je po defaultu sirina tabele)
            //To je slucaj kada je sirina kontenta tabele manja od sirine tabele i onda loading ide u prazno, ne prekriva samo kontent tabele
            adjust_cell_widths(_table);
            var guitable_body_width = _table.find('.sima-guitable-body').width();
            var guitable_body_table_width = _table.find('.sima-guitable-body table:first').width();
            if (guitable_body_table_width < guitable_body_width)
            {
                _table.find('.sima-guitable-loading').width(guitable_body_table_width);
            }

            sima.ajaxLong.start("guitable/ATModel", {
                data: collect_data(_table),
                group: _table.data('populate_group'),
                onCancel: function() {
                    _table.find('.tbody > *:not(.sima-guitable-fake-row)').remove();
                    _table.data('loading_obj').go(0);
                },
                onUpdate: function(response) {
                    $('body').prepend(response.process_output_before);
                    
                    $.each(response.rows, function(key, value) {
                        var row = value;
                        if (typeof (params.load_less) !== 'undefined')
                        {
                            if (first_tr !== null)
                            {
                                $(row).insertBefore(first_tr).fadeIn(1000);
                            }
                        }
                        else
                        {
                            $(row).insertBefore(_table.find('.sima-guitable-fake-row')).fadeIn(1000);
                        }                    
                    });
                    _table.data('loading_obj').go(response.percent);
                    
                    $('body').append(response.process_output_after);
                },
                onEnd: function(response) {
                    _table.data('curr_page', response.curr_page);
                    _table.simaGuiTable('setMaxPage', response.max_page);
                    
                    var _tbody = _table.find('.tbody');
                    
                    removeFakeRow(_table);
                    
                    var page_row = $("<tr class='page_break sima-guitable-row' page="+response.curr_page+"><td colspan='"+response.columns_number+"'></td></tr>");
                    if (typeof (params.load_more) !== 'undefined')
                    {                    
                        if (last_tr !== null)                    
                            page_row.insertAfter(last_tr);
                        var top_scroll = _table.data('_body').scrollTop();
                        var body_height = _table.data('_body').height();
                        var scroll_position = top_scroll + body_height / 2 + body_height / 4;
                        _table.data('_body').animate({scrollTop: scroll_position}, 'slow');
                        _table.find('.tbody .page_break:last').nextUntil('tr:last').effect("highlight", {}, 5000);
                    }
                    else if (typeof (params.load_less) !== 'undefined')
                    {
                        var prevFirst = _table.find('.tbody .page_break:first');                    
                        _table.find('.tbody tr:first').before(page_row);
                        _table.find('.tbody .page_break:first').nextUntil(prevFirst).effect("highlight", {}, 5000);
                    }
                    else
                    {
                        _tbody.prepend(page_row);
                        if (_to_unselect.length>0) 
                        {
                            _select_rows(_table,_to_unselect);
                        }
                        _tbody.children('.sima-guitable-for-delete').remove();
                        //selektujemo polje koje bilo selektovano pre populate-a
                        if (typeof selected_field_model !== 'undefined' && typeof selected_field_index !== 'undefined')
                        {
                            _tbody.find('tr[model_id='+selected_field_model+'] td:eq('+selected_field_index+')').addClass('field_selected');
                        }
                        //ako je zadato default polje za select, selektujemo ga
                        if (typeof params.default_selected_row_id !== 'undefined' && params.default_selected_row_id !== null)
                        {
                            select_row(_table, _tbody.find('tr[model_id='+params.default_selected_row_id+']'));                                                        
                        }
                    }
                    
                    if (_table.find('.empty-table-message').length === 0)
                    {
                         _table.find('.sima-guitable-body .table_resize_wrapper').append($('<div class="empty-table-message">'+sima.translate('GuiTableNoData')+'</div>'));
                    }
                    
                    checkLoadMoreMessage(_table);
                    checkLoadLessMessage(_table);
                    
                    setColumnsOrderNumber(_table);
                    
                    checkEmptyTableMessage(_table);
                    
                    setNumberFieldClass(_table);
                    
                    //adjust_cell_widths se zove bez obzira sto se zove i u funkciji za sumu(ako je suma u pitanju), jer je moguce da racunanje sume potraje pa da se ne ceka
                    adjust_cell_widths(_table);

                    if (!params.no_sum)
                    {
                        if (_table.data('multiselect'))
                            populate_selected_sum(_table);
                        else
                            populate_sum(_table);
                    }
                    
                    if (typeof _table.data('on_populate_func') !== 'undefined')
                    {
                        sima.executeFunction(_table.data('on_populate_func'), _table.attr('id'));
                    }
                    
                    _table.trigger('onPopulate', [_table.attr('id')]);
                }
            });
        }
    }

    function populate_sum(_table)
    {
        if (!_table.data('_calc_sum'))
            return;
        
        var data = '';
//        var data = (filter!=null)?'&'+filter.serialize():'';
        _table.data('_header').find('input').each(function() {
            data += '&' + $(this).attr('name') + '=' + $(this).val();
        });
        _table.data('_header').find('select').each(function() {
            data += '&' + $(this).attr('name') + '=' + $(this).val();
        });

        data += (_table.data('_header').find('input.text').size() > 0)
                ? '&' + _table.attr('model') + '[text]=' + _table.data('_header').find('input.text').val() : '';
//        data+=js_data;

        sima.ajax.get('guitable/ATModelSum', {
            get_params: {
                Model: _table.attr('model'),
                columns_type: _table.attr('columns_type')
            },
            data: collect_data(_table),
            async: true,
            success_function: function(response) {
                _table.find('tfoot').html(response.html);
                adjust_cell_widths(_table);
                setNumberFieldClass(_table);
            }
        });
    }

    function populate_selected_sum(_table)
    {
        if (!_table.data('_calc_sum'))
            return;
        
        var local_data = jQuery.extend(true, {}, collect_data(_table));//deep copy
        
        var ids = [];
        _table.find('.row.selected').each(function()
        {
            ids.push($(this).attr('model_id'));
        });
        local_data[_table.attr('model')].ids = ids;

        sima.ajax.get('guitable/ATModelSum', {
            get_params: {
                Model: _table.attr('model'),
                columns_type: _table.attr('columns_type')
            },
            data: local_data,
            async: true,
            success_function: function(response) {
                _table.find('tfoot').html(response.html);
                adjust_cell_widths(_table);
                setNumberFieldClass(_table);
            }
        });
    }

    //svim td-ovima dodajemo klasu za broj, ako je u th-u setovan atribut da je u pitanju broj
    function setNumberFieldClass(_table)
    {        
        //mora rucno da se broji zbog skrivenih polja
        var _index = 0;        
        if (sima.isElementInDom(_table))
        {
            _table.data('_header').find('th:not(._hidden)').each(function(){
                var _th = $(this);
                if (typeof _th.attr('number_field') !== 'undefined' && _th.attr('number_field') === 'true')
                {
                    _table.find('td').each(function(){
                        if ($(this).index() === _index)
                        {
                            $(this).addClass('number_field');
                        }
                    });
                }
                _index++;
            });
        }
    }

    function adjust_cell_widths(_table, column, auto_height)
    {      
        auto_height = auto_height || false;
        if (typeof (column) === 'undefined' || column === '')
        {
            column = '';
        }
        
        _table.data('_header').find('.table_resize_wrapper').width(_table.find('.table_resize_wrapper').width() + 15000);
        
        var width_cumulativ = 0;
        
        _table.data('_header').find('th').each(function()
        {
            var head_td = _table.data('_header').find('th.' + $(this).attr('class'));
            var body_td = _table.data('_body').find('tr td.' + $(this).attr('class'));
            var foot_td = _table.data('_footer').find('td.' + $(this).attr('class'));

            if ($(this).hasClass('options'))
            {
                var width = head_td.width();
                var newWidth = body_td.width();
                _table.data('_body').find('tr').each(function(){
                    var icons_number = $(this).find('td.options > span').length;
                    var icon_width = $(this).find('td.options > span:first').width();
                    if ((icons_number*icon_width+12) >= newWidth)
                    {
                        newWidth = icons_number*icon_width+12;//10 je ukupni padding
                    }
                });
                if (newWidth <= width)
                {
                    newWidth = width;
                }
                head_td.width(newWidth);
                body_td.width(newWidth);
                if (foot_td.size() > 0)
                    foot_td.width(newWidth);

                width_cumulativ += newWidth + 12;
            }
            else
            {
                var width_head = head_td.width();

                body_td.width(width_head);
                if (foot_td.size() > 0)
                    foot_td.width(width_head);

                var width_body = body_td.width();
                var width_foot = 0;
                if (foot_td.size() > 0)
                    width_foot = foot_td.width();
                var newWidth = 0;
    //            if (head_td.hasClass('partner_id')) alert('newwidth: '+newWidth+'  width: '+width+'  width2: '+width2+'  width3: '+width3);
                if (newWidth < width_head)
                    newWidth = width_head;
                if (newWidth < width_body)
                    newWidth = width_body;
                if (newWidth < width_foot)
                    newWidth = width_foot;
                
                //ako je sirina td veci od max sirine th, onda se th-u ukida max sirina
                if (typeof head_td.data('max_width') !== 'undefined' && newWidth > head_td.data('max_width'))
                {
                    head_td.removeAttr('data-max_width');
                    head_td.css({'max-width': ''});
                }
                
    //            if (head_td.hasClass('partner_id')) alert('newwidth: '+newWidth+'  width: '+width+'  width2: '+width2+'  width3: '+width3);

                if (newWidth > width_head + 2)
                {
                    head_td.width(newWidth);
                    body_td.width(newWidth);
                    if (foot_td.size() > 0)
                        foot_td.width(newWidth);
                }
                width_cumulativ += newWidth + 10;
            }                
        });
        
//        _table.find('.table_resize_wrapper').css('width', (_table.data('_header').find('table').width() + 5) + 'px');
        _table.find('.table_resize_wrapper').css('width', (width_cumulativ + 5) + 'px');
        resetSliderPositions(_table);
        saveColumnSettings(_table);
        if (auto_height || _table.data('_calc_sum'))
        {
            _table.trigger('sima-layout-allign',_table.height(),_table.width(),'TRIGGER_guitable_adjust_cell_widths');
        }
    }

    function group_by(event)
    {
        var _table = event.data.table;
        if ($(this).hasClass('clicked'))
        {
            $(this).removeClass('clicked');
        }
        else
        {
            $(this).addClass('clicked');
        }
        _table.data('group_by', '');
        var group_by = _table.data('group_by');
        _table.data('_header').find('span.group_by').each(function()
        {
            if ($(this).hasClass('clicked'))
                group_by += $(this).parent().attr('column') + ',';
        });
        populate(_table);
    }

    function order_by(event)
    {
        var _table = event.data.table;
        if ($(this).hasClass('clicked'))
        {
            if ($(this).hasClass('asc'))
            {
                $(this).removeClass('asc');
                $(this).addClass('desc');
            }
            else
            {
                $(this).removeClass('desc');
                $(this).addClass('asc');
            }
        }
        else
        {
            _table.data('_header').find('span.order_by').removeClass('clicked');
            $(this).addClass('clicked asc');
        }
        _table.data('order_by', $(this).attr('value') + (($(this).hasClass('asc')) ? ' ASC' : ' DESC'));        
        populate(_table);
    }

    function first_link(event)
    {
        var _table = event.data.table;
        _table.data('curr_page', 1);
        populate(_table, {no_sum: true});
    }

    function next_link(event)
    {
        var _table = event.data.table;
        var curr_page = parseInt(_table.data('curr_page'));
        var max_page = parseInt(_table.data('max_page'));
        if (curr_page < max_page)
        {
            curr_page++;
            _table.data('curr_page', curr_page);
        }
        populate(_table, {no_sum: true});
    }

    function prev_link(event)
    {
        var _table = event.data.table;
        var curr_page = parseInt(_table.data('curr_page'));
        if (curr_page > 1)
        {
            curr_page--;
            _table.data('curr_page', curr_page);
        }
        populate(_table, {no_sum: true});
    }

    function last_link(event)
    {
        var _table = event.data.table;
        var max_page = parseInt(_table.data('max_page'));
        _table.data('curr_page', max_page);
        populate(_table, {no_sum: true});
    }

    function load_more(event)
    {
        var _table = event.data.table;
        var curr_page = parseInt(_table.data('curr_page'));
        var max_page = parseInt(_table.data('max_page'));

        if (curr_page < max_page)
        {
            curr_page++;
            _table.data('curr_page', curr_page);
        }
        populate(_table, {no_sum: true, load_more: true});
        event.preventDefault();
    }

    function load_less(event)
    {
        var _table = event.data.table;
        var curr_page = parseInt(_table.data('curr_page'));
        var max_page = parseInt(_table.data('max_page'));

        if (curr_page > 1)
        {
            curr_page--;
            _table.data('curr_page', curr_page);
        }
        populate(_table, {no_sum: true, load_less: true});
        event.preventDefault();
    }

    function goto_page(event)
    {
        var _table = event.data.table;
//        if (event.keyCode == 13)
        if (event.keyCode === 13)
        {
            if (!isNaN(_table.data('goto_page').val()))
            {
                var curr_page = parseInt(_table.data('goto_page').val());
                var max_page = parseInt(_table.data('max_page'));
                
                if (curr_page <= max_page)
                {
                    _table.data('curr_page', curr_page);
                    populate(_table, {no_sum: true});
                }
                else
                {
                    sima.dialog.openInfo('Ne postoji trazena strana.');
                }
            }
            else
            {
                sima.dialog.openInfo('Stranica ne moze biti string.');
            }
        }
    }

    function body_scroll(event)
    {
        var _table = event.data.table;
        _table.find('.loading_circle').css('left', 0);
        _table.find('.loading_circle').css('top', _table.data('_header').height() + _table.find('.sima-guitable-options').height());
        if (_table.data('_body').scrollLeft() > 0) {
            var left_scroll = _table.data('_body').scrollLeft();
            _table.data('load_more').stop().animate({
                left: left_scroll
            });
            _table.data('load_less').stop().animate({
                left: left_scroll
            });
        } else {
            _table.data('load_more').stop().animate({
                left: 0
            });
            _table.data('load_less').stop().animate({
                left: 0
            });
        }

        if (_table.data('_body').scrollTop() > 0) {
            var x = $(window).width()/2;
            var y = _table.data('_body').offset().top - _table.data('_body').height() / 2;
            var closest_with_row = _table.find('.tbody .page_break').next().nearest({x: x, y: y});
            _table.data('curr_page', closest_with_row.prev().attr('page'));
            _table.data('goto_page').val(closest_with_row.prev().attr('page'));
        }
        
        //poziva refresh za sva belongs polja
        _table.find('.sima-ui-belongs').each(function(){
                $(this).simaBelongs('refresh');
        });
        
        $('.sima-popup').hide();
    }

    function file_tag_filter_recursive(event)
    {
        var _table = event.data.table;
        _table.toggleClass('selected-red');
        if (_table.hasClass('selected-red'))
            _table.siblings("input").val('true');
        else
            _table.siblings("input").val('false');
    }
    
    //podesava se sirina unutar th u zavisnosti da li ima order by ili group by
    function setThIcons(_table)
    {
        _table.data('_header').find('th').not('.order-number').not('.options').each(function(){
            var th = $(this);
            var width_minus = 0;
            if (th.find('.order_by').length > 0)
            {
                width_minus += 20;
            }
            if (th.find('.group_by').length > 0)
            {
                width_minus += 20;
            }
            if (th.find('._status-filter').length > 0)
            {
                width_minus += 20*parseInt(th.find('._status-filter').length);
            }
            if (width_minus > 0)
            {
                th.children().not('.order_by, .group_by, ._status-filter').each(function(){
                    $(this).css({width:"-moz-calc(100% - "+width_minus+"px)", width:"-webkit-calc(100% - "+width_minus+"px)", width:"calc(100% - "+width_minus+"px)"});
                });
            }
        });
    }
    
    function setThMaxMinWidth(_table)
    {
        _table.data('_header').find('th').each(function(){
            var th = $(this);            
            if (typeof th.data('min_width') !== 'undefined')
            {
                th.css({'min-width':th.data('min_width')});
            }
            if (typeof th.data('max_width') !== 'undefined')
            {
                th.css({'max-width':th.data('max_width')});
            }
        });
    }
    
    function disableOptions(_table)
    {
        _table.data('_body').find('td.options > span').each(function(){
            $(this).addClass('_disabled');
        });
        
        /// disejbluj svako dugme u opcijama (trebalo bi da je samo jedno)
        _table.data('_body').find('td.options > .sima-old-btn').simaButton('disable');
    }
    
    function enableOptions(_table)
    {
        _table.data('_body').find('td.options > span').each(function(){
            $(this).removeClass('_disabled');
        });
        
        /// enejlbuj svako dugme u opcijama (trebalo bi da je samo jedno)
        _table.data('_body').find('td.options > .sima-old-btn').simaButton('enable');
    }
    
    function enableMultiselectOptions(_table)
    {
        _table.data('_header').find('th.options .options_name').hide();
        _table.data('_header').find('th.options .multiselect_icons').show();
    }
    
    function disableMultiselectOptions(_table)
    {
        _table.data('_header').find('th.options .options_name').show();
        _table.data('_header').find('th.options .multiselect_icons').hide();
        enableOptions(_table);
    }
    
    function unselectAll(_table)
    {
        _table.data('_body').find('tr').each(function(){
            $(this).removeClass('selected');
        });
    }
    
    /**
     * funkcija koja je hendler kada se klikne na ikonice u header-u u koloni "Opcije"
     * @param {type} event
     * @returns {undefined}
     */
    function statuses(event)
    {
        var _table = event.data.table;
        var localThis = $(this);
        if (!localThis.hasClass('_disabled'))
        {
            if (localThis.hasClass('_status-initial'))
            {
                localThis.removeClass('_status-initial').addClass('_ok');
                localThis.attr('value', true);
            }
            else if (localThis.hasClass('_ok'))
            {
                localThis.removeClass('_ok').addClass('_not-ok');
                localThis.attr('value', false);
            }
            else if (localThis.hasClass('_not-ok'))
            {
                if (localThis.data('allow_null') === true)
                {
                    localThis.removeClass('_not-ok').addClass('_intermediate');
                    localThis.attr('value', "null");
                }
                else
                {
                    localThis.removeClass('_not-ok').addClass('_status-initial');
                    localThis.attr('value', '');
                }
            }
            else if (localThis.hasClass('_intermediate'))
            {
                localThis.removeClass('_intermediate').addClass('_status-initial');
                localThis.attr('value', '');
            }

            if (_table.data('globalTimeout') !== null)
                clearTimeout(_table.data('globalTimeout'));
            _table.data('globalTimeout', setTimeout(function()
            {
                populate(_table);
            }, 500));
        }
    }
    
    function renderFilterForColumn(_table, td)
    {
        var th = _table.data('_header').find('th[column="'+td.attr('column')+'"]');
        if (typeof th.attr('edit') !== 'undefined' && th.attr('edit') == 'true')
        {
            var old_value = $.trim(td.text());
            if (typeof old_value !== 'undefined' && typeof th.attr('column') !== 'undefined')
            {
                var fixed_filter = _table.data('fixed_filter');
                sima.ajax.get('guitable/renderFilterForColumn', {
                    get_params: {
                        model_name: _table.attr('model'),
                        model_id: td.parents('tr:first').attr('model_id'),
                        column: th.attr('column'),
                        table_id: _table.attr('id'),
                        columns_type:_table.attr('columns_type')
                    },
                    data: {fixed_filter:fixed_filter},
                    async: true,
                    loadingCircle: td,
                    success_function: function(response) {
                        td.html(response.html);
                        enableColumnEdit(_table);
                    }
                });
            }
        }
        else
        {
            sima.dialog.openInfo('Nije dozvoljena izmena ovog polja.');
        }
    }
    
    function enableColumnEdit(_table)
    {
        var td = _table.find('td.field_selected');
        _table.data('column_edit', true);
         var field_params = getFieldEditParams(td);
        _table.data('column_edit_old_value', field_params['value']);
        setFieldFocus(td);
        _table.find('td:not(".field_selected")').each(function(){
            $(this).css({opacity:0.3});
        });
        //ako td sadrzi search field ekstenziju onda pozivamo klik kako bi se otvorio deo za pretragu
        if(td.find('input.sima-ui-sf-display').length > 0)
        {
            td.find('input.sima-ui-sf-display').trigger("click");
        }
        td.bind("clickoutside", function(event){
            if ($(event.target).parents('.sima-guitable[id='+_table.attr('id')+']').length > 0)
            {
                checkColumnSave(_table, td);
            }
        });
        td.bind("keydown", function(event){
//            if (event.which == 13) //enter
            if (event.which === 13) //enter
            {                
                var th = _table.data('_header').find('th[column="'+td.attr('column')+'"]');        
                if (typeof th.attr('edit') !== 'undefined' && th.attr('edit') == 'true')
                {
                    saveColumnValue(_table, td);
                }
                event.stopPropagation();
                event.preventDefault();
            }
            else if (event.which == 27) //esc
            {
                checkColumnSave(_table, td);
                event.stopPropagation();
                event.preventDefault();
            }
        });
        adjust_cell_widths(_table);
    }
    
    function checkColumnSave(_table, td)
    {
        var old_value = _table.data('column_edit_old_value');
        var field_params = getFieldEditParams(td);
        if (field_params['value'] !== old_value)
        {
            sima.dialog.openYesNo('Vrednost kolone je promenjena. Da li želite da sačuvate?',
                function(){
                    sima.dialog.close();
                    saveColumnValue(_table, td);
                }, 
                function(){
                    sima.dialog.close();
                    disableColumnEdit(_table);
                    _table.simaGuiTable('refresh_row', [td.parents('tr:first').attr('model_id')]);
                },
                null,
                function(){
                    setFieldFocus(td);
                }
            );
        }
        else
        {
            disableColumnEdit(_table);
            _table.simaGuiTable('refresh_row', [td.parents('tr:first').attr('model_id')]);
        }
    }
    
    function disableColumnEdit(_table)
    {
        var td = _table.find('td.field_selected');
        _table.find('td:not(".field_selected")').each(function(){
            $(this).css({opacity:1});
        });
        _table.data('column_edit', null);
        _table.data('column_edit_old_value', null);
        td.unbind("clickoutside");
        td.unbind("keydown");
        window.last_functional_element_clicked = _table;
        window.last_element_clicked = td.parents('tbody:first');
    }
    
    function saveColumnValue(_table, td)
    {
        var model_name = _table.attr('model');
        var model_id = td.parents('tr:first').attr('model_id');
        var column = td.attr('column');
        var field_params = getFieldEditParams(td);
        var data = collect_data(_table);        
        data[field_params['name']] = field_params['value'];        
        if (typeof model_name !== 'undefined' && typeof model_id !== 'undefined')
        {
            sima.ajax.get('guitable/saveColumnValue',{
                get_params:{
                    model_name: model_name,
                    model_id: model_id             
                },
                data: data,
                async: true,
                loadingCircle: td,
                failure_function:function(response)
                {
                    var params = {};
                    params.close_function = function(){
                        setFieldFocus(td);
                    };
                    sima.dialog.openWarn(response.message,'',params);
                },
                success_function:function(response){
                    disableColumnEdit(_table);
                    //Sasa A. - mora da se pozove rucno refresh modela, jer iako se u php-u cuva model i samim tim automatski salje na update,
                    //moze da se desi da model ima update_relations i ako se ovaj konkretan model nalazi unutar nekog drugog modela koji je definisan u 
                    //update_relations, onda se nece izvrsiti update tog modela. Primer gde se javljao taj problem je u magacinu u tabu promet magacina
                    _table.simaGuiTable('refresh_row', [model_id]);
                }
            });
        }
    }
    
    function setFieldFocus(td)
    {
        if (td.find('input:visible').length > 0)
        {
            td.find('input:visible').on('focus', function (e) {
                var _this = $(this);
                window.setTimeout (function(){ 
                   _this.select();
                },100);
            });
            td.find('input:visible').focus();
        }
        else if (td.find('select').length > 0)
        {
            td.find('select').focus();
        }
    }
    
    function getFieldEditParams(td)
    {
        var return_data = [];
        if (td.find('input').length > 0)
        {
            return_data['name'] = td.find('input:first').attr('name');
            return_data['value'] = td.find('input:first').val();            
        }
        else if (td.find('select').length > 0)
        {
            return_data['name'] = td.find('select:first').attr('name');
            return_data['value'] = td.find('select:first').val();            
        }
        
        return return_data;
    }
    
    function onKeyPressed(event)
    {
        var _table = event.data.table;
        var last_functional_element_clicked = $(window.last_functional_element_clicked);
        var last_element_clicked = $(window.last_element_clicked);
        var selected_td = _table.find('tr td.field_selected');
        if (_table.data('column_edit') === null && _table.data('add_inline_row') === null 
                && last_functional_element_clicked.attr('id') === _table.attr('id') 
                && last_element_clicked.parents('#'+_table.attr('id')+'_list_body_table').length > 0)
        {
            switch(event.which) {
                case 114: // F3
                    _table.simaGuiTable('add');
                    event.stopPropagation();
                    event.preventDefault();
                    break;
                case 113: case 13: //enter || F2
                    if (selected_td.length > 0)
                    {
                        if (_table.data('_header').find('th[column="'+selected_td.attr('column')+'"]').attr('edit') == 'true')
                        {
                            renderFilterForColumn(_table, selected_td);
                        }
                        else
                        {
                            dblclick_row(_table, event, selected_td.parents('.sima-guitable-row:first'), false);
                        }
                        event.stopPropagation();
                        event.preventDefault();
                    }
                break;
                case 37: // left
                    if (selected_td.length > 0)
                    {
                        var left = selected_td.prev();
                        if (!sima.isEmpty(left))
                        {
                            var change_selected = true;
                            if (left.hasClass('options'))
                            {
                                left = left.prev();
                                if (sima.isEmpty(left))
                                {
                                    change_selected = false;
                                }
                            }
                            
                            if (change_selected === true)
                            {
                                selected_td.removeClass('field_selected');
                                left.addClass('field_selected');
                            }
                        }
                        event.stopPropagation();
                        event.preventDefault();
                    }
                break;
                case 38: // up
                    if (selected_td.length > 0)
                    {
                        var index = selected_td.index();
                        var up = selected_td.parents('tr:first').prev().find('td').eq(index);
                        if (!sima.isEmpty(up))
                        {
                            var type = '';
                            if (event.shiftKey)
                            {
                                type = 'shiftKey';
                            }
                            click_row(_table, up.parents('tr:first'), up, type); 
                        }
                        event.stopPropagation();
                        event.preventDefault();
                    }
                break;
                case 39: // right
                    if (selected_td.length > 0)
                    {
                        var right = selected_td.next();
                        if (!sima.isEmpty(right))
                        {
                            if (right.hasClass('options'))
                            {
                                right = right.next();
                            }
                            selected_td.removeClass('field_selected');
                            right.addClass('field_selected'); 
                        }
                        event.stopPropagation();
                        event.preventDefault();
                    }
                break;
                case 40: // down
                    if (selected_td.length > 0)
                    {
                        var index = selected_td.index();
                        var down = selected_td.parents('tr:first').next().find('td').eq(index);
                        if (!sima.isEmpty(down))
                        {
                            var type = '';
                            if (event.shiftKey)
                            {
                                type = 'shiftKey';
                            }
                            click_row(_table, down.parents('tr:first'), down, type);
                        }
                        event.stopPropagation();
                        event.preventDefault(); 
                    }
                break;

                default: return;
            }
        }
    }       
    
    function addInlineRow(_table, init_data)
    {
        if (_table.find('tr.inline_row').length === 0)
        {
            var model = _table.attr('model');
            var columns_type = _table.attr('columns_type');
            var columns = get_columns(_table);
            var not_visible_columns = get_not_visible_columns(_table);
            if (typeof model !== 'undefined' && typeof columns_type !== 'undefined' && typeof columns !== 'undefined')
            {
                var fixed_filter = _table.data('fixed_filter');
                sima.ajax.get('guitable/renderInlineRow',{
                    get_params:{
                        model: model,
                        table_id: _table.attr('id'),
                        columns_type: columns_type
                    },
                    async:true,
                    data:{
                        columns: columns, 
                        not_visible_columns: not_visible_columns, 
                        fixed_filter: fixed_filter,
                        without_order_number: _table.data('without_order_number')
                    },
                    success_function:function(response){
                        if (response.row !== '')
                        {                        
                            _table.find('tbody').append(response.row);
                            var new_added_row = _table.find('tbody tr:last');
                            enableInlineRow(_table, new_added_row, init_data);
                        }
                    }
                });
            }
        }
        else
        {
            setRowFocus(_table, _table.find('tr.inline_row'));
        }
    }
    
    function enableInlineRow(_table, row, init_data)
    {        
        _table.data('add_inline_row', true);        
        setRowFocus(_table, row);
        _table.find('tr:not(".inline_row")').each(function(){
            $(this).css({opacity:0.3});
        });

        //morao sam da stavim timeout inace odmah ulazi u disableInlineRow, ne znam zasto se to desava
        setTimeout(function(){
            row.bind("clickoutside", function(event){                
                if (
                        $(event.target).parents('.sima-guitable[id='+_table.attr('id')+']').length > 0 &&
                        !$(event.target).hasClass('add_button')
                    )
                {
                    disableInlineRow(_table, row, init_data);
                }
            });
            row.bind("keydown", function(event){                
//                if (event.which == 13)
                if (event.which === 13)
                {                    
                    saveInlineRow(_table, row, init_data, true);
                    event.stopImmediatePropagation();
                    event.preventDefault();
                    return false;
                }
//                else if (event.which == 9)
                else if (event.which === 9)
                {
                    var focused_td = $(':focus').parents('td:first');
                    var founded = false;
                    focused_td.nextAll().each(function(){
                        var input = $(this).find('input:visible');
                        var select = $(this).find('select');
                        if (input.length > 0)
                        {
                            input.focus();
                            if($(this).find('input.sima-ui-sf-display').length > 0)
                            {
                                $(this).find('input.sima-ui-sf-display').trigger("click");
                            }
                            founded = true;
                            return false;
                        }
                        else if(select.length > 0)
                        {
                            select.focus();
                            founded = true;
                            return false;
                        }
                    });
                    if (!founded)
                    {
                        setRowFocus(_table, row);
                    }
                    event.stopPropagation();
                    event.preventDefault();
                }
//                else if (event.which == 27)
                else if (event.which === 27)
                {
                    disableInlineRow(_table, row, init_data);
                    event.stopPropagation();
                    event.preventDefault();
                }
            });
        },10);
        adjust_cell_widths(_table);
        _table.data('_body').scrollTop(_table.data('_body').outerHeight(true)+row.outerHeight(true));
        
        checkEmptyTableMessage(_table);
    }
    
    function setRowFocus(_table, row)
    {
        var th = _table.data('_header').find('th[edit="true"]:first');
        var first_td = row.find('td[column="'+th.attr('column')+'"]');
        if (first_td.find('input:visible').length > 0)
        {
            first_td.find('input:visible').focus();
            if(first_td.find('input.sima-ui-sf-display').length > 0)
            {
                first_td.find('input.sima-ui-sf-display').trigger("click");
            }
        }
        else if (first_td.find('select').length > 0)
        {
            first_td.find('select').focus();
        }
    }
    
    function disableInlineRow(_table, row, init_data)
    {
        var toSave = false;
        row.find('td').each(function(){
            if ($(this).find('input').length > 0)
            {
//                if ($(this).find('input').val() != '')
                if ($(this).find('input').val() !== '')
                {
                    toSave = true;
                }
            }
            else if ($(this).find('select').length > 0)
            {
//                if ($(this).find('select').val() != '')
                if ($(this).find('select').val() !== '')
                {
                    toSave = true;
                }
            }
        });
        if (toSave)
        {
            sima.dialog.openYesNo('Da li želite da sačuvate vrednosti?',
                function(){
                    sima.dialog.close();
                    saveInlineRow(_table, row, init_data);
                }, 
                function(){
                    sima.dialog.close();
                    removeInlineRow(_table, row);
                }
            );
        }
        else
        {
            removeInlineRow(_table, row);
        }
    }
    
    function removeInlineRow(_table, row)
    {        
        row.unbind("clickoutside");
        row.unbind("keydown");
        _table.data('add_inline_row', null);        
        _table.find('tr:not(".inline_row")').each(function(){
            $(this).css({opacity:1});
        });
        row.remove();        
        adjust_cell_widths(_table);
        
        checkEmptyTableMessage(_table);
    }
    
    function saveInlineRow(_table, row, init_data, addInlineRowAgain)
    {
        var _addInlineRowAgain = (typeof addInlineRowAgain !== 'undefined') ? addInlineRowAgain : false;
        var data = {};
        data[_table.attr('model')] = init_data;        
        row.find('.sima-guitable-field-edit').each(function(){
            var _input = $(this).find('input');
            if (_input.length===0)
            _input = $(this).find('select');
            if (_input.length > 0)
            {
                _input.each(function(){
                    if (typeof _input.attr('name')!=='undefined')
                    {
                        data[_input.attr('name')] = _input.val();
                    }
                });
            }
            
        });
        sima.ajax.get('guitable/saveInlineRow',{
            get_params:{
                model: _table.attr('model')
            },
            data:data,
            async:true,
            failure_function:function(response)
            {
                var params = {};
                params.close_function = function(){
                    setRowFocus(_table, row);
                };
                sima.dialog.openWarn(response.message,'',params);
            },
            success_function:function(response){
                removeInlineRow(_table, row);
                if (_addInlineRowAgain)
                {
                    _table.simaGuiTable('add');
                }
                _table.simaGuiTable('append_row', response.model_id, {inline:true,async:true,addInlineRowAgain:_addInlineRowAgain});                
            }
        });
    }
    
    function setColumnsOrderNumber(_this)
    {
        var i = 1;
        _this.find('.sima-guitable-body table tr .order-number').each(function() {
            $(this).html(i);
            i++;
        });
    }
    
    function checkEmptyTableMessage(_this)
    {
        var tbody = _this.find('.tbody');

        if (
                tbody.find('.sima-guitable-row.row').length === 0 && 
                tbody.find('.sima-guitable-fake-row').length === 0
            )
        {
            _this.find('.empty-table-message').show();
        }
        else
        {
            _this.find('.empty-table-message').hide();
        }
    }
    
    function addFakeRow(_table, is_prepend)
    {
        var columns_html = '';

        _table.data('_header').find('th').each(function() {
            columns_html += '<td class="' + $(this).attr('class') + '"><div></div></td>';
        });
        
        if (is_prepend)
        {
            _table.find('.tbody').prepend($("<tr class='sima-guitable-fake-row'>'" + columns_html + "'</tr>"));
        }
        else
        {
            _table.find('.tbody').append($("<tr class='sima-guitable-fake-row'>'" + columns_html + "'</tr>"));
        }
    }

    function removeFakeRow(_table)
    {
        _table.find('.tbody .sima-guitable-fake-row').remove();
    }
    
    function checkLoadMoreMessage(_table)
    {
        var load_more_el = _table.find('.load_more');
        var curr_page = parseInt(_table.data('curr_page'));
        var max_page = parseInt(_table.data('max_page'));

        if (curr_page === max_page || _table.find('.tbody .sima-guitable-row.row').length === 0)
        {
            load_more_el.hide();
        }
        else if (parseInt(_table.find('.tbody .page_break:last').attr('page')) !== max_page)
        {
            load_more_el.show();
        }
    }

    function checkLoadLessMessage(_table)
    {
        var load_less_el = _table.find('.load_less');
        var first_page_break = _table.find('.tbody .page_break:first');
        var curr_page = parseInt(_table.data('curr_page'));
        
        if (curr_page === 1 || _table.find('.tbody .sima-guitable-row.row').length === 0)
        {
            load_less_el.hide();
            first_page_break.hide();
        }
        else if (parseInt(first_page_break.attr('page')) !== 1)
        {
            load_less_el.show();
            first_page_break.show();
        }
    }
    
    $.fn.simaGuiTable = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.simaGuiTable');
            }
        });
        return ret;
    };
    
})(jQuery);

function simaGuiTablePopulateThis(){
    $(this).parents('.sima-guitable').simaGuiTable('populate');
};