
/* global sima */

function SIMAGuiTableGlobObj()
{
    this.repopulateGuiTableForObj = function(obj)
    {        
        var gui_table = obj.closest('.sima-guitable');
                
        if(sima.isEmpty(gui_table))
        {
            return;
        }
        
        gui_table.simaGuiTable('populate');
    };
}
