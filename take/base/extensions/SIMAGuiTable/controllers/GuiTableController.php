<?php

class GuiTableController extends SIMAController
{
    public $guitable_indexes;
    
    public function localRenderPartial($view, $data = [], $return = false, $processOutput = false)
    {
        $view = 'base.extensions.SIMAGuiTable.views.guiTable.' . $view;

        if ($return)
        {
            return parent::renderPartial($view, $data, $return, $processOutput);
        }
        else
        {
//            echo parent::renderPartial($view, $data, $return, $processOutput);
            $errmsg = __METHOD__.' - nije trebalo doci dovde';
            error_log($errmsg);
            Yii::app()->errorReport->createAutoClientBafRequest($errmsg);
        }
    }
  
    public function filters()
    {
        return array(
            'ajaxOnly-atmodel-generateFileToExport'
                ) + parent::filters();
    }

    public function actionIndex($id = null)
    {
        try
        {
            $params = [];
            if(isset($id))
            {
                if(preg_match("/^[a-zA-Z]+_\d{1,}$/", $id) === 1) /// ako je oblika {modul}_{broj}
                {
                    $splitId = explode('_', $id, 2);
                    $moduleName = $splitId[0];
                    $guiTableIndex = $splitId[1];
                    $params = SIMAGuiTableDefinition::getGuiTableDefinition($moduleName, $guiTableIndex);

                }
                else /// inace stari oblik
                {
                   $params = SIMAGuiTableDefinition::getOldGuiTableDefinition($id);
                }
            }
            else
            {
                $params = filter_input_array(INPUT_GET);
            }

            $color = isset($params['color'])?$params['color']:'';
            $title = isset($params['label'])?$params['label']:'New tab';        
            $guitable_params = isset($params['settings'])?$params['settings']:[];

            $html = $this->localRenderPartial('index', array(
                'params' => $guitable_params,            
            ), true, true);

            $this->respondOK([
                'html' => $html,
                'color' => $color,
                'title' => $title,
            ]);
        }
        catch(SIMAGuiTableIndexNotFound $e)
        {
            throw new CHttpException(404, $e->getMessage());
        }
    }

    public static function renderEmbeddedSimple($params)
    {
        $Model = $params['Model'];
        $model_filter = [];
        if (isset($params['filter_params']))
        {
           $model_filter = $params['filter_params'];
        }

        $condition = new SIMADbCriteria([
            'Model' => $Model,
            'model_filter' => $model_filter
        ]);

        $settings = $Model->modelSettings('GuiTable');
        $scopes = (isset($settings['scopes'])) ? $settings['scopes'] : array();
        foreach ($scopes as $scope)
        {
            $Model = $Model->$scope();
        }

        $scopes = $Model->scopes();
        if (isset($scopes['recently']))
        {
            $Model = $Model->recently();
        }
        elseif (isset($scopes['byName']))
        {
            $Model = $Model->byName();
        }

        if (isset($params['modifiers']))
        {
            foreach ($params['modifiers'] as $key => $value)
            {
                $condition->select .= ", '$value' as $key";
            }
        }

        if (!isset($params['rows']))
        {
            $params['rows'] = $Model::model()->findAll($condition);
        }

        if (!isset($params['columns_type']))
        {
            $params['columns_type'] = '';
        }
        $_cols = $Model->guiTableSettings($params['columns_type']);
        $params['columns'] = [];
        foreach ($_cols['columns'] as $col)
        {
            if (is_array($col))
            {
                if (isset($col[0]))
                {
                    $params['columns'][] = $col[0];
                }
            }
            else
            {
                $params['columns'][] = $col;
            }
        }

        if (!isset($params['modifiers']))
        {
            $params['modifiers'] = array();
        }

        if (!isset($params['id']))
        {
            $params['id'] = SIMAHtml::uniqid();
        }

        $params['ModelName'] = (gettype($Model) == 'object') ? get_class($Model) : $Model;

        return Yii::app()->controller->renderPartial('base.views.guiTable.simpleDisplayTable', $params, true, false);
    }

    public static function getRelationFilterParams($model, $array, &$preTableParams, $escape_tex=true)
    {        
        $relations = $model->relations();
        foreach ($array as $key => $value)
        {
            if (!is_int($key) && (!is_array($value) || in_array($key, array('text', 'ids', 'with', 'scopes', 'display_scopes', 'filter_scopes', 'group_by', 'order_by', 'onSave', 'sum'))))
            {                
                $label = $model->getAttributeLabel($key);
                $label = strip_tags($label);
                $label = TemplatedFile::escapeSpecialCharactersInTex($label);
                switch ($key)
                {
                    case 'text':
                        $preTableParams['Pretraženo po tekstu'] = $value;
                        break;
                    case 'ids':                        
                        $model = $model->findByPk($value);
                        $final_value = strip_tags($model->DisplayName);
                        if ($escape_tex === true)
                        {
                            $final_value = TemplatedFile::escapeSpecialCharactersInTex($final_value);
                        }
                        $preTableParams[$label] = $final_value;
                        break;
                    case 'with':
                    case 'scopes':
                    case 'display_scopes':
                    case 'filter_scopes':
                    case 'group_by':
                    case 'order_by':
                    case 'onSave':
                    case 'sum':
                        break;
                    default:
                        //prvo proveravamo da li je kolona strani kljuc. Ako jeste onda dovlacimo vrednost iz relacije
                        $is_foreign_key = false;
                        foreach ($relations as $value1)
                        {
                            if (isset($value1[2]) && $value1[2] == $key)
                            {
                                $rel_model = $value1[1]::model()->findByPk($value);
                                $final_value = strip_tags($rel_model->DisplayName);
                                if ($escape_tex === true)
                                {
                                    $final_value = TemplatedFile::escapeSpecialCharactersInTex($final_value);
                                }
                                $preTableParams[$label] = $final_value;
                                $is_foreign_key = true;
                            }
                        }
                        if ($is_foreign_key == false)
                        {
                            if (strpos($value, 'TAG$') === 0)
                            {
                                $value = FileTagGUI::getDisplayForTag($value);
                            }
                            $final_value = strip_tags($value);
                            if ($escape_tex === true)
                            {
                                $final_value = TemplatedFile::escapeSpecialCharactersInTex($final_value);
                            }
                            $preTableParams[$label] = $final_value;
                        }
                        break;
                }
            }
            else if(isset($relations[$key]))
            {
                $rel_model = $relations[$key][1]::model();
                GuiTableController::getRelationFilterParams($rel_model, $value, $preTableParams, $escape_tex);
            }
        }
    }

    public function actionExportOptions($id, $model, $table_id)
    {
        // render view u html obliku i vrati
         
        if (!isset($id) || !isset($model) || !isset($table_id))
            throw new Exception('actionGetColumns - nisu pravilno zadati parametri');

        $columns = $this->filter_post_input('columns');
        $export_list = $this->filter_post_input('export_list');
        $export_list_array = $export_list;
        $modelObj = $model::model();
        
        $html = $this->localRenderPartial('export_options', array(
            'id' => $id,
            'model' => $modelObj,
            'columns' => $columns,
            'table_id'=> $table_id,
            'export_list' => $export_list_array
        ), true, true);
        $this->respondOK([
            'html' => $html,
            'table_id' => $table_id,
        ]);
    }
    
    public function actionGenerateFileToExport()
    {
        $data = $this->setEventHeader();  
        $this->filter_get_input('event_id');
        if (!isset($data['model']) || !isset($data['columns_type']) || !isset($data['columns']) || !isset($data['format']))
        {
            throw new SIMAException('actionGenerateFileToExport - nisu lepo zadati parametri');
        }
        $model = $data['model'];
        $columns_type = $data['columns_type'];
        $format = $data['format'];
        $columns = $data['columns'];
        $guitable_filter = !empty($data['guitable_filter'])?$data['guitable_filter']:[];
        
        $download_name = '';
        switch ($format)
        {
            case 'csv':
            {
                $download_name = 'Izvoz - '.$model::model()->modelLabel().'.csv';
                $temp_file = $this->tableToCsv($model, $columns_type, $columns, $guitable_filter, function($percent){                                            
                    $this->sendUpdateEvent($percent);
                });
                $temp_file_name = $temp_file->getFileName();
                break;
            }
            case 'xls':
            case 'ods':
            {
                $download_name = 'Izvoz - '.$model::model()->modelLabel().'.'.$format;
                $temp_file = $this->tableToCsv($model, $columns_type, $columns, $guitable_filter, function($percent){                                            
                    $this->sendUpdateEvent($percent);
                });
                
                /**
                 * https://wiki.openoffice.org/wiki/Documentation/DevGuide/Spreadsheets/Filter_Options
                 * 1. 44 - comma separated
                 * 2. 34 - text delimiter "
                 * 3. 76 - utf8 encoding
                 * 4. 1 - number of first line
                 * 5. prazno - Cell Format Codes for the Columns
                 * 6. 3081 - Language identifier (English (en): 0x0C09)
                 */
                $convertedTempFile = $temp_file->createNewTempFileConvertedTo($format, '44,34,76,1,,3081');
                $temp_file_name = $convertedTempFile->getFileName();
                break;
            }
            case 'pdf':
            {
                $download_name = 'Izvoz - '.$model::model()->modelLabel().'.pdf';
                $temp_file_name = $this->tableToPdf($model, $columns_type, $columns, $guitable_filter, function($percent){                                            
                    $this->sendUpdateEvent($percent);
                });
                break;
            }
            default:
            {
                throw new Exception(__METHOD__.' - nepoznat format!');
            }
        }
        
        return $this->sendEndEvent([
            'temp_file_path'=>$temp_file_name,
            'download_name' => $download_name
        ]);
        
    }
    
    public function tableToCsv($model, $columns_type, $columns, $guitable_filter, $callback_func=null)
    {
        $Model = $model::model();        
        $fixed_filter = isset($guitable_filter['fixed_filter'])?$guitable_filter['fixed_filter']:[];
        $select_filter = isset($guitable_filter[$model])?$guitable_filter[$model]:[];
                
        $criteria = new SIMADbCriteria(array(
            'Model' => $model,
            'model_filter' => $fixed_filter
        ));
        $select_criteria = new SIMADbCriteria(array(
            'Model' => $model,
            'model_filter' => $select_filter
        ));
        $criteria->mergeWith($select_criteria);        
        
        $crit_scopes = $criteria->scopes;
        if (is_null($crit_scopes))
        {
            $crit_scopes = [];
        }
        elseif(!is_array($crit_scopes))
        {
            $crit_scopes = [$crit_scopes];
        }
        if ($criteria->group == null)
        {
            //mora ovaj redosled primenjivanja scope'ova jer je bitnije sortiranje od eksplicitno primenjenih scopeova nego od podrazumevanih
            $all_scopes = $Model->scopes();
            if (isset($all_scopes['recently']))
            {
                $crit_scopes[] = 'recently';
            }
            else if (isset($all_scopes['byName']))
            {
                $crit_scopes[] = 'byName';
            }

            $settings = $Model->modelSettings('GuiTable');
            $scopes = (isset($settings['scopes'])) ? $settings['scopes'] : array();
            foreach ($scopes as $scope)
            {
                $crit_scopes[] = $scope;
            }
        }
        
        $temp_file = new TemporaryFile('',null,'csv');
        $path = $temp_file->getFullPath();
        $output = fopen($path, "w");        

        //ispis filtera
        $used_filters = [];
        GuiTableController::getRelationFilterParams($Model, $fixed_filter, $used_filters, false);
        GuiTableController::getRelationFilterParams($Model, $select_filter, $used_filters, false);
        $preTableParams = array();
        $preTableParams['Vreme kreiranja'] = date("d.m.Y. H:i", time());
        $preTableParams['Kreirao korisnik'] = Yii::app()->user->model->DisplayName;
        $preTableParams['Korišćeni filteri'] = $used_filters;
        $select_filters='';
        foreach ($preTableParams as $key => $info)
        {
            if (gettype($info) != 'array')
            {
                fputcsv($output, [$key, $info]);
            }
            else 
            {
                foreach($info as $filter)
                {
                    $select_filters .= $filter.', ';
                }
                $select_filters = rtrim($select_filters, ', ');
                fputcsv($output, [$key, $select_filters]);
            }
        }
        $names=[];
        foreach($columns as $col)
        {
            $names = array_merge($names, [$this->tableToCsvFormatCell($Model->getAttributeLabel($col))]);
        }
        fputcsv($output, $names);
        
        //ispis podataka
        $i = 1;
        $total_rows = $Model->count($criteria);
        $criteria->scopes = $crit_scopes;
        $model_settings_number_fields = $Model->modelSettings('number_fields');
        $Model->findAllByParts(function($rows) use ($columns, $output, $callback_func, $total_rows, $model_settings_number_fields, &$i) {
            foreach ($rows as $row)
            {
                $arr = [];
                foreach ($columns as $column)
                {
                    if ($column === '#')
                    {
                        $arr[$column] = $i;
                    }
                    else
                    {
                        $column_value = $row->getAttributeDisplay($column);
                        if (in_array($column, $model_settings_number_fields))
                        {
                            $column_value = $row->$column;
                        }
                        $arr[$column] = $this->tableToCsvFormatCell($column_value);
                    }
                }
                fputcsv($output, $arr);
                
                if (!is_null($callback_func))
                {                    
                    $callback_func(round(($i/$total_rows)*100, 0, PHP_ROUND_HALF_DOWN));
                }
                $i++;
            }
        }, $criteria);
        
        //ispis sume
        $sum_row = $Model->getSumRow($fixed_filter, $select_filter, $columns_type);
        if ($sum_row !== null)
        {
            $arr = [];
            foreach ($columns as $column)
            {
                if ($Model->isSum($column, $columns_type))
                {
                    $arr[$column] = $this->tableToCsvFormatCell($sum_row->$column);
                }
                else
                {
                    $arr[$column] = '';
                }
            }
            fputcsv($output, $arr);
        }
        
        fclose($output);
        
        return $temp_file;
    }
    
    private function tableToCsvFormatCell($value)
    {
        $value = filter_var($value, FILTER_SANITIZE_STRING);
        $value = str_replace(["\t"], ' ', $value); //menjamo tabove sa space
        $value = str_replace(["\n", "\r\n", "\r"], '', $value); //menjamo nove redove sa praznim stringom
        $value = str_replace(["<br/>", "<br />"], ' | ', $value); //menjamo <br /> sa uspravnom crtom
        
        return $value;
    }
    
    public function tableToPdf($model, $columns_type, $columns, $guitable_filter, $callback_func=null)
    {
        $fixed_filter = isset($guitable_filter['fixed_filter'])?$guitable_filter['fixed_filter']:[];
        $select_filter = isset($guitable_filter[$model])?$guitable_filter[$model]:[];
        
        $Model = $model::model();
        $criteria = new SIMADbCriteria(array(
            'Model' => $model,
            'model_filter' => $fixed_filter
        ));
        $select_criteria = new SIMADbCriteria(array(
            'Model' => $model,
            'model_filter' => $select_filter
        ));
        $criteria->mergeWith($select_criteria);
        
        $crit_scopes = $criteria->scopes;
        if (is_null($crit_scopes))
        {
            $crit_scopes = [];
        }
        elseif(!is_array($crit_scopes))
        {
            $crit_scopes = [$crit_scopes];
        }
        if ($criteria->group == null)
        {
            //mora ovaj redosled primenjivanja scope'ova jer je bitnije sortiranje od eksplicitno primenjenih scopeova nego od podrazumevanih
            $all_scopes = $Model->scopes();
            if (isset($all_scopes['recently']))
            {
                $crit_scopes[] = 'recently';
            }
            else if (isset($all_scopes['byName']))
            {
                $crit_scopes[] = 'byName';
            }

            $settings = $Model->modelSettings('GuiTable');
            $scopes = (isset($settings['scopes'])) ? $settings['scopes'] : array();
            foreach ($scopes as $scope)
            {
                $crit_scopes[] = $scope;
            }
        }
           
        $temp_file = new TemporaryFile();
        $path = $temp_file->getFullPath();
        
        $preTableParams = array();
        $preTableParams['Vreme kreiranja'] = date("d.m.Y. H:i", time());
        $preTableParams['Kreirao korisnik'] = Yii::app()->user->model->DisplayName;
        $used_filters = array();
        GuiTableController::getRelationFilterParams($Model, $fixed_filter, $used_filters);
        GuiTableController::getRelationFilterParams($Model, $select_filter, $used_filters);
        $preTableParams['Korišćeni filteri'] = $used_filters;                

        //gradjenje headera
        $header = array();
        $data = array();
        $sum = array();
        $firstcolumnarray = array();
        foreach ($columns as $column)
        {
            array_push($firstcolumnarray, array($this->tableToPdfFormatCell($Model->getAttributeLabel($column)), 1));
        }
        array_push($header, $firstcolumnarray);
        
        //gradjenje podataka
        $i = 1;
        $total_rows = $Model->count($criteria);
        $criteria->scopes = $crit_scopes;
        $Model->findAllByParts(function($rows) use ($columns, &$data, $callback_func, $total_rows, &$i) {
            foreach ($rows as $row)
            {
                $rowarray = array();
                foreach ($columns as $column)
                {
                    if ($column === '#')
                    {
                        array_push($rowarray, $i);
                    }
                    else
                    {
                        array_push($rowarray, $this->tableToPdfFormatCell($row->columnDisplays($column)));
                    }
                }
                array_push($data, $rowarray);
                if (!is_null($callback_func))
                {                    
                    $callback_func(round(($i/$total_rows)*100, 0, PHP_ROUND_HALF_DOWN));
                }
                $i++;
            }
        }, $criteria);
               
        //gradjenje sume
        $sum_row = $Model->getSumRow($fixed_filter, $select_filter, $columns_type);
        if ($sum_row !== null)
        {
            $count = 0;
            foreach ($columns as $column)
            {
                if ($Model->isSum($column, $columns_type))
                {
                    $row_col = $this->tableToPdfFormatCell($sum_row->$column);
                    if(is_numeric($row_col))
                    {
                        $row_col = number_format($row_col, 2, '.', ' ');
                    }
                    
                    array_push($sum, array($row_col, $count));
                }
                else
                {
                    array_push($sum, array('$\sum$', $count));
                }
                $count++;
            }
        }
        
        $texobj = new SIMAtexlive();
        $texobj->tableToPdf($preTableParams, count($columns), $header, $data, $sum, $path);
        
        return $temp_file->getFileName().'.pdf';
    }
    
    private function tableToPdfFormatCell($cellValue)
    {
        $cellValue_strippedtags = strip_tags($cellValue);
        $cellValue_trimmed = trim($cellValue_strippedtags);
        $cellValue_escapedspecialchars = TemplatedFile::escapeSpecialCharactersInTex($cellValue_trimmed);
        $cellValue_spacedopenedparentheses = preg_replace("/(?! )\(/", " (", $cellValue_escapedspecialchars);
        $cellValue_spacedclosedparentheses = preg_replace("/\)(?! )/", ") ", $cellValue_spacedopenedparentheses);
        $result = $cellValue_spacedclosedparentheses;
        
        return $result;
    }
    
    private function getColumnWidths($model, $columns_type, $columns, $default_width = 100)
    {
        $column_widths = [];

        $guiTableSettings = $model->guiTableSettings($columns_type);

        $paged_table = PagedTableSetting::model()->findByAttributes([
            'user_id' => Yii::app()->user->id, 
            'table_string' => get_class($model) . $columns_type
        ]);

        if (!is_null($paged_table))
        {
            foreach ($paged_table->settings as $value)
            {
                if (in_array($value['name'], $columns))
                {
                    $column_widths[$value['name']] = $value['width'];
                }
                else
                {
                    $column_widths[$value['name']] = $default_width;
                }
            }
        }
        foreach ($guiTableSettings['columns'] as $_name => $_column)
        {
            if (is_array($_column))
            {
                if (!isset($column_widths[$_name]))
                {
                    $column_widths[$_name] = $default_width;
                }
                if (isset($_column['max_width']))
                {
                    $_max_width = intval($_column['max_width']);
                    if ($column_widths[$_name] > $_max_width)
                    {
                        $column_widths[$_name] = $_max_width;
                    }
                }
                if (isset($_column['min_width']))
                {
                    $_min_width = intval($_column['min_width']);
                    if ($column_widths[$_name] < $_min_width)
                    {
                        $column_widths[$_name] = $_min_width;
                    }
                }
            }
            else
            {
                if (!isset($column_widths[$_column]))
                {
                    $column_widths[$_column] = $default_width;
                }
            }
        }
        
        return $column_widths;
    }

    public function actionATModel()
    {       
        $_trace_cat = 'SIMA.GuiTableController.ATModel';
        Yii::trace('start',$_trace_cat);
        
        $data = $this->setEventHeader();
        
        $this->filter_get_input('event_id');
        $Model = $this->filter_input($data, 'Model');
        $columns = $this->filter_input($data, 'columns');
        
        $without_order_number = SIMAMisc::filter_bool_var($this->filter_input($data, 'without_order_number', false));
        
        $fixed_filter = (isset($data['fixed_filter']) && is_array($data['fixed_filter'])) ? $data['fixed_filter'] : [];
//        $fixed_filter_without_display_scopes = $fixed_filter;
//        $fixed_filter_without_display_scopes['display_scopes'] = null;
        $select_filter = (isset($data[$Model]) && is_array($data[$Model])) ? $data[$Model] : [];  
        
        $resetScope = false;
        if(isset($fixed_filter['resetScope']))
        {
            $resetScope = filter_var($fixed_filter['resetScope'], FILTER_VALIDATE_BOOLEAN);
            unset($fixed_filter['resetScope']);
        }
               
        $criteria = new SIMADbCriteria(array(
            'Model' => $Model,
            'model_filter' => $fixed_filter
        ));
//        $criteriaWithoutDisplayScopes = new SIMADbCriteria(array(
//            'Model' => $Model,
//            'model_filter' => $fixed_filter_without_display_scopes
//        ));
        $select_criteria = new SIMADbCriteria(array(
            'Model' => $Model,
            'model_filter' => $select_filter
        ));
        $criteria->mergeWith($select_criteria);
//        $criteriaWithoutDisplayScopes->mergeWith($select_criteria);
        
        unset($fixed_filter);
        unset($fixed_filter);
        unset($select_criteria);
        
        $model = $Model::model();
        $rows = [];
        
        $pager = new SIMAPager();
        //ova funkcija brise pozvane scopove za $Model jer se model iskoriscava TODO:
        $pager->setPage($model, $criteria, $data['page'], Yii::app()->params['pt_page_size'], $resetScope);
//            $pager->setPage($model, $criteriaWithoutDisplayScopes, $data['page'], Yii::app()->params['pt_page_size']);

        Yii::trace('after_pager',$_trace_cat);
        $all_scopes = $model->scopes();
        //mora ovaj redosled primenjivanja scope'ova jer je bitnije sortiranje od eksplicitno primenjenih scopeova nego od podrazumevanih
        $order_scope = $model->modelSettings('default_order_scope');
        if (!empty($order_scope))
        {
            $model = $model->$order_scope();
        }
        else
        {
            if (isset($all_scopes['recently']))
            {
                $model = $model->recently();
            }
            elseif (isset($all_scopes['byName']))
            {
                $model = $model->byName();
            }
        }
        $settings = $model->modelSettings('GuiTable');
        $scopes = (isset($settings['scopes'])) ? $settings['scopes'] : array();
        foreach ($scopes as $scope)
        {
            $model = $model->$scope();
        }         

//            $criteria->offset = $criteriaWithoutDisplayScopes->offset;
//            $criteria->limit = $criteriaWithoutDisplayScopes->limit;

        Yii::trace('before_find',$_trace_cat);
        if($resetScope === true)
        {
            $rows = $model->resetScope()->findAll($criteria);
        }
        else
        {
            $rows = $model->findAll($criteria);
        }
        
//        error_log(__METHOD__.count($rows));
        
        unset($criteria);
        
        $curr_page = $pager->curr_page;
        $max_page = $pager->max_page;
        
        unset($pager);
                    
        if (!is_null($model))
        {
            $column_widths = $this->getColumnWidths($model, $data['columns_type'], $columns);
            
            $percent = 0;
            $total_rows = count($rows);
            $i = 1;            
            $rows_html = [];
            $time_before = round(microtime(true) * 1000);            
            $path2 = isset($model->path2)?$model->path2:'';  
            Yii::trace('before_display',$_trace_cat);
            
            foreach ($rows as $key => $row) 
            {    
                
                Yii::beginProfile('render_rows',$_trace_cat);
                $rows_html[] = $this->localRenderPartial('row',array(
                    'row'=>$row,
                    'columns'=>$columns,
                    'column_widths'=>$column_widths,
                    'path2'=>$path2,
                    'without_order_number' => $without_order_number
                ), true, false);
                $time_after = round(microtime(true) * 1000);
                if (($time_after - $time_before) > Yii::app()->params['guiTableSendUpdateInterval'])
                {
                    $percent = round(($i/$total_rows)*100, 0, PHP_ROUND_HALF_DOWN);
                    $this->sendUpdateOnTableDataLoad($percent, $rows_html);
                    $rows_html = [];
                    $time_before = $time_after;
                }
                $i++;
                
                unset($rows[$key]);
                gc_collect_cycles();
                
                Yii::endProfile('render_rows',$_trace_cat);
            }
            
            Yii::trace('after_display',$_trace_cat);
            if (count($rows_html) > 0)
            {
                $this->sendUpdateOnTableDataLoad(100, $rows_html);
            }
        }
              
        $col_number = count($columns) + 2; //+2 zbog kolona redni broj i options
        
        Yii::trace('end',$_trace_cat);
        
        $this->sendEndEvent([
            'curr_page' => $curr_page,
            'max_page' => $max_page,
            'columns_number' => $col_number
        ]);
    }
    
    private function sendUpdateOnTableDataLoad($percent, $rows_html)
    {
        $this->sendUpdateEvent($percent, [
            'rows' => $rows_html, 
            'process_output_before' => Yii::app()->clientScript->renderBodyBegin(),
            'process_output_after' => Yii::app()->clientScript->renderBodyEnd() . Yii::app()->clientScript->renderBodyLoadAndReady()
        ]);
        
        Yii::app()->clientScript->scripts = []; //moramo da resetujemo registrovane skripte, jer process output se samo nadovezuje u jednom pozivu
    }

    public function actionATModelSum($Model, $columns_type)
    {        
        if (!isset($_POST['columns']))
        {
            throw new Exception('nisu zadate kolone!!!');
        }
        
        $model = $Model::model();
        $columns = $_POST['columns'];
        $fixed_filter = (isset($_POST['fixed_filter']) && is_array($_POST['fixed_filter'])) ? $_POST['fixed_filter'] : [];
        $select_filter = (isset($_POST[$Model]) && is_array($_POST[$Model])) ? $_POST[$Model] : [];
        $sum_row = $model->getSumRow($fixed_filter, $select_filter, $columns_type);
        
        $without_order_number = SIMAMisc::filter_bool_var($this->filter_post_input('without_order_number', false));
                               
        if ($model === NULL)
        {
            $html_response = ''; //ukoliko vise ne zadovoljava uslove tabele, nestaje iz tabele
        }
        else
        {
            $html_response = $this->localRenderPartial('table_sum', [
                'model' => $model, 
                'sum_row' => $sum_row, 
                'columns_type' => $columns_type, 
                'columns' => $columns,
                'without_order_number' => $without_order_number
            ], true, false);
        }

        $this->respondOK([
            'html' => $html_response
        ]);
    }

    /**
     * Vraca samo jedan red
     * @param type $model
     * @param type $model_id
     * @param type $columns - kolone koje treba da ima red
     * @throws Exception
     */
    public function actionGetRow($model)
    {
        $columns = $this->filter_post_input('columns');
        $columns_type = $this->filter_post_input('columns_type', false);
        
        $without_order_number = SIMAMisc::filter_bool_var($this->filter_post_input('without_order_number', false));
        
        $Model = $model::model();
        $fixed_filter = (isset($_POST['fixed_filter']) && is_array($_POST['fixed_filter'])) ? $_POST['fixed_filter'] : [];
        $select_filter = (isset($_POST[$model]) && is_array($_POST[$model])) ? $_POST[$model] : [];
        $criteria = new SIMADbCriteria(array(
            'Model' => $model,
            'model_filter' => $fixed_filter
        ));
        $select_criteria = new SIMADbCriteria(array(
            'Model' => $model,
            'model_filter' => $select_filter
        ));
        $criteria->mergeWith($select_criteria);        

        $settings = $Model->modelSettings('GuiTable');
        $scopes = (isset($settings['scopes'])) ? $settings['scopes'] : array();
        foreach ($scopes as $scope)
            $Model = $Model->$scope();

        $models = $Model->findAll($criteria);
        
        if (is_null($columns_type))
        {
            $column_widths = $this->getColumnWidths($Model, $columns_type, $columns, null);
        }
        else
        {
            $column_widths = $this->getColumnWidths($Model, $columns_type, $columns);
        }
        
        $html_response = array();
        $temp_model_ids = array(); //pomocni niz u koji se pamte id-evi modela koji zadovoljavaju sve filtere
        foreach ($models as $model)
        {
            array_push($temp_model_ids, $model->id);
            $path2 = isset($model->path2)?$model->path2:'';
            $temp = array();
            $temp['model_id'] = $model->id;
            $temp['row'] = $this->localRenderPartial('row', array(
                'row' => $model, 
                'columns' => $columns,
                'column_widths' => $column_widths,
                'path2' => $path2,
                'without_order_number' => $without_order_number
            ), true, false);
            array_push($html_response, $temp);
        }
        
        if (isset($_POST['model_ids']))
        {
            $model_ids = $_POST['model_ids'];
            $result = array_diff($model_ids, $temp_model_ids); //razlika izmedju svih modela i onih koji zadovoljavaju filtere, oni koji ne zadovoljavaju se brisu

            foreach ($result as $value)
            {
                $temp = array();
                $temp['model_id'] = $value;
                $temp['row'] = "";
                array_push($html_response, $temp);
            }
        }

        $this->respondOK([
            'rows' => $html_response,
//            'processOutput' => $this->processOutput('')
        ]);
    }

    public function actionGetColumns($id, $model, $table_id)
    {
        if (!isset($id) || !isset($model) || !isset($table_id))
            throw new Exception('actionGetColumns - nisu pravilno zadati parametri');

        if (!isset($_POST['columns']))
        {
            throw new Exception('Nisu zadate kolone!');
        }
        $columns = $_POST['columns'];
        $model = $model::model();

        $html = $this->localRenderPartial('columns', array(
            'id' => $id,
            'model' => $model,
            'columns' => $columns,
            'table_id' => $table_id
        ), true, true);

        $this->respondOK([
            'html' => $html
        ]);
    }

    public function actionSaveColumns()
    {
        if (!isset($_POST['table_string']))
            throw new Exception('nisu lepo zadati parametri.');

        $table_string = $_POST['table_string'];
        if (isset($_POST['settings']))
        {
            $settings = $_POST['settings'];
        }
        
        $table_settings = PagedTableSetting::model()->findByAttributes(array('user_id' => Yii::app()->user->id, 'table_string' => $table_string));
        if ($table_settings == null)
        {            
            $table_settings = new PagedTableSetting();
            $new = true;
        }
        $table_settings->user_id = Yii::app()->user->id;
        $table_settings->table_string = $table_string;
        if (isset($settings))
        {
            $table_settings->settings = $settings;
        }
        if (isset($new))
        {
            $table_settings->save();
        }
        else
        {
            $table_settings->update();
        }

        $this->respondOK();
    }

    public function tableToPdfPreTableWork($model_filter)
    {
        throw new SIMAWarnException('not impleneted');
    }

    public function actionRenderFilterForColumn($model_name, $model_id, $column, $table_id, $columns_type)
    {
        $Model = $model_name::model();
        $form_display_scopes = $Model->modelSettings('form_display_scopes');
        foreach ($form_display_scopes as $key=>$scope)
        {
            if (is_array($scope))
            {
                throw new Exception('nije implementirano postavljane parametrizovanih scopova za forme '.$key);
            }
            else
            {
                $Model = $Model->$scope();
            }
        }

        $fixed_filter = $this->filter_post_input('fixed_filter', false);
        if (empty($fixed_filter))
        {
            $fixed_filter = [];
        }

        //videti komentar u funkciji actionSaveColumnValue zasto radimo preko fixed filtera, kada vec imamo model_id
        $fixed_filter['ids'] = $model_id;
        $_model = $Model->find([
            'model_filter' => $fixed_filter
        ]);
        
        if (empty($_model))
        {
            throw new SIMAWarnExceptionModelNotFound($model_name, $model_id);
        }
        
        $options = [
            'fixed_filter'=>$fixed_filter,
            'onkeyup'=>'',
            'onchange'=>'',
            'label'=>''
        ];
        $guiTableSettings = $_model->guiTableSettings($columns_type);
        if (isset($guiTableSettings['columns'][$column]['edit']))
        {
            $edit_props = $guiTableSettings['columns'][$column]['edit'];
            if (gettype($edit_props) === 'string')
            {
                $options['filter_type'] = $edit_props;
            }
            elseif (gettype($edit_props) === 'array')
            {
                $options['filter_type'] = $edit_props[0];
                $column = $edit_props['relName'];
            }
        }
        $filter = SIMAGuiTable::renderColumnFilter($_model, $column, $table_id, $options);
        
        $html = $this->localRenderPartial('render_filter', array(                
                'filter' => $filter,                
        ), true, true);
        
        $this->respondOK(array(
            'html' => $html
        ));
    }

    public function actionSaveColumnValue($model_name, $model_id)
    {
        $Model = $model_name::model();
        $form_display_scopes = $Model->modelSettings('form_display_scopes');
        foreach ($form_display_scopes as $key=>$scope)
        {
            if (is_array($scope))
            {
                throw new Exception('nije implementirano postavljane parametrizovanih scopova za forme '.$key);
            }
            else
            {
                $Model = $Model->$scope();
            }
        }
        
        $fixed_filter = $this->filter_post_input('fixed_filter', false);
        if (empty($fixed_filter))
        {
            $fixed_filter = [];
        }
        
        //da bismo nasli model dovoljan je $model_id, ali u ovom slucaju primenjujemo i fixed_filter, jer nema sta da poremeti, posto je ovo inline popunjavanje
        //polja, sto znaci da je ono vidljivo, odnosno da je zadovoljilo fixed_filter. To radimo zbog toga sto za neke modele se neke vrednosti setuje preko 
        //filtera, kao sto je recimo month_id za WorkedHoursEvidenceToPerson, pa nam u tom slucaju treba
        $fixed_filter['ids'] = $model_id;
        $model = $Model->find([
            'model_filter' => $fixed_filter
        ]);
        
        if (empty($model))
        {
            throw new SIMAWarnExceptionModelNotFound($model_name, $model_id);
        }

        $model_data = $this->filter_post_input($model_name, false);
        if (empty($model_data))
        {
            $model_data = [];
        }
        $model->setModelAttributes($model_data);
        if (!$model->validate())
        {            
            $this->respondFail(SIMAHtml::showErrorsInHTML($model));
        }
        else
        {      
            $model->save(false);            
            $model->refresh();
        }
        
        $this->respondOK();
    }

    public function actionRenderInlineRow($model, $columns_type, $table_id)
    {
        if (!isset($model) || !isset($columns_type))
        {
            throw new Exception('nije zadat model ili tip kolone.');
        }

        if (!isset($_POST['columns']))
        {
            throw new Exception('nisu zadate kolone.');
        }
        else
        {
            $columns = $_POST['columns'];
        }
        $not_visible_columns = isset($_POST['not_visible_columns'])?$_POST['not_visible_columns']:[];
        $model = $model::model();
        $fixed_filter = isset($_POST['fixed_filter']) ? $_POST['fixed_filter'] : [];
        $row = '';
        
        $without_order_number = SIMAMisc::filter_bool_var($this->filter_post_input('without_order_number', false));
        
        $hidden_required_columns = [];
        foreach ($not_visible_columns as $not_visible_column) 
        {
            if ($model->isColumnRequired($not_visible_column))
            {
                array_push($hidden_required_columns, $not_visible_column);
            }
        }        
        if (count($hidden_required_columns) === 0)
        {
            $guiTableSettings = $model->guiTableSettings($columns_type);
            $guiTable_columns = $guiTableSettings['columns'];
            $guiTable_relations = $model->relations();
            $columns_for_edit = array();
            foreach ($guiTable_columns as $key => $value)
            {
                if (is_array($value))
                {
                    if (isset($value['edit']) && $value['edit'] == true)
                    {
                        array_push($columns_for_edit, $key);
                    }
                }
            }
            foreach ($columns as $column)
            {   
                if ($model->isColumnRequired($column))
                {
                    $clas = 'sima-guitable-field-required';
                }
                else
                {
                    $clas = 'sima-guitable-field-safe';
                }
                if (in_array($column, $columns_for_edit))
                {
                    $clas .= ' sima-guitable-field-edit';
                    $options = [
                        'fixed_filter'=>$fixed_filter,
                        'onkeyup'=>'',
                        'onchange'=>''
                    ];
                    if (isset($guiTable_columns[$column]['edit']))
                    {
                        $edit_props = $guiTable_columns[$column]['edit'];
                        if (gettype($edit_props) === 'string')
                        {
                            $options['filter_type'] = $edit_props;
                        }
                        elseif (gettype($edit_props) === 'array')
                        {
                            $options['filter_type'] = $edit_props[0];
                            $column = $edit_props['relName'];
                        }
                    }
                    $filter = SIMAGuiTable::renderColumnFilter($model, $column, $table_id, $options);
                }
                else
                {
                    $filter = '';
                }
                $row .= $this->localRenderPartial('inline_row', array(
                    'column' => $column,
                    'filter' => $filter,
                    'clas' => $clas
                        ), true, false);
            }
//            $row = $this->processOutput($row);
            
            $order_number_column = '';
            if (!isset($without_order_number) || $without_order_number === false)
            {
                $order_number_column = "<td class='order-number'></td>";
            }
            
            $row = "<tr class='sima-guitable-row inline_row' model='" . get_class($model) . "'>$order_number_column<td class='options'></td>" . $row . "</tr>";
        }
        else 
        {
            $raise_note_text = '';
            foreach ($hidden_required_columns as $hidden_required_column) 
            {
                $raise_note_text .= $model->getAttributeLabel($hidden_required_column).', ';
            }
            $this->raiseNote("Sledeće kolone moraju biti uključene jer su obavezna polja: ".substr($raise_note_text, 0, -2).'.');
        }

        $this->respondOK(array(
            'row' => $row
        ));
    }

    public function actionSaveInlineRow($model)
    {
        if (!isset($_POST[$model]))
        {
            throw new Exception('nisu zadate vrednosti');
        }
        $_m = new $model();

        $_m->setModelAttributes($_POST[$model]);

        foreach ($_POST[$model] as $key => $value)
        {
            if (is_array($value) && isset($value['ids']) && !empty($value['ids']) && is_numeric($value['ids']))
            {
                $relations = $_m->relations();
                if (isset($relations[$key]))
                {
                    $_attr = $relations[$key][2];
                    $_m->$_attr = $value['ids'];
                }
            }
        }
        if (!$_m->validate())
        {
            $this->respondFail(SIMAHtml::showErrorsInHTML($_m));
        }
        else
        {
            $_m->save(false);
            $_m->refresh();
        }

        $this->respondOK(array('model_id' => $_m->id), $_m);
    }

    public function actionGetColumnsDisplayName($model)
    {
        if (!isset($model))
        {
            throw new Exception('nije zadat model.');
        }

        if (!isset($_POST['columns']))
        {
            throw new Exception('nisu zadate kolone');
        }
        $model = $model::model();
        $columns = $_POST['columns'];
        $columns_names = array();
        foreach ($columns as $column)
        {
            $column_name = $model->getAttributeLabel($column);
            $columns_names[$column] = $column_name;
        }

        $this->respondOK([
            'columns_names' => $columns_names
        ]);
    }

    public function actionGetColumnValue($model, $model_id, $column)
    {
        if (!isset($model) || !(isset($model_id)) || !(isset($column)))
        {
            throw new Exception('Nije zadat model, model_id ili kolona.');
        }

        $model = $model::model()->findByPk($model_id);
        if ($model === null)
        {
            throw new Exception('Ne postoji model sa ovim id-em.');
        }

        $this->respondOK(array('value' => $model->$column));
    }    

}
