<?php

return [
    'YouCanNotExportEmptyTable' => 'Table to export must not be empty!',
    'ExportOptions' => 'Export options',
    'YouMustChooseFormat' => 'Please choose format.',
    'ChooseAtLeastOneColumn' => 'Please choose at least one column.',
    'Export' => 'Export',
    'ChooseFormatYouWantToExportTo' => 'Please choose document format you prefer.',
    'ChooseColumnsToShowInTable' => 'Please choose columns you want to display in the table to export.',
    'MoveRowsToMakeDifferentDisplayOrderOfColumnsToExport' => 'Move rows to change display order of columns in the table to export.',
    'export' => 'export',
    
    
    
];

