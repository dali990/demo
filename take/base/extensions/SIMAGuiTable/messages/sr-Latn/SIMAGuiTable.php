<?php

return [
    'YouCanNotExportEmptyTable' => 'Ne možete da izvezete praznu tabelu!',
    'ExportOptions' => 'Opcije za izvoz',
    'YouMustChooseFormat' => 'Morate izabrati format!',
    'ChooseAtLeastOneColumn' => 'Morate izabrati bar jednu kolonu!',
    'Export' => 'Izvezi',
    'ChooseFormatYouWantToExportTo' => 'Izaberite format u kojem želite dokument.',
    'ChooseColumnsToShowInTable' => 'Izaberite kolone koje će se prikazati u tabeli.',
    'MoveRowsToMakeDifferentDisplayOrderOfColumnsToExport' => 'Povlačenjem redova možete napraviti drugačiji redosled prikaza kolona u tabeli koju želite da izvezete.',
    'export' => 'izvezi',
    
    
];

