<?php

return [
    'Export' => 'Izvezi',
    'export' => 'izvezi',
    'ProblemInGeneratingTable' => 'Doslo je do problema u generisanju tabele, administratori su kontaktirani.',
    'SettingsColumns' => 'Kolone',
    'SettingsFilterIsSet' => 'Imate postavljen filter za ovu kolonu',
    'SettingsDeleteFilter' => 'Obrišite filter'
];
