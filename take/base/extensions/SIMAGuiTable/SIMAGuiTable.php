<?php

class SIMAGuiTable extends CWidget
{
    public static $GUITABLE_COLUMN_MAX_WIDTH = 1000;
    public static $GUITABLE_COLUMN_MIN_WIDTH = 30;
    public static $GUITABLE_COLUMN_DEFAULT_WIDTH = 100;
    
    //parametri koji se setuju spolja
    public $id = null;
    public $class = null;
    public $model = null; //obavezan
    public $columns_type = '';
    public $auto_height = true;
    public $add_button = false;
    public $fixed_filter = [];
    public $select_filter = [];    
    public $populate = true;
    public $setRowDblClick = null;
    public $setRowSelect = null;
    public $setRowUnSelect = null;
    public $onSelectionChange = null;
    public $setAppendRowTrigger = null;
    public $setRefreshRowTrigger = null;
    public $setMultiSelect = null;
    public $multiselect_options = true;
    public $disableMultiSelect = false;
    public $selectRowOnAppend = false; //selektuje red nakon dodavanja
    public $selectRowOnAppendAndLaunchAddButton = false; //automatski pokrece klik na add button(odnosi se na add button unutar dela koji se otvara nakon selektovanja reda)                                                         
    public $launchAddButtonOnLoad = false; //pokrece add button nakon ucitavanja tabele
    public $default_selected_row_id = null; //id red koji je po defaultu selektovan
    public $on_populate_func = null;
    public $additional_options_html = null;
    
    //delovi header opcija koji se mogu ukljuciti/iskljuciti
    public $title='', $table_settings=true, $show_add_button=true, $pagination=true, $filters_in=true;
    public $export=['pdf', 'csv', 'ods', 'xls'], $custom_buttons=[], $filters_out=true;    
    //default redosled header opcija
    public $headerOptionRows = [
        ['title'],
        ['table_settings','show_add_button','pagination','filters_in','export','custom_buttons','filters_out','additional_options_html']
    ];
    
    //lokalni parametri
    private $guiTable_columns = []; //kolone koje se zadaju u modelu
    private $columns = []; //niz kolona koje ce se prikazati u tabeli
    private $guiTable_filters = []; //filteri van tabele

    public function run()
    {        
        if ($this->id === null)
        {
            $this->id = SIMAHtml::uniqid();
        }
        if ($this->model === null)
        {
            throw new Exception('SIMAGuiTable - model is null');
        }
        $id = $this->id;
        if (is_string($this->model))
        {
            $_model = $this->model;
            $this->model = $_model::model();
        }
        $model = $this->model;
        $guiTableSettings = $model->guiTableSettings($this->columns_type);
        $guitable_model_settings = $model->modelSettings('GuiTable');
        
        //proveravamo da li tabela ima zadatu sumu, group by i order by        
        $model_has_sums = (isset($guiTableSettings['sums'])) ? ($guiTableSettings['sums'] != []) : false;
        $order_by = (isset($guiTableSettings['order_by'])) ? ((gettype($guiTableSettings['order_by']) == 'string') ?
                        explode(',', preg_replace('/\s+/', '', $guiTableSettings['order_by'])) : $guiTableSettings['order_by']) : [];
        
        //string koji oznacava tip tabele, kombinacija modela i kolona, sluzi za pamcenje sirina kolona
        $table_string = get_class($model) . $this->columns_type;
        //proveravamo da li su kolone vec zapamcene u bazi
        $paged_table = PagedTableSetting::model()->findByAttributes(array('user_id' => Yii::app()->user->id, 'table_string' => $table_string));
        if ($paged_table !== null && $paged_table->settings !== null)
        {
            $paged_table_settings = $paged_table->settings;
        }

        //proveravamo da li je default filter string, ako jeste, pretvaramo ga u niz       
        if (is_string($this->fixed_filter))
        {
            $this->fixed_filter = CJSON::decode($this->fixed_filter);
        }

        //ako je ukljucen prikaz filtera van tabele onda ih renderujemo
        if ($this->showFiltersOut() === true)
        {
            //filteri koji se ispisuju van tabele
            if (isset($guiTableSettings['filters']))
            {
                foreach ($guiTableSettings['filters'] as $_guitable_filter)
                {
                    $this->guiTable_filters[$_guitable_filter] = SIMAGuiTable::renderColumnFilter($this->model, $_guitable_filter, $this->id, ['fixed_filter'=>$this->fixed_filter]);                
                }            
            }
        }

        //ukoliko je prosledjen samo jedan button 
        if (!is_array($this->add_button) || !isset($this->add_button['button1']))
        {
            if (is_string($this->add_button) && ($this->add_button === '0' || $this->add_button === '1'))
            {
                $this->add_button = filter_var($this->add_button, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
            }
            $this->add_button = ['button1' => $this->add_button];
        }
        //pakovanje add_button za index
        $index_add_button = [];
        foreach ($this->add_button as $key => $add_button)
        {
            $_access = true;
            if (is_array($add_button) && isset($add_button['access']))
            {
                if (is_bool($add_button['access']))
                {
                    $_access = $add_button['access'];
                }
                else if (is_string($add_button['access']))
                {
                    $_access = Yii::app()->user->checkAccess($add_button['access'], [], $model);
                }
                else
                {
                    throw new Exception('access u addButton nije odgovarajuceg tipa :'.  gettype($add_button['access']));
                }
            }
            if (count($index_add_button)>0 && !$_access)
            {
                continue;
            }
            $disabled = 
                    (is_bool($add_button) && !$add_button)
                    ||
                    (is_array($add_button) && isset($add_button['disabled']) && $add_button['disabled'])
                    ||
                    !$_access;
            $classes = $key;
            if (!$_access)
            {
                $classes .= ' _no_access';
            }
            if ($disabled)
            {
                $classes .= ' _disabled';
            }
            
            $index_add_button[] = [
//                'onhover' => isset($add_button['onhover'])?$add_button['onhover']:'dodaj '.$model->modelLabel(),
                'onhover' => isset($add_button['onhover'])?$add_button['onhover']:Yii::t('BaseModule.Common', 'Add').' '.$model->modelLabel(),
                'onclick' => isset($add_button['action'])?$add_button['action']:
                    'if (!$(this).hasClass("_disabled")) $("#'.$id.'").simaGuiTable("add", "'.$key.'");',
                'classes' => $classes
            ];
        }

        //setovanje kolona za prikaz        
        if (!isset($guiTableSettings['columns']))
        {
            throw new Exception('Nisu zadate kolone u gui tabeli.');
        }
        else
        {
            $this->guiTable_columns = $guiTableSettings['columns'];
        }
        //preracunavanje kolona koje treba da se vide
        if (!isset($paged_table_settings) || $this->table_settings === false) //ako nisu zapamcene kolone u bazi, onda uzimamo samo one koje su zadate iz modela
        {
            foreach ($this->guiTable_columns as $key => $value)
            {
                if (is_array($value))
                {
                    $this->addColumn($key);
                }
                else
                {
                    $this->addColumn($value);
                }
            }
        }
        //ako su zapamcene onda postavljamo kolone da budu one koje su zapamcene u bazi i dodajemo/oduzimamo razliku sa kolonama koje su zadate u modelu
        else
        {
            $_column_names = $this->getGuiTableColumnsNames();
            foreach ($paged_table_settings as $key => $value)
            {
                if (in_array($value['name'], $_column_names))
                {
                    $hidden = isset($value['hidden'])?$value['hidden']:'';
                    $this->addColumn($value['name'], $value['width'], $hidden);
                }
            }
            //razlika kolona u bazi i zadatih kolona. Izbacujemo sve one kolone koje su zapamcene u bazi, a nisu vise zadate
            //iz modela            
//            $diff1 = array_diff($this->getSavedColumnsNames($paged_table_settings), $this->getGuiTableColumnsNames());
//            foreach ($diff1 as $value)
//            {
//                foreach ($this->columns as $key1 => $value1)
//                {
//                    if ($value == $value1['name'])
//                    {
//                        unset($this->columns[$key1]);
//                    }
//                }
//            }
            //razlika izmedju zadatih kolona i kolona u bazi. Dodajemo sve one kolone koje su zadate u modelu, a nema ih u bazi.
            //One su sakrivene.
            $diff2 = array_diff($this->getGuiTableColumnsNames(), $this->getSavedColumnsNames($paged_table_settings));
            foreach ($diff2 as $value)
            {
                if(isset($value))
                {
                    $this->addColumn($value, '', 'false');
                }
            }
        }
        
        $without_order_number = isset($guitable_model_settings['without_order_number']) ? $guitable_model_settings['without_order_number'] : false;
        
        echo $this->render('index', [
            'id' => $id,
            'class' => (is_null($this->class))?'':$this->class,
            'model' => $model,
            'auto_height' => $this->auto_height,
            'columns_type' => $this->columns_type,
            'columns' => $this->columns,
            'add_buttons' => $index_add_button,            
            'guitable_filters' => $this->guiTable_filters,            
            'order_by' => $order_by,
            'table_string' => $table_string,
            'fixed_filter' => $this->fixed_filter,                        
            'multiselect_options' => $this->multiselect_options,
            'headerOptionRows' => $this->headerOptionRows,
            'without_order_number' => $without_order_number
        ]);

        //podesavanja koja se salju javascriptu
        $guitable_params = [
            'add_button' => $this->add_button,
            'without_order_number' => $without_order_number
        ];
        $guitable_params['fixed_filter'] = $this->fixed_filter;
        $guitable_params['select_filter'] = $this->select_filter;
        $guitable_params['table_settings'] = $this->table_settings;
        $guitable_params['disableMultiSelect'] = $this->disableMultiSelect;
        $guitable_params['selectRowOnAppend'] = $this->selectRowOnAppend;
        $guitable_params['selectRowOnAppendAndLaunchAddButton'] = $this->selectRowOnAppendAndLaunchAddButton;
        $guitable_params['launchAddButtonOnLoad'] = $this->launchAddButtonOnLoad;
        $guitable_params['default_selected_row_id'] = $this->default_selected_row_id;
        $guitable_params['translation'] = [
            'you_can_not_export_empty_table' => Yii::t('SIMAGuiTable.SIMAGuiTable', 'YouCanNotExportEmptyTable'),
            'export_options' => Yii::t('SIMAGuiTable.SIMAGuiTable', 'ExportOptions'),
            'you_must_choose_format' => Yii::t('SIMAGuiTable.SIMAGuiTable', 'YouMustChooseFormat'),
            'choose_at_least_one_column' => Yii::t('SIMAGuiTable.SIMAGuiTable', 'ChooseAtLeastOneColumn'),
            'export' => Yii::t('SIMAGuiTable.SIMAGuiTable', 'Export'),
            
        ];
        
        if ($model_has_sums)
        {
            $guitable_params['has_sum'] = true;
        }
        if ($this->populate)
        {
            $guitable_params['populate'] = true;
        }
        if ($this->setRowDblClick !== null)
        {
            $guitable_params['setRowDblClick'] = $this->setRowDblClick;
        }
        if ($this->setRowSelect !== null)
        {
            $guitable_params['setRowSelect'] = $this->setRowSelect;
        }
        if ($this->setRowUnSelect !== null)
        {
            $guitable_params['setRowUnSelect'] = $this->setRowUnSelect;
        }
        if ($this->onSelectionChange !== null)
        {
            $guitable_params['onSelectionChange'] = $this->onSelectionChange;
        }
        if ($this->setAppendRowTrigger !== null)
        {
            $guitable_params['setAppendRowTrigger'] = $this->setAppendRowTrigger;
        }
        if ($this->setRefreshRowTrigger !== null)
        {
            $guitable_params['setRefreshRowTrigger'] = $this->setRefreshRowTrigger;
        }
        if ($this->setMultiSelect !== null)
        {
            $guitable_params['setMultiSelect'] = $this->setMultiSelect;
        }
        $guiTableSettings['column_max_width'] = SIMAGuiTable::$GUITABLE_COLUMN_MAX_WIDTH;
        $guiTableSettings['column_min_width'] = SIMAGuiTable::$GUITABLE_COLUMN_MIN_WIDTH;
        if (!empty($this->on_populate_func))
        {
            $guitable_params['on_populate_func'] = $this->on_populate_func;
        }
        
        $json = CJSON::encode($guitable_params);

        $register_script = "$('#" . $id . "').simaGuiTable('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);        
    }

    /**
     * pomocna funkcija koja dodaje kolonu sa svim njenim dodatnim parametrima u niz kolona
     * @param string $name
     * @param string $width
     * @param string $hidden
     */
    private function addColumn($name, $width = null, $hidden = '')
    {
        //proveravamo da li je kolona vec dodata...ne bi trebalo da se desava
        //onda se samo ignorise, jer ce nakon ucitavanja tabele da se sacuvaju kolone u bazi i samim tim ce
        //se ispraviti spisak kolona(nece biti dupliranih)
        if ($this->isColumnAdded($name))
        {
            return;
        }
        
        //proveravamo da li se kolona nalazi na spisku za filtere van tabele. Ako se nalazi onda je ne dodajemo 
        if (($key = array_key_exists($name, $this->guiTable_filters)) === false)
        {
            $column = [];
            $column['name'] = $name;
            $column['hidden'] = $hidden;
            $column['edit'] = '';
            $column['hide_filter'] = '';
            $column['max_width'] = SIMAGuiTable::$GUITABLE_COLUMN_MAX_WIDTH;
            $column['min_width'] = SIMAGuiTable::$GUITABLE_COLUMN_MIN_WIDTH;
            if (isset($this->guiTable_columns[$name]))
            {
                if (isset($this->guiTable_columns[$name]['edit']) || in_array('edit', $this->guiTable_columns[$name]))
                {
                    $column['edit'] = 'true';
                }
                if (isset($this->guiTable_columns[$name]['hide_filter']) && $this->guiTable_columns[$name]['hide_filter'] == true)
                {
                    $column['hide_filter'] = 'true';
                }
                if (isset($this->guiTable_columns[$name]['max_width']))
                {
                    $column['max_width'] = $this->guiTable_columns[$name]['max_width'];
                }
                if (isset($this->guiTable_columns[$name]['min_width']))
                {
                    $column['min_width'] = $this->guiTable_columns[$name]['min_width'];
                }
                //ako nije spolja prosledjena sirina onda se uzima iz podesavanja za guitable, ako je tamo postavljena
                if (empty($width) && isset($this->guiTable_columns[$name]['default_width']))
                {
                    $width = $this->guiTable_columns[$name]['default_width'];
                }
            }
            
            if (empty($width))
            {
                $width = SIMAGuiTable::$GUITABLE_COLUMN_DEFAULT_WIDTH;
            }

            $_max_width = intval($column['max_width']);
            if ($width > $_max_width)
            {
                $width = $_max_width;
            }
            $_min_width = intval($column['min_width']);
            if ($width < $_min_width)
            {
                $width = $_min_width;
            }
            $column['width'] = $width;
            $column_params = SIMAGuiTable::getColumnParams($this->model, $column['name'], $this->fixed_filter);
            $column['number_field'] = in_array($column_params['column'], $column_params['model']->modelSettings('number_fields'))?true:false;
            $column['column_filter_type'] = '';
            if (is_string($column_params['filter_type']))
            {
                $column['column_filter_type'] = $column_params['filter_type'];
            }
            else if (is_array($column_params['filter_type']) && isset($column_params['filter_type'][0]))
            {
                $column['column_filter_type'] = $column_params['filter_type'][0];
            }
            //ako je ukljucen prikaz filtera u tabeli onda ih renderujemo
            if ($this->showFiltersIn() === true)
            {
                $column['filter'] = SIMAGuiTable::renderColumnFilter($this->model, $column['name'], $this->id, ['fixed_filter'=>$this->fixed_filter]);
            }
            $column = array_merge($column, $column_params);            
            array_push($this->columns, $column);
        }
    }

    /**
     * kreira niz sa imenima kolona koje su zapamecene u bazi
     * @param array $saved_columns - kolone koje su zapamcene u bazi
     * @return array
     */
    private function getSavedColumnsNames($saved_columns)
    {
        $saved_columns_names = [];
        //kreira samo niz sa imenima kolona koje su zapamcene u bazi
        foreach ($saved_columns as $value)
        {
            array_push($saved_columns_names, $value['name']);
        }

        return $saved_columns_names;
    }

    private $curr_columns_names = null;
    /**
     * kreira samo niz sa imenima kolona koje su zadate u modelu     
     * @return array
     */
    private function getGuiTableColumnsNames()
    {
        if (is_null($this->curr_columns_names))
        {
            $this->curr_columns_names = [];
            foreach ($this->guiTable_columns as $key => $value)
            {
                if (is_array($value))
                {
                    array_push($this->curr_columns_names, $key);
                }
                else
                {
                    array_push($this->curr_columns_names, $value);
                }
            }
        }

        return $this->curr_columns_names;
    }
    
    /**
     * pakuje podatke potrebne za filter. Ako se kolona sastoji od relacija razdvojenih tackom ova funkcija obradi tu putanju i vrati potrebne podatke za filter
     * @param Model $model
     * @param string $path
     * @param array $fixed_filter
     * @return array
     */
    private static function getColumnParams($model, $path, $fixed_filter)
    {
        if (!isset($path) || $path === '')
            return [];
        
        $column_array = explode('.', $path);
        $start_model = $model;        
        $name = get_class($model);
        $column = $column_array[0];
        $show_filter = true;
        $filter_type = '';
        foreach ($column_array as $col)
        {
            $relations = $model->relations();
            $model_settings = $model->modelSettings('filters');
            $name = $name . "[$col]";
            //ako je relacija u pitanju
            if (isset($relations[$col]))
            {
                if ($show_filter === true && !isset($model_settings[$col]))
                {
                    $show_filter = false;
                }
                //ako je poslednji element u nizu onda postavljamo name i kolonu
                if ($col === end($column_array))
                {
                    $name = $name . "[ids]";
                    $column = $col;
                }
                //inace nastavljamo sa relacionim modelom
                else
                {
                    $model = $relations[$col][1]::model();
                }
            }
            else
            {
                $column = $col;
            }
            $fixed_filter = isset($fixed_filter[$col])?$fixed_filter[$col]:[];
        }
        if ($show_filter === true)
        {
            $filter_type = $model->getAttributeFilter($column);
        }
        $labels = $start_model->attributeLabels();        
        if (isset($labels[$path]))
        {           
            $label = $start_model->getAttributeHtml($path);            
        }
        else
        {            
            $label = $model->getAttributeHtml($column);
        }
        
        return [
            'model'=>$model,
            'column'=>$column,
            'label'=>$label,
            'input_name'=>$name,
            'filter_type'=>$filter_type,
            'fixed_filter'=>$fixed_filter
        ];
    }

    /**
     * renderuje filter za prosledjenu kolonu
     * @param Model $model - model
     * @param string $path - kolona ili putanja do kolone
     * @param string $table_id - id tabele u kojoj se nalazi kolona
     * @param array $options - dodatne opcije za filter za tu kolonu(onchange, onkeyup...)
     * @return string
     */
    public static function renderColumnFilter($model, $path, $table_id, $options=[])
    {
        $fixed_filter = isset($options['fixed_filter'])?$options['fixed_filter']:[];
        $column_params = SIMAGuiTable::getColumnParams($model, $path, $fixed_filter);
        $model = $column_params['model'];
        $column = $column_params['column'];
        $label = isset($options['label'])?$options['label']:$column_params['label'];
        $name = $column_params['input_name'];
        $filter_type = isset($options['filter_type'])?$options['filter_type']:$column_params['filter_type'];        
        $onkeyup = isset($options['onkeyup'])?$options['onkeyup']:"$('#" . $table_id . "').simaGuiTable('populate')";
        $onchange = isset($options['onchange'])?$options['onchange']:"$('#" . $table_id . "').simaGuiTable('populate')";
        $column_filter = $column_params['fixed_filter'];     
        $filter_type_params = null;
        if (is_array($filter_type))
        {
            $filter_type_params = array_slice($filter_type, 1);
            $filter_type = isset($filter_type[0])?$filter_type[0]:'';            
        }
              
        switch ($filter_type)
        {
            case 'text': return CHtml::activeTextField($model, $column, array(
                            'name' => $name,
                            'oninput' => $onkeyup,
                            'placeholder' => $label,
                            'value' => $model->$column,
                            'class' => 'sima-guitable-filter-text'
                ));                
            case 'integer': return CHtml::activeTextField($model, $column, array(
                            'name' => $name,
                            'onkeyup' => $onkeyup,
                            'placeholder' => $label,
                            'value' => $model->$column,
                            'class' => 'sima-guitable-filter-text'
                ));
            case 'relation':                
                return Yii::app()->controller->widget('ext.SIMASearchField.SIMASearchField', array(
                            'name' => $name,
                            'model' => $model,
                            'relation' => $column,
                            'onchange' => $onchange,
                            'htmlOptions' => array(
                                'placeholder' => $label,
                            ),
                            'filters' => $column_filter
                       ), true);                
            case 'belongs': return Yii::app()->controller->widget('ext.SIMABelongs.SIMABelongs', array(
                            'name' => $name,
                            'model' => $model,
                            'attribute' => $column,
                            'placeholder' => $label,
                            'onchange' => $onchange,
                       ), true);
            case 'dropdown': case 'boolean':
                if ($filter_type === 'boolean')
                {
                    $_choose_list = $model->droplist($column);                    
                    if (isset($_choose_list[1]) && $_choose_list[1] === 'prazno')
                    {                        
                        $_choose_list = ['0' => Yii::t('BaseModule.Common', 'No'), '1' => Yii::t('BaseModule.Common', 'Yes')];
                    }
                }
                else if ($filter_type_params !== null && isset($filter_type_params['default']))
                {
                    $_choose_list = $filter_type_params['default'];
                }
                else
                {
                    $_choose_list = $model->droplist($column);
                }
                return CHtml::activeDropDownList($model, $column, $_choose_list, array('name' => $name, 'empty' => $label, 'onchange' => $onchange));              
            case 'file_tag':
                return CHtml::activeDropDownList(
                                $model, $column, $model->droplist($column), array(
                            'empty' => 'Pripadnost',
                            'onchange' => $onchange
                        )) . "<span class='file_tag_filter_recursive' onclick='$('#<?php echo $table_id ?>').simaGuiTable('populate')'>R</span>" . CHtml::activeTextField($model, 'belongs_recursive', array(
                            'style' => "display:none;",
                            'value' => 'false',
                ));                
            /**
             * Promenjen je dateformat iz dd.mm.yyyy. u mm-dd-yy jer je imao problem pri pretrazivanju u bazi
             * dodaj jos alternativni nacin prikaza za korisnika.
             */
            case 'date':
            case 'date_range':
            case 'date_time_range':
                return SIMAHtml::dateRange($name, $model->$column, [
                    'htmlOptions'=>[
                        'class' => 'sima-guitable-filter-date_range',
                        'placeholder' => $label
                    ],
                    'onchange' => isset($options['onchange'])?$options['onchange']:['sima.callPluginMethod',"#$table_id",'simaGuiTable','populate'],
                    'readonly' => true
                ]);
            case 'numeric':
                $origin_column = $column;
                if (!empty($filter_type_params) && isset($filter_type_params['origin_column']))
                {
                    $origin_column = $filter_type_params['origin_column'];
                }
                $default_sign = '<=';
                if (!empty($filter_type_params) && isset($filter_type_params['default_sign']))
                {
                    $default_sign = $filter_type_params['default_sign'];
                }
//                return Yii::app()->controller->widget('base.extensions.SIMANumericFilter.SIMANumericFilter', array(
                return Yii::app()->controller->widget(SIMANumericFilter::class, array(
                            'name' => $name,
                            'value' => $model->$column,
                            'onchange' => $onchange,
                            'htmlOptions' => array(
                                'placeholder' => $label,
                            ),
                            'numericInputParams' => SIMAMisc::numberFieldParams($model, $origin_column),
                            'default_sign' => $default_sign
                       ), true);
            case 'numberField':                
                return SIMAHtml::numberField($model, $column);
            default: return $label;
        }
    }

    public function showFiltersIn()
    {
        $show_filters = false;
        foreach ($this->headerOptionRows as $headerOptionRow) 
        {
            foreach ($headerOptionRow as $key => $option)
            {
                if ($option === 'filters_in' && $this->filters_in === true)
                {
                    $show_filters = true;
                }
            }
        }
        
        return $show_filters;
    }
    
    public function showFiltersOut()
    {
        $show_filters = false;
        foreach ($this->headerOptionRows as $headerOptionRow) 
        {
            foreach ($headerOptionRow as $key => $option)
            {
                if ($option === 'filters_out' && $this->filters_out === true)
                {
                    $show_filters = true;
                }
            }
        }
        
        return $show_filters;
    }
    
    private function isColumnAdded($column_name)
    {
        $founded = false;
        
        foreach ($this->columns as $column)
        {
            if ($column['name'] === $column_name)
            {
                $founded = true;
                break;
            }
        }
        
        return $founded;
    }
    
    public function registerManual()
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.simaGuiTable.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/simaGuiTable.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/guiTable.css');
        
        Yii::app()->clientScript->registerScript(
            SIMAHtml::uniqid(),
            "sima.guiTable = new SIMAGuiTableGlobObj();", 
            CClientScript::POS_READY
        );
    }

    public static function renderUsedFiltersHtmlForPdf($used_filters)
    {
        $used_filters_html = '<style type="text/css"> .used-filters {font-size: 8px;} .used-filters-row-title td {
            text-align: left;
            font-size: 9px;
            font-weight: bold;
        }
        .used-filters-row td {
            text-align: left;
            font-size: 8px;
        }</style>';
        $used_filters_html .= '<table class="used-filters" width="100%" cellpadding="2">'
                . '<tr class="used-filters-row-title"><td>Korišćeni filteri:</td></tr>';
        foreach($used_filters as $key=>$value)
        {
            if(is_array($value))
            {
                $had_first = false;
                foreach($value as $key1=>$value1)
                {
                    if(!$had_first)
                    {
                        $used_filters_html .= '<tr class="used-filters-row">
                            <td width="10%">'.$key.':</td>
                            <td width="90%"></td>
                        </tr>';
                        $had_first = true;
                    }
                    $used_filters_html .= '<tr class="used-filters-row">
                        <td width="10%"></td>
                        <td width="90%">'."$key1: $value1".'</td>
                    </tr>';
                }
            }
            else
            {
                $used_filters_html .= '<tr class="used-filters-row"><td colspan="2">'."$key: $value".'</td></tr>';
            }
        }
        $used_filters_html .= '</table>';
        
        return $used_filters_html;
    }
}
