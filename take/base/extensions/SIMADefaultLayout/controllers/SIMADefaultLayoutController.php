<?php

class SIMADefaultLayoutController extends SIMAController
{
    /// MilosJ: nije nadjeno da se koristi
//    public function localRenderPartial($view, $data=NULL, $return=false, $processOutput=false)
//    {
//        $view = 'base.extensions.SIMADefaultLayout.views.defaultLayout.'.$view;
//        
//        if($return)
//        {
//            return parent::renderPartial($view, $data, $return, $processOutput);
//        }
//        else
//        {
//            echo parent::renderPartial($view, $data, $return, $processOutput);
//        }
//    }
    
    public function actionIndex($model_name, $model_id)
    {
        $model = $model_name::model()->findByPkWithWarnCheck($model_id);
        $display_model = $model->getDisplayModel();
        
        if (empty($display_model))
        {
            throw new SIMAWarnException(Yii::t('BaseModule.Common', 'DoNotHaveAccessToInformations'));
        }

        $selector = $this->filter_post_input('selector', false);
        if(empty($selector))
        {
            $selector = [];
        }

        if (gettype($selector) === 'string')
        {
           $selector = CJSON::decode($selector,true);
        }

        $default_selected_tab = [];
        $default_selected = array_shift($selector);
        if (!empty($default_selected) && isset($default_selected['simaTabs']))
        {
            $default_selected_tab = [$default_selected['simaTabs'], 'selector'=>$selector];
        }
        
        $html = $this->widget('SIMADefaultLayout', [
            'model'=>$display_model,
            'default_selected_tab'=>$default_selected_tab
        ], true);
        
        $this->respondOK([
            'title'=>$display_model->DisplayName,
            'html'=>$html
        ]);
    }
    
    public function actionUpdate($model_name, $model_id)
    {
        $model = $model_name::model()->findByPk($model_id);
        $respond_params = [];

        if (!is_null($model))
        {
            $default_layout_id = $this->filter_post_input('default_layout_id', false);
            $fixed_tabs = $this->filter_post_input('fixed_tabs', false);
            $ignore_tabs = $this->filter_post_input('ignore_tabs', false);
            $full_info_model_name = $this->filter_post_input('full_info_model_name', false);
            $full_info_model_id = $this->filter_post_input('full_info_model_id', false);

            //updejtujemo tabove
            $model_tabs = SIMADefaultLayout::getTabs($model, $fixed_tabs, $ignore_tabs);
            SIMATree2::repackTree($model_tabs);
            $respond_params['tabs'] = $model_tabs;
            
            //updejtujemo full info ako se promenio model za koji se prikazuje full info
            if (!empty($full_info_model_name) && !empty($full_info_model_id))
            {
                $full_info_data = SIMADefaultLayout::getFullInfoData($model);
                if ($full_info_data['type'] !== 'model' || ($full_info_data['model'] === 'model' && get_class($full_info_data['value']) !== $full_info_model_name))
                {
                    if($full_info_data['type'] !== 'model')
                    {
                        $full_info_model = $model;
                    }
                    else
                    {
                        $full_info_model = $full_info_data['value'];
                    }
                    
                    $respond_params['full_info'] = SIMADefaultLayout::renderDataFullInfo($model, $full_info_data);
                    if (!empty($default_layout_id))
                    {
                        $script = "$('#{$default_layout_id}').simaDefaultLayout('setFullInfoModel',".CJSON::encode([
                            'model_name' => get_class($full_info_model),
                            'model_id' => $full_info_model->id
                        ]).");";
                        Yii::app()->clientScript->registerScript("setFullInfoModel".SIMAHtml::uniqid(),$script, CClientScript::POS_END);
                    }
                }
            }

            //updejtujemo upozorenja
            if (!empty($default_layout_id))
            {
                Yii::app()->controller->raiseAction('sima.warns.renderWarnsForModel', [$model_name, $model_id, ".sima-default-layout-warns.{$default_layout_id}"], true);
            }
        }
        else
        {
            $respond_params['not_exists'] = true;
        }
        
        $this->respondOK($respond_params);
    }
    
    public function actionGetFullInfoHtml($model_name, $model_id)
    {
        $model = $model_name::model()->findByPk($model_id);
        
        $full_info_html = '';
        if (!is_null($model))
        {
            $full_info_html = SIMADefaultLayout::renderFullInfo($model);
        }
        
        $this->respondOK([
            'html' => $full_info_html
        ]);
    }
}