<?php

class SIMADefaultLayout extends CWidget 
{
    public $id = null;
    public $class = '';
    
    public $model = null;
    
    public $model_name = null;
    public $model_id = null;
    
    public $title = '';
    public $options = [];
    public $basic_info_html = '';
    public $full_info_html = '';
    
    public $tabs = [];
    public $tabs_params = []; //parametri koji se salju SIMATree2 jer SIMATree2 prikazuje tabove
    public $default_selected_tab = []; //zadaje se u obliku ['tab_code', 'selector'=>$selector] - selector(array) je nesto sto bi se selektovalo unutar taba
                                       //npr. ako je tab_code za tab komentari, selektor bi mogao biti da se selektuje neki comment thread unutar taba komentari
    public $ignore_tabs = [];
    
    public $main_proportion = [0.2,0.8];
    public $left_side_proportion = [0.5,0.5];
    
    private $full_info_model = null;
    
    public function run() 
    {
        if (empty($this->id))
        {
            $this->id = SIMAHtml::uniqid();
        }

        //moze da se desi da je prosledjen model, ali je null, jer recimo korisnik nema pravo pristupa fajlu, pa se zato prikazuje genericka poruka korisniku
        if (empty($this->model_name) && empty($this->model_id) && empty($this->model))
        {
            throw new SIMAWarnException(Yii::t('BaseModule.Common', 'DoNotHaveAccessToInformations'));
        }
        
        if ($this->model instanceof BafRequest || $this->model instanceof ServerErrorReport)
        {
            $default_selected_tab_string = CJSON::encode($this->default_selected_tab);
            $ignore_tabs_string = CJSON::encode($this->ignore_tabs);
            $display_model_tag = SIMAMisc::getVueModelTag($this->model);
            //mora da se stavi div, jer vue komponenta ne moze direktno u maintab(ili dialog) posto jos uvek nema izgenerisan html
            echo "
                <sima-model-default-layout class='sima-layout-panel'
                    id='default_layout_{$this->id}' 
                    v-bind:model='model'
                    v-bind:default_selected_tab='$default_selected_tab_string'
                    v-bind:ignore_tabs='$ignore_tabs_string'
                >
                </sima-model-default-layout>
            ";

            Yii::app()->clientScript->registerScript("register_default_layout_{$this->id}", '
                new Vue({
                    el: "#default_layout_'.$this->id.'",
                    data: {
                        model: sima.vue.shot.getters.model("'.$display_model_tag.'")
                    }
                });
                
            ', CClientScript::POS_READY);
        }
        else
        {
            $model_title = '';
            $model_basic_info_html = '';
            $model_full_info_html = '';
            $javascript_params = [];

            if (empty($this->default_selected_tab))
            {
                $this->default_selected_tab = ['default_layout_full_info'];
            }

            if (!empty($this->model))
            {
                $this->model_name = get_class($this->model);
                $this->model_id = $this->model->id;
            }

            $full_info_tab_data = [
                'content_html' => $this->full_info_html
            ];

            if (!empty($this->model_name) && !empty($this->model_id))
            {
                try
                {
                    $this->model = $this->model_name::model()->hasAccess(Yii::app()->user->id)->findByPkWithWarnCheck($this->model_id);
                } 
                catch (SIMAWarnExceptionModelNotFound $e)
                {
                    throw new SIMAWarnException(Yii::t('BaseModule.Common', 'DoNotHaveAccessToInformations'));
                }

                $this->model = $this->model->getDisplayModel();

                $model_title = Yii::app()->controller->renderModelView($this->model, 'title');
                array_push($this->options, [
                    'html' => Yii::app()->controller->renderModelView($this->model, 'options')
                ]);

                $is_full_info_exist = true;
                $model_full_info_html = SIMADefaultLayout::renderFullInfo($this->model, $is_full_info_exist);
                if ($is_full_info_exist === true)
                {
    //                $model_basic_info_html = Yii::app()->controller->renderModelView($this->model, 'basic_info', ['class'=>'basic_info']);
                    $model_basic_info_html = SIMADefaultLayout::getModelBasicInfoHTML($this->model);
                }

                $full_info_tab_data = [
                    'content_action' => [
                        'action' => 'defaultLayout/getFullInfoHtml',
                        'get_params' => [
                            'model_name' => $this->model_name,
                            'model_id' => $this->model_id
                        ]
                    ]
                ];
            }

            //dodajemo na pocetku fiksnih tabova tab osnovne informacije koji prikazuje full info
            array_unshift($this->tabs, [
                'title' => SIMADefaultLayout::getTabTitle(Yii::t('SIMADefaultLayout.SIMADefaultLayout', 'BasicInformationTab'), !empty($this->model)),
                'code' => 'default_layout_full_info',
                'data' => $full_info_tab_data,
                'class' => '_loaded',
                'order' => -1
            ]);

            $javascript_params['fixed_tabs'] = $this->tabs;

            //mergujemo model i fiksne tabove
            $this->tabs = $this->getTabs($this->model, $this->tabs, $this->ignore_tabs);

            if (empty($this->title))
            {
                $this->title = $model_title;
            }
            if (empty($this->basic_info_html))
            {
                $this->basic_info_html = $model_basic_info_html;
            }
            if (empty($this->full_info_html))
            {
                $this->full_info_html = $model_full_info_html;
            }

            $panels = [];
            if(!empty($this->basic_info_html) || !empty($this->tabs))
            {
                $panels[] = [
                    'proportion'=>$this->main_proportion[0],
                    'min_width' => 300,
                    'html' => $this->render('left_panel',[
                        'tabs_html' =>  $this->widget('SIMATree2', array_merge([
                                            'id' => "sima_default_layout_tabs_$this->id",
                                            'class'=>'sdl-tabs sima-vertical-tabs',
                                            'list_tree_data'=>$this->tabs,
                                            'hide_item_without_content'=>true
                                        ], $this->tabs_params), true)
                    ], true),
                    'class' => 'sdl-lp-container'
                ];
            }
            $panels[] = [
                'proportion'=>$this->main_proportion[1],
                'html' => $this->render('right_panel',[], true),
                'class' => 'sdl-rp-container'
            ];

            echo Yii::app()->controller->renderContentLayout(
                [
                    'name'=>$this->title,
                    'class'=>'sdl-title',
                    'options_class'=>'sdl-options',
                    'options' => $this->options
                ],
                $panels,
                [
                    'id' => $this->id,
                    'class'=>"sdl {$this->class}",
                    'processOutput'=>false
                ]
            );

            $javascript_params['ignore_tabs'] = $this->ignore_tabs;
            $javascript_params['model_name'] = $this->model_name;
            $javascript_params['model_id'] = $this->model_id;

            if (!empty($this->default_selected_tab) && is_array($this->default_selected_tab))
            {
                $javascript_params['default_selected_tab_code'] = array_shift($this->default_selected_tab);
                if (isset($this->default_selected_tab['selector']))
                {
                    $javascript_params['default_selected_tab_selector'] = $this->default_selected_tab['selector'];
                }
            }

            if (!empty($this->full_info_model))
            {
                $javascript_params['full_info_model_name'] = get_class($this->full_info_model);
                $javascript_params['full_info_model_id'] = $this->full_info_model->id;
            }

            $javascript_params_json = CJSON::encode($javascript_params);
            $register_script = "$('#$this->id').simaDefaultLayout('init',$javascript_params_json);";
            Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
        }
    }
    
    public static function renderDataFullInfo(SIMAActiveRecord $model, array $max_priority_fullinfo_data, &$is_full_info_exist=true)
    {
        try
        {
            if($max_priority_fullinfo_data['type'] === 'model')
            {
                $return = Yii::app()->controller->renderModelView($max_priority_fullinfo_data['value'], 'full_info', ['class'=>'sima-full-info sima-layout-panel']);
            }
            else if($max_priority_fullinfo_data['type'] === 'view')
            {
                $return = Yii::app()->controller->renderPartial($max_priority_fullinfo_data['value'], [
                    'class' => 'sima-full-info sima-layout-panel',
                    'model' => $model
                ], true, false);
            }
            else
            {
                throw new SIMAException('should not be here');
            }
        } 
        catch (SIMAModelViewNotFound $ex)
        {
            if ($ex->getViewName() === 'full_info' && $max_priority_fullinfo_data['type'] === 'model' && $ex->getModelName() === get_class($model))
            {
                $is_full_info_exist = false;
                try
                {
//                    $return = Yii::app()->controller->renderModelView($model, 'basic_info', ['class'=>'basic_info']);
                    $return = SIMADefaultLayout::getModelBasicInfoHTML($model);
                }
                catch (SIMAModelViewNotFound $ex)
                {
                    throw new SIMAException('TODO za sastanak - nije pronadjen basic_info prilikom pokusaja da se renderuje full_info'
                            . ', da li nova greska ili izmenjen tekst?');
                }
            }
            else
            {
                throw $ex;
            }
        }
        
        return $return;
    }

    public static function getTabs($model, $fixed_tabs=[], $ignore_tabs=[])
    {
        $_fixed_tabs = !empty($fixed_tabs) ? $fixed_tabs : [];
        $_ignore_tabs = !empty($ignore_tabs) ? $ignore_tabs : [];

        $new_fixed_tabs = [];
        foreach ($_fixed_tabs as $fixed_tab)
        {
            if (!in_array($fixed_tab['code'], $_ignore_tabs))
            {
                array_push($new_fixed_tabs, $fixed_tab);
            }
        }

        $model_tabs = [];
        if (!empty($model))
        {
            $model_tabs = SIMADefaultLayout::getModelTabs($model, $_ignore_tabs);
        }
        
        return array_merge($new_fixed_tabs, $model_tabs);
    }
    
    private static function getModelTabs($model, $ignore_tabs=[])
    {
        $model_tabs = [];
        if($model->hasTabs)
        {
            $ModelTabs = $model->TabsBehavior;
            $model->attachBehavior('Tabs', new $ModelTabs());
            $model_tabs = $model->listTabs();
        }

        $tabs = [];

        foreach ($model_tabs as $tab)
        {
            if (!in_array($tab['code'], $ignore_tabs))
            {
                SIMADefaultLayout::repackTabParams($tab);
                $tabs[] = $tab;
            }
        }
        
        return $tabs;
    }
    
    private static function repackTabParams(&$tab, $has_refresh = true)
    {
        if (is_string($tab['title']))
        {
            $tab['title'] = SIMADefaultLayout::getTabTitle($tab['title'], $has_refresh);
        }

        if (isset($tab['action']))
        {
            $tab['data'] = [];
            $tab['data']['content_action'] = [];
            $tab['data']['content_action']['action'] = $tab['action'];
            $tab['data']['content_action']['get_params'] = [];
            $tab['data']['content_action']['post_params'] = [];
            if (isset($tab['get_params']) && is_array($tab['get_params']))
            {
                foreach ($tab['get_params'] as $get_param_key=>$get_param_value)
                {
                    $tab['data']['content_action']['get_params'][$get_param_key] = $get_param_value;
                }
                unset($tab['get_params']);
            }
            if (isset($tab['params']) && is_array($tab['params']))
            {
                foreach ($tab['params'] as $param_key=>$param_value)
                {
                    $tab['data']['content_action']['post_params'][$param_key] = $param_value;
                }
                unset($tab['params']);
            }
            unset($tab['action']);
        }

        if (isset($tab['subtree']))
        {
            $_subtree = [];
            foreach ($tab['subtree'] as $value) 
            {
                SIMADefaultLayout::repackTabParams($value, $has_refresh);
                array_push($_subtree, $value);
            }
            $tab['subtree'] = $_subtree;
        }
    }
    
    public static function getFullInfoData($model)
    {
        $event = new SIMAEvent();
        $model->onGetFullInfo($event);

        $max_priority = 0;
        $max_priority_fullinfo_data = [
            'type' => 'model',
            'value' => $model
        ];
        foreach ($event->getResults() as $result)
        {
            foreach ($result as $value)
            {
                if(!is_array($value))
                {
                    throw new SIMAException('must be array');
                }

                if(!isset($value['priority']))
                {
                    throw new SIMAException('priority not set');
                }

                if(!isset($value['model']) && !isset($value['view']))
                {
                    throw new SIMAException('model or view must be set');
                }

                $curr_priority = intval($value['priority']);
                if ($curr_priority === $max_priority)
                {
                    throw new SIMAException('Na dva mesta ste zadali full info sa istim prioritetom.');
                }
                if ($curr_priority > $max_priority)
                {
                    $max_priority = $curr_priority;

                    if(isset($value['model']))
                    {
                        $max_priority_fullinfo_data = [
                            'type' => 'model',
                            'value' => $value['model']
                        ];
                    }
                    else if(isset($value['view']))
                    {
                        $max_priority_fullinfo_data = [
                            'type' => 'view',
                            'value' => $value['view']
                        ];
                    }
                    else
                    {
                        throw new SIMAException('should not be here');
                    }
                }
            }
        }

        return $max_priority_fullinfo_data;
    }
    
    private static function getTabTitle($title, $has_refresh = true)
    {
        $new_title = [
            'text' => $title
        ];
        
        if ($has_refresh === true)
        {
            $new_title['parts'] = [
                [
                    'class'=>'sdl-tab-refresh sima-icon _refresh _white _16',
                    'position'=>'after'
                ]
            ];
        }
        
        return $new_title;
    }
    
    private static function getModelBasicInfoHTML($model)
    {
        return Yii::app()->controller->renderModelView($model, 'basic_info', ['class'=>'basic_info']);
    }
    
    public static function renderFullInfo(SIMAActiveRecord $model, &$is_full_info_exist=true)
    {
        $full_info_data = SIMADefaultLayout::getFullInfoData($model);
        $model_full_info_html = SIMADefaultLayout::renderDataFullInfo($model, $full_info_data, $is_full_info_exist);
        return $model_full_info_html;
    }
    
    public function registerManual() 
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.simaDefaultLayout.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/simaDefaultLayoutEventHandler.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaDefaultLayout.css');
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaVerticalTabs.css');
    }
}
