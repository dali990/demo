/* global sima */

(function($){
    var methods = {
        init: function(options)
        {
            var _default_layout = $(this);
            var tabs_selector = _default_layout.find('.sdl-tabs');
            
            _default_layout.data('tabs_selector', tabs_selector);
            
            _default_layout.data('full_info_model_name', (typeof options.full_info_model_name !== 'undefined') ? options.full_info_model_name : null);
            _default_layout.data('full_info_model_id', (typeof options.full_info_model_id !== 'undefined') ? options.full_info_model_id : null);
            
            if (typeof options.model_name !== 'undefined')
            {
                _default_layout.data('model_name', options.model_name);
            }
            if (typeof options.model_id !== 'undefined')
            {
                _default_layout.data('model_id', options.model_id);
            }
            if (typeof options.default_selected_tab_code !== 'undefined')
            {
                _default_layout.data('default_selected_tab_code', options.default_selected_tab_code);
            }
            if (typeof options.default_selected_tab_selector !== 'undefined')
            {
                _default_layout.data('default_selected_tab_selector', options.default_selected_tab_selector);
            }
            if (typeof options.fixed_tabs !== 'undefined')
            {
                _default_layout.data('fixed_tabs', options.fixed_tabs);
            }
            if (typeof options.ignore_tabs !== 'undefined')
            {
                _default_layout.data('ignore_tabs', options.ignore_tabs);
            }

            tabs_selector.on('loadItem', function(event, item, data) {

                if (sima.isEmpty(item))
                {
                    return;
                }

                var tab_content_html = '';
                var item_data = item.data();
                var item_data_code = item_data.code;

                //prvo proveravamo da li je setovan content_html
                if (typeof data !== 'undefined' && !sima.isEmpty(data.content_html)) //slucaj kada je poslat iz php prilikom loadovanja item-a
                {
                    tab_content_html = data.content_html;
                } 
                else if (!sima.isEmpty(item_data.content_html)) //slucaj ako je inicijalno zadat u item data
                {
                    tab_content_html = item_data.content_html;
                }

                if (tab_content_html !== '')
                {
                    appendTabContentHtml(_default_layout, item_data_code, tab_content_html);
                }
                else if (!sima.isEmpty(item_data.content_action)) //ako je inicijalno zadata akcija u item data za dovlacenje kontenta
                {
                    appendTabContentHtml(_default_layout, item_data_code, '');
                    var content_action = item_data.content_action;
                    var get_params = !sima.isEmpty(content_action.get_params) ? content_action.get_params : {};
                    var post_params = !sima.isEmpty(content_action.post_params) ? content_action.post_params : {};
                    var content_html = _default_layout.find('.sdl-rp-tab-content[data-code="'+item_data_code+'"]');

                    if (!sima.isEmpty(_default_layout.data('default_selected_tab_selector')))
                    {
                        post_params.selector = _default_layout.data('default_selected_tab_selector');
                    }

                    sima.ajax.get(content_action.action,{
                        get_params:get_params,
                        data:post_params,
                        async:true,
                        loadingCircle: (!sima.isEmpty(content_html) && content_html.is(':visible')) ? content_html : false,
                        success_function:function(response)
                        {
                            appendTabContentHtml(_default_layout, item_data_code, response.html);
                        }
                    });
                }
            });
            tabs_selector.on('selectItem', function(event, item) {
                onSelectTab(_default_layout, item);
            });
            tabs_selector.on('unselectItem', function(event, item) {
                var full_info_tab = _default_layout.find('.sima-tree-list .sima-tree-item[data-code="default_layout_full_info"]');
                
                full_info_tab.addClass('_selected');
                onSelectTab(_default_layout, full_info_tab);
            });
            tabs_selector.on('click', '.sdl-tab-refresh', function(event){
                refreshSelectedTabContent(_default_layout, $(this).parents('.sima-tree-item:first'));
                event.stopPropagation();
            });
            
            //inicijalno selektovanje taba
            if (!sima.isEmpty(_default_layout.data('default_selected_tab_code')))
            {
                tabs_selector.simaTree2('selectItem', _default_layout.data('default_selected_tab_code'));
            }
        },
        refreshSelectedTabContent: function()
        {
            var _default_layout = $(this);
            var selected_tab = getSelectedTab(_default_layout);
            refreshSelectedTabContent(_default_layout, selected_tab);
        },
        update: function()
        {
            var _default_layout = $(this);

            if (!sima.isEmpty(_default_layout.data('model_name')) && !sima.isEmpty(_default_layout.data('model_id')))
            {
                //updejtujemo tabove ako su to tabovi modela, inace nema potrebe, jer su oni prosledjeni samo inicijalno i vise se nece menjati
                //updejtujemo model view full info(ovo mora da se updejtujemo posebno, a ne preko model view, jer je moguce da ce se prikazivati drugi full info
                //npr. kada se fajl zavede, moguce je da zavodni broj ima svoj full info sa vecim prioritetom i on ce zamniti trenutni full info fajla)
                sima.ajax.get('defaultLayout/update',{
                    get_params:{
                        model_name: _default_layout.data('model_name'),
                        model_id: _default_layout.data('model_id')
                    },
                    data: {
                        fixed_tabs: _default_layout.data('fixed_tabs'),
                        ignore_tabs: _default_layout.data('ignore_tabs'),
                        default_layout_id: _default_layout.attr('id'),
                        full_info_model_name: _default_layout.data('full_info_model_name'),
                        full_info_model_id: _default_layout.data('full_info_model_id')
                    },
                    async:true,
                    success_function:function(response)
                    {
                        if (typeof response.not_exists !== 'undefined' && response.not_exists === true)
                        {
                            _default_layout.find('.sima-tree-list').empty();
                            _default_layout.find('.sdl-rp').empty();
                        }
                        else
                        {
                            if (typeof response.tabs !== 'undefined')
                            {
                                var model_tabs = response.tabs;

                                _default_layout.data('tabs_selector').simaTree2('reloadTree', model_tabs);

                                $.each(model_tabs, function(index, model_tab) {
                                    if (
                                            typeof model_tab.reload_on_update !== 'undefined' && model_tab.reload_on_update === true && 
                                            _default_layout.find('.sdl-rp-tab-content[data-code="'+model_tab.code+'"]').length > 0
                                        )
                                    {
                                        _default_layout.data('tabs_selector').trigger('loadItem', [getTabByCode(_default_layout, model_tab.code)]);
                                    }
                                });
                            }
                            if (typeof response.full_info !== 'undefined')
                            {
                                _default_layout.find('.sdl-rp-fi').html(response.full_info);
                            }
                        }
                        sima.layout.allignObject(_default_layout.parent(),'TRIGGER_on_default_layout_update');
                    }
                });
            }
        },
        setFullInfoModel: function(model_params)
        {
            var _default_layout = $(this);
            if (typeof model_params.model_name !== 'undefined')
            {
                _default_layout.data('full_info_model_name', model_params.model_name);
            }
            if (typeof model_params.model_id !== 'undefined')
            {
                _default_layout.data('full_info_model_id', model_params.model_id);
            }
        },
        selectTabByCode: function(tab_code)
        {
            var default_layout = $(this);
            var tabs_selector = default_layout.find('.sdl-tabs');
            
            tabs_selector.simaTree2('selectItem', tab_code);
        },
        getTabByCode: function(tab_code)
        {
            var default_layout = $(this);
            return getTabByCode(default_layout, tab_code);
        }
    };
    
    function refreshSelectedTabContent(_default_layout, tab)
    {
        if (!sima.isEmpty(tab))
        {
            var tabs_selector = _default_layout.data('tabs_selector');
            tabs_selector.trigger('loadItem', [tab]);
        }
    }
    
    function getSelectedTab(_default_layout)
    {
        var tabs_selector = _default_layout.data('tabs_selector');
        
        return tabs_selector.find('.sima-tree-item._selected');
    }
    
    function getTabByCode(_default_layout, tab_code)
    {
        return _default_layout.data('tabs_selector').simaTree2('getTreeItemByCode', tab_code);
    }
    
    function onSelectTab(_default_layout, item)
    {
        var item_data = item.data();
        var tab_code = item_data.code;
        
        _default_layout.find('.sima-tree-list .sima-tree-item').each(function(){
            if ($(this).data('code') !== tab_code)
            {
                $(this).removeClass('_selected');
            }
        });
        
        //skrivamo sve content-e i refresh ikonicu za tabove
        _default_layout.find('.sdl-rp-tab-content').hide();
        _default_layout.find('.sdl-tab-refresh').hide();
        
        //prikazuje refresh ikonicu za selektovani tab
        item.find('.sdl-tab-refresh:first').show();
        item.find('.sdl-tab-refresh:first').css('display','inline-block');

        var tab_content = _default_layout.find('.sdl-rp-tab-content[data-code="'+tab_code+'"]');
        if (tab_content.length > 0)
        {
            tab_content.show();
            tab_content.children().trigger('onShow');
        }
        
        _default_layout.trigger('sima-layout-allign', [_default_layout.height(), _default_layout.width(), 'TRIGGER_on_select_tab']);
    }
    
    function appendTabContentHtml(_default_layout, tab_code, tab_html)
    {
        var tab_content = _default_layout.find('.sdl-rp-tab-content[data-code="'+tab_code+'"]');
        if (tab_content.length === 0)
        {
            _default_layout.find('.sdl-rp').append('<div class="sdl-rp-tab-content sima-layout-panel" data-code="'+tab_code+'">'+tab_html+'</div>');
        }
        else
        {
            tab_content.html(tab_html);
        }
        sima.layout.allignObject(_default_layout,'TRIGGER_appendTabContentHtml');
    }
    
    $.fn.simaDefaultLayout = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.simaDefaultLayout');
            }
        });
        return ret;
    };
})( jQuery );
