
/* global sima */

$('body').on('sima-layout-allign', '.sdl-rp', function(event, height, width, source){

    if (event.target === this)
    {
        if (sima.layout.log())
        {
            console.log(source);
            console.log('HANDLER_sima_default_layout_simaresize');
        }

        var _default_layout = $(this);
        _default_layout.height(height);
        _default_layout.width(width);
        
        var fixed_height = 0;
        _default_layout.children('.sima-layout-fixed-panel').each(function(){
            fixed_height += $(this).outerHeight(true);
        });
        var _panels_height = height - fixed_height;
        _default_layout.children('.sdl-rp-tab-content').each(function(){
            var _panel = $(this);
            _panel.height(_panels_height);
            _panel.width(width);
        });

        var selected_tab = _default_layout.children('.sdl-rp-tab-content:visible');
        sima.layout.setLayoutSize(
                selected_tab,
                _panels_height,
                width,
                'TRIGGER_sima_default_layout_resize'
        );

        event.stopPropagation();
    }
});