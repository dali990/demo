<?php $uniq = SIMAHtml::uniqid(); ?>

<div class="sdl-rp sima-layout-panel _horizontal">
    <sima-action-notifications
        id="<?="action_notifications_$uniq"?>"
        class="sima-default-layout-notifs sima-layout-fixed-panel"
        v-bind:actions="actions"
    ></sima-action-notifications>
    <div class="sima-default-layout-warns sima-layout-fixed-panel <?php echo $this->id; ?>">

    </div>
    <div class="sdl-rp-fi sdl-rp-tab-content sima-layout-panel <?php echo (!empty($this->model) ? SIMAHtml::getTag($this->model): '') ?>" 
         data-code="default_layout_full_info">
        <?php
            echo $this->full_info_html;
        ?>
    </div>
</div>

<?php 
    if (!empty($this->model))
    {
        Yii::app()->controller->raiseAction('sima.warns.renderWarnsForModel', [$this->model_name, $this->model_id, ".sima-default-layout-warns.{$this->id}"], true);
        
        Yii::app()->clientScript->registerScript("action_notifications_script_$uniq", "
            new Vue({
                el: '#action_notifications_$uniq',
                data: {
                    actions: [
                        {
                            action: '".$this->model->getDisplayAction()."',
                            action_id: {
                                model_name: '".$this->model_name."',
                                model_id: ".$this->model_id."
                            }
                        },
                        {
                            action: '".$this->model->path2."',
                            action_id: ".$this->model_id."
                        }
                    ]
                }
            })
        ", CClientScript::POS_READY);
    }

?>