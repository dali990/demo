
<?php 
    $has_splitter = (!empty($this->basic_info_html) && !empty($this->tabs)) ? true : false;
?>
<div class="sdl-lp sima-layout-panel _horizontal <?php echo $has_splitter?'_splitter':''; ?>">
    <?php if (!empty($this->basic_info_html)) { ?>
        <div class="sdl-bi sima-layout-panel"
            <?php if ($has_splitter === true) { ?>
                data-sima-layout-init='<?=CJSON::encode([
                    'proportion' => $this->left_side_proportion[0],
                ])?>'
            <?php } ?>
        >
            <?php
                echo $this->basic_info_html;
            ?>
        </div>
    <?php } ?>
    <?php if (!empty($this->tabs)) { ?>
        <div class="sdl-tabs-container sima-layout-panel"
            <?php if ($has_splitter === true) { ?>
                data-sima-layout-init='<?=CJSON::encode([
                    'proportion' => $this->left_side_proportion[1],
                ])?>'
            <?php } ?>
        >
            <?php
                echo $tabs_html;
            ?>
        </div>
    <?php } ?>
</div>



