<?php

$this->widget('SIMAGuiTable', [
    'id' => "partner_lists_$uniq_id",
    'model' => PartnerList::model(),
    'title' => !empty($title) ? $title : '',
    'add_button' => [
        'init_data' => [
            'PartnerList' => [
                'owner_id' => Yii::app()->user->id
            ]
        ]
    ],
    'fixed_filter' => [
        'filter_scopes' => 'withUserAccessToList'
    ],
    'setRowSelect' => ["sima.addressbook.onPartnerListSelect", "$uniq_id", $related_model_name],
    'setRowUnSelect' => ["sima.addressbook.onPartnerListUnSelect", "$uniq_id", $related_model_name],
    'setMultiSelect' => ["sima.addressbook.onPartnerListUnSelect", "$uniq_id", $related_model_name]
]);

?>

<script type="text/javascript">
    $(function() {
        $('#related_model_<?=$uniq_id?>').hide();
    });
</script>