<?php

$this->widget('SIMAGuiTable', [
    'id' => "related_model_$uniq_id",
    'model' => $related_model_name::model(),
    'title' => !empty($title) ? $title : '',
    'columns_type' => 'from_partner_list',
    'add_button' => false,
    'fixed_filter' => [
        'ids' => -1
    ]
]);

