<div class='addressbook_persons sima-addressbook-results-list-wrapper sima-layout-panel _horizontal' id="<?php echo $persons_id; ?>">
    <div class="sima-layout-fixed-panel">
        <div class="sima-addressbook-main-header-persons _full-width sima-addressbook-capital-letters">
            <div class="sima-addressbook-subheader sima-addressbook-subheader-caption  sima-addressbook-capital-letters">
                <b><?php echo Yii::t('SIMAAddressbook.SIMAAddressbook', 'Persons'); ?></b>
            </div>
        </div>
        <div class="sima-addressbook-subheader-add-button">
            <?php
            if (!empty($company_id) && intval($company_id) === Yii::app()->company->id)
            {
                $company_id = null;
            }
            if(SIMAMisc::isVueComponentEnabled())
            {
                $this->widget(SIMAButtonVue::class,[
                    'no_border' => true,
                    'transparent_background' => true,
                    'half' => true,
                    'tooltip'=> Yii::t('SIMAAddressbook.SIMAAddressbook', 'AddPerson'),
                    'icon' => '_add',
                    'icon_additional_classes' => ['_black'],
                    'iconsize' => 16,
                    'onclick'=>["sima.callPluginMethod", "#{$widget_id}", "simaAddressbook", "onAddNewPerson", [
                        'id_persons'=> $persons_id, 
                        'widget_id'=>$widget_id, 
                        'company_id'=>$company_id,
                        'search_id' => $search_id
                        ]
                    ],
                ]);
            }
            else
            {
                $this->widget('base.extensions.SIMAButton.SIMAButton',[
                    'class' => ' _sima-addr-button-no-border',
                    'action'=>[
                        'tooltip'=> Yii::t('SIMAAddressbook.SIMAAddressbook', 'AddPerson'),
                        'icon'=>'sima-icon _add _black _16',
                        'onclick'=>["sima.callPluginMethod", "#{$widget_id}", "simaAddressbook", "onAddNewPerson", [
                            'id_persons'=> $persons_id, 
                            'widget_id'=>$widget_id, 
                            'company_id'=>$company_id,
                            'search_id' => $search_id
                            ]
                        ],
                    ]
                ]);
            }
            ?>
        </div>
    </div>
    
    <div class="sima-addressbook-results-list addressbook_persons_list sima-layout-panel" >
    <?php 
    if (!empty($html_persons))
    {
        echo $html_persons;
    }
    ?>
    </div>
    
    <div class='sima-addressbook-footer sima-layout-fixed-panel'>
    <?php   
    $this->widget('SIMAPagination', array(
        'id' => $persons_pagination_id,
        'totalDataCount' => $count_persons,
        'pageSize'=>20
    ));
    ?>
    </div>
</div>