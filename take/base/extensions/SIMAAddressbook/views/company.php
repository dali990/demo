<div class='sima-addressbook addressbook_companies sima-addressbook-results-list-wrapper sima-layout-panel _horizontal' id="<?php echo $companies_id; ?>">
    <div class="sima-layout-fixed-panel">
        <div class="sima-addressbook-main-header-companies _full-width">
            <div class="sima-addressbook-subheader sima-addressbook-subheader-caption  sima-addressbook-capital-letters">
                <b><?php echo Yii::t('SIMAAddressbook.SIMAAddressbook', 'Companies'); ?></b>
            </div>
        </div>
        <div class="sima-addressbook-subheader-add-button">
            <?php
            if(SIMAMisc::isVueComponentEnabled())
            {
                $this->widget(SIMAButtonVue::class,[
                    'no_border' => true,
                    'transparent_background' => true,
                    'half' => true,
                    'tooltip'=> Yii::t('SIMAAddressbook.SIMAAddressbook', 'AddCompany'),
                    'icon' => '_add',
                    'icon_additional_classes' => ['_black'],
                    'iconsize' => 16,
                    'onclick'=>["sima.callPluginMethod", "#{$this->id}", "simaAddressbook", "onAddNewCompany", 
                        [
                            'id_companies'=> $companies_id, 
                            'widget_id'=>$this->id, 
                            'search_id'=>$search_id
                        ]
                    ],
                ]);
            }
            else
            {
                $this->widget('SIMAButton',[
                    'id'=>$add_new_company_btn,
                    'class' => ' _sima-addr-button-no-border',
                    'action'=>[
                        'tooltip'=> Yii::t('SIMAAddressbook.SIMAAddressbook', 'AddCompany'),
                        'icon'=>'sima-icon _add _black _16',
                        'onclick'=>["sima.callPluginMethod", "#{$this->id}", "simaAddressbook", "onAddNewCompany", 
                            [
                                'id_companies'=> $companies_id, 
                                'widget_id'=>$this->id, 
                                'search_id'=>$search_id
                            ]
                        ],
                    ]
                ]);
            }
            ?>
        </div>
    </div>
    <div class='sima-addressbook-results-list addressbook_companies_list sima-layout-panel'>
    </div>
    <div class="sima-addressbook-footer sima-layout-fixed-panel">
    <?php
    $count_companies = Company::model()->count();
    $this->widget('SIMAPagination', array(
        'id' => $companies_pagination_id,
        'totalDataCount' => $count_companies,
        'pageSize'=>20
    ));
    ?>
    </div>
</div>
