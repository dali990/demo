<?php

class SIMAAddressbookComponent
{
    public static function getAddressbookHtmlPartner_contactWidgetParams(Partner $partner)
    {        
        $addresses_informations = SIMAAddressbookComponent::getAddressbookHtmlPartner_contactWidgetParams_addresses($partner);
        $phone_informations = SIMAAddressbookComponent::getAddressbookHtmlPartner_contactWidgetParams_phones($partner);
        $email_informations = SIMAAddressbookComponent::getAddressbookHtmlPartner_contactWidgetParams_emails($partner);

        $have_extended_informations = (
                $addresses_informations['include_extended_informations'] 
                || 
                $phone_informations['include_extended_informations']
                || 
                $email_informations['include_extended_informations']
        );
        
        $contact_basic_informations = [
            $addresses_informations['basic_informations'],
            $phone_informations['basic_informations'],
            $email_informations['basic_informations']
        ];
        $contact_expanded_informations = [];
        if($have_extended_informations)
        {
            $contact_expanded_informations = array_merge(
                    $addresses_informations['extended_informations'],
                    $phone_informations['extended_informations'],
                    $email_informations['extended_informations']
            );
        }
        
        return [
            'contact_basic_informations' => $contact_basic_informations,
            'contact_expanded_informations' => $contact_expanded_informations
        ];
    }
    
    public static function getAddressbookHtmlPartner_contactWidgetParams_addresses(Partner $partner)
    {
        $partner_to_addresses = $partner->partner_to_address;
        $addresses_count = count($partner_to_addresses);

        $have_extended_informations = false;
        $address_basic_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Address'),
            'value' => ''
        ];
        $address_extended_informations = [];

        if($addresses_count > 0)
        {
            $pre_value = null;
            if($addresses_count>1)
            {
                $pre_value = '<b>('.$addresses_count.')</b>';
                $have_extended_informations = true;
            }
            
            $address_to_display = $partner->getmain_or_any_address();
            $address_basic_informations = SIMAAddressbookComponent::getAddressbookHtmlPartner_contactWidgetParams_singleAddress($address_to_display, $pre_value);
            
            foreach($partner_to_addresses as $partner_to_address)
            {
                $address_extended_informations[] = SIMAAddressbookComponent::getAddressbookHtmlPartner_contactWidgetParams_singleAddress($partner_to_address->address);
            }
        }
        
        return [
            'basic_informations' => $address_basic_informations,
            'extended_informations' => $address_extended_informations,
            'include_extended_informations' => $have_extended_informations
        ];
    }
    
    public static function getAddressbookHtmlPartner_contactWidgetParams_singleAddress(Address $address, $pre_value=null)
    {
        $result = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Address'),
            'value' => $address->DisplayName,
            'pre_value' => $pre_value,
            'copy_button_value' => $address->DisplayNameLong
        ];
        
        return $result;
    }
    
    public static function getAddressbookHtmlPartner_contactWidgetParams_phones(Partner $partner)
    {
        $have_extended_informations = false;
        $basic_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Telephone'),
            'value' => ''
        ];
        $extended_informations = [];
        
        $phone_numbers = $partner->phone_numbers;
        $phone_numbers_count = count($phone_numbers);
        
        if($phone_numbers_count > 0)
        {
            $pre_value = null;
            if($phone_numbers_count>1)
            {
                $pre_value = '<b>('.$phone_numbers_count.')</b>';
                $have_extended_informations = true;
            }
            
            $any_or_main_phone = $partner->getmain_or_any_phone_contact();
            $copy_button_value = '';
            $any_or_main_phone_display = '';
            if(!empty($any_or_main_phone))
            {
                $any_or_main_phone_display = $any_or_main_phone->DisplayName;
                $copy_button_value = $any_or_main_phone->display_name;                
            }
            
            $basic_informations = SIMAAddressbookComponent::getAddressbookHtmlPartner_contactWidgetParams_singleContact(
                $any_or_main_phone_display, 
                Yii::t('SIMAAddressbook.SIMAAddressbook', 'Telephone'), 
                $pre_value,
                $copy_button_value
            );
            
            foreach($phone_numbers as $phone_number)
            {
                $extended_informations[] = SIMAAddressbookComponent::getAddressbookHtmlPartner_contactWidgetParams_singleContact(
                    $phone_number->DisplayName, 
                    Yii::t('SIMAAddressbook.SIMAAddressbook', 'Telephone'),
                    null,
                    $phone_number->display_name
                );
            }
        }
        
        return [
            'basic_informations' => $basic_informations,
            'extended_informations' => $extended_informations,
            'include_extended_informations' => $have_extended_informations
        ];
    }
    
    /**
     * 
     * @param string $value
     * @param string $label
     * @param string $pre_value
     * @return array
     */
    public static function getAddressbookHtmlPartner_contactWidgetParams_singleContact($value, $label, $pre_value = null, $copy_button_value = null)
    {
        if (empty($copy_button_value))
        {
            $copy_button_value = $value;
        }

        $result = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => $label,
            'value' => $value,
            'pre_value' => $pre_value,
            'copy_button_value' => $copy_button_value
        ];
        
        return $result;
    }
    
    public static function getAddressbookHtmlPartner_contactWidgetParams_emails(Partner $partner)
    {
        $have_extended_informations = false;
        $basic_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Email'),
            'value' => ''
        ];
        $extended_informations = [];
        
        $email_addresses = $partner->email_addresses;
        $emails_address_count = count($partner->email_addresses);
        
        if($emails_address_count > 0)
        {
            $pre_value = null;
            if($emails_address_count>1)
            {
                $pre_value = '<b>('.$emails_address_count.')</b>';
                $have_extended_informations = true;
            }
            
            $main_or_any_email_display = $partner->getmain_or_any_email_display();
            $basic_informations = SIMAAddressbookComponent::getAddressbookHtmlPartner_contactWidgetParams_singleContact($main_or_any_email_display, Yii::t('SIMAAddressbook.SIMAAddressbook', 'Email'), $pre_value);
            
            foreach($email_addresses as $email_address)
            {
                $extended_informations[] = SIMAAddressbookComponent::getAddressbookHtmlPartner_contactWidgetParams_singleContact($email_address->DisplayName, Yii::t('SIMAAddressbook.SIMAAddressbook', 'Email'));
            }            
        }
        
        return [
            'basic_informations' => $basic_informations,
            'extended_informations' => $extended_informations,
            'include_extended_informations' => $have_extended_informations
        ];
    }
    
    public static function getAddressbookHtmlPerson_businessWidgetParams(Person $person)
    {
        $other_basic_informations = SIMAAddressbookComponent::getAddressbookHtmlPerson_otherBasicInformations($person);
        $other_expanded_informations = SIMAAddressbookComponent::getAddressbookHtmlPerson_otherExpandedInformations($person);
        $banks_informations = SIMAAddressbookComponent::getAddressbookHtmlPartner_businessWidgetParams_banks($person->partner);

        $have_extended_informations = (
                $other_expanded_informations['include_extended_informations']
                ||
                $banks_informations['include_extended_informations']
        );
        
        $business_basic_informations = array_merge(
                $other_basic_informations,
                [
                    $banks_informations['basic_informations']
                ]
        );
        $business_expanded_informations = [];
        if($have_extended_informations === true)
        {
            $business_expanded_informations = array_merge(
                    $other_expanded_informations['extended_informations'],
                    $banks_informations['extended_informations']
            );
        }
        
        return [
            'business_basic_informations' => $business_basic_informations,
            'business_expanded_informations' => $business_expanded_informations
        ];
    }
    
    public static function getAddressbookHtmlPartner_businessWidgetParams_banks(Partner $partner)
    {
        $have_extended_informations = false;
        $basic_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Banks'),
            'value' => ''
        ];
        $extended_informations = [];
        
        $model_bank_accounts = $partner->bank_accounts;
        
        if(count($model_bank_accounts) > 0)
        {
            $have_extended_informations = true;
        }
        
        $new_value = '';
        $parsed_banks = [];
        foreach ($model_bank_accounts as $bank_account)
        {
            $bank_company = $bank_account->bank->company;
            
            if(!in_array($bank_company->id, $parsed_banks))
            {
                $parsed_banks[] = $bank_company->id;
                $new_value .= "<div class=\"sima-b-info-additional-icons _small-indent\">"
                        ."<img class=\"sima-icon sima-b-info-image-icon\" "
                            . "src=\"".$bank_company->getAttributeDisplay('image_id')."\" "
                            . "title=\"".$bank_company->DisplayName."\" "
                            . " onclick=\"sima.dialog.openModel('".$bank_company->path2."', '".$bank_company->id."');\" "
                        . " />"
                . "</div>";              
//                $new_value = Yii::app()->controller->widget(SIMAButton::class, [
//                        'action'=>[ 
//                            'title' => 'test',
//                            'tooltip' => $bank_company->DisplayName,
//                            'onclick'=>['sima.dialog.openModel', $bank_company->path2, $bank_company->id]
//                        ]
//                    ], true);
            }
            
            $extended_informations[] = [
                'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
                'label' => $bank_account->bank->short_name,
                'value' => $bank_account->number,
                'pre_value' => "<div class=\"sima-b-info-additional-icons _small-indent\">"
                            ."<img class=\"sima-icon sima-b-info-image-icon\" "
                                . "src=\"".$bank_company->getAttributeDisplay('image_id')."\" "
                                . "title=\"".$bank_company->DisplayName."\" "
                                . " onclick=\"sima.dialog.openModel('".$bank_company->path2."', '".$bank_company->id."');\" "
                            . " />"
                    . "</div>",
                'copy_button_value' => $bank_account->number
            ];
        }
        
        if(!empty($new_value))
        {
            $basic_informations['value'] = $new_value;
        }
        
        return [
            'basic_informations' => $basic_informations,
            'extended_informations' => $extended_informations,
            'include_extended_informations' => $have_extended_informations
        ];
    }
    
    public static function getAddressbookHtmlPerson_otherBasicInformations(Person $person)
    {
        $work_positions_basic_informations = SIMAAddressbookComponent::getAddressbookHtmlPerson_otherBasicInformations_workPositions($person);
        $comment_basic_informations = SIMAAddressbookComponent::getAddressbookHtmlPerson_otherBasicInformations_comment($person);
        
        return [
            $work_positions_basic_informations,
            $comment_basic_informations
        ];
    }
    
    public static function getAddressbookHtmlPerson_otherBasicInformations_workPositions(Person $person)
    {
        $result = [
            'type' => SIMAExpandableInformations::$TYPE_HTML,
            'html' => '',
        ];
        
        $work_positions = $person->work_positions;
        $company = $person->company;
        
        if(!empty($work_positions) || !empty($company))
        {
            $value = '';
            
            if(!empty($work_positions))
            {
                $work_position = $work_positions[0];
                $value = $work_position->name;
            }
            
            if(!empty($company))
            {
                if(!empty($value))
                {
                    $value .= ' | ';
                }
                
                $value .= $company->DisplayName;
            }
            
            $result['html'] = $value;
        }
        
        return $result;
    }
    
    public static function getAddressbookHtmlPerson_otherBasicInformations_comment(Person $person)
    {
        $result = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Comment'),
            'value' => $person->comment,
        ];
        
        if(!empty($person->comment))
        {
            $result['copy_button_value'] = $person->comment;
        }
        
        return $result;
    }
    
    public static function getAddressbookHtmlPerson_otherExpandedInformations(Person $person)
    {
        $have_extended_informations = false;
//        $have_extended_informations = true;
        
        $work_position_informations = SIMAAddressbookComponent::getAddressbookHtmlPerson_otherBasicInformations_workPositions($person);
        
        $extended_informations = [
            $work_position_informations
        ];
        
        $jmbg_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'PersonID'),
            'value' => ''
        ];
        if(Yii::app()->user->checkAccess('menuShowAdmin_employees') && !empty($person->JMBG)) 
        {
            $have_extended_informations = true;
            $jmbg_informations['value'] = $person->JMBG;
            $jmbg_informations['copy_button_value'] = $person->JMBG;
        }
        $extended_informations[] = $jmbg_informations;
        
        $middlename_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Middlename'),
            'value' => ''
        ];
        if(!empty($person->middlename))
        {
            $have_extended_informations = true;
            $middlename_informations['value'] = $person->middlename;
            $middlename_informations['copy_button_value'] = $person->middlename;
        }
        $extended_informations[] = $middlename_informations;
                
        $nickname_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Nickname'),
            'value' => ''
        ];
        if(!empty($person->nickname))
        {
//            $have_extended_informations = true;
            $nickname_informations['value'] = $person->nickname;
            $nickname_informations['copy_button_value'] = $person->nickname;
        }
        $extended_informations[] = $nickname_informations;
        
        $birthdate_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'DateOfBirth'),
            'value' => ''
        ];
        if(!empty($person->birthdate))
        {
            $have_extended_informations = true;
            $birthdate_informations['value'] = $person->birthdate;
            $birthdate_informations['copy_button_value'] = $person->birthdate;
        }
        $extended_informations[] = $birthdate_informations;
        
        $company_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'EmployedInCompany'),
            'value' => ''
        ];
        if(!empty($person->company))
        {
//            $have_extended_informations = true;
            $company_informations['value'] = $person->company->DisplayName;
            $company_informations['copy_button_value'] = $person->company->DisplayName;
        }
        $extended_informations[] = $company_informations;
        
        $iks_license_activated_date_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'DateOfIKSlicence'),
            'value' => ''
        ];
        if(!empty($person->iks_license_activated_date))
        {
            $have_extended_informations = true;
            $iks_license_activated_date_informations['value'] = $person->iks_license_activated_date;
            $iks_license_activated_date_informations['copy_button_value'] = $person->iks_license_activated_date;
        }
        $extended_informations[] = $iks_license_activated_date_informations;
        
        $comment_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Comment'),
            'value' => ''
        ];
        if(!empty($person->comment))
        {
//            $have_extended_informations = true;
            $comment_informations['value'] = $person->comment;
            $comment_informations['copy_button_value'] = $person->comment;
        }
        $extended_informations[] = $comment_informations;
        
        $professional_degree_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'DegreeOfVocationalEducation'),
            'value' => ''
        ];
        if(!empty($person->professional_degree))
        {
            $have_extended_informations = true;
            $professional_degree_informations['value'] = $person->professional_degree->name;
            $professional_degree_informations['copy_button_value'] = $person->professional_degree->name;
        }
        $extended_informations[] = $professional_degree_informations;
        
        $education_title_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'EducationTitle'),
            'value' => ''
        ];
        if(!empty($person->education_title))
        {
            $have_extended_informations = true;
            $education_title_informations['value'] = $person->education_title->DisplayName;
            $education_title_informations['copy_button_value'] = $person->education_title->DisplayName;
        }
        $extended_informations[] = $education_title_informations;
        
        $applications_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Applications'),
            'value' => ''
        ];
        if(!empty($person->employee_candidate) && !empty($person->employee_candidate->work_position_competitions))
        {
            $value = '';
            foreach($person->employee_candidate->work_position_competitions as $work_position_competition)
            {
                if(!empty($value))
                {
                    $value .= ' | ';
                }
                
                $value .= $work_position_competition->DisplayName;
            }
            
            $have_extended_informations = true;
            $applications_informations['value'] = $value;
        }
        $extended_informations[] = $applications_informations;
        
        return [
            'extended_informations' => $extended_informations,
            'include_extended_informations' => $have_extended_informations
        ];
    }
    
    
    public static function getAddressbookHtmlCompany_businessWidgetParams(Company $company)
    {
        $other_basic_informations = SIMAAddressbookComponent::getAddressbookHtmlCompany_otherBasicInformations($company);
        $other_expanded_informations = SIMAAddressbookComponent::getAddressbookHtmlCompany_otherExpandedInformations($company);
        $banks_informations = SIMAAddressbookComponent::getAddressbookHtmlPartner_businessWidgetParams_banks($company->partner);

        $have_extended_informations = false;
        $have_extended_informations = (
                $other_expanded_informations['include_extended_informations']
                ||
                $banks_informations['include_extended_informations']
        );
        
        $basic_informations = array_merge(
                $other_basic_informations,
                [
                    $banks_informations['basic_informations']
                ]
        );
        $expanded_informations = [];
        if($have_extended_informations === true)
        {
            $expanded_informations = array_merge(
                    $other_expanded_informations['extended_informations'],
                    $banks_informations['extended_informations']
            );
        }
        
        return [
            'basic_informations' => $basic_informations,
            'expanded_informations' => $expanded_informations
        ];
    }
    
    public static function getAddressbookHtmlCompany_otherBasicInformations(Company $company)
    {
        $html = '';
        
        $pdv_additional_classes = '_pdv-no';
        if($company->inVat)
        {
            $pdv_additional_classes = '_pdv-yes';
        }
        $html .= '<div class="sima-b-info-icons-text-bold '.$pdv_additional_classes.'">PDV</div>';
        
        $pib_additional_classes = '';
        if(!empty($company->PIB))
        {
            $pib_additional_classes = '_black';
        }
        $html .= '<div class="sima-b-info-icons-text-bold '.$pib_additional_classes.'">PIB</div>';
        
        $sd_additional_classes = '';
        if(!empty($company->work_code))
        {
            $sd_additional_classes = '_black';
        }
        $html .= '<div class="sima-b-info-icons-text-bold '.$sd_additional_classes.'">ŠD</div>';
        
        $mb_additional_classes = '';
        if(!empty($company->MB))
        {
            $mb_additional_classes = '_black';
        }
        $html .= '<div class="sima-b-info-icons-text-bold '.$mb_additional_classes.'">MB</div>';
        
        $comment_additional_classes = '';
        if(!empty($company->comment))
        {
            $comment_additional_classes = '_black';
        }
        $html .= '<div class="sima-b-info-icons-text-bold '.$comment_additional_classes.'">K</div>';
        
        return [
            [
                'type' => SIMAExpandableInformations::$TYPE_HTML,
                'html' => ''
            ],
            [
                'type' => SIMAExpandableInformations::$TYPE_HTML,
                'html' => $html,
                'no_title' => true
            ]
        ];
    }
    
    public static function getAddressbookHtmlCompany_otherExpandedInformations(Company $company)
    {
        $have_extended_informations = false;
        $extended_informations = [];
        
        $pib_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'PIB'),
            'value' => ''
        ];
        if(!empty($company->PIB))
        {
            $have_extended_informations = true;
            $pib_informations['value'] = $company->PIB;
            $pib_informations['copy_button_value'] = $company->PIB;
            $pib_informations['label_additional_classes'] = '_black';
        }
        $extended_informations[] = $pib_informations;
        
        $pdv_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'InVat'),
            'value' => Yii::t('BaseModule.Common', 'No')
        ];
        if(!empty($company->inVat))
        {
            $pdv_informations['value'] = Yii::t('BaseModule.Common', 'Yes');
            $pdv_informations['label_additional_classes'] = '_black';
        }
        $extended_informations[] = $pdv_informations;
        
        $sd_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'WorkCode'),
            'value' => ''
        ];
        if(!empty($company->work_code))
        {
            $have_extended_informations = true;
            $sd_informations['value'] = $company->work_code->DisplayName;
            $sd_informations['copy_button_value'] = $company->work_code->DisplayName;
            $sd_informations['label_additional_classes'] = '_black';
        }
        $extended_informations[] = $sd_informations;
        
        $mb_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('Company', 'MB'),
            'value' => ''
        ];
        if(!empty($company->MB))
        {
            $have_extended_informations = true;
            $mb_informations['value'] = $company->MB;
            $mb_informations['copy_button_value'] = $company->MB;
            $mb_informations['label_additional_classes'] = '_black';
        }
        $extended_informations[] = $mb_informations;
        
        $comment_informations = [
            'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
            'label' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Comment'),
            'value' => ''
        ];
        if(!empty($company->comment))
        {
            $have_extended_informations = true;
            $comment_informations['value'] = $company->comment;
            $comment_informations['copy_button_value'] = $company->comment;
            $comment_informations['label_additional_classes'] = '_black';
        }
        $extended_informations[] = $comment_informations;
        
        return [
            'extended_informations' => $extended_informations,
            'include_extended_informations' => $have_extended_informations
        ];
    }
}