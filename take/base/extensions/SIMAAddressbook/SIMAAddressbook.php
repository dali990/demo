<?php

class SIMAAddressbook extends CWidget 
{
    public $id = null;
    public $limit_companies = 20;
    public $offset_companies = null;
    public $limit_persons = 20;
    public $offset_persons = null;
    public $persons_model_filter = [];
//    public $menu_array = null;
    public $addressbook2 = false;
    
    public function run() 
    {   
        $uniq = SIMAHtml::uniqid();
        
        if(empty($this->id))
            $this->id = SIMAHtml::uniqid().'_simaAddressbook';
        
        if (empty($this->limit_companies) && (gettype($this->limit_companies)==='integer'))
            $this->limit_companies = Yii::app()->params['pt_page_size'];
        
        if (empty($this->offset_companies))
            $this->offset_companies = 0;
        
        if (empty($this->limit_persons) && (gettype($this->limit_persons)==='integer'))
            $this->limit_persons = Yii::app()->params['pt_page_size'];
        
        if (empty($this->offset_persons))
            $this->offset_persons = 0;
        
        $persons_model_filter['limit'] = $this->limit_persons;
        $persons_model_filter['offset'] = $this->offset_persons;
        $companies_model_filter['limit'] = $this->limit_companies;
        $companies_model_filter['offset'] = $this->offset_companies;
        
        $criteria_companies = new SIMADbCriteria([
            'Model'=>'Company',
            'model_filter'=>$companies_model_filter
        ]);
        
        $criteria_persons = new SIMADbCriteria([
            'Model'=>'Person',
            'model_filter'=>$persons_model_filter
        ]);
        
        $companies_pagination_id = $uniq.'_companies_pagination';
        $persons_pagination_id = $uniq.'_persons_pagination';
        $companies_id = $uniq.'_addressbook_companies';
        $persons_id = $uniq.'_addressbook_persons';
        $add_new_company_btn = $uniq.'add_company';
        $add_new_person_btn = $uniq.'add_person';
        $search_id = $uniq.'_sima_addressbook_search';
        $count_persons = Person::model()->count();
        
        $panels = [
            [
                'proportion'=>0.5,
                'html' => $this->render('company', [
                    'add_new_company_btn' => $add_new_company_btn,
                    'companies_id' => $companies_id,
                    'companies_pagination_id'=>$companies_pagination_id,
                    'widget_id' => $this->id,
                    'search_id' => $search_id
                ], true),
            ],
            [
                'proportion'=>0.5,
                'html' => $this->render('person', [
                    'html_persons' => null,
                    'add_new_person_btn' => $add_new_person_btn,
                    'persons_id' => $persons_id,
                    'persons_pagination_id'=>$persons_pagination_id,
                    'widget_id' => $this->id,
                    'company_id' => null,
                    'count_persons' => $count_persons,
                    'search_id' => $search_id
                ], true)
            ]
        ];
        
        $search =  $this->widget('SIMAUIFilter', [
                            'id' => $search_id,
                            'class' => 'sima-addressbook-search-width',
                            'searchOnKeyup' => true,
                        ], true);
        $subactions = [     
                            [
                                'title' => Yii::t('GeoModule.Address', 'Addresses'),
                                'onclick' => ['sima.dialog.openActionInDialogAsync', 'guitable', [
                                    'get_params'=>[
                                        'id'=>'base_15'
                                    ]
                                ]]
                            ],
                            [
                                'title' => Yii::t('GeoModule.Street', 'Streets'),
                                'onclick' => ['sima.dialog.openActionInDialogAsync', 'guitable', [
                                    'get_params'=>[
                                        'id'=>'base_14'
                                    ]
                                ]]
                            ],
                            [
                                'title' => Yii::t('GeoModule.PostalCode', 'PostalCodes'),
                                'onclick' => ['sima.dialog.openActionInDialogAsync', 'guitable', [
                                    'get_params'=>[
                                        'id'=>'base_13'
                                    ]
                                ]]
                            ],
                            [
                                'title' => Yii::t('BaseModule.GuiTable', 'Cities'),
                                'onclick' => ['sima.dialog.openActionInDialogAsync', 'guitable', [
                                    'get_params'=>[
                                        'id'=>'base_1'
                                    ]
                                ]]
                            ],
                            [
                                'title' => Yii::t('GeoModule.Municipality', 'Municipalities'),
                                'onclick' => ['sima.dialog.openActionInDialogAsync', 'guitable', [
                                    'get_params'=>[
                                        'id'=>'geo_1'
                                    ]
                                ]]
                            ],
                            [
                                'title' => Yii::t('BaseModule.GuiTable', 'Countries'),
                                'onclick' => ['sima.dialog.openActionInDialogAsync', 'guitable', [
                                    'get_params'=>[
                                        'id'=>'base_0'
                                    ]
                                ]]
                            ],
                            [
                                'title' => Yii::t('CompanyWorkCode', 'CompanyWorkCodes'),
                                'onclick' => ['sima.dialog.openActionInDialogAsync', 'guitable', [
                                    'get_params'=>[
                                        'id'=>'base_11'
                                    ]
                                ]]
                            ],
                            [
                                'title' => Yii::t('BaseModule.PartnerList', 'PartnerLists'),
                                'onclick' => ['sima.dialog.openActionInDialog', 'simaAddressbook/partnersToPartnerLists']
                            ]
                        ];

        if (Yii::app()->user->checkAccess('AddPhoneNumberTypes'))
        {
            $subactions[] = [
                'title' => Yii::t('BaseModule.PhoneNumber', 'PhoneNumberTypes'),
                'onclick' => ['sima.dialog.openActionInDialog', 'simaAddressbook/phoneNumberTypes'],
            ];
        }
        
        if(SIMAMisc::isVueComponentEnabled())
        {
            $settings_button = $this->widget(SIMAButtonVue::class, [
                'icon' => '_main-settings',
                'iconsize' => 18,
                'subactions_data' => $subactions
            ], true);
        }
        else
        {
            $settings_button = $this->widget('SIMAButton', [
                'action' => [
                    'icon' => 'sima-icon _main-settings _18',
                    'subactions' => $subactions
                ]
            ], true);
        }
        
        echo Yii::app()->controller->renderContentLayout(
            [
                'name' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Addressbook'),
                'options_class'=>'sima-addressbook-options',
                'options'=>[
                    //Sasa A. - zakomentarisana opcija dok se ne implementira
//                    [
//                        'class' => '_disabled',
//                        'title'=>Yii::t('SIMAAddressbook.SIMAAddressbook', 'MultiselektList'),
//                        'html' => $this->widget('SIMAButton',[
//                            'action'=>[
//                                'title' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'MultiselektList'),
//                                'tooltip'=> Yii::t('SIMAAddressbook.SIMAAddressbook', 'MultiselektList'),
////                                'onclick'=>[]
//                            ]
//                        ], true)
//                    ],
                    [
                        'html'=> $search,
                    ],
                    [
                        'class'=>'sima-email-client-option _settings',
                        'title'=>Yii::t('SIMAAddressbook.SIMAAddressbook', 'Settings'),
                        'html' => $settings_button
                    ]
                ]
            ], 
            $panels, 
            [
                'id' => $this->id,
                'class' => 'sima-addressbook-widget',
                'processOutput'=>false
            ]
        );
        
        $params = [
            'id_companies_pagination' => $companies_pagination_id, 
            'id_companies' => $companies_id,
            'id_persons_pagination' => $persons_pagination_id, 
            'id_persons' => $persons_id,
            'id_search' => $search_id,
            'id_simaAddressbook' => $this->id,
            'persons_model_filter'=>$persons_model_filter,
            'companies_model_filter' => $companies_model_filter,
            'companies_criteria' => $criteria_companies,
            'persons_criteria' => $criteria_persons,
            'addressbook2' => $this->addressbook2
            ];
        $json = CJSON::encode($params);
        $register_script = "$('#" . $this->id . "').simaAddressbook('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
    
    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        $uniq = SIMAHtml::uniqid();
        
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaAddressbook.css');
        
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.simaAddressbook.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/simaAddressbookEvents.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/layoutAllignHandler.js', CClientScript::POS_END);
        
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/simaAddressbook.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScript("addressbook_$uniq", "sima.addressbook = new SIMAAddressbook();", CClientScript::POS_END);
    }
}
