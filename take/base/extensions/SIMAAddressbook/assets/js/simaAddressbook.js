/* global sima */

function SIMAAddressbook()
{
    this.onPartnerListSelect = function(uniq_id, related_model, row)
    {
        var partner_list_id = row.attr('model_id');

        sima.ajax.get('simaAddressbook/onPartnerListSelectCheckParams', {
            get_params: {
                partner_list_id: partner_list_id
            },
            async: true,
            success_function: function(response) {
                var related_model_tbl = $('#related_model_' + uniq_id);

                related_model_tbl.show();
                
                if (!response.show_add_button)
                {
                    related_model_tbl.simaGuiTable('setAddButton', false);
                }
                else
                {
                    var model_choose = 'Partner';
                    if (related_model === 'UserToPartnerList')
                    {
                        model_choose = 'User';
                    }
                    related_model_tbl.simaGuiTable('setAddButton', function() {
                        sima.model.choose(model_choose, function(data) {

                            var related_model_tbl = $('#related_model_' + uniq_id);
                            sima.ajaxLong.start('simaAddressbook/savePartnerListMembers', {
                                showProgressBar: related_model_tbl,
                                data: {
                                    partner_list_id: partner_list_id,
                                    related_model_name: related_model,
                                    choosed_models: data
                                },
                                onEnd: function(response) {
                                    related_model_tbl.simaGuiTable('populate');
                                }
                            });
                        },
                        {
                            multiselect: true,
                            params: {
                                add_button: false
                            }
                        });
                    });
                }
                
                related_model_tbl.simaGuiTable('setFixedFilter', {
                    partner_list: {
                        ids: partner_list_id
                    }
                }).simaGuiTable('populate');

                sima.layout.allignObject(related_model_tbl);
            }
        });
    };
    
    this.onPartnerListUnSelect = function(uniq_id)
    {
        var related_model_tbl = $('#related_model_' + uniq_id);
        
        related_model_tbl.hide();
        related_model_tbl.simaGuiTable('setFixedFilter', {
            partner_list: {
                ids: -1
            }
        }).simaGuiTable('populate');
    };
    
    this.calcAllSystemPartnerLists = function(uniq_id)
    {
        var partner_lists_tbl = $('#partner_lists_'+uniq_id);
        var related_model_tbl = $('#related_model_'+uniq_id);

        sima.ajaxLong.start('simaAddressbook/calcAllSystemPartnerLists', {
            showProgressBar: partner_lists_tbl,
            onEnd: function(response) {
                partner_lists_tbl.simaGuiTable('populate');
                related_model_tbl.simaGuiTable('populate');
            }
        });
    };
}