
$('body').on('sima-layout-allign', '.sima-b-info', function(event, height, width, source)
{ // drugo bolje resenje

    if (event.target === this) 
    {
        var _this = $(this);
        var results = $(this).parents('.sima-addressbook-results-list:first');

// Ako nije vrapovano u setTimeout f-ju, dimenzije unutar verikalne pregrade,
// koje se pokupe na event pomeranja pregrade (promene sirine panela) odgovaraju 
// pocetnom momentu pozicije pregrade, a ocekivano je da odgovaraju krajnjoj poziciji 
// pregrade.
// Pocetni polozaj pregrade: pozicija pregrade kada se pregrada uhvati misem 
// Krajni polozaj pregrade: pozicija se pragrada drzana misem posle prevlacenja po layoutu pusti 
        setTimeout(function(){ 

            // zakomentarisano jer treba resiti na drugi nacin !! -> za sada reseno f-jom setTimeout(), ali mozda treba jos razmotriti
//                    
//                    panel_height = $(this).parents('.sima-addressbook-results-list-wrapper:first').outerHeight(true);
//                    main_header_height = $(this).parents('.sima-addressbook-results-list-wrapper:first').find('.sima-addressbook-main-header').outerHeight(true);
//                    subheader_height = $(this).parents('.sima-addressbook-results-list-wrapper:first').find('.sima-addressbook-subheader').outerHeight(true);
//                    results_pagination_list_height = $(this).parents('.sima-addressbook-results-list-wrapper:first').find('.sima-addressbook-footer').outerHeight(true);
//                    results_list = panel_height - main_header_height - subheader_height - results_pagination_list_height;
//                    $(this).css('height',results_list);
//
//                    sima.layout.allignOLDChildren(
//                        $(this)
//                    );
//                    event.stopPropagation();    



// Prvo stajalo u simaLayout.js jer je bila ideja da 
// komponente sa sima-repacker klasom mogu da se prepakuju
// ne samo u imeniku i basic info, vec na globalnom nivou 
// gde je potrebno. 
// Za sada je vraceno Addressbook widget, dok ne bude postojala 
// potreba da se koristi globalno, da se ne poziva bespotrebno.

            padding = 12;
            all_boxes_min_width_horizontal = 0;
            all_boxes_height = 0;
            data_min_height = 0;
            data_boxes_min_width = 0;

            // trenitna sirina panela za smestanje modela
            results_panel_width = _this.parents('.sima-addressbook-results-list:first').width();
            _this.css('width', '100%');
            _this.parents('.sima-basic-info-wrapper:first').css('width', '100%');

            //izracunavanje minimalne sirine skupa data box-eva u HORIZONTALNOM polozaju
            _this.find('.sima-repacker-data').each(function(){
                data_boxes_min_width += parseInt($(this).css('min-width'));
            });

            // minimalna sirina data box-a u VERTIKALNOM polozaju
            data_min_width_to_set = _this.find('.sima-repacker-data:first').outerWidth(true); 

            // odredjivanje ukupne visine data box-eva - za VERTIKALNI prikaz Basic Info-a
            var data_boxes_min_height = 0;
            var one_data_box_max_height = 0;
            _this.find('.sima-repacker-data').each(function(){
                var _box_height = $(this).outerHeight(true);
                data_boxes_min_height += _box_height;
                if (one_data_box_max_height < _box_height)
                {
                    one_data_box_max_height = _box_height;
                }
            });

            // sirina slike
            photo_width = parseInt(_this.find('img.sima-repacker-photo').outerWidth());
            if (photo_width === null)
            {
                photo_width = 0;
            }

            // visina slike
            photo_height = parseInt(_this.find('img.sima-repacker-photo').outerHeight(true));
            if (photo_height === null)
            {
                photo_height = 0;
            }
            if (one_data_box_max_height < photo_height)
            {
                one_data_box_max_height = photo_height;
            }

            // visina header-a
            basic_info_header_height = _this.find('.sima-repacker-header').outerHeight(true);
            if (basic_info_header_height === null)
            {
                basic_info_header_height = 0;
            }


            // sirina basic inf0-a za HORIZONTALNI prikaz
            all_boxes_min_width_horizontal = data_boxes_min_width + photo_width + 5*padding ;

            // visina za basic info za VERTIKALNI prikaz
            all_boxes_height = data_boxes_min_height + photo_height + basic_info_header_height + 3*padding; // dva padding-a za gornju i donju stranu

            // min sirina za jedan data box - potrebno za VERTIKALNI prikaz
            min_data_box_width = data_min_width_to_set + 2*padding; // dva padding-a za levu i desnu stranu

            // visina basic Info-a u HORIZONTALNOM prikazu
            horizontal_basic_info_height = one_data_box_max_height + basic_info_header_height + 2*padding;

            // prepakivanje prikaza u VERIKALNI ili HORIZONTALNI prikaz
            if (results_panel_width < all_boxes_min_width_horizontal)
            {
                // VERTIKALNI prikaz
                _this.css('height', all_boxes_height);
                _this.parents('.sima-basic-info-wrapper:first').css('height', all_boxes_height);
                _this.find(".sima-repacker-data-wrapper-short").addClass('_narrow');
                _this.find('div.sima-repacker-photo').addClass('_full-width');
                _this.find('div.sima-repacker-photo').css('text-align', 'center');
                _this.find('.sima-repacker-data').each(function()
                {
                    $(this).addClass('_full-width');
                });

                //minimalni VERTIKALNI prikaz
                if (results_panel_width < min_data_box_width)
                {
                    _this.css('min-width', min_data_box_width);
                } 
            } else {
                // HORIZONTALNI prikaz
                _this.css('height', horizontal_basic_info_height);
                _this.parents('.sima-basic-info-wrapper:first').css('height', horizontal_basic_info_height);
                _this.find(".sima-repacker-data-wrapper-short").removeClass('_narrow');
                _this.find('div.sima-repacker-photo').css('display', 'inline-block');
                _this.find('div.sima-repacker-photo').removeClass('_full-width');
                _this.find('.sima-repacker-data').each(function()
                {
                    $(this).css('float', 'left');
                    $(this).css('display', 'inline-block');
                    $(this).removeClass('_full-width');
                    $(this).find('img.sima-repacker-photo').css('display', 'inline-block');
                });
            }
            // preracunavanje teksta unutar redova za jedan basic info
            results.parents('.sima-addressbook-widget:first').simaAddressbook('refreshLayoutAllign', _this);

        }, 0); // setTimeout wrapper f-ja
    }
}); 
        
