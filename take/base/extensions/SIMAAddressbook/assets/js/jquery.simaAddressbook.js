/* global sima */

(function($)
{
    var methods = {
        init: function(params)
        { 
            var localThis = $(this);
            var id_companies = params.id_companies;
            var id_companies_pagination = params.id_companies_pagination;
            var id_persons = params.id_persons;
            var id_persons_pagination = params.id_persons_pagination;
            var id_search = params.id_search;
            localThis.data('companies_model_filter', params.companies_model_filter); // da li treba koristiti extend ?
            localThis.data('persons_model_filter', params.persons_model_filter);     // da li treba koristiti extend ? 
            localThis.data('id_search', id_search);
            localThis.data('id_simaAddressbook', params.id_simaAddressbook);
            localThis.data('id_companies', params.id_companies);
            localThis.data('id_companies_pagination', params.id_companies_pagination);
            localThis.data('id_persons', params.id_persons);
            localThis.data('id_persons_pagination', params.id_persons_pagination);
            
            localThis.data('companies_action', 'simaAddressbook/searchSIMAAddressbookCompanies');
            localThis.data('persons_action', 'simaAddressbook/searchSIMAAddressbookPersons');
            
            localThis.data('companies_group_id', sima.uniqid());
            localThis.data('persons_group_id', sima.uniqid());
            
            $('#'+id_companies_pagination).on('pagin_event', function(event, data)
            {
                var local_companies_model_filter = getModelFilterCompanies(localThis);
                local_companies_model_filter['limit'] = data.limit;
                local_companies_model_filter['offset'] = data.offset;

                action = localThis.data('companies_action');
                model_filter = local_companies_model_filter;
                ladingCircle_area = $("#"+id_companies).find('.addressbook_companies_list');
                success_func = function(response) 
                {
                    var companies_list = $("#"+id_companies).find('.addressbook_companies_list');
                    companies_list.html(response.companies_html);
                    companies_list.trigger('sima-layout-allign', [companies_list.height(),companies_list.width(), 'TRIGGER_address_book_on_companies_pagin_event']);
                };
                
                getModelByAjax(localThis, action, model_filter, ladingCircle_area, success_func);
                
            });
            
            $('#'+id_persons_pagination).on('pagin_event', function(event, data)
            {
                var local_persons_model_filter = getModelFilterPersons(localThis);
                local_persons_model_filter['limit'] = data.limit;
                local_persons_model_filter['offset'] = data.offset;
                
                action = localThis.data('persons_action');
                model_filter = local_persons_model_filter;
                ladingCircle_area = $("#"+id_persons).find('.addressbook_persons_list');
                success_func = function(response){
                    var persons_list = $("#"+id_persons).find('.addressbook_persons_list');
                    persons_list.html(response.persons_html);
                    persons_list.trigger('sima-layout-allign', [persons_list.height(),persons_list.width(), 'TRIGGER_address_book_on_persons_pagin_event']);
                };
                
                getModelByAjax(localThis, action, model_filter, ladingCircle_area, success_func);
            });
            
            // pretraga
            $('#'+id_search).on('text_search', function(event, data)
            {
                // model filter kupi rec koja se pretrazuje
                var persons_model_filter = getModelFilterPersons(localThis);
                var companies_model_filter = getModelFilterCompanies(localThis);

                getSearchResults(localThis, companies_model_filter, persons_model_filter);
            });
            
            getSearchResults(localThis, localThis.data('companies_model_filter'), localThis.data('persons_model_filter'));
        },
        
        refreshLayoutAllign: function(sima_b_info)
        {
          //preracunavanje visine bloka za priazivanje rezultata 
            RowsWidthExpanded(sima_b_info);
        },
        
        expand_b_info: function(params)
        {
            var expand_btn = params.expand_btn;
            var basic_info = expand_btn.parents('.sima-b-info:first');

            if ($(basic_info).find('.sima-b-info-business-partner-expanded').hasClass('sima-b-info-bussines-partner-colapsed'))
            {
                closeAllBInfoBoxes();

                //open one - begin
                $(basic_info).find('.sima-b-info-business-partner').css('z-index', '2');
                $(basic_info).find('.sima-b-info-business-partner-expanded').addClass('sima-b-info-business-expanded');
                $(basic_info).find('.sima-b-info-business-partner').css('background-color', '#ffe3cc'); // trebalo bi da postoji clasa za oznacavanje a ne da se postavlja css
                $(basic_info).find('._pdv').css('background-color', ''); 
                $(basic_info).find('.sima-b-info-business-partner-expanded').slideDown();
                $(basic_info).find('.sima-b-info-business-partner-expanded').removeClass('sima-b-info-bussines-partner-colapsed');
                $(basic_info).find('.sima-b-info-row-primary-only').slideUp();
                $(basic_info).find('.sima-b-info-row-primary-only').addClass('sima-b-info-row-primary-only-colapsed');
                $(basic_info).find('.sima-b-info-front-info').slideUp();
                $(basic_info).find('.sima-b-info-front-info').addClass('sima-b-info-front-info-colapsed');
                $(basic_info).find('.sima-b-info-business-partner-wrapper').find('._expand-arrows').addClass('_close');
                $(basic_info).find('.sima-b-info-business-partner-wrapper').find('._close').removeClass('_expand-arrows');
                $(basic_info).find('.sima-b-info-business-partner-wrapper').find('._close').hide();
                $(basic_info).find('.sima-b-info-business-partner-wrapper').find('._close').show();
                RowsWidthExpanded(basic_info);
                return false;
                //open one - end
            }
            else if ($(basic_info).find('.sima-b-info-business-partner-expanded').hasClass('sima-b-info-business-expanded'))
            {
                //close one - begin
                closeBusinessInfoForBasicInfo(basic_info);
                return false;
                //close one - end
            }   
        },
        
        expand_contact_b_info: function(params)
        {
            var expand_btn = params.expand_btn;
            var basic_info = expand_btn.parents('.sima-b-info:first');
            
            if ($(basic_info).find('.sima-b-info-contact-partner-expanded').hasClass('bi-partner-contact-colapsed'))
            {
                closeAllBInfoBoxes();

                //open one - begin
                $(basic_info).find('.sima-b-info-contact-partner').css('z-index', '2');
                $(basic_info).find('.sima-b-info-contact-partner-expanded').addClass('bi-partner-contact-expanded');
                $(basic_info).find('.sima-b-info-contact-partner').css('background-color', '#ffe3cc'); // trebalo bi da postoji clasa za oznacavanje a ne da se postavlja css
                $(basic_info).find('.sima-b-info-contact-partner-short').slideUp();
                $(basic_info).find('.sima-b-info-contact-partner-expanded').slideDown();
                $(basic_info).find('.sima-b-info-contact-partner-expanded').removeClass('bi-partner-contact-colapsed');
                $(basic_info).find('.sima-b-info-contact-partner-wrapper').find('._expand-arrows').addClass('_close');
                $(basic_info).find('.sima-b-info-contact-partner-wrapper').find('._close').removeClass('_expand-arrows');
                $(basic_info).find('.sima-b-info-contact-partner-wrapper').find('._close').hide();
                $(basic_info).find('.sima-b-info-contact-partner-wrapper').find('._close').show();
                RowsWidthExpanded(basic_info);
                return false;
                //open one - end
            }

            if ($(basic_info).find('.sima-b-info-contact-partner-expanded').hasClass('bi-partner-contact-expanded'))
            {
                //close one - begin
                closeContactInfoForBasicInfo(basic_info);
                return false;
                //close one - end
            }
        },
        
        onAddNewCompany: function(params)
        {
            var localThis = $(this);
            var id_companies = params.id_companies;
            var widget_id = params.widget_id;
            sima.model.form('Company', '', {
                onSave: function(response){
                    sima.ajax.get('simaAddressbook/renderNewCompany', {
                        get_params: {
                            model_id: response.model_id,
                            widget_id: widget_id
                        },
                    async: true,
                    loadingCircle: $("#"+id_companies).find('.addressbook_companies_list'),
                        success_function: function(response1) {
                            var companies_list = $("#"+id_companies).find('.addressbook_companies_list');
                            companies_list.prepend(response1.added_model);
                            companies_list.find('.sima-addressbook-company:first').children(':first-child').effect("highlight", {}, 5000);
                            sima.layout.allignObject(companies_list, 'TRIGGER_address_book_on_getSearchResults_companies');
                        }
                    });
                }
            });
        },
        
        onAddNewPerson: function(params)
        {
            var localThis = $(this);
            var id_persons = params.id_persons;
            var widget_id = params.widget_id;
            var company_id = params.company_id;
            var init = {};
            if (company_id!== null && company_id !== '')
            {
                init = {
                    Person:{
                        company_id: company_id
                    }
                };
            }
            sima.model.form('Person', '', {
                onSave: function(response){
                    sima.ajax.get('simaAddressbook/renderNewPerson', {
                        get_params: {
                            model_id: response.model_id,
                            widget_id: widget_id
                        },
                    async: true,
                    loadingCircle: $("#"+id_persons).find('.addressbook_persons_list'),
                        success_function: function(response1) {
                            var persons_list = $("#"+id_persons).find('.addressbook_persons_list');
                            persons_list.prepend(response1.added_model);
                            persons_list.find('.sima-addressbook-person:first').children(':first-child').effect("highlight", {}, 5000);
                            sima.layout.allignObject(persons_list, 'TRIGGER_address_book_on_getSearchResults_persons');
                        }
                    });
                },
                init_data: init
            });
        },
        
        onCopyText: function(params)
        {
//            var button_id = params.button_id;
            var text_to_copy = params.text_to_copy;
            sima.misc.copyToClipboard(text_to_copy);
        },
        
        setupRowsWidth: function(params)
        {
            var localThis1 = params.sima_b_info;
            RowsWidth(localThis1);
        },
        
        onShowEmplyeesCompany: function(params)
        {
            var id_company = params.id_company;
            var company_name = params.company_name;
            var widget_id = params.widget_id;
            
            sima.ajax.get('simaAddressbook/showCompanyEmployees', {
                get_params: {
                    model_id: id_company,
                    company_name: company_name,
                    widget_id: widget_id
                },
                async: true,
                loadingCircle: $("#"+widget_id).find('.addressbook_companies_list'),
                success_function: function(response1) {
                    var firma = "Lista zaposlenih u firmi "+response1.company_name;
                    sima.dialog.openInFullScreen(response1.html, firma, response1.data); 

                }
            });
        },
        
        employeesPagination: function(params)
        {
            var localThis = $(this);
            employees_pagination_id = params.employees_pagination_id;
            employees_dialog_id = params.employees_dialog_id;
            persons_model_filter = params.persons_model_filter;
            
            action = localThis.data('persons_action');
            local_emplyees_model_filter = persons_model_filter;
            ladingCircle_area = $("#"+employees_dialog_id);
            success_func = function(response){
                var persons_list = $("#"+employees_dialog_id).find('.sima-addressbook-results-list');
                persons_list.html(response.persons_html);
                persons_list.trigger('sima-layout-allign', [persons_list.height(),persons_list.width(), 'TRIGGER_address_book_on_persons_pagin_event']);
            };
                
            getModelByAjax(localThis, action, local_emplyees_model_filter, ladingCircle_area, success_func);
            
            $('#'+employees_pagination_id).on('pagin_event', function(event, data)
            {
                local_emplyees_model_filter['limit'] = data.limit;
                local_emplyees_model_filter['offset'] = data.offset;
                
                getModelByAjax(localThis, action, local_emplyees_model_filter, ladingCircle_area, success_func);
            });
        }
    };
    
    function closeAllBInfoBoxes()
    {
        //close all - begin
        
        // all business info - begin
        var basic_infos_to_close_business = {};
        $('.sima-b-info-business-partner-expanded.sima-b-info-business-expanded').each(function(index, val){
            
            var basic_info = $(val).parents('.sima-b-info:first');
            basic_infos_to_close_business[basic_info.data('bi_id')] = basic_info;
        });
        $.each(basic_infos_to_close_business, function (index, value) {
            closeBusinessInfoForBasicInfo(value);
        });
        // all business info - end

        // all contact info - begin
        var basic_infos_to_close_contact = {};
        $('.sima-b-info-contact-partner-expanded.bi-partner-contact-expanded').each(function(index, val){
            
            var basic_info = $(val).parents('.sima-b-info:first');
            basic_infos_to_close_contact[basic_info.data('bi_id')] = basic_info;
        });
        $.each(basic_infos_to_close_contact, function (index, value) {
            closeContactInfoForBasicInfo(value);
        });
        // all contact info - end 
                
        //close all - end
    }
    
    function closeBusinessInfoForBasicInfo(basic_info)
    {
        basic_info.find('.sima-b-info-business-partner-expanded').addClass('sima-b-info-bussines-partner-colapsed');
        basic_info.find('.sima-b-info-business-partner-expanded').slideUp();
        basic_info.find('.sima-b-info-business-partner').css('background-color', ''); // trebalo bi da postoji clasa za oznacavanje a ne da se postavlja css
        basic_info.find('.sima-b-info-business-partner-expanded').removeClass('sima-b-info-business-expanded');
        basic_info.find('.sima-b-info-business-partner').css('z-index',"");
        basic_info.find('.sima-b-info-row-primary-only').removeClass('sima-b-info-row-primary-only-colapsed');
        basic_info.find('.sima-b-info-row-primary-only').slideDown();
        basic_info.find('.sima-b-info-front-info').removeClass('sima-b-info-front-info-colapsed');
        basic_info.find('.sima-b-info-front-info').slideDown();
        basic_info.find('.sima-b-info-business-partner-wrapper').find('._close').addClass('_expand-arrows');
        basic_info.find('.sima-b-info-business-partner-wrapper').find('._expand-arrows').removeClass('_close');
        basic_info.find('.sima-b-info-business-partner-wrapper').find('._expand-arrows').hide();
        basic_info.find('.sima-b-info-business-partner-wrapper').find('._expand-arrows').show();
        RowsWidthExpanded(basic_info);
    }
    
    function closeContactInfoForBasicInfo(basic_info)
    {
        basic_info.find('.sima-b-info-contact-partner-expanded').addClass('bi-partner-contact-colapsed');
        basic_info.find('.sima-b-info-contact-partner-expanded').slideUp();
        basic_info.find('.sima-b-info-contact-partner-short').slideDown();
        basic_info.find('.sima-b-info-contact-partner').css('z-index', '');
        basic_info.find('.sima-b-info-contact-partner').css('background-color', ''); // trebalo bi da postoji clasa za oznacavanje a ne da se postavlja css
        basic_info.find('.sima-b-info-contact-partner-expanded').removeClass('bi-partner-contact-expanded');
        basic_info.find('.sima-b-info-contact-partner-wrapper').find('._close').addClass('_expand-arrows');
        basic_info.find('.sima-b-info-contact-partner-wrapper').find('._expand-arrows').removeClass('_close');
        basic_info.find('.sima-b-info-contact-partner-wrapper').find('._expand-arrows').hide();
        basic_info.find('.sima-b-info-contact-partner-wrapper').find('._expand-arrows').show();
        RowsWidthExpanded(basic_info);
    }
    
    function RowsWidthExpanded(sima_b_info)
    {
        var row_width = 0;
        var row_limited_width =0;
        var first_div_icon_width = 0;
        var second_div_icons_width = 0;
        var third_div_text_width = 0;
        var padding = 10;

        $(sima_b_info).find('.sima-b-info-data-row').each(function()
        {
            i=0;
            if ($(this).not(':empty') && $(this).is(':visible') && !($(this).hasClass('_pdv')))
            {
                i++;
                row_width = $(this).width();
                
                if ($(this).find('.sima-b-info-data-row-option').length > 0)
                    row_options_width = $(this).find('.sima-b-info-data-row-option').width();
                else
                    row_options_width = 0;

                first_div_icon_width = $(this).find('.sima-b-info-icons').outerWidth();
                second_div_icons_width = $(this).find('.sima-b-info-label').outerWidth();
                third_div_text_width = $(this).find('.sima-b-info-value').width();
                
                third_div_text_width_referent = row_width - first_div_icon_width - second_div_icons_width - row_options_width -25;
                row_limited_width = row_width - padding - first_div_icon_width - second_div_icons_width - 25;

                if(third_div_text_width_referent < third_div_text_width)
                {
                    $(this).find('.sima-b-info-value').width(third_div_text_width_referent);// !!! PROBLEM !!!
                } else {
                    $(this).find('.sima-b-info-value').css('width', '');
                    fresh_width = $(this).find('.sima-b-info-value').width();
                    if (fresh_width > row_limited_width)
                    {
                        $(this).find('.sima-b-info-value').css('width', row_limited_width);
                    }
                }
            }
        });
    }
    
    function RowsWidth(localthis2)
    {
        $(localthis2).find('.sima-b-info').each(function()
        { 
            RowsWidthExpanded($(this));
        });
    }

    function getSearchResults(localThis, companies_model_filter, persons_model_filter)
    {
        var id_simaAddressbook = localThis.data('id_simaAddressbook');
        var id_companies = localThis.data('id_companies');
        var id_companies_pagination = localThis.data('id_companies_pagination');
        var id_persons = localThis.data('id_persons');
        var id_persons_pagination = localThis.data('id_persons_pagination');
        
        var action = localThis.data('companies_action');
        var model_filter = companies_model_filter;
        var ladingCircle_area = $('#'+id_simaAddressbook).find('.addressbook_companies_list');
        var success_func = function(response) {
            //MilosS(26.2.2018): triger koji poziva ovo je definisan u ovoj listi
//            sima.set_html($("#"+id_companies).find('.addressbook_companies_list'), response.companies_html, 'TRIGGER_address_book_on_getSearchResults_companies');
            var _list = $("#"+id_companies).find('.addressbook_companies_list');
            _list.html(response.companies_html);
            sima.layout.allignObject(_list, 'TRIGGER_address_book_on_getSearchResults_companies');
            $('#'+id_companies_pagination).simaPagination('resetPagination', response.count_companies); 
//            $("#"+id_companies).trigger('sima-layout-allign');
        };
                
        getModelByAjax(localThis, action, model_filter, ladingCircle_area, success_func);
        
        var action1 = localThis.data('persons_action');
        var model_filter1 = persons_model_filter;
        var ladingCircle_area1 = $('#'+id_simaAddressbook).find('.addressbook_persons_list');
        var success_func1 = function(response){
            //MilosS(26.2.2018): triger koji poziva ovo je definisan u ovoj listi
//            sima.set_html($("#"+id_persons).find('.addressbook_persons_list'), response.persons_html, 'TRIGGER_address_book_on_getSearchResults_persons');
            var _list = $("#"+id_persons).find('.addressbook_persons_list');
            _list.html(response.persons_html);
            sima.layout.allignObject(_list, 'TRIGGER_address_book_on_getSearchResults_persons');
            $('#'+id_persons_pagination).simaPagination('resetPagination', response.count_persons);
//            $("#"+id_persons).trigger('sima-layout-allign');
        };
        
        getModelByAjax(localThis, action1, model_filter1, ladingCircle_area1, success_func1);
    }
    
    function getModelFilterPersons(localThis)
    {
        var persons_model_filter = jQuery.extend({}, localThis.data('persons_model_filter'));
        var id_search = localThis.data('id_search');
        //kupe se svi select filteri
        //text pretragu
        var search_text = $('#'+id_search).simaUIFilter('getValue');
        if (search_text !== '')
        {
            persons_model_filter['text'] = search_text;
        }
        //paginaciju
        //----ne za sada -----//
        
        return persons_model_filter;
    }
    
    function getModelFilterCompanies(localThis)
    {
        var companies_model_filter = jQuery.extend({}, localThis.data('companies_model_filter'));
        var id_search = localThis.data('id_search');
        var search_text = $('#'+id_search).simaUIFilter('getValue');
        if (search_text !== '')
        {
            companies_model_filter['text'] = search_text;
        }
        return companies_model_filter;
    }
    
    function getModelByAjax(localThis, action, model_filter, ladingCircle_area, success_func)
    {
        var group_id = localThis.data('companies_group_id');
        if (action === localThis.data('persons_action'))
        {
            group_id = localThis.data('persons_group_id');
        }

        id_simaAddressbook = localThis.data('id_simaAddressbook');
        sima.ajax.get(action, {
            get_params: {
                widget_id: id_simaAddressbook
            },
            data: {
                model_filter: model_filter
            },
            async: true,
            group:group_id,
            loadingCircle: ladingCircle_area,
            success_function: success_func
        });
    }
    
    $.fn.simaAddressbook = function(methodOrOptions) 
    {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.addressbook');
            }
        });
        return ret;
    };
    
})( jQuery );