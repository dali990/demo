
/* global sima */

$('body').on('sima-layout-allign','.sima-addressbook-results-list',function(event, height, width, source){
    if (event.target === this)
    {
        if(isNaN(height) || isNaN(width))
        {
            event.stopPropagation();
            return;
        }
        if (sima.layout.log())
        {
            console.log(source);
            console.log('HANDLER_addressbook_handler');
            console.log($(this));
            console.log('height: '+height+' width: '+width);
        }
        var localThis = $(this);
        
        var siblings = localThis.siblings();
        var siblings_height = 0;
        $.each(siblings, function(index, sibling){
            siblings_height += $(sibling).outerHeight(true);
        });

        localThis.height(height);
        localThis.width(width);

        localThis.children('.sima-layout-panel').trigger('sima-layout-allign',[height,width,'TRIGGER_addressbook_handler']);
        

        event.stopPropagation();
    }
});