<?php

class SIMAAddressbookController extends SIMAController{
    
    public $views_path = 'base.extensions.SIMAAddressbook.views.addressbook.';

    public function actionIndex()
    {
        $html = $this->widget('SIMAAddressbook', [], true);
        $this->respondOK([
            'html' => $html,
            'title' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Addressbook')
        ]);
    }
    /**
     * TODO: ukloniti (potrebna migracija zbog zapamcenih tabova)
     */
//    public function actionIndex2()
//    {
//        $html = $this->widget('SIMAAddressbook', [], true);
//        $this->respondOK([
//            'html' => $html,
//            'title' => Yii::t('SIMAAddressbook.SIMAAddressbook', 'Addressbook')
//        ]);
//    }
    
    public function actionRenderNewCompany($model_id, $widget_id)
    {
        $model = Company::model()->findByPk($model_id);
        $html = $this->renderCompany($model, $widget_id);
        $this->respondOK([
            'added_model' => $html
        ]);
    }
    
    public function actionRenderNewPerson($model_id, $widget_id)
    {
        $model = Person::model()->findByPk($model_id);
        $html = $this->renderPerson($model, $widget_id);
        $this->respondOK([
            'added_model' => $html
        ]);
    }
    
    public function actionSearchSIMAAddressbookCompanies($widget_id)
    {
//        $model_filter = $this->filter_post_input('model_filter');
//        if (!isset($model_filter))
//        {
//            throw new SIMAException('actionSearchSIMAAddressbook - nisu lepo zadati parametri');
//        } else {
//            $companies_model_filter =  $this->filter_post_input('model_filter');
//        }
        $companies_model_filter = $this->filter_post_input('model_filter');
        
        $companies_model_filter_without_limit_and_offset = $companies_model_filter;
        $companies_model_filter_without_limit_and_offset['offset'] = null;
        $companies_model_filter_without_limit_and_offset['limit'] = null;
        
        $companies_count = Company::model()->count(new SIMADbCriteria([
            'Model'=>'Company',
            'model_filter'=> $companies_model_filter_without_limit_and_offset
        ]));
        
        $companies_html = $this->getAddressbookHtmlCompanies($companies_model_filter, $widget_id);

        $this->respondOK([
            'companies_html' => $companies_html,
            'count_companies' => $companies_count,
        ]);
    }
    
    public function actionSearchSIMAAddressbookPersons($widget_id)
    {
//        $model_filter = $this->filter_post_input('model_filter');
//        if (!isset($model_filter))
//        {
//            throw new SIMAException('actionSearchSIMAAddressbook - nisu lepo zadati parametri');
//        } else {
//            $persons_model_filter =  $this->filter_post_input('model_filter');
//        }
        $persons_model_filter = $this->filter_post_input('model_filter');
        
        $persons_model_filter_without_limit_and_offset = $persons_model_filter;
        $persons_model_filter_without_limit_and_offset['offset'] = null;
        $persons_model_filter_without_limit_and_offset['limit'] = null;
        
        $persons_count = Person::model()->count(new SIMADbCriteria([
            'Model'=>'Person',
            'model_filter'=> $persons_model_filter_without_limit_and_offset
        ]));
        
        $persons_model_filter['display_scopes'] = 'byName';
        
        $persons_html = $this->getAddressbookHtmlPersons($persons_model_filter, $widget_id);
        
        $this->respondOK([
            'persons_html' => $persons_html,
            'count_persons' => $persons_count
        ]);
    }
    
    public function actionShowCompanyEmployees($model_id, $company_name, $widget_id)
    {
        $uniq = SIMAHtml::uniqid();
        $persons_id = $uniq.'companies_employees';
        $employees_pagination_id = $uniq.'companies_emplyees_pagination';
        $view_path = 'base.extensions.SIMAAddressbook.views.person';
        $count_persons = Person::model()->countByAttributes(['company_id'=>$model_id]);
                
        $persons_model_filter = [
            'company'=>[
                'ids'=> $model_id,
            ],
            'limit'=>20,
            'offset'=>0
        ];
        
//        render persons.php
        $persons_view_html = $this->renderPartial($view_path, [
            'html_persons' => '',
            'persons_pagination_id' => $employees_pagination_id,
            'persons_id' => $persons_id,
            'widget_id' => $widget_id,
            'company_id' => $model_id,
            'count_persons' => $count_persons,
            'search_id' => null,
        ], true, false);
        
        $params = [
            'persons_model_filter' => $persons_model_filter,
            'company_id' => $model_id,
            'employees_dialog_id' => $persons_id,
            'employees_pagination_id' => $employees_pagination_id
        ];
        
        $json= CJSON::encode($params);
        $register_script = "$('#" . $widget_id . "').simaAddressbook('employeesPagination', $json)";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
        
        $this->respondOK(array(
            'html' => $persons_view_html,
            'company_name' => $company_name,
            'data' => ''
        ));
    }
    
    private function getAddressbookHtmlCompanies($criteria_model_filter, $widget_id)
    {
        if(isset($criteria_model_filter['display_scopes']))
        {
            if((gettype($criteria_model_filter['display_scopes']) === 'string') && ($criteria_model_filter['display_scopes'] !== 'byName'))
            {
                $criteria_model_filter['display_scopes'] = [$criteria_model_filter['display_scopes'], 'byName'];
            }
            elseif (is_array($criteria_model_filter['display_scopes']))
            {
                if (!isset($criteria_model_filter['display_scopes']['byName']) && !in_array('byName', $criteria_model_filter['display_scopes'])) // zasto bi "byName" bio key?
                {
                    $criteria_model_filter['display_scopes'] = array_merge($criteria_model_filter['display_scopes'], ['byName']);
                }
            }
        } else {
            $criteria_model_filter['display_scopes'] = 'byName';
        }
        
        $companies = Company::model()->findAll(new SIMADbCriteria([
            'Model'=>'Company',
            'model_filter'=> $criteria_model_filter
        ]));
        
        $companies_html = '';
        if(count($companies) == 0)
        {
            $companies_html .= 'Nije pronadjena ni jedna kompanija.';
        } else {
            foreach ($companies as $company) 
            {
                $companies_html .= $this->renderCompany($company, $widget_id);
            }
        }
        return $companies_html;
    }
    
    private function getAddressbookHtmlPersons($criteria_model_filter, $widget_id)
    {
        if(isset($criteria_model_filter['display_scopes']))
        {
            if((gettype($criteria_model_filter['display_scopes']) === 'string') && ($criteria_model_filter['display_scopes'] !== 'byName'))
            {
                $criteria_model_filter['display_scopes'] = [$criteria_model_filter['display_scopes'], 'byName'];
            }
            elseif (is_array($criteria_model_filter['display_scopes']))
            {
                if (!isset($criteria_model_filter['display_scopes']['byName']) && !in_array('byName', $criteria_model_filter['display_scopes'])) // zasto bi "byName" bio key?
                {
                    $criteria_model_filter['display_scopes'] = array_merge($criteria_model_filter['display_scopes'], ['byName']);
                }
            }
        } else {
            $criteria_model_filter['display_scopes'] = 'byName';
        }
        
        $persons = Person::model()->findAll(new SIMADbCriteria([
            'Model'=>'Person',
            'model_filter'=> $criteria_model_filter
        ]));
        
        $persons_html = '';
        if(count($persons) == 0)
        {
            $persons_html .= 'Nije pronadjena ni jedna osoba.';
        } else {
            foreach ($persons as $person) 
            {   
                $persons_html .= $this->renderPerson($person, $widget_id);
            }
        }
        return $persons_html;
    }
    
    public function actionPartnersToPartnerLists()
    {
        $uniq_id = SIMAHtml::uniqid();
        $title = Yii::t('BaseModule.PartnerList', 'PartnerLists');
        
        $recalc_button = '';
        if(SIMAMisc::isVueComponentEnabled())
        {
            $recalc_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title' => Yii::t('BaseModule.PartnerList', 'CalcAllSystemPartnerLists'),
                'onclick' => ['sima.addressbook.calcAllSystemPartnerLists', $uniq_id],
                'security_question' => Yii::t('BaseModule.PartnerList', 'CalcAllSystemPartnerListsYesNo')
            ], true);
        }
        else
        {
            $recalc_button = Yii::app()->controller->widget('SIMAButton', [
                'action' => [ 
                    'title' => Yii::t('BaseModule.PartnerList', 'CalcAllSystemPartnerLists'),
                    'onclick' => ['sima.addressbook.calcAllSystemPartnerLists', $uniq_id],
                    'security_questions' => Yii::t('BaseModule.PartnerList', 'CalcAllSystemPartnerListsYesNo')
                ]
            ], true);
        }

        $html = $this->renderPartnerLists(
            PartnerToPartnerList::class, 
            $title, 
            $uniq_id, 
            [
                [
                    'html'=> $recalc_button
                ],
            ], 
            ['title' => Yii::t('BaseModule.PartnerList', 'Lists')], 
            ['title' => Yii::t('BaseModule.PartnerList', 'Partners')]
        );
        
        $this->respondOk([
            'html' => $html,
            'title' => $title
        ]);
    }
    
    private function renderPartnerLists($related_model_name, $title, $uniq_id, $options = [], $left_panel_params = [], $right_panel_params = [])
    {
        $html = Yii::app()->controller->renderContentLayout([
            'name' => $title,
            'options' => $options
        ], [
            [
                'proportion' => 0.7,
                'min_width' => 500,
                'html' => $this->renderPartial($this->views_path . 'partner_lists.left_panel', array_merge($left_panel_params, [
                    'uniq_id' => $uniq_id,
                    'related_model_name' => $related_model_name
                ]), true, false)
            ],
            [
                'proportion' => 0.3,
                'min_width' => 200,
                'html' => $this->renderPartial($this->views_path . 'partner_lists.right_panel', array_merge($right_panel_params, [
                    'uniq_id' => $uniq_id,
                    'related_model_name' => $related_model_name
                ]), true, false)
            ]
        ]);
        
        return $html;
    }
    
    public function actionOnPartnerListSelectCheckParams($partner_list_id)
    {
        $show_add_button = false;
        
        $partner_list = PartnerList::model()->findByPk($partner_list_id);
        
        if (!empty($partner_list) && !$partner_list->is_system && $partner_list->hasUserAccessToList())
        {
            $show_add_button = true;
        }
        
        $this->respondOK([
            'show_add_button' => $show_add_button
        ]);
    }
    
    public function actionCalcAllSystemPartnerLists()
    {
        Yii::app()->setUpdateModelViews(false);

        $this->setEventHeader();
        
        PartnerList::calcSystemLists(function($percent) {
            $this->sendUpdateEvent($percent);
        });
        
        $this->sendEndEvent();
    }
    
    public function actionSavePartnerListMembers()
    {
        $data = $this->setEventHeader();
        
        $partner_list_id = $this->filter_input($data, 'partner_list_id');
        $related_model_name = $this->filter_input($data, 'related_model_name');
        $choosed_models = $this->filter_input($data, 'choosed_models', false);
        
        if (!empty($choosed_models))
        {
            $i = 1;
            $choosed_models_cnt = count($choosed_models);
            foreach ($choosed_models as $choosed_model)
            {
                $this->sendUpdateEvent(round(($i/$choosed_models_cnt)*100, 0, PHP_ROUND_HALF_DOWN));
                $related_model_name::addByIds($partner_list_id, $choosed_model['id']);
                $i++;
            }
        }
        
        $this->sendEndEvent();
    }
    
    public function actionPhoneNumberTypes() 
    {
        $uniq = SIMAHtml::uniqid();
        
        $html = $this->widget(SIMAGuiTable::class, [
            'id' => 'phone-number-types-table'.$uniq,   
            'model' => PhoneNumberType::class,     
            'title' => Yii::t('BaseModule.PhoneNumber', 'PhoneNumberTypes'),
            'table_settings' => true,
            'add_button' => true,
        ], true);

        $this->respondOK([
            'html' => $html,
            'title' => Yii::t('BaseModule.PhoneNumber', 'PhoneNumberTypes')
        ]);
    }
    
    private function renderPerson(Person $person, $widget_id)
    {
        $view_name = 'addressbook_basic_info';
        $person_html = Yii::app()->controller->renderModelView($person, $view_name, [
            'params'=>[
                'widget_id' => $widget_id
            ],
            'class'=>'sima-layout-panel sima-basic-info-wrapper'
        ]);
        
        return $person_html;
    }
    
    private function renderCompany(Company $company, $widget_id)
    {
        $view_name = 'addressbook_basic_info';
        $companies_html = Yii::app()->controller->renderModelView($company, $view_name, [
            'params'=>[
                'widget_id' => $widget_id,
            ],
            'class'=>'sima-layout-panel sima-basic-info-wrapper'
        ]);
        
        return $companies_html;
    }
}

