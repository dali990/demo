/* global sima */

/**
 * Prikaz browsera fajlova 
 * @param {type} $
 * @returns {undefined}
 */
(function($){
    var methods = {
        init: function(params){
            var localThis = this;
            
            if(sima.isEmpty(params.directoryDefinitionsDroplistId))
            {
                sima.addError('fileBrowser2 - init', 'empty params.directoryDefinitionsDroplistId');
                return; /// TODO: MilosJ - morao bi return, ne sme se nastaviti ako je doslo do greske
            }
                        
            localThis.data('id', params.id);
            localThis.data('tree_id', params.tree_id);
            localThis.data('tree_init_params', params.tree_init_params);
            localThis.data('location_code', params.location_code);
            localThis.data('filesGuiTableId', params.filesGuiTableId);
            localThis.data('directoryDefinitionsDroplistId', params.directoryDefinitionsDroplistId);
            localThis.data('user_id', params.user_id);
            localThis.data('searchFilterId', params.searchFilterId);
            localThis.data('init_tql', params.init_tql);
            localThis.data('current_dir_query', null);
            localThis.data('selected_dir_query', null);
            
            localThis.data('used_filters', []);
            
//            $('#'+params.tree_id).on('selectItem', {localThis:localThis}, onSelectItem);
//            $('#'+params.tree_id).on('unselectItem', {localThis:localThis}, onUnselectItem);
            $('#'+params.tree_id).on('selectionChanged', {localThis:localThis}, onSelectionChanged);
            $('#'+params.tree_id).on('loadTree', {localThis:localThis}, onLoadTree);
            
            displayUsedFilters(localThis);
            
            initDDDropdownList(localThis);
        },
        directoryDefinitionsChange: function()
        {
            var localThis = this;
            
            var new_dd_id = getSelectedDDId(localThis);
            
            var location_code = localThis.data('location_code');
            var user_id = localThis.data('user_id');
            
            sima.ajax.get('ddmanager/changeSelectedDirectoryDefinition', {
                get_params: {
                    selected: new_dd_id
                },
                data:{
                    location_code: location_code,
                    user_id: user_id
                },
                success_function: function(response) {
                    afterSetDirectoryDefinition(localThis);
                }
            });
        },
        openDDManager: function()
        {
            var localThis = this;
            
            var location_code = localThis.data('location_code');
            var user_id = localThis.data('user_id');
                                    
            var selected_dd_id = getSelectedDDId(localThis);
            
            sima.ajax.get('ddmanager/fileManager', {
                get_params: {
                    selected: selected_dd_id
                },
                data:{
                    location_code: location_code,
                    user_id: user_id
                },
                success_function: function(response) {
                    sima.dialog.openInFullScreen(response.html, response.title, {
                        close_func: function(){
                            initDDDropdownList(localThis);
                        }
                    });
                }
            });
        },
        searchFilter: function()
        {
            var localThis = this;
            
            var searchFilterId = localThis.data('searchFilterId');
                        
            var searchFilter = $('#'+searchFilterId);

            var searchText = searchFilter.simaUIFilter('getValue');
            
            searchFilter.simaUIFilter('resetFilters');
            
            if(!sima.isEmpty(searchText))
            {
                var used_filters = localThis.data('used_filters');
                used_filters.push({
                    id: sima.uniqid()+'_search_filter',
                    value: searchText,
                    type: 'FILE_NAME'
                });
                
                localThis.data('used_filters', used_filters);
                
                afterUsedFiltersChanged(localThis);
            }
        },
        remove_used_filter: function(used_filter_id)
        {            
            var localThis = this;
            
            var used_filters = localThis.data('used_filters');
            
            var new_used_filters = [];
            
            var used_filters_length = used_filters.length;
            for (var i = 0; i < used_filters_length; i++) {
                if(used_filters[i].id !== used_filter_id)
                {
                    new_used_filters.push(used_filters[i]);
                }
            }
            
            localThis.data('used_filters', new_used_filters);
            
            afterUsedFiltersChanged(localThis);
        },
        reloadFileBrowser: function()
        {
            var localThis = $(this);
            afterSetDirectoryDefinition(localThis);
        }
    };
    
    function afterUsedFiltersChanged(localThis)
    {
        var used_filters = localThis.data('used_filters');
        
        displayUsedFilters(localThis);

        var tree_id = localThis.data('tree_id');

        var list_tree_params = $('#'+tree_id).simaTree2('getListTreeParams');

        list_tree_params.used_filters = used_filters;

        $('#'+tree_id).simaTree2('setListTreeParams', list_tree_params);
        $('#'+tree_id).simaTree2('reloadTree');
        
//        list_tree_ids_files[]
        
//        populateFilesTableWithNewIds(localThis, -1);
//        populateFilesTableBySelectedQuery(localThis);
        parseDirQueryToFilesAndPopulate(localThis);
    }
    
    function displayUsedFilters(localThis)
    {
        var used_filters = localThis.data('used_filters');
        
        var used_filters_div = localThis.find('.sima-fb-used-filters').find('._filters');
        used_filters_div.empty();
                
        var used_filters_length = used_filters.length;
        for (var i = 0; i < used_filters_length; i++)
        {
            var title = '';
            
            if(used_filters[i].type === 'FILE_NAME')
            {
                title = 'Sadrzi u nazivu: '+used_filters[i].value;
            }
            else
            {
                sima.addError('sima.filebrowser2 - displayUsedFilters', 'invalid used filter type');
                /// return; /// TODO: MilosJ - morao bi return, ne sme se nastaviti ako je doslo do greske
            }
            
            var used_filter_div = $('<div class="sima-fb-used-filter">'
                    +title
                    +'<span '
                        +'class="sima-icon _recycle _16" '
                        +'onclick="$(\'#'+localThis.data('id')+'\').fileBrowser2(\'remove_used_filter\', \''+used_filters[i].id+'\');" '
                        +'title="Obrisi"></span>'
                +'</div>');
            used_filters_div.append(used_filter_div);
        }
    }
    
    function getSelectedDDId(localThis)
    {
        var directoryDefinitionsDroplistId = localThis.data('directoryDefinitionsDroplistId');
        
        if(sima.isEmpty(directoryDefinitionsDroplistId))
        {
            sima.addError('fileBrowser2 - getSelectedDDId', 'empty directoryDefinitionsDroplistId');
            /// return; /// TODO: MilosJ - morao bi return, ne sme se nastaviti ako je doslo do greske
        }
        
        var directoryDefinitionsDroplist = $('#'+directoryDefinitionsDroplistId);
        
        if(sima.isEmpty(directoryDefinitionsDroplist))
        {
            sima.addError('fileBrowser2 - getSelectedDDId', 'empty directoryDefinitionsDroplist');
            /// return; /// TODO: MilosJ - morao bi return, ne sme se nastaviti ako je doslo do greske
        }
        
        var dd_id = directoryDefinitionsDroplist.find('select').val();
        
        if(sima.isEmpty(dd_id))
        {
            sima.addError('fileBrowser2 - getSelectedDDId', 'empty dd id');
            /// return; /// TODO: MilosJ - morao bi return, ne sme se nastaviti ako je doslo do greske
        }
        
        return dd_id;
    }
    
    function initDDDropdownList(localThis)
    {
        var user_id = localThis.data('user_id');
        var location_code = localThis.data('location_code');
                
        sima.ajax.get('filebrowser2/initDDDropdownList', {
            data:{
                fileBrowser_id: localThis.data('id'),
                location_code: location_code,
                user_id: user_id
            },
            async: true,
            loadingCircle: localThis,
            relative_elements: [localThis],
            success_function: function(response) {
                
                var directoryDefinitionsDroplistId = localThis.data('directoryDefinitionsDroplistId');
                
                if(sima.isEmpty(directoryDefinitionsDroplistId))
                {
                    sima.addError('fileBrowser2 - initDDDropdownList', 'empty directoryDefinitionsDroplistId');
                    return;
                }
                
                var directoryDefinitionsDroplist = $('#'+directoryDefinitionsDroplistId);
                                
                directoryDefinitionsDroplist.html(response.dropdown_list_html);
                sima.layout.allignObject(localThis.parent(),'TRIGGER_initDDDropdownList');

                afterSetDirectoryDefinition(localThis);
            }
        });
    }
    
    function afterSetDirectoryDefinition(localThis)
    {        
        var dir_query = getRootDirQuery(localThis);

        var tree_id = localThis.data('tree_id');

        var list_tree_params = $('#'+tree_id).simaTree2('getListTreeParams');
        list_tree_params.root_query = dir_query;
        $('#'+tree_id).simaTree2('setListTreeParams', list_tree_params);
        $('#'+tree_id).simaTree2('reloadTree');
        
        loadingData = sima.showLoading($('#'+localThis.data('filesGuiTableId')), 0);
        localThis.data('reloadTreeLoadingData', loadingData);
        localThis.data('selected_dir_query', dir_query);

//        parseDirQueryToFilesAndPopulate(localThis);
    }
    
    function parseDirQueryToFilesAndPopulate(localThis)
    {
        var query = localThis.data('selected_dir_query');
        var used_filters = localThis.data('used_filters');
        
        sima.ajax.get('filebrowser2/getFilesTableDataForDirQuery', {
            data:{
                dir_query_hash: query,
                used_filters: used_filters
            },
            async: true,
            loadingCircle: $('#'+localThis.data('filesGuiTableId')),
            success_function: function(response) {
                localThis.data('current_dir_query', query);
                
                setAddButton(localThis, response.can_add_file);
                
                setFilesTableFixedFilter(localThis, response.query, query);
            }
        });
    }
    
    function setFilesTableFixedFilter(localThis, files_query, dir_query)
    {
        var filesGuiTableId = localThis.data('filesGuiTableId');
        
        var fixed_filter = $('#'+filesGuiTableId).simaGuiTable('getFixedFilter');
                
        if(sima.isEmpty(files_query))
        {
            delete fixed_filter['belongs'];
            files_query = '';
        }
        else
        {
            fixed_filter['belongs'] = files_query;
        }

        if(fixed_filter['ids'] === -1)
        {
            delete fixed_filter['ids'];
        }
        
        fixed_filter['display_scopes'] = {
            fileBrowserNameInDirQuery: dir_query,
            byNameAsc: true
        };
                
        $('#'+filesGuiTableId).simaGuiTable('setFixedFilter', fixed_filter).simaGuiTable('populate');
    }
    
//    function populateFilesTableBySelectedQuery(localThis)
//    {
//        var tree_id = localThis.data('tree_id');
//        var selected_leaf_data = $('#'+tree_id).simaTree2('getSelectedItemData');
//        
//        parseDirQueryToFilesAndPopulate(localThis, selected_leaf_data.query);
//    }
    
//    function onSelectItem(event, tree_item)
//    {
//        var localThis = event.data.localThis;
//        var tree_item_data = tree_item.data();
//        parseDirQueryToFilesAndPopulate(localThis, tree_item_data.query);
//        
//    }
    
//    function onUnselectItem(event)
//    {
//        var localThis = event.data.localThis;
//        
//        var dir_query = getRootDirQuery(localThis);
//
//        parseDirQueryToFilesAndPopulate(localThis, dir_query);
//    }

    function onSelectionChanged(event, type, tree_item)
    {
        var localThis = event.data.localThis;
                
        if(type === 'SELECTED' || type === 'UNSELECTED')
        {
            var dir_query = null;
            if(type === 'SELECTED')
            {
                var tree_item_data = tree_item.data();
                dir_query = tree_item_data.query;
            }
            else if(type === 'UNSELECTED')
            {
                dir_query = getRootDirQuery(localThis);
            }
            
            localThis.data('selected_dir_query', dir_query);
            
            parseDirQueryToFilesAndPopulate(localThis);
        }
        else
        {
            sima.addError('fileBrowser2 onSelectionChanged', sima.translate('FileBrowserOnSelectionChangedInvalidType'));
            /// return; /// TODO: MilosJ - morao bi return, ne sme se nastaviti ako je doslo do greske
        }
    }
    
    function onLoadTree(event, type, tree_item)
    {
        var localThis = event.data.localThis;
        
        loadingData = localThis.data('reloadTreeLoadingData');
        sima.hideLoading($('#'+localThis.data('filesGuiTableId')), loadingData);
        localThis.data('reloadTreeLoadingData', null);
        
        parseDirQueryToFilesAndPopulate(localThis);
    }
    
//    function onUpdateSelectedLeafContent(event)
//    {
//        var localThis = event.data.localThis;
//        populateFilesTableBySelectedQuery(localThis);
//    }
    
    function setAddButton(localThis, can_add_file)
    {
        var filesGuiTableId = localThis.data('filesGuiTableId');
        if(can_add_file === true)
        {
            var current_dir_query = localThis.data('current_dir_query');
            sima.ajax.get('filebrowser2/getPrepareDataForAddFile', {
                data: {
                    query: current_dir_query
                },
                async: true,
                loadingCircle: $('#'+filesGuiTableId),
                success_function: function(response) {
                    var belong_tags = [];

                    $.each(response.tags, function(index, value) {
                        var curr_tag = {};
                        curr_tag.id = value.id;
                        curr_tag.display_name = value.display_name;
                        curr_tag.class = value.class;
                        belong_tags.push(curr_tag);
                    });

                    var document_type_id = response.document_type_id;
                    var document_type_chage_disabled = true;
                    if(sima.isEmpty(document_type_id))
                    {
                        document_type_chage_disabled = false;
                    }

                    $('#'+filesGuiTableId).simaGuiTable('setAddButton',{
                        init_data:{
                            File: {
                                file_belongs: {
                                    'ids': {
                                        'ids' : belong_tags
                                    }
                                }, 
                                document_type_id:{
                                    'disabled':document_type_chage_disabled,
                                    'init':document_type_id
                                },
                                static_fix_dir_id: response.static_fix_dir_id
                            }
                        }
                    });
                }
            });
        }
        else
        {
            $('#'+filesGuiTableId).simaGuiTable('setAddButton');
        }
    }
    
    function getRootDirQuery(localThis)
    {
        var dd_id = getSelectedDDId(localThis);
        
        if(sima.isEmpty(dd_id))
        {
            sima.addError('fileBrowser2 - getRootDirQuery', 'empty dd id');
            /// return; /// TODO: MilosJ - morao bi return, ne sme se nastaviti ako je doslo do greske
        }
        
        var dir_query = '$$'+dd_id+'$$';
        
        var init_tql = localThis.data('init_tql');
        if(!sima.isEmpty(init_tql))
        {
            /// ne radi se + '!!'
            /// jer je inittql hash-ovan
            /// a dosadasnji dirquery je samo dd id
            dir_query += init_tql;
        }
        
        return dir_query;
    }
    
    $.fn.fileBrowser2 = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.fileBrowser2');
            }
        });
        return ret;
    };
})( jQuery );