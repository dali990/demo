/* global sima */

/**
 * File Manager je interfejs za sklapanje definicija koja sluze prikazivanju foldera
 * @param {type} $
 * @returns {undefined}
 */
(function($){
    
    
    var methods = {
        /**
         * inicijalizacija pocetnih parametra
         * @param {type} params
         * @returns {undefined}
         */
        init: function(params){            
            var _this = this;
                        
            this.data('user_id', params.user_id);
            this.data('company_id', params.company_id);
            this.data('location_code', params.location_code);
            this.data('dd_creator_id', params.dd_creator_id);
                        
            this.data('selected_id', params.selected);
            treeChange(_this, false);
            this.data('multiselect', false);
                                    
            _this.find('.sima-ui-file-manager-directory_definitions .directory_definition').on('click', {localThis:_this}, selectDirectoryDefinitionHendler);
            _this.find('.sima-ui-file-manager-directory_definitions .header ._add').on('click', {localThis:_this}, addDirectoryDefinitionHendler);
//            _this.find('.sima-ui-file-manager-directory_definitions .header ._copy').on('click', {localThis:_this}, copyDirectoryDefinitionHendler);
            _this.find('.sima-ui-file-manager-directory_definitions .header ._delete').on('click', {localThis:_this}, deleteDirectoryDefinitionHendler);
//            _this.find('.sima-ui-file-manager-directory_definitions .header ._export').on('click', {localThis:_this}, exportDirectoryDefinitionsHendler);
            _this.find('.sima-ui-file-manager-directory_definitions .header ._edit').on('click', {localThis:_this}, editDirectoryDefinitionsHendler);
            _this.find('.sima-ui-file-manager-directory_definitions .definition_attributes input[type=checkbox]').on('click', {localThis:_this}, definitionAttributeCheckBoxChange);
            $('#ui-dialog-file-manager-wiki').on('click',{browser:this},wikiHelp);
          
            $('#'+this.data('dd_creator_id')).on('saveDefinition', {localThis:_this}, saveTreeJson);
            $('#'+this.data('dd_creator_id')).on('changeTree', {localThis:_this}, onChangeTree);
                        
            if (this.data('selected_id') !== undefined)
            {
                var li = this.find('.sima-ui-file-manager-directory_definitions ul').find("[data-id='" + this.data('selected_id') + "']");
                selectDirectoryDefinition(this, li);
            }
        },
        importDefinitions: function()
        {
            var _this = this;
            if (_this.data('changed'))
            {
                sima.dialog.openYesNo("Imate nesacuvanu definiciju! Ako nastavite definicija nece biti sacuvana.", function(){
                    sima.dialog.close();
                    importDefinitions(_this);
                });
            }
            else
            {
                importDefinitions(_this);
            }
        }
    };   
    
//    function importDefinitions(_this)
//    {
//        sima.ajax.get('ddmanager/importDefinitions', {
//            data: {
//                user_id: _this.data('user_id'),
//                company_id: _this.data('company_id')
//            },
//            async:true,
//            loadingCircle:$('body'),
//            success_function: function(response)
//            {
//                refreshDirectoryDefinitionsList(_this);
//                _this.find('.sima-ui-file-manager-directory_definitions .directory_definition').on('click', {tree:_this.find(".sima-ui-file-manager-tree"), localThis:_this}, selectDirectoryDefinitionHendler);
//            }
//        });
//    }
    
//    //dodaje drag za directory definition
//    function makeDirectoryDefinitionDraggable(_this)
//    {
//        _this.find('.sima-ui-file-manager-directory_definitions li.directory_definition span.name').draggable({
//                delay: 200,
//                revert: 'invalid',
//                appendTo: '.ui-dialog',
//                helper: 'clone',
//                containment: 'window',
//                scroll: false,	
//                start: function(e, ui) {
//                    if ($(this).parent().hasClass('active'))
//                    {
//                        return false;   
//                    }
//                    $(ui.helper).addClass('sima-ui-file-manager-drag');
//                },
//                stop: function(e, ui) {
//                    $(ui.helper).removeClass('sima-ui-file-manager-drag');
//                }
//        });
//    }
    
//    function treeRecursive(tree)
//    {
//        var obj = [];
//        tree.find('ul:first').children().each(function(){
//            var temp = [];
//            temp = $(this).data();
//            if ($(this).children('ul').length > 0)
//            {
//                temp.children = treeRecursive($(this));
//                obj.push(temp);
//            }
//            else
//            {
//                obj.push(temp);
//            }
//        });
//        return obj;
//    }
    
    function definitionAttributeCheckBoxChange(event)
    {
        var definition = event.data.localThis.find('.sima-ui-file-manager-directory_definitions .directory_definition.active');
        definition.data($(this).attr('id'), $(this).is(':checked'));
        treeChange(event.data.localThis, true);
    }
    
    //ucitava deiniciju kada se klikne na nju
    function selectDirectoryDefinitionHendler(event)
    {
        var definition = $(this);
        var localThis = event.data.localThis;
      
        event.stopPropagation();

        if(event.ctrlKey) 
        {
            if (localThis.data('changed'))
            {
                sima.dialog.openYesNo("Imate nesacuvanu definiciju! Ako nastavite definicija nece biti sacuvana.", function(){
                    sima.dialog.close();
                    disableTree(localThis);
                    definition.toggleClass('active');
                    if (localThis.find('.sima-ui-file-manager-directory_definitions li.active').length)
                        localThis.find('.sima-ui-file-manager-directory_definitions .header ._delete').removeClass('_disabled');
                    localThis.data('multiselect', true);
                });
            }
            else
            {
                disableTree(localThis);
                definition.toggleClass('active');
                if (localThis.find('.sima-ui-file-manager-directory_definitions li.active').length)
                        localThis.find('.sima-ui-file-manager-directory_definitions .header ._delete').removeClass('_disabled');
                localThis.data('multiselect', true);
            }
        }
        else
        {
            selectDirectoryDefinition(localThis, definition);
            localThis.data('multiselect', false);
        }
    }
    
    function selectDirectoryDefinition(localThis, definition)
    {
        //ako je vec selektovana definicija, deselektuj je
        if (definition.hasClass('active'))
        {
            if (localThis.data('multiselect') === true)
            {
                localThis.find('.sima-ui-file-manager-directory_definitions li.active').removeClass('active');
                definition.addClass('active');
                loadDefinitionAttributes(localThis);
                loadDDTree(localThis, definition.data('id'));
            }
            else
            {
                if (localThis.data('changed'))
                {
                    sima.dialog.openYesNo("Imate nesacuvanu definiciju! Ako nastavite definicija nece biti sacuvana.", function(){
                        sima.dialog.close();
                        definition.removeClass('active');
                        disableTree(localThis);
                    });
                }
                else
                {
                    definition.removeClass('active');
                    disableTree(localThis);
                }
            }
        }
        else
        {
            if (localThis.data('changed'))
            {
                sima.dialog.openYesNo("Imate nesacuvanu definiciju! Ako nastavite definicija nece biti sacuvana.", function(){
                    sima.dialog.close();
                    localThis.find('.sima-ui-file-manager-directory_definitions li.active').removeClass('active');
                    definition.addClass('active');
                    loadDefinitionAttributes(localThis);
                    loadDDTree(localThis, definition.data('id'));
                });
            }
            else
            {
                localThis.find('.sima-ui-file-manager-directory_definitions li.active').removeClass('active');
                localThis.data('selected_id', definition.data('id'));
                definition.addClass('active');
                loadDefinitionAttributes(localThis);
                enableTree(localThis);
                loadDDTree(localThis, definition.data('id'));
            }
        }
    }
    
    function loadDDTree(localThis, dd_id)
    {
        treeChange(localThis, false)
        sima.ajax.get('ddmanager/loadDDTree', {
            get_params: {
                dd_id: dd_id
            },
            data: {
            },
            success_function: function(response) {
                var params = {
                    tree_json: response.tree_json,
                    tree_name: response.tree_name
                };
                $('#'+localThis.data('dd_creator_id')).ddCreator('loadTree', params);
            }
        });
    }
    
    function saveTreeJson(event, treeObject)
    {
        var localThis = event.data.localThis;
        var definition = {
            name: getDefinitionName(localThis, localThis.data('selected_id')),
            children: treeObject,
            attributes: {}
        };
        var definition_attributes = localThis.find('.sima-ui-file-manager-directory_definitions .definition_attributes');
        definition.attributes.show_unlisted = definition_attributes.find('#show_unlisted').is(":checked");
        
        sima.ajax.get('ddmanager/createDirectoryDefinition', {
            data: {
                id: localThis.data('selected_id'), 
                tree:definition,
                user_id: localThis.data('user_id'),
                company_id: localThis.data('company_id')
            },
            success_function: function(response) {
                sima.dialog.openInfo('Definicija je sacuvana!');
                treeChange(localThis, false);
            }
        });
    }
    
    function addDirectoryDefinitionHendler(event)
    {
        if (event.data.localThis.data('changed'))
        {
            sima.dialog.openYesNo("Imate nesacuvanu definiciju! Ako nastavite definicija nece biti sacuvana.", function(){
                sima.dialog.close();
                addDefinition(event.data.localThis, event.data.tree);
            });
        }
        else
        {
            addDefinition(event.data.localThis, event.data.tree);
        }
    }
    
//    function copyDirectoryDefinitionHendler(event)
//    {
//        if (!$(this).hasClass('_disabled'))
//        {
//            if (event.data.localThis.find('.sima-ui-file-manager-directory_definitions li.active').length === 0)
//            {
//                sima.dialog.openInfo('Morate da selektujete definiciju koju hocete da kopirate.');
//            }
//            else
//            {
//                if (event.data.localThis.data('isChanged'))
//                {
//                    sima.dialog.openYesNo("Imate nesacuvanu definiciju! Ako nastavite definicija nece biti sacuvana.", function(){
//                        sima.dialog.close();
//                        addDefinition(event.data.localThis, event.data.tree, true);
//                    });
//                }
//                else
//                {
//                    addDefinition(event.data.localThis, event.data.tree, true);
//                }
//            }
//        }
//    }
    
    function addDefinition(localThis, tree, isCopied)
    {
        sima.dialog.openPrompt('', function(input_value){
            if (input_value !== '' && input_value !== null)
            {
                treeChange(localThis, false);
                sima.ajax.get('ddmanager/addDirectoryDefinition', {
                    get_params: {
                        name: input_value
                    },
                    data:{
                        user_id: localThis.data('user_id'),
                        company_id: localThis.data('company_id')
                    },
                    success_function: function(response) {
                        var li = $("<li class='directory_definition' data-id='"+response.id+"' data-show_unlisted='false'><span class='icon'></span><span class='name'>"+input_value+"</span></li>");
                        li.on('click',{tree:tree, localThis:localThis}, selectDirectoryDefinitionHendler);
                        localThis.find('.sima-ui-file-manager-directory_definitions ul').append(li);
//                        if (isCopied !== undefined && isCopied === true)
//                            saveDefinition(localThis, response.id, input_value);
                        selectDirectoryDefinition(localThis, li);
                    }
                });
            }
        }, 
        {
            text:'Unesite naziv definicije:',
            type:'input'
        });
    }
    
    function deleteDirectoryDefinitionHendler(event)
    {
        if (!$(this).hasClass('_disabled'))
        {
            if (event.data.localThis.find('.sima-ui-file-manager-directory_definitions li.active').length === 0)
            {
                sima.dialog.openInfo('Morate da selektujete definiciju koju hocete da obrisete.');
            }
            else
            {
                var selected_dd_ids = '';
                event.data.localThis.find('.sima-ui-file-manager-directory_definitions li.active').each(function(){
                    selected_dd_ids += $(this).data('id') + ';';
                });
                selected_dd_ids = selected_dd_ids.slice(0,-1);
                
                var selectedDefinition = isSelected(event.data.localThis, selected_dd_ids);
                if (selectedDefinition !== '')
                {
                    sima.dialog.openInfo(sima.translate('DDManagerDefinitionCurrentlySelected', {
                        '{name}': selectedDefinition
                    }));
                }
                else
                {
                    var pointerHtml = isPointer(event.data.localThis, selected_dd_ids);
                    if (pointerHtml !== '')
                    {
                        sima.dialog.openCustom(pointerHtml, '');
                        sima.dialog.Center();
                    }
                    else
                    {
                        sima.dialog.openYesNo("Da li ste sigurni da zelite da obrisete?", function(){
                            sima.dialog.close();
                            sima.ajax.get('ddmanager/deleteDirectoryDefinitions', {
                               get_params: {
                                   ids: selected_dd_ids
                               },
                               data: {
                                   user_id: event.data.localThis.data('user_id'),
                                   company_id: event.data.localThis.data('company_id')
                               },
                               success_function: function(response) {
                                   event.data.localThis.find('.sima-ui-file-manager-directory_definitions li.active').remove();
                                   disableTree(event.data.localThis);
                               }
                            });
                        });
                    }
                }
            }
        }
    }
    
    function isPointer(localThis, selected_dd_ids)
    {
        var html = '';
        sima.ajax.get('ddmanager/isPointer', {
           get_params: {
               ids: selected_dd_ids
           },
           data: {
               user_id: localThis.data('user_id'),
               company_id: localThis.data('company_id')
           },
           success_function: function(response) {
               html = response.html;
           }
        });
        
        return html;
    }
    
    function isSelected(localThis, selected_dd_ids)
    {
        var state;
        sima.ajax.get('ddmanager/isSelectedDefinition', {
           get_params: {
               ids: selected_dd_ids
           },
           data: {
               user_id: localThis.data('user_id'),
               company_id: localThis.data('company_id'),
               location_code: localThis.data('location_code')
           },
           success_function: function(response) {
               state = response.state;
           }
        });
        
        return state;
    }
    
    function onChangeTree(event)
    {
        treeChange(event.data.localThis, true);
    }
    
    function treeChange(_this, new_status)
    {
        _this.data('changed', new_status);
        
        _this.find('.sima-ui-file-manager-directory_definitions li .changed').remove();
        
        if(new_status === true)
        {
            _this.find('.sima-ui-file-manager-directory_definitions li.active').append('<span class="changed"> *</span>');
        }
        
    }
    
    function disableTree(localThis)
    {
        disableToolbarOptions(localThis);
        unLoadDefinitionAttributes(localThis);
        
        $('#'+localThis.data('dd_creator_id')).ddCreator('disableTree');
    }
    
    function enableTree(localThis)
    {
        enableToolbarOptions(localThis);
        
        $('#'+localThis.data('dd_creator_id')).ddCreator('enableTree');
    }
    
    function enableToolbarOptions(localThis)
    {
        localThis.find('.sima-ui-file-manager-directory_definitions .header ._edit').removeClass('_disabled');
//        localThis.find('.sima-ui-file-manager-directory_definitions .header ._copy').removeClass('_disabled');
        localThis.find('.sima-ui-file-manager-directory_definitions .header ._delete').removeClass('_disabled');
    }
    
    function disableToolbarOptions(localThis)
    {
        localThis.find('.sima-ui-file-manager-directory_definitions .header ._edit').addClass('_disabled');
//        localThis.find('.sima-ui-file-manager-directory_definitions .header ._copy').addClass('_disabled');
        localThis.find('.sima-ui-file-manager-directory_definitions .header ._delete').addClass('_disabled');
    }
    
    function loadDefinitionAttributes(localThis)
    {
        var definition_attributes = localThis.find('.sima-ui-file-manager-directory_definitions .definition_attributes');
        var definition = localThis.find('.sima-ui-file-manager-directory_definitions .directory_definition.active');
        var show_unlisted = definition.data('show_unlisted');

        definition_attributes.find('#show_unlisted').prop('checked', show_unlisted);
        definition_attributes.find('.attribute_item').each(function(){
            $(this).fadeIn('slow');
        });
        definition_attributes.addClass('active');
    }
    
    function unLoadDefinitionAttributes(localThis)
    {
        var definition_attributes = localThis.find('.sima-ui-file-manager-directory_definitions .definition_attributes');
        definition_attributes.find('.attribute_item').each(function(){
            $(this).fadeOut('slow');
        });
        definition_attributes.removeClass('active');
    }
    
//    function exportDirectoryDefinitionsHendler(event)
//    {
//        if (event.data.localThis.find('.sima-ui-file-manager-directory_definitions li.active').length !== 0)
//        {
//            var ids = '';
//
//            event.data.localThis.find('.sima-ui-file-manager-directory_definitions li.active').each(function(){
//                ids += $(this).data('id') + ';';
//            });
//            ids = ids.slice(0,-1);
//            
//            var pointerHtml = definitionHasPointers(event.data.localThis);
//            if (pointerHtml !== '')
//            {
//                sima.dialog.openCustom(pointerHtml, '');
//                sima.dialog.Center();
//            }
//            else
//            {
//                window.location=sima.getBaseUrl()+'/index.php?r=ddmanager/exportDefinitions'+'&ids='+ids;
//            }
//        }
//        else
//        {
//            sima.dialog.openInfo('Niste selektovali definicije koje hocete da izvezete!');
//        }
//    }
    
//    function definitionHasPointers(localThis)
//    {
//        var ids = '';
//        localThis.find('.sima-ui-file-manager-directory_definitions li.active').each(function(){
//            ids += $(this).data('id') + ';';
//        });
//        ids = ids.slice(0,-1);
//        var html = '';
//        sima.ajax.get('ddmanager/definitionPointers', {
//           get_params: {
//               ids: ids
//           },
//           success_function: function(response) {
//               html = response.html;
//           }
//        });
//        
//        return html;
//    }
    
    function editDirectoryDefinitionsHendler(event)
    {
        if (!$(this).hasClass('_disabled'))
        {
            sima.dialog.openPrompt('', function(input_value){
                if (input_value !== '' && input_value !== null)
                {
                    sima.ajax.get('ddmanager/renameDirectoryDefinition', {
                        get_params: {
                            id: event.data.localThis.data('selected_id'),
                            name: input_value
                        },
                        success_function: function(response) {
                            event.data.localThis.find('.sima-ui-file-manager-directory_definitions li.active .name').html(input_value);
//                            event.data.localThis.find('.sima-ui-file-manager-tree-options .directory_definition_name').val(input_value);
                        }
                     });
                }
            }, 
            {
                text:'Unesite novi naziv definicije:',
                type:'input',
                default_option:event.data.localThis.find(".sima-ui-file-manager-tree-options .directory_definition_name").val()
            });
        }
    }
    
//    function refreshDirectoryDefinitionsList(_this)
//    {
//        sima.ajax.get('ddmanager/refreshDirectoryDefinitionsList', {
//            data: {
//                user_id: _this.data('user_id'),
//                company_id: _this.data('company_id')
//            },
//            success_function: function(response) {
//                _this.find('.sima-ui-file-manager-directory_definitions ul').replaceWith(response.html);
//            }
//         });
//    }
    
    function wikiHelp(event)
    {
        sima.icons.docWiki($(this),"SIMA_uputstvo_-_DD_Menadzer");
    }
    
//    function isDefinitionInCircle(parent, child)
//    {
//        var state;
//        sima.ajax.get('ddmanager/isDefinitionInCircle', {
//            get_params: {
//                parent: parent,
//                child: child
//            },
//            success_function: function(response) {
//                if (response.state === true)
//                {
//                    state = true;
//                }
//                else
//                {
//                    state = false;
//                }
//            }
//         });
//
//         return state;
//    }

    function getDefinitionName(localThis, id)
    {
        var dds = localThis.find('.sima-ui-file-manager-directory_definitions li.directory_definition');
        for(var i=0; i<dds.length; i++)
        {
            var obj = $(dds[i]);
            if(obj.data('id') === id)
            {
                return obj.find('span.name').text();
            }
        }
    }
    
    $.fn.ddManager = function(methodOrOptions) {
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.ddManager' );
        }    
    };  
    
})( jQuery );