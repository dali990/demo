/* global sima */

/**
 * Prikaz browsera fajlova 
 * @param {type} $
 * @returns {undefined}
 */
(function($){
    var methods = {
        /**
         * inicijalizacija pocetnih parametra
         * 
         * @param {type} params
         * @returns {undefined}
         */
        init: function(params){
            var localThis = this;
            var folder_tree_options =  this.find(".sima-ui-tree-wrapper .options");
            var folder_tree =  this.find(".sima-ui-tree");
            var files_display = this.find(".sima-ui-fb-files");
            var files_toolbar = this.find(".sima-ui-fb-files-toolbar");
            var files_from_parents_cp = this.find(".files-from-parents-cb");
            var short_path_cp = this.find(".short-path-cb");
            var exclude_empty_cp = this.find(".exclude-empty-cb");
            
            files_from_parents_cp.attr("checked", false);
            short_path_cp.attr("checked", false);
            exclude_empty_cp.attr("checked", false);
  
            this.data('user_id', params.user_id);
            this.data('company_id', params.company_id);
            this.data('location_code', params.location_code);
                        
            this.data('folder_tree_options', folder_tree_options);
            this.data('folder_tree', folder_tree);
            this.data('files_display', files_display);
            this.data('files_toolbar', files_toolbar);
            
            this.data('filters_had_change', false);
            this.data('files_from_parents', 'false');
            this.data('short_path', 'false');
            this.data('exclude_empty', 'false');
            
            this.data('iconSize', 20);
            this.data('name_size_type_datamodifiedSize', 150);
            
            this.data('root_id', folder_tree.data('leaf-id'));
            
            this.data('multiselect',false);
            
            this.data('shiftFirstSelected', null);
            this.data('ctrlLastSelected', null);
            this.data('filteredCond', null);
            this.data('filteredCond_old', null);
            this.data('filteredCondArray', []);
            this.data('filteredDisplayArray', []);
            
            var alreadyclickedTimeout;
            var alreadyclicked = false, clickDelay = 300; //milliseconds //TODO: moze da se postavi kao podesavanje
            files_display.delegate('tbody tr.sima-ui-fb-file', 'click', function(event) {
                var file = $(this);
                if (alreadyclicked)
                {
                    alreadyclicked = false; // reset
                    clearTimeout(alreadyclickedTimeout); // prevent this from happening
                    open_file(file, this);
                }
                else
                {
                    alreadyclicked = true;
                    alreadyclickedTimeout = setTimeout(function() {
                        alreadyclicked = false; // reset when it happens  
                    fileSelect(event, localThis, file);
                    }, clickDelay); // <-- dblclick tolerance here
                }
            });
              
            this.data('files_button_add', files_toolbar.children('.button.add'));
            this.data('files_button_edit', files_toolbar.children('.button.edit'));
            this.data('files_button_new_tab', files_toolbar.children('.button.new_tab'));
            this.data('files_button_dialog', files_toolbar.children('.button.dialog'));
            this.data('files_button_delete', files_toolbar.children('.button.delete'));
            this.data('files_button_download', files_toolbar.children('.button.download'));
            this.data('files_button_download_zip', files_toolbar.children('.button.download_zip'));
            this.data('files_file_open', files_toolbar.children('.button.file_open'));
            this.data('folder_button_add', folder_tree_options.find('.sima-icon._add._16'));
            this.data('folder_button_edit', folder_tree_options.find('.sima-icon._edit._16'));
            this.data('folder_button_delete', folder_tree_options.find('.sima-icon._delete._16'));
            this.data('folder_button_create_dd', folder_tree_options.find('.sima-ui-tree-button-create_dd'));
            this.data('directory_definition', folder_tree_options.find('.sima-ui-tree-dd-list'));
            this.data('filters_button', folder_tree_options.find('.sima-ui-button.filters'));
            this.data('filters_wrapper', folder_tree_options.find('.filters-wrapper'));
            this.data('used_filters', folder_tree_options.find('.used-filters'));
            
            this.data('files_button_add').on('click',{browser:this,files_display:files_display},filesButtonAddHandler);
            this.data('files_button_edit').on('click',{browser:this,files_display:files_display},filesButtonEditHandler);
            this.data('files_button_new_tab').on('click',{browser:this,files_display:files_display},filesButtonNewTabHandler);
            this.data('files_button_dialog').on('click',{browser:this,files_display:files_display},filesButtonDialogHandler);
            this.data('files_button_delete').on('click',{browser:this,files_display:files_display},filesButtonDeleteHandler);
            this.data('files_button_download').on('click',{browser:this,files_display:files_display},filesButtonDownloadHandler);
            this.data('files_button_download_zip').on('click',{browser:this,files_display:files_display},filesButtonDownloadZipHandler);
            this.data('files_file_open').on('click',{browser:this,files_display:files_display},filesButtonFileOpenHandler);
            this.data('folder_button_add').on('click',{browser:this, tree:folder_tree},folderButtonAddHandler);
            this.data('folder_button_edit').on('click',{browser:this, tree:folder_tree},folderButtonEditHandler);
            this.data('folder_button_delete').on('click',{browser:this, tree:folder_tree},folderButtonDeleteHandler);
            this.data('folder_button_create_dd').on('click',{browser:this, tree:folder_tree},folderButtonCreateDdHandler);
            this.data('directory_definition').on('change',{browser:this, tree:folder_tree},directoryDefinitionHendler);
            this.data('filters_button').on('click',{browser:this, tree:folder_tree},filtersButtonHandler);
            
            this.data('description_file', this.find(".sima-disk-description-file"));
            this.data('description_folder', this.find(".sima-disk-description-folder"));
            this.data('description_folder').find('.name_folder').on('click','img',{browser:this,tree:folder_tree},folderButtonNewTabHandler);
            
            if (this.data('folder_tree').data('leaf-id') !== undefined && this.data('folder_tree').data('leaf-id') !== '')
            {
                this.data('folder_tree').fileBrowser('refresh_tree', localThis);
            }

            if (folder_tree.data('can-add-dir-to-root') !== undefined 
                    && (folder_tree.data('can-add-dir-to-root') === true
                        || folder_tree.data('can-add-dir-to-root') === 'true'
                        || folder_tree.data('can-add-dir-to-root') === 1
                        || folder_tree.data('can-add-dir-to-root') === '1'))
            {
                toolbarStatus(localThis,'can_add_dir_to_root');
            }

            if (folder_tree.data('can-add-fix-dir') !== undefined 
                    && (folder_tree.data('can-add-fix-dir') === true
                        || folder_tree.data('can-add-fix-dir') === 'true'
                        || folder_tree.data('can-add-fix-dir') === 1
                        || folder_tree.data('can-add-fix-dir') === '1'))
            {
                toolbarStatus(localThis,'can_add_fix_dir');
            }
            
            if (folder_tree.data('can-add-file-to-root') !== undefined 
                    && (folder_tree.data('can-add-file-to-root') === true
                        || folder_tree.data('can-add-file-to-root') === 'true'
                        || folder_tree.data('can-add-file-to-root') === 1
                        || folder_tree.data('can-add-file-to-root') === '1'))
            {
                toolbarStatus(localThis,'can_add_file_to_root');
            }
            
            folder_tree.on('click','span.sima-ui-tree-title',function(){
                var _this = $(this);
                var localThisParent = $(this).parent();
                var currentSelected = _this.parents('.sima-ui-tree').find('li.selected');
                currentSelected.removeClass('selected');
                if (!localThisParent.hasClass('sima-ui-tree-leaf-opened'))
                {
                    localThisParent.addClass('sima-ui-tree-leaf-opened');
                }
                if (localThisParent.data('leaf-id') !== currentSelected.data('leaf-id'))
                {
                    localThis.fileBrowser('select_leaf',localThisParent);
                }
                else
                {
                    if (folder_tree.data('can-add-fix-dir') === true)
                    {
                        toolbarStatus(localThis,'can_add_fix_dir');
                    }
                    else
                    {
                        toolbarStatus(localThis,'cannot_add_fix_dir');
                    }
                    
                    if (folder_tree.data('can-add-file-to-root') === true)
                    {
                        toolbarStatus(localThis,'can_add_file_to_root');
                    }
                    else
                    {
                        toolbarStatus(localThis,'cannot_add_file_to_root');
                    }
                    
                    if (folder_tree.data('can-rename-fix-dir') === true)
                    {
                        toolbarStatus(localThis,'can_rename_fix_dir');
                    }
                    else
                    {
                        toolbarStatus(localThis,'cannot_rename_fix_dir');
                    }
                    
                    if (folder_tree.data('fix-dir-type') !== '')
                    {
                        toolbarStatus(localThis,'can_delete_fix_dir');
                    }
                    else
                    {
                        toolbarStatus(localThis,'cannot_delete_fix_dir');
                    }
                    
                    toolbarStatus(localThis,'root_folder');   
                }
            });
            
            folder_tree.on('click','span.sima-ui-tree-icon',function(){
                var _this = $(this);
                var localThisParent = $(this).parent();
                localThisParent.toggleClass('sima-ui-tree-leaf-opened');
                var selected_children = localThisParent.find('li.selected');
                if (selected_children.size()>0)
                {
                        selected_children.removeClass('selected');
                        localThis.fileBrowser('select_leaf',localThisParent);
                }
                else
                {
                    var selected = _this.parents('.sima-ui-tree').find('li.selected');
                    if (localThisParent.hasClass('sima-ui-tree-leaf-opened'))
                    {
                        selected.removeClass('selected');
                        localThis.fileBrowser('select_leaf',localThisParent);
                    }
                } 
            });
            setTimeout(function(){
                set_columns_width(localThis, localThis.data('files_display'));
                localThis.data('files_display').find('table').addClass('tablesorter');
                localThis.data('files_display').find('table').tablesorter({headers: { data_modified: { sorter: 'shortDate'}, size: { sorter: 'digit' }}});
            }, 10);
            setTimeout(function(){
                makeResizable(localThis, localThis.data('files_display').find('table'));
            }, 20);
            
            this.data('filters_wrapper').on("click", "li", {browser:this, tree:folder_tree}, filtersElementClicked); 
            this.data('filters_wrapper').on("change", "li input[type='checkbox']", {browser:this, tree:folder_tree}, filtersCheckboxElementChanged); 
            this.data('filters_wrapper').on("keyup", "li input[type='text']", {browser:this, tree:folder_tree}, filtersTextElementChanged);
            
            files_from_parents_cp.on('change', {browser:this, tree:folder_tree}, filesFromParentsCbChanged);
            short_path_cp.on('change', {browser:this, tree:folder_tree}, shortPathCbChanged);
            exclude_empty_cp.on('change', {browser:this, tree:folder_tree}, excludeEmptyCbChanged);
        },
        
        refresh_tree: function(leaf)
        {
            var localThis = this;
            
            var query = '';
            if(localThis.data('leaf-id') !== undefined && localThis.data('leaf-id') !== null)
            {
                query = localThis.data('leaf-id');
            }
            
            localThis.fileBrowser('list_tree', leaf, query);
        },
        
        list_tree : function(_this, id)
        {
            var localThis = this;
            var localUl = localThis.children('ul');
            var leaf_id;
            if (typeof id !== 'undefined')
            {
                leaf_id = id;
            }
            else 
            {
                leaf_id = localThis.data('leaf-id');
            }
            
            var filteredCond = _this.data('filteredCond');
            if(filteredCond !== null && filteredCond !== '')
            {
                _this.data('filteredCond', '');
                _this.data('filteredCond_old', filteredCond);
                leaf_id += '!!'+filteredCond;
            }
            var dir_type = 'Tip: ';
            if( localThis.data('fix-dir-type') === 'FIX_DIR_STATIC')
            {
                dir_type = dir_type.concat('Staticki fiksan direktorijum');
            }
            else if( localThis.data('fix-dir-type') === 'FIX_DIR_DYNAMIC')
            {
                dir_type = dir_type.concat('Dinamicki fiksan direktorijum');
            }
            
            var files_from_parents = _this.data('files_from_parents');
            var short_path = _this.data('short_path');
            var exclude_empty = _this.data('exclude_empty');
                        
            sima.ajax.get('filebrowser/list',{
                get_params:{
                    files_from_parents:files_from_parents,
                    short_path:short_path,
                    exclude_empty:exclude_empty
                },
                data:{
                    query:leaf_id
                },
                async: true,
                loadingCircle: [_this.find('.sima-ui-tree').parent(), _this.find('.sima-ui-fb-files'), _this.find('.description-wrapper')],
                success_function:function(data)
                {
                    localUl.html(data.tree);
                    _this.data('files_display').find('tbody').html(data.files);
                    _this.data('files_toolbar').find('.files_count .number').html(data.files_count);
                    makeFileDraggable(_this);
                    makeFolderDraggable(_this);
                    makeFileFolderDroppable(_this);
                    setTimeout(function(){
                        makeResizable(_this, _this.data('files_display').find('table'));
                    }, 200);
                    _this.data('files_display').find('table').trigger("update");
                    _this.data('description_folder').find('.name_folder').empty().prepend(localThis.children('.sima-ui-tree-title').html());
                    _this.data('description_folder').find('.dir_type_wrapper').empty().prepend(dir_type);
                    if (data.id !== '' && data.action !== '')
                    {
                        _this.data('description_folder').find('.name_folder').append(" <img data-id='"+data.id+"' data-action='"+data.action+"' class='button new_tab'  src='images/new_tab_16.png' />");
                    }
                }
            });
        },
        
        select_leaf : function(leaf)
        {
            if(typeof leaf !== 'undefined' && leaf !== null)
            {
                var _this = this;
                leaf.fileBrowser('list_tree', _this);
                leaf.addClass('selected');
                if (leaf.data('can-add-fix-dir') === true)
                {
                    toolbarStatus(_this, 'can_add_fix_dir');
                }
                else
                {
                    toolbarStatus(_this, 'cannot_add_fix_dir');
                }
                if (leaf.data('can-add-file') === false)
                {
                    _this.data('files_toolbar').find('.add').addClass('disabled');
                }
                else
                {
                    if (_this.data('files_toolbar').find('.add').hasClass('disabled'))
                    {
                        _this.data('files_toolbar').find('.add').removeClass('disabled');
                    }
                }
                if (leaf.data('can-rename-fix-dir') === true)
                {
                    toolbarStatus(_this, 'can_rename_fix_dir');
                }
                else
                {
                    toolbarStatus(_this, 'cannot_rename_fix_dir');
                }

                if (leaf.data('fix-dir-type') !== '')
                {
                    toolbarStatus(_this, 'can_delete_fix_dir');

                    _this.data('description_folder').find('p.dir_type_wrapper').show();
                }
                else
                {
                    toolbarStatus(_this, 'cannot_delete_fix_dir');

                    _this.data('description_folder').find('p.dir_type_wrapper').hide();
                }

                _this.data('description_file').empty();
                toolbarStatus(_this, 'no_files');
            }
        }
    };
    
    function makeFolderDraggable(_this)
    {
        _this.data('folder_tree').find('li span').draggable({
                delay: 200,
                revert: 'invalid',
                appendTo: 'body',
                containment: 'window',
                scroll: false,
                helper: 'clone',				
                start: function(e, ui) {
                    $(ui.helper).addClass('sima-disk-ui-drag-cursor');
                },
                stop: function(e, ui) {
                    $(ui.helper).removeClass('sima-disk-ui-drag-cursor');
                }
        });
    }
    
    function makeFileDraggable(_this)
    {
        _this.data('files_display').find('tbody tr.sima-ui-fb-file').draggable({
                delay: 200,
                revert: 'invalid',
                appendTo: 'body',
                containment: 'window',
                scroll: false,
                helper: function(){
                    var selected = _this.data('files_display').find('tbody tr.sima-ui-fb-file.selected');
                    if (selected.length === 0) {
                      selected = $(this);
                    }
                    var container = $('<div/>').attr('class', 'sima-file-multi-dragg');
                    container.append(selected.clone());
                    return container; 
                },
                start: function(e, ui) {
                    $(ui.helper).addClass('sima-disk-ui-drag-cursor');
                },
                stop: function(e, ui) {
                    $(ui.helper).removeClass('sima-disk-ui-drag-cursor');
                }
        });
        
        _this.data('files_display').find('tbody tr.sima-ui-fb-file').bind("drag", function(event, ui) {
            ui.helper.find('tr.sima-ui-fb-file.selected').removeClass('selected');
        });
    }
    
    function makeFileFolderDroppable(_this)
    {
        _this.data('folder_tree').find('li span.sima-ui-tree-title').filter(function(){
            var can_add_file = $(this).parent().data('can-add-file');
            return can_add_file;
        }).droppable({
                hoverClass: "ui-drop-hover",
                greedy: true,
                tolerance: "pointer",
                drop: function(event, ui){
                    if (ui.helper.hasClass('sima-file-multi-dragg'))
                    {
                        var files = [];
                        var i = 0;
                        ui.helper.children().each(function(){
                            files[i] = $(this).data('model-id');
                            i++;
                        });
                        var currentSelected = _this.data('folder_tree').find('li.selected');
                        var current_query_id = currentSelected.data('leaf-id');
                        var target_query_id = $(this).parent().data('leaf-id');
                        var can_add_file = $(this).parent().data('can-add-file');
                        
                        var move_type;
                        var parent_satisfy = true;
                        if( currentSelected.parent().closest('li').data('leaf-id') === target_query_id )
                        {
                            move_type = 'PARENT';
                        }
                        else if ( $(this).parent().parent().closest('li').data('leaf-id') === current_query_id )
                        {
                            move_type = 'CHILD';
                        }
                        else
                        {
                            parent_satisfy = false;
                        }

                        if(parent_satisfy)
                        {
                            if (can_add_file)
                            {
                                sima.ajax.get('filebrowser/moveFile',{
                                      get_params: {
                                            async: true
                                      },
                                      data: {
                                          current_query_id:current_query_id, 
                                          target_query_id:target_query_id, 
                                          files:files, 
                                          move_type:move_type
                                      },
                                      success_function:function(data){
                                                   ui.draggable.remove();
                                                   _this.fileBrowser('select_leaf',currentSelected);
                                             }
                                });
                            }
                            else
                            {
                                sima.dialog.openInfo('Ne mozete prebaciti fajl u ovaj folder.');
                            }
                        }
                        else
                        {
                            sima.dialog.openInfo('Fajl se moze prebaciti samo u direktan podfolder ili nadfolder.');
                        }
                    }
                    else if(ui.helper.hasClass('sima-ui-tree-title')
                            && ui.helper.hasClass('ui-draggable')
                            && ui.helper.hasClass('ui-droppable'))
                    {
//                        var dir_query = ui.draggable.parent().data('leaf-id');
//                        var current_parent = ui.draggable.parent().parents('.sima-ui-tree-leaf-opened:first');
//                        var source_dir_query = current_parent.data('leaf-id');
//                        var target_parent = $(this).parent();
//                        var destination_dir_query = target_parent.data('leaf-id');
//                        var can_add_folder = $(this).parent().data('can-add-fix-dir');
                        
//                        if (can_add_folder)
//                        {
//                            sima.ajax.get('filebrowser/moveFolder',{
//                                async: true,
//                                data: {
//                                    dir_query:dir_query, 
//                                    destination_dir_query:destination_dir_query, 
//                                    source_dir_query:source_dir_query
//                                },
//                                success_function:function(data){
//                                              ui.draggable.parent().hide();
//                                              _this.find('li.selected').removeClass('selected');
//                                              target_parent.addClass('sima-ui-tree-leaf-opened');
//                                             _this.fileBrowser('select_leaf',target_parent);
//                                       }
//                            });
//                        }
//                        else
//                        {
                            sima.dialog.openInfo('Ne mozete prebaciti u ovaj folder.');
//                        }
                    }
                } 
         });
    }
    
    function open_file(file, browser)
    {
        if(!browser.data('files_file_open').hasClass('_not_connected'))
        {
            var file_id = file.attr('data-model-id');
            if(file_id === null)
            {
                sima.addError('fileBrowser - open_file', 'file_id is null');
                return; /// TODO: MilosJ - morao bi return, ne sme se nastaviti ako je doslo do greske
            }
            
            sima.dialog.openInfo('Nije implementirano!!!');
//            sima.file.openInClient(file_id, "File");
        }
        else
        {
            sima.dialog.openInfo('Odaberite sesiju');
        }
    }
    
    function makeResizable(_table, table)
    {
        //get number of columns
        var numberOfColumns = table.find('TR:first TH').size();

        //id is needed to create id's for the draghandles
        var tableId = table.attr('id');

        for (var i = 0; i <= numberOfColumns; i++)
        {
            //enjoy this nice chain :)
            $('<div class="srt-draghandle" id="' + tableId + '_id' + i + '"></div>').insertBefore(table).data('tableid', tableId).data('myindex', i).draggable(
                    {axis: "x",
                        start: function()
                        {
                            var tableId = ($(this).data('tableid'));
                            $(this).toggleClass("dragged");
                            //set the height of the draghandle to the current height of the table, to get the vertical ruler
                            $(this).css({height: $('#' + tableId).height() + 'px'});
                        },
                        stop: function(event, ui)
                        {
                            var tableId = ($(this).data('tableid'));
                            $(this).toggleClass("dragged");
                            var oldPos = ($(this).data("ui-draggable").originalPosition.left);
                            var newPos = ui.position.left;
                            var index = $(this).data("myindex");
                            resetTableSizes(_table, newPos - oldPos, index - 1);
                        }
                    }
            );
        }
        
        resetSliderPositions(_table);
        return table;
    }

    function resetTableSizes(_table, change, columnIndex)
    {
        var table_resize_wraper = _table.data('files_display').find('.table_resize_wrapper');
        table_resize_wraper.width(table_resize_wraper.width() + 5000);
        var new_width = _table.data('files_display').find('th').eq(columnIndex).innerWidth() + change - 5;
        _table.data('files_display').find('tr').each(function(){     
            if(!$(this).find('th, td').eq(columnIndex).hasClass('icon'))
            {
                if (new_width < _table.data('name_size_type_datamodifiedSize'))
                    new_width = _table.data('name_size_type_datamodifiedSize');
            } else if($(this).find('th, td').eq(columnIndex).hasClass('icon'))
            {
                if (new_width < _table.data('iconSize'))
                    new_width = _table.data('iconSize');
            }
            
            $(this).find('th').eq(columnIndex).css('min-width',new_width);
            $(this).find('th').eq(columnIndex).css('width',new_width);
            $(this).find('th').eq(columnIndex).css('max-width',new_width);
            
            $(this).find('td').eq(columnIndex).css('min-width',new_width);
            $(this).find('td').eq(columnIndex).css('width',new_width);
            $(this).find('td').eq(columnIndex).css('max-width',new_width);
        });
        
        table_resize_wraper.width(table_resize_wraper.width() + 5);

        adjust_cell_widths(_table);
        resetSliderPositions(_table);

    }

    function resetSliderPositions(_table)
    {
        var table = _table.data('files_display').find('table');
        var tableId = table.attr('id');
        //put all sliders on the correct position
        table.find(' TR:first TH').each(function(index)
        {
            var td = $(this);
            var newSliderPosition = td.position().left + td.innerWidth();
            $("#" + tableId + "_id" + (index + 1)).css({left: newSliderPosition, height: table.height() + 'px'});
        });
    }
    
    function adjust_cell_widths(_table)
    {
        _table.find('.table_resize_wrapper').css('width', (_table.data('files_display').find('table').width() + 5) + 'px');
        resetSliderPositions(_table);
    }
    
    /**
     * Funkcija koja kontrolise status toolbar-a i description-a
     * @param {type} browser
     * @param {type} status
     * @returns {undefined}
     */
    function toolbarStatus(browser, status)
    {
        switch(status)
        {
            /**--- FOLDERS ----- */
            case 'root_folder':
                browser.data('description_file').hide();
                browser.data('description_folder').hide();
                browser.data('files_display').find('tbody').empty();
                toolbarStatus(browser, 'no_files');
                break;
            case 'can_add_dir_to_root':
                browser.data('folder_button_add').removeClass('_disabled');
                break;
            case 'can_add_fix_dir':
                browser.data('folder_button_add').removeClass('_disabled');
                break;
            case 'cannot_add_fix_dir':
                browser.data('folder_button_add').addClass('_disabled');
                break;
            case 'can_rename_fix_dir':
                browser.data('folder_button_edit').removeClass('_disabled');
                break;
            case 'cannot_rename_fix_dir':
                browser.data('folder_button_edit').addClass('_disabled');
                break;
            case 'can_delete_fix_dir':
                browser.data('folder_button_delete').removeClass('_disabled');
                    break;
            case 'cannot_delete_fix_dir':
                browser.data('folder_button_delete').addClass('_disabled');
                    break;
            case 'can_add_file_to_root':
                browser.data('files_button_add').removeClass('disabled');
                toolbarStatus(browser, 'no_files');
                break;
            case 'cannot_add_file_to_root':
                browser.data('files_button_add').addClass('disabled');
                break;
            case 'folder':
                browser.data('files_button_add').addClass('disabled');
                toolbarStatus(browser, 'no_files');
                break;
            /**--- FILES ----- */
            case 'no_files':
                browser.data('files_button_edit').addClass('disabled');
                browser.data('files_button_new_tab').addClass('disabled');
                browser.data('files_button_dialog').addClass('disabled');
                browser.data('files_button_download').addClass('disabled');
                browser.data('files_button_download_zip').addClass('disabled');
                browser.data('files_file_open').addClass('_disabled');
                browser.data('description_folder').show();
                browser.data('description_file').hide();
                break;
            case 'one_file':
                browser.data('files_button_edit').removeClass('disabled');
                browser.data('files_button_new_tab').removeClass('disabled');
                browser.data('files_button_dialog').removeClass('disabled');
                browser.data('files_button_download').removeClass('disabled');
                browser.data('files_button_download_zip').removeClass('disabled');
                browser.data('description_folder').show();
                browser.data('description_file').show();
                if(!browser.data('files_file_open').hasClass('_not_connected'))
                {
                    browser.data('files_file_open').removeClass('_disabled');
                }
                break;
            case 'multi_files':
                browser.data('files_button_edit').addClass('disabled');
                browser.data('files_button_new_tab').addClass('disabled');
                browser.data('files_button_dialog').addClass('disabled');
                browser.data('files_button_download').addClass('disabled');
                browser.data('files_button_download_zip').removeClass('disabled');
                browser.data('files_file_open').addClass('_disabled');
                browser.data('description_folder').show();
                 browser.data('description_file').hide();
                break;
        }
    }
    
    /**
     * event hanfdler kada se selektuje fajl
     * @param {type} event
     * @param {type} localThis
     * @param {type} file
     * @returns {undefined}
     */
    function fileSelect(event, localThis, file)
    {
        event.stopPropagation();

        if(!event.ctrlKey && !event.shiftKey) {
            
            if (file.hasClass('selected'))
            {
                if (localThis.data('multiselect') === true)
                {
                    unselectAllFiles(localThis);
                    file.addClass('selected');
                    toolbarStatus(localThis,'one_file');
            
                    sima.ajax.get('filebrowser/fileDescription', {
                        get_params: {
                            id: file.data('model-id')
                        },
                        async: true,
                        success_function: function(response) {
                            localThis.data('description_file').html(response.html);
                        }
                    });
                }
                else
                {
                    file.toggleClass('selected');
                    toolbarStatus(localThis,'no_files');
                }
            }
            else
            {
                localThis.data('files_display').find("tbody tr.sima-ui-fb-file").removeClass('selected');
                file.addClass('selected');

                toolbarStatus(localThis,'one_file');
            
                sima.ajax.get('filebrowser/fileDescription', {
                    get_params: {
                        id: file.data('model-id')
                    },
                    async: true,
                    success_function: function(response) {
                        localThis.data('description_file').html(response.html);
                    }
                });
            }
            localThis.data('multiselect',false);
        }else if (event.ctrlKey){
            file.toggleClass('selected');
            localThis.data('ctrlLastSelected', file.index());
            toolbarStatus(localThis,'multi_files');
            localThis.data('multiselect',true);
        }else if (event.shiftKey){
            var selected = localThis.data('files_display').find("tbody tr.sima-ui-fb-file.selected");
            if (selected.size() >= 1)
            {   
                if (selected.size() >= 1)
                {
                    var row_index = file.index();
                    var first_index = localThis.data('shiftFirstSelected');
                    if (selected.size() === 1)
                    {
                        localThis.data('shiftFirstSelected', localThis.data('files_display').find("tbody tr.sima-ui-fb-file.selected:first").index());
                        first_index = localThis.data('shiftFirstSelected');
                    }
                    else if (localThis.data('ctrlLastSelected') !== null)
                    {
                        first_index = localThis.data('ctrlLastSelected');
                        localThis.data('shiftFirstSelected', first_index);
                    }

                    if (row_index >= first_index)
                    {
                        unselectAllFiles(localThis);
                        for (var i=first_index; i<=row_index; i++)
                            localThis.data('files_display').find("tbody tr.sima-ui-fb-file:eq("+i+")").addClass('selected');
                    }
                    else if (row_index <= first_index)
                    {
                        unselectAllFiles(localThis);
                        for (var i=row_index; i<=first_index; i++)
                            localThis.data('files_display').find("tbody tr.sima-ui-fb-file:eq("+i+")").addClass('selected');
                    }
                }
            }
            else
            {
                file.addClass('selected');
            }
            localThis.data('ctrlLastSelected', null);
            toolbarStatus(localThis,'multi_files');
            localThis.data('multiselect',true);
        }   
    }
    
    function unselectAllFiles(localThis)
    {
        localThis.data('files_display').find("tbody tr.sima-ui-fb-file.selected").each(function(){
            $(this).removeClass('selected');
        });
    }
    
    /**
     * event hanfdler
     * @param {type} event
     * @returns {undefined}
     */
    function filesButtonAddHandler(event)
    {
        if (!$(this).hasClass('disabled'))
        {
            var selected = event.data.browser.data('folder_tree').find('li.selected');
            var query_id;
            if (typeof selected.data('leaf-id') !== 'undefined')
                query_id = selected.data('leaf-id');
            else
                query_id = event.data.browser.find('.sima-ui-tree').data('leaf-id');
            
            var document_type_id = '';
            var static_fix_dir_id = '';
            var belong_tags = [];
            sima.ajax.get('filebrowser/getPrepareDataForAddFile', {
                    get_params: {
                        query_id: query_id,
                        async: true
                    },
                    success_function: function(response) {
                        document_type_id = response.document_type_id;
                        static_fix_dir_id = response.static_fix_dir_id;
                        var tags = response.tags;
                        $.each(tags, function(index, value) {
                            var curr_tag = {};
                            curr_tag.id = value.id;
                            curr_tag.display_name = value.display_name;
                            curr_tag.class = value.class;
                            belong_tags.push(curr_tag);
                        });
                    }
            });
            var document_type_chage_disabled = true;
            if(document_type_id === undefined || document_type_id === '')
            {
                document_type_chage_disabled = false;
            }
            sima.model.form("File", '', {
                init_data: {
                    File: {
                        file_belongs: {
                            'ids': {
                                'ids' : belong_tags
                            }
                        }, 
                        document_type_id:{
                            'disabled':document_type_chage_disabled,
                            'init':document_type_id
                        },
                        static_fix_dir_id: static_fix_dir_id
                    }
                },
                onSave: function(){
                    if (typeof selected.data('leaf-id') !== 'undefined')
                    {
                        event.data.browser.fileBrowser('select_leaf',selected);
                    }
                    else
                    {
                        event.data.browser.data('folder_tree').fileBrowser('list_tree', event.data.browser);
                    }
                }
            });
        }else console.log('pritisnuto dugme nije dozvoljeno');
    }
    
    /**
     * event hanfdler
     * @param {type} event
     * @returns {undefined}
     */
    function filesButtonEditHandler(event)
    {
        if (!$(this).hasClass('disabled'))
        {
            var selected = event.data.browser.data('folder_tree').find('li.selected');
            sima.model.form('File',event.data.files_display.find(".sima-ui-fb-file.selected").data('modelId'), {
                onSave: function()
                {
                    if (typeof selected.data('leaf-id') !== 'undefined')
                    {
                        event.data.browser.fileBrowser('select_leaf',selected);
                    }
                    else
                    {
                        event.data.browser.data('folder_tree').fileBrowser('list_tree', event.data.browser);
                    }
                }
            });
        }else console.log('pritisnuto dugme nije dozvoljeno');
    }
    
    /**
     * event hanfdler
     * @param {type} event
     * @returns {undefined}
     */
    function filesButtonNewTabHandler(event)
    {
        if (!$(this).hasClass('disabled'))
        {
            sima.mainTabs.openNewTab('files/file', event.data.files_display.find(".sima-ui-fb-file.selected").data('modelId'));
        }else console.log('pritisnuto dugme nije dozvoljeno');
    }
    
    function filesButtonDialogHandler(event)
    {
        if (!$(this).hasClass('disabled'))
        {
            sima.dialog.openModel('files/file', event.data.files_display.find(".sima-ui-fb-file.selected").data('modelId'));
        }else console.log('pritisnuto dugme nije dozvoljeno');
    }
    
    function getFolderNewTabAction(_table)
    {
        var currentSelected_id = _table.data('folder_tree').find('li.selected').data('leaf-id');
        var action = '';
        var id = '';
        sima.ajax.get('filebrowser/openFolderNewTab', {
                    get_params: {
                        async: true,
                        query_id: currentSelected_id
                    },
                    success_function: function(response) {
                        action = response.action;
                        id = response.id;
                    }
        });
        
        var params = [];
        if (action !== '' && id !== '')
        {
            params['id'] = id;
            params['action'] = action;
            return params;
        }
        else
            return '';
    }
    
    function folderButtonNewTabHandler(event)
    {
        var action = $(this).data('action');
        var id = $(this).data('id');
        sima.mainTabs.openNewTab(action, id);
    }
    
    /**
     * event hanfdler
     * @param {type} event
     * @returns {undefined}
     */
    function filesButtonDeleteHandler(event)
    {
        if (!$(this).hasClass('disabled'))
        {
            
        }else console.log('pritisnuto dugme nije dozvoljeno');
    }
    
    /**
     * event hanfdler
     * @param {type} event
     * @returns {undefined}
     */
    function filesButtonDownloadHandler(event)
    {
        var file_id = event.data.files_display.find(".sima-ui-fb-file.selected").data('modelId');
        sima.icons.fileDownload(file_id);
    }
    
    /**
     * event hanfdler
     * @param {type} event
     * @returns {undefined}
     */
    /// ceo filebrowser ne bi trebalo vise da se koristi, umesto ovog filebrowser2
//    function filesButtonDownloadZipHandler(event)
//    {
//        if (!$(this).hasClass('disabled'))
//        {
//            var foldername = event.data.browser.data('folder_tree').find('li.selected').find('.sima-ui-tree-title').html();
//            var files = [];
//            var i = 0;
//            event.data.files_display.find("tbody tr.sima-ui-fb-file.selected").each(function(){
//                files[i] = $(this).data('model-id');
//                i++;
//            });
//            window.location=sima.getBaseUrl()+'/index.php?r=files/transfer/downloadzip'+'&IDs='+files+'&foldername='+foldername;
//        }else console.log('pritisnuto dugme nije dozvoljeno');
//    }
    
    function filesButtonFileOpenHandler(event)
    {
        var file = event.data.files_display.find('.sima-ui-fb-file.selected');
        
        if (!$(this).hasClass('disabled'))
        {
            open_file(file, event.data.browser);
        }
        else 
            console.log('pritisnuto dugme nije dozvoljeno');
    }
    
    function folderButtonAddHandler(event)
    {
        if (!$(this).hasClass('disabled'))
        {
            var currentSelected = event.data.tree.find('li.selected');
            var query_id;
            if (typeof currentSelected.data('leaf-id') !== 'undefined')
                query_id = currentSelected.data('leaf-id');
            else
                query_id = event.data.browser.data('root_id');
            
            var browser_id = event.data.browser.attr('id');
            
            var uniq_id = sima.uniqid();
            sima.dialog.openYesNo('<div id="'+uniq_id+'">'
                                    +'<div class="row">'
                                        +'<label>Unesite ime foldera: </label>'
                                        +'<input type="text" name="folderName">'
                                    +'</div>'
                                    +'<div class="row">'
                                        +'<label>Folder kao tag: </label>'
                                    +'<input type="checkbox" name="folderType">'
                                    +'</div>'
                                +'</div>',function(){
                    
                    var name = $("div#"+uniq_id+" input[name='folderName']").val();
                    var type = $("div#"+uniq_id+" input[name='folderType']").prop('checked');

                    if (name !== '' && name !== null)
                    {
                        sima.ajax.get('filebrowser/addFolder', {
                            data: {query_id: query_id, name: name, type:type},
                            success_function: function(status) {
                                sima.dialog.closeYesNo();
                                var selected = $('#'+browser_id).find(".sima-ui-tree ul.line li.selected");
                                if (typeof selected.data('leaf-id') !== 'undefined')
                                {
                                    $('#'+browser_id).fileBrowser('select_leaf', selected);
                                }
                                else
                                {
                                    $('#'+browser_id).find('.sima-ui-tree').fileBrowser('list_tree', $('#'+browser_id));
                                }
                            }
                        });
                    }
                });
        }
    }
    
    function folderButtonEditHandler(event)
    {
        if (!$(this).hasClass('disabled'))
        {
            var currentSelected = event.data.tree.find('li.selected');
            var currentParentSelected = currentSelected.parents('.sima-ui-tree-leaf-opened:first');
            var query_id = currentSelected.data('leaf-id');
            var query_parent_id = currentParentSelected.data('leaf-id');
            var old_name = $.trim(currentSelected.find('.sima-ui-tree-title').text());
            if (currentSelected.length > 0)
            {
                sima.dialog.openPrompt('', function(input_value){
                    if (input_value !== '' && input_value !== null)
                    {
                        if (input_value !== old_name)
                        {
                            sima.ajax.get('filebrowser/renameFolder', {
                                async: true,
                                data: {query_parent_id: query_parent_id, query_id: query_id, name: input_value},
                                success_function: function(status) {
                                    event.data.browser.fileBrowser('select_leaf', currentParentSelected);
                                }
                            });
                        }
                    }
                    else if (input_value === '')
                    {
                        sima.dialog.openInfo('Ime foldera ne moze biti prazno.');
                    }
                }, 
                {
                    text:'Unesite novo ime foldera:',
                    type:'input',
                    default_option:currentSelected.children('.sima-ui-tree-title').html()
                });
            }
            else
            {
                sima.dialog.openInfo('Niste selektovali folder.');
            }
        }
    }
    
    function folderButtonDeleteHandler(event)
    {
        if (!$(this).hasClass('disabled'))
        {
            var currentSelected = event.data.tree.find('li.selected');
            var currentParentSelected = currentSelected.parents('.sima-ui-tree-leaf-opened:first');
            var dir_query = currentSelected.data('leaf-id');
            if (currentSelected.length > 0)
            {
                sima.ajax.get('filebrowser/deleteFolder', {
                    async: true,
                    data: {dir_query: dir_query},
                    success_function: function(status) {
                        event.data.browser.fileBrowser('select_leaf', currentParentSelected);
                    }
                });
            }
            else
            {
                sima.dialog.openInfo('Niste selektovali folder.');
            }
        }
    }
    
    //funkcija koja se poziva kada se klikne na dugme za pravljenje novog directory definition-a
    function folderButtonCreateDdHandler(event)
    {
        if (!$(this).hasClass('disabled'))
        {
            sima.ajax.get('ddmanager/fileManager', {
                get_params: {
                    selected:event.data.browser.find('#directory_definition').val()
                },
                data:{
                    user_id: event.data.browser.data('user_id'),
                    company_id: event.data.browser.data('company_id'),
                    location_code: event.data.browser.data('location_code')
                },
                success_function: function(response) {
                    event.data.tree.find('li span.sima-ui-tree-title').droppable({ disabled: true });
                    var params = {};
                    params.close_func = function(){
                        event.data.tree.find('li span.sima-ui-tree-title').droppable({ disabled: false });
                        sima.ajax.get('ddmanager/checkDefinitionsDropDown', {
                            get_params: {
                                selected:event.data.browser.find('#directory_definition').val()
                            },
                            data:{
                                user_id: event.data.browser.data('user_id'),
                                company_id: event.data.browser.data('company_id'),
                                location_code: event.data.browser.data('location_code')
                            },
                            success_function: function(response) {
                                event.data.browser.find('.sima-ui-tree-dd-list').empty().html(response.html);
                                if (event.data.browser.find('#directory_definition option').size() > 0)
                                {
                                    event.data.browser.find('.sima-ui-tree-dd-list').removeClass('disabled');
                                    if (event.data.tree.find('li.not_loaded').length === 0)
                                    {
                                        event.data.browser.data('folder_tree').fileBrowser('refresh_tree', event.data.browser);
                                    }
                                }
                            }
                       });
                    };
                    sima.dialog.openInFullScreen(response.html, 'Fajl Menadzer <img id="ui-dialog-file-manager-wiki"></img>', params);
                }
           });
        }
    }
    
    function directoryDefinitionHendler(event)
    {
        var definition = $(this);
        sima.ajax.get('ddmanager/changeSelectedDirectoryDefinition', {
            get_params: {
                selected:event.data.browser.find('#directory_definition').val()
            },
            data:{
                user_id: event.data.browser.data('user_id'),
                company_id: event.data.browser.data('company_id'),
                location_code: event.data.browser.data('location_code')
            },
            success_function: function(response) {
                var new_query = "$$"+definition.find('#directory_definition').val()+"$$";
                
                var init_tql = event.data.browser.data('folder_tree').data('init-tql');
                if(init_tql !== undefined && init_tql !== '')
                {
                    new_query += '!!'+init_tql;
                }
                
                event.data.browser.data('folder_tree').data('leaf-id', new_query);
                event.data.browser.data('filteredCond', event.data.browser.data('filteredCondArray').join('!!'));
                
                event.data.browser.data('folder_tree').fileBrowser('refresh_tree', event.data.browser);
            }
        });
    }
    
    function set_columns_width(_this, files_display)
    {  
        files_display.find('table thead tr th, table tbody tr td').each(function(){
            var each_this = $(this);
            if (each_this.hasClass('icon'))
            {
                each_this.css('min-width', _this.data('iconSize'));
                each_this.css('width', _this.data('iconSize'));
                each_this.css('max-width', _this.data('iconSize'));
            }
            else if (each_this.hasClass('size') 
                    || each_this.hasClass('type') 
                    || each_this.hasClass('data_modified') 
                    || each_this.hasClass('partner') 
                    || each_this.hasClass('note'))
            {
                each_this.css('min-width', _this.data('name_size_type_datamodifiedSize'));
                each_this.css('width', _this.data('name_size_type_datamodifiedSize'));
                each_this.css('max-width', _this.data('name_size_type_datamodifiedSize'));
            }
            else if (each_this.hasClass('name'))
            {
                var width = files_display.parent().width() - _this.data('iconSize') - 3*_this.data('name_size_type_datamodifiedSize') - 52;
                if (width < 150)
                {
                    each_this.css('min-width', _this.data('name_size_type_datamodifiedSize')+'px');
                }
                else
                {
                    each_this.css('min-width', width+'px');
                }
                each_this.css('width', width+'px');
                each_this.css('max-width', width+'px');
            }
        });
    }
    
    /**
     * event hanfdler
     * @param {type} event
     * @returns {undefined}
     */
    function filtersButtonHandler(event)
    {
        if (!$(this).hasClass('disabled'))
        {
            if( event.data.browser.data('filters_wrapper').is(":visible")  )
            {
                $('#global_overlay').css({height: "0px"});
                event.data.browser.data('filters_wrapper').hide();
                
                if( event.data.browser.data('filters_had_change') === true )
                {
                    /* build from selected filters */
                    event.data.browser.data('filteredCond', event.data.browser.data('filteredCondArray').join('!!'));

                    setFiltersDisplay(event);

                    /* refresh */
                    event.data.browser.data('folder_tree').fileBrowser('refresh_tree', event.data.browser);
                    
                    event.data.browser.data('filters_had_change', false);
                }
            }
            else
            {
                $('#global_overlay').css({height: "100%"});
                event.data.browser.data('filters_wrapper').show();
            }
        }
    }
    
    function setFiltersDisplay(event)
    {
        var res = '';
        for(var i=0; i<event.data.browser.data('filteredDisplayArray').length; i++)
        {
            if(res !== '')
            {
                res += ' | ';
            }

            res += event.data.browser.data('filteredDisplayArray')[i];
        }

        event.data.browser.data('used_filters').html(res);
    }
    
    /**
     * event hanfdler
     * @param {type} event
     * @returns {undefined}
     */
    function filtersElementClicked(event)
    {
        if (!$(this).hasClass('disabled'))
        {
            var first_ul = $(this).find('ul:first');
            
            $(this).parent().find('.sub-level').not(first_ul).hide();

            if(first_ul.length !== 0)
            {
                first_ul.css("left", first_ul.parent().outerWidth());
                first_ul.css("top", first_ul.parent().position().top);
                
                first_ul.toggle();
            }
        }
        
        event.stopPropagation();
    }
    
    /**
     * event hanfdler
     * @param {type} event
     * @returns {undefined}
     */
    function filtersCheckboxElementChanged(event)
    {
        event.data.browser.data('filters_had_change', true);
        
        var checked = $(this).is(':checked');
        
        var to_remove = [];
        var to_add = '';
        var to_add_display = '';
        var to_remove_display = [];
        var value = $(this).data('filter-value');
        var display = $(this).data('filter-display');

        if( value.indexOf('$', value.length - 1) !== -1 )
        {
            if(event.data.browser.data('filteredCondArray').length > 0)
            {
                var to_remove = jQuery.grep(event.data.browser.data('filteredCondArray'), function(n, i) {
                    return n.indexOf(value) === 0;
                });
            }
            
            if(event.data.browser.data('filteredDisplayArray').length > 0)
            {
                var to_remove_display = jQuery.grep(event.data.browser.data('filteredDisplayArray'), function(n, i) 
                {
                    return n.indexOf(display) !== -1;
                });
            }

            if(checked)
            {
                if( value.indexOf('FILE_NAME$') === 0 )
                {
                    var temp = $(this).siblings(".filter-filename").attr("value");
                    if (temp !== null && temp !== '')
                    {
                        var temp2 = temp.trim();
                        temp2 = temp2.replace(/ +/g, ' ');
                        temp2 = temp2.replace(/ /g, '!!FILE_NAME$');
                        to_add = value + temp2;
                        to_add_display = display + ": '" + temp + "'";
                    }
                }
                else
                {
                    var temp = $(this).siblings(".sima-ui-sf").find(".sima-ui-sf-input").attr("value");
                    var disp = $(this).siblings(".sima-ui-sf").find(".sima-ui-sf-display").attr("value");

                    if (temp !== null && temp !== '')
                    {
                        to_add = value + temp;
                        to_add_display = display + ": '" + disp + "'";
                    }
                }
            }
        }
        else
        {
            if(checked)
            {
                to_add = value;
                to_add_display = display;
            }
            else
            {
                to_remove.push(value);
                to_remove_display.push(display);
            }
        }
        
        if( to_remove.length > 0 )
        {
            var newArray = jQuery.grep(event.data.browser.data('filteredCondArray'), function(n, i) 
            {
                return $.inArray(n, to_remove) === -1;
            });

            event.data.browser.data('filteredCondArray', newArray);
            
            var newArray = jQuery.grep(event.data.browser.data('filteredDisplayArray'), function(n, i) 
            {
                return $.inArray(n, to_remove_display) === -1;
            });
            
            event.data.browser.data('filteredDisplayArray', newArray);
        }

        if( to_add !== '' )
        {
            event.data.browser.data('filteredCondArray').push( to_add );
            event.data.browser.data('filteredDisplayArray').push( to_add_display );
        }
    }
    
    /**
     * event hanfdler
     * @param {type} event
     * @returns {undefined}
     */
    function filtersTextElementChanged(event)
    {        
        var sibling = null;
        if($(this).hasClass("filter-filename"))
        {
            sibling = $(this).siblings(".filter-checkbox");
        }
        else if($(this).hasClass("sima-ui-sf-display"))
        {
            sibling = $(this).parents(".sima-ui-sf").siblings(".filter-checkbox");
        }
        
        if(sibling !== null)
        {
            sibling.attr("checked", false);
            sibling.trigger("change");
        }
    }
    
    function addDisplayFilter(event, filt_display)
    {
        event.data.browser.data('filteredDisplayArray').push( filt_display );
        setFiltersDisplay(event);
    }
    
    function removeDisplayFilter(event, filt_display)
    {
        var newArray = jQuery.grep(event.data.browser.data('filteredDisplayArray'), function(n, i) 
        {
            return n !== filt_display;
        });
        event.data.browser.data('filteredDisplayArray', newArray);
        setFiltersDisplay(event);
    }
    
    function filesFromParentsCbChanged(event)
    {
        var filt_display = $(this).attr('title');
        event.data.browser.data('filteredCond', event.data.browser.data('filteredCond_old'));
        
        if($(this).is(":checked"))
        {
            event.data.browser.data('files_from_parents', 'true');
            
            addDisplayFilter(event, filt_display);
        }
        else
        {
            event.data.browser.data('files_from_parents', 'false');
            
            removeDisplayFilter(event, filt_display);
        }
                        
        /* refresh */
        event.data.browser.data('folder_tree').fileBrowser('refresh_tree', event.data.browser);
    }
    
    function shortPathCbChanged(event)
    {
        var filt_display = $(this).attr('title');
        event.data.browser.data('filteredCond', event.data.browser.data('filteredCond_old'));
        
        if($(this).is(":checked"))
        {            
            sima.dialog.openYesNo("Da li ste sigurni da zelite da ukljucite skracivanje putanja?<br>Potencijalno moze raditi sporije", function(){
                sima.dialog.close();
                event.data.browser.data('short_path', 'true');
                
                addDisplayFilter(event, filt_display);

                /* refresh */
                event.data.browser.data('folder_tree').fileBrowser('refresh_tree', event.data.browser);
            }, function(){
                sima.dialog.close();
                var short_path_cp = event.data.browser.find(".short-path-cb");
                short_path_cp.attr("checked", false);
            });
        }
        else
        {
            event.data.browser.data('short_path', 'false');
                
            removeDisplayFilter(event, filt_display);
            
            /* refresh */
            event.data.browser.data('folder_tree').fileBrowser('refresh_tree', event.data.browser);
        }
    }
    
    function excludeEmptyCbChanged(event)
    {
        var filt_display = $(this).attr('title');
        event.data.browser.data('filteredCond', event.data.browser.data('filteredCond_old'));
        
        if($(this).is(":checked"))
        {            
            sima.dialog.openYesNo("Da li ste sigurni da zelite da ukljucite da se ne prikazuju prazni direktorijumi?<br>Potencijalno moze raditi sporije", function(){
                sima.dialog.close();
                event.data.browser.data('exclude_empty', 'true');
                                
                addDisplayFilter(event, filt_display);

                /* refresh */
                event.data.browser.data('folder_tree').fileBrowser('refresh_tree', event.data.browser);
            }, function(){
                sima.dialog.close();
                var exclude_empty_cp = event.data.browser.find(".exclude-empty-cb");
                exclude_empty_cp.attr("checked", false);
            });
        }
        else
        {
            event.data.browser.data('exclude_empty', 'false');
            
            removeDisplayFilter(event, filt_display);
            
            /* refresh */
            event.data.browser.data('folder_tree').fileBrowser('refresh_tree', event.data.browser);
        }
    }
    
    $.fn.fileBrowser = function(methodOrOptions) {
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method or options "' +  methodOrOptions + '" does not exist on jQuery.fileBrowser' );
        }    
    };  
    
})( jQuery );