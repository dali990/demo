<?php

/**
 * Kontroler koji kontrolise pregled fajlova u browser-u
 * @package SIMA
 * @subpackage Files
 */
class FileBrowserController extends SIMAController
{
    public function localRenderPartial($view, $data=NULL, $return=false, $processOutput=false)
    {
        $view = 'base.extensions.SIMAFileBrowser.views.fileBrowser.'.$view;
        
        if($return)
        {
            return parent::renderPartial($view, $data, $return, $processOutput);
        }
        else
        {
//            echo parent::renderPartial($view, $data, $return, $processOutput);
            $errmsg = __METHOD__.' - nije trebalo doci dovde';
            error_log($errmsg);
            Yii::app()->errorReport->createAutoClientBafRequest($errmsg);
        }
    }
    
    /**
     * funkcija koja ispisuje sve podatke o fajlu na osnovu id-a
     * @param type $id
     */
    public function actionFileDescription($id)
    {
        $file = File::model()->findByPk($id);
        if ($file == null)
        {
            throw new Exception('fajl ne postoji');
        }
        
        $html = $this->renderModelView($file, 'info');

        $this->respondOK([
            'html' => $html
        ]);
    }
    

    public function actionGetPrepareDataForAddFile($query_id)
    {
        $document_type_id = '';
        $tags_ids = array();
        
        $static_fix_dir_id = SIMASQLFunctions::getStaticFixDirIdFromQuery($query_id);
                
        $tags_to_be_added = SIMASQLFunctions::getTagsToBeAddedToFileFromQuery($query_id);
        if (isset($tags_to_be_added) && $tags_to_be_added !== '')
        {
            $tags_ids = explode(',', $tags_to_be_added);
        }
        
        foreach ($tags_ids as $tag_key => $tag_value)
        {
            $tag = FileTag::model()->findByPk($tag_value);
            if ($tag !== null && $tag->model_table == 'public.document_types')
            {
                $document_type_id = $tag->model_id;
                unset($tags_ids[$tag_key]);
                break;
            }
        }
        $belong_tags = array();
        if (count($tags_ids) >  0)
        {
            $criteria = new SIMADbCriteria([
                'Model' => 'FileTag',
                'model_filter' => [
                    'ids' => $tags_ids
                ]
            ]);
            $belong_tags = FileTag::model()->forUsers()->findAll($criteria);
        }
        $tags = array();
        $visible_tags_ids = array();
        //pakujemo sve tagove koji su belongs
        foreach ($belong_tags as $belong_tag)
        {
            array_push($visible_tags_ids, $belong_tag->id);
            $curr_id = array();
            $curr_id['id'] = $belong_tag->id;
            $curr_id['display_name'] = $belong_tag->DisplayName;
            $curr_id['class'] = '_non_editable';
            array_push($tags, $curr_id);
        }
        //trazimo razliku izmedju belongs tagova i oni koji dolaze iz baze(tags_ids) i sve oni koji dolaze iz baze a nema ih u belongs tagovima
        //postavljamo da su skriveni
        $diff = array_diff($tags_ids, $visible_tags_ids);
        foreach ($diff as $_id)
        {
            $curr_id = array();
            $curr_id['id'] = $_id;
            $curr_id['display_name'] = '';
            $curr_id['class'] = '_disabled';
            array_push($tags, $curr_id);
        }

        $this->respondOK([
            'document_type_id' => $document_type_id,
            'static_fix_dir_id' => $static_fix_dir_id,
            'tags' => $tags
        ]);
    }
    
    /**
     * 
     * @param type $files_from_parents
     * @param type $short_path
     * @param type $exclude_empty
     * @param type $filteredCond
     * @throws Exception
     */
    public function actionList($files_from_parents, $short_path, $exclude_empty)
    {
        $query = $this->filter_post_input('query');
        
        if(preg_match('/^\$\$\d+\$\$$/', $query))
        {
            $dd_id = preg_replace('/\D/', '', $query);
            $ddModel = DirectoryDefinition::model()->findByPk($dd_id);
            if(empty($ddModel->children))
            {
                throw new SIMAWarnException(Yii::t('SIMAFileBrowser.FileBrowser', 'EmptyDD'));
            }
        }
        
        $tree = '';
        $files = '';

        $rows = SIMASQLFunctions::getDirContent($query, $files_from_parents, $short_path, $exclude_empty);

        $files_count = 0;
        foreach ($rows as $row)
        {
            if ($row['content_type'] === 'TYPE_DIR')
            {
                $type = '';
                if (strpos($row['content_extension'], 'FIX_') !== false)
                {
                    $type = $row['content_extension'];
                }
                
                $tree .= '  <li 
                                class="' . $type . '"
                                data-leaf-id="' . $row['dir_query'] . '"
                                data-can-add-file="' . (($row['dir_canaddfile'] == 1) ? 'true' : 'false') . '"
                                data-can-add-fix-dir="' . (($row['dir_canaddfixdir'] == 1) ? 'true' : 'false') . '"
                                data-can-rename-fix-dir="' . (($row['dir_canrename'] == 1) ? 'true' : 'false') . '"
                                data-fix-dir-type="' . $type . '"
                            >
                                <span class="sima-ui-tree-icon">&nbsp;</span>
                                <span class="sima-ui-tree-title">' . $row['display_name'] . '</span>
                                <ul></ul>
                            </li>';
            }
            else if ($row['content_type'] === 'TYPE_FILE')
            {
                $files_count++;
                $files .= $this->localRenderPartial('displayFiles', array('file' => $row), true, true);
            }
            else
            {
                throw new Exception('nepostojeci tip sadrzaja');
            }
        }

        $tag_parent = str_replace('TAG_PARENT$', '', strstr($query, 'TAG_PARENT$'));
        $model_table = '';
        $model_id = '';
        $action = '';
        $id = '';

        if ($tag_parent != '' && is_numeric($tag_parent))
        {
            $tag = FileTag::model()->findByPk($tag_parent);
            $model_table = $tag->model_table;
            $model_id = $tag->model_id;
        }

        if ($model_table == 'private.jobs')
        {
            $action = 'jobs/job';
            $id = $model_id;
        }

        $this->respondOK([
            'tree' => $tree,
            'files' => $files,
            'files_count' => $files_count,
            'action' => $action,
            'id' => $id
        ]);
    }
    
    public function actionAddFolder()
    {
        $query_id = $this->filter_post_input('query_id');
        $name = $this->filter_post_input('name');
        $type = $this->filter_post_input('type');
        
        $new_dir_type = 'STATIC';

        if ($type === 'true')
        {
            $new_dir_type = 'DYNAMIC';
        }

        $create_dir_result = SIMASQLFunctions::createDir($query_id, $name, $new_dir_type);

        if ($create_dir_result === 'OK')
        {
            $this->respondOK();
        }
        else if ($create_dir_result === 'EEXISTS')
        {
            $message = 'Folder ili fajl sa ovim imenom vec postoji.';
        }
        else if ($create_dir_result === 'DYNSTAT')
        {
            $message = 'Ne moze se kreirati dinamicki direktorijum u statickoj strukturi.';
        }
        else if ($create_dir_result === 'CANTADD')
        {
            $message = 'Na ovoj lokaciji nije moguce kreirati fiksan direktorijum.';
        }
        else
        {
            $message = 'Doslo je do greske.';
        }

        $this->respondFail($message);
    }

    public function actionRenameFolder()
    {
        $query_parent_id = $this->filter_post_input('query_parent_id');
        $query_id = $this->filter_post_input('query_id');
        $name = $this->filter_post_input('name');

        $data = SIMASQLFunctions::renameDir($query_parent_id, $query_id, $name);

        if ($data[0]['rename_dir'] == 'OK')
        {
            $this->respondOK();
        }
        else if ($data[0]['rename_dir'] == 'EEXISTS')
        {
            $message = 'Folder ili fajl sa ovim imenom vec postoji.';
        }
        else if ($data[0]['rename_dir'] == 'CANTADD')
        {
            $message = 'Ne moze se dodati fiksan direktorijum na ovoj lokaciji.';
        }
        else
        {
            $message = 'Doslo je do greske.';
        }

        $this->respondFail($message);
    }

    public function actionDeleteFolder()
    {
        $dir_query = $this->filter_post_input('dir_query');

        $data = SIMASQLFunctions::removeDir($dir_query);

        if ($data[0]['remove_dir'] == 'OK')
        {
            $this->respondOK();
        }
        else
        {
            $this->respondFail($message);
        }
        
        $this->respondFail('UNKNOWN');
    }

    public function actionMoveFile()
    {
        $current_query_id = $this->filter_post_input('current_query_id');
        $target_query_id = $this->filter_post_input('target_query_id');
        $files = $this->filter_post_input('files');
        $move_type = $this->filter_post_input('move_type');

        $new_files = implode(",", $files);


        $data = SIMASQLFunctions::moveFile($current_query_id, $target_query_id, $new_files, $move_type);

        if ($data[0]['move_file'] == 'OK')
        {
            $this->respondOK();
        }
        else if ($data[0]['move_file'] == 'EEXISTS')
        {
            $message = 'Fajl ili folder sa istim imenom vec postoji u krajnjem direktorijumu';
        }
        else if ($data[0]['move_file'] == 'TAGRM')
        {
            $message = 'Nije dozvoljeno menjanje pripadnosti na ovaj nacin.';
        }
        else
        {
            $message = 'Doslo je do greske.';
        }

        $this->respondFail($message);
    }

    public function actionMoveFolder()
    {
        $dir_query = $this->filter_post_input('dir_query');
        $destination_dir_query = $this->filter_post_input('destination_dir_query');
        $source_dir_query = $this->filter_post_input('source_dir_query');

        $data = SIMASQLFunctions::moveDir($source_dir_query, $destination_dir_query, $dir_query);

        if ($data[0]['move_dir'] == 'OK')
        {
            $this->respondOK();
        }
        else if ($data[0]['move_dir'] == 'EEXISTS')
        {
            $message = 'U ciljnom folderu postoji fajl ili folder sa istim imenom.';
        }
        else if ($data[0]['move_dir'] == 'DYNSTAT')
        {
            $message = 'Ne moze se premestiti dinamicki direktorijum u staticki.';
        }
        else if ($data[0]['move_dir'] == 'TAGRM')
        {
            $message = 'Nije dozvoljeno menjanje pripadnosti na ovaj nacin.';
        }
        else if ($data[0]['move_dir'] == 'STATICMV')
        {
            $message = 'Nije dozvoljeno premestanje direktorijuma iz staticke strukture.';
        }
        else
        {
            $message = 'Doslo je do greske.';
        }

        $this->respondFail($message);
    }
}
