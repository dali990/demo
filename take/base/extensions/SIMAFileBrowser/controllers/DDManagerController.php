<?php

/**
 * Kontroler koji kontrolise kreiranje i editovanje definicija
 * @package SIMA
 * @subpackage Files
 */
class DDManagerController extends SIMAController
{
    public function localRenderPartial($view, $data=NULL, $return=false, $processOutput=false)
    {
        $view = 'base.extensions.SIMAFileBrowser.views.ddManager.'.$view;
        
        if($return)
        {
            return parent::renderPartial($view, $data, $return, $processOutput);
        }
        else
        {
//            echo parent::renderPartial($view, $data, $return, $processOutput);
            $errmsg = __METHOD__.' - nije trebalo doci dovde';
            error_log($errmsg);
            Yii::app()->errorReport->createAutoClientBafRequest($errmsg);
        }
    }
    
    /**
     * akcija koja otvara fajl menadzer
     * @param int $selected
     */
    public function actionFileManager($selected=null)
    {
        $user_id = $this->filter_post_input('user_id');
//        $company_id = $this->filter_post_input('company_id');
        $company_id = Yii::app()->company->id;
        $location_code = $this->filter_post_input('location_code');
        $user = User::model()->findByPk($user_id);
        
        if($user === null)
        {
            throw new Exception("user is null");
        }
        
        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
//        $directory_definition_types = Yii::app()->params['directoryDefinitionTypes'];
//        $directory_definition_filters = Yii::app()->params['directoryDefinitionFilters'];
        
        $ids_list = $ddFileManager->getDirectoryDefinitionsList($user->id, $company_id);
        $personal_tags = $user->limittag_personalTags;
        $security_tags = $user->tql_security_tags;
        
        $dd_creator_id = SIMAHtml::uniqid().'_dd_creator';
        $dd_tree_name = '';
//        $dd_tree = '';
        
        $uniq_id = SIMAHtml::uniqid().'_file_manager';
        
        $html = $this->localRenderPartial('fileManager', array(
            'uniq_id' => $uniq_id,
            'personal_tags'=> $personal_tags,
            'security_tags'=> $security_tags,
            'directory_definitions'=>$ids_list,
            'dd_creator_id' => $dd_creator_id,
            'dd_tree_name' => $dd_tree_name,
            'user_id' => $user_id,
            'company_id' => $company_id,
            'location_code' => $location_code,
            'selected' => $selected,
        ), true, true);
        
        $this->respondOK(array(
            'html'=>$html
        ));
    }
    
    /**
     * akcija koja se poziva za kreiranje definicija
     * @throws Exception
     */
    public function actionCreateDirectoryDefinition()
    {
        $id = $this->filter_post_input('id');
        $user_id = $this->filter_post_input('user_id');
        $company_id = $this->filter_post_input('company_id');
        $tree = $this->filter_post_input('tree', false);
     
        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
        $ddFileManager->removeDirectoryDefinition($id, $user_id, $company_id);
        
        $definition = DirectoryDefinition::model()->findByPk($id);
        $definition->children = "";
        $definition->update();
        
        $ddFileManager->createDirectoryDefinitions($id, $user_id, $tree);
        
        $this->respondOK();
    }
    
    
    /**
     * Sluzi da se updejtuje lista definicija korisnika ako je dodata nova definicija - poziva se prilikom zatvaranja dialoga za dodavanje definicija
     * @param int $selected
     * @throws Exception
     */
    public function actionCheckDefinitionsDropDown($selected=null)
    {
        $user_id = $this->filter_post_input('user_id');
        $company_id = $this->filter_post_input('company_id');
        $location_code = $this->filter_post_input('location_code');
        
        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
        $list = $ddFileManager->getDirectoryDefinitionsListNames($user_id, $company_id);
        if (isset($selected))
        {
            $selected_definition = $selected;
        }
        else if (count($list) > 0)
        {
            $user = User::model()->findByPk($user_id);
            
            $selected_definition = $ddFileManager->getSelectedDirectoryDefinition($user, $company_id, $location_code);
        }
        else
        {
            $selected_definition = '';
        }
        $html = CHtml::dropDownList('directory_definition', $selected_definition, $list);
        
        $this->respondOK([
            'html'=>$html,
        ]);
    }
    
    /**
     * ucitava definiciju na osnovu zadatog id-a
     * @param int $id
     * @throws Exception-ako nije zadat id izbacuje gresku
     */
//    public function actionLoadDirectoryDefinition($id)
//    {
//        if (!isset($id))
//        {
//            throw new Exception('Nije zadat id definicije!');
//        }
//        
//        $json = DirectoryDefinitionJSON::model()->findByAttributes(array('directory_definition_id'=>$id));
//
//        echo CJSON::encode(array(
//            'status'=>'success',
//            'message'=>'uspeh',
//            'response'=>array(
//                'json'=>  $json->json,
//            )    
//        ));
//        
//    }
 
    /**
     * akcija za dodavanje definicije
     * @param string $name
     */
    public function actionAddDirectoryDefinition($name)
    {        
        $user_id = $this->filter_post_input('user_id');
        $company_id = $this->filter_post_input('company_id');
        
        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
        $id = $ddFileManager->addDirectoryDefinition($name, $user_id, $company_id);
        
        $this->respondOK([
            'id'=>$id,
        ]);
    }
    
    /**
     * Menja default selected definiciju korisnika
     * @param int $selected - id selektovane definicije
     * @throws Exception
     */
    public function actionChangeSelectedDirectoryDefinition($selected)
    {
        $user_id = $this->filter_post_input('user_id');
        $location_code = $this->filter_post_input('location_code');
        $user = User::model()->findByPk($user_id);
        
        if (!isset($selected))
        {
            throw new Exception('Nije zadat id definicije');
        }
        
        $user_params=CJSON::decode($user->config_params);
        $config_name = 'selected_directory_definition_ids';
        $user_params[$config_name][$location_code]=$selected;
        $user->config_params=CJSON::encode($user_params);
        $user->save();
        
        $this->respondOK();
    }
    
    /**
     * akcija koja se poziva za brisanje definicije
     * @param string $ids
     * @throws Exception
     */
    public function actionDeleteDirectoryDefinitions($ids)
    {
        $user_id = $this->filter_post_input('user_id');
        $company_id = $this->filter_post_input('company_id');
        
        if (!isset($ids))
        {
            throw new Exception('Nisu zadate definicije');
        }
        
        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
        $ids_array = explode(';', $ids);

        foreach ($ids_array as $id)
        {
            $ddFileManager->removeDirectoryDefinition($id, $user_id, $company_id);
            $definition = DirectoryDefinition::model()->findByPk($id);
            $definition->delete();
        }
        
        $this->respondOK();
    }
    
    /**
     * Proverava da li trenutno selektovana definicija(u GUI-u) ista sa definicijom koja se vodi kao default selected za korisnika
     * To se radi zbog toga sto ne moze da se brise definicija ako se vodi kao default selected za tog korisnika, vec mora da se promeni default
     * selected u drugu definiciju pa da se onda ova izbrise 
     * @param string $ids
     */
    public function actionIsSelectedDefinition($ids)
    {
        $user_id = $this->filter_post_input('user_id');
        $company_id = $this->filter_post_input('company_id');
        $location_code = $this->filter_post_input('location_code');
        $user = User::model()->findByPk($user_id);
        
        if (!isset($ids))
        {
            throw new Exception('Nisu zadate definicije');
        }
        
        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
        $current_selected_definition_id = $ddFileManager->getSelectedDirectoryDefinition($user, $company_id, $location_code); 
        $state = '';
        $ids_array = explode(';', $ids);       
        foreach ($ids_array as $id)
        {
            if ($current_selected_definition_id == $id)
            {
                $name = $ddFileManager->getDirectoryDefinitionName($id);
                $state = $name;
            }
        }
          
        echo CJSON::encode(array(
            'status'=>'success',
            'message'=>'uspeh',
            'response'=>array(
                'state'=>$state,
            )    
        ));
    }
    
    /**
     * Eksportuje definiciju
     * @param string $ids - dobija kao parametar sve id-jeve definicija koje treba da se eksportuju
     * @throws Exception
     */
    /// MilosJ: nije nadjeno da se koristi
//    public function actionExportDefinitions($ids)
//    {
//        if (!isset($ids))
//        {
//            throw new Exception('Nisu zadate definicije');
//        }
//
//        $ids_array = explode(';', $ids);
//        $definitions = array();
//        foreach ($ids_array as $id)
//        {
//            $query = DirectoryDefinitionJSON::model()->findByAttributes(array('directory_definition_id'=>$id));
//            $definitions[$query->directory_definition_id] = $query->json;
//        }
//        
//        header('Content-disposition: attachment; filename=definitions.json');
//        header('Content-type: application/json');
//
//        echo CJSON::encode(array(
//            'definitions' => $definitions,
//        ));
//    }
    
    /**
     * importuje definicije
     */
//    public function actionImportDefinitions()
//    {
//        $user_id = $this->filter_post_input('user_id');
//        $company_id = $this->filter_post_input('company_id');
//        
//        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
//        $file_version = Yii::app()->sima_session['UploadedFileVersion'];
//        $full_file_path = Yii::app()->params['temp_file_repository'].$file_version['filename'];
//        $file_data = file_get_contents($full_file_path);
//        $data = json_decode($file_data, true);
//        
//        $definitions = $data['definitions'];
//        
//        $keys = array();
//        foreach ($definitions as $definition_key => $definition_value)
//        {
//            $definition_array = json_decode($definition_value, true);
//            $id = $ddFileManager->addDirectoryDefinition($definition_array['name'], $user_id, $company_id);
//            $keys[$definition_key] = $id;
//            $ddFileManager->createDirectoryDefinitions($id, $user_id, $definition_array);
//        }
//        
//        foreach ($definitions as $definition_key => $definition_value)
//        {
//            $definition_array = json_decode($definition_value, true);
//            $ddFileManager->checkForPointers($definition_array['children'], $keys);
//            $ddFileManager->createDirectoryDefinitions($keys[$definition_key], $user_id, $definition_array);
//        }
//        
//        $this->respondOK();
//    }
    
    /**
     * akcija koja menja ime definicije
     * @param int $id
     * @param string $name
     * @throws Exception
     */
    public function actionRenameDirectoryDefinition($id, $name)
    {
        if (!isset($id))
        {
            throw new Exception('Nije zadata definicija');
        }
        if (!isset($name))
        {
            throw new Exception('Nije zadato ime definicije');
        }
        
        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
        $ddFileManager->setDirectoryDefinitionName($id, $name);
        
        $this->respondOK();
    }
    
    /**
     * refresuje listu definicija tako sto kreira html sa novom listom definicija i zameni staru listu
     */
    public function actionRefreshDirectoryDefinitionsList()
    {
        $user_id = $this->filter_post_input('user_id');
        $company_id = $this->filter_post_input('company_id');
        
        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
        $ids_list = $ddFileManager->getDirectoryDefinitionsList($user_id, $company_id);
        
        $html = $this->localRenderPartial('directory_definitions_list', array(
            'directory_definitions'=>$ids_list,
        ), true, true);
        
        $this->respondOK([
            'html'=>$html,
        ]);
    }
    
    /**
     * dodaje default definicije
     */
    public function actionAddDirectoryDefinitions()
    {
        $user_id = $this->filter_post_input('user_id');
        $company_id = $this->filter_post_input('company_id');
        
        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
        $ddFileManager->addDirectoryDefinitions($user_id, $company_id);
        
        $this->respondOK();
    }
    
    /**
     * proverava da li je definicija pokazivac u nekoj drugoj definiciji i vraca html sa spiskom svih definicija u kojima je data definicija pokazivac
     * @param string $ids
     * @throws Exception
     */
    public function actionIsPointer($ids)
    {
        $user_id = $this->filter_post_input('user_id');
        $company_id = $this->filter_post_input('company_id');
        
        if (!isset($ids))
        {
            throw new Exception('Nisu zadate definicije');
        }
        
        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
        $ids_array = explode(';', $ids);
        
        $hasDefinitionWithPointer = false;
        $html = '';
        //prolazimo kroz sve definicije koje treba da se obrisu
        foreach ($ids_array as $id)
        {
            //za svaku od njih trazimo skup definicija u kojim se javljaju i smestamo ih u html
            $names = $ddFileManager->isDefinitionPointer($id, $user_id, $company_id);
            if (count($names) > 0)
            {
                $hasDefinitionWithPointer = true;
                $definition_name = $ddFileManager->getDirectoryDefinitionName($id);
                $html .= "<h3>Definicija <span style='color:red;'>$definition_name</span> se nalazi kao pokazivac u sledecim definicijama: </h3>";
                foreach ($names as $name)
                {
                    $html .= "- $name <br />";
                }
            }
        }
          
        if ($hasDefinitionWithPointer == true)
        {
            $html .= "<p style='color:red;'>Morate prvo da obrisete pokazivace definicija pa tek onda same definicije.</p>";
        }
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    /**
     * proverava sve pokazivace za svaku definiciju koja je ukljucena u export da li su i svi pokazivaci unutar nje ukljuceni...ukoliko postoji pokazivac
     * koji nije ukljucen onda se izbacuje lista definicija koje moraju biti ukljucene u export
     * @param string $ids
     * @throws Exception
     */
    public function actionDefinitionPointers($ids)
    {
        if (!isset($ids))
        {
            throw new Exception('Nisu zadate definicije');
        }
        
        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
        $definitions_to_export = explode(';', $ids);
        
        $hasPointers = false;
        $html = '';
        //prolazimo kroz sve definicije koje treba da se obrisu
        foreach ($definitions_to_export as $id)
        {
            //za svaku od njih trazimo skup definicija u kojim se javljaju i smestamo ih u html
            $pointers_ids = $ddFileManager->getNotIncludedPointers($id, $definitions_to_export);
            if (count($pointers_ids) > 0)
            {
                $hasPointers = true;
                $definition_name = $ddFileManager->getDirectoryDefinitionName($id);
                $html .= "<h3>U definiciji <span style='color:red;'>$definition_name</span> se nalaze sledeci pokazivaci definicija koji moraju biti ukljuceni u izvoz: </h3>";
                foreach ($pointers_ids as $pointers_id)
                {
                    $name = $ddFileManager->getDirectoryDefinitionName($pointers_id);
                    $html .= "- $name <br />";
                }
            }
        }
          
        if ($hasPointers == true)
        {
            $html .= "<p style='color:red;'>Ne mozete da izvezete definiciju koja ima u sebi pokazivac na neku drugu definiciju a da tu definiciju niste ukljucili u izvoz. Proverite u gore navedenim definicijama i pokazivacima koje sve definicije treba da ukljucite.</p>";
        }
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    /**
     * proverava da li se pokazivac na definiciju vrti u krug
     * @param int $parent
     * @param int $child
     */
    public function actionIsDefinitionInCircle($parent, $child)
    {
        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
        $definition = DirectoryDefinitionJSON::model()->findByAttributes(array('directory_definition_id'=>$parent));

        $state = $ddFileManager->checkDefinitionChildrenForCircle(json_decode($definition->json, true), $child);
        
        $this->respondOK([
            'state' => $state
        ]);
    }
    
    public function actionLoadDDTree($dd_id)
    {
        $ddJson = DirectoryDefinitionJSON::model()->findByAttributes(array('directory_definition_id'=>$dd_id));
        
        $tree_json = $ddJson->json;
        
        $tree_decoded = CJSON::decode($ddJson->json);
        if(isset($tree_decoded['name']))
        {
            $tree_json = CJSON::encode($tree_decoded['children']);
        }
        
        $this->respondOK(array(
            'tree_json'=>$tree_json,
            'tree_name'=>$ddJson->name
        ));
    }
}

