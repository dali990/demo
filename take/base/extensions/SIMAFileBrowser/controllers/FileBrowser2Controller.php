<?php

/**
 * Kontroler koji kontrolise pregled fajlova u browser-u
 * @package SIMA
 * @subpackage Files
 */
class FileBrowser2Controller extends SIMAController
{
    /// MilosJ: nije nadjeno da se koristi
//    public function localRenderPartial($view, $data=NULL, $return=false, $processOutput=false)
//    {
//        $view = 'base.extensions.SIMAFileBrowser.views.fileBrowser.'.$view;
//        
//        if($return)
//        {
//            return parent::renderPartial($view, $data, $return, $processOutput);
//        }
//        else
//        {
//            echo parent::renderPartial($view, $data, $return, $processOutput);
//        }
//    }
    
    public function actionList()
    {
        $params = $this->filter_post_input('params');
        $leaf_data = $this->filter_post_input('item_data', false);
                        
        $query_to_use = $leaf_data['query'];
        if(empty($query_to_use))
        {
            $query_to_use = $params['root_query'];
        }
        
        $parsed_used_filters_to_query = '';
        if(!empty($params['used_filters']))
        {
            $used_filters = $params['used_filters'];
            $parsed_used_filters_to_query = $this->parseUsedFilters($used_filters);
        }
        
        if(!empty($parsed_used_filters_to_query))
        {
            $query_to_use .= '!!'.$parsed_used_filters_to_query;
        }
        
        $rows = SIMASQLFunctions::getDirContent(
                $query_to_use, 
                $params['files_from_parents'], 
                $params['short_path'], 
                $params['exclude_empty']
        );
        
        $parentLeafId = '';
        if(isset($leaf_data['leafId']))
        {
            $parentLeafId = $leaf_data['leafId'];
        }
        
        $subtree = [];
        //$file_ids = []; //Sasa A. - nisam nasao da se koristi, pa sam zakomentarisao
        
        foreach ($rows as $row)
        {
            if ($row['content_type'] === 'TYPE_DIR')
            {
                $type = '';
                if (strpos($row['content_extension'], 'FIX_') !== false)
                {
                    $type = $row['content_extension'];
                }
                
                $title = $row['display_name'];
                if(!empty($parentLeafId))
                {
                    $parentLeafId_forsubstring = $parentLeafId.'_';
                    if(SIMAMisc::stringStartsWith($title, $parentLeafId_forsubstring))
                    {
                        $parsed_title = trim(substr($title, strlen($parentLeafId_forsubstring)));
                        
                        if(!empty($parsed_title))
                        {
                            $title = '*'.$parsed_title;
                        }
                    }
                }
                
                $subtree[] = [
                    //'id' => $row['dir_query'],
                    'id' => $row['display_name'],
                    'title' => [
                        'text' => $title,
                        'parts' => [
                            [
                                'class'=>'sima-fb-tree-item-icon',
                                'position'=>'before'
                            ]
                        ]
                    ],
                    'data' => [
                        'can_add_file' => (($row['dir_canaddfile'] == 1) ? 'true' : 'false'),
                        'can_add_fix_dir' => (($row['dir_canaddfixdir'] == 1) ? 'true' : 'false'),
                        'can_rename_fix_dir' => (($row['dir_canrename'] == 1) ? 'true' : 'false'),
                        'type' => $type,
//                        'query' => $row['dir_query']
                        'query' => $row['dir_query_hash']
                    ]
                ];
            }
//            else if ($row['content_type'] === 'TYPE_FILE')
//            {
//                $file_ids[] = $row['file_id'];
//            }
            else
            {
                throw new Exception('nepostojeci tip sadrzaja');
            }
        }
                
        $this->respondOK([
            'subtree' => $subtree,
//            'json' => $file_ids
        ]);
    }
    
    public function actionInitDDDropdownList()
    {
        $ddFileManager = Yii::app()->getModule('base')->ddmanager;
        $fileBrowser_id = $this->filter_post_input('fileBrowser_id');
        $user_id = $this->filter_post_input('user_id');
        $location_code = $this->filter_post_input('location_code');
        
        $company_id = Yii::app()->company->id;
        
        $user = ModelController::GetModel('User', $user_id);
        
        $definitions_list = $ddFileManager->getDirectoryDefinitionsListNames($user_id, $company_id);
        $selected_directory_definition_id = $ddFileManager->getSelectedDirectoryDefinition($user, $company_id, $location_code);
        
        $dropdownList = SIMAHtml::dropDownList(
                null, 
                $selected_directory_definition_id, 
                $definitions_list, 
                [
                    'onchange' => '$(\'#'.$fileBrowser_id.'\').fileBrowser2(\'directoryDefinitionsChange\');'
                ]
        );
        
        $this->respondOK([
            'dropdown_list_html' => $dropdownList
        ]);
    }
    
    public function actionGetFilesTableDataForDirQuery()
    {
        $dir_query_hash = $this->filter_post_input('dir_query_hash');
        $used_filters = $this->filter_post_input('used_filters', false);
        $parsed_used_filters = $this->parseUsedFilters($used_filters);
        $dir_query = SIMASQLFunctions::GetQueryFromOptimizatorHash($dir_query_hash);
        if(!empty($parsed_used_filters))
        {
            $dir_query .= '!!'.$parsed_used_filters;
        }
        
        $query = SIMASQLFunctions::GetFilesQueryForDirQuery($dir_query);
        $can_add_file = SIMASQLFunctions::canAddFileForQuery($dir_query);
        
        $this->respondOK([
            'query' => $query,
            'can_add_file' => $can_add_file
        ]);
    }
    
    private function parseUsedFilters($used_filters=null)
    {
        $parsed_used_filters_to_query = '';
        
//        $params = $this->filter_post_input('params');
//        if(empty($params['used_filters']))
        if(empty($used_filters))
        {
            return $parsed_used_filters_to_query;
        }
//        
//        $used_filters = $params['used_filters'];
        
        if(!is_array($used_filters))
        {
            throw new Exception(Yii::t('SIMAFileBrowser.FileBrowser', 'UsedFiltersNotArray'));
        }

        foreach($used_filters as $used_filter)
        {
            $used_filter_type = $used_filter['type'];
            switch($used_filter_type)
            {
                case 'FILE_NAME':
                    $used_filter_value = $used_filter['value'];
                    $parsed_used_filters_to_query .= $this->parseUsedFilters_filename($parsed_used_filters_to_query, $used_filter_value);
                    break;
                default:
                    throw new Exception(Yii::t('SIMAFileBrowser.FileBrowser', 'InvalidUserFilterType', [
                        '{used_filter_type}' => $used_filter_type
                    ]));
            }
        }
        
        return $parsed_used_filters_to_query;
    }
    
    private function parseUsedFilters_filename($parsed_used_filters_to_query, $used_filter_value)
    {
        $result = '';
        
        if(strpos($used_filter_value, '\\') !== false)
        {
            $used_filter_value = str_replace('\\', '', $used_filter_value);
            $this->raiseNote(Yii::t('SIMAFileBrowser.FileBrowser', 'UsedFilterBackSlasNotUsed')); 
        }
        if(!empty($parsed_used_filters_to_query))
        {
            $result = '!!';
        }
        $result .= 'FILE_NAME$'.$used_filter_value;
        
        return $result;
    }
    
    public function actionGetPrepareDataForAddFile()
    {
        $input_query = $this->filter_post_input('query');
        
        $document_type_id = '';
        $tags_ids = array();
        
        $query = SIMASQLFunctions::GetQueryFromOptimizatorHash($input_query);
        
        $static_fix_dir_id = SIMASQLFunctions::getStaticFixDirIdFromQuery($query);
                
        $tags_to_be_added = SIMASQLFunctions::getTagsToBeAddedToFileFromQuery($query);
        if (isset($tags_to_be_added) && $tags_to_be_added !== '')
        {
            $tags_ids = explode(',', $tags_to_be_added);
        }
        
        foreach ($tags_ids as $tag_key => $tag_value)
        {
            $tag = FileTag::model()->findByPk($tag_value);
            if ($tag !== null && $tag->model_table == 'public.document_types')
            {
                $document_type_id = $tag->model_id;
                unset($tags_ids[$tag_key]);
                break;
            }
        }
        $belong_tags = array();
        if (count($tags_ids) >  0)
        {
            $belong_tags = FileTag::model()->forUsers()->findAll([
                'model_filter' => [
                    'ids' => $tags_ids
                ]
            ]);
        }
        $tags = array();
        $visible_tags_ids = array();
        //pakujemo sve tagove koji su belongs
        foreach ($belong_tags as $belong_tag)
        {
            array_push($visible_tags_ids, $belong_tag->id);
            $curr_id = array();
            $curr_id['id'] = $belong_tag->id;
            $curr_id['display_name'] = $belong_tag->DisplayName;
            $curr_id['class'] = '_non_editable';
            array_push($tags, $curr_id);
        }
        //trazimo razliku izmedju belongs tagova i oni koji dolaze iz baze(tags_ids) i sve oni koji dolaze iz baze a nema ih u belongs tagovima
        //postavljamo da su skriveni
        $diff = array_diff($tags_ids, $visible_tags_ids);
        foreach ($diff as $_id)
        {
            $curr_id = array();
            $curr_id['id'] = $_id;
            $curr_id['display_name'] = '';
            $curr_id['class'] = '_disabled';
            array_push($tags, $curr_id);
        }

        $this->respondOK([
            'document_type_id' => $document_type_id,
            'static_fix_dir_id' => $static_fix_dir_id,
            'tags' => $tags
        ]);
    }
}
