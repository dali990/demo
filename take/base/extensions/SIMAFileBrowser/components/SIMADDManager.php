<?php

/**
 *  Komponenta u kojoj se definisu sve funkciuje vezane za File Manager 
 */

class SIMADDManager
{
    
    public function init()
    {
        
    }
    
    /**
     * Kreira definicije direktorijuma za neku definciiju. 
     * Kao parametre dobija id definicije za koju se kreiraju, 
     * model korisnika za koga se kreira i niz definicije direktorijuma
     * @param int $id
     * @param User $user_id
     * @param array $tree
     */
    public function createDirectoryDefinitions($id, $user_id, $tree=null)
    {        
        $json = '';
        $root_ids = "";
        if (isset($tree['children']))
        {
            /// children je niz elemenata, 
            /// moze se desiti da prosledjeni niz nema 'pravilne' kljuceve, redom od 0
            /// desavalo se da se dobije da je prvi element niza pod kljucem 0 
            $fixed_children_tree = [];
            foreach($tree['children'] as $child)
            {
                $fixed_children_tree[] = $child;
            }
            $tree['children'] = $fixed_children_tree;
            
            $json = json_encode($tree);
            
            $root_ids = $this->createItemChildren($tree, $user_id);
        }
        else
        {
            $json = '{"children":[], "attributes":{"show_unlisted":"'.$tree['attributes']['show_unlisted'].'"}}';
        }
                
        $definition = DirectoryDefinition::model()->findByPk($id);
        $definition->children = $root_ids;
        $definition->show_unlisted = ($tree['attributes']['show_unlisted'] == 'false')?0:1;
        $definition->null_name = $tree['name'];
        $definition->update();
                
        $definition_json = DirectoryDefinitionJSON::model()->findByAttributes(array('directory_definition_id'=>$id));
        $definition_json->json = $json;
        $definition_json->name = $tree['name'];
        $definition_json->update();
    }
    
    /**
     * pravi decu za neku definiciju i vraca spisak id-eva te dece
     * poziva se u createDirectoryDefinitions i callRecursiveDirectoryDefinition
     * @param array $item
     * @param int $user_id
     * @return array
     */
    private function createItemChildren(&$item, $user_id)
    {        
        $root_ids = "";
      
        //prolazi kroz sve elemente u prvom nivou i pravi njihovu decu, 
        //a zatim taj element i za svaki od njih dodaje id-jeve dece 
        foreach($item['children'] as $level)
        {
            $level_children = array();
            foreach ($level as $item_value)
            {                
                $children_ids = "";
                if (isset($item_value['children']))
                {
                    $children_ids = $this->createItemChildren($item_value, $user_id);
                }
                $child_id = $this->createDirectoryDefinition($item_value, $user_id, $children_ids);
                if ($child_id !== false)
                {
                    $level_children[] = $child_id;
                }
            }
            
            if(count($level_children) > 0)
            {                
                $lc_final = '';
                foreach($level_children as $lc)
                {
                    if(!empty($lc_final))
                    {
                        $lc_final .= ',';
                    }
                    $lc_final .= $lc;
                }

                if(!empty($root_ids))
                {
                    $root_ids .= "|";
                }

                $root_ids .= $lc_final;
            }
        }
        
        return $root_ids;
    }
    
    /**
     * Kreira definiciju direktorijuma za datog korisnika
     * poziva se u  createDirectoryDefinitions, createItemChildren
     * @param array $properties - niz atributa definicije
     * @param int $user_id
     * @param array $children_ids - id-evi dece za tu definiciju (ne mora da budu prosledjeni)
     * @return int ili false ako ne moze da kreira definiciju, u slucaju da ne postoji tag
     */
    private function createDirectoryDefinition($properties, $user_id, $children_ids=null)
    {
        if (isset($properties['definition_id']))
        {            
            return $properties['definition_id'];
        }
        else
        {            
            $no_tag = false;    // promenljiva koja se setuje na true ako nepostoji tag za taj filter, 
                                // da ne bi kreirali definiciju u bazi
            $values = array();
            if (isset($properties['model']) && $properties['model'] != '')
            {
                $tag_group_table = $properties['model']::model()->tableName();
                $values['type_table'] = $tag_group_table;
            }
            
            if (isset($properties['type_column']) && $properties['type_column'] != '')
            {
                $values['type_column'] = $properties['type_column'];
            }
            
            if (isset($properties['condition']) && $properties['condition'] != '')
            {
                $values['conditions'] = $properties['condition'];
            }
            
            if (isset($properties['type']) && $properties['type'] != '')
            {
                $values['type'] = $properties['type'];
            }
            
            if (isset($properties['attributes']) && !empty($properties['attributes']))
            {
                if (isset($properties['attributes']['show_unlisted']))
                {
                    $values['show_unlisted'] = $properties['attributes']['show_unlisted']=='false'?0:1;
                }
                if (isset($properties['attributes']['hideempty']))
                {
                    $values['hideempty'] = $properties['attributes']['hideempty']=='false'?0:1;
                }
                if (isset($properties['attributes']['tag_group_tree_view']))
                {
                    $values['tag_group_tree_view'] = $properties['attributes']['tag_group_tree_view']=='false'?0:1;
                }
                if (isset($properties['attributes']['min_file_listed']) && $properties['attributes']['min_file_listed'] != '')
                {
                    $values['min_file_listed'] = $properties['attributes']['min_file_listed'];
                }
                if (isset($properties['attributes']['null_name']) && $properties['attributes']['null_name'] != '')
                {
                    $values['null_name'] = $properties['attributes']['null_name'];
                }
                if (isset($properties['filter']))
                {
                    if (isset($properties['attributes']['skip']))
                    {
                        $values['is_skip'] = $properties['attributes']['skip'];
                    }
                    if (isset($properties['filter_type']))
                    {
                        if ($properties['filter_type'] == 'tql_limits_filter')
                        {
                            if (isset($properties['personality']) 
                                    && filter_var($properties['personality'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === true)
                            {
                                $limit_tag = TQLLimitTag::model()->findByAttributes(array('user_id'=>$user_id, 'name'=>$properties['filter']));
                            }
                            else
                            {
                                $limit_tag = TQLLimitTag::model()->findByAttributes(array('name'=>$properties['filter']));
                            }
                            if (isset($limit_tag) && $limit_tag !== null)
                            {
                                $tag = $limit_tag->tag;
                                if ($tag != null)
                                {
                                    $query = $tag->query.'$R';
                                    if (isset($values['conditions']))
                                    {
                                         $values['conditions'][] = $query;
                                    }
                                    else
                                    {
                                        $values['conditions'] = array($query);
                                    }
                                }
                            }
                            else
                            {
                                $no_tag = true;
                            }
                        }
                        else if ($properties['filter_type'] == 'table_name_filter')
                        {
                            $filter_table_name = $properties['filter']::model()->tableName();
                            $tag = FileTag::model()->findByAttributes(array('name'=>$filter_table_name));
                            if ($tag !== null)
                            {
                                $query = $tag->query.'$R';
                                if (isset($values['conditions']))
                                {
                                    $values['conditions'][] = $query;
                                }
                                else
                                {
                                    $values['conditions'] = array($query);
                                }
                            }
                            else
                            {
                                $no_tag = true;
                            }
                        }
                        else if ($properties['filter_type'] == 'tql_security_filter')
                        {
                            $security_tag = TQLSecurityTag::model()->findByPk($properties['id']);
                            if (isset($security_tag) && $security_tag !== null)
                            {
                                $tag = $security_tag->tag;
                                if ($tag != null)
                                {
                                    $query = $tag->query.'$R';
                                    if (isset($values['conditions']))
                                    {
                                        $values['conditions'][] = $query;
                                    }
                                    else
                                    {
                                        $values['conditions'] = array($query);
                                    }
                                }
                            }
                            else
                            {
                                $no_tag = true;
                            }
                        }
                        else if ($properties['filter_type'] == 'tql_personal_filter')
                        {
                            $personal_tag = TQLPersonalTag::model()->findByPk($properties['id']);
                            if (isset($personal_tag) && $personal_tag != null)
                            {
                                $tag = $personal_tag->tag;
                                if ($tag != null)
                                {
                                    $query = $tag->query.'$R';
                                    if (isset($values['conditions']))
                                    {
                                        $values['conditions'][] = $query;
                                    }
                                    else
                                    {
                                        $values['conditions'] = array($query);
                                    }
                                }
                            }
                            else
                            {
                                $no_tag = true;
                            }
                        }
                        else if ($properties['filter_type'] == 'individual')
                        {
                            $query = '';
                            if(isset($properties['type']) && $properties['type'] === 'tag')
                            {
                                $model = $properties['individual_model']::model()->findByAttributes([
                                    $properties['individual_column'] => $properties['value']
                                ]);
                                $tag = $model->tag;
                                $query = 'TAG$'.$tag->id.'$R';
                                $values['type'] = null;
                            }
                            else if (isset($properties['individual_model']) && $properties['individual_model'] !== '')
                            {
                                $tag_group_table = $properties['individual_model']::model()->tableName();
                                $query = 'TAG_GROUP$'.$tag_group_table.'$'.$properties['individual_column'].'$'.$properties['value'];
                            }
                            if (isset($values['conditions']))
                            {
                                $values['conditions'][] = $query;
                            }
                            else
                            {
                                $values['conditions'] = array($query);
                            }
                        }
                    }
                }
            }
                        
            if (!isset($values['null_name']) && isset($properties['alias']))
            {
                $values['null_name'] = $properties['alias'];
            }
            
            if (isset($children_ids) && !empty($children_ids))
            {
                $values['children'] = $children_ids;
            }
            
            if (!$no_tag)
            {                
                $definition = new DirectoryDefinition();
                
                foreach($values as $key => $value)
                {
                    $definition->$key = $value;
                }
                
                $definition->save();
                $definition->refresh();
                                
                return $definition->id;
            }
            else
            {
                return false;
            }
        }
    }
    
    /**
     * vraca listu definicija za datog korisnika
     * @param User $user_id
     * @param int $company_id
     * @return array
     */
    public function getDirectoryDefinitionsList($user_id, $company_id)
    {
        $ids_list = array();
        $directory_definition_ids = DirectoryDefinitionJSON::model()->findAllByAttributes(array(
            'user_id'=>$user_id, 
            'company_id'=>$company_id
        ), array('order'=>'id asc'));
        
        if (count($directory_definition_ids) > 0)
        {
            foreach ($directory_definition_ids as $directory_definition)
            {
                $temp = array();
                $json_array = json_decode($directory_definition->json, true);
                $id = $directory_definition->directory_definition_id;
                $temp['id'] = $id;
                $temp['name'] = $directory_definition->name;
                $temp['attributes'] = $json_array['attributes'];
                array_push($ids_list, $temp);
            }
        }
        
        return $ids_list;
    }
    
    /**
     * vraca niz gde su kljucevi id-evi a vrednosti nazivi (pogodno za dropdown)
     * @param type $user_id
     * @param type $company_id
     * @return array
     */
    public function getDirectoryDefinitionsListNames($user_id, $company_id)
    {
        $definitions_list = array();
        
        $directory_definitions = $this->getDirectoryDefinitionsList($user_id, $company_id);
        foreach ($directory_definitions as $definition)
        {
            $definitions_list[$definition['id']] = $definition['name'];
        }
        
        return $definitions_list;
    }
    
    /**
     * vraca niz gde su kljucevi nazivi a vrednosti id-evi (pogodno za kreiranje definicije sa pokazivacima)
     * @param type $user_id
     * @param type $company_id
     * @return array
     */
    public function getDirectoryDefinitionsListIds($user_id, $company_id)
    {
        $definitions_list = array();
        
        $directory_definitions = $this->getDirectoryDefinitionsList($user_id, $company_id);
        foreach ($directory_definitions as $definition)
        {
            $definitions_list[$definition['name']] = $definition['id'];
        }
        
        return $definitions_list;
    }
    
    public function getDirectoryDefinitionName($id)
    {
        $criteria = new SIMADbCriteria([
            'Model' => DirectoryDefinitionJSON::class,
            'model_filter' => [
                'directory_definition' => [
                    'ids' => $id
                ]
            ]
        ]);
        $directory_definition_json = DirectoryDefinitionJSON::model()->find($criteria);
        if(empty($directory_definition_json))
        {
            $directory_definition = DirectoryDefinition::model()->findByPkWithCheck($id);
            if(!empty($directory_definition->null_name))
            {
                return $directory_definition->null_name;
            }
            
            throw new SIMAExceptionModelNotFound(DirectoryDefinitionJSON::class, $id, 'directory_definition_id');
        }
        
        $jsonObj = json_decode($directory_definition_json->json);
        $name = $jsonObj->name;

        return $name;
    }
    
    /**
     * kreira definiciju za datog korisnika, i vraca njen id
     * @param string $name
     * @param User $user_id
     * @param int $company_id
     * @return int
     */
    public function addDirectoryDefinition($name, $user_id, $company_id)
    {        
        $json = '{"children":"", "attributes":{"show_unlisted":"false"}}';
                
        $definition = new DirectoryDefinition();
        $definition->children = "";
        $definition->show_unlisted = 0;
        $definition->is_skip = 1;
        $definition->null_name = $name;
        $definition->save();
        $definition->refresh();
                        
        $id = $definition->id;
                
        $definition_json = new DirectoryDefinitionJSON();
        $definition_json->directory_definition_id = $id;
        $definition_json->name = $name;
        $definition_json->user_id = $user_id;
        $definition_json->json = $json;
        $definition_json->company_id = $company_id;
        $definition_json->save();
             
        return $id;
    }
    
    public function addDefaultDirectoryDefinition($user_id, $company_id, $json, $location_code)
    {  
        $keys = $this->getDirectoryDefinitionsListIds($user_id, $company_id);
        $definition_array = json_decode($json, true);
        if(is_null($definition_array))
        {
            throw new Exception('error decoding default json');
        }
        $id = $this->addDirectoryDefinition($definition_array['name'], $user_id, $company_id);
        $keys[$definition_array['name']] = $id;
        $this->checkForPointers($definition_array['children'], $keys);
                
        $this->createDirectoryDefinitions($id, $user_id, $definition_array);
        
        $this->setUserSelectedDirectoryDefinition($user_id, $location_code, $id);
        
        return $id;
    }
    
    
    /**
     * Proverava da li defincija ima pokazivace. Ti pokazivaci su smesteni u nizu $keys
     * @param array $definition
     * @param array $keys
     */
    public function checkForPointers(&$definition, $keys)
    {
        foreach ($definition as $definition_key => $definition_value)
        {
            if (is_array($definition_value))
            {
                $this->checkForPointers($definition[$definition_key], $keys);
            }
            else if ($definition_key === 'definition_id')
            {
                if(isset($keys[$definition_value]))
                {
                    $definition[$definition_key] = $keys[$definition_value];
                }
                else
                {
                    /// MilosJ.
                    /// desava se da dodje ovde, a ne bi trebao
                    error_log(__METHOD__.' - Yii::app()->user->id: '.Yii::app()->user->id);
                    error_log(__METHOD__.' - $definition_key: '.CJSON::encode($definition_key));
                    error_log(__METHOD__.' - $definition: '.CJSON::encode($definition));
                    error_log(__METHOD__.' - $keys: '.CJSON::encode($keys));
                    error_log(__METHOD__.' - INPUT_GET: '.CJSON::encode(filter_input_array(INPUT_GET)));
                    error_log(__METHOD__.' - INPUT_POST: '.CJSON::encode(filter_input_array(INPUT_POST)));
                }
            }
            
        }
    }
    
    /**
     * brise definiciju
     * @param int $id definicije
     * @param User $user
     * @param bool $type - moze biti true ili false u zavisnosti da li se brise i pocetna definicija
     */
    public function removeDirectoryDefinition($id, $user_id, $company_id, $type=null)
    {
        $directory_definition = DirectoryDefinition::model()->findByPk($id);
        
        if ($directory_definition != null)
        {
            if ($directory_definition->children != '')
            {
                $dd_children = $directory_definition->children;
                $explode1 = explode("|", $dd_children);
                foreach($explode1 as $ex)
                {
                    $ids = explode(",", $ex);
                    
                    if(count($ids) > 0)
                    {
                        foreach ($ids as $value)
                        {
                            if (count($this->isDefinitionPointer($id, $user_id, $company_id)) == 0)
                            {
                                $this->removeDirectoryDefinition($value, $user_id, $company_id, true);
                            }
                        }
                    }
                }
            }

            if (isset($type) && $type === true)
            {
                $definitions = DirectoryDefinitionJSON::model()->findByAttributes(array('directory_definition_id'=>$id));

                if ($definitions === null)
                {
                    $definition = DirectoryDefinition::model()->findByPk($id);
                    $definition->delete();
                }
            }
        }
     
    }
    
    /**
     * setuje ime definicije za datog korisnika
     * @param int $id
     * @param string $name
     */
    public function setDirectoryDefinitionName($id, $name)
    {
        $json = DirectoryDefinitionJSON::model()->findByAttributes(array('directory_definition_id'=>$id));
        
        $json->name = $name;
        $json->update();
    }
    
    /**
     * proverava da li je definicija pokazivac u nekoj drugoj definiciji i vraca imena tih definicija ako postoje
     * @param int $id
     * @param User $user_id
     * @param int $company_id
     * @return array
     */
    public function isDefinitionPointer($id, $user_id, $company_id)
    {
        $names = array();
        $definitions = DirectoryDefinitionJSON::model()->findAllByAttributes(array(
            'user_id'=>$user_id, 
            'company_id'=>$company_id
        ),array('order'=>'id asc'));

        foreach ($definitions as $definition)
        {
            if ($id != $definition->directory_definition_id)
            {
                $ind = $this->findDefinitionAsChild($id, json_decode($definition->json, true));
                if ($ind === true)
                {
                    array_push($names, $definition->name);
                }
            }
        }
        
        return $names;
    }
    
    /**
     * trazi vrednost u multidimenzionalnom nizu
     * poziva se u isDefinitionPointer
     * @param int $id
     * @param array $definition
     * @return bool
     */
    private function findDefinitionAsChild($id, $definition)
    {
        foreach($definition as $key => $element) 
        {
            if($key === 'definition_id' && $element === $id)
            {
                return true;
            }
            if(is_array($element) && $this->findDefinitionAsChild($id, $element))
            {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * vraca id-eve pokazivaca unutar definicije koji nisu ukljuceni u export
     * @param int $id
     * @param array $definitions_to_export
     * @return array
     */
    public function getNotIncludedPointers($id, $definitions_to_export)
    {
        $ids = array();
        $definitions = DirectoryDefinitionJSON::model()->findByAttributes(array('directory_definition_id'=>$id));
        
        $this->findPointersInDefinition(json_decode($definitions->json, true), $ids, $definitions_to_export);
        
        return $ids;
    }
    
    /**
     * nalazi sve pokazivace unutar date definicije i ubacuje ih u niz ako se ne nalaze u listi definicija koje se exportuju
     * poziva se u getNotIncludedPointers
     * @param array $definition
     * @param array $ids
     * @param array $definitions_to_export
     */
    private function findPointersInDefinition($definition, &$ids, $definitions_to_export)
    {    
        foreach ($definition as $definition_key => $definition_value)
        {
            if (is_array($definition_value))
            {
                $this->findPointersInDefinition($definition_value, $ids, $definitions_to_export);
            }
            else if ($definition_key == 'definition_id')
            {
                if (!in_array($definition_value, $definitions_to_export))
                {
                    array_push($ids, $definition_value);
                }
            }           
        }
    }
    
    /**
     * trazi u definiciji id definicije koji je pokazivac druge definicije, ako ga nadje vraca true inace proverava svu decu koja su pokazivaci
     * @param array $definition
     * @param int $id
     * @param User $user
     * @return boolean
     */
    public function checkDefinitionChildrenForCircle($definition, $id)
    {
        foreach ($definition as $definition_key => $definition_value)
        {
            if ($definition_key != false && $definition_key == 'definition_id')
            {
                if ($definition_value == $id)
                {
                    return true;
                }
                else
                {
                    $child_definition = DirectoryDefinitionJSON::model()->findByAttributes(array('directory_definition_id'=>$definition_value));
                    if ($this->checkDefinitionChildrenForCircle(json_decode($child_definition->json, true), $id) == true)
                    {
                        return true;
                    }
                }
            }
            if (is_array($definition_value))
            {
                if ($this->checkDefinitionChildrenForCircle($definition_value, $id) == true)
                {
                        return true;
                }
            } 
        }
        
        return false;
    }
    
    public function getSelectedDirectoryDefinition($user, $company_id, $location_code)
    {
        $selected_directory_definition_id = null;
        
        $config_name = 'selected_directory_definition_ids';
        $user_params = CJSON::decode($user->config_params);
        if(!isset($user_params[$config_name]) || $user_params[$config_name] === '')
        {
            $user_params[$config_name] = array();
        }
        
        $definitions_list = $this->getDirectoryDefinitionsListNames($user->id, $company_id);
        
        if($location_code !== null)
        {
            if (isset($user_params[$config_name][$location_code]) && $user_params[$config_name][$location_code] != '')
            {
                $dd_ids = array_keys($definitions_list);
                foreach ($dd_ids as $id)
                {
                    if ($id == $user_params[$config_name][$location_code])
                    {
                        $selected_directory_definition_id = $user_params[$config_name][$location_code];
                    }
                }
            }
        }
        
        return $selected_directory_definition_id;
    }
    
    public function setUserSelectedDirectoryDefinition($user_id, $location_code, $dd_id)
    {
        $config_name = 'selected_directory_definition_ids';
        
        $user = User::model()->findByPk($user_id);
        $user_params = CJSON::decode($user->config_params);
                
        if(!isset($user_params[$config_name]))
        {
            $user_params[$config_name] = array();
        }
                
        $user_params[$config_name][$location_code] = $dd_id;
        $user->config_params = CJSON::encode($user_params);
        $user->save();
    }
}

