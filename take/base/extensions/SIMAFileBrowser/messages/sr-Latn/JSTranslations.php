<?php

return [
    'DDManagerDefinitionCurrentlySelected' => 'Definicija "{name}" je trenutno selektovana definicija pa ne mozete da je obrisete.'
                                                .' Izadjite iz Fajl Menadzera i promenite selektovanu definiciju u neku drugu pa pokusajte opet.'
];
