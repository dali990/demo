<?php

return [
    'EmptyDD' => 'Prazna definicija direktorijuma, molimo promenite',
    'FileBrowser' => 'Pretraživač',
    'UsedFilters' => 'Korišćeni filteri',
    'FilesChanges' => 'Prethodne izmene',
    'FileViewsHistory' => 'Istorija pregleda',
    'InvalidDirectoryDefinition' => 'Postavljena definicija nije validna.<br>Izaberite drugu ili izmenite postojeću.',
    'FolderDefinition' => 'Definicije foldera',
    'ShowUnlisted' => 'Prikaži neizlistane',
    'DefinitionGeneratedComplexQueryDirContent' => 'Izabrana definicija proizvodi previse kompleksan upit za dovlacenje foldera',
    'DefinitionGeneratedComplexQueryGetFiles' => 'Izabrana definicija proizvodi previse kompleksan upit za dovlacenje fajlova',
    'SIMAWarnExceptionQueryComplex' => 'Izabrana definicija proizvodi previse kompleksan upit',
    'UsedFilterBackSlasNotUsed' => 'u pretrazi se ignorise "\"'
];
