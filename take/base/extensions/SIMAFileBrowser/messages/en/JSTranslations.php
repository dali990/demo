<?php

return [
    'DDManagerDefinitionCurrentlySelected' => 'Definition "{name}" is currently selected so you can not delete it.'
                                                .' Exit File Manager, change selected definition and try again.'
];
