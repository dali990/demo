<?php

return [
    'FileBrowser' => 'File browser',
    'EmptyDD' => 'Empty directory definition, please change',
    'UsedFilters' => 'Used filters',
    'FilesChanges' => 'Files changes',
    'FileViewsHistory' => 'File views history',
    'FolderDefinition' => 'Folder definition',
    'ShowUnlisted' => 'Show unlisted'
];
