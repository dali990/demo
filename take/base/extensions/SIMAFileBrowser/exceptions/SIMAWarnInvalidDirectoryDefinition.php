<?php

class SIMAWarnInvalidDirectoryDefinition extends SIMAWarnException
{
    public function __construct()
    {
        $err_msg = Yii::t('SIMAFileBrowser.FileBrowser', 'InvalidDirectoryDefinition');
        parent::__construct($err_msg);
    }
}