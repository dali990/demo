<?php

class SIMAWarnExceptionQueryComplex extends SIMAWarnException
{
    public function __construct()
    {
        $err_msg = Yii::t('SIMAFileBrowser.FileBrowser', 'SIMAWarnExceptionQueryComplex');
        if(get_class($this) === SIMAWarnExceptionQueryComplexForGetDirContent::class)
        {
            $err_msg = Yii::t('SIMAFileBrowser.FileBrowser', 'DefinitionGeneratedComplexQueryDirContent');
        }
        else if(get_class($this) === SIMAWarnExceptionQueryComplexForGetFiles::class)
        {
            $err_msg = Yii::t('SIMAFileBrowser.FileBrowser', 'DefinitionGeneratedComplexQueryGetFiles');
        }
        
        parent::__construct($err_msg);
        
        $autobafreq_message = '__METHOD__: '.__METHOD__
                .' </br> get_class($this): '.get_class($this)
                .' </br> $err_msg: '.$err_msg;
        
        if(Yii::app()->isWebApplication())
        {            
            $autobafreq_message .= ' </br> INPUT_GET: '.SIMAMisc::toTypeAndJsonString(filter_input_array(INPUT_GET));
            $autobafreq_message .= ' </br> INPUT_POST: '.SIMAMisc::toTypeAndJsonString(filter_input_array(INPUT_POST));
        }
        
        Yii::app()->errorReport->createAutoClientBafRequest($autobafreq_message, 3161, 'AutoBafReq ComplexQuery');
    }
}

