<?php

/**
    How to use:

    view:
    $this->widget('base.extensions.SIMAFileBrowser.SIMAFileBrowser', array(
    ));

    <div class='sima-ui-tabs'>
    </div>
    ..

    $this->endWidget();

 */
class SIMAFileBrowser extends CWidget 
{
    public $id = null;
    public $new_layout = true;   /// obavezan
    public $location_code = null;   /// obavezan
    public $default_json = null;    /// obavezan
    public $user_id = null;         /// opcioni
    public $company_id = null;      /// opcioni 
    public $init_tql = null;        /// opcioni
    public $model_tag = null;        /// opcioni
    
    private $user_model = null;     
    
    public function run() 
    {        
        if (empty($this->id))
        {
            $this->id = SIMAHtml::uniqid().'_fb';
        }
        
        if($this->location_code === null)
        {
            throw new Exception('location_code is null');
        }
        if($this->default_json === null)
        {
            throw new Exception('default_json is null');
        }
        
        if($this->user_id === null || empty($this->user_id))
        {
            $this->user_id = Yii::app()->user->id;
        }
        $this->user_model = User::model()->findByPk($this->user_id);
        
        if($this->company_id === null || empty($this->company_id))
        {
           $this->company_id = Yii::app()->company->id;
        }
        
        $html = "";
        
        if($this->new_layout === true)
        {
            /// validiranje da postoji dd za ovu lokaciju
            $ddFileManager = Yii::app()->getModule('base')->ddmanager;
            $selected_directory_definition_id = $ddFileManager->getSelectedDirectoryDefinition($this->user_model, $this->company_id, $this->location_code);
            if (empty($selected_directory_definition_id))
            {            
                $selected_directory_definition_id = $ddFileManager->addDefaultDirectoryDefinition(
                        $this->user_model->id, 
                        $this->company_id, 
                        $this->default_json, 
                        $this->location_code
                );
            }
            
            $uniq = SIMAHtml::uniqid();
            
            $directoryDefinitionsDroplistId = $uniq.'_dd_d_id';
            $searchFilterId = $uniq.'_sf_id';
            
            $tree_id = $uniq.'_fb_tree';
            $tree_init_params = [
                'id' => $tree_id,
                'class' => 'sima-fb-tree',
                'load_on_init' => false,
                'list_tree_action'=>'filebrowser2/list',
                'list_tree_params' => [
                    'files_from_parents' => false,
                    'short_path' => false,
                    'exclude_empty' => false,
                    'used_filters' => []
                ]
            ];
            $tree = $this->widget('SIMATree2', $tree_init_params, true);
            $used_filters_and_tree = $this->render2('used_filters_and_tree', [
                'tree' => $tree
            ], true);
            
            $guitableId = $uniq.'_f_gt';
                        
            $html = $this->render2('index', [
                'id' => $this->id,
                'used_filters_and_tree' => $used_filters_and_tree,
                'filesGuiTableId' => $guitableId,
                'directoryDefinitionsDroplistId' => $directoryDefinitionsDroplistId,
                'searchFilterId' => $searchFilterId,
                'model_tag' => $this->model_tag
            ], true);
            
            $init_tql = SIMASQLFunctions::OptimizeQueryAndGetHash($this->init_tql);
            
            $json = json_encode([
                'id' => $this->id,
                'tree_id' => $tree_id,
                'tree_init_params' => $tree_init_params,
                'location_code' => $this->location_code,
                'filesGuiTableId' => $guitableId,
                'directoryDefinitionsDroplistId' => $directoryDefinitionsDroplistId,
                'user_id' => $this->user_id,
                'searchFilterId' => $searchFilterId,
                'init_tql' => $init_tql
            ]);

            $register_script = "$('#$this->id').fileBrowser2('init',$json);";
            Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
        }
        else
        {
            $ddFileManager = Yii::app()->getModule('base')->ddmanager;
            $selected_directory_definition_id = $ddFileManager->getSelectedDirectoryDefinition($this->user_model, $this->company_id, $this->location_code);

            if (isset($selected_directory_definition_id) && $selected_directory_definition_id != '')
            {
                $root_folder = "$$" . $selected_directory_definition_id . "$$";
            }
            else
            {            
                $selected_directory_definition_id = $ddFileManager->addDefaultDirectoryDefinition(
                        $this->user_model->id, 
                        $this->company_id, 
                        $this->default_json, 
                        $this->location_code
                );

                $root_folder = "$$" . $selected_directory_definition_id . "$$";
            }

            $treeFilters = $this->render('treeFilters', array(
                        'uniq' => SIMAHtml::uniqid(),
                        'id_set' => false,
                        'user' => $this->user_model,
                    ), 
                    true);


            if(!isset($root_folder))
            {
                $root_folder = $this->init_tql;
            }
            else
            {
                if(isset($this->init_tql))
                {
                    $root_folder .= '!!'.$this->init_tql;
                }
            }

            if (isset($root_folder))
            {
                $definitions_list = $ddFileManager->getDirectoryDefinitionsListNames($this->user_model->id, $this->company_id);
                $can_add_dir_to_root = SIMASQLFunctions::canAddFixDirForQuery($root_folder);
                if($can_add_dir_to_root)
                {
                    $can_add_dir_to_root = 'true';
                }

                $can_add_file_to_root = SIMASQLFunctions::canAddFileForQuery($root_folder);
                if($can_add_file_to_root)
                {
                    $can_add_file_to_root = 'true';
                }

                $id_set = false;

                $html = $this->render('index', array(
                    'uniq' => SIMAHtml::uniqid(),
                    'user_id' => $this->user_model->id,
                    'company_id' => $this->company_id,
                    'location_code' => $this->location_code,
                    'root_folder' => $root_folder,
                    'selected_directory_definition_id' => $selected_directory_definition_id,
                    'init_tql' => $this->init_tql,
                    'can_add_dir_to_root' => $can_add_dir_to_root,
                    'can_add_file_to_root' => $can_add_file_to_root,
                    'definitions_list' => $definitions_list,
                    'id_set' => $id_set,
                    'tree_filters' => $treeFilters
                        ), true);
            }
            else
            {
                $html = $this->render('index', array(
                    'uniq' => SIMAHtml::uniqid(),
                    'user_id' => $this->user_model->id,
                    'company_id' => $this->company_id,
                    'location_code' => $this->location_code,
                    'id_set' => false,
                    'tree_filters' => $treeFilters
                        ), true);
            }
        }
        
        echo $html;
    }
    
    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.fileBrowser.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.fileBrowser2.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.ddManager.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/fileBrowser.css');
        Yii::app()->clientScript->registerCssFile($baseUrl . '/fileBrowser2.css');
        Yii::app()->clientScript->registerCssFile($baseUrl . '/fileManager.css');
    }
    
    public function render($view, $params=null, $return = false)
    {
        $view = 'base.extensions.SIMAFileBrowser.views.fileBrowser.'.$view;
                
        return parent::render($view, $params, true);
    }
    
    public function render2($view, $params)
    {
        $view = 'base.extensions.SIMAFileBrowser.views.fileBrowser2.'.$view;
                
        return parent::render($view, $params, true);
    }
}
