

<tr class='sima-ui-fb-file <?php echo $file['file_from_parent']?"from-parent":""; ?>' 
    data-model-id="<?php echo $file['file_id']; ?>" 
    title="<?php echo $file['file_user_locked']?'Fajl je zaključao '.$file['lock_user_display_name']:'' ?>"
    data-file-from-parent="<?php echo $file['file_from_parent'] ? "true":"false"; ?>">
    <?php 

        if ($file['content_extension'] != 'NULL') {
            $ext = $file['content_extension'];
    ?>
        <td class='icon <?php echo $file['file_user_locked']?'locked':$file['content_extension'] ?>'></td>
    <?php }
        else {
    ?>
      <td class='icon default'></td>
    <?php 
        }
    ?>
    <td class='name'><?php echo $file['display_name']; ?></td>
    <td class='partner'><?php echo isset($file['partner'])?$file['partner']:'nije implementirano'; ?></td>
    <td class='note'><?php echo isset($file['note'])?$file['note']:'nije implementirano'; ?></td>
    <td class='size'><?php echo intval($file['file_size']); ?></td>
    <td class='type'><?php if (isset($ext)) echo $ext; ?></td>
    <td class='data_modified'><?php echo SIMAHtml::DbToUserDate($file['file_modtime']); ?></td>
</tr>

