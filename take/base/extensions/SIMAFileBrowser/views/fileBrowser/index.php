
<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>

<?php $uniq = SIMAHtml::uniqid();?>
 
<?php

    if (isset($params['description_panel']))
        $description_panel = $params['description_panel'];
    else
        $description_panel = true;

?>
<div id='<?=$uniq?>_panel' class='sima-layout-panel _splitter _vertical sima-ui-fb'>
<?php // $this->beginWidget('ext.SIMAResizablePanel.SIMAResizablePanel',
//    array(
//        'id'=>$uniq.'_panel',
//        'type'=>'vertical',
//        'proportions'=>array(0.2,0.5,0.3),
//        'class'=>'sima-ui-fb'
//        )); ?>

    <div class="sima-layout-panel sima-ui-tree-wrapper">
        <div class="options" style="padding-bottom: 10px;">
            <?php if(Yii::app()->docWiki->isInstalled()) {?>
                <span style='position:relative; z-index:5; margin-right:5px;vertical-align:top;' class='sima-icon _wiki _16' onclick='sima.icons.docWiki($(this),"SIMA_uputstvo_-_Pretrazivac_dokumenata");'></span>
            <?php } ?>
                <!--<span class="sima-icon _add _16 _disabled"></span>-->
                <span class="sima-icon _edit _16 _disabled"></span>
                <span class="sima-icon _delete _16 _disabled"></span>
            <?php if ((isset($id_set) && !$id_set)){?>
                <span class="sima-ui-tree-button-create_dd">
                    <img class="button" title="Dodaj/izmeni/obrisi definicije" src='<?php echo Yii::app()->getBaseUrl()?>/images/add2_16.png' />
                </span>
                <span class="sima-ui-tree-dd-list <?php echo isset($root_folder)?'':'disabled'; ?>">
                    <?php
                    if (isset($root_folder))
                    {
                        echo CHtml::dropDownList('directory_definition', $selected_directory_definition_id, $definitions_list); 
                    }
                    ?>
                </span>
            <?php }?>
                
            <input class='files-from-parents-cb' type='checkbox' title="Prikazivanje fajlova iz naddirektorijuma"/>
            <input class='short-path-cb' type='checkbox' title="Skrati nazive direktorijuma"/>
            <input class='exclude-empty-cb' type='checkbox' title="Ne prikazuj prazne direktorijume"/>
               
            <?php if(isset($tree_filters)) echo $tree_filters; ?>
        </div>
        <div class='sima-ui-tree scroll-container' data-leaf-id="<?php echo isset($root_folder)?$root_folder:''; ?>" data-init-tql="<?php echo isset($init_tql)?$init_tql:''; ?>" data-can-add-dir-to-root="<?php echo isset($can_add_dir_to_root)?$can_add_dir_to_root:''; ?>" data-can-add-file-to-root="<?php echo isset($can_add_file_to_root)?$can_add_file_to_root:''; ?>">
            <ul class="line">
                <?php
                    if (!isset($root_folder))
                    {
                        ?>
                        <li class="not_loaded">Nemate nijednu definiciju direktorijuma. Kreirajte prvo neku definiciju direktorijuma.</li>
                        <?php
                    }
                    else
                    {
                        ?>
                        <li class="not_loaded">NIJE UCITANO DRVO!!!!</li>
                        <?php
                    }
                ?>
            </ul>
        </div>
    </div>
    <div class="sima-layout-panel">

        <div class='sima-ui-fb-files-toolbar'  >

            <img title="Dodaj fajl" class='button add disabled'  src="images/add_16.png" />
            <img title="Izmeni fajl" class='button edit disabled'  src='images/edit_16.png' />
            <img title="Otvori fajl u dijalogu" class='button dialog disabled'  src='images/open_dialog_16.png' />
            <img title="Izbrisi fajl" class='button delete disabled'  src='images/delete_16.png' />
            <img title="Preuzmi fajl" class='button download disabled' src='images/down_16.png' />
            <img title="Preuzmi zip" class='button download_zip disabled' src='images/zip_16.png' />
            <?php 
                $not_connected_class = '_not_connected';
                if (isset(Yii::app()->sima_session['sima_apps_session']) 
                        && Yii::app()->sima_session['sima_apps_session'] !== -1)
                {
                    $not_connected_class = '';
                }
            ?>
            <span title='Klijent nije konektovan' class='button file_open sima-icon _open-in-client _16 <?php echo $not_connected_class; ?> _disabled' style="margin-top: -5px;"></span>
      
            <div class="files_count">
                Broj fajlova je: <span class="number"></span>
            </div>
            
        </div>

        <div class='sima-ui-fb-files scroll-container'>
            <div class='table_resize_wrapper'>
                <table id="<?php echo $uniq;?>" class='sima-ui-fb-files-display'>
                    <thead>
                            <tr>
                                    <th class='icon'></th>
                                    <th class='name'>Ime</th>
                                    <th class='partner'>Partner</th>
                                    <th class='note'>Napomena</th>
                                    <th class='size'>Velicina</th>
                                    <th class='type'>Tip fajla</th>
                                    <th class='data_modified'>Datum izmene</th>
                            </tr>
                    </thead>

                    <tbody>

                    </tbody>

                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>

    <?php
        if ($description_panel) {
    ?>    
            <div class="sima-layout-panel">
                <div class="description-wrapper scroll-container">
                    <div class="sima-disk-description-folder">
                        <p class="dir_type_wrapper" style="display:none;"></p>
                        <b>Naziv foldera:</b> <p class='name_folder'></p> 
                    </div>
                    <div class="sima-disk-description-file">
                    </div>
                </div>
            </div>

    <?php
        }
    ?>
</div>

<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>

<script type='text/javascript'>
    var params = {
        "user_id": <?php echo $user_id; ?>,
        "company_id": <?php echo $company_id; ?>,
        "location_code": "<?php echo $location_code; ?>"
    };
    
    $('#<?php echo $uniq;?>_panel').fileBrowser('init', params);
</script>