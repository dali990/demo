<button class="sima-ui-button filters">filteri</button>

<?php
//$security_tags = Yii::app()->user->model->tql_security_tags;
//$personal_tags = Yii::app()->user->model->limittag_personalTags;
$security_tags = $user->tql_security_tags;
$personal_tags = $user->limittag_personalTags;
$directory_definition_filters = Yii::app()->params['directoryDefinitionFilters'];
$directory_definition_types = Yii::app()->params['directoryDefinitionTypes'];
?>

<div class="filters-wrapper">
    <ul class="top-level">
        <?php if(count($security_tags) > 0) { ?>
        <li>
            Bezbednosni
            <ul class="sub-level">
                <?php 
                foreach ($security_tags as $tag) 
                {
                    $security_tag = TQLSecurityTag::model()->findByPk($tag->id);
                    if (isset($security_tag) && $security_tag != null)
                    {
                        $tag = $security_tag->tag;
                        if ($tag != null)
                        {
                            $value = $tag->query.'$R';
                            $display = $tag->name;
                            echo "<li><input type='checkbox' class='filter-checkbox' data-filter-display='$display' data-filter-value='$value'>$display</li>";
                        }
                    }
                } 
                ?>
            </ul>
        </li>
        <?php } ?>

        <?php if(count($personal_tags) > 0) { ?>
        <li>
            Licni
            <ul class="sub-level">
                <?php 
                foreach ($personal_tags as $tag) 
                {
                    $personal_tag = TQLPersonalTag::model()->findByPk($tag->id);
                    if (isset($personal_tag) && $personal_tag != null)
                    {
                        $tag = $personal_tag->tag;
                        if ($tag != null)
                        {
                            $value = $tag->query.'$R';
                            $display = $tag->name;
                            echo "<li><input type='checkbox' class='filter-checkbox' data-filter-display='$display' data-filter-value='$value'>$display</li>";
                        }
                    }
                } 
                ?>
            </ul>
        </li>
        <?php } ?>

        <?php if(count($directory_definition_filters) > 0 || count($directory_definition_types) > 0) { ?>
        <li>
            Ostali
            <ul class="sub-level">
                <?php
                foreach ($directory_definition_filters as $filter_key => $filter_value)
                {
                    if (isset($filter_value['personality']) && $filter_value['personality'] == true)
                    {
                        $limit_tag = TQLLimitTag::model()->findByAttributes(array('user_id'=>Yii::app()->user->id, 'name'=>$filter_key));
                    }
                    else
                    {
                        $limit_tag = TQLLimitTag::model()->findByAttributes(array('name'=>$filter_key));
                    }
                    
                    if (isset($limit_tag) && $limit_tag != null)
                    {
                        $tag = $limit_tag->tag;
                        if ($tag != null)
                        {
                            $value = $tag->query.'$R';
                            $display = $filter_value['alias'];
                            echo "<li><input type='checkbox' class='filter-checkbox' data-filter-display='$display' data-filter-value='$value'>$display</li>";
                        }
                    }
                }
                ?>

                <?php
                foreach ($directory_definition_types as $key => $value)
                {
                    if( isset($value['alias']) 
                            && (isset($value['filter']) || isset($value['types'])) )
                    {
                        $filter_table_name = $key::model()->tableName();
                        
                        echo "<li>";
                        echo $value['alias'];

                        echo "<ul class='sub-level'>";
                        if(isset($value['filter']))
                        {
                            if($value['filter']['type'] == 'table_name_filter')
                            {
                                $tag = FileTag::model()->findByAttributes(array('name'=>$filter_table_name));
                                
                                if ($tag != null)
                                {
                                    $v = $tag->query.'$R';
                                    $display = $value['filter']['alias'];
                                    echo "<li><input type='checkbox' class='filter-checkbox' data-filter-display='$display' data-filter-value='$v'>$display</li>";
                                }
                            }
                            
                            if($value['type'] == 'tag_group')
                            {
                                $val = 'TAG_GROUP$'.$filter_table_name.'$id$';
                                echo "<li><input type='checkbox' class='filter-checkbox' data-filter-display='$display' data-filter-value='$val'>$display";
                                $this->beginWidget('ext.SIMASearchField.SIMASearchField', array(
                                                    'model'=>$key,
                                               ));
                                $this->endWidget();
                                echo "</li>";
                            }
                        }
                            
                        if(isset($value['types']))
                        {
                            $attributes = '';
                            $filter_attributes = '';
                            $conditions = '';
                            if (isset($value['attributes']) && count($value['attributes']) > 0)
                            {
                                foreach ($value['attributes'] as $attribute)
                                {
                                    $attributes .= $attribute.";";
                                }
                                $attributes = rtrim($attributes, ";");
                            }
                            if (isset($value['filter']['attributes']) && count($value['filter']['attributes']) > 0)
                            {
                                foreach ($value['filter']['attributes'] as $attribute)
                                {
                                    $filter_attributes .= $attribute.";";
                                }
                            }
                            if (isset($value['conditions']) && count($value['conditions']) > 0)
                            {
                                $conditions = '{';
                                foreach ($value['conditions'] as $condition)
                                {
                                    $tag = FileTag::model()->findByAttributes(array('name'=>$condition));
                                    $conditions .= $tag->query.'$R'.",";
                                }
                                $conditions = substr($conditions, 0, -1).'}';
                            }

                            foreach($value['types'] as $key1 => $value1)
                            {
                                if(isset($value1['alias']) 
                                        && isset($value1['filter_type']) 
                                        && $value1['filter_type'][0] == 'searchModel')
                                {
                                    $Model = $value1['filter_type'][1];
                                    $display = $value1['alias'];
                                    
                                    $filtertype = '';
                                    if($value1['type'] == 'file_attribute')
                                    {
                                        $filtertype = 'FILE';
                                    }
                                    else if($value1['type'] == 'tag_group')
                                    {
                                        $filtertype = 'TAG_GROUP';
                                    }
                                    
                                    $val = $filtertype.'$'.$filter_table_name.'$'.$key1.'$';
                                    echo "<li><input type='checkbox' class='filter-checkbox' data-filter-display='$display' data-filter-value='$val'>$display";
                                    $this->beginWidget('ext.SIMASearchField.SIMASearchField', array(
                                                        'model'=>$Model,
                                                   ));
                                    $this->endWidget();
                                    echo "</li>";
                                }
                            }
                        }
                        else 
                        {
                            error_log(__METHOD__.' - '.'1 - $key: '.$key);
                            error_log(__METHOD__.' - '.'1 - $value: '.json_encode($value));
                        }

                        echo "</ul></li>";
                    }
                }
                ?>
            </ul>
        </li>
        <?php } ?>

        <li>
            Po imenu fajla
            <ul class="sub-level">
                <li>
                    <input type="checkbox" class="filter-checkbox" data-filter-display="Ime fajla" data-filter-value='FILE_NAME$'>
                    <input type="text" class="filter-filename" size="6">
                </li>
            </ul>
        </li>
    </ul>
</div>

<div>
Korisceni filteri:
<div class="used-filters"></div>
</div>
