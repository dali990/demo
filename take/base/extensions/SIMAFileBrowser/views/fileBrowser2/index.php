<?php	
$panels = [
    [
        'proportion'=> 0.01,
        'min_width' => 300,
        'max_width' => 3000,
        'class' => '_horizontal',
        'html' => $used_filters_and_tree
    ],
    [
        'html'=> $this->widget('SIMAGuiTable', [
            'id' => $filesGuiTableId,
            'model' => File::model(),
            'columns_type' => 'file_browser',
            'fixed_filter'=>array( 
                'ids' => -1
            )
        ], true)
    ]
];

$open_dd_manager_button = $this->widget(SIMAButtonVue::class, [
    'icon' => '_open-dd-manager',
    'onclick' => ['sima.callPluginMethod', '#'.$id, 'fileBrowser2', 'openDDManager'],
], true);

$file_vews_history_fixed_filter = [
    'user' => [
        'ids' => Yii::app()->user->id
    ],
    'order_by' => 'time DESC'
];

if(!empty($model_tag))
{
    $file_vews_history_fixed_filter['file'] = [
        'belongs' => $model_tag
    ];
}

$main_settings_button_subactions_data = [
    [
        'title' => Yii::t('SIMAFileBrowser.FileBrowser', 'FilesChanges'),
        'onclick' => ["sima.dialog.openActionInDialog", 'files/file/filesChanges']
    ],
    [
        'title' => Yii::t('SIMAFileBrowser.FileBrowser', 'FileViewsHistory'),
        'onclick' => ["sima.dialog.openActionInDialogAsync", 'guitable', [
                'get_params' => [
                    'settings' => [
                        'model' => FileView::class,
                        'columns_type' => 'file_browser',
                        'fixed_filter' => $file_vews_history_fixed_filter
                    ],
                    'label' => Yii::t('SIMAFileBrowser.FileBrowser', 'FileViewsHistory')
                ]
            ]
        ]
    ]
];
if(Yii::app()->user->checkAccess('modelDocumentTypeAdmin'))
{
    $main_settings_button_subactions_data[] = [
        'title' => Yii::t('FilesModule.DocumentType', 'DocumentTypes'),
        'onclick' => ['sima.mainTabs.openNewTab', 'guitable', 'files_11']
    ];
}
$main_settings_button = $this->widget(SIMAButtonVue::class, [
    'icon' => '_main-settings',
    'iconsize' => 18,
    'subactions_data' => $main_settings_button_subactions_data
], true);

echo Yii::app()->controller->renderContentLayout([
        'name'=> Yii::t('SIMAFileBrowser.FileBrowser', 'FileBrowser'),                
        'options_class'=>'sima-fb-options',
        'options'=>[
            [
                'html' => $this->widget(SIMAUIFilter::class, [
                    'id' => $searchFilterId,
                    'search_function' => ['sima.callPluginMethod', '#'.$id, 'fileBrowser2', "searchFilter"],
                ], true)
            ],
//            [
//                'html' => $this->widget('SIMADesktopApp', [], true)
//            ],
            [
                'html' => '<div id="'.$directoryDefinitionsDroplistId.'"></div>'
            ],
            [
                'html' => $open_dd_manager_button
            ],
            [
                'html' => $main_settings_button
            ]
        ]
    ], 
    $panels, 
    [
        'id' => $id,
        'class'=>'sima-fb',
        'processOutput'=>false
    ]
);
