
<div id="<?=$uniq_id?>" class="sima-ui-file-manager sima-layout-panel _splitter _vertical">
    <div class="sima-layout-panel sima-layout-panel" data-sima-layout-init='{"proportion":0.2}'>
        <div class="sima-ui-file-manager-directory_definitions sima-layout-panel _horizontal">
            <div class="header sima-layout-fixed-panel">
                <span class="title"><?php echo Yii::t('SIMAFileBrowser.FileBrowser', 'FolderDefinition'); ?></span>
                <span class="sima-icon _add _16" title="Dodaj">
                </span>
                <span class="sima-icon _edit _16 _disabled" title="Izmeni">
                </span>
                <span class="sima-icon _copy _16 _disabled" title="Kopiraj">
                </span>
                <span class="sima-icon _delete _16 _disabled" title="Obrisi">
                </span>
                <span class="sima-icon _export _16" title="Izvezi definicije">
                </span>
                <span class="sima-icon _import _16" title="Uvezi definicije">
                    <?php
//                        $this->widget('application.extensions.EAjaxUpload.EAjaxUpload', array(
//                            'id' => SIMAHtml::uniqid(),
//                            'config' => array(
//                                'action' => Yii::app()->createUrl('files/transfer/upload'),
//                                'allowedExtensions' => array("json"),
//                                'sizeLimit' => Yii::app()->params['upload_sizeLimit'], // maximum file size in bytes
//                                'onComplete' => 'js:function(id, fileName, responseJSON){
//                                                    $("#' . $uniq_id . '").ddManager("importDefinitions");
//                                                }',
//                                'messages' => Yii::app()->params['AjaxUploadMessages'],
//                                'drag_and_drop' => false,
//                                'img_class' => 'sima-icon _upload _16',
//                            )
//                        ));
                    ?>
                </span>
            </div>
            <div class="definition_attributes sima-layout-fixed-panel">
                <span class="attribute_item">
                    <?php echo Yii::t('SIMAFileBrowser.FileBrowser', 'ShowUnlisted'); ?><input type="checkbox" id="show_unlisted" name="show_unlisted">
                </span>
            </div>
            <div class="sima-layout-panel">
                <ul>
                    <?php
                    foreach ($directory_definitions as $directory_definition)
                    {
                    ?>
                        <li class="directory_definition" data-id='<?php echo $directory_definition['id']; ?>' data-show_unlisted=<?php echo $directory_definition['attributes']['show_unlisted']?>>
                            <span class="icon"></span>
                            <span class="name"><?php echo $directory_definition['name']; ?></span>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="sima-layout-panel">
        <?php
            $this->widget('SIMADDCreator', array(
                'id' => $dd_creator_id,
                'name' => $dd_tree_name,
                'tree_json' => $dd_tree_name,
                'security_tags'=> $security_tags,
                'personal_tags'=> $personal_tags,
            ));
        ?>
    </div>
</div>
        
<?php
    $params = array(
        'user_id' => $user_id,
        'company_id' => $company_id,
        'location_code' => $location_code,
        'selected' => $selected,
        'dd_creator_id' => $dd_creator_id
    );
    $json = CJSON::encode($params);
    $register_script = "$('#" . $uniq_id . "').ddManager('init', $json);";
    Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY); 
?>
