    
<?php
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>

    <ul>
        <?php
         foreach ($directory_definitions as $directory_definition)
         {
             ?>
             <li class="directory_definition" data-id='<?php echo $directory_definition['id']; ?>' data-show_unlisted=<?php echo $directory_definition['attributes']['show_unlisted']?>>
                 <span class="icon"></span>
                 <span class="name"><?php echo $directory_definition['name']; ?></span>
             </li>
             <?php
         }
        ?>
    </ul>

<?php
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>