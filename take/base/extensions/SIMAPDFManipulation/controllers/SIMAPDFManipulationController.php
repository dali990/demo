<?php

class SIMAPDFManipulationController extends SIMAController
{
    public function actionIndex()
    {
        $html = $this->widget('SIMAPDFManipulation', [], true);
        
        $title = 'Manipulacija PDF Dokumentima';
        
        $this->respondOK([
            'html' => $html,
            'title' => $title
        ]);
    }
    
    public function actionParseUploadedFile()
    {
        $upload_session_key = $this->filter_post_input('upload_session_key');
        
        $upload_session_key_parsed = SIMAUpload::ParseUploadKey($upload_session_key);
        $temp_file_name = $upload_session_key_parsed['temp_file_name'];
        $file_display_name = $upload_session_key_parsed['file_display_name'];
        
        $tempFile = TemporaryFile::GetByName($temp_file_name);
        
        if(!$tempFile->validateIsPDF(false))
        {
            $this->respondFail(Yii::t('SIMAPDFManipulation.Translation', 'UploadedFileIsNotPDF'));
        }
        
        try
        {
            $pages = $tempFile->getPdfPagesAsImagesArray();
        }
        catch(SIMAExceptionFileSizeLimit $e)
        {
            throw new SIMAWarnException(Yii::t('SIMAPDFManipulation.Translation', $e->getMessage()));
        }
        
        $this->respondOK([
            'temp_file_name' => $temp_file_name,
            'file_display_name' => $file_display_name,
            'pages' => $pages
        ]);
    }
    
    public function actionParseChosenFileIds()
    {        
        $file_ids = $this->filter_post_input('file_ids');

        $files_for_manipulation = [];
        
        foreach($file_ids as $file_id)
        {
            $fileModel = File::model()->findByPkWithCheck($file_id);
            
            $tempFile = TemporaryFile::CreateFromFile($fileModel);
            
            $temp_file_name = $tempFile->GetFileName();
            $file_display_name = $fileModel->DisplayName;
            try
            {
            $pages = $tempFile->getPdfPagesAsImagesArray();
            }
            catch(SIMAExceptionFileSizeLimit $e)
            {
                throw new SIMAWarnException(Yii::t('SIMAPDFManipulation.Translation', $e->getMessage()));
            }
            
            $files_for_manipulation[] = [
                'temp_file_name' => $temp_file_name,
                'file_display_name' => $file_display_name,
                'pages' => $pages
            ];
        }
        
        $this->respondOK([
            'files_for_manipulation' => $files_for_manipulation
        ]);
    }
    
    public function actionCreateDownloadablePDF()
    {
        $document_pages = $this->filter_post_input('document_pages');
        
        $resultTempFile = Yii::app()->stapler->createTempPDF($document_pages);
        
        if(empty($resultTempFile))
        {
            throw new Exception(Yii::t('SIMAPDFManipulation.Translation', 'ErrorCreatingPDF'));
        }
        
        $this->respondOK([
            'tn' => $resultTempFile->getFileName(),
            'dn' => 'File.pdf'
        ]);
    }
    
    public function actionSaveAsNewVersionForFile()
    {
        $document_pages = $this->filter_post_input('document_pages');
        $file_id = $this->filter_post_input('file_id');
        
        $file = File::model()->findByPkWithCheck($file_id);
        
        $resultTempFile = Yii::app()->stapler->createTempPDF($document_pages);
        
        $file->appendTempFile($resultTempFile->getFileName(), 'File.pdf');
                
        $this->respondOk([
            'file_id' => $file->id
        ]);
    }
    
    public function actionsaveNewFile()
    {
        $document_pages = $this->filter_post_input('document_pages');
        
        $resultTempFile = Yii::app()->stapler->createTempPDF($document_pages);
        
        $file = new File();
        $file->responsible_id = Yii::app()->user->id;
        
        $file->file_version_id = CJSON::encode([
            'key' => 'SIMAUpload',
            'status' => SIMAUpload::$STATUS_SUCCESS,
            'temp_file_name' => $resultTempFile->getFileName(),
            'file_display_name' => 'File.pdf',
            'additional_data' => [
                'explicit_temp_upload' => true
            ]
        ]);
        
        $file->save();
        
        $this->respondOk([
            'file_id' => $file->id
        ]);
    }
}
