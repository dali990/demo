<div id="<?php echo $id; ?>" class="sima-layout-panel _splitter _vertical">
    <div class="s-b-pdfm-files sima-layout-panel"
         data-sima-layout-init='<?=CJSON::encode([
                'min_width' => 400,
                'max_width' => 400
            ])?>'>
        <div class="sima-layout-fixed-panel">
        <?php
        if(SIMAMisc::isVueComponentEnabled())
        {
            $this->widget(SIMAButtonVue::class, [
                'title' => Yii::t('SIMAPDFManipulation.Translation', 'AddFile'),
                'onclick' => [
                    "sima.callPluginMethod", "#".$id, "pdfManipulation", "addFile"
                ]
            ]);
            $this->widget(SIMAButtonVue::class, [
                'title' => Yii::t('SIMAPDFManipulation.Translation', 'ChooseFile'),
                'onclick' => [
                    "sima.callPluginMethod", "#".$id, "pdfManipulation", "chooseFile"
                ]
            ]);
        }
        else
        {
            $this->widget('SIMAButton', [
                'action' => [
                    'title' => Yii::t('SIMAPDFManipulation.Translation', 'AddFile'),
                    'onclick' => [
                        "sima.callPluginMethod", "#".$id, "pdfManipulation", "addFile"
                    ]
                ]
            ]);
            $this->widget('SIMAButton', [
                'action' => [
                    'title' => Yii::t('SIMAPDFManipulation.Translation', 'ChooseFile'),
                    'onclick' => [
                        "sima.callPluginMethod", "#".$id, "pdfManipulation", "chooseFile"
                    ]
                ]
            ]);
        }
        ?>
        </div>
        <div class="s-b-pdfm-files-list sima-layout-panel"></div>
    </div>
    <div class="s-b-pdfm-pages sima-layout-panel _horizontal"
         data-sima-layout-init='<?=CJSON::encode([
                'min_width' => 500,
                'max_width' => 2000
            ])?>'>
        <div class="sima-layout-fixed-panel">
        <?php
        if(SIMAMisc::isVueComponentEnabled())
        {
            $this->widget(SIMAButtonVue::class, [
                'title' => Yii::t('SIMAPDFManipulation.Translation', 'AddSelectedPages'),
                'onclick' => [
                    "sima.callPluginMethod", "#".$id, "pdfManipulation", "addSelectedPages"
                ]
            ]);
        }
        else
        {
            $this->widget('SIMAButton', [
                'action' => [
                    'title' => Yii::t('SIMAPDFManipulation.Translation', 'AddSelectedPages'),
                    'onclick' => [
                        "sima.callPluginMethod", "#".$id, "pdfManipulation", "addSelectedPages"
                    ]
                ]
            ]);
        }
        ?>
        </div>
        <div class="s-b-pdfm-pages-list sima-layout-panel"></div>
    </div>
    <div class="s-b-pdfm-result sima-layout-panel _horizontal"
         data-sima-layout-init='<?=CJSON::encode([
                'min_width' => 250,
                'max_width' => 250
            ])?>'>
        <div class="sima-layout-fixed-panel">
        <?php
            if(SIMAMisc::isVueComponentEnabled())
            {
                $this->widget(SIMAButtonVue::class, [
                    'title' => Yii::t('SIMAPDFManipulation.Translation', 'RemoveSelectedPages'),
                    'onclick' => [
                        "sima.callPluginMethod", "#".$id, "pdfManipulation", "removeSelectedPages"
                    ]
                ]);
                $this->widget(SIMAButtonVue::class, [
                    'id' => $saveButtonId,
                    'title' => Yii::t('SIMAPDFManipulation.Translation', 'SavePDF'),
                    'onclick' => [
                        "sima.callPluginMethod", "#".$id, "pdfManipulation", "downloadPDF"
                    ],
                    'subactions_data' => [
                        [
                            'title' => Yii::t('SIMAPDFManipulation.Translation', 'SaveNewVersionThisFile'),
                            'onclick' => [
                                "sima.callPluginMethod", "#".$id, "pdfManipulation", "saveNewVersionThisFile"
                            ],
                            'htmlOptions' => [
                                'data-code' => 'SaveNewVersionThisFile'
                            ]
                        ],
                        [
                            'title' => Yii::t('SIMAPDFManipulation.Translation', 'SaveNewVersionOtherFile'),
                            'onclick' => [
                                "sima.callPluginMethod", "#".$id, "pdfManipulation", "saveNewVersionOtherFile"
                            ]
                        ],
                        [
                            'title' => Yii::t('SIMAPDFManipulation.Translation', 'SaveNewFile'),
                            'onclick' => [
                                "sima.callPluginMethod", "#".$id, "pdfManipulation", "saveNewFile"
                            ]
                        ],
                        [
                            'title' => Yii::t('SIMAPDFManipulation.Translation', 'DownloadPDF'),
                            'onclick' => [
                                "sima.callPluginMethod", "#".$id, "pdfManipulation", "downloadPDF"
                            ]
                        ]
                    ]
                ]);
            }
            else
            {
                $this->widget('SIMAButton', [
                    'action' => [
                        'title' => Yii::t('SIMAPDFManipulation.Translation', 'RemoveSelectedPages'),
                        'onclick' => [
                            "sima.callPluginMethod", "#".$id, "pdfManipulation", "removeSelectedPages"
                        ]
                    ]
                ]);
                $this->widget('SIMAButton', [
                    'id' => $saveButtonId,
                    'action' => [
                        'title' => Yii::t('SIMAPDFManipulation.Translation', 'SavePDF'),
                        'onclick' => [
                            "sima.callPluginMethod", "#".$id, "pdfManipulation", "downloadPDF"
                        ],
                        'subactions' => [
                            [
                                'title' => Yii::t('SIMAPDFManipulation.Translation', 'SaveNewVersionThisFile'),
                                'onclick' => [
                                    "sima.callPluginMethod", "#".$id, "pdfManipulation", "saveNewVersionThisFile"
                                ],
                                'htmlOptions' => [
                                    'data-code' => 'SaveNewVersionThisFile'
                                ]
                            ],
                            [
                                'title' => Yii::t('SIMAPDFManipulation.Translation', 'SaveNewVersionOtherFile'),
                                'onclick' => [
                                    "sima.callPluginMethod", "#".$id, "pdfManipulation", "saveNewVersionOtherFile"
                                ]
                            ],
                            [
                                'title' => Yii::t('SIMAPDFManipulation.Translation', 'SaveNewFile'),
                                'onclick' => [
                                    "sima.callPluginMethod", "#".$id, "pdfManipulation", "saveNewFile"
                                ]
                            ],
                            [
                                'title' => Yii::t('SIMAPDFManipulation.Translation', 'DownloadPDF'),
                                'onclick' => [
                                    "sima.callPluginMethod", "#".$id, "pdfManipulation", "downloadPDF"
                                ]
                            ]
                        ]
                    ]
                ]);
            }
        ?>
        </div>
        <div class="s-b-pdfm-pages-list sima-layout-panel">
            <ul class="ui-sortable"></ul>
        </div>
    </div>
</div>
