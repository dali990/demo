/* global sima */

(function($){
    var methods = {
        init: function(params){
            var localThis = $(this);
            
            localThis.data('files_for_manipulation', []);
            localThis.data('file_models_ids', params.file_models_ids);
            
            localThis.on('click', '.s-b-pdfm-remove-file', {localThis:localThis}, onRemoveFile);
            localThis.on('click', '.s-b-pdfm-file-display-name', {localThis:localThis}, onDisplayNameSelect);
            localThis.on('click', '.s-b-pdfm-page', onPageSelect);
            
            localThis.find('.s-b-pdfm-result').find('.s-b-pdfm-pages-list').find('ul').sortable({
                cursor: "move",
                axis: "y"
            });
            
            if(!sima.isEmpty(params.file_models_ids))
            {
                parseChosenFileIds(localThis, params.file_models_ids);
            }
            
            if(params.is_from_file_model !== true)
            {
                var saveButtonId = params.saveButtonId;
                var saveButtonObj = $('#'+saveButtonId);
                if(saveButtonObj.hasClass('_vue'))
                {
                    var vue_obj = sima.vue.getVueComponentFromObj(saveButtonObj);
                    vue_obj.disableSubActionsByCode('SaveNewVersionThisFile');
                }
                else
                {
                    saveButtonObj.simaButton('disableSubActionsByCode', 'SaveNewVersionThisFile');
                }
            }
        },
        addFile: function(){
            var localThis = $(this);
            var params = {
                status: 'validate',
                onSave: function(response){
                    var upload_session_key = response.form_data.File.attributes.file_version_id;
                    
                    sima.ajax.get('pdfManipulation/parseUploadedFile',{
                        data:{
                            upload_session_key: upload_session_key
                        },
                        async: true,
                        loadingCircle: localThis.find('.s-b-pdfm-files').find('.s-b-pdfm-files-list'),
                        success_function:function(response){
                            var file_for_manipulation = {
                                'display_name': response.file_display_name,
                                'temp_name': response.temp_file_name,
                                'pages': response.pages
                            };

                            var files_for_manipulation = localThis.data('files_for_manipulation');
                            files_for_manipulation.push(file_for_manipulation);
                            localThis.data('files_for_manipulation', files_for_manipulation);

                            rebuildFilesForManipulation(localThis);
                        }
                    });
                },
                formName: 'justupload'
            };
            sima.model.form('File', '', params);
        },
        chooseFile: function(){
            var localThis = $(this);
            sima.model.choose('File', function(data){
                var file_ids = [];
                
                $.each(data, function(index, value){
                    file_ids.push(value.id);
                });
                
                parseChosenFileIds(localThis, file_ids);
            }, {
                multiselect: true,
                fixed_filter: {
                    'last_version': {
                        'file_extension': {
                            'name': 'pdf'
                        }
                    }
                }
            });
        },
        addSelectedPages: function()
        {
            var localThis = $(this);
            
            var selected_pages = localThis.find('.s-b-pdfm-pages').find('.s-b-pdfm-pages-list').find('._selected');
            
            if(selected_pages.length === 0)
            {
                sima.dialog.openInfo(sima.translate('NoSelectedPages'));
            }
            else
            {
                var pages_div = localThis.find('.s-b-pdfm-result').find('.s-b-pdfm-pages-list').find('ul');

                localThis.find('.s-b-pdfm-pages').find('.s-b-pdfm-pages-list').find('.s-b-pdfm-page').removeClass('_selected');

                $.each(selected_pages, function(index, value){
                    var cloned_page = $(value).clone();
                    var li = $('<li class=\'ui-state-default ui-sortable-handle\'></li>');
                    li.append(cloned_page);
                    pages_div.append(li);
                });
            }
        },
        removeSelectedPages: function()
        {
            var localThis = $(this);
            
            var selected_pages = localThis.find('.s-b-pdfm-result').find('.s-b-pdfm-pages-list').find('._selected');
            
            if(selected_pages.length === 0)
            {
                sima.dialog.openInfo('Nema izabranih strana');
            }
            else
            {
                $.each(selected_pages, function(index, value){
                    $(value).parent('li').remove();
                });
            }
        },
        downloadPDF: function()
        {
            var localThis = $(this);
            var pages = validateAndParsePages(localThis);
            if(pages === false)
            {
                return;
            }

            sima.ajax.get('pdfManipulation/createDownloadablePDF',{
                data:{
                    document_pages: pages
                },
                async: true,
                loadingCircle: $('body'),
                success_function:function(response){
                    sima.temp.download(response.tn, response.dn);
                }
            });
        },
        saveNewVersionThisFile: function()
        {
            var localThis = $(this);
            var pages = validateAndParsePages(localThis);
            if(pages === false)
            {
                return;
            }
            
            $.each(localThis.data('file_models_ids'), function(index, value){
                saveAsNewVersionForFile(pages, value);
            });
        },
        saveNewVersionOtherFile: function()
        {            
            var localThis = $(this);
            var pages = validateAndParsePages(localThis);
            if(pages === false)
            {
                return;
            }
            
            sima.model.choose('File', function(data){
                var file_id = null;
                $.each(data, function(index, value){
                    file_id = value.id;
                });
                saveAsNewVersionForFile(pages, file_id, true);
            }, {view:'guiTable'});
        },
        saveNewFile: function()
        {            
            var localThis = $(this);
            var pages = validateAndParsePages(localThis);
            if(pages === false)
            {
                return;
            }
            
            sima.ajax.get('pdfManipulation/saveNewFile',{
                data:{
                    document_pages: pages
                },
                async: true,
                loadingCircle: $('body'),
                success_function:function(response){
                    sima.dialog.openModel('files/file',response.file_id);
                }
            });
        }
    };
    
    function validateAndParsePages(localThis)
    {
        var pages = localThis.find('.s-b-pdfm-result').find('.s-b-pdfm-page');
        if(pages.length === 0)
        {
            sima.dialog.openInfo('Nema strana za preuzimanje');
            return false;
        }
        
        var parsed_pages = [];
        $.each(pages, function(index, value){
            var _this = $(value);
            parsed_pages.push({
                'temp_name': _this.data('document_temp_name'),
                'page_number': _this.data('page_number')
            });
        });
        
        return parsed_pages;
    }
    
    function saveAsNewVersionForFile(pages, file_id, open_after_save)
    {
        sima.ajax.get('pdfManipulation/saveAsNewVersionForFile',{
            data:{
                document_pages: pages,
                file_id: file_id
            },
            async: true,
            loadingCircle: $('body'),
            success_function:function(response){
                if(open_after_save === true)
                {
                    sima.dialog.openModel('files/file',response.file_id);
                }
                else
                {
                    sima.dialog.openInfo('Uspesno');
                }
            }
        });
    }
    
    function fileTemplate(counter, display_name, temp_name)
    {
        var file_template = $('<div class=\'s-b-pdfm-file\' data-temp_name=\''+temp_name+'\'>'
                +'<div class=\'s-b-pdfm-remove-file\'>X</div>'
                +'<div class=\'s-b-pdfm-file-counter\'>'+counter+'.</div>'
                +'<div class=\'s-b-pdfm-file-display-name\'>'+display_name+'</div>'
            +'</div>');
        return file_template;
    }
    
    function pageTemplate(page_image, document_temp_name, document_number, page_number)
    {
        var page_template = '<div '
                +'data-document_temp_name=\''+document_temp_name+'\' '
                +'data-page_number=\''+page_number+'\' '
                +'class=\'s-b-pdfm-page\'>'
            +page_image
            +'<div>Document '+document_number+' Page '+page_number+'</div>'
            +'</div>';
        return page_template;
    }
    
    function rebuildFilesForManipulation(localThis)
    {
        var selected_file = localThis.find('.s-b-pdfm-file._selected');
        var temp_name = null;
        if(!sima.isEmpty(selected_file))
        {
            temp_name = selected_file.data('temp_name');
        }
        
        var files_for_manipulation = localThis.data('files_for_manipulation');
        var files_list = localThis.find('.s-b-pdfm-files-list');
        
        files_list.empty();
        
        var counter = 0;
        
        $.each(files_for_manipulation, function(index, value){
            counter++;
            var file_template = fileTemplate(counter, value.display_name, value.temp_name);
            files_list.append(file_template);
            
            if(!sima.isEmpty(temp_name) && value.temp_name === temp_name)
            {
                file_template.addClass('_selected');
            }
        });
    }
    
    function onRemoveFile(event)
    {
        event.stopPropagation();
        
        var _this = $(this);
        var localThis = event.data.localThis;
                
        var file_wrap = _this.parent('.s-b-pdfm-file');
        
        var temp_name = file_wrap.data('temp_name');
        
        var new_files_for_manipulation = [];
        var files_for_manipulation = localThis.data('files_for_manipulation');
        $.each(files_for_manipulation, function(index, value){
            if(value.temp_name !== temp_name)
            {
                new_files_for_manipulation.push(value);
            }
        });
        localThis.data('files_for_manipulation', new_files_for_manipulation);
        
        rebuildFilesForManipulation(localThis);
    }
    
    function onDisplayNameSelect(event)
    {
        event.stopPropagation();
        
        var _this = $(this);
        var localThis = event.data.localThis;
                
        var file_wrap = _this.parent('.s-b-pdfm-file');
        
        if(!file_wrap.hasClass('_selected'))
        {
            localThis.find('.s-b-pdfm-file').removeClass('_selected');
            
            file_wrap.addClass('_selected');
            
            rebuildPDFPagesForSelectedFile(localThis);
        }
    }
    
    function rebuildPDFPagesForSelectedFile(localThis)
    {
        var selected_file = localThis.find('.s-b-pdfm-file._selected');
        var temp_name = selected_file.data('temp_name');
        var document_number = selected_file.find('.s-b-pdfm-file-counter').text();
        
        var pages_div = localThis.find('.s-b-pdfm-pages').find('.s-b-pdfm-pages-list');
        sima.set_html(pages_div, '');
        
        var files_for_manipulation = localThis.data('files_for_manipulation');
        $.each(files_for_manipulation, function(index, value){
            if(value.temp_name === temp_name)
            {
                var pages_div_html = '';
                var counter=0;
                $.each(value.pages, function(index, page){
                    counter++;
                    pages_div_html += pageTemplate(page, temp_name, document_number, counter);
                });
                sima.set_html(pages_div, pages_div_html);
                
                return false; /// break
            }
        });
    }
    
    function onPageSelect(event)
    {
        event.stopPropagation();
        
        var _this = $(this);
        
        _this.toggleClass('_selected');
    }
    
    function parseChosenFileIds(localThis, file_ids)
    {
        sima.ajax.get('pdfManipulation/parseChosenFileIds',{
            data: {
                file_ids: file_ids
            },
            async: true,
            loadingCircle: localThis.find('.s-b-pdfm-files').find('.s-b-pdfm-files-list'),
            success_function:function(response){
                var files_for_manipulation = localThis.data('files_for_manipulation');

                $.each(response.files_for_manipulation, function(index, value){
                    var file_for_manipulation = {
                        'display_name': value.file_display_name,
                        'temp_name': value.temp_file_name,
                        'pages': value.pages
                    };

                    files_for_manipulation.push(file_for_manipulation);
                });

                localThis.data('files_for_manipulation', files_for_manipulation);

                rebuildFilesForManipulation(localThis);
            }
        });
    }
    
    $.fn.pdfManipulation = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.pdfManipulation');
            }
        });
        return ret;
    };
})( jQuery );