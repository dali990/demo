<?php

return [
    'UploadedFileIsNotPDF' => 'Dokument nije PDF',
    'AddFile' => 'Uvezi PDF fajl sa racunara',
    'AddSelectedPages' => 'Dodaj izabrane strane',
    'RemoveSelectedPages' => 'Ukloni izabrane strane',
    'DownloadPDF' => 'Preuzmi PDF',
    'ChooseFile' => 'Izaberi PDF fajl iz IS SIMA',
    'SavePDF' => 'Sacuvaj',
    'SaveNewVersionThisFile' => 'Kao novu verziju ovom fajlu',
    'SaveNewVersionOtherFile' => 'Kao novu verziju drugom fajlu',
    'SaveNewFile' => 'Kao novi fajl',
    'SaveNewVersionThisFile' => 'Kao novu verziju ovom fajlu',
    'GetPdfPagesAsImagesArray' => 'Fajl je prevelik da bi se obradio u PDF manipulatoru'
];
