<?php

return [
    'UploadedFileIsNotPDF' => 'Uploaded file is not PDF',
    'AddFile' => 'Upload PDF',
    'AddSelectedPages' => 'Add selected pages',
    'RemoveSelectedPages' => 'Remove selected pages',
    'DownloadPDF' => 'Download PDF',
    'ChooseFile' => 'Choose PDF file from IS SIMA',
    'SavePDF' => 'Save',
    'SaveNewVersionThisFile' => 'Save as new version for this file',
    'SaveNewVersionOtherFile' => 'Save as new version for other file',
    'SaveNewFile' => 'Save as new file',
     'GetPdfPagesAsImagesArray' => 'File is too large for parsing in PDF manipulator'
];
