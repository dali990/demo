<?php

class SIMAPDFManipulation extends CWidget
{
    public $id = null;
    public $file_models = [];
    public $file_models_ids = [];
    public $is_from_file_model = false;
    
    public function run() 
    {
        $uniq = SIMAHtml::uniqid();
        
        if(empty($this->id))
        {
            $this->id = $uniq.'_sima_pdf_manipulation';
        }
        
        $saveButtonId = $uniq.'_save_button';
        
        echo $this->render('index', [
            'id' => $this->id,
            'saveButtonId' => $saveButtonId
        ]);
        
        foreach($this->file_models as $file_model)
        {
            $this->file_models_ids[] = $file_model->id;
        }
        
        $js_params = [
            'file_models_ids' => $this->file_models_ids,
            'saveButtonId' => $saveButtonId,
            'is_from_file_model' => $this->is_from_file_model
        ];
        $js_params_json = CJSON::encode($js_params);
        $register_script = "$('#".$this->id."').pdfManipulation('init', $js_params_json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
    
    public static function registerManual()
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.pdfManipulation.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/simaPDFManipulation.css');
    }
}

