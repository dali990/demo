(function($){
    var methods = {
        init: function(params){
            var localThis = this;
            
            localThis.data('multiselect', params.multiselect);
        },
        
        getResultIds: function()
        {
            var localThis = this;
            
            var result = null;
            
            if(localThis.data('multiselect') === true)
            {
                result = getMultiselectResultIds(localThis);
            }
            else
            {
                result = getSingleselectResultIds(localThis);
            }
            
            return result;
        }
    };
    
    function getMultiselectResultIds(localThis)
    {        
        var ids = [];
        
        localThis.find('.right_side').find('.sima-search-model-choose-list').find('.item').each(function(){
            var id = $(this).data('id');
            ids.push(id);
        });
        
        var result = ids;
        
        return result;
    }
    
    function getSingleselectResultIds(localThis)
    {        
        var result = localThis.find('.sima-ui-sf-input').val();
        
        return result;
    }
    
    $.fn.simaModelChoose = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.fileBrowser2');
            }
        });
        return ret;
    };
})( jQuery );