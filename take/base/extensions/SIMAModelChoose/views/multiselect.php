<?php
$uniq = SIMAHtml::uniqid();
$multichoose_id = $id;
$ids_list = 'ids_list_'.$uniq;
?>

<div id="<?php echo $multichoose_id; ?>" class='sima-layout-panel _splitter _vertical sima-search-model-choose'>
    <div class='sima-layout-panel left_side'>
        <?php echo $html; ?>
    </div>
    <div class='sima-layout-panel right_side'>
        <div id="<?php echo $ids_list; ?>" class='sima-search-model-choose-list'>
            <?php
            foreach($initial_values as $initial_value)
            {
                ?>
                <span class="item <?php echo $initial_value['classes'] ?>" data-id="<?php echo $initial_value['id'] ?>">
                    <span class="sima-icon _delete _16" title="Obriši" onclick="$(this).parent().remove();"></span>
                    <span class="name" title="<?php echo $initial_value['display_name'] ?>"><?php echo $initial_value['display_name'] ?></span>
                </span>
                <?php
            }
            ?>
        </div>
    </div>
</div>

<script>
$('#<?php echo $multichoose_id; ?>').on('add', function(event,data){
    var ids_list = $('#<?php echo $ids_list; ?>');
    $.each(data, function(index, value) {
        if (ids_list.find(".item[data-id='" + value.id + "']").length === 0)
        {
            var span = $('<span class="item" data-id="'+value.id+'"></span>');
            var span_delete = $('<span class="sima-icon _delete _16" title="Obriši" onclick="$(this).parent().remove();"></span>');
            var span_name = $('<span class="name" title="'+$.trim(value.display_name)+'">'+$.trim(value.display_name)+'</span>');
            span.append(span_delete).append(span_name);
            ids_list.append(span);
        }
    });
});
</script>
