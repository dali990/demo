
<?php 
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>
    <?php        
        $params = [];
        $params['model'] = get_class($model);
        $params['multiselect'] = false;
        $params['view'] = 'guiTable';
    ?>
<p style='max-width: 455px; margin: 10px 30px 20px 30px;'><?php echo $title?></p>
    <div>
        <span  style='display: inline-block; width: 20px;'></span>
        <span style="display: inline-block;">
            <?php
                $searchfield_params = [
                    'id'=>$id,
                    'model'=>$model, 
                    'filters'=>$simple_filter,
                ];
                
                if(!empty($initial_values))
                {
                    $searchfield_params['real_value'] = $initial_values['real_value'];
                    $searchfield_params['display_value'] = $initial_values['display_value'];
                }
            
                Yii::app()->controller->widget('ext.SIMASearchField.SIMASearchField', $searchfield_params);
            ?>
        </span>
        <span style='display: inline-block;' class='sima-icon _search_form _16' onclick='sima.misc.searchFieldAddButton("<?php echo $id; ?>",<?php echo CJSON::encode($params); ?>);'></span>
    </div>    
<?php 
if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
