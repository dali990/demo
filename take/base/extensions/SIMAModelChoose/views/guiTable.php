
<?php 
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
    $uniq = SIMAHtml::uniqid();
    $guitable_params = [
        'id'=>  $id,
        'model'=>$model,
        'add_button'=>isset($params['add_button'])?$params['add_button']:true,        
        'setRowDblClick' => "on_dbclick$uniq",            
    ];
    if (isset($fixed_filter))
    {
        $guitable_params['fixed_filter'] = $fixed_filter;
    }
    else if (isset($filter))
    {
        $guitable_params['fixed_filter'] = $filter;
    }
    
    if (isset($select_filter))
    {
        $guitable_params['select_filter'] = $select_filter;
    }
?>
    <?php
        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', $guitable_params);            
    ?>
<?php 
if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>

<script type="text/javascript">
    function on_dbclick<?php echo $uniq; ?>(obj)
    {
        var params = [];
        params.push([{'id':obj.attr('model_id'), 'display_name':sima.model.getDisplayName(obj.attr('model'), obj.attr('model_id'))}]);
        $('#<?php echo $id; ?>').parents('.sima-search-model-choose:first').trigger('add', params);
    }
</script>