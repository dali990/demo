<?php

class SIMAModelChoose extends CWidget 
{
    public $id = null;
    public $modelName = null;
    public $multiselect = false;
    public $view = null;
    public $params = [];
    public $title = null;
    public $fullscreen = true;
    public $model_ids = [];
    public $filter = [];
    public $fixed_filter = [];
    public $select_filter = [];
    
    public function run() 
    {
        $uniq = SIMAHtml::uniqid();
        
        if(empty($this->id))
        {
            $this->id = $uniq.'_model_choose';
        }
        
        if(empty($this->modelName))
        {
            throw new Exception(Yii::t('SIMAModelChoose.Translation', 'ModelNameEmpty'));
        }
        
        $modelName = $this->modelName;
        $model = $modelName::model();
        $module_name = $model->moduleName();
        
        if(empty($this->title))
        {
            $this->title = Yii::t('SIMAModelChoose.Translation', 'Choose').' '.$model->modelLabel();
        }
        
        $render_view = null;
        $modelSearchViews = $model->modelSearchViews();
        if (empty($this->view))
        {
            if (in_array('default', $modelSearchViews))
            {
                $render_view = $module_name.'.views.modelSearch.'.$this->modelName.'.default';
            }
            else if ($this->multiselect === true)
            {
                $render_view = 'guiTable';
            }
            else
            {
                $render_view = 'simple';
            }
        }
        else
        {
            if ($this->view === 'guiTable')
            {
                $render_view = 'guiTable';
            }
            else if ($this->view === 'simple')
            {
                $render_view = 'simple';
            }
            else if (in_array($this->view, $modelSearchViews))
            {
                $render_view = $module_name.'.views.modelSearch.'.$this->modelName.'.'. $this->view;
            }
            else
            {
//                throw new Exception('actionSearchModel - nije zadat search view <b>'.$this->view.'</b> za model <b>'.$this->modelName.'</b>.');
                throw new Exception(Yii::t('SIMAModelChoose.Translation', 'NoViewForModel', [
                    '{view}' => $this->view,
                    '{modelName}' => $this->modelName
                ]));
            }
        }
        
        $is_model_obj = false;
        if($render_view === 'simple' && !empty($this->model_ids) && count($this->model_ids) === 1 && !empty($this->model_ids[0]))
        {
            $modelObj = $modelName::model()->findByPk($this->model_ids[0]);
            if(!empty($modelObj))
            {
                $model = $modelObj;
                $is_model_obj = true;
            }
        }
        
        $view_params = [
            'model'=>$model,            
            'params'=>$this->params,
            'title' => $this->title,
            'filter' => $this->filter,
            'simple_filter' => $this->filter,
            'fixed_filter' => $this->fixed_filter,
            'select_filter' => $this->select_filter
        ];
        if(!empty($this->fixed_filter))
        {
            $view_params['simple_filter'] = $this->fixed_filter;
        }
        if(!empty($this->select_filter))
        {
            $view_params['simple_filter'] = $this->select_filter;
        }
        
        $initial_values = [];
        
        if($this->multiselect === true)
        {
            $view_params['id'] = $uniq.'_search_field_id';
            
            foreach($this->model_ids as $model_id)
            {
                $initial_model = $modelName::model()->findByPk($model_id);
                
                $initial_values[] = [
                    'classes' => '',
                    'id' => $initial_model->id,
                    'display_name' => $initial_model->DisplayName
                ];
            }
        }
        else
        {
            $view_params['id'] = $this->id;
            
            if($is_model_obj === true)
            {
                $real_value = $model->id;
                $display_value = $model->DisplayName;
            
                $initial_values['real_value'] = $real_value;
                $initial_values['display_value'] = $display_value;

                $view_params['initial_values'] = $initial_values;
            }
        }
        
        $choose_html = $this->localRender($render_view, $view_params);
        
        $result_html = '';
        if($this->multiselect === true)
        {
            $result_html = $this->localRender('multiselect', [
                'id' => $this->id,
                'html' => $choose_html,
                'initial_values' => $initial_values
            ]);
        }
        else
        {
            $result_html = $choose_html;
        }
        
        echo $result_html;
        
        $js_params = [
            'multiselect' => $this->multiselect
        ];
        $json = CJSON::encode($js_params);
        $register_script = "$('#" . $this->id . "').simaModelChoose('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
    
    private function localRender($view, $params)
    {
        $view = 'base.extensions.SIMAModelChoose.views.'.$view;
                
        return parent::render($view, $params, true);
    }
    
    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.simaModelChoose.js', CClientScript::POS_HEAD);
    }
}

