<?php

class SIMABasicInfoToExpandableNotImplementedType extends SIMAException
{
    public function __construct($param_key, $param_value)
    {
        $message = 'key: '.CJSON::encode($param_key).' - value: '.CJSON::encode($param_value);
        
        parent::__construct($message);
    }
}
