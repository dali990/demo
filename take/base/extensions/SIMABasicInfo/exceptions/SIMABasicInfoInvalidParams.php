<?php

class SIMABasicInfoInvalidParams extends SIMAException
{
    public function __construct($message, $code = null, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

