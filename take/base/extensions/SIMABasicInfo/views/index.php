<div id='<?=$this->id?>' class='basic_info_widget'>
<?php
foreach($params as $param)
{
    ?>
    <div class='row'>
    <?php
        if(
                $param['type'] === SIMABasicInfo::$TYPE_ATTRIBUTE
                ||
                $param['type'] === SIMABasicInfo::$TYPE_LABEL_VALUE
        )
        {
            $label = null;
            $value = null;
            
            if($param['type'] === SIMABasicInfo::$TYPE_ATTRIBUTE)
            {
                $label = $model->getAttributeLabel($param['attribute']);
                $value = $model->getAttributeDisplay($param['attribute']);
            }
            else if($param['type'] === SIMABasicInfo::$TYPE_LABEL_VALUE)
            {
                $label = $param['label'];
                $value = $param['value'];
            }
            
            
        ?>
            <span class='title'><?=$label?>:</span>
            <span class='data'>
                <?php
                    echo $value;
                    if(isset($param['attribute_post_html']))
                    {
                        echo $param['attribute_post_html'];
                    }
                ?>
            </span>
        <?php
        }
        else if($param['type'] === SIMABasicInfo::$TYPE_HTML)
        {
            echo $param['html'];
        }
        else if($param['type'] === SIMABasicInfo::$TYPE_EXPANDABLE_INFORMATIONS_FOR_MODEL)
        {
            $this->widget(SIMAExpandableInformations::class, [
                'basic_informations' => $param['basic_informations'],
                'expanded_informations' => $param['expanded_informations'],
                'expanded_informations_absolute' => $param['expanded_informations_absolute']
            ]);
        }
        else
        {
            throw new Exception('not implemented');
        }
    ?>
    </div>
    <?php
}
?>
</div>