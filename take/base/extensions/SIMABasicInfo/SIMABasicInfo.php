<?php

class SIMABasicInfo extends CWidget 
{
    public static $TYPE_HTML = 'html';
    public static $TYPE_ATTRIBUTE = 'attr';
    public static $TYPE_LABEL_VALUE = 'label_value';
    public static $TYPE_EXPANDABLE_INFORMATIONS_FOR_MODEL = 'expandable_informations_for_model';
    public static $TYPE_ATTRIBUTE_COUNT = 'attribute_count';
    
    public $id = null;
    public $model = null;
    public $params = [];
    
    private $_parsed_params = [];
    
    public function run()
    {
        if (empty($this->id))
        {
            $this->id = SIMAHtml::uniqid().'sbi';
        }
        
        if(empty($this->model))
        {
            throw new SIMABasicInfoInvalidParams('no model');
        }
        
        $display_model = $this->model->getDisplayModel();
        
        $this->parseBasicInfoParams();
        
        $this->validateParams();
        
        $view_params = [
            'params' => $this->_parsed_params,
            'model' => $display_model
        ];
        
        echo $this->render('index', $view_params, true);
                
//        $params = [
//            'dropdown_box'=>$dropdown_box
//        ];
//        $json = CJSON::encode($params);
//        $register_script = "$('#" . $this->id . "').simaButton('init',$json);";
//        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
    
//    public function registerManual() {
//        $assets = dirname(__FILE__) . '/assets';
//        $baseUrl = Yii::app()->assetManager->publish($assets);
//        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/simaButtonEvents.js', CClientScript::POS_HEAD);
//        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),"sima.buttonEvents = new SIMAButtonEvents();", CClientScript::POS_HEAD);
//        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.simaButton.js', CClientScript::POS_HEAD);
//        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaButton.css');
//    }
    
    public static function recalculateContent($params)
    {
        $id = $params['id'];
        $model_name = $params['model_name'];
        $model_id = $params['model_id'];
        $model = $model_name::model()->findByPkWithCheck($model_id);
        return Yii::app()->controller->widget(SIMABasicInfo::class, [
            'id' => $id,
            'model' => $model
        ], true);
    }
    
    private function validateParams()
    {
        if(!is_array($this->_parsed_params))
        {
            throw new SIMABasicInfoInvalidParams('params not array');
        }
        
        foreach($this->_parsed_params as $param)
        {
            if(is_int($param)) /// prioritet
            {
                continue;
            }
            
            if(!is_array($param))
            {
                throw new SIMABasicInfoInvalidParams('param not array');
            }
            
            if(!isset($param['type']))
            {
                throw new SIMABasicInfoInvalidParams('param no type');
            }
            
            if(
                    $param['type'] !== SIMABasicInfo::$TYPE_HTML
                    &&
                    $param['type'] !== SIMABasicInfo::$TYPE_ATTRIBUTE
                    &&
                    $param['type'] !== SIMABasicInfo::$TYPE_LABEL_VALUE
                    &&
                    $param['type'] !== SIMABasicInfo::$TYPE_EXPANDABLE_INFORMATIONS_FOR_MODEL
            )
            {
                throw new SIMABasicInfoInvalidParams('invalid type');
            }
            
            if($param['type'] === SIMABasicInfo::$TYPE_HTML)
            {
                if(!isset($param['html']))
                {
                    throw new SIMABasicInfoInvalidParams('invalid html type');
                }
            }
            else if($param['type'] === SIMABasicInfo::$TYPE_ATTRIBUTE)
            {
                if(!isset($param['attribute']))
                {
                    throw new SIMABasicInfoInvalidParams('invalid attribute type - no attr');
                }
                
                if(!is_string($param['attribute']))
                {
                    throw new SIMABasicInfoInvalidParams('invalid attribute type - attr not str');
                }
                
//                if(!isset($this->model->{$param['attribute']}))
//                {
//                    throw new SIMABasicInfoInvalidParams('invalid attribute type - model do not have attr');
//                }
            }
            else if($param['type'] === SIMABasicInfo::$TYPE_LABEL_VALUE)
            {
                if(!isset($param['label']))
                {
                    throw new SIMABasicInfoInvalidParams('invalid labelvalue type - no label');
                }
                
                if(!isset($param['value']))
                {
                    throw new SIMABasicInfoInvalidParams('invalid labelvalue type - no value');
                }
            }
            else if($param['type'] === SIMABasicInfo::$TYPE_EXPANDABLE_INFORMATIONS_FOR_MODEL)
            {
                if(!isset($param['basic_informations']))
                {
                    throw new SIMABasicInfoInvalidParams('invalid expandable informations for model type - no basic_informations');
                }
                if(!is_array($param['basic_informations']))
                {
                    throw new SIMABasicInfoInvalidParams('invalid expandable informations for model type - basic_informations not array');
                }
                
                if(!isset($param['expanded_informations']))
                {
                    throw new SIMABasicInfoInvalidParams('invalid expandable informations for model type - no expanded_informations');
                }
                if(!is_array($param['expanded_informations']))
                {
                    throw new SIMABasicInfoInvalidParams('invalid expandable informations for model type - expanded_informations not array');
                }
            }
        }
    }
    
    private function parseBasicInfoParams()
    {
        $results_by_priorities = [];
        
        foreach ($this->params as $result)
        {
            $priority = 100;
            foreach ($result as $value)
            {
                if(is_int($value))
                {
                    $priority = $value;
                    continue;
                }
                
                if($value['type'] === SIMABasicInfo::$TYPE_EXPANDABLE_INFORMATIONS_FOR_MODEL)
                {
                    $basic_informations = [];
                    $expanded_informations = [];
                    $expanded_informations_absolute = false;

                    $model = $value['model'];
                    if(isset($value['basic_informations']))
                    {
                        $basic_informations = $this->parseBasicInfoToExpandableParams($model, $value['basic_informations']);
                    }
                    if(isset($value['expanded_informations']))
                    {
                        $expanded_informations = $this->parseBasicInfoToExpandableParams($model, $value['expanded_informations']);
                    }

                    if(isset($value['expanded_informations_absolute']))
                    {
                        $expanded_informations_absolute = $value['expanded_informations_absolute'];
                    }
                    
                    $results_by_priorities[$priority][] = [
                        'type' => SIMABasicInfo::$TYPE_EXPANDABLE_INFORMATIONS_FOR_MODEL,
                        'basic_informations' => $basic_informations,
                        'expanded_informations' => $expanded_informations,
                        'expanded_informations_absolute' => $expanded_informations_absolute
                    ];
                }
                else
                {
                    $results_by_priorities[$priority][] = $value;
                }
            }
        }
        
        ksort($results_by_priorities);
        foreach($results_by_priorities as $priority => $results_by_priority)
        {
            $this->_parsed_params = array_merge($this->_parsed_params, $results_by_priority);
        }
    }
    
    private function parseBasicInfoToExpandableParams($model, $params)
    {
        $result = [];
        
        foreach($params as $basic_informations_key => $basic_informations_value)
        {
            $value_to_check = $basic_informations_value;
            if(!is_array($basic_informations_value))
            {
                $value_to_check = [
                    'type' => SIMABasicInfo::$TYPE_ATTRIBUTE,
                    'attribute' => $basic_informations_value,
                ];
            }
            
            if($value_to_check['type'] === SIMABasicInfo::$TYPE_ATTRIBUTE)
            {
                $result[] = [
                    'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
                    'label' => $model->getAttributeLabel($value_to_check['attribute']),
                    'value' => $model->getAttributeDisplay($value_to_check['attribute']),
                ];
            }
            else if($value_to_check['type'] === SIMABasicInfo::$TYPE_ATTRIBUTE_COUNT)
            {
                $result[] = [
                    'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
                    'label' => $model->getAttributeLabel($value_to_check['attribute']),
                    'value' => (string)count($model->{$value_to_check['attribute']})
                ];
            }
            else if($value_to_check['type'] === SIMABasicInfo::$TYPE_HTML)
            {
                $result[] = [
                    'type' => SIMAExpandableInformations::$TYPE_HTML,
                    'html' => $value_to_check['html']
                ];
            }
            else if($value_to_check['type'] === SIMABasicInfo::$TYPE_LABEL_VALUE)
            {
                $result[] = [
                    'type' => SIMAExpandableInformations::$TYPE_LABEL_VALUE,
                    'label' => $value_to_check['label'],
                    'value' => $value_to_check['value']
                ];
            }
            else
            {
                throw new SIMABasicInfoToExpandableNotImplementedType($basic_informations_key, $basic_informations_value);
            }
        }
        
        return $result;
    }
}

