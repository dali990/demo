<?php

class NotificationGUI extends SIMAActiveRecordGUI {
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'seen':
                return CHtml::checkBox ('seen', $owner->seen, [
                    "onclick" => "sima.notifications.onGUITableCheckBoxToggle($owner->id, $owner->seen)"
                ]);
            case 'additional_info':
                return $owner->getInfoText();
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelLabel($plural = false)
    {
        return $plural?Yii::t('BaseModule.Notification', 'Notfications'):Yii::t('BaseModule.Notification', 'Notfication');
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'seen'=>'checkBox',
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return array(
                'columns' => array(
                    'title', 'create_time', 'seen', 'additional_info'
                )                
            );
        }
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'model_options_data': 
                return array_merge(parent::vuePropValue($prop), [
                    'open_new_tab_notification' => SIMAHtmlButtons::OpenNewTabNotificationWidgetAction($owner),
                ]);
            case 'info_text':
                return $owner->getInfoText();
            default: return parent::vuePropValue($prop);
        }
    }
}