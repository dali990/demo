<?php

class UserConfigCounterItemGUI extends SIMAActiveRecordGUI
{
    
    public function columnLabels()
    {
        return array(
            'month' => Yii::t('BaseModule.Common', 'Month'),
            'year' => Yii::t('BaseModule.Common', 'Year'),
            'last_taken_value' => Yii::t('BaseModule.UserConfigCounter', 'LastTakenValue'),
//            'note' => Yii::t('BaseModule.Common', 'Note'),
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.UserConfigCounter',$plural?'UserConfigCounterItems':'UserConfigCounterItem');
    }    
    

//    
//    public function getDisplayAction()
//    {
//        return $this->owner->path2;
//    }
    
    public function getDisplayModel()
    {
        return $this->owner;
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
//            case 'time_interval': return UserConfigCounter::$interval_types[$owner->time_interval];
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        if (empty($owner->month_id) && empty($owner->year_id))
        {
            $_year = ['relation', 'relName' => 'year'];
            $_month = ['relation', 'relName' => 'month'];
        }
        else
        {
            $_year = 'display';
            $_month = 'display';
        }

        if (isset($owner->user_config_counter))
        {
            if ($owner->user_config_counter->time_interval === UserConfigCounter::$INTERVAL_YEAR)
            {
                $_month = 'hidden';
            }
            else
            {
                $_year = 'hidden';
            }
        }
        
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'user_config_counter_id'=>'hidden',
                    'month'=>$_month,
                    'year'=>$_year,
                    'last_taken_value'=>'numberField',
//                    'note'=>'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => [
                        'month', 
                        'year',
                        'last_taken_value'
                    ],
                    'order_by' => [
                        'month', 
                        'year',
                    ]
                ];
        }
    }
}