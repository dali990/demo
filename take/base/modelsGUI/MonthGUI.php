<?php

class MonthGUI extends SIMAActiveRecordGUI
{
    /**
     * niz koji predstavlja imena kolona array(key=>value)
     * @return array 
     */
    public function columnLabels()
    {
        return array(
            'month_range'=> Yii::t('BaseModule.Common', 'Month'),
        )+parent::columnLabels();
    }
    
    public function toString()
    {
        $owner = $this->owner;
        
        $month_disp = '';
        switch($owner->month)
        {
            case 1:
                $month_disp = Yii::t('BaseModule.Month', 'January');
                break;
            case 2:
                $month_disp = Yii::t('BaseModule.Month', 'February');
                break;
            case 3:
                $month_disp = Yii::t('BaseModule.Month', 'March');
                break;
            case 4:
                $month_disp = Yii::t('BaseModule.Month', 'April');
                break;
            case 5:
                $month_disp = Yii::t('BaseModule.Month', 'May');
                break;
            case 6:
                $month_disp = Yii::t('BaseModule.Month', 'June');
                break;
            case 7:
                $month_disp = Yii::t('BaseModule.Month', 'July');
                break;
            case 8:
                $month_disp = Yii::t('BaseModule.Month', 'August');
                break;
            case 9:
                $month_disp = Yii::t('BaseModule.Month', 'September');
                break;
            case 10:
                $month_disp = Yii::t('BaseModule.Month', 'October');
                break;
            case 11:
                $month_disp = Yii::t('BaseModule.Month', 'November');
                break;
            case 12:
                $month_disp = Yii::t('BaseModule.Month', 'December');
                break;
            default:
               $month_disp = Yii::t('BaseModule.Common', 'Unknown');
        }
        
        return $month_disp;
    }
}