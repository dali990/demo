<?php

class ConfigurationParameterGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'user_changable' => Yii::t('BaseModule.Configuration', 'UserChangable'),
            'display_name' => Yii::t('BaseModule.Configuration', 'DisplayName'),
            'default_value' => Yii::t('BaseModule.Configuration', 'Default'),
            'value' => Yii::t('BaseModule.Configuration', 'Value'),
        )+parent::columnLabels();
    }
    
    public function columnDisplays($column)
    {
//        $result = '';
        
        $owner = $this->owner;
        
        switch ($column)
        {
            case 'value':
                $result = $this->generateGUIValueDisplay('value');
                                    
                return $result;
            case 'default_value':
//                $result = '';
                $result = $this->generateGUIValueDisplay('default');
                
                return $result;
            default: 
                $result = parent::columnDisplays($column); 
                break;
        }
        
        return $result;
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return array(
                    'columns' => array(
                        'display_name',
                        'value',
                        'description',
                        'default_value'
                    )
                );
        }
    }
    
    private function generateGUIValueDisplay($field)
    {
        $result = '';
                
        $owner = $this->owner;
                
        $values = [];
        
        if($owner->value_is_array === true && is_array($owner->$field))
        {
            foreach($owner->$field as $val)
            {
                if(isset($val))
                {
                    $values[] = array(
                        'type' => $owner->type,
                        'type_params' => $owner->type_params,
                        'value' => $val
                    );
                }
            }
        }
        else
        {
            $val = $owner->$field;
            if(!is_null($val))
            {
                $values[] = array(
                    'type' => $owner->type,
                    'type_params' => $owner->type_params,
                    'value' => $val
                );
            }
        }
                
        foreach($values as $val)
        {
            $result .= $this->generateSingleValueDisplay($val['type'], $val['type_params'], $val['value']);
        }
        
        return $result;
    }
    
    private function generateSingleValueDisplay($type, $type_params, $value)
    {
        $result = '';
                
        if($type === 'searchField')
        {
            $modelName = $type_params['modelName'];
            $model = $modelName::model()->findByPk($value);
            if (is_null($model))
            {
                $result .= 'IZBRISANO';
            }
            else
            {
                $result .= '<span data-value="'.$value.'">'.$model->DisplayHtml.'</span>';
            }
        }
        else if($type === 'dropdown')
        {
            foreach($type_params as $typeparam)
            {
                if($typeparam['value'] == $value)
                {
                    $result .= $typeparam['title'];
                    break;
                }
            }
        }
        else if($type === SIMAConfigManager::$TYPE_BOOLEAN)
        {
            $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
            if($value === true)
            {
                $result = Yii::t('BaseModule.Common', 'Yes');
            }
            else
            {
                $result = Yii::t('BaseModule.Common', 'No');
            }
        }
        else
        {
            $result .= $value;
        }
        
        if(!empty($result))
        {
            $result .= '</br>';
        }
        
        return $result;
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'model_options_data': 
                return array_merge(parent::vuePropValue($prop), [
                    'configuration_parameter_edit' => SIMAHtmlButtons::ConfigurationParameterEditWidgetAction($owner, false),
                    'configuration_parameter_edit_user' => SIMAHtmlButtons::ConfigurationParameterEditWidgetAction($owner, true),
                    'configuration_parameter_revert_to_default' => SIMAHtmlButtons::ConfigurationParameterRevertToDefaultWidgetAction($owner, false),
                    'configuration_parameter_revert_to_default_user' => SIMAHtmlButtons::ConfigurationParameterRevertToDefaultWidgetAction($owner, true),
                    'configuration_parameter_user_changable' => SIMAHtmlButtons::ConfigurationParameterUserChangableWidgetAction($owner),
                ]);
                
            default: return parent::vuePropValue($prop);
        }
    }
}