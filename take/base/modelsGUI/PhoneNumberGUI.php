<?php

class PhoneNumberGUI extends SIMAActiveRecordGUI
{
    public function guiTableSettings($type)
    {
        switch ($type)
        {   
            default:
                return [
                    'columns' => [
                        'country', 'provider_number', 'phone_number', 'comment'
                    ]
                ];
        }
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.PhoneNumber', $plural ? 'PhoneNumbers' : 'PhoneNumber');
    }
    
    public function columnLabels()
    {
        return [
            'country_id'        => Yii::t('BaseModule.PhoneNumber', 'Country'),
            'country'           => Yii::t('BaseModule.PhoneNumber', 'Country'),
            'provider_number'   => Yii::t('BaseModule.PhoneNumber', 'ProviderNumber'),
            'phone_number'      => Yii::t('BaseModule.PhoneNumber', 'Number'),
            'full_phone_number' => Yii::t('BaseModule.PhoneNumber', 'PhoneNumber'),
            'type_id'           => Yii::t('BaseModule.PhoneNumber', 'PhoneNumberType'),
            'partner_id'        => Yii::t('BaseModule.PhoneNumber', 'Partner'),
            'partner'           => Yii::t('BaseModule.PhoneNumber', 'Partner'),
            'comment'           => Yii::t('BaseModule.Common', 'Comment')
        ]+parent::columnLabels();
    }
    
    public function modelForms()
    {
        return [
            'default'=> [
                'type'=>'generic',
                'columns'=> [
                    'type_id' => ['dropdown'],
                    'full_phone_number' => 'phoneField',
                    'partner_id' => 'hidden',
                    'comment' => 'textArea'
                ]
            ],
            'from_partner'=> [
                'type'=>'generic',
                'columns'=> [                   
                    'type_id' => 'dropdown',
                    'full_phone_number' => 'phoneField',
                    'partner_id' => 'hidden',
                    'comment' => 'textArea'
                ]
            ]
        ];
    }
}