<?php

class ModelChangeGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'change_type' => Yii::t('BaseModule.ModelChange', 'ChangeType'),
            'change_time' => Yii::t('BaseModule.ModelChange', 'ChangeTime'),
        ]+parent::columnLabels();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'actual_model':
                $result = Yii::t('BaseModule.Common', 'Unknown');
                if(isset($owner->actual_model))
                {
                    $result = $owner->actual_model->DisplayHTML;
                }
                return $result;
            case 'change_type':
                $result = Yii::t('BaseModule.Common', 'Unknown');
                $change_type_data = ModelChangeGUI::getChangeTypeData();
                if(isset($change_type_data[$owner->change_type]))
                {
                    $result = $change_type_data[$owner->change_type];
                }
                return $result;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => [
                        'model_name',
                        'model_id',
                        'actual_model',
                        'change_type',
                        'change_columns',
                        'change_time',
                        'user'
                    ],
                    'order_by'=>['change_time']
                ];
        }
    }
    
    public function droplist($key, $relName='', $filter=null) {
        switch ($key) {
            case 'change_type':
                return ModelChangeGUI::getChangeTypeData();
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public static function getChangeTypeData()
    {
        return [
            ModelChange::$CHANGE_TYPE_INSERT => Yii::t('BaseModule.ModelChange', 'ChangeInsert'),
            ModelChange::$CHANGE_TYPE_UPDATE => Yii::t('BaseModule.ModelChange', 'ChangeUpdate'),
            ModelChange::$CHANGE_TYPE_DELETE => Yii::t('BaseModule.ModelChange', 'ChangeDelete')
        ];
    }
}
