<?php

class MainMenuFavoriteItemGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels()
    {
        return [
            'label' => Yii::t('BaseModule.MainMenuFavoriteItem', 'Label'),
            'action' => Yii::t('BaseModule.MainMenuFavoriteItem', 'Action'),
            'component_name' => Yii::t('BaseModule.MainMenuFavoriteItem', 'ComponentName'),
            'user_id' => Yii::t('BaseModule.MainMenuFavoriteItem', 'User'),
            'user' => Yii::t('BaseModule.MainMenuFavoriteItem', 'User')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.MainMenuFavoriteItem', $plural ? 'MainMenuFavoriteItems' : 'MainMenuFavoriteItem');
    }
}