<?php

class UserToPartnerListGUI extends SIMAActiveRecordGUI
{   
    public function columnLabels()
    {
        return [
            'user' => Yii::t('BaseModule.UserToPartnerList', 'User'),
            'user_id' => Yii::t('BaseModule.UserToPartnerList', 'User'),
            'partner_list' => Yii::t('BaseModule.UserToPartnerList', 'PartnerList'),
            'partner_list_id' => Yii::t('BaseModule.UserToPartnerList', 'PartnerList')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.UserToPartnerList', $plural ? 'UsersToPartnerLists': 'UserToPartnerList');
    }
    
    public function modelForms()
    {
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'partner_list_id' => ['relation', 'relName' => 'partner_list', 'add_button' => true],
                    'user_id' => ['relation', 'relName' => 'user']
                ]
            ]
        ];
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'from_partner_list': return [
                'columns' => ['user']
            ];
            case 'from_user': return [
                'columns' => ['partner_list']
            ];
            default:
                return [
                    'columns' => ['partner_list', 'user']
                ];
        }
    }
}
