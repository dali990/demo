<?php

class PartnerToPartnerListGUI extends SIMAActiveRecordGUI
{   
    public function columnLabels()
    {
        return [
            'partner' => Yii::t('BaseModule.PartnerToPartnerList', 'Partner'),
            'partner_id' => Yii::t('BaseModule.PartnerToPartnerList', 'Partner'),
            'partner_list' => Yii::t('BaseModule.PartnerToPartnerList', 'PartnerList'),
            'partner_list_id' => Yii::t('BaseModule.PartnerToPartnerList', 'PartnerList')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.PartnerToPartnerList', $plural ? 'PartnersToPartnerLists': 'PartnerToPartnerList');
    }
    
    public function modelForms()
    {
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'partner_list_id' => ['relation', 'relName' => 'partner_list', 'add_button' => true],
                    'partner_id' => ['relation', 'relName' => 'partner', 'add_button'=>['action'=>'simaAddressbook/index']]
                ]
            ]
        ];
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'from_partner_list': return [
                'columns' => ['partner']
            ];
            case 'from_partner': return [
                'columns' => ['partner_list']
            ];
            default:
                return [
                    'columns' => ['partner_list', 'partner']
                ];
        }
    }
}
