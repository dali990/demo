<?php

class PartnerListGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'code' => Yii::t('BaseModule.PartnerList', 'Code'),
            'name' => Yii::t('BaseModule.PartnerList', 'Name'),
            'owner' => Yii::t('BaseModule.PartnerList', 'Owner'),
            'owner_id' => Yii::t('BaseModule.PartnerList', 'Owner'),
            'type' => Yii::t('BaseModule.PartnerList', 'Type'),
            'description' => Yii::t('BaseModule.PartnerList', 'Description'),
            'is_system_filter' => Yii::t('BaseModule.PartnerList', 'IsSystem'),
            'calc_system_list' => Yii::t('BaseModule.PartnerList', 'CalcSystemLists'),
            'users_list' => Yii::t('BaseModule.PartnerList', 'Users'),
            'created_time' => Yii::t('BaseModule.PartnerList', 'CreatedTime')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.PartnerList', $plural ? 'PartnerLists': 'PartnerList');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;

        switch ($column)
        {
            case 'type':
                return PartnerList::$types[$owner->$column];
            case 'is_system_filter':
                return $owner->is_system ? Yii::t('BaseModule.Common', 'Yes') : Yii::t('BaseModule.Common', 'No');
            case 'calc_system_list': 
                $return = '';
                if ($owner->is_system)
                {
                    if(SIMAMisc::isVueComponentEnabled())
                    {
                        $return = Yii::app()->controller->widget(SIMAButtonVue::class, [
                            'title' => Yii::t('BaseModule.PartnerList', 'Recalculate'),
                            'onclick' => ['sima.misc.calcSystemPartnerList', $owner->code],
                            'security_question' => Yii::t('BaseModule.PartnerList', 'RecalculateYesNo')
                        ], true);
                    }
                    else
                    {
                        $return = Yii::app()->controller->widget('SIMAButton', [
                            'action' => [ 
                                'title' => Yii::t('BaseModule.PartnerList', 'Recalculate'),
                                'onclick' => ['sima.misc.calcSystemPartnerList', $owner->code],
                                'security_questions' => Yii::t('BaseModule.PartnerList', 'RecalculateYesNo')
                            ]
                        ], true);
                    }
                }
                
                return $return;
            case 'partner_list_to_users.user': 
                $return = '';
                
                if (!$owner->is_system && $owner->type === PartnerList::$PRIVATE)
                {
                    foreach ($owner->users as $user)
                    {
                        $return .= "<div style='padding-bottom: 5px;'>{$user->DisplayHtml}</div>";
                    }

                    if ($owner->hasUserAccessToList())
                    {
                        if(SIMAMisc::isVueComponentEnabled())
                        {
                            $return .= Yii::app()->controller->widget(SIMAButtonVue::class, [
                                'title' => Yii::t('BaseModule.Common', 'Add'),
                                'onclick' => ['sima.misc.addUsersToPartnerList', $owner->id]
                            ], true);
                        }
                        else
                        {
                            $return .= Yii::app()->controller->widget('SIMAButton', [
                                'action' => [ 
                                    'title' => Yii::t('BaseModule.Common', 'Add'),
                                    'onclick' => ['sima.misc.addUsersToPartnerList', $owner->id]
                                ]
                            ], true);
                        }
                    }

                    if (!empty($return))
                    {
                        $return = "<div style='margin: 5px 0px;'>$return</div>";
                    }
                }
                
                return $return;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        
        $name = ['textField'];
        $type = ['dropdown'];
        $description = ['textArea'];
        
        if (!$owner->isNewRecord && $owner->is_system)
        {
            $name['disabled'] = true;
            $description['disabled'] = true;
        }
        
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'name' => $name,
                    'type' => $type,
                    'users_list' => ['list',
                        'model' => 'User',
                        'relName' => 'users',
                        'multiselect' => true,
                        'params' => [
                            'add_button' => false
                        ],
                        'filter' => [
                            'filter_scopes' => 'onlyActive'
                        ],
                        'dependent_on' => [
                            'type' => [
                                'onValue'=>[
                                    ['hide', 'for_values' => PartnerList::$PUBLIC],
                                    ['show', 'for_values' => PartnerList::$PRIVATE]
                                ]
                            ]
                        ]
                    ],
                    'description' => $description
                ]
            ]
        ];
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'model_choose':
                return [
                    'columns' => [
                        'name', 'owner'
                    ]
                ];
            default:
                return [
                    'columns' => ['name', 'owner', 'type', 'created_time', 'is_system_filter', 'code', 'description', 'partner_list_to_users.user', 'calc_system_list']
                ];
        }
    }
    
    public function droplist($key, $relName = '', $filter = null) 
    {
        switch ($key)
        {
            case 'type' : return PartnerList::$types;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}