<?php

class MeasurementUnitGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name' => 'Naziv',
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.MeasurementUnit', $plural ? 'MeasurementUnits' : 'MeasurementUnit');
    }    
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return array(
                'columns' => array(
                    'name'
                )                
            );
        }
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(                    
                    'name' => 'textField'
                )
            ),
        );
    }
    
}