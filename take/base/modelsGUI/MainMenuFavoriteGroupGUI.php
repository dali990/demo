<?php

class MainMenuFavoriteGroupGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels()
    {
        return [
            'group_name' => Yii::t('BaseModule.MainMenuFavoriteGroup', 'GroupName'),
            'user_id' => Yii::t('BaseModule.MainMenuFavoriteGroup', 'User'),
            'user' => Yii::t('BaseModule.MainMenuFavoriteGroup', 'User'),
            'is_default' => Yii::t('BaseModule.MainMenuFavoriteGroup', 'IsDefault')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.MainMenuFavoriteGroup', $plural ? 'MainMenuFavoriteGroups' : 'MainMenuFavoriteGroup');
    }
    
    public function modelForms()
    {
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'group_name' => 'textField',
                    'user_id' => 'hidden'
                ]
            ]
        ];
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'has_curr_user_item':
                $attributes = [
                    'user_id' => Yii::app()->user->id
                ];

                if (!empty($scopes['component_name']))
                {
                    $attributes['component_name'] = $scopes['component_name'];
                }
                else if (!empty($scopes['action']))
                {
                    $attributes['action'] = $scopes['action'];
                }

                $mainmenu_favorite_item = MainMenuFavoriteItem::model()->findByAttributes($attributes);
                
                $has_curr_user_item = false;
                if (!empty($mainmenu_favorite_item))
                {
                    $has_curr_user_item = MainMenuFavoriteItemToGroup::model()->countByAttributes([
                        'group_id' => $owner->id,
                        'item_id' => $mainmenu_favorite_item->id
                    ]) > 0;
                }
                
                return [$has_curr_user_item, SIMAMisc::getModelTags($owner)];
            case 'sorted_main_menu_favorite_group_to_items':
                $sorted_main_menu_favorite_group_to_items = $owner->getSortedMainMenuFavoriteGroupToItems();

                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($sorted_main_menu_favorite_group_to_items)
                ];

                return [$prop_value, SIMAMisc::getModelTags($sorted_main_menu_favorite_group_to_items)];
            default: return parent::vueProp($prop, $scopes);
        }
    }
}