<?php

class ClientBafRequestGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels()
    {
        return array(
            'name'=>Yii::t('BaseModule.ClientBafRequest', 'Name'),
            'requested_by_id'=>Yii::t('BaseModule.ClientBafRequest', 'OnRequest'),
            'requested_by'=>Yii::t('BaseModule.ClientBafRequest', 'OnRequest'),
            'create_time' => Yii::t('BaseModule.ClientBafRequest', 'CreateTime'),
            'send_time' => Yii::t('BaseModule.ClientBafRequest', 'SendTime'),
            'description'=>Yii::t('BaseModule.ClientBafRequest', 'Description'),
            'client_baf_request_sent'=>Yii::t('BaseModule.ClientBafRequest', 'Sent'),
            'priority'=>Yii::t('BaseModule.ClientBafRequest', 'Priority'),
            'send'=>Yii::t('BaseModule.ClientBafRequest', 'Send'),
            'id' => 'ID'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.ClientBafRequest', 'ClientBafRequest');
    }
    
    public function modelViews() 
    {
        return array_merge(parent::modelViews(), [
            'basic_info'=>['class' => 'basic_info']
        ]);
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'priority': return ClientBafRequest::$priorities[$owner->$column];
            case 'client_baf_request_sent':
                $return = Yii::t('BaseModule.ClientBafRequest', 'Yes');
                if (empty($owner->server_baf_request_id))
                {
                    $return = Yii::t('BaseModule.ClientBafRequest', 'No').' ';
                    
                    if(SIMAMisc::isVueComponentEnabled())
                    {
                        $return .= Yii::app()->controller->widget(SIMAButtonVue::class,[
                            'title'=>Yii::t('BaseModule.ClientBafRequest', 'Send'),
                            'tooltip'=>Yii::t('BaseModule.ClientBafRequest', 'Send'),
                            'onclick'=>['sima.errorReport.sendClientBafRequest',"$owner->id"]
                        ], true);
                    }
                    else
                    {
                        $return .= Yii::app()->controller->widget('SIMAButton',[
                            'action'=>[
                                'title'=>Yii::t('BaseModule.ClientBafRequest', 'Send'),
                                'tooltip'=>Yii::t('BaseModule.ClientBafRequest', 'Send'),
                                'onclick'=>['sima.errorReport.sendClientBafRequest',"$owner->id"]
                            ]
                        ], true);
                    }
                }
                return $return;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        $columns = [
            'name'=>'textField',
            'requested_by_id'=>'hidden',
            'priority'=>'dropdown'
        ];
        if (empty($owner->server_baf_request_id))
        {
            $columns['send'] = ['checkBox'];
        }
        $columns['description'] = ['tinymce', 'image_upload' => false];
        return array(
            'default'=>array(
                'type' => 'generic',
                'columns' => $columns
            )
        );
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            case 'fromRequestedBy': return [
                'columns'=>[
                    'name', 'create_time', 'send_time', 'priority', 'client_baf_request_sent', 'id'
                ]
            ];
            default:
                return array(
                'columns' => array(
                    'name', 'create_time', 'send_time', 'requested_by', 'priority', 'client_baf_request_sent', 'id'
                )
            );
        }
    }
    
    public function droplist($key, $relName='', $filter=null) 
    {
        switch ($key)
        {
            case 'priority': return ClientBafRequest::$priorities;
            case 'client_baf_request_sent': return ['NO' => Yii::t('BaseModule.ClientBafRequest', 'No'), 'YES' => Yii::t('BaseModule.ClientBafRequest', 'Yes')];
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}