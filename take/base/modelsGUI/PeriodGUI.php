<?php

class PeriodGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'period_name' => 'Period',
            'years' => 'Broj godina',
            'months' => 'Broj meseca',
            'days' => 'Broj dana'
               
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Period';
    }    
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            case 'period_name_only': return [
                'columns'=> ['period_name'],
            ];
            default:
                return array(
                'columns' => array(
                    'period_name', 'years', 'months', 'days'
                )                
            );
        }
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(                    
                    'period_name' => 'textField',
                    'years' => 'textField',
                    'months' => 'textField',
                    'days' => 'textField'
                )
            ),
        );
    }
}