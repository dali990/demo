<?php

class ErrorReportGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels()
    {
        return array(
            'time'=>Yii::t('BaseModule.ErrorReport', 'ReportingTime'),
            'message'=>Yii::t('BaseModule.ErrorReport', 'Message'),
            'message_strip_tags'=>Yii::t('BaseModule.ErrorReport', 'MessageStripTags'),
            'action'=>Yii::t('BaseModule.ErrorReport', 'Action'),
            'description'=>Yii::t('BaseModule.ErrorReport', 'Description'),
            'get_params'=>Yii::t('BaseModule.ErrorReport', 'GetParams'),
            'post_params'=>Yii::t('BaseModule.ErrorReport', 'PostParams'),
            'submitter_id'=>Yii::t('BaseModule.ErrorReport', 'Submitter'),
            'submitter'=>Yii::t('BaseModule.ErrorReport', 'Submitter'),
            'fixed'=>Yii::t('BaseModule.ErrorReport', 'Fixed'),
            'error_sent'=>Yii::t('BaseModule.ErrorReport', 'ErrorSent'),
            'id' => 'ID'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.ErrorReport', 'ErrorReport');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'get_params': return CJSON::encode($owner->get_params);
            case 'post_params': return CJSON::encode($owner->post_params);
            case 'message': 
                return SIMAHtml::addContentToIframe(Yii::app()->errorReport->getErrorMessageDisplay($owner->message));
            case 'message_strip_tags': return strip_tags(Yii::app()->errorReport->getErrorMessageDisplay($owner->message));
            case 'error_sent':
                $return = Yii::t('BaseModule.ErrorReport', 'Yes');
                if (empty($owner->server_error_report_id))
                {
                    $return = Yii::t('BaseModule.ErrorReport', 'No').' ';
                    
                    if(SIMAMisc::isVueComponentEnabled())
                    {
                        $return .= Yii::app()->controller->widget(SIMAButtonVue::class,[
                            'title'=>Yii::t('BaseModule.ErrorReport', 'ErrorSend'),
                            'tooltip'=>Yii::t('BaseModule.ErrorReport', 'ErrorSend'),
                            'onclick'=>['sima.errorReport.sendError',"$owner->id"]
                        ], true);
                    }
                    else
                    {
                        $return .= Yii::app()->controller->widget('SIMAButton',[
                            'action'=>[
                                'title'=>Yii::t('BaseModule.ErrorReport', 'ErrorSend'),
                                'tooltip'=>Yii::t('BaseModule.ErrorReport', 'ErrorSend'),
                                'onclick'=>['sima.errorReport.sendError',"$owner->id"]
                            ]
                        ], true);
                    }
                }
                return $return;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelViews() 
    {
        return array_merge(parent::modelViews(), [
            'basic_info'=>['class' => 'basic_info']
        ]);
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return array(
                'columns' => array(
                    'error_sent', 'description', 'time', 'action', 'submitter', 'id'
                )                
            );
        }
    }
    
    public function droplist($key, $relName='', $filter=null) 
    {
        switch ($key)
        {
            case 'error_sent': return ['NO' => Yii::t('BaseModule.ErrorReport', 'No'), 'YES' => Yii::t('BaseModule.ErrorReport', 'Yes')];
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}