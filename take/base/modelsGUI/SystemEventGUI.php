<?php

class SystemEventGUI extends SIMAActiveRecordGUI
{
    public function modelLabel($plural = false)
    {
        return $plural ? Yii::t('BaseModule.SystemEvent','SystemEvents') : Yii::t('BaseModule.SystemEvent','SystemEvent');
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns' => ['timestamp', 'name', 'class', 'function', 'status', 'params'],
                'order_by' => ['timestamp']
            ];
        }
    }
    
    public function droplist($key, $relName = '', $filter = null)
    {
        switch ($key) 
        {
            case 'status': return [
                SystemEvent::$STATUS_WAITING => Yii::t('BaseModule.SystemEvent', SystemEvent::$STATUS_WAITING),
                SystemEvent::$STATUS_PROGRESS => Yii::t('BaseModule.SystemEvent', SystemEvent::$STATUS_PROGRESS),
                SystemEvent::$STATUS_DONE => Yii::t('BaseModule.SystemEvent', SystemEvent::$STATUS_DONE)
            ];
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}

