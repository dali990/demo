<?php

class DriverLicenseCategoryGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'category' => 'Kategorija',
            'age_range' => 'Godine starosti',
            'description' => 'Opis'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Vozačka kategorija';
    }    
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'category'=>'textField',
                    'age_range'=>'textField',
                    'description'=>'textArea'                    
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return array(
                'columns' => array(
                    'category','description', 'age_range'
                ),
            );
        }
    }
}