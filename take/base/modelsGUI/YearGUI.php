<?php

class YearGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'year' => 'Godina'            
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Godina';
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'year'
                )
            );
        }
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'year' => 'textField'                    
                )
            )
        );
    }

}