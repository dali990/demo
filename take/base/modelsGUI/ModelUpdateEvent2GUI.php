<?php

class ModelUpdateEvent2GUI extends SIMAActiveRecordGUI
{
//    public function columnDisplays($column)
//    {
//        $owner = $this->owner;
//        switch ($column)
//        {
//            case 'old_data': 
//            case 'new_data': 
//                return json_encode($owner->$column);
//            default: return parent::columnDisplays($column);
//        }
//    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => [
                        'php_session_id',
                        'create_time',
                        'tg_op',
                        'tg_table_schema',
                        'tg_table_name',
//                        'old_data',
//                        'new_data'
                    ],
                    'order_by' => [
                        'create_time'
                    ]
                ];
        }
    }
}
