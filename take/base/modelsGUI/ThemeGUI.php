<?php

ThemeGUI::$types = [
    'PLAIN' => Yii::t('BaseModule.Theme', 'Plain'),
    'JOB' => Yii::t('BaseModule.Theme', 'Job'),
    'BID' => Yii::t('BaseModule.Theme', 'Bid')
];

ThemeGUI::$statuses = [
    'ACTIVE' => Yii::t('BaseModule.Theme', 'Active'),
    'NOT_ACTIVE' => Yii::t('BaseModule.Theme', 'NotActive')
];

class ThemeGUI extends SIMAActiveRecordDisplayGUI
{
    public static $types = [];
    public static $statuses = [];

    public function columnLabels()
    {
        return array(
            'display_name'=>Yii::t('BaseModule.Common', 'DisplayName'),
            'display_name_with_code'=>Yii::t('BaseModule.Common', 'DisplayName'),
            'name'=>Yii::t('BaseModule.Common', 'Name'),
            'coordinator'=>Yii::t('BaseModule.Theme', 'Coordinator'),
            'coordinator_id'=>Yii::t('BaseModule.Theme', 'Coordinator'),
            'parent'=>Yii::t('BaseModule.Theme', 'ParentTheme'),
            'parent_id'=>Yii::t('BaseModule.Theme', 'ParentTheme'),
            'tag'=>Yii::t('BaseModule.Common', 'Tag'),
            'job_type_id' => Yii::t('BaseModule.Theme', 'JobType'),
            'job_type' => Yii::t('BaseModule.Theme', 'JobType'),
            'cost_location_id' => Yii::t('BaseModule.Theme', 'CostLocation'),
            'cost_location' => Yii::t('BaseModule.Theme', 'CostLocation'),
            'add_cost_location' => Yii::t('BaseModule.Theme', 'CostLocation'),
            'geo_objects_list' => GeoObject::model()->modelLabel(true),
            'geo_objects' => GeoObject::model()->modelLabel(true),
            'settings' => Yii::t('BaseModule.Theme', 'Settings')
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.Theme','Theme');
    }
    
    public function getDisplayHtml($params=[])
    {
        $owner = $this->owner;
        
        $curr_display_html = parent::getDisplayHtml(['display_html_class' => $owner->getHTMLClass(), 'display_text' => $owner->name]);
        
        if (!empty($owner->parent))
        {
            return $owner->parent->getDisplayHtml($params) . '_' . $curr_display_html;
        }
        
        return $curr_display_html;
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column) 
        {
            case 'type': return isset(self::$types[$owner->type]) ? self::$types[$owner->type] : $owner->type;
            case 'job_type': return isset($owner->job_type) ? parent::columnDisplays($column) : Yii::t('BaseModule.Theme', 'EmptyJobType');
            case 'status': 
                switch ($owner->type) 
                {
                    case Theme::$BID: $suffix = $owner->bid->getAttributeDisplay('bid_status'); break;
                    case Theme::$JOB: $suffix = $owner->job->getAttributeDisplay('status'); break;
                    default: $suffix = ''; break;
                }
                if (!empty($suffix))
                {
                    $suffix = '('.$suffix.')';
                }
                
                return self::$statuses[$owner->status].$suffix;
            case 'add_cost_location':
            if (isset($owner->cost_location)) //SASA -  TREBA DA SE PROMENI NAZIV UMESTO JOB_ORDER U COST_LOCATION
            {
                $_res = '';
                if (Yii::app()->user->checkAccess('modelCostLocationAdd'))
                {
                    $_res .= SIMAHtml::modelFormOpen($owner->cost_location);
                }
                return $_res.$owner->cost_location->DisplayName;
            }
            else
            {
                if (Yii::app()->user->checkAccess('modelCostLocationAdd'))
                {
                    if (isset($owner->job_type))
                    {
                        $prefix = $owner->job_type->code;
                    }
                    else
                    {
                        $prefix = 'SE';
                    }
                    $year = Year::get()->id;
                    return "<button class='sima-ui-button' onclick='sima.jobs.addCostLocation(".$owner->id.",\"$prefix\",$year);'>Dodaj mesto troška</button>";
                }
                else
                {
                    return '';
                }
            };
            case 'geo_objects': 
                $txt = [];
                foreach ($owner->geo_objects as $geo_object)
                {
                    $txt[] = $geo_object->display_name_full;
                }
                return implode(' <br /> ', $txt);
            case 'persons_to_teams.person': 
                $html = '';
                foreach ($owner->persons_to_teams as $person_to_theme)
                {
                    $html .= $person_to_theme->person->DisplayHtml . '<br />';
                }

                return $html;
            default: return parent::columnDisplays($column);
        }
        
    }
    
    public function modelViews()
    {
        return array_merge(parent::modelViews(), [
            'options'=>['params_function' => 'paramsViewOptions'],
            'financeInfo' => ['params_function'=>'financeInfoParams', 'class' => 'sima-layout-panel'],
            'indexTitle'=>array('origin'=>'Theme'),
            'full_info' => ['params_function'=>'ParamsFullInfo'],
            'full_info_solo' => ['html_wrapper' => null],
            'title',
            'basic_info'
        ]);
    }
    
    public function financeInfoParams()
    {
        $owner = $this->owner;
        
        $criteria = new SIMADbCriteria([
            'Model' => 'JobFinance',
            'model_filter' => [
                'display_scopes' => array(
                    'withMainSelect',
                    'withMainJoin',
                    'byPartners',
//                    'forTheme' => array(
//                        $owner->id
//                    )
                ),
            ]
        ]);
        
        $job_finance = JobFinance::model()->forTheme($owner->id)->sumFunc($criteria);
        
        return [
            'model' => $owner,
            'job_finance' => $job_finance
        ];
    }
    
    public function ParamsFullInfo()
    {
        $model = $this->owner;
        
        $_crit1 = new SIMADbCriteria([
            'Model' => 'File',
            'model_filter' => [
                'belongs' => $model->tag->query
            ]
        ]);
        $file_cnt = File::model()->count($_crit1);
        
        $_crit2 = new SIMADbCriteria([
            'Model' => 'File',
            'model_filter' => [
                'belongs' => $model->tag->queryR
            ]
        ]);
        $file_cnt_r = File::model()->count($_crit2);
        
        $_crit3 = new SIMADbCriteria([
            'Model' => 'Task',
            'model_filter' => [
                'theme' => ['ids' => $model->id]
            ]
        ]);
        $task_cnt = Task::model()->count($_crit3);
        $task_cnt_r = '??';
        $subtheme_cnt = '??';
        
        return [
            'model' => $model,
            'coordinatorDisplay' => isset($model->coordinator)?$model->coordinator->DisplayHTML:'nema koordinatora',
            'file_cnt' => $file_cnt,
            'file_cnt_r' => $file_cnt_r,
            'task_cnt' => $task_cnt,
            'task_cnt_r' => $task_cnt_r,
            'subtheme_cnt' => $subtheme_cnt,
        ];
    }
    
    public function paramsViewOptions()
    {
        $owner = $this->owner;
        
        $subactions = [];
        
        $can_convert_theme = Yii::app()->user->checkAccess('Convert',[],$owner) || Yii::app()->user->id === intval($owner->coordinator_id);
        if ($can_convert_theme)
        {
            if ($owner->type == Theme::$PLAIN)
            {
//                $subactions[] = [
//                    'title' => Yii::t('BaseModule.Theme', 'ToJobBid'),
//                    'onclick' => [
//                        'sima.theme.convertThemeTo', $owner->id, 'JobBid'
//                    ]
//                ];
//                $subactions[] = [
//                    'title' => Yii::t('BaseModule.Theme', 'ToQualificationBid'),
//                    'onclick' => [
//                        'sima.theme.convertThemeTo', $owner->id, 'QualificationBid'
//                    ]
//                ];
                $subactions[] = [
                    'title' => Yii::t('BaseModule.Theme', 'ToJob'),
                    'onclick' => [
                        'sima.theme.convertThemeTo', $owner->id, 'Job'
                    ]
                ];
            }
            else
            {
                $subactions[] = [
                    'title' => Yii::t('BaseModule.Theme', 'ToPlaneTheme'),
                    'onclick' => [
                        'sima.theme.convertThemeTo', $owner->id, 'Theme'
                    ]
                ];
            }
        }
        
        return [
            'model' => $owner,
            'subactions' => $subactions
        ];
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        if ($owner->type == Theme::$PLAIN)
        {
            $status_sett = ['dropdown','empty' => null];
        }
        else
        {
            $status_sett = 'hidden';
        }
        
        $form_columns = [
            'name' => 'textField',
            'parent_id' => ['relation','relName'=>'parent', 
                'filters' => [
                        'filter_scopes' => ['withoutTypeBaf']
                ]
            ],
            'coordinator_id' => ['relation', 'relName' => 'coordinator'],
            'job_type_id' => ['dropdown', 'relName'=>'job_type', 'empty' => Yii::t('BaseModule.Theme', 'EmptyJobType'), 
//                'dependent_on' => [
//                    'parent_id' => [
//                        'onValue' => ['trigger', 'func' => ['sima.theme.themeJobTypeAndCostLocationDependencyInForm']],
//                        'onEmpty' => ['enable']
//                    ]                        
//                ]
            ],
            'status' => $status_sett,
            'geo_objects_list'=>array('list',
                'model'=>'GeoObject',
                'relName'=>'geo_objects', 
                'multiselect'=>true
            ),
            'comment' => 'textArea'
        ];
        
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => $form_columns
            )
        );
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            case 'belongs': return [
                'columns' => [
                    'display_name' => ['min_width'=>'700px'],
                    'job_type',
                    'status',
                    'coordinator',
                    'type',
                    'comment'
                ]
            ];
            case 'model_choose':
                return [
                    'columns' => ['display_name', 'persons_to_teams.person']
                ];
            default:
                return array(
                    'columns' => array(
                        'display_name',
                        'status',
                        'coordinator',
                        'type',
                        'job_type',
                        'geo_objects',
                        'comment'
                    ),
                    'order_by' => [
                        'display_name'
                    ]
                );
        }
    }
    
    public function droplist($key, $relName = '', $filter = null)
    {
        switch ($key) 
        {
            case 'type': return self::$types;
            case 'status': return self::$statuses;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function getHTMLClass()
    {
        $owner = $this->owner;
        switch ($owner->type) {
            case Theme::$JOB: return '_job';
            case Theme::$BID: return '_bid';
            case Theme::$PLAIN: return '_plain';
            default: return ''; 
        }
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {
        $allowed_tables = [];
        
        array_push($allowed_tables, PersonToTheme::model()->tableName().':theme_id');
        array_push($allowed_tables, Job::model()->tableName().':id');
        array_push($allowed_tables, Bid::model()->tableName().':id');
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'has_super_access':
                $update_models = Theme::model()->findAll([
                    'model_filter' => [
                        'filter_scopes' => [
                            'onlyCurrThemeAndParentThemes' => $owner->id
                        ]
                    ]
                ]);

                return [!empty(Theme::model()->hasSuperAccess(Yii::app()->user->id)->findByPk($owner->id)), SIMAMisc::getModelTags($update_models)];
            case 'has_change_access':
                $update_models = Theme::model()->findAll([
                    'model_filter' => [
                        'filter_scopes' => [
                            'onlyCurrThemeAndParentThemes' => $owner->id
                        ]
                    ]
                ]);

                return [!empty(Theme::model()->hasChangeAccess(Yii::app()->user->id)->findByPk($owner->id)), SIMAMisc::getModelTags($update_models)];
            default: return parent::vueProp($prop, $scopes);
        }
    }
}