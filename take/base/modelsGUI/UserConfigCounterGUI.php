<?php

class UserConfigCounterGUI extends SIMAActiveRecordDisplayGUI
{

    public function columnLabels()
    {
        return array(
            'name' => Yii::t('BaseModule.Common', 'Name'),
            'time_interval' => Yii::t('BaseModule.UserConfigCounter', 'TimeInterval'),
            'template' => Yii::t('BaseModule.UserConfigCounter', 'Template'),
            'note' => Yii::t('BaseModule.Common', 'Note'),
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.UserConfigCounter',$plural?'UserConfigCounters':'UserConfigCounter');
    }    
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'time_interval': return UserConfigCounter::$interval_types[$owner->time_interval];
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelViews()
    {
        return array_merge([
            'full_info'
        ],parent::modelViews());
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'name'=>'textField',
                    'time_interval'=>'dropdown',
                    'template'=>'textField',
                    'note'=>'textArea'
                )
            )
        );
    }
    
    public function droplist($key, $relName = '', $filter = null)
    {
        switch ($key)
        {
            case 'time_interval': return UserConfigCounter::$interval_types;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => array(
                        'name', 
                        'time_interval',
                        'template', 
                        'note'
                    ),
                    'order_by' => [
                        'name', 
                        'time_interval',
                        'template', 
                        'note'
                    ]
                ];
        }
    }
}