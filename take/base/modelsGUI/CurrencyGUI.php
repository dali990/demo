<?php

class CurrencyGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'code' => Yii::t('BaseModule.Currency', 'Code'),
            'short_name' => Yii::t('BaseModule.Currency', 'ShortName')
        )+parent::columnLabels();
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => array(
                        'code', 
                        'short_name',
                        'name', 
                        'description'
                    ),
                    'order_by' => [
                        'code', 
                        'name', 
                        'short_name',
                        'description'
                    ]
                ];
        }
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'code'=>'textField',
                    'name'=>'textField',
                    'short_name'=>'textField',
                    'description'=>'textArea'
                )
            )
        );
    }
}