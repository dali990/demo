<?php

class SessionGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'user' => Yii::t('BaseModule.Session','User'),
            'start_time' =>  Yii::t('BaseModule.Session','Start'),
            'end_time'=>Yii::t('BaseModule.Session','End'),
            'server_id'=>Yii::t('BaseModule.Session','Server'),
            'user_address'=>Yii::t('BaseModule.Session','UserAddress'),
            'session_type'=>Yii::t('BaseModule.Session','SessionType'),
            'file_disk_id'=>Yii::t('BaseModule.Session','FileDiskId'),
            'starting_directory_definition'=>Yii::t('BaseModule.Session','StartingDirectoryDefinition'),
            'pseudonim'=>Yii::t('BaseModule.Session','Pseudonim'),
            'last_seen'=>Yii::t('BaseModule.Session','LastSeen'),
            'idle_since'=>Yii::t('BaseModule.Session','IdleSince'),
            'licence_type'=>Yii::t('BaseModule.Session','LicenceType'),
            'data'=>Yii::t('BaseModule.Session','Data'),
            'active' => Yii::t('BaseModule.Session','Active'),
            'is_idle' => Yii::t('BaseModule.Session','IsIdle'),
            'log_out_session' => Yii::t('BaseModule.Session','LogOutSession'),
            'operating_system' => Yii::t('BaseModule.Session','OperatingSystem'),
            'browser' => Yii::t('BaseModule.Session','Browser'),
            'browser_version' => Yii::t('BaseModule.Session','BrowserVersion'),
            'browser_string' => Yii::t('BaseModule.Session','BrowserString'),
            'width_sima_resolution' => Yii::t('BaseModule.Session','WidthSimaResolution'),
            'height_sima_resolution' => Yii::t('BaseModule.Session','HeightSimaResolution'),
            'zoom' => Yii::t('BaseModule.Session','Zoom')
               
        )+parent::columnLabels();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'data': return CJSON::encode($owner->data);
                
            case 'active': 
                if ($owner->is_active)
                    return Yii::t('BaseModule.Session','Active');
                else if ($owner->is_idle)
                    return Yii::t('BaseModule.Session','Idle');
                else if ($owner->is_inactive)
                    return Yii::t('BaseModule.Session','Inactive');
                else
                    return '';
            case 'log_out_session' :
                $result = '';
                if($owner->is_online && $owner->session_type !== Session::$TYPE_SIMAMobileApp)
                {
                    $result = "<button onclick='sima.misc.logOutSession($(this))' data-model_id='$owner->id'> ".Yii::t('BaseModule.Session','LogOutSession')." </button>";
                }
                return $result;
            case 'session_type':
                $type_displays = $this->owner->getTypesArray();
                if (!empty($owner->session_type))
                {
                    $result = $type_displays[$owner->session_type];
                }
                else
                {
                    $result = '';
                }
                return $result;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.Session','Session');
    }
    
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return array(
                'columns' => array(
                    'user',
                    'start_time',
                    'user_address', 
                    'session_type',
                    'pseudonim',
                    'last_seen',
                    'idle_since',
                    'licence_type',
                    'active',
                    'log_out_session',
                    'operating_system',
                    'browser',
                    'browser_version',
                    'browser_string',
                    'width_sima_resolution',
                    'height_sima_resolution',
                    'zoom'
                ),
                'order_by'=>array('last_seen', 'start_time')
            );
        }
    }
    
    
    public function droplist($key, $relName='', $filter=null) {
        
        switch ($key) {
            case 'licence_type':
                return [
                    Session::$LICENCE_TYPE_MASTER => Yii::t('BaseModule.Session', Session::$LICENCE_TYPE_MASTER),
                    Session::$LICENCE_TYPE_PERSONAL => Yii::t('BaseModule.Session', Session::$LICENCE_TYPE_PERSONAL),
                    Session::$LICENCE_TYPE_SHARED => Yii::t('BaseModule.Session', Session::$LICENCE_TYPE_SHARED),
                    Session::$LICENCE_TYPE_UNLICENCED => Yii::t('BaseModule.Session', Session::$LICENCE_TYPE_UNLICENCED),
                ];
            case 'session_type':
                $type_displays = $this->owner->getTypesArray();
                return $type_displays;
            case 'active': return array ('0' => Yii::t('BaseModule.Session','ActiveSessions'), '1' => Yii::t('BaseModule.Session','IdleSessions'), '2' => Yii::t('BaseModule.Session','InactiveSessions'));
            case 'browser': return Session::$browsers;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
  
}