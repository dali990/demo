<?php

class PhoneNumberTypeGUI extends SIMAActiveRecordGUI
{
    public function guiTableSettings($type)
    {
        switch ($type)
        {   
            default:
                return [
                    'columns' => [
                        'name'
                    ]
                ];
        }
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.PhoneNumber', $plural ? 'PhoneNumberTypes' : 'PhoneNumberType');
    }
    
    public function columnLabels()
    {
        return [
            'name'=> Yii::t('BaseModule.Common', 'Name')
        ]+parent::columnLabels();
    }
    
}