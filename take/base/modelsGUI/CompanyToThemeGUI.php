<?php

class CompanyToThemeGUI extends SIMAActiveRecordGUI
{
    public function columnLabels() 
    {
        return [
            'company_id' => Yii::t('Company', 'Company'),
            'theme_id' => Yii::t('BaseModule.Theme', 'Theme'),
            'contract_id' => Yii::t('BaseModule.CompanyToTheme', 'Contract'),
            'company' => Yii::t('Company', 'Company'),
            'theme' => Yii::t('BaseModule.Theme', 'Theme'),
            'contract' => Yii::t('BaseModule.CompanyToTheme', 'Contract'),
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.CompanyToTheme', $plural ? 'CompaniesToThemes' : 'CompanyToTheme');
    }
    public function columnDisplays($column)
    {
        switch ($column)
        {
            default: return parent::columnDisplays($column);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'from_company': return [
                'columns' => [
                    'theme', 'contract'
                ]
            ];
            case 'from_theme': return [
                'columns' => [
                    'company', 'contract'
                ]
            ]; 
            default: return [
                'columns' => [
                    'company', 'theme', 'contract'
                ]
            ];
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        $company_id = ['relation', 'relName' => 'company', 'add_button' => true];
        $theme_id = ['relation', 'relName' => 'theme', 'add_button' => true];
        if (!empty($owner->company_id))
        {
            $company_id['disabled'] = true;
        }
        if (!empty($owner->theme_id))
        {
            $theme_id['disabled'] = true;
        }
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'company_id' => $company_id,
                    'theme_id' => $theme_id,
                    'contract_id' => ['relation', 'relName' => 'contract', 'add_button' => true]
                ]
              ],
        ];
    }
}
?>
