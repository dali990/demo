<?php

class PersonToThemeGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'person_id' => 'Član',
            'person' => 'Član',
            'role_in_team' => Yii::t('BaseModule.PersonToTheme', 'RoleInTeam'),
            'to_notif' => Yii::t('BaseModule.PersonToTheme', 'NotifWhenStatusOfThemeChange'),
            'is_cost_controller' => Yii::t('BaseModule.PersonToTheme', 'CostController'),
        ) + parent::columnLabels();
    }

    public function getDisplayHtmlText()
    {
        $owner = $this->owner;
        return $owner->person->DisplayName.(($owner->isCoordinator())?' koordinator':'');
    }
    
    public function getDisplayHtml($params=[])
    {
        $owner = $this->owner;
        return $owner->person->DisplayHTML.(($owner->isCoordinator())?' - koordinator':'');
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.Theme', $plural?'Team members':'Team member');
    }
    

//    public function modelViews()
//    {
//        return array(
//                ) + parent::modelViews();
//    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'person', 'role_in_team', 'is_cost_controller'
                )
            );
        }
    }

    public function modelForms()
    {
        $owner = $this->owner;
        $person_id = ['relation', 'relName' => 'person'];
        $cost_controller = ['checkBox',
            'dependent_on' => [
                'person_id' => [
                    'onValue' => [
                        'trigger', 'func' => ['sima.theme.personOnCostControllerHendler']
                    ],
                    'onEmpty' => ['hide']
                ]
            ]
        ];
        $curr_user_id = Yii::app()->user->id;
        if (!empty($owner->person_id))
        {
            $person_id['disabled'] = true;
        }
        if(!empty($owner->theme_id))
        {
            if($owner->theme->coordinator_id !== $curr_user_id && !(Yii::app()->user->checkAccess('modelPersonToThemeCostController')))
            {
                $cost_controller['disabled'] = true;
            }
        }
        return array(
            //ova forma bi trebala da se koristi samo za dodavanje ili za edit da se samo upise role_in_team
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'person_id' => $person_id,
                    'theme_id' => 'hidden',
                    'role_in_team' => 'textField',
                    'to_notif'=>['checkBox', 'dependent_on'=>[
                        'person_id'=>[
                            'onValue'=>['show', 'for_values'=>[$curr_user_id]],
                            'onEmpty'=>'hide'
                        ]
                    ]],
                    'is_cost_controller' => $cost_controller
                )
            )
        );
    }

}
