<?php

class QRGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'base_model'=> Yii::t('BaseModule.QRCode', 'BaseModel'),
            'qr_data' => Yii::t('BaseModule.QRCode', 'QRData'),
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.QRCode', 'QRCode');
    }
    
    public function modelViews()
    {
        return [
            'qrcode',
            'qrcode_multiselect',
        ];
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'base_model': 
                $result = '';
                
                if(empty($owner->base_model))
                {
                    $result = Yii::t('BaseModule.Common', 'DoNotHave');
                }
                else
                {
                    $result = $owner->base_model->DisplayHTML;
                }
                
                return $result;
            default: return parent::columnDisplays($column);
            case 'image_tag_src_path': 
                $temp_file = QRCodeComponent::generateQRPngTemp($owner, 1);
                $png_temp_file_path = $temp_file->getFullPath();
                
                return $png_temp_file_path;
        }
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return array(
                'columns' => array(
                    'base_model'
                )                
            );
        }
    }
}
