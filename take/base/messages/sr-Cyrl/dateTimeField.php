<?php

return array(
    'timeOnlyTitle'=> 'Изаберите време',
    'timeText'=> 'Време',
    'hourText'=> 'Сат',
    'minuteText'=> 'Минут',
    'secondText'=> 'Секунд',
    'millisecText'=> 'Милисекунд',
    'timezoneText'=> 'Временска зона',
    'currentText'=> 'Данас/Сад',
    'closeText'=> 'Затвори',
    'firstDay' => '1'  
)

?>

