<?php

/* 
 * Trivijalni prevodi
 */

return array(
    'Note' => 'Напомена',
    'Date' => 'Датум',
    'Greater then zero' => 'Веће од нуле',
    'Document' => 'Документ',
    'Order' => 'Редни број'
);
