<?php

return array(
    'rangeStartTitle'=> 'Почетак',
    'rangeEndTitle'=> 'Крај',
    'nextLinkText'=> 'Следећи',
    'prevLinkText'=> 'Претходни',
    'doneButtonText'=> 'Завршено',
    'specificDate'=> 'Специфичан датум',
    'allDatesBefore'=> 'Сви дани пре',
    'allDatesAfter'=> 'Сви дани после',
    'dateRange'=> 'Опсег датума',
    'presetRanges_text1' => 'Данас',    
    'presetRanges_text2' => 'Последњих 7 дана',
    'presetRanges_text3' => 'Од почетка месеца',
    'presetRanges_text4' => 'Од почетка године',
    'presetRanges_text5' => 'Претходни месец',
)

?>

