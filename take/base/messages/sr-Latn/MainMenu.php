<?php

return [
    'AddressBook' => 'Imenik',
    'MyTeam' => 'Moj tim',
    'AddNewClientBafRequest' => 'Dodajte novi zahtev',
    'SIMADocumentation' => 'SIMA dokumentacija',
    'PersonalDocumentation' => 'Lična dokumentacija',
    'ProfileUserConfiguration' => 'Korisnička podešavanja',
    'ProfileTasks' => 'Zadaci',
    'ProfileActivities' => 'Aktivnosti',
    'Logout' => 'Izlogujte se',
    'ToggleLeftBar' => 'Skupite/raširite meni',
    'AddFavoriteItemGroup' => 'Dodajte grupu',
    'ToggleItemFromFavorites' => 'Dodajte/uklonite stavku iz omiljenih',
    'RemoveOpenedItem' => 'Uklonite stavku iz otvorenih',
    'ReloadItemAction' => 'Učitajte ponovo',
    'SetFavoriteGroupToOpened' => 'Dodajte grupu u otvorenim stavkama',
    'Email' => 'Email'
];