<?php

return [
    'PhoneNumbers'   => 'Telefoni',
    'PhoneNumber'    => 'Telefon',
    'Country'        => 'Država',
    'ProviderNumber' => 'Pozivni broj',
    'Number'         => 'Broj',
    'PhoneNumberAlreadyExist' => 'Telefon koji ste uneli već postoji',
    'PhoneNumberType' => 'Tip telefona',
    'PhoneNumberTypes' => 'Tipovi telefona',
    'AddPhoneNumberTypesOperation' => 'Dodavanje tipa telefona',
    'Partner' => 'Partner'
];

