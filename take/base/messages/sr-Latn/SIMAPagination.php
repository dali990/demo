<?php

return [
    'CurrentPage' => 'Trenutna strana',
    'FirstPage' => 'Prva strana',
    'PreviousPage' => 'Prethodna strana',
    'NextPage' => 'Sledeća strana',
    'LastPage' => 'Poslednja strana',
    'TotalNumberOfPages' => 'Ukupan broj strana'
];