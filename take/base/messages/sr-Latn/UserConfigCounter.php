<?php

return [
    'UserConfigCounter' => 'Korisnički brojač',
    'UserConfigCounters' => 'Korisnički brojači',
    'TimeInterval' => 'Vremenski interval',
    'Template' => 'Šablon',
    'LastTakenValue' => 'Poslednja uzeta vrednost',
    'UserConfigCounterItem' => 'Vrednost brojača',
    'UserConfigCounterItems' => 'Vrednosti brojača'
];