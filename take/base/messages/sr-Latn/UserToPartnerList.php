<?php

return [
    'UserToPartnerList' => 'Veza korisnik-lista partnera',
    'UsersToPartnerLists' => 'Veza korisnici-liste partnera',
    'User' => 'Korisnik',
    'PartnerList' => 'Lista partnera',
    'PartnerListGroups' => 'Grupe liste partnera'
];