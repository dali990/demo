<?php

return [
    'WorkDayTypeConfNotSet' => 'Konfiguracija za tip radnog dana nije postavljena.</br>Molimo kontaktirajte administratora',
    'WekendDayTypeConfNotSet' => 'Konfiguracija za tip dana za vikend nije postavljena.</br>Molimo kontaktirajte administratora',
    'SIMAExceptionFunctionWrongParamsMessage' => 'Funkciji {function_name} nisu prosledjeni parametri: {params}',
    'SIMAModelViewNotFound'=>"Modelu '{model_name}' nije registrovan pogled '{view_name}', ili mu fali fajl sa izgledom",
    'SIMAFunctionWrongParamsExceptionMessage' => 'Funkciji {function_name} nisu prosledjeni parametri: {params}',
    'WarnModelNotFound' => "Zahtevan <b>\"{model_label}\"</b> nije pronađen u bazi.",
    'WarnModelTabNotFound' => "Zahtevan tab '{code}' nije pronađen za model '{model_label}' ID: {model_id}."
];

