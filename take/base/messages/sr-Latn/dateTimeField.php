<?php

return array(
    'timeOnlyTitle'=> 'Izaberite vreme',
    'timeText'=> 'Vreme',
    'hourText'=> 'Sat',
    'minuteText'=> 'Minut',
    'secondText'=> 'Sekund',
    'millisecText'=> 'Milisekund',
    'timezoneText'=> 'Vremenska Zona',
    'currentText'=> 'Danas/Sad',
    'closeText'=> 'Zatvori',
    'firstDay' => '1'  
)

?>

