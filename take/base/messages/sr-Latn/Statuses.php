<?php

return [
    'ValidationDidnotPassedForNextModels' => 'Za sledeće modele nije prošla validacija',
    'DontHavePrivilegeForThisConfirmation' => 'Nemate potrebnu privilegiju za ovu potvrdu',
    'DontHavePrivilegeForThisRevert' => 'Nemate potrebnu privilegiju za ovo opovrgavanje potvrde',
    'DontHavePermissionForThisConfirmation' => 'Nemate pravo na ovu potvrdu - kod 1',
    'DontHavePermissionForThisRevert' => 'Nemate pravo na ovo opovrgavanje potvrde - kod 1',
    'ThisConfirmationCanNotBeSigned' => 'Ova potvrda ne može da se potpiše',
    'ThisConfirmationCanNotBeReverted' => 'Ova potvrda ne može da se opovrgne'
];

