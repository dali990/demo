<?php

return [
    'PasswordStrengthIsWeak' => 'Šifra nije dovoljno jaka',
    'WrongPasswordLength' => 'Dužina mora da bude minimum {min_length} karaktera.',
    'PasswordMustHaveLowercaseLetters' => 'Mora imati bar jedno malo slovo.',
    'PasswordMustHaveUppercaseLetters' => 'Mora imati bar jedno veliko slovo.',
    'PasswordMustHaveNumbers' => 'Mora imati bar jedan broj.',
    'PasswordMustHaveSpecialCharacters' => 'Mora imati bar jedan specijalni karakter iz liste dozvoljenih: {allowed_special_characters}.',
];