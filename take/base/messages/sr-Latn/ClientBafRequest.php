<?php

return [
    'PriorityLow' => 'Nizak - ako se nekad stigne',
    'PriorityMiddle' => 'Srednji - bilo bi korisno',
    'PriorityHigh' => 'Visok - uraditi što pre',
    'PriorityError' => 'Greška - puca sistem, pravi probleme',
    'Name' => 'Naziv',
    'OnRequest' => 'Na zahtev',
    'CreateTime' => 'Vreme kreiranja',
    'SendTime' => 'Vreme slanja',
    'Description' => 'Opis',
    'Sent' => 'Poslat',
    'Priority' => 'Prioritet',
    'Send' => 'Pošalji',
    'ClientBafRequest' => 'Novi zahtev',
    'Yes' => 'Jeste',
    'No' => 'Nije',
    'UnsendClientBafRequests' => 'Neposlati zahtevi',
    'UnsendClientBafRequestsCount' => 'Broj neposlatih zahteva',
];

