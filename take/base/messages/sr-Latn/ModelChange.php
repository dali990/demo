<?php

return [
    'ChangeInsert' => 'Kreiranje',
    'ChangeUpdate' => 'Izmena',
    'ChangeDelete' => 'Brisanje',
    'ChangeType' => 'Tip izmene',
    'ChangeTime' => 'Vreme izmene'
];
