<?php


return array(
    'CreatesCycle' => 'Izabrali ste isti {name}, ili ste napravili ciklus',
    'NoChangeWhenConfirmed' => 'Ne može da se menja kad je potvrđeno',
    'NoChange' => 'Ne može da se menja',
);
