<?php

return [
    'MainMenuFavoriteGroups' => 'Grupe',
    'MainMenuFavoriteGroup' => 'Grupa',
    'GroupName' => 'Naziv grupe',
    'User' => 'Korisnik',
    'IsDefault' => 'Da li je grupa osnovna',
    'Default' => 'Radna površina',
    'CanNotDeleteDefaultGroup' => 'Ne možete da obrišete osnovnu grupu'
];