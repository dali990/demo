<?php

return [
    'RoleInTeam' => 'Uloga u timu',
    'NotifPersonInTeam' => 'Postali ste deo tima na temi "{theme_name}"',
    'NotifPersonOutOfTeam' => 'Niste više deo tima na temi "{theme_name}"',
    'NotifWhenStatusOfThemeChange' => 'Obavesti me kada se promeni status teme',
    'CostController' => 'Kontrolor troška',
    'CanNotDeleteBecauseThisMemberIsAlreadyMemberOfBillOfQuantity' => 'Ne možete da obrišete ovog člana tima teme jer je on ujedno i član tima na predmeru "{bill_of_quantity_name}"',
    'CanNotDeleteBecauseThisMemberIsAlreadyMemberOfBuildingConstruction' => 'Ne možete da obrišete ovog člana tima teme jer je on ujedno i član tima na izvođenju "{building_construction_name}"'
];