<?php

return [
    'MainMenuFavoriteItems' => 'Omiljene stavke',
    'MainMenuFavoriteItem' => 'Omiljena stavka',
    'Label' => 'Labela',
    'Action' => 'Akcija',
    'ComponentName' => 'Naziv komponente',
    'User' => 'Korisnik',
    'ActionOrComponentNameMustBeSet' => 'Akcija ili komponenta moraju biti postavljeni',
    'IntegrityError' => 'Došlo je do greške prilikom čuvanja redosleda tabova. Pokušajte ponovo.'
];