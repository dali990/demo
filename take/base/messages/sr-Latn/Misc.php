<?php

return [
    'ProblemParsingBoolVar' => 'Problem u obradi boolean vrednosti',
    'TakeDataFromCodeBook' => 'Preuzmi podatke iz šifarnika',
    'Others' => 'Ostalo',
    'ModelChooseGuitableChooseItems' => 'Stavke koje birate duplim klikom na red u tabeli',
    'ModelChooseGuitableItemsThatYouChoose' => 'Stavke koje ste izabrali. Skidate ih iz spiska izabranih duplim klikom na red u tabeli',
    'UndefinedMemoryUnit' => 'Memorijska jedinica nije definisana'
];