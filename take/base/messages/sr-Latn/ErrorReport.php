<?php

return [
    'ErrorReport' => 'Prijavljena greška',
    'ErrorReportedInSystem' => 'Prijavljena je greška u sistemu',
    'ReportingTime' => 'Vreme prijavljivanja',
    'Message' => 'Poruka',
    'MessageStripTags' => 'Poruka(bez html tagova)',
    'Action' => 'Akcija',
    'Description' => 'Opis',
    'GetParams' => 'Get parametri',
    'PostParams' => 'Post parametri',
    'Submitter' => 'Prijavio',
    'Fixed' => 'Popravljeno',
    'ErrorSent' => 'Poslata',
    'Yes' => 'Jeste',
    'No' => 'Nije',
    'ErrorSend' => 'Pošalji'
];

