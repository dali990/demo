<?php

return [
    'GetHTMLContentNotDefined' => 'Funkcija getHTMLContent() nije definisana u klasi {class_name}',
    'GetXMLContentNotDefined' => 'Funkcija getXMLContent() nije definisana u klasi {class_name}'
];