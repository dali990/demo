<?php

return [
    'Notfication' => 'Obaveštenje',
    'Notfications' => 'Obaveštenja',
    'MarkAllAsRead' => 'Obeležite sve kao pročitano',
    'AllNotfications' => 'Sva obaveštenja',
    'UnreadNotfications' => 'Nepročitana obaveštenja',
    'LoadMore' => 'Učitajte još',
    'SeeAll' => 'Vidite sve',
    'MarkAsRead' => 'Označite kao pročitano',
    'AdditionalInformation' => 'Dodatne informacije'
];