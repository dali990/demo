<?php

return [
    'checkResolveFuncNotFound' => 'Za upozorenje <b>{warn_code}</b> po modelu <b>{model_name}</b> nije zadata <b>checkResolveFunc</b> funkcija.',
    'resolveFuncNotFound' => 'Za upozorenje <b>{warn_code}</b> po modelu <b>{model_name}</b> nije zadata <b>resolveFunc</b> funkcija.',
    'checkResolveFuncIncorrectResponse' => 'Funkcija <b>checkResolveFunc</b> je vratila nepravilan odgovor. <b>Status</b> nije postavljen.',
    'warnForModelWrongFormat' => 'Nepravilno zadata <b>forModel</b> vrednost za upozorenje <b>{warn_code}</b>. Polja model i countFunc su obavezna prilikom zadavanja.',
    'WarnsByUsersOverview' => 'Pregled upozorenja korisnika'
];

