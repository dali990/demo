<?php

return [
    'Timestamp' => 'Vreme',
    'Done' => 'Odradjeno',
    'AlreadyExistsUndone' => 'Postoji neizvrsen zahtev za azuriranjem',
    'STATUS_DONE' => 'Izvrseno',
    'STATUS_PROGRESS' => 'U toku',
    'STATUS_WAITING' => 'U cekanju'
];
