<?php

return array(
    'rangeStartTitle' => 'Početak',
    'rangeEndTitle' => 'Kraj',
    'nextLinkText' => 'Sledeći',
    'prevLinkText' => 'Prethodni',
    'doneButtonText' => 'Završeno',
    'specificDate' => 'Specifičan datum',
    'allDatesBefore' => 'Svi dani pre',
    'allDatesAfter' => 'Svi dani posle',
    'dateRange' => 'Opseg datuma',
    'presetRanges_text1' => 'Danas',    
    'presetRanges_text2' => 'Poslednjih 7 dana',
    'presetRanges_text3' => 'Od početka meseca',
    'presetRanges_text4' => 'Od početka godine',
    'presetRanges_text5' => 'Prethodni mesec',
)

?>

