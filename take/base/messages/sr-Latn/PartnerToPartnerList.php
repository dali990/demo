<?php

return [
    'PartnerToPartnerList' => 'Veza partner-lista partnera',
    'PartnersToPartnerLists' => 'Veza partneri-liste partnera',
    'Partner' => 'Partner',
    'PartnerList' => 'Lista partnera'
];