<?php

return [
    'YearShort' => 'g',
    'MonthShort' => 'm',
    'DayShort' => 'd',
    'HourShort' => 'č',
    'MinuteShort' => 'min',
    'SecondShort' => 'sec',
    'WordBefore' => 'pre',
    'WordFor' => 'za',
];