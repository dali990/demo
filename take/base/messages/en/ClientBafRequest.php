<?php

return [
    'PriorityLow' => 'Low - if ever gets time',
    'PriorityMiddle' => 'Middle - it may be useful',
    'PriorityHigh' => 'High - do it as soon as possible',
    'PriorityError' => 'Error - the system crashes, causing problems',
    'Name' => 'Name',
    'OnRequest' => 'On request',
    'CreateTime' => 'Create time',
    'SendTime' => 'Send time',
    'Description' => 'Description',
    'Sent' => 'Sent',
    'Priority' => 'Priority',
    'Send' => 'Send',
    'ClientBafRequest' => 'Baf request',
    'Yes' => 'Yes',
    'No' => 'No',
    'UnsendClientBafRequests' => 'Unsend client baf request',
    'UnsendClientBafRequestsCount' => 'Unsend client baf request count',
];

