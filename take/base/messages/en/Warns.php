<?php

return [
    'checkResolveFuncNotFound' => 'For warning <b>{warn_code}</b> by model <b>{model_name}</b> function <b>checkResolveFunc</b> not set.',
    'resolveFuncNotFound' => 'For warning <b>{warn_code}</b> by model <b>{model_name}</b> function <b>resolveFunc</b> not set.',
    'checkResolveFuncIncorrectResponse' => 'Function <b>checkResolveFunc</b> returned incorrect response. Must be set <b>status</b>.',
    'warnForModelWrongFormat' => 'Wrong format for <b>forModel</b> value in warning <b>{warn_code}</b>. Fields model and countFunc are required.',
    'WarnsByUsersOverview' => 'Overview of users warnings'
];

