<?php

return [
    'MainMenuFavoriteGroups' => 'Groups',
    'MainMenuFavoriteGroup' => 'Group',
    'GroupName' => 'Group name',
    'User' => 'User',
    'IsDefault' => 'Is group default',
    'Default' => 'Workspace',
    'CanNotDeleteDefaultGroup' => 'Can not delete default group'
];