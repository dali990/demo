<?php

return [
    'GetHTMLContentNotDefined' => 'Function getHTMLContent() not defined in class {class_name}',
    'GetXMLContentNotDefined' => 'Function getXMLContent() not defined in class {class_name}'
];