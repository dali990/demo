<?php

return array(
    'ThemesOverview' => 'Themes overview',
    'TabFiles' => 'Files',
    'TabMeetings' => 'Meetings',
    'TabTasks' => 'Tasks',
    'TabComments' => 'Comments',
    'TabRWMCCorrespondences' => 'Correspondences',
    'ParentTheme' => 'Part of',
    'ToPlaneTheme' => 'To plain theme',
    'ToJob' => 'To job',
    'ToJobBid' => 'To job bid',
    'ToQualificationBid' => 'To qualification bid',
    'PersonToThemeUniq' => 'There is already a pair {person_name} and {theme_name}',
    'NotifUserAddedYouAsCoordinator' => 'User {added_by_user} added you as coordinator on the theme "{theme_name}"',
    'NotifUserRemovedYouAsCoordinator' => 'User {removed_by_user} removed you as coordinator on the theme "{theme_name}"',
    'Settings' => 'Settings',
    'ThemeSettings' => 'Theme settings',
    'UsersHasTasksAndAreNotInTheTeam' => 'Users has tasks on this theme and are not in the team',
    'Plain' => 'Plain',
    'Job' => 'Job',
    'Bid' => 'Bid',
    'Active' => 'Active',
    'NotActive' => 'Not active',
    'Activities' => 'Activities',
    'ActivitiesTable' => 'Activities Table',
    'UserChangedStatusForTheme' => '{username} changed status for theme {theme}'
);