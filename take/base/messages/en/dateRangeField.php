<?php

return array(
    'rangeStartTitle' => 'Start date',
    'rangeEndTitle' => 'End date',
    'nextLinkText' => 'Next',
    'prevLinkText' => 'Prev',
    'doneButtonText' => 'Done',
    'specificDate' => 'Specific Date',
    'allDatesBefore' => 'All Dates Before',
    'allDatesAfter' => 'All Dates After',
    'dateRange' => 'Date Range',
    'presetRanges_text1' => 'Today',    
    'presetRanges_text2' => 'Last 7 days',
    'presetRanges_text3' => 'Month to date',
    'presetRanges_text4' => 'Year to date',
    'presetRanges_text5' => 'The previous Month',
)

?>

