<?php

return [
    'Session' => 'Session',
    'MySessions' => 'My sessions',
    'Start' => 'Session start',
    'End'=>'Session end',
    'Server'=>'Server',
    'UserAddress'=>'User address',
    'SessionType'=>'Session type',
    'FileDiskId'=>'FileDisk ID',
    'StartingDirectoryDefinition'=>'Starting directory definition',
    'Pseudonim'=>'Pseudonim',
    'LastSeen'=>'Last seen',
    'IdleSince'=>'Idle since',
    'IsIdle' => 'Is idle',
    'Idle' => 'Idle',
    'LicenceType'=>'Licence type',
    'Data'=>'Data',
    'Active' => 'Active',
    'LogOutSession' => 'Logout',
    'Inactive' => 'Inactive',
    'ActiveSessions' => 'Active',
    'IdleSessions' => 'Idle',
    'InactiveSessions' => 'Inactive',
    'OperatingSystem' => 'Operating system',
    'Browser' => 'Browser',
    'BrowserVersion' => 'Browser version',
    'BrowserString' => 'Browser string',
    'WidthSimaResolution' => 'Width SIMA resolution',
    'HeightSimaResolution' => 'Height SIMA resolution',
    'Zoom' => 'Zoom'
];

