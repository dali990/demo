<?php

return [
    'UserToPartnerList' => 'User to partner list',
    'UsersToPartnerLists' => 'Users to partner lists',
    'User' => 'User',
    'PartnerList' => 'Partner list',
    'PartnerListGroups' => 'Partner list groups'
];