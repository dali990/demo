<?php

/* 
 * Trivijalni prevodi
 */

return array(
    'DisplayName' => 'Display name',
    'Belongs' => 'Belongs(job/bid/...)',
    'Name' => 'Name',
    'Status' => 'Status',
    'Unknown' => 'Unknown',
    'Description' => 'Description',
    'Active' => 'Active',
    'Add' => 'Add',
    'Time' => 'Time',
    'DoNotHave' => 'do not have',
    'IsNot' => 'is not',
    'Settings' => 'Settings',
    'Comment' => 'Comment',
    'Address' => 'Address',
    'Contact' => 'Contact',
    'Total' => 'Total',
    'ConvertTo' => 'Convert to',
    'Overview' => 'Overview',
    'YouDoNotHaveAccess' => 'You do not have access',
    'NotIn' => 'Not in',
    'CommentsCount' => 'Comments count',
    'CommentThreadsCount' => 'Comment threads count',
    'MoreDetails' => 'More details',
    'Edit' => 'Edit',
    'Delete' => 'Delete',
    'DoNotHaveAccessToInformations' => 'You do not have the right to access the requested informations.',
    'City' => 'Settlement',
    'Cities' => 'Settlements',
    'Recycle' => 'Recycle',
    'Refresh' => 'Refresh',
    'Copy' => 'Copy',
    'OpenInDialog' => 'Open in dialog',
    'OpenInMainTab' => 'Open in main tab'
);
