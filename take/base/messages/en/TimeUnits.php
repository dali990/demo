<?php

return [
    'YearShort' => 'y',
    'MonthShort' => 'm',
    'DayShort' => 'd',
    'HourShort' => 'h',
    'MinuteShort' => 'min',
    'SecondShort' => 'sec',
    'WordBefore' => 'before',
    'WordFor' => 'in',
];