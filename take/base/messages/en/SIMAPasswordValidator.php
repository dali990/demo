<?php

return [
    'PasswordStrengthIsWeak' => 'Password strength is weak',
    'WrongPasswordLength' => 'Length must be minimum {min_length} characters.',
    'PasswordMustHaveLowercaseLetters' => 'Must contain at least one lowercase letter.',
    'PasswordMustHaveUppercaseLetters' => 'Must contain at least one uppercase letter.',
    'PasswordMustHaveNumbers' => 'Must contain at least one number.',
    'PasswordMustHaveSpecialCharacters' => 'Must have at least one special character from a list of allowed: {allowed_special_characters}.',
];