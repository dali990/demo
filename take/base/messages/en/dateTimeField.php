<?php

return array(
    'timeOnlyTitle'=> 'Choose Time',
    'timeText'=> 'Time',
    'hourText'=> 'Hour',
    'minuteText'=> 'Minute',
    'secondText'=> 'Second',
    'millisecText'=> 'Millisecond',
    'timezoneText'=> 'Time zone',
    'currentText'=> 'Today',
    'closeText'=> 'Close',
    'firstDay' => '1'  
)

?>

