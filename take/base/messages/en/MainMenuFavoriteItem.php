<?php

return [
    'MainMenuFavoriteItems' => 'Favorite items',
    'MainMenuFavoriteItem' => 'Favorite item',
    'Label' => 'Label',
    'Action' => 'Action',
    'ComponentName' => 'Component name',
    'User' => 'User',
    'ActionOrComponentNameMustBeSet' => 'Action or component must be set',
    'IntegrityError' => 'An error occurred while saving tabs order. Please try again.'
];