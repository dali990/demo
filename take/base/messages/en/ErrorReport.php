<?php

return [
    'ErrorReport' => 'Error report',
    'ErrorReportedInSystem' => 'Error reported in the system',
    'ReportingTime' => 'Reporting time',
    'Message' => 'Message',
    'MessageStripTags' => 'Message(striped tags)',
    'Action' => 'Action',
    'Description' => 'Description',
    'GetParams' => 'Get params',
    'PostParams' => 'Post params',
    'Submitter' => 'Submitter',
    'Fixed' => 'Fixed',
    'ErrorSent' => 'Sent',
    'Yes' => 'Yes',
    'No' => 'No',
    'ErrorSend' => 'Send'
];

