<?php

return [
    'CurrentPage' => 'Current page',
    'FirstPage' => 'First page',
    'PreviousPage' => 'Previous page',
    'NextPage' => 'Next page',
    'LastPage' => 'Last page',
    'TotalNumberOfPages' => 'Total number of pages'
];