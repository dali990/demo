<?php

return [
    'RoleInTeam' => 'Role in team',
    'NotifPersonInTeam' => 'You have become part of the team on the theme "{theme_name}"',
    'NotifPersonOutOfTeam' => 'You are no longer a part of the team on the theme "{theme_name}"',
    'NotifWhenStatusOfThemeChange' => 'Notify me when the status of the theme changes',
    'CostController' => 'Cost controller',
    'CanNotDeleteBecauseThisMemberIsAlreadyMemberOfBillOfQuantity' => 'You cannot delete this team member because he is also a team member at bill of quantity "{bill_of_quantity_name}"',
    'CanNotDeleteBecauseThisMemberIsAlreadyMemberOfBuildingConstruction' => 'You cannot delete this team member because he is also a team member at building construction "{building_construction_name}"'
];