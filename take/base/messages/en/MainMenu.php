<?php

return [
    'AddressBook' => 'Address book',
    'MyTeam' => 'My team',
    'AddNewClientBafRequest' => 'Add new request',
    'SIMADocumentation' => 'SIMA documentation',
    'PersonalDocumentation' => 'Personal documentation',
    'ProfileUserConfiguration' => 'User configuration',
    'ProfileTasks' => 'Tasks',
    'ProfileActivities' => 'Activities',
    'Logout' => 'Logout',
    'ToggleLeftBar' => 'Toggle menu',
    'AddFavoriteItemGroup' => 'Add group',
    'ToggleItemFromFavorites' => 'Toggle item from favorites',
    'RemoveOpenedItem' => 'Remove item from opened',
    'ReloadItemAction' => 'Reload',
    'SetFavoriteGroupToOpened' => 'Add a group in opened items',
    'Email' => 'Email'
];