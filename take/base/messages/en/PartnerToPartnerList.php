<?php

return [
    'PartnerToPartnerList' => 'Partner to partner list',
    'PartnersToPartnerLists' => 'Partners to partner lists',
    'Partner' => 'Partner',
    'PartnerList' => 'Partner list'
];