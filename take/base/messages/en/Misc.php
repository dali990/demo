<?php

return [
    'ModelChooseGuitableChooseItems' => 'Choose items by double clicking on the row in the table',
    'ModelChooseGuitableItemsThatYouChoose' => 'The items you choose. Remove item from the list by double clicking on the row in the table',
    'UndefinedMemoryUnit' => 'Undefined memory unit'
];

