<?php

return [
    'ValidationDidnotPassedForNextModels' => 'Validation did not pass for next models',
    'DontHavePrivilegeForThisConfirmation' => 'You do not have privilege for this confirmation',
    'DontHavePrivilegeForThisRevert' => 'You do not have privilege for this revert',
    'DontHavePermissionForThisConfirmation' => 'Do not have permission for this confirmation - code 1',
    'DontHavePermissionForThisRevert' => 'Do not have permission for this revert - code 1',
    'ThisConfirmationCanNotBeSigned' => 'This confirmation can not be signed',
    'ThisConfirmationCanNotBeReverted' => 'This confirmation can not be reverted'
];

