<?php

return [
    'Notfication' => 'Notfication',
    'Notfications' => 'Notfications',
    'MarkAllAsRead' => 'Mark all as read',
    'AllNotfications' => 'All notfications',
    'UnreadNotfications' => 'Unread notfications',
    'LoadMore' => 'Load more',
    'SeeAll' => 'See all',
    'MarkAsRead' => 'Mark as read',
    'AdditionalInformation' => 'Additional information'
];