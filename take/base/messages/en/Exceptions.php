<?php

return [
    'WorkDayTypeConfNotSet' => 'Configuration for work day type not set.</br>Please contact administrator',
    'WekendDayTypeConfNotSet' => 'Configuration for weekend day type not set.</br>Please contact administrator',
    'SIMAExceptionFunctionWrongParamsMessage' => 'Next parameters not passed to function {function_name}: {params}',
    'SIMAModelViewNotFound'=>"Model '{model_name}' doesn't have registered view '{view_name}', or view file doesn't exist",
    'SIMAFunctionWrongParamsExceptionMessage' => 'Next parameters not passed to function {function_name}: {params}',
    'WarnModelNotFound' => "Required <b>\"{model_label}\"</b> was not found in the database."
];

