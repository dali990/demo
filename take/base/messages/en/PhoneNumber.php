<?php

return [
    'PhoneNumbers'   => 'Phones',
    'PhoneNumber'    => 'Phone',
    'Country'        => 'Country',
    'ProviderNumber' => 'Area Code',
    'Number'         => 'Number',
    'PhoneNumberAlreadyExist' => 'The phone you entered already exists',
    'PhoneNumberType' => 'Phone number type',
    'PhoneNumberTypes' => 'Phone number types',
    'AddPhoneNumberTypesOperation' => 'Adding phone number types',
    'Partner' => 'Partner'
];

