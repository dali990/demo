<script type="text/x-template" id="public-link-form-email-addresses-list">
    <div class="public-link-form-email-addresses-list">
        <!-- Lupica -->
        <span class="sima-icon _search_form _16" @click="openModelChooser"></span>
        <!-- Izabrane stavke -->
        <div class="selected-items">
            <div class="item" v-for="item in selected_items">
                <span class="sima-icon _delete _16" title="<?=Yii::t('BaseModule.Common', 'Delete')?>" @click="removeItem(item.id)"></span> 
                {{item.display_name}} 
                <input type="checkbox" :value="item.id" v-model="notify_emails" :title="'<?=Yii::t('FilesModule.PublicLink', 'Notify')?>'">
            </div>
        </div>
        <input type="hidden" :name="input_filed_name" v-bind:value="data_string"/>
    </div>
</script>