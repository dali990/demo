/* global Vue, sima */

Vue.component('public-link-form-email-addresses-list', {
    template: '#public-link-form-email-addresses-list',
    props:  {
        field_value: {type: String, default: null}
    },
    computed: {
        
    },
    watch: {
        notify_emails(new_val, old_val){
            this.selected_items.map( (email) => {
                if(this.notify_emails.includes(email.id))
                {
                    email.notify = true;
                }
                else if(email.notify)
                {
                    email.notify = false;
                }
            });
            this.data_string = JSON.stringify(this.selected_items);
        },
        selected_items(new_val, old_val) {
            this.data_string = JSON.stringify(new_val);
        }
    },
    data: function () {
        return {
            model_name: 'EmailAddress',
            input_filed_name: 'PublicLink[email_addresses_list]',
            columns_type: 'from_public_link',
            selected_items: [],
            notify_emails: [],
            data_string: ""
        };
    },
    mounted: function(){
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
        if(this.field_value)
        {
            this.selected_items = JSON.parse(this.field_value);
            this.selected_items.map( (email) => {
                if(email.notify && !this.notify_emails.includes(email.id))
                {
                    this.notify_emails.push(email.id);
                }
            });
        }
    },
    methods: {
        openModelChooser(){
            var _this = this;
            var params = {};               
            params.view = "guiTable";
            params.multiselect = true;
            params.params = {columns_type: this.columns_type};
            params.ids = this.selected_items;

            sima.model.choose(this.model_name, function(data){
                if(!_this.selected_items.find(item => item.id === data.id))
                {
                    _this.selected_items = data;
                }
            }, params);   
        },
        removeItem(id){
            this.selected_items = this.selected_items.filter( item => item.id !== id );
            this.notify_emails = this.notify_emails.filter( item => item.id !== id );
        },
        emptyField(){
            this.selected_items = [];
            this.notify_emails = [];
            this.data_string = "";
        }
    }
    
});
