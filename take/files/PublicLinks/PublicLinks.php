<script type="text/x-template" id="public-links">
    <div class="public-links">
        <div class='sima-layout-fixed-panel'>
            <h3 style="display:inline-block"><?=Yii::t('FilesModule.PublicLink', 'PublicLinks');?></h3> 
            <sima-button v-on:click="addLink" title="<?=Yii::t('FilesModule.PublicLink', 'AddPublicLink')?>">
                <i class="fas fa-plus"></i>
            </sima-button>
        </div>
        <div class='sima-layout-panel'>       
            <table class="public-links-table" ref="publicLinksTable" v-observe-visibility="componentInstanceVisibilityChanged">
                <thead>
                    <tr>
                        <th style="width: 30px"></th>
                        <th style="width: 140px"><?=Yii::t('FilesModule.PublicLink', 'TotalDownloads')?></th>
                        <th><?=Yii::t('FilesModule.PublicLink', 'Email')?></th>
                        <th><?=Yii::t('FilesModule.PublicLink', 'Partner')?></th>
                        <th><?=Yii::t('FilesModule.PublicLink', 'Notification')?></th>
                        <th><?=Yii::t('FilesModule.PublicLink', 'Created')?></th>
                        <th><?=Yii::t('FilesModule.PublicLink', 'ExpirationTime')?></th>
                        <th><?=Yii::t('FilesModule.PublicLink', 'CreatedBy')?></th>
                    </tr>
                </thead>
                <tbody class="pl-table-body" v-for="(item, index) in model.public_links" :key="item.id">                                   
                    <tr  class="pl-table-row">
                        <!-- Expand row-->
                        <td>
                            <div class="arrow-column">
                                <div class="angle-right" v-bind:class="{'down': expanded_rows.includes(index)}" @click.stop.prevent="expandeRow(index, $event)">
                                    <i class="fas fa-angle-right"></i>
                                </div>
                            </div>
                        </td>
                        <!-- TotalDownloads-->
                        <td>
                            <div class="counter" v-bind:class="{'counter-zero':!item.public_link_to_downloads_cnt}"
                                v-bind:title="!item.public_link_to_downloads_cnt ? '<?=Yii::t('FilesModule.PublicLink', 'FileNotDownloaded')?>' : ''">
                            {{item.public_link_to_downloads_cnt}}
                            </div>
                            <div class="options">
                                <sima-model-options
                                    v-bind:model="item"
                                ></sima-model-options>
                            </div>
                        </td>
                        <!-- Email-->
                        <td :colspan="item.sending_option === '<?=PublicLink::$SENDING_OPTION_EMAIL?>' ? 1 : 2 "
                            v-bind:class="{'link-cell': item.sending_option === '<?=PublicLink::$SENDING_OPTION_LINK?>'}">
                            <span v-if="item.sending_option === '<?=PublicLink::$SENDING_OPTION_EMAIL?>'" v-html="item.email_address.email"></span>
                            <span v-else>
                                <?=Yii::t('FilesModule.PublicLink', 'ThisIsLink')?> -> 
                                <a target="_blank" v-bind:href="item.getViewLink"><?=Yii::t('FilesModule.PublicLink', 'Link')?></a>
                                <sima-button v-on:click="copyLink(item.getViewLink)" :title="'<?=Yii::t('BaseModule.Common', 'Copy')?>'"><i class="far fa-copy"></i></sima-button>
                            </span>
                        </td>
                        <!-- Partner-->
                        <td v-if="item.sending_option === '<?=PublicLink::$SENDING_OPTION_EMAIL?>'">
                            <span v-if="item.email_address.owner" v-html="item.email_address.owner.DisplayHtml"></span>
                        </td>
                        <!-- Notification-->
                        <td>
                            <input type="checkbox" :checked="item.notify" @click="setNotification(item, $event)" :key="item.id" :title="'<?=Yii::t('FilesModule.PublicLink', 'Notify')?>'">
                        </td>
                        <!-- Created-->
                        <td><span :title="item.created_time">{{item.created_time | formatDate('', true)}}</span></td>
                        <!-- ExpirationTime-->
                        <td>
                            <span v-if="item.time_limit" :title="item.time_limit">{{item.time_limit | formatDate('', true)}}</span>
                            <span v-else><?=Yii::t('FilesModule.PublicLink', 'Unlimited')?></span>
                        </td>
                        <!-- CreatedBy-->
                        <td class="created-by" v-if="item.created_by">
                            <div class="profile">
                                <sima-model-display-icon 
                                    v-bind:model="item.created_by.person"
                                    class="image"
                                >
                                </sima-model-display-icon>
                            </div>
                            <div class="user-desc">
                                <span>                        
                                    <sima-model-display-html v-bind:model="item.created_by"></sima-model-display-html>
                                </span>
                                <span class="light work-position" 
                                    v-if="item.created_by.person && item.created_by.person.work_position_names"
                                    v-bind:style="{ 'width': '100%'}" 
                                    v-html = "item.created_by.person.work_position_names"  
                                    v-bind:title="item.created_by.person.work_position_names" >
                                </span>
                            </div>
                        </td>
                    </tr>
                    <!-- Expanded row -->
                    <tr class="expanded-row">
                        <td colspan="8" v-bind:class="{'expanded-cell': expanded_rows.includes(index)}">
                            <div class="expanded-row-wrapper hidden" v-bind:class="{'show': expanded_rows.includes(index)}">
                                <div class="expanded-cell-content">
                                    <div v-for="(pl_download, index) in item.public_link_to_downloads">
                                        <div>
                                            {{++index}} 
                                            <b><?=Yii::t('FilesModule.PublicLink', 'DownloadTime')?>:</b> 
                                            <span :title="pl_download.download_time">{{pl_download.download_time | formatDate('', true)}}</span> 
                                            <b><?=Yii::t('FilesModule.PublicLink', 'IPAddress')?>:</b> 
                                            {{pl_download.visitor_ip_address}}
                                        </div>
                                    </div>
                                    <p v-if="!item.public_link_to_downloads.length"><?=Yii::t('FilesModule.PublicLink', 'FileNotDownloaded')?></p>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</script>