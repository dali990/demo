/* global Vue, sima */

Vue.component('public-links', {
    template: '#public-links',
    mixins: [sima.vue.getBaseComponent()],
    props:  {
        type_id: {required: true},
        type: {required: true, type: String}
    },
    computed: {
        model(){
            return sima.vue.shot.getters.model(this.type+"_"+this.type_id);
        },
        dataTrue(){
            return this.model.shotStatus('public_links') === 'OK';
        }
    },
    watch: {
    },
    data: function () {
        return {
            pl_table: null,
            expanded_rows: [],
            timeout: null,
            current_user_id: sima.getCurrentUserId()
        };
    },
    mounted: function(){
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
        this.pl_table = this.$refs.publicLinksTable;
    },
    methods: {
        copyLink(view_link){
            sima.misc.copyToClipboard(view_link);
        },
        editLink(id){
            sima.model.form('PublicLink', id);           
        },
        addLink(type){
            sima.model.form('PublicLink', '', {
                init_data: {
                    PublicLink: {
                        type: this.type,
                        type_id: this.type_id
                    }
                }
            });      
        },
        removeLink(id, downloads_cnt){
            if(downloads_cnt > 0)
            {
                sima.dialog.openWarn(sima.translate('PublicLinkCanNotRemoveIfDownloaded'), sima.translate('PublicLink'));
            }
            else
            {
                sima.model.remove('PublicLink', id);
            }
        },
        setNotification(public_link, e){
            var _this = this;
            e.preventDefault();
            e.target.classList.add('_disabled');
            
            var globalTimeout = this.timeout;
            if (globalTimeout !== null) {
                clearTimeout(globalTimeout);
                this.timeout = globalTimeout;
            }
            this.timeout = setTimeout(function(){
                sima.ajax.get('files/file/publicLinkSetEmailDownloadNotification', {
                    get_params:{
                        public_link_id: public_link.id,
                        notify: !public_link.notify
                    },
                    async:true,
                    //loadingCircle: $(_this.$el),
                    success_function:function(response){
                            e.target.classList.remove('_disabled');
                    }
                });
            },400);
        },
        expandeRow(index, e) {
            var row = e.target.closest(".pl-table-row");
            var arrow = e.target.closest(".angle-right");
            
            if (
                    !arrow.classList.contains("down") &&
                    !arrow.classList.contains("right")
                ) 
            {
                arrow.classList.add("down");
                row.classList.add("selected-row-is-expanded");
                this.expanded_rows.push(index);
            } 
            else 
            {
                if (arrow.classList.contains("down")) 
                {
                    row.classList.remove("selected-row-is-expanded");
                    this.expanded_rows = this.expanded_rows.filter( value => value !== index );
                    arrow.classList.remove("down");
                    arrow.classList.add("right");
                } 
                else 
                {
                    row.classList.add("selected-row-is-expanded");
                    this.expanded_rows.push(index);
                    arrow.classList.remove("right");
                    arrow.classList.add("down");
                }
            }
        }
    }
});
