<?php
// City
class gui {
    public function modelForms()
    {
        $owner = $this->owner;

        if (empty($owner->country_id))
        {
            $system_country_id = Yii::app()->configManager->get('base.system_country_id', false);
            if (!empty($system_country_id))
            {
                $owner->country_id = $system_country_id;
            }
        }

        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'name'=>'textField',
                    'area_code'=>'textField',
                    'postal_codes_lists'=>array('list',
                        'model'=>'PostalCode',
                        'relName'=>'postal_codes', 
                        'multiselect'=>true,
                        'default_item'=> [
                            'model'=>'City',
                            'column'=>'default_postal_code_id'
                        ]
                    ),
                    'country_id'=>array('searchField','relName'=>'country', 'add_button'=>true),
                    'bzr_code' => 'textField',
                    'municipality_id' => [
                        'relation', 'relName' => 'municipality', 'add_button'=>true,
                        'dependent_on' => [
                            'country_id' => [
                                'onValue' => [
                                    'enable',
                                    ['condition', 'model_filter' => 'country.ids']
                                ],
                                'onEmpty' => [
                                    'disable'
                                ]
                            ]
                        ]
                    ],
                    'work_position_id' => ['searchField', 'relName' => 'work_position', 'filters' => ['scopes' => 'withoutIncreasedRisk']],
                )
            )
        );
        
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'name' => 'textField',
                    'area_code' => 'textField',
                    'postal_codes_list'=>['list',
                        'model'=>'CodebookPostalCode',
                        'relName'=>'postal_codes', 
                        'multiselect'=>true,
                        'default_item'=> [
                            'model'=>'CodebookCity',
                            'column'=>'default_postal_code_id'
                        ]
                    ],
                    'country_id' => ['relation', 'relName' => 'country', 'add_button'=>true],
                    'bzr_code' => 'textField',
                    'treasury_office_unit_id' => [
                        'relation', 
                        'relName' => 'treasury_office_unit', 
                        'add_button' => [
                            'filter' => [
                                'filter_scopes'=>[
                                    'onlyMunicipalitiesOrSettlements'
                                ]
                            ],
                            'params' => [
                                'columns_type' => 'period_name_only', //SafeEvd6GUI
                                'add_button' => [
                                    'init_data' => [
                                        'CodebookCountry' => [
                                            //'disabled',
                                            'type' => ''
                                        ]
                                    ],
                                    'formName' => 'period_name_only', 
                                    'scenario' => 'period_name_only'
                                ]
                            ]
                        ],
                        'dependent_on' => [
                            'country_id' => [
                                'onValue' => [
                                    'enable',
                                    ['condition', 'model_filter' => 'country.ids']
                                ],
                                'onEmpty' => [
                                    'disable'
                                ]
                            ]
                        ],
                        'filters' => [
                            'filter_scopes'=>[
                                'onlyMunicipalitiesOrSettlements'
                            ]
                        ]
                    ],
                ],
            ],
        ];
    }

}

