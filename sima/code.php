<?php

$alias = CodebookCity::model()->getTableAlias();
$condition_bzr_code = "$alias.bzr_code = '{$item['city_bzr_code']}'";
$condition_city_name = "$alias.name = '{$item['city_name']}'";
$condition_city_name_latin = "$alias.name = '{$item['city_name']}' OR $alias.name = '{$item['city_name_latin']}'";
$criteria = new CDbCriteria();
$criteria->condition = $condition;
$codebook_city = CodebookCity::model()->findAll($criteria);

$codebook_city = CodebookCity::model()->find([
    'model_filter' => [
        'OR',
        [
            'bzr_code' => $item['city_bzr_code'],
        ],
        [
            'name' => $item['city_name'],
        ],
        'filter_scopes' => [
            'withPeriod' => [$date]
        ]
    ]
]);


public function withPeriod($data)
{
    $alias = $this->getTableAlias();            
    $criteria = new CDbCriteria();
    $criteria->select = "
        $alias.date_check_theoretical_training + (training_interval_period.years || ' year')::interval + (training_interval_period.months || ' month')::interval + (training_interval_period.days || ' day')::interval
        AS date_check_theoretical_training_interval, 
        $alias.date_check_practical_training + (training_interval_period.years || ' year')::interval + (training_interval_period.months || ' month')::interval + (training_interval_period.days || ' day')::interval
        AS date_check_practical_training_interval  
    ";
    $criteria->with = ['training_interval_period'];
    $this->getDbCriteria()->mergeWith($criteria);
    return $this;
}

public static function getBeginDateRange($begin_date, $end_date, $user_id)
{      
    return Activity::model()->findAll([
        'model_filter' => [
            'AND',
            [
                'OR',
                [
                    'coordinator' => [
                        'ids' => $user_id
                    ]
                ],
                [
                    'theme' => [
                        'coordinator' => [
                            'ids' => $user_id
                        ]
                    ]
                ]
            ],
            [
                'begin_date' => $begin_date . '<>' . $end_date
            ]                             
        ]
    ]);
}


<!--            <sima-popup
                v-if="has_params_info"
                v-bind:show="popup_additional_info"
                v-bind:position="'right'"
                class="popup-additional-info"
            >
                <div v-html="params.info"></div>
            </sima-popup> -->
        
        isInViewPort: function (elem) {
            var bounding = elem.getBoundingClientRect();
            return (
                bounding.top >= 0 &&
                bounding.left >= 0 &&
                bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        }
        
        //BillOfQuantityController::actionSaveGroupItems() LINE:197 umesto foreach ($group->bill_of_quantity->leaf_objects as $leaf_object)
        foreach ($group->bill_of_quantity->objects as $object)
        {
            $group->calcValueRecursiveForObject($object);
            gc_collect_cycles();
            data_log(memory_get_usage(), 'MEMORY');
        }
