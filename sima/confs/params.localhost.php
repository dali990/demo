<?php

//defined('GLOBAL_SIMA_DBCRITERIA_OPTIMIZATION') or define('GLOBAL_SIMA_DBCRITERIA_OPTIMIZATION',true);

return [
    'file_repositories'=>"/mnt/repo_sima/",
    'temp_file_repository'=>'/var/www/html/sima2/protected/runtime/temp',
    'temp_file_repository_qrcache'=>'/var/www/sima2/protected/runtime/temp/qrcache',
    'server_FQDN'=>'demo-sima.setpp.rs',
    'is_production' => false,
    'codebook_base' => 'http://localhost/simarecords2/',//***mergovati sa main
//    'fictive_emails' => [
//        'path' => '/home/sasa/Documents/fictive_emails', //putanja foldera gde su smesteni eml fajlovi
//        'emails' => [
//            'test@mail.com' => [
//                'INBOX' => ['email1', 'email2', 'email3'], //nazivi eml fajlova. Samo INBOX ide velikim slovima, ostali idu samo prvo veliko slovo(zato sto tako dolazi sa imapa)
//                'Sent' => ['email4', 'email5', 'email6']
//            ],
//            'test1@mail.com' => [
//                'INBOX' => ['email1', 'email2', 'email3'],
//                'Sent' => ['email4', 'email5', 'email6'],
//                'Draft' => ['email7', 'email8', 'email9']
//            ],
//        ]
//    ],
//    'use_vue_components' => true
//    'use_new_codebooks' => true
];
