<?php
return [
    'base',
    'files'=>array(
        'defaultController' => 'files',
    ),
    'fixedAssets',
    'jobs',
    'accounting'=>array(
        'modules' => array(
            'paychecks',
            'warehouse'
        ),
    ),
    'bosal',
    'legal',
    'messages',
    'admin',
    'geo',
    'HR',
    'mobile',
    'buildings',
    'setpp' => [
        'modules' => [
            'codebooks',
            'bafs',
            'testing'
        ]
    ],
    
];