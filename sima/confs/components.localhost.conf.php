<?php
return [
    'db' => [
        'connectionString' => 'pgsql:host=localhost;port=5432;dbname=sima2',//ios_start_account
        'emulatePrepare' => true,
        'username' => 'postgres',
        'password' => 'postgres',
        'charset' => 'utf8'
    ],
    'dbcodebooks' => [
        'class' => 'CDbConnection',
        'connectionString' => 'pgsql:host=localhost;port=5432;dbname=codebook',
        'emulatePrepare' => true,
        'username' => 'postgres',
        'password' => 'postgres',
        'charset' => 'utf8'
    ],
    'clientScript' => array(
        'class' => 'SIMAClientScript',
        'mergeJsExcludePattern' => '/tiny_mce|tinymce|Plumb|jquery.js|jquery-ui/',
        'mergeJs' => false,
        'compressMergedJs' => false,
        'mergeCss' => false,
        'compressMergedCss' => false,
//        'serverBaseUrl' => 'http://localhost/sima2',
//        'serverBaseUrl' => 'http://192.168.17.130/sima2',
        'serverBaseUrl' => 'http://192.168.117.6/sima2/',
    ),
    'eventRelay'=>array(
        'class' => 'application.extensions.SIMAEventRelay.SIMAEventRelay',
        'host' => 'localhost',
        'port' => '52112',
        'pidfile' => 'protected/runtime/eventRelay.pid',
        'logfile' => 'protected/runtime/eventRelay.log',
        'messages_log_file' => 'protected/runtime/eventRelay_messages.log',
//        'client_connection_string' => 'ws://192.168.17.130:52112',
        'client_connection_string' => 'ws://192.168.117.6:52112',
    ),
];