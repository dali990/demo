<?php
return [
    'params' => array_merge([
        'shared_licences' => 500,
        'server_FQDN'=>'demo-sima.setpp.rs',
        'file_repositories'=>"/var/www/html/sima2/protected/runtime",
        'temp_file_repository'=>'/var/www/html/sima2/protected/runtime/temp',
        'isUpdating' => false,
        'updatingStartTimestamp' => false,
    ], include('params.localhost.php')),
    'components' => include('components.localhost.conf.php')
];