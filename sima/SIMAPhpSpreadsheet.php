<?php
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

/*
 * PhpSpreadsheet
 */
class SIMAPhpSpreadsheet
{
    protected $params = [];
    protected $spreadsheet = null;
    protected $sheet_columns = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 
        'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
        'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ'
    ];
    protected  $column_index = 0;//index niza $sheet_columns
    protected  $last_column_index = 0;//index niza $sheet_columns
    protected  $column = 'A';
    protected  $last_column = 'A';
    protected  $row_index = 1;
    protected  $last_row_index = 0;
    protected  $calculate_formulas = true; //da li da izvrsava formule u celijama, za velike predmere moguce usporenje tokom generisanja
    public function __construct($params=[]) 
    {
        $this->params = $params;
        $this->spreadsheet = new Spreadsheet();
    }
    
    protected function getActiveSheet()
    {
        return $this->spreadsheet->getActiveSheet();
    }


    protected function getContent() 
    {
        throw new SIMAException(Yii::t('BaseModule.SIMAPhpSpreadsheet', 'SpreadsheetContentNotDefined'));
    }
    
    /**
     * Generise xlsx fajl
     * @return \TemporaryFile
     */
    public function generateFile() : TemporaryFile
    {
        $this->getContent();
        $writer = new Xlsx($this->spreadsheet);

        $resultTempFile = new TemporaryFile('', null, 'xlsx');
        $result_full_path = $resultTempFile->getFullPath();
        $resultTempFile->delete();
        //Kreira xls fajl na prosledjenoj putanji 
        $writer->setPreCalculateFormulas($this->calculate_formulas);
        $writer->save($result_full_path);   

        return $resultTempFile;
    }
    
    /**
     * Vraca naziv kolone u kojoj se trenutno nalazimo
     * @return intiger
     */
    public function getCurrentColumn() 
    {
        return $this->sheet_columns[$this->column_index];
    }
    
    /**
     * Vraca poziciju kolone u kojoj se trenutno nalazimo
     * @return string
     */
    public function getCurrentCell()
    {
        return $this->getCurrentColumn().$this->row_index;
    }
    
    public function getNextCellBottom()
    {
        return $this->getCurrentColumn().($this->row_index+1);
    }
   
    /**
     * Vraca index kolone u kojoj se nalazimo
     * @return intiger
     */
    public function getCurrentColumnIndex() 
    {
        return array_search($this->getCurrentColumn(), $this->sheet_columns);
    }
    /**
     * Vraca naziv kolone za dati index
     * @return string
     */
    public function getColumnOfIndex($index) 
    {
        return $this->sheet_columns[$index];
    }
    
    /**
     * Vraca poziciju poslednje kolone u kojoj se trenutno nalazimo
     * @return string
     */
    public function getLastCell()
    {
        return $this->last_column.$this->last_row_index;
    }
    
    /**
     * Vraca poziciju poslednje kolone u kojoj se trenutno nalazimo
     * @return string
     */
    public function getLastColumn() 
    {
        return $this->sheet_columns[$this->last_column_index];
    }

    public function getPrevCellFromCell($cell)
    {
        $column_name = $this->getColumnName($cell);
        $row = $this->getCellRow($cell);
        $prev_column_name = $this->getColumnBefore($column_name);
        return $prev_column_name.$row;
    }
    
    /**
     * Selektuje celiju u sledecoj koloni i vraca njen naziv
     * @return string
     */
    public function selectNextColumn() 
    {
        $this->column_index++;
        $this->column = $this->sheet_columns[$this->column_index];
        if($this->column_index > $this->last_column_index)
        {
            $this->last_column_index = $this->column_index;
            $this->last_column = $this->column;
        }
        return $this->getCurrentColumn();
    }
    public function selectPrevColumn() 
    {
        $this->column_index--;
        $this->column = $this->sheet_columns[$this->column_index];
        if($this->column_index > $this->last_column_index)
        {
            $this->last_column_index = $this->column_index;
            $this->last_column = $this->column;
        }
        return $this->getCurrentColumn();
    }
    /**
     * Selektuje sledecu celiju desno od selektovane i vraca njenu poziciju
     * @return string
     */
    public function selectNextCell()
    {
        return $this->selectNextColumn().$this->row_index;
    }
    public function selectPrevCell()
    {
        return $this->selectPrevColumn().$this->row_index;
    }
    /**
     * Vraca sledecu kolonu u odnosu na prosledjenu, ako kolona nije do sada koriscena vraca null
     * @param string $column
     * @return string/null
     */
    public function getColumnAfter($column) 
    {
        $column_index = array_search($column, $this->sheet_columns);
        if($column_index > $this->last_column_index)
        {
            return null;
        }
        $column_index++;
        return $this->getColumnOfIndex($column_index);
    }
    public function getColumnBefore($column) 
    {
        $column_index = array_search($column, $this->sheet_columns);
        if($column_index > $this->last_column_index)
        {
            return null;
        }
        return $this->getColumnOfIndex(($column_index-1));
    }
    /**
     * Vraca sledecu celiju desnto od prosledjene
     * @param type $cell
     * @return string
     */
    public function getCellAfter($cell) 
    {
        $column_name = $this->getColumnName($cell);
        $row = $this->getCellRow($cell);
        $next_column = $this->getColumnAfter($column_name);
        return $next_column.$row;
    }
    
    /**
     * Selektujemo kolonu u kojoj se trenutno nalazimo, cuva poziciju/index selektovane kolone
     * @param string $column
     * @return intiger
     */
    public function selectColumn($column) 
    {
        $position = array_search($column, $this->sheet_columns);
        $this->column_index = $position;
        $this->column = $this->sheet_columns[$this->column_index];
        if($this->column_index > $this->last_column_index)
        {
            $this->last_column_index = $this->column_index;
            $this->last_column = $this->column;
        }
        return $this->column_index;
    }
    
    /**
     * Selektujemo celiju
     * @param string $column
     * @param integer $row
     * @return string
     */
    public function selectCell($column, $row)
    {
        $this->selectColumn($column);
        $this->selectRow($row);
        return $this->getCurrentCell();
    }
    
    /**
     * Isto kao selectColumn, dodato zbog citljivosti
     * @param string $column
     * @return intiger
     */
    public function selectLastColumn($column) 
    {
        return $this->selectColumn($column);
    }
    
    /**
     * Vraca broj reda u koji se trenutno nalazimo
     * @return intiger
     */
    public function getCurrentRow() 
    {
        return $this->row_index;
    }
    
    /**
     * Vraca poslednji red
     * @return intiger
     */
    public function getLastRow() 
    {
        return $this->last_row_index;
    }
    
    /**
     * Selektuje sledeci red i vraca njen broj
     * @return intiger
     */
    public function selectNextRow() 
    {
        $this->row_index++;
        if($this->row_index > $this->last_row_index)
        {
            $this->last_row_index = $this->row_index;
        }
        return $this->getCurrentRow();
    }
    public function selectRow($row) 
    {
        $this->row_index = $row;
        if($row > $this->last_row_index)
        {
            $this->last_row_index = $this->row_index;
        }
        return $this->getCurrentRow();
    }
    /**
     * Selektuje prethodni red i vraca njen broj
     * @return intiger
     */
    public function selectPreviousRow() 
    {
        $this->row_index--;
        return $this->getCurrentRow();
    }
    
    public function getColumnName($cell) 
    {
        $cell_path = preg_split("/(,?\s+)|((?<=[a-z])(?=\d))|((?<=\d)(?=[a-z]))/i", $cell);
        return $cell_path[0];      
    }
    public function getCellRow($cell) 
    {
        $cell_path = preg_split("/(,?\s+)|((?<=[a-z])(?=\d))|((?<=\d)(?=[a-z]))/i", $cell);
        return $cell_path[1];      
    }
}

