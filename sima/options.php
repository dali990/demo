<?php

$model->getModelOptions(Yii::app()->user->model);

echo Yii::app()->controller->widget(SIMAButtonVue::class, [
    'id' => 'btn-generate-pdf'.$uniq,
    'title'=>Yii::t('BuildingsModule.BuildingMeasurementSheet','GeneratePDF'),
    'tooltip'=> Yii::t('BuildingsModule.BuildingMeasurementSheet','GeneratePDF'),                               
    'onclick'=> ['sima.hr.applyActiveAbsencesFilter', $uniq]
],true);