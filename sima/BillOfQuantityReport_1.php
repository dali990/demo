<?php

class BillOfQuantityReport extends SIMAReport
{
    protected $orientation = 'Landscape';
    protected $css_file_name = 'css.css';
    protected $margin_top = 5;
    protected $margin_bottom = 12;
    protected $include_page_numbering = false;
    protected $update_func = null;
    
    private $bill_of_quantity = null;
    private $exporting_pdf = false;
    private $leaf_objects_count = 1; //ukupan broj objekata
    private $has_object_value_per_unit = false;
    private $object_colspan = 0; //za naslove objekata u headru i za izracunavanje sirine kolone za stavke (item_to_object)
    private $group_item_title_colspan = 2; //colspan za naziv grupe/segmenta
    private $object_columns_width = 0;
    private $indent_item = 0; //padding-left u mm
    private $indent_item_increment = 2;//za koliko da poveca za svaku podgrupu
    private $object_order = []; //redosled id objekata za prikaz
    private $tilde = '';//znak za pribliznu vrednost jedinicne cene
    private $has_measurement_columns = false;//prikaz kolona za uradjeno ako je izabrana barem jedna od sledecih kolona za prikaz
    private $show_quantity_done_column = false;
    private $show_percentage_done_column = false;
    private $show_value_done_column = false;
    public $split_value_per_unit = false;
    private $folder = ""; //Ucitavanje drugo izlgeda tabele iz drugog foldera
    private $hide_homogeneous_items_value_per_unit = false; //Ne prikazivati jediničnu cenu pozicije ako je ista
    private $hide_heterogeneous_group_value_per_unit = false; //Ne prikazivati prosečnu cenu segmenta ako su različite u pozicijama
    private $hide_group_value_per_unit_and_quantity = false; //Ne prikazivati količinu i prosečnu jediničnu cenu segmenta
    private $group_iteration = 0;
    
    public $show_zeros = false;
    public $max_indent = 20;//maksimalno uvlacenje
    public $total_groups = 0;
    
    public function __construct($params, $update_func=null)
    {
        $this->update_func = $update_func;
        
        parent::__construct($params);
    }
    
    protected function getModuleName()
    {
        return 'buildings';
    }
    
    protected function renderBOQTableHeader() 
    {
        //SASA A. - mora da se radi reset ove 2 promenljive jer se funkcija za generisanje headera zove 2 puta
        $boq_table_header = [
            'order_number' => [
                'label' => Yii::t('BuildingsModule.BillOfQuantityItem', 'OrderNumberShort'),
                'css_class' => 'item-order'
            ],
            'name' => [
                'label' => Yii::t('BuildingsModule.BillOfQuantityItem', 'Name'),
                'css_class' => $this->exporting_pdf ? 'item-name-pdf' : 'item-name'
            ],
            'measurement_unit' => [
                'label' => Yii::t('BuildingsModule.BillOfQuantityItem', 'MeasurementUnit'),
                'css_class' => $this->exporting_pdf ? 'measurement-unit-name-pdf' : 'measurement-unit-name'
            ],
            'value_per_unit' => [
                'label' => Yii::t('BuildingsModule.BillOfQuantityItem', 'ValuePerUnit'),
                'css_class' => $this->exporting_pdf ? 'value-per-unit-pdf' : 'value-per-unit'
            ],
            'object_columns' => [
                'objects' => [],
                'width' => ''
            ]
        ];
        $boq_table_header['object_columns']['objects'] = $this->bill_of_quantity->leaf_objects;
        $objects_cnt = count($boq_table_header['object_columns']['objects']);
        
        if($objects_cnt > 1)
        {
            $measurement_columns_cnt = 0;
            $this->object_colspan = 2;
        }
        else 
        {
            $measurement_columns_cnt = 3; //broj kolona - izvedno kumalativno, po predhodnum, po ovoj situaciji
            $this->object_colspan = 0;
            $this->group_item_title_colspan = 6;
            
            //povecavamo colspan za kolonu nazive objekata ako je ukljucen prikaz procenat i ako predmer ima samo jedan objekat
            if($this->show_quantity_done_column)
            {
                $this->object_colspan++;
                $this->has_measurement_columns = true;
            }
            if($this->show_percentage_done_column)
            {
                $this->object_colspan++;
                $this->has_measurement_columns = true;
            }
            if($this->show_value_done_column)
            {
                $this->object_colspan++;
                $this->has_measurement_columns = true;
            }
        }
        
        //$this->object_colspan += $objects_cnt;
        //zbog prikaza kolona za situacija sirina kolona se deli sa 2
        $boq_table_header['object_columns']['width'] = $this->object_columns_width = round(94/$objects_cnt); //item-name(62)+measurement-unit-name(12)+value-per-unit(20)
        
        if($this->bill_of_quantity->has_object_value_per_unit)
        {
            unset($boq_table_header['value_per_unit']);
            $this->has_object_value_per_unit = true;
            $this->object_colspan++;
        }
        //dodata zbog greske koja nastaje kada se deli sa 0
        if($this->object_colspan === 0)
        {
            $this->object_colspan = 1;
        }
        
        if($this->leaf_objects_count === 1)
        {
            unset($boq_table_header['value_per_unit']);
            $columns_in_object = $this->object_colspan;
            $boq_table_header['object_columns']['width'] = $this->object_columns_width = round((94/$objects_cnt)/$columns_in_object);
        }
        $this->group_item_title_colspan += ($measurement_columns_cnt * $this->object_colspan);
        if($this->has_object_value_per_unit )
        {
            //Povecava colspan zbog jedinicne cene
            $this->group_item_title_colspan += ($measurement_columns_cnt * $this->leaf_objects_count);
        }
        return $this->render($this->folder.'boq_table_header', [
            'bill_of_quantity' => $this->bill_of_quantity,
            'boq_table_header' => $boq_table_header,
            'exporting_pdf' => $this->exporting_pdf,
            'has_object_value_per_unit' => $this->has_object_value_per_unit,
            'object_colspan' => $this->object_colspan,
            'leaf_objects_count' => $this->leaf_objects_count,
            'object_columns_width' => $this->object_columns_width,
            'has_measurement_columns' => $this->has_measurement_columns,
            'show_quantity_done_column' => $this->show_quantity_done_column,
            'show_percentage_done_column' => $this->show_percentage_done_column,
            'show_value_done_column' => $this->show_value_done_column,
            'split_value_per_unit' => $this->split_value_per_unit,
            'convert_to_cyrillic' => $this->params['convert_to_cyrillic'] ?? false
        ]);
    }
    
    private function getBOQHeader() 
    {
        return $this->render('header', [
            'bill_of_quantity' => $this->params['bill_of_quantity'],
            'convert_to_cyrillic' => $this->params['convert_to_cyrillic'] ?? false
        ]);
    }
    
    protected function getHTMLContent()
    {
        $this->bill_of_quantity = $this->params['bill_of_quantity'];
        $exporting_pdf = $this->params['ACTION_EXPORT_PDF'] ?? false;
        $this->exporting_pdf = $exporting_pdf;
        $this->leaf_objects_count = $this->bill_of_quantity->leaf_objects_count;
        $show_quantity_done_column = $this->params['show_quantity_done_column'] ?? null;
        $show_percentage_done_column = $this->params['show_percentage_done_column'] ?? null;
        $show_value_done_column = $this->params['show_value_done_column'] ?? null;
        $this->show_zeros = $this->params['show_zeros'] ?? true;
        $this->split_value_per_unit = $this->params['bill_of_quantity']->split_value_per_unit;
        $this->hide_homogeneous_items_value_per_unit = $this->params['bill_of_quantity']->hide_homogeneous_items_value_per_unit;
        $this->hide_heterogeneous_group_value_per_unit = $this->params['bill_of_quantity']->hide_heterogeneous_group_value_per_unit;
        $this->hide_group_value_per_unit_and_quantity = $this->params['bill_of_quantity']->hide_group_value_per_unit_and_quantity;
        $this->total_groups = $this->getTotalGroup() - 1;//-1 zbog root grupe
        
        if($this->split_value_per_unit)
        {
            $this->folder = "SplitedValuePerUnit/";
        }
        if($this->leaf_objects_count === 1)
        {
            if($show_quantity_done_column === 'true')
            {
                $this->show_quantity_done_column = true;
            }
            if($show_percentage_done_column === 'true')
            {
                $this->show_percentage_done_column = true;
            }
            if($show_value_done_column === 'true')
            {
                $this->show_value_done_column = true;
            }
        }
        
        $this->download_name = $this->bill_of_quantity->name . '.pdf';
        
        $content = $this->renderBOQTableHeader();
        $bill_of_quantity = $this->params['bill_of_quantity'];
        gc_enable();
        $total_group = $this->total_groups - 1; //-1 zbog root grupe
        $this->boqToArrayByParts(function($item_group_data) use (&$content, $total_group){
            $content .= $this->renderItemGroups($item_group_data);
//            $this->group_iteration++;
//            if (!empty($this->update_func))
//            {
//                call_user_func($this->update_func, round(($this->group_iteration/$total_group)*100, 0, PHP_ROUND_HALF_DOWN));
//            }
            gc_collect_cycles();
        });
        
        //Poslednji red - ukupna suma predmera
        $content .= $this->render($this->folder.'item_group_total', [
            'exporting_pdf' => $this->exporting_pdf,
            'group_item' => $this->getRootGroupItemTotal(),
            'leaf_objects_count' => $this->leaf_objects_count,
            'has_object_value_per_unit' => $this->has_object_value_per_unit,
            'indent_item' => 0,
            'last_row' => true,
            'show_quantity_done_column' => $this->show_quantity_done_column,
            'show_percentage_done_column' => $this->show_percentage_done_column,
            'show_value_done_column' => $this->show_value_done_column,
            'total_for_group' => $bill_of_quantity->name,
            'convert_to_cyrillic' => $this->params['convert_to_cyrillic'] ?? false
        ]);
        
        return "<div style='width: 100%; height: 5mm;'></div>" . $this->getBOQHeader() . $this->render('html', [
            'exporting_pdf' => $this->exporting_pdf,
            'content' => $content,
            'convert_to_cyrillic' => $this->params['convert_to_cyrillic'] ?? false
        ]);
    }
    
    private function renderItemGroups($group_item) 
    {
        $content = "";
        
        $content .= $this->render($this->folder.'item_group_title', [
            'exporting_pdf' => $this->exporting_pdf,
            'group_item' => $group_item,
            'group_item_title_colspan' => $this->group_item_title_colspan,
            'indent_item' => $this->indent_item,
            'convert_to_cyrillic' => $this->params['convert_to_cyrillic'] ?? false
        ]);
        foreach ($group_item['groups'] as $group)
        {
            $this->indent();
            $content .= $this->renderItemGroups($group);
            $this->indent_item -= $this->indent_item_increment;
        }
        $content .= $this->render($this->folder.'item_group_items', [
            'exporting_pdf' => $this->exporting_pdf,
            'group_item' => $group_item,
            'object_columns_width' => $this->object_columns_width,
            'has_object_value_per_unit' => $this->has_object_value_per_unit,
            'object_colspan' => $this->object_colspan,
            'leaf_objects_count' => $this->leaf_objects_count,
            'group_item_title_colspan' => $this->group_item_title_colspan,
            'indent_item' => $this->indent(),
            'show_quantity_done_column' => $this->show_quantity_done_column,
            'show_percentage_done_column' => $this->show_percentage_done_column,
            'show_value_done_column' => $this->show_value_done_column,
            'convert_to_cyrillic' => $this->params['convert_to_cyrillic'] ?? false
        ]);
        if(!$group_item['hide_total_row'])
        {
            $content .= $this->render($this->folder.'item_group_total', [
                'exporting_pdf' => $this->exporting_pdf,
                'group_item' => $group_item,
                'leaf_objects_count' => $this->leaf_objects_count,
                'has_object_value_per_unit' => $this->has_object_value_per_unit,
                'indent_item' => ($this->indent_item -= $this->indent_item_increment),
                'last_row' => false,
                'show_quantity_done_column' => $this->show_quantity_done_column,
                'show_percentage_done_column' => $this->show_percentage_done_column,
                'show_value_done_column' => $this->show_value_done_column,
                'total_for_group' => !empty($group_item['has_total_title']) ? " - ".$group_item['name'] : "",
                'convert_to_cyrillic' => $this->params['convert_to_cyrillic'] ?? false
            ]);
        }
            
        
        return $content;
    }
    public function indent() 
    {
        if($this->indent_item < $this->max_indent)
        {
            $this->indent_item += $this->indent_item_increment;
        }
        return $this->indent_item;
    }
    
    public function boqToArray() : array
    {
        $boq_table = [];
        $bill_of_quantity = $this->params['bill_of_quantity'];
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        foreach ($bill_of_quantity->first_level_groups as $item_group) 
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            $boq_table[] = $this->getGroupItemsRecurisve($item_group);
        }
      
        return $boq_table;
    }
    
    /**
     * Vraca predmer po grupama prvog nivoa kao niz, zajdeno sa sa podgrupama ukoliko postoje u datoj grupi
     * @param function $callback
     * @param callback function $item_group_data - niz koji sadrzi sve podatke jedne grupe zajedno sa svojim podgrupama
     */
    public function boqToArrayByParts($callback)
    {
        $bill_of_quantity = $this->params['bill_of_quantity'];
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        foreach ($bill_of_quantity->first_level_groups as $item_group) 
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            $item_group_data = $this->getGroupItemsRecurisve($item_group, function($items) use ($callback){
                $callback($items);
                gc_collect_cycles();
            });
            $item_group_data = null;
            unset($item_group_data);
            gc_collect_cycles();
        }
    }
    
    public function boqGroupToArray($item_group) : array
    {
        $boq_table = [];
        
        $boq_table = $this->getGroupItemsRecurisve($item_group);
      
        return $boq_table;
    }
    
    private function getGroupItemsRecurisve(BillOfQuantityItemGroup $item_group, $callback=null, $make_callback=true) : array
    {
        set_time_limit(ini_get('max_execution_time'));
        $items = [
            'order' => $item_group->getFullOrderPath(),
            'name' => $item_group->title,
            'is_bold' => $item_group->is_bold,
            'note' => $item_group->bill_of_quantity_note,
            'has_total_title' => $item_group->has_total_title,
            'measurement_unit_name' => '',
            'groups' => [], //podgrupe, sadrzi children, nizove iste strukture kao $items
            'items' => [], //stavke
            'group_value' => ['objects' => [] ], //suma objekata iz grupe
            'objects_cnt' => 0,
            'item_group_id' => $item_group->id,
            'hide_total_row' => $item_group->hide_total_row
        ];
        $has_same_measurement_unit = $this->getMesuramentUnitName($item_group);
        $ordered_item_group_to_objects_values = [];
        $this->object_order = [];//Cuvamo redosled objekata
        gc_collect_cycles();
        foreach($item_group->bill_of_quantity->leaf_objects as $object)
        {
            $this->object_order[] = $object->id;
            gc_collect_cycles();
        }
        //Stavke u grupi
        $boq_item = BillOfQuantityItem::model();
        $alias = $boq_item->getTableAlias();
        $by_order_scope = $boq_item->scopes()['byOrder'];

        $criteria = new SIMADbCriteria();
        $criteria->condition = "$alias.group_id = ".$item_group->id;
        $criteria->join = $by_order_scope['join'];
        $criteria->order = $by_order_scope['order'];
        BillOfQuantityItem::model()->findAllByParts(function($boq_items) use (&$items){
            foreach ($boq_items as $boq_item) {
                $ordered_item_to_objects = [];
                $value_per_unit = $this->showValueWithZeroCheck($boq_item->value_per_unit);         
                $items['items'][$boq_item->id] = [
                    'order' => $boq_item->getFullOrderPath(),
                    'name' => trim($boq_item->name),
                    'bill_of_quantity_note' => trim($boq_item->bill_of_quantity_note),
                    'measurement_unit_name' => $boq_item->measurement_unit->name,
                    'value_per_unit' => $value_per_unit,
                    'objects' => [],
                    'parent_object_value' => null
                ];
                //Cuvanje iznosa za svkau stavku (item_to_object)
                foreach($boq_item->item_to_objects_leafs as $item_to_object)
                {
                    $quantity = $this->showValueWithZeroCheck($item_to_object->quantity);
                    $quantity_done = $item_to_object->quantity_done;                
                    $percentage_done = $item_to_object->getPercentageDone();
                    $value = $this->showValueWithZeroCheck($item_to_object->value);
                    $value_work = $this->showValueWithZeroCheck($item_to_object->value_work);
                    $value_material = $this->showValueWithZeroCheck($item_to_object->value_material);

                    if($this->hide_homogeneous_items_value_per_unit && $item_to_object->item->group->hasAllItemsSameValuePerUnitWork($item_to_object->object))
                    {
                        $value_per_unit_work_display = "";
                    }
                    else
                    {
                        $value_per_unit_work_display = $this->showValueWithZeroCheck($item_to_object->value_per_unit_work_display);
                    }
                    if($this->hide_homogeneous_items_value_per_unit && $item_to_object->item->group->hasAllItemsSameValuePerUnitMaterial($item_to_object->object))
                    {
                        $value_per_unit_material_display = "";
                    }
                    else
                    {
                        $value_per_unit_material_display = $this->showValueWithZeroCheck($item_to_object->value_per_unit_material_display);
                    }
                    if($this->hide_homogeneous_items_value_per_unit && $item_to_object->item->group->hasAllItemsSameValuePerUnit($item_to_object->object))
                    {
                        $value_per_unit = "";
                    }
                    else
                    {
                        $value_per_unit = $this->showValueWithZeroCheck($item_to_object->value_per_unit);
                    }
                    gc_collect_cycles();
                    $items['items'][$boq_item->id]['objects'][$item_to_object->object->id] = [
                        'quantity' => $quantity,
                        'quantity_done' => $quantity_done,
                        'percentage_done' => $percentage_done,
                        'value_per_unit' => $value_per_unit,
                        'value' => $value,
                        'value_work' => $value_work,
                        'value_material' => $value_material,
                        'value_per_unit_work_display' => $value_per_unit_work_display,
                        'value_per_unit_material_display' => $value_per_unit_material_display,
                        'measurement_sheet' => $this->getBuildingMeasurementSheet($boq_item->id, $item_to_object->object->id),
                        'object_name' => $item_to_object->object->name
                    ];
                    //Ukupna suma za root objekat svake stavke
                    if($items['items'][$boq_item->id]['parent_object_value'] === null)
                    {
                        $paretn_time_to_object = BillOfQuantityItemToObject::model()->findByAttributes([
                            'object_id' => $this->params['bill_of_quantity']->root_object->id, 
                            'item_id' => $boq_item->id
                        ]);
                        if($paretn_time_to_object)
                        {
                            $items['items'][$boq_item->id]['parent_object_value'] = $paretn_time_to_object->value->round(2)->toString();    
                        }
                        else 
                        {
                            $items['items'][$boq_item->id]['parent_object_value'] = $item_to_object->value->round(2)->toString();
                        }
                    }
                    gc_collect_cycles();
                }
                //Sortiranje objekata
                foreach ($this->object_order as $objec_id)
                {
                    $ordered_item_to_objects[$objec_id] = $items['items'][$boq_item->id]['objects'][$objec_id];
                }
                $items['items'][$boq_item->id]['objects'] = $ordered_item_to_objects;
                gc_collect_cycles();
            }
        }, $criteria);
        //GRUPE Ukupno - Cuvanje sume objekata po grupama  
        $items['group_value']['parent_object_value'] = null;
        foreach($item_group->item_group_to_objects_leafs as $item_group_to_object)
        {
            $value_per_unit = "";
            $value_per_unit_work_display = "";
            $value_per_unit_material_display = "";
            $quantity = "";
            $quantity_done = $this->showValueWithZeroCheck($item_group_to_object->quantity_done);
            
            if($has_same_measurement_unit && $this->hide_group_value_per_unit_and_quantity === false)
            {
                $quantity = $this->showValueWithZeroCheck($item_group_to_object->quantity);
                //Skrivanje prosecne jed. cene ako je razlicita u stavkama/pozicijama (items)                
                if($this->hide_heterogeneous_group_value_per_unit && !$item_group_to_object->item_group->hasAllItemsSameValuePerUnitWork($item_group_to_object->object))
                {
                    $value_per_unit_work_display = "";
                }
                else
                {
                    $value_per_unit_work_display = $this->showValueWithZeroCheck($item_group_to_object->value_per_unit_work_display);
                }
                if($this->hide_heterogeneous_group_value_per_unit && !$item_group_to_object->item_group->hasAllItemsSameValuePerUnitMaterial($item_group_to_object->object))
                {
                    $value_per_unit_material_display = "";
                }
                else
                {
                    $value_per_unit_material_display = $this->showValueWithZeroCheck($item_group_to_object->value_per_unit_material_display);
                }
                if($this->has_object_value_per_unit)
                {
                    $value_per_unit = $this->tilde.$this->showValueWithZeroCheck($item_group_to_object->getApproximatelyValuePerUnit());
                }
                else
                {
                    $value_per_unit = $this->tilde.$this->showValueWithZeroCheck($item_group_to_object->item_group->getApproximatelyValuePerUnit());
                }
                if($this->hide_heterogeneous_group_value_per_unit && !$item_group_to_object->item_group->hasAllItemsSameValuePerUnit($item_group_to_object->object))
                {
                    $value_per_unit = "";
                }
            }
            $items['group_value']['objects'][$item_group_to_object->object->id] = [
                'value' => $this->showValueWithZeroCheck($item_group_to_object->value),
                'value_per_unit' => $value_per_unit,
                'value_work' => $this->showValueWithZeroCheck($item_group_to_object->value_work),
                'value_material' => $this->showValueWithZeroCheck($item_group_to_object->value_material),
                'value_per_unit_work_display' => $value_per_unit_work_display,
                'value_per_unit_material_display' => $value_per_unit_material_display,
                'quantity' => $quantity,
                'quantity_done' => $quantity_done,
                'percentage_done' => $item_group_to_object->getPercentageDone()->round(2)->toString(),
                'measurement_sheet' => $this->getBuildingMeasurementSheetGroup($item_group->id, $item_group_to_object->object->id),
            ];
            //Ukupno za segment - value
            if($items['group_value']['parent_object_value'] === null)
            {
                $paretn_time_to_object = BillOfQuantityItemGroupToObject::model()->findByAttributes([
                    'object_id' => $this->params['bill_of_quantity']->root_object->id, 
                    'item_group_id' => $item_group_to_object->item_group_id
                ]);
                if(!empty($paretn_time_to_object))
                {
                    $items['group_value']['parent_object_value'] = $paretn_time_to_object->value->round(2)->toString();    
                }
            }
            gc_collect_cycles();
        }
      
        //Sortiranje kolona za prikaz sume objekata po grupama
        foreach ($this->object_order as $objec_id)
        {
            $ordered_item_group_to_objects_values[$objec_id] = $items['group_value']['objects'][$objec_id];
        }
        $items['group_value']['measurement_unit_name'] = $has_same_measurement_unit; //measurement_unit_name
        $items['group_value']['value_per_unit'] = $has_same_measurement_unit ? $this->tilde.$item_group->getApproximatelyValuePerUnit()->round(2)->getValue() : '';
        $items['group_value']['objects'] = $ordered_item_group_to_objects_values;
        $items['objects_cnt'] = count($items['group_value']['objects']);
        
        //TODO: trebalo bi ispratiti raspored predmera, prikazati onako kako ide u predmeru
        
//        $callback($items);
//        $make_callback = true;
        //Ukoliko ima podgrupa rekurzivno kreiramo nove elemente istog formata 
        if($item_group->children_count > 0 && isset($item_group->parent))
        {
            $alias = BillOfQuantityItemGroup::model()->getTableAlias();
            $by_order_scope = $item_group->scopes()['byOrder'];
            
            $criteria = new SIMADbCriteria();
            $criteria->condition = "$alias.parent_id = ".$item_group->id." AND $alias.bill_of_quantity_id = ".$this->params['bill_of_quantity']->id;
            $criteria->join = $by_order_scope['join'];
            $criteria->order = $by_order_scope['order'];
            BillOfQuantityItemGroup::model()->findAllByParts(function($child_item_groups) use ($callback, &$items, &$make_callback){
                foreach ($child_item_groups as $child_item_group) {
//                    $items['groups'][] = $this->getGroupItemsRecurisve($child_item_group, $callback, true);
                    $i = $this->getGroupItemsRecurisve($child_item_group, $callback, true);
                    $callback($i);
//                    data_log($i);
                    $make_callback = false;
//                    data_log(vardump($make_callback), "AAA");
                    gc_collect_cycles();
                }
            }, $criteria);
//            $callback($items);
        }       
        else 
        {
            //Saljemo paket tek kada je neka grupa spremna
//            if($callback)
//            {
//                $callback($items);
//            }
//            $callback($items);
            if (!empty($this->update_func))
            {
                $this->group_iteration++;
                call_user_func($this->update_func, round(($this->group_iteration/$this->total_groups)*100, 0, PHP_ROUND_HALF_DOWN));
            }
        }
        
                
        gc_collect_cycles();
        return $items;
    }
    
    public function getBuildingMeasurementSheet($item_id, $object_id, $unformated = false) 
    {
        $bms = null;
        if(isset($this->params['building_interim_bill_id']))
        {
            $bms = BuildingMeasurementSheet::model()->findByAttributes([
                'building_interim_bill_id' => $this->params['building_interim_bill_id'],
                'bill_of_quantity_item_id' => $item_id,
                'object_id' => $object_id
            ]);
        }
        
        return $this->getBuildingMeasurementSheetItems($bms, $unformated);
    }
    
    public function getBuildingMeasurementSheetGroup($item_group_id, $object_id, $unformated = false) 
    {    
        $bmsg = null;
        if(isset($this->params['building_interim_bill_id']))
        {
            $bmsg = BuildingMeasurementSheetGroup::model()->findByAttributes([
                'building_interim_bill_id' => $this->params['building_interim_bill_id'],
                'bill_of_quantity_item_group_id' => $item_group_id,
                'object_id' => $object_id
            ]);
        }
        
        return $this->getBuildingMeasurementSheetItems($bmsg, $unformated, true);
    }
    
    private function getBuildingMeasurementSheetItems($bms, $unformated = false, $is_root = false) 
    {
        $default_value = SIMABigNumber::fromFloat(0.00, 2)->getValue();
        $hide_group_default_value = $default_value;
        if($is_root && $this->hide_group_value_per_unit_and_quantity)
        {
            $hide_group_default_value = "";
        }
        
        $bms_items = [
            'value_done' => $default_value,
            'quantity_done' => $hide_group_default_value,
            'percentage_done' => $default_value,
            'pre_value_done' => $default_value,
            'pre_quantity_done' => $hide_group_default_value,
            'pre_percentage_done' => $default_value,
            'total_value_done' => $default_value,
            'total_quantity_done' => $hide_group_default_value,
            'total_percentage_done' => $default_value,
            'total_value_done_work' => $default_value,
            'total_value_done_material' => $default_value,
        ];
        if(!empty($bms))
        {
            $bms_items['value_done'] = $unformated ? $bms->value_done->round(2)->getValue() : $bms->value_done->round(2)->toString();
            $bms_items['quantity_done'] = $this->displayBMSItemNumber('quantity_done', $bms, $unformated, $is_root);
            $bms_items['percentage_done'] = $unformated ? $bms->percentage_done->round(2)->getValue() : $bms->percentage_done->round(2)->toString();
            $bms_items['pre_value_done'] = $unformated ? $bms->pre_value_done->round(2)->getValue() : $bms->pre_value_done->round(2)->toString();
            $bms_items['pre_quantity_done'] = $this->displayBMSItemNumber('pre_quantity_done', $bms, $unformated, $is_root);
            $bms_items['pre_percentage_done'] = $unformated ? $bms->pre_percentage_done->round(2)->getValue() : $bms->pre_percentage_done->round(2)->toString();
            $bms_items['total_value_done'] = $unformated ? $bms->total_value_done->round(2)->getValue() : $bms->total_value_done->round(2)->toString();
            $bms_items['total_quantity_done'] = $this->displayBMSItemNumber('total_quantity_done', $bms, $unformated, $is_root);
            $bms_items['total_percentage_done'] = $unformated ? $bms->total_percentage_done->round(2)->getValue() : $bms->total_percentage_done->round(2)->toString();
            $bms_items['total_value_done_work'] = $unformated ? $bms->total_value_done_work->round(2)->getValue() : $bms->total_value_done_work->round(2)->toString();
            $bms_items['total_value_done_material'] = $unformated ? $bms->total_value_done_material->round(2)->getValue() : $bms->total_value_done_material->round(2)->toString();
        }
        
        return $bms_items;
    }
    
    private function displayBMSItemNumber($field_name, $bms, $unformated, $is_root) 
    {
        if($is_root && $this->hide_group_value_per_unit_and_quantity)
        {
            return null;
        }
        if(!empty($bms))
        {
            if($unformated)
            {
                return $bms->{$field_name}->round(2)->getValue();
            }
            else 
            {
                return $bms->{$field_name}->round(2)->toString();
            }
            
        }
        return null;
    }
    
    //Pakujemo poslednji red za prikaz ukupne sume jed. cene i ostale stavke svih objekata
    public function getRootGroupItemTotal() 
    {
        $bill_of_quantity = $this->params['bill_of_quantity'];
        $root_group_total['group_value']['value_per_unit'] = '';
        $root_group_total['title'] = $bill_of_quantity->root_group->title;
        $has_same_measurement_unit = $this->getMesuramentUnitName($bill_of_quantity->root_group);
        
        if($has_same_measurement_unit && !$this->has_object_value_per_unit)
        {
            $root_group_total['group_value']['value_per_unit'] = $bill_of_quantity->root_group->getApproximatelyValuePerUnit()->round(2)->getValue();
        }
        
        //Ispisivanje sume za objekte root grupe
        $paretn_time_group_to_object = BillOfQuantityItemGroupToObject::model()->findByAttributes([
            'object_id' => $bill_of_quantity->root_object->id, 
            'item_group_id' => $bill_of_quantity->root_group->id //boq_item
        ]);
        //Ukupno
        $root_group_total['group_value']['parent_object_value'] = $paretn_time_group_to_object->value->round(2)->toString();
        foreach($bill_of_quantity->root_group->item_group_to_objects_leafs as $item_group_to_object)
        {
            $value_per_unit_work_display = '';
            $value_per_unit_material_display = '';
            $value_per_unit = '';
            $value_work = '';
            $quantity = '';
            if(
                $has_same_measurement_unit && 
                $this->hide_group_value_per_unit_and_quantity === false &&
                (
                    $this->hide_heterogeneous_group_value_per_unit === false || 
                    $item_group_to_object->item_group->hasAllItemsSameValuePerUnit($item_group_to_object->object) === true
                )
            )
            {
                $value_per_unit_work_display = $this->showValueWithZeroCheck($item_group_to_object->value_per_unit_work_display);
                $value_per_unit_material_display = $this->showValueWithZeroCheck($item_group_to_object->value_per_unit_material_display);
                $quantity = $this->showValueWithZeroCheck($item_group_to_object->quantity);
                if($this->has_object_value_per_unit)
                {
                    $value_per_unit = $this->tilde.$this->showValueWithZeroCheck($item_group_to_object->getApproximatelyValuePerUnit());
                }
                else
                {
                    $value_per_unit = $this->tilde.$this->showValueWithZeroCheck($item_group_to_object->item_group->getApproximatelyValuePerUnit());
                }
            }
            $root_group_total['group_value']['objects'][$item_group_to_object->object->id] = [
                'value' => $this->showValueWithZeroCheck($item_group_to_object->value),
                'value_per_unit' => $value_per_unit,
                'value_work' => $this->showValueWithZeroCheck($item_group_to_object->value_work),
                'value_material' => $this->showValueWithZeroCheck($item_group_to_object->value_material),
                'value_per_unit_work_display' => $value_per_unit_work_display,
                'value_per_unit_material_display' => $value_per_unit_material_display,
                'quantity' => $quantity,
                'quantity_done' => $this->showValueWithZeroCheck($item_group_to_object->quantity_done),
                'percentage_done' => $this->showValueWithZeroCheck($item_group_to_object->getPercentageDone()),
                'measurement_sheet' => $this->getBuildingMeasurementSheetGroup($bill_of_quantity->root_group->id, $item_group_to_object->object->id),
            ];
        }
        
        foreach ($this->object_order as $objec_id)
        {
            $ordered_item_group_to_objects_values[$objec_id] = $root_group_total['group_value']['objects'][$objec_id];
        }
        $root_group_total['group_value']['measurement_unit_name'] = $has_same_measurement_unit; //measurement_unit_name
        $root_group_total['group_value']['objects'] = $ordered_item_group_to_objects_values;
        
        return $root_group_total;
    }
    
    private function getMesuramentUnitName(BillOfQuantityItemGroup $item_group) {
        $measurement_unit = '';
        if($item_group->hasAllItemsSameMeasurementUnit())
        {
            $group_first_item = $item_group->getFirstItemRecursively();
            if (!empty($group_first_item))
            {
                $measurement_unit = $group_first_item->measurement_unit->DisplayName;
            }
        }
        return $measurement_unit;
    }  
    
    public function getBigTextClass($column, $text, $indent=null) 
    {
        if($column === 'name')
        {
            if($indent >= 10 && strlen($text) > 1500)
            {
                return "text--xs";
            }
            else if($indent >= 6 && strlen($text) > 3000)
            {
                return "text--xs";
            }
            else if($indent >= 6 && strlen($text) > 2500)
            {
                return "text--s";
            }
            else if($indent <= 2 && strlen($text) > 3200)
            {
                return "text--xs";
            }
            else if($indent <= 2 && strlen($text) > 2500)
            {
                return "text--s";
            }
        }
        else if($column === 'number')
        {
            if(strlen($text) >= 14)
            {
                return "text--s";
            }
        }
    }
    
    public function getXLS() 
    {        
        $spreadsheet = new BillOfQuantitySpreadsheet($this->params, $this->update_func);
        return $spreadsheet->generateFile();
    }
    
    //TODO: U RAZVOJU - ispravka i optimizacija za prepakivanje koda
    public function getSpreadsheet($columns=[]) 
    {
        $building_construction = $this->params['building_construction'] ?? null;
        $building_interim_bill_id = $this->params['building_interim_bill_id'] ?? null;
        $item_group_id = $this->params['item_group_id'] ?? null;
        
        $boq_params = ['columns' => [] ];
        $boq_params['columns'] = [
            'quantity_done',
            'percentage_done'
        ];
        if($building_construction)
        {
            $boq_params['columns'] = [
                'quantity_done',
                'percentage_done'
            ];
        }
        if($building_interim_bill_id)
        {
            $boq_params['columns'] = [];
            $boq_params['boq']['building_interim_bill_id'] = $building_interim_bill_id;
            $show_quantity_done_column = false;
            if(isset($columns['show_quantity_done_column']))
            {
                $show_quantity_done_column = $columns['show_quantity_done_column'];
            }   
            //Kolicina
            //eventData vraca boolean kao string
            $show_quantity_done_column = $show_quantity_done_column === 'true' || $show_quantity_done_column === true ? true : false;
            if($show_quantity_done_column)
            {
                $boq_params['columns']['measurement_sheet'][] = 'quantity_done';
                $boq_params['columns']['measurement_sheet'][] = 'pre_quantity_done';
                $boq_params['columns']['measurement_sheet'][] = 'total_quantity_done';
            }
            //Procenat
            $show_percentage_done_column = false;
            if(isset($columns['show_percentage_done_column']))
            {
                $show_percentage_done_column = $columns['show_percentage_done_column'];
            }
            $show_percentage_done_column = $show_percentage_done_column === 'true' || $show_percentage_done_column === true ? true : false;
            if($show_percentage_done_column)
            {
                $boq_params['columns']['measurement_sheet'][] = 'percentage_done';
                $boq_params['columns']['measurement_sheet'][] = 'pre_percentage_done';
                $boq_params['columns']['measurement_sheet'][] = 'total_percentage_done';
            }
            //Iznos
            $show_value_done_column = false;
            if(isset($columns['show_value_done_column']))
            {
                $show_value_done_column = columns['show_value_done_column'];
            }
            $show_value_done_column = $show_value_done_column === 'true' || $show_value_done_column === true ? true : false;
            if($show_value_done_column)
            {
                $boq_params['columns']['measurement_sheet'][] = 'value_done';
                $boq_params['columns']['measurement_sheet'][] = 'pre_value_done';
                $boq_params['columns']['measurement_sheet'][] = 'total_value_done';
            }
        }
        
        $boq_params['boq']['bill_of_quantity'] = $this->params['bill_of_quantity'];
        $boq_params['boq']['leaf_objects_count'] = $this->params['bill_of_quantity']->leaf_objects_count;
        $boq_params['show_zeros'] = $this->show_zeros;
        if($item_group_id){
            $boq_params['boq']['item_group_id'] = $item_group_id;
        }       
        $spreadheet = new BillOfQuantitySpreadsheet($boq_params);
        $tempFile = $spreadheet->generateFile();
        return $tempFile;
    }
    
    public function showValueWithZeroCheck($value) 
    {
        if(!$this->show_zeros && $value->isZero())
        {
            return "";
        }
        return $value->round(2)->toString();
    }
    
    public function getTotalGroup() 
    {
        //$this->update_func se pozivia kada $item_group nema dece, tj kada se dodje do leaf, zato brojimo leaf
        return $this->params['bill_of_quantity']->leaf_groups_count;
    }
    
    public static function convertLatinToCyrillic($latin_text, $convert=false) 
    {
        if($convert)
        {
            return SIMAMisc::convertLatinToCyrillic($latin_text);
        }
        return $latin_text;
    }
    
}

/*
 * Structura niza koju vraca getGroupItemsRecurisve
 * Primer predmera sa 1objekat

segmenat 1                                              mer. jed.       jed. cena       kolicina        iznos
    grupa1 u segementu1
        stavka u segmentu 1 -> grupa 1                  m3              10.00           2.00            20.00
    UKUPNO                                                              10.00           2.00            20.00
    stavka u segmentu 1                                 м3              10.00           5.00            50.00
    stavka u segmentu 1                                 м3              0.00                            0.00
UKUPNO                                                                  12.00                           70.00
segmenat 2
    grupa1 u segmentu2
        grupa1.1 u grupi1 -> segemenat 2
            savka u grupi 1.1 -> grupe 1 u segemntu 2   kom             0.00            0.00            0.00
        UKUPNO                                                          0.00            0.00            0.00
    UKUPNO                                                              0.00            0.00            0.00
UKUPNO                                                                  0.00            0.00            70.00

* Reprezentacija niza
Array
(
    [0] => Array
        (
            [name] => segmenat 1
            [measurement_unit_name] => 
            [groups] => Array
                (
                    [0] => Array
                        (
                            [name] => grupa1 u segementu1
                            [measurement_unit_name] => 
                            [groups] => Array
                                (
                                )

                            [items] => Array
                                (
                                    [110] => Array
                                        (
                                            [name] => stavka u segmentu 1 -> grupa 1
                                            [measurement_unit_name] => m3
                                            [value_per_unit] => 10.00
                                            [objects] => Array
                                                (
                                                    [23] => Array
                                                        (
                                                            [quantity] => 2.00
                                                            [value_per_unit] => 10.00
                                                            [value] => 20.00
                                                            [object_name] => Predmer uvoz
                                                        )

                                                )

                                        )

                                )

                            [group_value] => Array
                                (
                                    [objects] => Array
                                        (
                                            [23] => Array
                                                (
                                                    [value] => 20.00
                                                    [value_per_unit] => 10.00
                                                    [quantity] => 2.00
                                                )

                                        )

                                    [measurement_unit_name] => m3
                                    [value_per_unit] => 10.00
                                )

                            [objects_cnt] => 1
                        )

                )

            [items] => Array
                (
                    [108] => Array
                        (
                            [name] => stavka u segmentu 1
                            [measurement_unit_name] => m3
                            [value_per_unit] => 590.00
                            [objects] => Array
                                (
                                    [23] => Array
                                        (
                                            [quantity] => 10.00
                                            [value_per_unit] => 5.00
                                            [value] => 50.00
                                            [object_name] => Predmer uvoz
                                        )

                                )

                        )

                    [109] => Array
                        (
                            [name] => Stavka u segmentu 1
                            [measurement_unit_name] => m3
                            [value_per_unit] => 0.00
                            [objects] => Array
                                (
                                    [23] => Array
                                        (
                                            [quantity] => 0.00
                                            [value_per_unit] => 0.00
                                            [value] => 0.00
                                            [object_name] => Predmer uvoz
                                        )

                                )

                        )

                )

            [group_value] => Array
                (
                    [objects] => Array
                        (
                            [23] => Array
                                (
                                    [value] => 70.00
                                    [value_per_unit] => 5.83
                                    [quantity] => 12.00
                                )

                        )

                    [measurement_unit_name] => m3
                    [value_per_unit] => 5.83
                )

            [objects_cnt] => 1
        )

    [1] => Array
        (
            [name] => segmenat 2
            [measurement_unit_name] => 
            [groups] => Array
                (
                    [0] => Array
                        (
                            [name] => grupa1 u segmentu2
                            [measurement_unit_name] => 
                            [groups] => Array
                                (
                                    [0] => Array
                                        (
                                            [name] => grupa1.1 u grupi1 -> segemenat 2
                                            [measurement_unit_name] => 
                                            [groups] => Array
                                                (
                                                )

                                            [items] => Array
                                                (
                                                    [133] => Array
                                                        (
                                                            [name] => savka u grupi 1.1 -> grupe 1 u segemntu 2
                                                            [measurement_unit_name] => kom
                                                            [value_per_unit] => 0.00
                                                            [objects] => Array
                                                                (
                                                                    [23] => Array
                                                                        (
                                                                            [quantity] => 0.00
                                                                            [value_per_unit] => 0.00
                                                                            [value] => 0.00
                                                                            [object_name] => Predmer uvoz
                                                                        )

                                                                )

                                                        )

                                                )

                                            [group_value] => Array
                                                (
                                                    [objects] => Array
                                                        (
                                                            [23] => Array
                                                                (
                                                                    [value] => 0.00
                                                                    [value_per_unit] => 0.00
                                                                    [quantity] => 0.00
                                                                )

                                                        )

                                                    [measurement_unit_name] => kom
                                                    [value_per_unit] => 0.00
                                                )

                                            [objects_cnt] => 1
                                        )

                                )

                            [items] => Array
                                (
                                    [111] => Array
                                        (
                                            [name] => stavka u segment 2 -> grupa1
                                            [measurement_unit_name] => m3
                                            [value_per_unit] => 0.00
                                            [objects] => Array
                                                (
                                                    [23] => Array
                                                        (
                                                            [quantity] => 0.00
                                                            [value_per_unit] => 0.00
                                                            [value] => 0.00
                                                            [object_name] => Predmer uvoz
                                                        )

                                                )

                                        )

                                )

                            [group_value] => Array
                                (
                                    [objects] => Array
                                        (
                                            [23] => Array
                                                (
                                                    [value] => 0.00
                                                    [value_per_unit] => 
                                                    [quantity] => 
                                                )

                                        )

                                    [measurement_unit_name] => 
                                    [value_per_unit] => 
                                )

                            [objects_cnt] => 1
                        )

                )

            [items] => Array
                (
                )

            [group_value] => Array
                (
                    [objects] => Array
                        (
                            [23] => Array
                                (
                                    [value] => 0.00
                                    [value_per_unit] => 
                                    [quantity] => 
                                )

                        )

                    [measurement_unit_name] => 
                    [value_per_unit] => 
                )

            [objects_cnt] => 1
        )

)


 * 
 */


