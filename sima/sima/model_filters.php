<?php
$work_contracts = WorkContract::model()->findAll([
            'model_filter' => [
                'calculated_end_date' => $begin_date . '<>' . $end_date,
//                'filter_scopes' => [
//                    'withoutConnectedContract' => [$begin_date, $end_date]
//                ]
//                'OR',
//                [
//                    'AND',
//                    ['filter_scopes' => 'withoutCloseWorkContract'],
//                    ['end_date' => $begin_date . '<>' . $end_date]
//
//                ],
//                [
//                    'AND',
//                    ['filter_scopes' => 'withCloseWorkContract'],
//                    ['calculated_end_date' => $begin_date . '<>' . $end_date],
//                ]              
            ]
        ]);

$work_contracts = WorkContract::model()->findAll([
    'model_filter' => [
        'AND',
        ['calculated_end_date' => $begin_date . '<>' . $end_date],
        [
            'NOT',
            [
                'filter_scopes' => [
                    'activeFromTo'
                ]
            ]
        ]
    ]
]);

$wc_cnt = WorkContract::model()->count([
    'model_filter' => [
        'AND',
        [
            'employee' => [
                'ids' => $work_contract->employee->id
            ],
            'work_position' => [
                'ids' => $work_contract->work_position->id
            ]
        ],
        [
            'OR',
            ['start_date' => $next_day],
            [
                'AND',
                ['start_date' => ['>',$work_contract->start_date]],
                ['start_date' => ['<=',$work_contract->end_date]],
            ]
        ]
    ]
]);

