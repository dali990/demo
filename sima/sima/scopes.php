<?php

function scopes()
{
    $alias = $this->getTableAlias();
    $uniq = SIMAHtml::uniqid();
    $work_contracts_table_name = WorkContract::model()->tableName();
    $closed_work_contracts_table_name = CloseWorkContract::model()->tableName();


    return array(
        'byName' => array('order' => $alias.'.start_date ASC, '.$alias.'.id'),
        'orderByStartDateASC' => array('order' => $alias.'.start_date ASC, '.$alias.'.id'),
        'hasDuplicatedStartDate' => [
            'condition' => "
                exists (
                    select 1 from $work_contracts_table_name wc2$uniq 
                    where $alias.id != wc2$uniq.id and $alias.start_date = wc2$uniq.start_date and $alias.employee_id = wc2$uniq.employee_id
                )
            "
        ],
        'withoutCloseWorkContract' => [
            'condition' => "not exists (select 1 from $closed_work_contracts_table_name cwc$uniq where cwc$uniq.work_contract_id = $alias.id)"
        ],
        'withCloseWorkContract' => [
            'condition' => "exists (select 1 from $closed_work_contracts_table_name cwc$uniq where cwc$uniq.work_contract_id = $alias.id)"
        ],
    );
    }
    
