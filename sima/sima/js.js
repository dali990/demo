trimWhiteSpaces(){            
            var body = this.$('body');
            
            this.recursiveTrimWhiteSpaces(body);
            
            this.editor.setContent(body.html());
            this.$emit('input', this.editor.getContent());
        },
        recursiveTrimWhiteSpaces(body){
            var _continue = false;
            if(body[0].firstElementChild.tagName === "P")
            {
                var p_first = body[0].firstElementChild;
                if(/^[&nbsp;\s]*$/.test(p_first.innerHTML) || p_first.innerHTML === '<br data-mce-bogus="1">')//ako sadrzi samo razmake ili novi red
                {
                    p_first.remove();
                    _continue = true;
                }
            }
            if(body[0].lastElementChild.tagName === "P")
            {
                var p_last = body[0].lastElementChild;
                if(/^[&nbsp;\s]*$/.test(p_last.innerHTML) || p_last.innerHTML === '<br data-mce-bogus="1">')
                {
                    p_last.remove();
                    _continue = true;
                }
            }
            if(_continue)
            {
                this.recursiveTrimWhiteSpaces(body);
            }
        }
        
        public_links2(){
            var _this = this;
            return sima.vue.shot.getters.model_list(this, 'PublicLink', {
                model_filter: {
                    type: this.type,
                    type_id: this.type_id,
                    scopes: {
                        hasAccess: [this.type]
                    }
                },
                order: 'created_time DESC'
            });
        },