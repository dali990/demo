<?php

class FixedAssetSpreadsheet extends SIMAPhpSpreadsheet
{
    protected $update_func;
    protected $date;
    protected $group_by;
      
    public function __construct($params, $update_func=null)
    {  
        $this->update_func = $update_func;
        parent::__construct($params);
    }

    protected function getContent()
    {
        $this->sheet = $this->getActiveSheet();
        $this->date = $this->params['date'];
        $this->group_by = $this->params['group_by'];
        
        $this->getHeader();
        $this->getFixedAsset();
    }
    
    private function getHeader() 
    {
        $this->sheet->getDefaultColumnDimension()->setWidth(12);

        $table_header = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => 'ffffff'],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => ['argb' => '677b80'],
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'ffffff'],
                ],
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true
            ]
        ];
        
        $this->sheet->getColumnDimension("B")->setWidth(30);
        $this->sheet->getColumnDimension("F")->setWidth(20);
        $this->sheet->getColumnDimension("G")->setWidth(20);
        $this->sheet->getColumnDimension("H")->setWidth(20);
        
        $this->sheet->setCellValue("A1", Yii::t('AccountingModule.FixedAssets', 'InventoryList'));
        $this->sheet->mergeCells("A1:H1");
        $this->sheet->getStyle("A1:H1")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->getStyle("A1")->getFont()->setSize(12);
        $this->sheet->getStyle("A1")->getFont()->setBold(true);
        
        $this->sheet->setCellValue("A2", $this->date);
        $this->sheet->getStyle("A2:H2")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->mergeCells("A2:H2");
        
        $this->sheet->getStyle("A3:H3")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->mergeCells("A3:H3");
         
        $this->sheet->setCellValue("A2", $this->date);
        
        $this->sheet->getRowDimension('4')->setRowHeight(40);
        $this->sheet->setCellValue("A4", Yii::t('FixedAssetsModule.FixedAsset', 'InventoryListFinancialNumber'));
        $this->sheet->setCellValue("B4", Yii::t('FixedAssetsModule.FixedAsset', 'Name'));
        $this->sheet->setCellValue("C4", Yii::t('FixedAssetsModule.FixedAsset', 'Quantity'));
        $this->sheet->setCellValue("D4", Yii::t('FixedAssetsModule.FixedAsset', 'PurchaseDate'));
        $this->sheet->setCellValue("E4", Yii::t('FixedAssetsModule.FixedAsset', 'DepreciationRate'));
        $this->sheet->setCellValue("F4", Yii::t('FixedAssetsModule.FixedAsset', 'PurchaseValue'));
        $this->sheet->setCellValue("G4", Yii::t('FixedAssetsModule.FixedAsset', 'ValueOff'));
        $this->sheet->setCellValue("H4", Yii::t('FixedAssetsModule.FixedAsset', 'CurrentValue'));
        
        $this->sheet->getStyle("A4:H4")->applyFromArray($table_header);
    }
    
    public function getFixedAsset() 
    {
        $this->selectCell("A", 4);
        $date = $this->date;
        if (empty($date))
        {
            $date = SIMAHtml::currentDateTime(false);
        }
        
        $date_object = new SIMADateTime($date);
        
        $criteria = new SIMADbCriteria([
            'Model' => FixedAssetType::class,
            'model_filter' => [
                'display_scopes' => 'withAllNames',
                'fixed_assets' => []
            ]
        ]);
        $group_by_model = FixedAssetType::class;
        if($this->group_by === 'location')
        {
            $group_by_model = CompanyLocation::class;
            $criteria = null;
        }
        
        $i = 1;
        $purchase_value_total_total = 0;
        $value_off_total_total = 0;
        $current_value_total_total = 0;
        if($this->group_by === 'location')
        {
            $fixed_asset_group_cnt = FixedAssetType::model()->withAllNames()->count();
        }
        else if($this->group_by === 'type')
        {
            $fixed_asset_group_cnt = CompanyLocation::model()->count();
        }
        $fixed_asset_group_part = round(100 / $fixed_asset_group_cnt, 0, PHP_ROUND_HALF_DOWN);
        $fixed_asset_group_prev_part_sum = 0;
        
        $total_row_style = [
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => ['argb' => 'a3a3a3'],
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'ffffff'],
                ],
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true
            ]
        ];
        //Dovlacimo grupe
        $group_by_model::model()->findAllByParts(function($group_by_models) use (
                $total_row_style, $date, $fixed_asset_group_part, &$fixed_asset_group_prev_part_sum, &$i,
                &$purchase_value_total_total, &$value_off_total_total, &$current_value_total_total, $date_object
        ) {
            foreach ($group_by_models as $_model)
            {
                $ac_fixed_assets_criteria = new SIMADbCriteria([
                    'Model' => AccountingFixedAsset::class,
                    'model_filter' => [
                        'usage_start_date' => ['<=', $date],
                        'order_by' => 'fixed_asset.display_name ASC',
                        'fixed_asset' => [
                            'fixed_asset_type' => [
                                'ids' => $_model->id
                            ],
                            'filter_scopes' => ['activeOnDate' => $date],
                            'scopes' => 'byName'
                        ],
                    ]
                ]);
                
                if($this->group_by === 'location')
                {
                    $ac_fixed_assets_criteria = new SIMADbCriteria([
                        'Model' => AccountingFixedAsset::class,
                        'model_filter' => [
                            'usage_start_date' => ['<=', $date],
                            'order_by' => 'fixed_asset.display_name ASC',
                            'fixed_asset' => [
                                'location' => [
                                    'ids' => $_model->id
                                ],
                                'filter_scopes' => ['activeOnDate' => $date],
                                'scopes' => 'byName'
                            ]
                        ]
                    ]);
                }
                $ac_fixed_assets_cnt = AccountingFixedAsset::model()->count($ac_fixed_assets_criteria);
                if ($ac_fixed_assets_cnt > 0)
                {
                    $display_name = 'display_name_full';
                    if($this->group_by === 'location')
                    {
                        $display_name = 'DisplayName';
                    }
        
                    $this->selectNextRow();
                    $this->selectColumn("A");
                    $this->sheet->mergeCells($this->getCurrentCell().":H".$this->getCurrentRow());
                    $this->sheet->setCellValue($this->getCurrentCell(), $_model->{$display_name});

                    $this->getAccountingFixedAssets(
                        $ac_fixed_assets_cnt, $fixed_asset_group_part, $fixed_asset_group_prev_part_sum,
                        $date_object,
                        $purchase_value_total_total, $value_off_total_total, $current_value_total_total,
                        $ac_fixed_assets_criteria, $total_row_style
                    );
                    
                }
                
                $fixed_asset_group_prev_part_sum += $fixed_asset_group_part;
                
                $i++;
            }
            //Za sva osnovna sredstva bez lokacije
            if($this->group_by === 'location')
            {
                $this->selectNextRow();
                $this->selectColumn("A");
                $this->sheet->mergeCells($this->getCurrentCell().":H".$this->getCurrentRow());
                
                $ac_fixed_assets_criteria_no_location = new SIMADbCriteria([
                    'Model' => AccountingFixedAsset::class,
                    'model_filter' => [
                        'usage_start_date' => ['<=', $date],
                        'order_by' => 'fixed_asset.display_name ASC',
                        'fixed_asset' => [
                            'location' => [
                                'ids' => 'null'
                            ],
                            'filter_scopes' => ['activeOnDate' => $date],
                            'scopes' => 'noLocation',
                        ]
                    ]
                ]);
                $ac_fixed_assets_cnt = AccountingFixedAsset::model()->count($ac_fixed_assets_criteria_no_location);
                if ($ac_fixed_assets_cnt > 0)
                {
                    $this->getAccountingFixedAssets(
                        $ac_fixed_assets_cnt, $fixed_asset_group_part, $fixed_asset_group_prev_part_sum,
                        $date_object,
                        $purchase_value_total_total, $value_off_total_total, $current_value_total_total,
                        $ac_fixed_assets_criteria_no_location, $total_row_style
                    );

                }
            }
            
            //Ukupno sve
            $this->selectNextRow();
            $this->selectColumn("A");
            $this->sheet->mergeCells($this->getCurrentCell().":H".$this->getCurrentRow());
            $this->selectNextRow();
            $this->sheet->mergeCells($this->getCurrentCell().":E".$this->getCurrentRow());
            $this->sheet->setCellValue($this->getCurrentCell(), Yii::t('FixedAssetsModule.FixedAsset', 'TotalTotal'));
            //Nabavna vrednost
            $this->selectColumn("F");
            $this->sheet->setCellValue($this->getCurrentCell(), $purchase_value_total_total);
            $this->sheet->getStyle($this->getCurrentCell())->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //Otpisana vrednost
            $this->sheet->setCellValue($this->selectNextCell(), $value_off_total_total);
            $this->sheet->getStyle($this->getCurrentCell())->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //Knjigovodstvena vrednost
            $this->sheet->setCellValue($this->selectNextCell(), $current_value_total_total);
            $this->sheet->getStyle($this->getCurrentCell())->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            
            $this->sheet->getStyle("A".$this->getCurrentRow().":".$this->getCurrentCell())->applyFromArray($total_row_style);
            
        }, $criteria);
               
    }
    
    public function getAccountingFixedAssets(
        $ac_fixed_assets_cnt, $fixed_asset_group_part, &$fixed_asset_group_prev_part_sum,
        $date_object,
        &$purchase_value_total_total, &$value_off_total_total, &$current_value_total_total,
        $ac_fixed_assets_criteria, $total_row_style
    ) {

        $purchase_value_total = 0;
        $value_off_total = 0;
        $current_value_total = 0;

        $j = 1;
        AccountingFixedAsset::model()->findAllByParts(function($ac_fixed_assets) use (
                $ac_fixed_assets_cnt, $fixed_asset_group_part, &$fixed_asset_group_prev_part_sum, &$j,
                &$purchase_value_total, &$value_off_total, &$current_value_total, $date_object
        ) {
            foreach ($ac_fixed_assets as $ac_fixed_asset)
            {
                $percent = round($fixed_asset_group_prev_part_sum + (($j/$ac_fixed_assets_cnt)*$fixed_asset_group_part), 0, PHP_ROUND_HALF_DOWN);
                if (!empty($this->update_func))
                {
                    call_user_func($this->update_func, $percent);
                }
                $this->selectNextRow();
                $this->selectColumn("A");

                $fixed_asset = $ac_fixed_asset->fixed_asset;

                $financial_number = $ac_fixed_asset->order;
                $purchase_date = $ac_fixed_asset->usage_start_date;
                $accounting_depreciation_rate = $ac_fixed_asset->accounting_depreciation_rate;
                $purchase_value = $ac_fixed_asset->purchaseValueOnDate($date_object);
                $value_off = $ac_fixed_asset->depreciationOnDate($date_object);
                $current_value = $purchase_value - $value_off;

                $purchase_value_total += $purchase_value;
                $value_off_total += $value_off;
                $current_value_total += $current_value;

                $j++;
                $this->sheet->setCellValue($this->getCurrentCell(), $financial_number ?? " ");//Fin br
                $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $this->sheet->setCellValue($this->selectNextCell(), $fixed_asset->DisplayName);//naziv
                $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $this->sheet->setCellValue($this->selectNextCell(), $fixed_asset->quantityOnDate($date_object));//kolicina
                $this->sheet->getStyle($this->getCurrentCell())->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $this->sheet->setCellValue($this->selectNextCell(), SIMAHtml::DbToUserDate($purchase_date));//datum nabavke
                $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $this->sheet->setCellValue($this->selectNextCell(), $accounting_depreciation_rate);//stepem amortizacije
                $this->sheet->getStyle($this->getCurrentCell())->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $this->sheet->setCellValue($this->selectNextCell(), $purchase_value);//nabavna vrednost
                $this->sheet->getStyle($this->getCurrentCell())->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $this->sheet->setCellValue($this->selectNextCell(), $value_off);//otpisna vrednost
                $this->sheet->getStyle($this->getCurrentCell())->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $this->sheet->setCellValue($this->selectNextCell(), $current_value);//knjigovodstvena vrednost
                $this->sheet->getStyle($this->getCurrentCell())->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            }
        }, $ac_fixed_assets_criteria);

        //Ukupno
        $this->selectNextRow();
        $this->selectColumn("A");
        $this->sheet->mergeCells($this->getCurrentCell().":E".$this->getCurrentRow());
        $this->sheet->setCellValue($this->getCurrentCell(), Yii::t('BaseModule.Common', 'Total'));
        //Nabavna vrednost
        $this->selectColumn("F");
        $this->sheet->setCellValue($this->getCurrentCell(), $purchase_value_total);
        $this->sheet->getStyle($this->getCurrentCell())->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        //Otpisana vrednost
        $this->sheet->setCellValue($this->selectNextCell(), $value_off_total);
        $this->sheet->getStyle($this->getCurrentCell())->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        //Knjigovodstvena vrednost
        $this->sheet->setCellValue($this->selectNextCell(), $current_value_total);
        $this->sheet->getStyle($this->getCurrentCell())->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $this->sheet->getStyle("A".$this->getCurrentRow().":".$this->getCurrentCell())->applyFromArray($total_row_style);

        $purchase_value_total_total += $purchase_value_total;
        $value_off_total_total += $value_off_total;
        $current_value_total_total += $current_value_total;
    }
    
}

