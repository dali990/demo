<?php
 
class AgriculturalHoldingTradeViewSpreadsheet extends SIMAPhpSpreadsheet
{
    public $sheet = null;
    public $update_func = null;
    
    public function __construct($params=[], $update_func=null)
    {
        parent::__construct($params);
        $this->update_func = $update_func;
    }
    
    protected function getContent() 
    {
        $this->sheet = $this->getActiveSheet();
        $this->sheet->getDefaultColumnDimension()->setWidth(16);
        
        //Naslov
        $title_cell = $this->getCurrentCell();
        $this->sheet->setCellValue($title_cell, Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'AgriculturalHoldingTrades'));
        $this->sheet->getStyle($title_cell)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $this->selectNextRow();
        $this->sheet->mergeCells($title_cell.":M".$this->getCurrentRow());
        //Godina
        $this->selectNextRow();
        $this->sheet->setCellValue($this->getCurrentCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'Year'));
        $this->sheet->setCellValue($this->selectNextCell(), $this->params['year']['year']);
        $this->sheet->getStyle($this->getCurrentCell())->getFont()->setBold(true);
        $this->selectNextRow();
        $this->selectColumn("A");
        //Tabela sa podacima
        $this->insertTable();
    }
    
    public function insertTable() 
    {
        //Header tabele
        $first_header_cell = $this->getCurrentCell();
        $this->sheet->setCellValue($first_header_cell, Yii::t('LegalModule.AgriculturalHolding', 'AgriculturalHoldingNumber'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->setCellValue($this->selectNextCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'AgriculturalHoldingName'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->setCellValue($this->selectNextCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'TradeAmount'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->setCellValue($this->selectNextCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'ReliefBill'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->setCellValue($this->selectNextCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'UnreliefBill'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->setCellValue($this->selectNextCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'TotalAmount'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->setCellValue($this->selectNextCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'DateFrom'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->setCellValue($this->selectNextCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'DateTo'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->setCellValue($this->selectNextCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'TradeType'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->setCellValue($this->selectNextCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'IdentificationType'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->setCellValue($this->selectNextCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'IdentificationNumber'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->setCellValue($this->selectNextCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'Address'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->setCellValue($this->selectNextCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'PhoneNumber'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $this->sheet->setCellValue($this->selectNextCell(), Yii::t('AccountingModule.AgriculturalHoldingTradeView', 'EmailAddress'));
        $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $last_header_cell = $this->getCurrentCell();
        
        //Podaci tabele
        $this->selectNextRow();
        $this->selectColumn("A");
        
        $agricultural_holding_trade_views = AgriculturalHoldingTradeView::model()->findAll([
            'model_filter' => [
                'year' => [
                    'ids' => $this->params['year']['id']
                ]
            ]
        ]);
        
        foreach ($agricultural_holding_trade_views as $agricultural_holding_trade_view)
        {
            $bills_total = $agricultural_holding_trade_view->bills_amount+$agricultural_holding_trade_view->relief_bills_amount+$agricultural_holding_trade_view->unrelief_bills_amount;
            $identification_type = "";
            $identification_number = "";
            if($agricultural_holding_trade_view->PIB)
            {
                $identification_type = "PIB";
                $identification_number = $agricultural_holding_trade_view->PIB;
            }
            else if($agricultural_holding_trade_view->JMBG)
            {
                $identification_type = "JMBG";
                $identification_number = $agricultural_holding_trade_view->JMBG;
            }
            $address = "";
            $address_model = $agricultural_holding_trade_view->partner->getmain_or_any_address();
            if(!empty($address_model))
            {
                $address = $address_model->DisplayName;
            }
            $phone_number = "";
            $phone_number_model = $agricultural_holding_trade_view->partner->getmain_or_any_phone_contact();;
            if(!empty($phone_number_model))
            {
                $phone_number = $phone_number_model->DisplayName;
            }
            $email = "";
            $email_model = $agricultural_holding_trade_view->partner->getmain_or_any_email();
            if(!empty($email_model))
            {
                $email = $email_model->email;
            }
            $this->sheet->setCellValue($this->getCurrentCell(), $agricultural_holding_trade_view->agricultural_holding_number);
            $this->sheet->setCellValue($this->selectNextCell(), $agricultural_holding_trade_view->partner->display_name);
            $this->sheet->setCellValue($this->selectNextCell(), $agricultural_holding_trade_view->bills_amount);
            $this->formatNumberCell($this->getCurrentCell());
            $this->sheet->setCellValue($this->selectNextCell(), $agricultural_holding_trade_view->relief_bills_amount);
            $this->formatNumberCell($this->getCurrentCell());
            $this->sheet->setCellValue($this->selectNextCell(), $agricultural_holding_trade_view->unrelief_bills_amount);
            $this->formatNumberCell($this->getCurrentCell());
            $this->sheet->setCellValue($this->selectNextCell(), $bills_total);
            $this->formatNumberCell($this->getCurrentCell());
            $this->sheet->setCellValue($this->selectNextCell(), $agricultural_holding_trade_view->date_from);
            $this->sheet->setCellValue($this->selectNextCell(), $agricultural_holding_trade_view->date_to);
            $this->sheet->setCellValue($this->selectNextCell(), "");
            $this->sheet->setCellValue($this->selectNextCell(), $identification_type);
            $this->sheet->setCellValue($this->selectNextCell(), $identification_number);
            $this->sheet->setCellValue($this->selectNextCell(), $address);
            $this->sheet->setCellValue($this->selectNextCell(), $phone_number);
            $this->sheet->setCellValue($this->selectNextCell(), $email);
            
            $last_table_cell= $this->getLastColumn().$this->getCurrentRow();
            $this->selectNextRow();
            $this->selectColumn("A");
        }
        
        $this->styleTable($first_header_cell, $last_header_cell, $last_table_cell);
    }
    
    private function styleTable($first_header_cell, $last_header_cell, $last_table_cell)
    {        
        $table_header = [
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => ['argb' => '427FB3'],
            ],
            'font' => [
                'color' => ['argb' => 'FFFFFF'],
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'FFFFFF'],
                ],
            ],
        ];
        $this->sheet->getStyle($first_header_cell.":".$last_header_cell)->applyFromArray($table_header);     
        
        $table_body = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];
        $this->sheet->getStyle($first_header_cell.":".$last_table_cell)->applyFromArray($table_body);
    }
    
    private function formatNumberCell($cell)
    {
        $this->sheet->getStyle($cell)->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        $this->sheet->getStyle($cell)
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
    }
    
}
