<page size="A4">
    <!--Rucno postavljena visina i sirina kako bi se postavila visina table po celoj strani-->
    <div id="container" class="building-measurment-sheet-report bms-report" style="width: 185mm; height: 265mm;">
        <table class="positioning-table" style="height: 100%;">
            <tr>
                <td class="pt-cell" style="padding-bottom: 3mm">
                    <header>
                        <table class="bms-header-table">
                            <tr>
                                <td style="width: 33%; padding: 0 2mm 0 0">
                                    <div class="field" style="width: 100%">
                                        <span class="field-label"><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','ConstructionSite')?>: </span>
                                        <div class="underlined-field">
                                        <span class="field-content">Nabavka, transport i ugrađivanje PVC
fazonskog elementa za povezivanje PVC cevi sa
gusanom cevi. Obračun po kom.Ø160 Nabavka, transport i ugrađivanje PVC
fazonskog elementa za povezivanje PVC cevi sa
gusanom cevi. Obračun po kom.Ø160</span>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="center-content">
                                        <small>(<?=Yii::t('BuildingsModule.BuildingMeasurementSheet','TagOrNumber')?>)</small>
                                    </div>
                                </td>
                                <td style="width: 33%; padding-right: 2mm">
                                    <div class="field" style="width: 100%">
                                        <span class="field-label"><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','Sector')?>: </span>
                                        <div class="underlined-field">
                                            <span class="field-content"><?=$sector?></span>
                                        </div>
                                    </div>
                                    <div class="center-content">
                                        <small>(<?=Yii::t('BuildingsModule.BuildingMeasurementSheet','Number')?>)</small>
                                    </div>
                                </td>
                                <td style="width: 33%;">
                                    <div class="field" style="width: 100%">
                                        <span class="field-label"><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','Object')?>: </span>
                                        <div class="underlined-field">
                                            <span class="field-content"><?=$object->name?></span>
                                        </div>
                                    </div>
                                    <div class="center-content">
                                        <small>(<?=Yii::t('BuildingsModule.BuildingMeasurementSheet','Number')?>)</small>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </header>
                </td>
            </tr>
            <tr>
                <td class="pt-cell" style="height: 100%;">
                    <div class="main" style="height: 100%;">
                        <div class="title" style="padding-left: 15mm">
                            <?=Yii::t('BuildingsModule.BuildingMeasurementSheet','ConstructionAccountSheet')?>
                        </div>
                        <div class="page-num">
                            <span><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','PageNum')?>:</span>
                            <div class="underlined-inline text-center" style="min-width: 20mm;">1</div>
                        </div>
                        <div style="clear:both"></div>
                        <table class="bms-table" style="height:100%;">
                            <thead>
                            <tr class="first-row">
                                <td style="width: 70%" colspan="2">
                                    <div class="field" style="width: 100%;margin-top: 2mm;vertical-align: bottom">
                                        <span class="field-label word-no-break"><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','KindOfWork')?> </span>
                                        <div class="underlined-field">
                                            <span class="field-content"><?=$kind_of_work?></span>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <table class="positioning-table">
                                        <tr>
                                            <td class="pt-cell">
                                                <div class="field" style="width: 100%">
                                                    <span class="field-label word-no-break"><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','BuildingNorm')?> </span>
                                                    <div class="underlined-field">
                                                        <span class="field-content"></span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="pt-cell" style="vertical-align: bottom; height: 7mm">
                                                <div class="field" style="width: 100%; margin-top: 4mm">
                                                    <span class="field-label word-no-break"><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','Code')?> </span>
                                                    <div class="underlined-field">
                                                        <span class="field-content"></span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table class="positioning-table">
                                        <tr>
                                            <td class="pt-cell">
                                                <div class="field center-content" style="width: 100%">
                                                    <span class="field-label word-no-break"><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','DraftCalculation')?> </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="pt-cell" style="vertical-align: bottom; height: 7mm">
                                                <div class="field" style="width: 100%; margin-top: 4mm">
                                                    <span class="field-label word-no-break"><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','Num')?> </span>
                                                    <div class="underlined-field">
                                                        <span class="field-content"></span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table class="positioning-table">
                                        <tr>
                                            <td class="pt-cell">
                                                <div class="field center-content" style="width: 100%">
                                                    <span class="field-label word-no-break"><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','AccountingPosition')?> </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="pt-cell" style="vertical-align: bottom; height: 7mm">
                                                <div class="field" style="width: 100%; margin-top: 4mm">
                                                    <span class="field-label word-no-break"><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','Num')?> </span>
                                                    <div class="underlined-field center-content">
                                                        <span class="field-content"><?=$bill_of_quantity_item->getFullOrderPath()?></span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="center-content">                        
                                    <span><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','UnitPriceDin')?></span>
                                    <div class="underlined-inline text-center" style="min-width: 20mm;">
                                        <span class="field-content"><?=$bill_of_quantity_item->value_per_unit->round(2)?></span>
                                    </div>
                                    <span><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','Per')?></span>
                                    <div class="underlined-inline text-center" style="min-width: 20mm;">
                                        <span class="field-content"><?=$bill_of_quantity_item->measurement_unit->name?></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div class="field" style="width: 100%">
                                        <span class="field-label"><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','Mesasure')?> </span>
                                        <span class="underlined-field">
                                            <span class="field-content"></span>
                                        </span>
                                    </div> 
                                </td>
                            </tr>
                            <tr>
                                <td>A =</td>
                                <td>B =</td>
                                <td>A + B</td>
                                <td><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','Monthly')?></td>
                                <td><?=Yii::t('BuildingsModule.BuildingMeasurementSheet','Total')?></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($sheet_items as $sheet_item): ?>
                            <tr style="page-break-inside: avoid;">
                                <td colspan="3" stle=" page-break-inside: avoid;">
                                    <div class="table-content-wrapper"">
                                        <div class="table-content-row">
                                            <div class="table-content-cell">
                                                <div class="sheet-item">
                                                    <div class="sheet-description">
                                                        <?=$sheet_item->description?>
                                                    </div>
                                                    <div class="sheet-calculation">
                                                        <?=$sheet_item->calculation?> = <u><?=$sheet_item->amount?></u>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-content-row">
                                            <div class="table-content-cell to-transfer">
                                                <?=Yii::t('BuildingsModule.BuildingMeasurementSheet','ToTransfer')?>:
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td style="vertical-align: bottom; text-align: right;"></td>
                                <td style="vertical-align: bottom; text-align: right;"></td>
                            </tr>
                            <?php endforeach ?>
                            <tr>
                                <td colspan="3" stle=" page-break-inside: avoid;">
                                    <div class="table-content-row">
                                        <div class="table-content-cell to-transfer">
                                            <?=Yii::t('BuildingsModule.BuildingMeasurementSheet','ToTransfer')?>:
                                        </div>
                                    </div>
                                </td>
                                <td style="vertical-align: bottom; text-align: right;"></td>
                                <td style="vertical-align: bottom; text-align: right;">
                                    <div class="quantity-done">
                                        <?=$building_measurement_sheet->quantity_done?>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</page>
