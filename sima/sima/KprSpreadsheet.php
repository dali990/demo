<?php

class KprSpreadsheet extends SIMAPhpSpreadsheet
{
    protected $sheet;
    protected $month;
    protected $year;
    protected $name;
    
    public function __construct($month, $year, $name, $update_func)
    {  
        $this->month = $month;
        $this->year = $year;
        $this->name = $name;
        $this->update_func = $update_func;
        
        parent::__construct();

    }
    
    protected function getContent()
    {
        $this->sheet = $this->getActiveSheet();
        
        $this->getHeader();
        $month_model = Month::get($this->month, $this->year);
        $this->getItems($month_model->id);
    }
    
    private function getHeader() 
    {
        $this->sheet->getDefaultColumnDimension()->setWidth(16);
        $table_header = [
            'font' => [
                'bold' => true,
                'size' => 9
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM,
                'wrapText' => true
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => ['argb' => '92EDE9'],
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];
        $this->sheet->getStyle("A1:T5")->applyFromArray($table_header);
        $this->sheet->getStyle("A6:T6")->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => ['argb' => '00C9CC'],
            ],
        ]);
        //Kolona 1
        $this->sheet->setCellValue("A1", "Redni broj");
        $this->sheet->mergeCells("A1:A5");
        $this->sheet->getColumnDimension("A")->setWidth(10);       
        $this->sheet->setCellValue("A6", 1);
        //2
        $this->sheet->setCellValue("B1", "Datum knjiženja isprave");
        $this->sheet->mergeCells("B1:B5");
        $this->sheet->setCellValue("B6", 2);
        //3
        $this->sheet->setCellValue("C1", "Datum plaćanja pri uvozu plaćanja naknada poljoprivredniku");
        $this->sheet->mergeCells("C1:C5");
        $this->sheet->setCellValue("C6", 3);
        //RAČUN ILI DRUGI DOKUMENT           
        $this->sheet->setCellValue("D1", "RAČUN ILI DRUGI DOKUMENT");
        $this->sheet->mergeCells("D1:M1");
        //4
        $this->sheet->setCellValue("D2", "Broj računa");
        $this->sheet->mergeCells("D2:D5");
        $this->sheet->setCellValue("D6", 4);
        //5
        $this->sheet->setCellValue("E2", "Datum izdavanja računa (ili drugog dokumenta)");
        $this->sheet->mergeCells("E2:E5");
        $this->sheet->setCellValue("E6", 5);
//        //Dobavljač           
//        $this->sheet->setCellValue("F2", "Dobavljač");
//        $this->sheet->mergeCells("F2:G2");
        //6
        $this->sheet->setCellValue("F2", "Naziv (ime i sedište) dobavljača");
        $this->sheet->mergeCells("F2:F5");
        $this->sheet->setCellValue("F6", 6);
        //7
        $this->sheet->setCellValue("G2", "PIB ili JMBG");
        $this->sheet->mergeCells("G2:G5");
        $this->sheet->setCellValue("G6", 7);
        //8
        $this->sheet->setCellValue("H2", "Ukupna naknada sa PDV tač.16");
        $this->sheet->mergeCells("H2:H5");
        $this->sheet->setCellValue("H6", 8);
        //9
        $this->sheet->setCellValue("I2", "Naknada bez PDV(na koju je obračunat PDV koji se može odbiti)");
        $this->sheet->mergeCells("I2:I5");
        $this->sheet->setCellValue("I6", 9);
        //9a
        $this->sheet->setCellValue("J2", "Naknada bez PDV(na koju je obračunat PDV koji se ne može odbiti)");
        $this->sheet->mergeCells("J2:J5");
        $this->sheet->setCellValue("J6", "9a");
        //10
        $this->sheet->setCellValue("K2", "Oslobođene nabavke tač.18");
        $this->sheet->mergeCells("K2:K5");
        $this->sheet->setCellValue("K6", 10);
        //11
        $this->sheet->setCellValue("L2", "Nabavka od lica koja nisu obveznici PDV tač.15");
        $this->sheet->mergeCells("L2:L5");
        $this->sheet->setCellValue("L6", 11);
        //12
        $this->sheet->setCellValue("M2", "Naknada za uvezena dobra na koje se ne plaća PDV tač.22");
        $this->sheet->mergeCells("M2:M5");
        $this->sheet->setCellValue("M6", 12);
        //13
        $this->sheet->setCellValue("N1", "Ukupan iznos obračunatog prethodnog PDV tač.17");
        $this->sheet->mergeCells("N1:N5");
        $this->sheet->setCellValue("N6", 13);
        //14
        $this->sheet->setCellValue("O1", "Iznos prethodnog PDV koji se može odbiti");
        $this->sheet->mergeCells("O1:O5");
        $this->sheet->setCellValue("O6", 14);
        //15
        $this->sheet->setCellValue("P1", "Iznos prethodnog PDV koji se ne može odbiti");
        $this->sheet->mergeCells("P1:P5");
        $this->sheet->setCellValue("P6", 15);
        //Uvoz
        $this->sheet->setCellValue("Q1", "Uvoz");
        $this->sheet->mergeCells("Q1:R1");
        //16
        $this->sheet->setCellValue("Q2", "Vrednost dobara bez PDV tač.21");
        $this->sheet->mergeCells("Q2:Q5");
        $this->sheet->setCellValue("Q6", 16);
        //17
        $this->sheet->setCellValue("R2", "Iznos PDV tač. 23");
        $this->sheet->mergeCells("R2:R5");
        $this->sheet->setCellValue("R6", 17);
        //Naknada poljoprivredniku
        $this->sheet->setCellValue("S1", "Naknada poljoprivredniku");
        $this->sheet->mergeCells("S1:T1");
        //18
        $this->sheet->setCellValue("S2", "Vrednost primljenih dobara i usluga");
        $this->sheet->mergeCells("S2:S5");
        $this->sheet->setCellValue("S6", 18);
        //19
        $this->sheet->setCellValue("T2", "Iznos naknade od 5% tač.24");
        $this->sheet->mergeCells("T2:T5");
        $this->sheet->setCellValue("T6", 19);
    }
    
    private function getItems($month_model_id)
    {
        $this->selectCell("A", 7);
        
        $rows = ($this->name === KPR::class) ?
            $this->name::model()->byOrderInYear()->inMonth($this->month, $this->year)->findAll() :
            $this->name::model()->findAll([
                'model_filter' => [
                    'scopes' => ['byOrder'],
                    'month' => [
                        'ids' => $month_model_id
                    ],
                    'order_by' => 'order_in_year desc'
                ]
            ]);
        
        $sum_rows = ($this->name === KPR::class) ?
            [
                'KPR_8' => 0,
                'KPR_9' => 0,
                'KPR_9a' => 0,
                'KPR_10' => 0,
                'KPR_11' => 0,
                'KPR_12' => 0,
                'KPR_13' => 0,
                'KPR_14' => 0,
                'KPR_15' => 0,
                'KPR_16' => 0,
                'KPR_17' => 0,
                'KPR_18' => 0,
                'KPR_19' => 0,
            ] :
            [
                'column_8' => 0,
                'column_9' => 0,
                'column_9a' => 0,
                'column_10' => 0,
                'column_11' => 0,
                'column_12' => 0,
                'column_13' => 0,
                'column_14' => 0,
                'column_15' => 0,
                'column_16' => 0,
                'column_17' => 0,
                'column_18' => 0,
                'column_19' => 0,
            ];
        
        $i = 1;
        $rows_cnt = count($rows);
        
        foreach ($rows as $row)
        {
            if (!empty($this->update_func))
            {
                call_user_func($this->update_func, round(($i++/$rows_cnt)*100, 0, PHP_ROUND_HALF_DOWN));
            }                
            //Kolona 1
            $this->sheet->setCellValue($this->getCurrentCell(), $row->order_in_year);     
            $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            //2
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $row->bill->booking_date);
            $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            //3
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), "");
            //4
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $row->bill->bill_number);
            //5
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $row->bill->income_date);
            $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            //6
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $row->bill->partner->DisplayName);
            //7
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $row->bill->partner->pib_jmbg);
            $this->sheet->getStyle($this->getCurrentCell())->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            //8
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $this->getColumnValue($row, 8));
            //9
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $this->getColumnValue($row, 9));
            //9a
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $this->getColumnValue($row, "9a"));
            //10 
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $this->getColumnValue($row, 10));
            //11 
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $this->getColumnValue($row, 11));
            //12 
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $this->getColumnValue($row, 12));
            //13 
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $this->getColumnValue($row, 13));
            //14 
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $this->getColumnValue($row, 14));
            //15 
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $this->getColumnValue($row, 15));
            //16 
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $this->getColumnValue($row, 16));
            //17 
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $this->getColumnValue($row, 17));
            //18
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $this->getColumnValue($row, 18));
            //19 
            $this->selectNextCell();
            $this->sheet->setCellValue($this->getCurrentCell(), $this->getColumnValue($row, 19));
            
            foreach ($sum_rows as $key => $value)
            {
                $sum_rows[$key] += $row->$key;
            }
            
            $this->selectNextRow();
            $this->selectColumn("A");
            $i++;
        }
        
        $this->sheet->setCellValue($this->getCurrentCell(), "∑ ");
        $this->sheet->getStyle($this->getCurrentCell())->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 12,
                'color' => ['argb' => '00C9CC'],
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
                'indent' => 2
            ]
        ]);
        $this->sheet->getRowDimension($this->getCurrentRow())->setRowHeight(20);
        $this->sheet->mergeCells($this->getCurrentCell().":G".$this->getCurrentRow());
        $this->selectColumn("H");
        foreach ($sum_rows as $key => $value)
        {
            $this->sheet->setCellValue($this->getCurrentCell(), $value);
            $this->selectNextCell();
        }
        
        $this->sheet->getStyle("H7:".$this->getLastCell())->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        $this->sheet->getStyle($this->getCurrentCell())->getFont()->getColor()
            ->setARGB("00C9CC");
        $this->sheet->getStyle("H".$this->getCurrentRow().":T".$this->getCurrentRow())->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('00C9CC');
    }
    
    private function getColumnValue($model, $column_number) 
    {
        $value = 0;
        if($this->name === KPR::class)
        {
            $column_name = "KPR_".$column_number;
        }
        else
        {
            $column_name = "column_".$column_number;
        }
        $value = $model->{$column_name};
        if($value != 0)
        {
            return $value;
        }
        return null;
    }
}

