enum Size { Small = 's', Medium = 'm', Large = 'l'};
let mySize: Size = Size.Medium;
let a;
console.log(Size);
console.log(mySize);

function nameB(params: number, check?: string) {
    if(check == 'as'){
        console.log(check);
    }
    params;
}
nameB(1);

let mixed = [1, 'test', false];
mixed.push(false);

let m: (string|number)[] = [];
m.push('ss');

const names: Array<string> = [];

type todo = {
    getName(): string
}
function displayName<T extends todo, U extends number>(p: T, age: U): string {
    return p.getName() + " " + age;
}
const person = {
    getName: () => {
        return "Person";
    }
}
displayName(person, 45);